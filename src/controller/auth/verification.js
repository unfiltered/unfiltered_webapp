import {
  getAllChatsForSingleUser,
  deleteSelectedMessages,
  deleteChatTotally,
  requestOtp,
  verifyOtp,
  verifyemail,
  verifyprofile,
  preference,
  MatchFound,
  UserdataPrefences,
  UserProfile,
  UserNewData,
  PhoneExistVerify,
  facebooklogin,
  Profiledelete,
  SearchUser,
  friendreq,
  friendlist,
  pastdatelist,
  pendingdatelist,
  friendreqres,
  reportuser,
  Otheruserprofile,
  likeby,
  onlineuser,
  superlikeby,
  recentvisitor,
  mylikes,
  mysuperlike,
  passedby,
  likepeople,
  superlikepeople,
  newplan,
  newcoins,
  coinhistory,
  myunlikeuser,
  searchseemefriend,
  addfriend,
  unMatchUser,
  updateseemeid,
  getseemeid,
  searchprefence,
  searchprefencepost,
  blockUser,
  Blockuserreason,
  getAllChats,
  messageWithoutMatch,
  coinChat,
  getActivePlan,
  activateBoost,
  getBoostDetails,
  ReWindUser,
  searchUserByPagination,
  getAllFriends,
  getAllFriendsReq,
  thumbsUpMe,
  iThumbsUpped,
  history,
  uploadMedia,
} from "../../services/auth";
import { API_HOST } from "../../lib/config";

// Reqest OTP Module
export const mobileVerification = (data) => {
  return new Promise((resolve, reject) => {
    requestOtp(data)
      .then((data) => {
        console.log("succ", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err", err);
        return reject(err);
      });
  });
};

// Verify OTP Module
export const otpVerification = (data) => {
  return new Promise((resolve, reject) => {
    verifyOtp(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const _revOtpVerification = (objData) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: "Basic M2VtYmVkOkRhdGVzbmRpbWUwMDc=",
        lang: "en",
      },
      body: JSON.stringify(objData),
    };
    let status = undefined;
    fetch(`${API_HOST}/verificaitonCode`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const uploadMediaOverLocalServer = (data) => {
  return new Promise((resolve, reject) => {
    uploadMedia(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const UnfriendUser = (token, targetUserId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ targetUserId: targetUserId, statusCode: 3 }),
    };
    let status = undefined;
    fetch(`${API_HOST}/unfriendUser`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const pruchasePlanFromAPI = (token, object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${API_HOST}/paymentAuthorize`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const purchaseSubscription = (token, object) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${API_HOST}/subscription`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getLikesOnBoost = (token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/boostWithLike`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const referralCodeExistVerification = (refCode, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ referelCode: refCode }),
    };
    let status = undefined;
    fetch(`${API_HOST}/referelCodeExistsVerificaiton`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// get user posts GET
export const getAllPosts = (token, offset, limit) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/userPost?offset=${offset}&limit=${limit}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const DateRating = (token, dateId, rate) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({
        dateId: dateId,
        rate: rate,
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/dateRating`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const RescheduleDate = (token, response, date_id, proposedOn, dateType, lat, lan, placeName, PLAN) => {
  let isFree = 0;
  if (PLAN === "Unfiltered Premium" || PLAN === "Unfiltered Elite") {
    isFree = 1;
  }
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({
        response: response,
        date_id: date_id,
        proposedOn: proposedOn,
        dateType: dateType,
        latitude: lat,
        longitude: lan,
        placeName: placeName,
        isFree: isFree,
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/dateResponse`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// POST -> create date
export const createDate = (token, targetUserId, proposedOn, dateType, lat, lng, placeName, PLAN) => {
  let isFree = 0;
  if (PLAN === "Unfiltered Premium" || PLAN === "Unfiltered Elite") {
    isFree = 1;
  }
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({
        targetUserId: targetUserId,
        proposedOn: proposedOn,
        dateType: dateType,
        latitude: lat,
        longitude: lng,
        placeName: placeName,
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/date`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getFCM = (token, uid) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `key=AIzaSyArN3LsON9n1q4TDvK1hleqiFqKj0XAB9g`,
      },
    };
    let status = undefined;
    fetch(`https://iid.googleapis.com/iid/v1/${token}/rel/topics/${uid}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const likeOrDislikePost = (token, type, postId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ type: type, postId: postId }),
    };
    let status = undefined;
    fetch(`${API_HOST}/userPostLikeUnlike`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getCommentsForSelectedPostWithPagination = (token, postId, offset, limit) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/userPostComment?postId=${postId}&offset=${offset}&limit=${limit}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getCommentsForSelectedPost = (token, postId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/userPostComment?postId=${postId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getLikersOfThePost = (token, postId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/userPostLike?postId=${postId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const addCommentForSelectedPost = (token, postId, comment) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ postId: postId, comment: comment }),
    };
    let status = undefined;
    fetch(`${API_HOST}/userPostComment`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// export const GetBank = (token, userId) => {
//   return new Promise((resolve, reject) => {
//     let data = {
//       method: "POST",
//       headers: {
//         Accept: "application/json",
//         "Content-Type": "application/json",
//         authorization: token,
//         lan: "en",
//       },
//       body: JSON.stringify({ data: { country: "India", userId: userId, bankData: [{ IFSC_Code: "56TYU789IOYD", Account_Number: "B1QAA456789090909" }], userType: 1 } }),
//     };
//     let status = undefined;
//     fetch(`${API_HOST}/bank`, data)
//       // fetch(`${API_HOST}/paymentGateway?limit=20&skip=0&status=1`, data)
//       .then((res) => {
//         status = res.status;
//         return res.json();
//       })
//       .then((responseObj) => {
//         return resolve({ status, data: responseObj });
//       })
//       .catch((err) => {
//         return reject(err);
//       });
//   });
// };

export const getEstimatedCoins = (token, userId, coin, userType, baseCurrency) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lan: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/walletEarning/withdrawAmt?userId=${userId}&coin=${coin}&userType=${userType}&baseCurrency=${baseCurrency}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const buyCoinPlan = (token, obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_HOST}/coinPlans`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const EditPost = (obj, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(obj),
    };
    console.log("sending data", data);
    let status = undefined;
    fetch(`${API_HOST}/userPost`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const withdrawCoins = (coin, realMoney, token, bankId, currency, userId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lan: "en",
      },
      body: JSON.stringify({
        coins: parseInt(coin),
        amount: realMoney.toString(),
        pgId: "5e85e275f5b3207eaa1830fc",
        bankId: bankId,
        currency: currency,
        userType: "user",
        autoPayout: true,
        userId: userId,
        pgName: "PAYPAL",
        notes: "no notes",
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/withdraw/coin`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getReportPostReasons = (token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/reportPostReasons`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const sumbitPostReport = (token, targetPostId, reason, message) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ targetPostId: targetPostId, reason: reason, message: message }),
    };
    let status = undefined;
    fetch(`${API_HOST}/reportPost`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const addNewPost = (token, typeFlag, description, url) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({
        typeFlag: typeFlag,
        description: description,
        url: url,
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/userPost`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const deletePost = (token, postId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ postId: postId }),
    };
    let status = undefined;
    fetch(`${API_HOST}/userPost`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const EmailLogin = (obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: "Basic M2VtYmVkMDA3OjNlbWJlZDAwNw==",
        lang: "en",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(API_HOST + "/login", data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => reject(err));
  });
};

export const findById = (token, findMateId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ findMateId: findMateId }),
    };
    let status = undefined;
    fetch(`${API_HOST}/searchFriend`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const sendRequestToFriendById = (token, findMateId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ targetUserId: findMateId }),
    };
    console.log("body is", data);
    let status = undefined;
    fetch(`${API_HOST}/friendRequest`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const sendRequestToFriendByName = (token, fName) => {
  console.log("fName", fName);
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ firstName: fName }),
    };
    let status = undefined;
    fetch(`${API_HOST}/FriendsByName`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const acceptOrRejectFriendRequest = (token, findMateId, statusCode) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({
        targetUserId: findMateId,
        statusCode: statusCode,
      }),
    };
    let status = undefined;
    fetch(`${API_HOST}/friendRequestResponse`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Email-ID Verification Module
export const emailVerfication = (data) => {
  return new Promise((resolve, reject) => {
    verifyemail(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Post User Profile Module
export const profileVerification = (data) => {
  return new Promise((resolve, reject) => {
    verifyprofile(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get User Preferences Module
export const getPreference = (data) => {
  return new Promise((resolve, reject) => {
    preference(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Update User Preferences Module
export const UserdataPrefence = (data) => {
  return new Promise((resolve, reject) => {
    UserdataPrefences(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get User Profile Module
export const UserProfileData = (data) => {
  return new Promise((resolve, reject) => {
    UserProfile(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// User Profile Module
export const UserProfileNewData = (data) => {
  return new Promise((resolve, reject) => {
    UserNewData(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Phone Exits Checking Module
export const PhoneExistVerification = (data) => {
  return new Promise((resolve, reject) => {
    PhoneExistVerify(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Facebook login Module
export const Userfacebooklogin = (data) => {
  return new Promise((resolve, reject) => {
    facebooklogin(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Delete User Profile Module
export const UserProfiledelete = (data) => {
  return new Promise((resolve, reject) => {
    Profiledelete(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// SearchResult ( Nearby People ) Module
export const NearbyPeople = (data) => {
  return new Promise((resolve, reject) => {
    SearchUser(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// FriendRequest Module
export const Pendingfriendreq = (data) => {
  return new Promise((resolve, reject) => {
    friendreq(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// FriendList Module
export const FriendList = (data) => {
  return new Promise((resolve, reject) => {
    friendlist(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Past Dates Module
export const PastDates = (data) => {
  return new Promise((resolve, reject) => {
    pastdatelist(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetAllFriends = (data) => {
  return new Promise((resolve, reject) => {
    getAllFriends(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetAllFriendsRequest = (data) => {
  return new Promise((resolve, reject) => {
    getAllFriendsReq(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Pending Dates Module
export const PendingDates = (data) => {
  return new Promise((resolve, reject) => {
    pendingdatelist(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// friendRequestResponse Module
export const Friendrequesrresponce = (data) => {
  return new Promise((resolve, reject) => {
    friendreqres(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Report User Module
export const ReportUser = (data) => {
  return new Promise((resolve, reject) => {
    reportuser(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        console.log("datadata", err.response.status);
        return reject(err.response);
      });
  });
};

// To Get OtherUser Profile Module
export const OtherUserProfile = (data) => {
  return new Promise((resolve, reject) => {
    Otheruserprofile(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// My UserProfile Liked by OtherUser Module
export const Profilelikedby = (data) => {
  return new Promise((resolve, reject) => {
    likeby(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Online User Module
export const UserOnline = (data) => {
  return new Promise((resolve, reject) => {
    onlineuser(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// SuperLiked By Module
export const Superlikedby = (data) => {
  return new Promise((resolve, reject) => {
    superlikeby(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Recent Visitor Module
export const RecentVisitors = (data) => {
  return new Promise((resolve, reject) => {
    recentvisitor(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// My Likes Module
export const MyLikes = (data) => {
  return new Promise((resolve, reject) => {
    mylikes(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// My SuperLiked Module
export const MySuperLike = (data) => {
  return new Promise((resolve, reject) => {
    mysuperlike(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// thumbs up me
export const ThumbsUpMe = (data) => {
  return new Promise((resolve, reject) => {
    thumbsUpMe(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const IThumbsUpped = (data) => {
  return new Promise((resolve, reject) => {
    iThumbsUpped(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const History = (data) => {
  return new Promise((resolve, reject) => {
    history(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Passed by User Module
export const PassedUser = (data) => {
  return new Promise((resolve, reject) => {
    passedby(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Like a Nearby People Module
export const LikeNearPeople = (data) => {
  return new Promise((resolve, reject) => {
    likepeople(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const _searchUserByPagination = (start, end) => {
  return new Promise((resolve, reject) => {
    searchUserByPagination(start, end)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// SuperLike a Nearby People Module
export const SuperLikeNearPeople = (data) => {
  return new Promise((resolve, reject) => {
    superlikepeople(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Buy New Plan Module -> Subscription get
export const BuyNewPlan = (data) => {
  return new Promise((resolve, reject) => {
    newplan(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const CurrentCoinBalance = (userId, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    // if (userId && token) {
    fetch(`${API_HOST}/wallet?userId=${userId}&userType=user`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
    // }
  });
};

export const SuperLikeUser = (object, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lang: "en",
        authorization: token,
      },
      body: JSON.stringify(object),
    };
    let status = undefined;
    fetch(`${API_HOST}/supperLike`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const buyCoinsAPI = (userId, authorizationId, amount, coinsPurchase) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lang: "en",
      },
      body: JSON.stringify({
        userId: userId,
        authorizationId: authorizationId,
        amount: amount,
        coinsPurchase: coinsPurchase,
      }),
    };
    fetch(`${API_HOST}/planPayment`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GiveThumbsUp = (token, userId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lang: "en",
        authorization: token,
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/profileLike?userId=${userId}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const buySubscriptionPlanAPI = (userId, authorizationId, amount, planId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lang: "en",
      },
      body: JSON.stringify({
        userId: userId,
        authorizationId: authorizationId,
        amount: amount,
        planId: planId,
      }),
    };
    fetch(`${API_HOST}/payment/plan`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetEarningWalletTransaction = (walletId, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    fetch(`${API_HOST}/walletEarningTransaction?walletEarningId=${walletId}&fetchSize=20`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetEarningWalletTransactionWithTxn = (walletId, token, type, pageState, boolean) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    let url;
    if (boolean) {
      url = `${API_HOST}/walletEarningTransaction?walletEarningId=${walletId}&fetchSize=20&txnType=${type}&pageState=${pageState}`;
    } else {
      url = `${API_HOST}/walletEarningTransaction?walletEarningId=${walletId}&fetchSize=20&txnType=${type}`;
    }
    fetch(url, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const walletEarning = (token, userId, userType, baseCurrency) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    fetch(`${API_HOST}/walletEarning/withdrawAmt?userId=${userId}&userType=${userType}&baseCurrency=${baseCurrency}`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getWalletData_without_txnType = (walletId, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    fetch(`${API_HOST}/walletTransaction?walletId=${walletId}&fetchSize=20`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetWalletData = (walletId, token, type, pageState, boolean) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lan: "en",
        authorization: token,
      },
    };
    let url;
    if (boolean) {
      url = `${API_HOST}/walletTransaction?walletId=${walletId}&fetchSize=20&txnType=${type}&pageState=${pageState}`;
    } else {
      url = `${API_HOST}/walletTransaction?walletId=${walletId}&fetchSize=20&txnType=${type}`;
    }

    fetch(url, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const InstagramLogin = (token, instagramId, instagramName, instaGramToken) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "FETCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        lang: "en",
        authorization: token,
      },
      body: JSON.stringify({
        data: [
          {
            instagramId: instagramId,
            instagramName: instagramName,
            instaGramToken: instaGramToken,
          },
        ],
      }),
    };
    fetch(`${API_HOST}/instagramId`, data)
      .then((res) => {
        return resolve(res.json());
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Buy New Coins Module
export const BuyNewCoins = (data) => {
  return new Promise((resolve, reject) => {
    newcoins(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Coin Spent Histroy Module
export const CoinSpentHistory = (data) => {
  return new Promise((resolve, reject) => {
    coinhistory(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// My UnLike User Module
export const MyDisLikeUser = (data) => {
  return new Promise((resolve, reject) => {
    myunlikeuser(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To Search a Friend by Seeme ID Module
export const SearchSeemeFriend = (data) => {
  return new Promise((resolve, reject) => {
    searchseemefriend(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To Add new Friend Module
export const AddnewFriend = (data) => {
  return new Promise((resolve, reject) => {
    addfriend(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To update a Findmate ID of User
export const UpdateSeemeID = (data) => {
  return new Promise((resolve, reject) => {
    updateseemeid(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To Get a Findmate ID of User
export const GetSeemeID = (data) => {
  return new Promise((resolve, reject) => {
    getseemeid(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Search Prefernce // Filter User
export const searchprefenceuser = (data) => {
  return new Promise((resolve, reject) => {
    searchprefence(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// Search Prefernce Post // Filter User
export const Searchpostprefenceuser = (data) => {
  return new Promise((resolve, reject) => {
    searchprefencepost(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To Get Reason to Block a User
export const ReasonblockUser = (data) => {
  return new Promise((resolve, reject) => {
    Blockuserreason(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const matchFound = (data) => {
  return new Promise((resolve, reject) => {
    MatchFound(data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const GetAllChats = (pgNo) => {
  return new Promise((resolve, reject) => {
    getAllChats(pgNo)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const MessageWithoutMatch = (senderId, payload, type, id, userImage, name) => {
  return new Promise((resolve, reject) => {
    messageWithoutMatch(senderId, payload, type, id, userImage, name)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const GetAllChatsForSingleUser = (chatId, timeStamp, pageSize) => {
  return new Promise((resolve, reject) => {
    getAllChatsForSingleUser(chatId, timeStamp, pageSize)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

// To Block a User
export const BlockUser = (targetUserId) => {
  return new Promise((resolve, reject) => {
    blockUser(targetUserId)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err.response);
      });
  });
};

export const CoinChat = (obj) => {
  return new Promise((resolve, reject) => {
    coinChat(obj)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const DeleteSelectedMessages = (messageIds) => {
  return new Promise((resolve, reject) => {
    deleteSelectedMessages(messageIds)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const unmatchuser = (targetUserId) => {
  return new Promise((resolve, reject) => {
    unMatchUser(targetUserId)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const deletechatTotally = (chatId) => {
  return new Promise((resolve, reject) => {
    deleteChatTotally(chatId)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetActivePlan = () => {
  return new Promise((resolve, reject) => {
    getActivePlan()
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const ActivateBoost = () => {
  return new Promise((resolve, reject) => {
    activateBoost()
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const GetBoostDetails = () => {
  return new Promise((resolve, reject) => {
    getBoostDetails()
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

let commonFetch = (API, data, resolve, reject) => {
  let status = undefined;
  return fetch(API, data)
    .then((res) => {
      status = res.status;
      return res.json();
    })
    .then((responseObj) => {
      return resolve({ status, data: responseObj });
    })
    .catch((err) => {
      return reject(err);
    });
};

export const getIPAddress = () => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    return commonFetch(`https://ipapi.co/json`, data, resolve, reject);
  });
};

export const setLocation = (object, token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(object),
    };
    return commonFetch(`${API_HOST}/location`, data, resolve, reject);
  });
};

// export const getLocation = (token) => {
//   return new Promise((resolve, reject) => {
//     let data = {
//       method: "GET",
//       headers: {
//         Accept: "application/json",
//         "Content-Type": "application/json",
//         authorization: token,
//         lang: "en",
//       },
//     };
//     return commonFetch(`${API_HOST}/location`, data, resolve, reject);
//   });
// };

export const getGifs = (searchTerm) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(
      `https://api.giphy.com/v1/stickers/search?api_key=YVjl9SnQVtyU6Ck03vDoLlTSp8aNqYjX&q=${searchTerm}&limit=25&offset=0&rating=G&lang=en`,
      data
    )
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const sendCoins = (token, obj) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify(obj),
    };
    let status = undefined;
    fetch(`${API_HOST}/Transfer`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getRandomGifs = () => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(`https://api.giphy.com/v1/stickers/trending?api_key=YVjl9SnQVtyU6Ck03vDoLlTSp8aNqYjX&limit=25&offset=0&rating=G&lang=en`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Get ReWind Back User Details
export const GetRewindDetails = () => {
  return new Promise((resolve, reject) => {
    ReWindUser()
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getCoinConfig = (token) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
    };
    let status = undefined;
    fetch(`${API_HOST}/coinConfig`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const ReportWithReason = (type, textToSend, token) => {
  let _type = null;
  switch (type) {
    case 1:
      _type = "/reportAnIssue";
      break;
    case 2:
      _type = "/makeASuggestion";
      break;
    case 3:
      _type = "/askAQuestion";
      break;
    default:
      _type = null;
  }
  console.log("ReportWithReason API", _type);
  return new Promise((resolve, reject) => {
    let data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ description: textToSend }),
    };
    let status = undefined;
    fetch(`${API_HOST}/${_type}`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Patch -> Unblock User
export const unblockuser = (token, targetUserId) => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: token,
        lang: "en",
      },
      body: JSON.stringify({ targetUserId: targetUserId }),
    };
    let status = undefined;
    fetch(`${API_HOST}/unBlock`, data)
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const instagramLogin = () => {
  return new Promise((resolve, reject) => {
    let data = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    let status = undefined;
    fetch(
      `https://api.instagram.com/oauth/authorize
  ?client_id=585069925671821
  &redirect_uri=https://socialsizzle.herokuapp.com/auth/
  &scope=user_profile,user_media
  &response_type=code`,
      data
    )
      .then((res) => {
        status = res.status;
        return res.json();
      })
      .then((responseObj) => {
        return resolve({ status, data: responseObj });
      })
      .catch((err) => {
        return reject(err);
      });
  });
};
