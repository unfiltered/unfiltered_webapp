import { getCookie } from "../../lib/session";

export const redirectIfAuthenticated = route => {
  let Userauth = getCookie("token");

  if (!route) {
    console.log("PLZ PASS ROUTE PROPS");
    return;
  }

  if (route && Userauth) {
    route.push("/app/");
    return true;
  }
};

export const redirectIfNotAuthenticated = route => {
  let Userauth = getCookie("token");

  if (!route) {
    console.log("PLZ PASS ROUTE PROPS");
    return;
  }

  if (!route && !Userauth) {
    route.push("/");
    return true;
  }
};
