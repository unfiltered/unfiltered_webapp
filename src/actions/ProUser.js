import { CHECK_IF_PRO_USER } from "./index";

export const checkIfProUser = data => {
  return {
    type: CHECK_IF_PRO_USER,
    data: data
  };
};
