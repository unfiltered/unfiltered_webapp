export const LOCALE_SET = "LOCALE_SET";
export const MOBILE_LOGIN_ACTION = "MOBILE_LOGIN_ACTION";
export const CAROUSEL_INDEX = "CAROUSEL_INDEX";
export const ADD_FRIEND = "ADD_FRIEND";
export const USER_PROFILE_ACTION = "USER_PROFILE_ACTION";
export const SELECTED_PROFILE_ACTION = "SELECTED_PROFILE_ACTION";
export const IMAGE_CHANGE_ON_PROFILE_IMG_UPDATE = "IMAGE_CHANGE_ON_PROFILE_IMG_UPDATE";
export const SET_OTP = "SET_OTP";

// filters
export const FILTER_BY_AGE_OBJ = "FILTER_BY_AGE_OBJ";
export const FILTER_BY_HEIGHT_OBJ = "FILTER_BY_HEIGHT_OBJ";
export const FILTER_BY_DISTANCE_OBJ = "FILTER_BY_DISTANCE_OBJ";
export const SELETED_FILTER = "SELETED_FILTER";

// coin
export const COIN_CHAT = "COIN_CHAT";
export const STORE_COINS = "STORE_COINS";
export const SET_CURRENCY_SYMBOL = "SET_CURRENCY_SYMBOL";

// chat
export const UPDATE_ACKNOWLEDGEMENT = "UPDATE_ACKNOWLEDGEMENT";
export const USER_SELECTED_TO_CHAT = "USER_SELECTED_TO_CHAT";
export const LEFT_SIDE_MESSAGE_LOGS = "LEFT_SIDE_MESSAGE_LOGS";
export const SELECTED_USER_ALL_MESSAGES = "SELECTED_USER_ALL_MESSAGES";
export const APPEND_MESSAGE_EXISTING_CHAT = "APPEND_MESSAGE_EXISTING_CHAT";
export const CHAT_STARTED = "CHAT_STARTED";
export const CHAT_ENDED = "CHAT_ENDED";
export const UPLOAD_IMAGE = "UPLOAD_IMAGE";
export const DELETE_ALL_MESSAGES_FOR_SINGLE_USER = "DELETE_ALL_MESSAGES_FOR_SINGLE_USER";
export const DOUBLE_TICK = "DOUBLE_TICK";
export const SINGLE_TICK = "SINGLE_TICK";
export const ADD_CHAT_IF_NOT_EXISTING = "ADD_CHAT_IF_NOT_EXISTING";
export const ADD_COINS_TO_EXISTING_COINS = "ADD_COINS_TO_EXISTING_COINS";
export const DISMISS_COIN_CHAT_TOAST = "DISMISS_COIN_CHAT_TOAST";
export const SHOW_COIN_CHAT_TOAST = "SHOW_COIN_CHAT_TOAST";
export const JUST_VIEW_PROFILE_ACTIVE = "JUST_VIEW_PROFILE_ACTIVE";
export const JUST_VIEW_PROFILE_REMOVE = "JUST_VIEW_PROFILE_REMOVE";
export const STORE_UNREAD_CHAT_COUNT = "STORE_UNREAD_CHAT_COUNT";
export const STORE_UNREAD_CHAT_ARRAY = "STORE_UNREAD_CHAT_ARRAY";
export const UPDATE_UNREAD_CHAT_ARRAY = "UPDATE_UNREAD_CHAT_ARRAY";
export const CHAT_DRAWER = "CHAT_DRAWER";

// location
export const SET_DEFAULT_LOCATION = "SET_DEFAULT_LOCATION";
export const SET_LATITUDE = "SET_LATITUDE";
export const SET_LONGITUDE = "SET_LONGITUDE";
export const ADD_NEW_LOCATION_TO_SEARCH = "ADD_NEW_LOCATION_TO_SEARCH";
export const CHECK_IF_PRO_USER = "CHECK_IF_PRO_USER";
export const DISCOVER_PEOPLE_LIST = "DISCOVER_PEOPLE_LIST";
export const BOOST_ACTIVED = "BOOST_ACTIVED";
export const BOOST_DETAILS_ON_REFRESH = "BOOST_DETAILS_ON_REFRESH";
export const MATCH_FOUND = "MATCH_FOUND";
export const BOOST_ACTIVED_SESSION_INCOMING_DATA = "BOOST_ACTIVED_SESSION_INCOMING_DATA";

// AGE AND DISTANCE
export const AGE_DETAILS = "AGE_DETAILS";
export const DETAILS_DETAILS = "DETAILS_DETAILS";

// COIN CONFIG

export const GET_COIN_CONFIG = "GET_COIN_CONFIG";
export const GET_PRO_USER_PLAN = "GET_PRO_USER_PLAN";
export const ON_SEARCH_SEND_LOCATION = "ON_SEARCH_SEND_LOCATION";
export const MQTT_DATA = "MQTT_DATA";
export const MQTT_MSG_UPDATE = "MQTT_MSG_UPDATE";
export const WEBRTC_CALL_INIT = "WEBRTC_CALL_INIT";
export const SET_URL_AFTER_CROPPING = "SET_URL_AFTER_CROPPING";
export const SET_PROFILE_OBJECT = "SET_PROFILE_OBJECT";
export const ROUTE_AFTER_PAYMENT = "ROUTE_AFTER_PAYMENT";

// POSTS
export const M_STORE_ALL_POSTS = "M_STORE_ALL_POSTS";
export const M_ADD_NEW_POST = "M_ADD_NEW_POST";
export const M_DELETE_POST = "M_DELETE_POST";
export const M_LIKE_DISLIKE = "M_LIKE_DISLIKE";
export const M_ADD_COMMENT = "M_ADD_COMMENT";
export const M_POST_UPLOADING_STATUS = "M_POST_UPLOADING_STATUS";

// FRIEND REQUESTS
export const FRIEND_REQUEST_COUNT = "FRIEND_REQUEST_COUNT";
