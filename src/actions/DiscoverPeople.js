import { DISCOVER_PEOPLE_LIST } from ".";

export const discoverPeople = peopleList => {
  return {
    type: DISCOVER_PEOPLE_LIST,
    data: peopleList
  };
};
