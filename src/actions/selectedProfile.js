import { SELECTED_PROFILE_ACTION } from "./index";

export const SELECTED_PROFILE_ACTION_FUN = (type, value) => {
  return {
    type: SELECTED_PROFILE_ACTION,
    data: value,
    datatype: type
  };
};
