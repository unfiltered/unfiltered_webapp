import { GET_COIN_CONFIG, GET_PRO_USER_PLAN, ROUTE_AFTER_PAYMENT } from "./index";

export const getCoinConfig = (data) => {
  return {
    type: GET_COIN_CONFIG,
    data: data,
  };
};

export const getProUserPlans = (data) => {
  return {
    type: GET_PRO_USER_PLAN,
    data: data,
  };
};

export const indirectRouting = (boolean) => {
  return {
    type: ROUTE_AFTER_PAYMENT,
    data: boolean,
  };
}
