import { M_ADD_NEW_POST, M_DELETE_POST, M_STORE_ALL_POSTS, M_LIKE_DISLIKE, M_ADD_COMMENT, M_POST_UPLOADING_STATUS } from "./index";

export const storeAllPosts = (data) => {
  return {
    type: M_STORE_ALL_POSTS,
    data: data,
  };
};

export const deleteSinglePost = (data) => {
  return {
    type: M_DELETE_POST,
    data: data,
  };
};

export const addNewPost = (data) => {
  return {
    type: M_ADD_NEW_POST,
    data: data,
  };
};

export const likeDislikePost = (data) => {
  return {
    type: M_LIKE_DISLIKE,
    data: data,
  };
};

export const addComment = (data) => {
  return {
    type: M_ADD_COMMENT,
    data: data,
  };
};

export const uploadingPostStatus = (text, boolean) => {
  return {
    type: M_POST_UPLOADING_STATUS,
    text: text,
    boolean: boolean,
  };
};
