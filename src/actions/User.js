import { SET_PROFILE_OBJECT, MOBILE_LOGIN_ACTION, SET_DEFAULT_LOCATION, SET_LONGITUDE, SET_LATITUDE, SET_OTP, SET_URL_AFTER_CROPPING, ADD_NEW_LOCATION_TO_SEARCH, CAROUSEL_INDEX } from "./index";

export const MOBILE_ACTION_FUNC = (type, value) => {
  return {
    type: MOBILE_LOGIN_ACTION,
    data: value,
    datatype: type,
  };
};

export const saveCarouselIndex = (index) => {
  return {
    type: CAROUSEL_INDEX,
    data: index,
  };
};

export const setDefaultLocation = (location) => {
  return {
    type: SET_DEFAULT_LOCATION,
    data: location,
  };
};

export const setLatitude = (lan) => {
  return {
    type: SET_LATITUDE,
    data: lan,
  };
};

export const setLongiitutde = (long) => {
  return {
    type: SET_LONGITUDE,
    data: long,
  };
};

export const OTP = (otp) => {
  return {
    type: SET_OTP,
    data: otp,
  };
};

export const addNewLocationToSearch = (newLocation) => {
  return {
    type: ADD_NEW_LOCATION_TO_SEARCH,
    data: newLocation,
  };
};

export const __setUrlAfterCropping = (data) => {
  return {
    type: SET_URL_AFTER_CROPPING,
    data: data,
  };
};

export const __setFileObjectAfterCropped = (data) => {
  return {
    type: SET_PROFILE_OBJECT,
    data: data
  }
}
