import {
  USER_SELECTED_TO_CHAT,
  SELECTED_USER_ALL_MESSAGES,
  CHAT_STARTED,
  CHAT_ENDED,
  UPLOAD_IMAGE,
  DOUBLE_TICK,
  SINGLE_TICK,
  ON_SEARCH_SEND_LOCATION,
  JUST_VIEW_PROFILE_ACTIVE,
  JUST_VIEW_PROFILE_REMOVE,
} from "./index";

export const userSelectedToChat = (selectedUser) => {
  return {
    type: USER_SELECTED_TO_CHAT,
    data: selectedUser,
  };
};

export const justViewProfileActive = () => {
  return {
    type: JUST_VIEW_PROFILE_ACTIVE,
  };
};

export const justViewProfileRemove = () => {
  return {
    type: JUST_VIEW_PROFILE_REMOVE,
  };
};

export const allMessagesForSelectedUser = (allMessages) => {
  console.log("------------------ storeMessages() called ------------------", allMessages.length);
  return {
    type: SELECTED_USER_ALL_MESSAGES,
    data: allMessages,
  };
};

export const chatSessionStarted = () => {
  return {
    type: CHAT_STARTED,
  };
};

export const chatSessionEnded = () => {
  return {
    type: CHAT_ENDED,
  };
};

export const uploadImage = (image) => {
  return {
    type: UPLOAD_IMAGE,
    data: image,
  };
};

export const doubleTickAcknowledgement = (data) => {
  return {
    type: DOUBLE_TICK,
    data: data,
  };
};

export const singleTickAcknowledgement = (data) => {
  return {
    type: SINGLE_TICK,
    data: data,
  };
};

export const onSearchSendLocation = (data) => {
  return {
    type: ON_SEARCH_SEND_LOCATION,
    data: data,
  };
};
