import {
  APPEND_MESSAGE_EXISTING_CHAT,
  COIN_CHAT,
  DELETE_ALL_MESSAGES_FOR_SINGLE_USER,
  STORE_UNREAD_CHAT_COUNT,
  SHOW_COIN_CHAT_TOAST,
  DISMISS_COIN_CHAT_TOAST,
  STORE_UNREAD_CHAT_ARRAY,
  UPDATE_UNREAD_CHAT_ARRAY,
  CHAT_DRAWER,
} from "./index";

export const updateExistingUnreadChatArray = (chatId) => {
  return {
    type: UPDATE_UNREAD_CHAT_ARRAY,
    data: chatId,
  };
};

export const chatDrawer = (boolean) => {
  return {
    type: CHAT_DRAWER,
    data: boolean,
  };
};

export const storeUnreadChatCount = (count) => {
  return {
    type: STORE_UNREAD_CHAT_COUNT,
    data: count,
  };
};

export const addNewChatToExistingObject = (data) => {
  return {
    type: APPEND_MESSAGE_EXISTING_CHAT,
    data: data,
  };
};

export const storeUnreadChatArray = (array) => {
  return {
    type: STORE_UNREAD_CHAT_ARRAY,
    data: array,
  };
};

export const coinChat = (data) => {
  return {
    type: COIN_CHAT,
    data: data,
  };
};

export const deleteAllMessagesForSingleUser = (chatId) => {
  return {
    type: DELETE_ALL_MESSAGES_FOR_SINGLE_USER,
    data: chatId,
  };
};

export const showUpCoinChatToast = () => {
  return {
    type: SHOW_COIN_CHAT_TOAST,
  };
};

export const dismissCoinChatToast = () => {
  return {
    type: DISMISS_COIN_CHAT_TOAST,
  };
};
