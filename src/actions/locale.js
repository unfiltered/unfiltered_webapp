import { LOCALE_SET } from "./index";

export const localeset = lang => ({
  type: LOCALE_SET,
  lang
});

export const setLocale = lang => dispatch => {
  localStorage.alhubLang = lang;
  dispatch(localeset(lang));
};
