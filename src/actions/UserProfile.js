import {
  USER_PROFILE_ACTION,
  LEFT_SIDE_MESSAGE_LOGS,
  FILTER_BY_AGE_OBJ,
  FILTER_BY_HEIGHT_OBJ,
  FILTER_BY_DISTANCE_OBJ,
  STORE_COINS,
  SET_CURRENCY_SYMBOL,
  MATCH_FOUND,
  AGE_DETAILS,
  DETAILS_DETAILS,
  ADD_CHAT_IF_NOT_EXISTING,
  IMAGE_CHANGE_ON_PROFILE_IMG_UPDATE,
  ADD_COINS_TO_EXISTING_COINS,
  UPDATE_ACKNOWLEDGEMENT,
} from "./index";

export const updateAcknowledgement = (msgId, status) => {
  return {
    type: UPDATE_ACKNOWLEDGEMENT,
    data: msgId,
    status: status,
  };
};

export const USER_PROFILE_ACTION_FUN = (type, value) => {
  return {
    type: USER_PROFILE_ACTION,
    data: value,
    datatype: type,
  };
};

export const UpdateProfilePictureOnProfileImageUpdate = (data) => {
  return {
    type: IMAGE_CHANGE_ON_PROFILE_IMG_UPDATE,
    data: data,
  };
};

export const AgeDetails = (data) => {
  return {
    type: AGE_DETAILS,
    data: data,
  };
};

export const DistanceDetails = (data) => {
  return {
    type: DETAILS_DETAILS,
    data: data,
  };
};

export const leftSideChatLogs = (listOfChats) => {
  return {
    type: LEFT_SIDE_MESSAGE_LOGS,
    data: listOfChats,
  };
};

export const addChatIfNotExisting = (chatObj) => {
  return {
    type: ADD_CHAT_IF_NOT_EXISTING,
    data: chatObj,
  };
};

export const filterByAgeObjectVal = (min, max) => {
  return {
    type: FILTER_BY_AGE_OBJ,
    min: min,
    max: max,
  };
};

export const filterByHeightObjectVal = (min, max) => {
  return {
    type: FILTER_BY_HEIGHT_OBJ,
    min: min,
    max: max,
  };
};

export const filterByDistanceObjectVal = (min, max) => {
  return {
    type: FILTER_BY_DISTANCE_OBJ,
    min: min,
    max: max,
  };
};

export const storeCoins = (coin) => {
  return {
    type: STORE_COINS,
    data: coin,
  };
};

export const addCoinsToExistingCoins = (coin) => {
  return {
    type: ADD_COINS_TO_EXISTING_COINS,
    data: coin,
  };
};

export const setCurrencySymbol = (currencySymbol) => {
  return {
    type: SET_CURRENCY_SYMBOL,
    data: currencySymbol,
  };
};

export const matchFound = (data) => {
  return {
    type: MATCH_FOUND,
    data: data,
  };
};
