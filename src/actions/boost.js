import { BOOST_ACTIVED, BOOST_DETAILS_ON_REFRESH, BOOST_ACTIVED_SESSION_INCOMING_DATA } from "./index";

export const boostActivated = data => {
  return {
    type: BOOST_ACTIVED,
    data: data
  };
};

export const getBoostDetailsOnWindowRefresh = data => {
  return {
    type: BOOST_DETAILS_ON_REFRESH,
    data: data
  };
};

export const boostActivatedSessionIncomingData = data => {
  return {
    type: BOOST_ACTIVED_SESSION_INCOMING_DATA,
    data: data
  };
};
