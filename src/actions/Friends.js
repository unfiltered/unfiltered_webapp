import { FRIEND_REQUEST_COUNT } from "./index";

export const FRIEND_REQUEST_COUNT_FUNC = (data) => {
  return {
    type: FRIEND_REQUEST_COUNT,
    data: data,
  };
};
