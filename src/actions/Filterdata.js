import { SELETED_FILTER } from "./index";

export const SELETED_FILTER_ACTION = (type, value) => {
  return {
    type: SELETED_FILTER,
    data: value,
    datatype: type
  };
};
