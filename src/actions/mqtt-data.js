import { MQTT_DATA } from "./index";

export const __mqttData = data => {
  return {
    type: MQTT_DATA,
    data: data
  };
};
