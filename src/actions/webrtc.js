import { MQTT_MSG_UPDATE, WEBRTC_CALL_INIT } from ".";

export const __updateMqttMessage = data => {
    return {
        type: MQTT_MSG_UPDATE,
        data: data
    };
};

export const __callInit = data => {
    console.log("Inside ", data);
    return {
        type: WEBRTC_CALL_INIT,
        data: data
    };
};