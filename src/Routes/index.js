import React, { Component } from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import WebIndex from "../View/LandingPage/index";
import MobileLandingPage from "../View/MobileHomePage/index";
import Messages from "../message";
import TermsAndConditons from "../View/WebHomePage/Components/StaticPages/TermsAndConditons";
import PrivacyPolicy from "../View/WebHomePage/Components/StaticPages/PrivacyPolicy";
import CookiePolicy from "../View/WebHomePage/Components/StaticPages/CookiePolicy";
import { IntlProvider } from "react-intl";
import { geolocated } from "react-geolocated";
import { connect } from "react-redux";
import Home from "../View/WebHomePage/Components/Maps/Home";
// import RTCMesh from "../View/WebHomePage/Components/WebRtc/RTCMesh";
// import { ICE_SERVER_URLS } from "../View/WebHomePage/Components/WebRtc/functions/constants";
import AppRtc from "../View/WebHomePage/Components/Apprtc/apprtc";
import { getCookie, setCookie } from "../lib/session";
import { messaging } from "../init-fcm";
import MqttHOC from "../lib/MqttHOC/mqttHOC";
import { getIPAddress } from "../controller/auth/verification";
import FAQ from "../View/WebHomePage/Components/StaticPages/FAQ";

// const constraints = {
//   video: true,
//   audio: {
//     sampleSize: 8,
//     echoCancellation: true,
//   },
// };

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      return getCookie("uid") && getCookie("token") ? (
        <MqttHOC store={rest.store}>
          <Component {...props} {...rest} />
        </MqttHOC>
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location },
          }}
        />
      );
    }}
  />
);

class Index extends Component {
  async componentDidMount() {
    navigator.permissions &&
      navigator.permissions.query({ name: "geolocation" }).then(function (PermissionStatus) {
        if (PermissionStatus.state == "granted") {
          setCookie("permission", 1);
        } else if (PermissionStatus.state == "prompt") {
          setCookie("permission", 2);
        } else {
          setCookie("permission", 3);
        }
      });
    messaging
      .requestPermission()
      .then(async function () {
        const token = await messaging.getToken();
        console.log("fcmToken", token);
        setCookie("fcmToken", token);
        // localStorage.setItem("fcmToken", token);
      })
      .catch(function (err) {
        console.log("Unable to get permission to notify.", err);
      });
    /** geolocation */
    try {
      let baseAddr = getCookie("baseAddress");
      let ipDetails = await getIPAddress();
      console.log("ipDetails", ipDetails);
      console.log("zzzz working ?", baseAddr);
      setCookie("timezone", ipDetails.data.timezone);
      setCookie("ipAddress", ipDetails.data.ip.toString());
      if (baseAddr === undefined || baseAddr == null) {
        const location = window.navigator && window.navigator.geolocation;
        // console.log("location", location);
        if (location) {
          location.getCurrentPosition(
            (position) => {
              console.log("position", position.coords.latitude, position.coords.longitude);
              setCookie("lat", position.coords.latitude);
              setCookie("lng", position.coords.longitude);
              this.setState(
                {
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
                },
                () => {
                  let google = window.google;
                  if (google) {
                    let geocoder = new google.maps.Geocoder();
                    let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    // console.log("latlng", latlng);
                    geocoder.geocode({ latLng: latlng }, (results, status) => {
                      console.log("results, status", results, status);
                      if (status === google.maps.GeocoderStatus.OK) {
                        setCookie("location", results[0].formatted_address);
                        setCookie("UserCurrentCity", ipDetails.data.city);
                        setCookie("citylocation", ipDetails.data.city);
                        if (getCookie("selectedLocation") == undefined || getCookie("selectedLocation") == null) {
                          setCookie("selectedLocation", {
                            address: results[0].formatted_address,
                            city: ipDetails.data.city,
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                          });
                        }
                        setCookie("baseAddress", {
                          address: results[0].formatted_address,
                          city: ipDetails.data.city,
                          lat: position.coords.latitude,
                          lng: position.coords.longitude,
                        });
                      }
                    });
                  }
                }
              );
            },
            (error) => {
              console.log("lat lng error");
              this.setState({
                latitude: "err-latitude",
                longitude: "err-longitude",
              });
            }
          );
        }
      } else {
        if (baseAddr == null || baseAddr == undefined) {
          setCookie("lat", ipDetails.data.latitude);
          setCookie("lng", ipDetails.data.longitude);
          if (getCookie("selectedLocation") == undefined || getCookie("selectedLocation") == null) {
            setCookie("selectedLocation", {
              address: ipDetails.data.city,
              city: ipDetails.data.city,
              lat: ipDetails.data.latitude,
              lng: ipDetails.data.longitude,
            });
          }
          setCookie("baseAddress", {
            address: ipDetails.data.city,
            city: ipDetails.data.city,
            lat: ipDetails.data.latitude,
            lng: ipDetails.data.longitude,
          });
        }
        console.log("already geolocation data available");
      }
    } catch (e) {}
  }

  render() {
    const { lang, webrtc, UserProfile, UserSelectedToChat, selectedProfile } = this.props;
    return (
      <IntlProvider locale={lang} key={lang} messages={Messages[lang]}>
        {/* <RTCMesh
          mediaConstraints={constraints}
          iceServers={ICE_SERVER_URLS}
          URL="wss://letshare.ml/"
          // userId={userId}
          // currentUserData={
          //   this.props.UserProfile ? { ...this.props.UserProfile, userId: userId } : {}
          // }
          // opponentData={updateduserprofile && updateduserprofile || { userId: userId }}
          webrtc={webrtc}
          currentUserData={UserProfile}
          opponentData={UserSelectedToChat}
          // UserSelectedToChat={UserSelectedToChat}
          onRef={(ref) => {
            this.webRtcCon = ref;
          }}
        > */}
        <div style={window.innerWidth <= 576 ? { backgroundColor: "#161616" } : {}}>
          <AppRtc
            webrtc={webrtc}
            currentUserData={UserProfile}
            opponentData={Object.keys(UserSelectedToChat).length > 0 ? UserSelectedToChat : selectedProfile}
            dispatch={this.props.dispatch}
          >
            <Router>
              <Switch>
                <Route exact path="/" component={WebIndex} />
                <PrivateRoute path="/app" store={this.props.store} component={MobileLandingPage} />
                {/* <Route path="/app" render={(props) => <MobileLandingPage {...props} {...this.props} />} /> */}
                <Route path="/map" render={(props) => <Home {...props} {...this.props} />} />
                <Route path="/terms-and-conditions" render={(props) => <TermsAndConditons {...props} {...this.props} />} />
                <Route path="/privacy-policy" render={(props) => <PrivacyPolicy {...props} {...this.props} />} />
                <Route path="/cookie-policy" render={(props) => <CookiePolicy {...props} {...this.props} />} />
                <Route path="/faq" render={() => <FAQ />} />
                {/* </Switch> */}
              </Switch>
            </Router>
          </AppRtc>
        </div>
        {/* </RTCMesh> */}
      </IntlProvider>
    );
  }
}

function mapstateToProps(state) {
  return {
    lang: state.locale.lang,
    webrtc: state.webrtc,
    UserProfile: state.UserProfile && state.UserProfile.UserProfile,
    UserSelectedToChat: state.UserSelectedToChat && state.UserSelectedToChat.UserSelectedToChat,
    selectedProfile:
      state.selectedProfile && state.selectedProfile.selectedProfile && state.selectedProfile.selectedProfile.selectedProfile,
  };
}

export default connect(
  mapstateToProps,
  null
)(
  geolocated({
    positionOptions: { enableHighAccuracy: false },
    userDecisionTimeout: 5000,
  })(Index)
);
