import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "../Coin/Coin.scss";
import "./CoinBalance.scss";
import { BuyNewCoins, pruchasePlanFromAPI, buyCoinsAPI, buyCoinPlan } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import Skeleton from "react-loading-skeleton";
import { addCoinsToExistingCoins } from "../../../../actions/UserProfile";
import Button from "../../../../Components/Button/Button";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { Images } from "../../../../Components/Images/Images";
import { keys } from "../../../../lib/keys";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";

function TopUp(props) {
  const [coinData, setCoinData] = useState([]);
  const [plan, selectedPlan] = useState("x");
  const [ifSelectedPlan, setSelectedPlan] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [changeScreen, setChangeScreen] = useState(false);
  const [colors] = useState(["#19ACFF", "#E33022", "#9462FD"]);
  const [discount] = useState(["1%", "11%", "30%"]);
  const [isPaymentDone, setIsPaymentDone] = useState("");
  const [processing, isProcessing] = useState(false);
  const [state, setState] = useState({
    cvc: "",
    expiry: "",
    name: "",
    number: "",
    focus: "",
  });

  const handleInputFocus = (e) => {
    setState({ ...state, focus: e.target.name });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  /** acts as componentDidMount() */
  useEffect(() => {
    BuyNewCoins().then((data) => {
      setCoinData(data.data.data);
      setLoaded(true);
    });
    /** API to get wallet info */
    props.renderBgColorBasedOnLink(window.location.href);
  }, []);

  const purchasePlan = async (plan) => {
    isProcessing(true);
    let data = await pruchasePlanFromAPI(getCookie("token"), {
      cardNumber: state.number,
      expireDate: state.expiry,
      cardCode: state.cvc,
      planId: plan.planId,
      type: "0",
    });
    if (data.status === 200) {
      let obj = {
        paymentGatewayTxnId: data.data.message,
        paymentGateway: "paypal",
        currencyCode: "USD",
        amount: plan.baseCost,
        coinValue: plan.noOfCoinUnlock.Coin,
      };
      let repsonseForCoinPlans = await buyCoinPlan(getCookie("token"), obj);
      setIsPaymentDone(`Payment is successfull. ${plan.noOfCoinUnlock.Coin} has been credited.`);
      props.addCoins(parseInt(plan.noOfCoinUnlock.Coin));
      setTimeout(() => {
        isProcessing(false);
      }, 1000);
    } else {
      setIsPaymentDone(data.data.message);
      setTimeout(() => {
        isProcessing(false);
      }, 1000);
      setTimeout(() => {
        setIsPaymentDone("");
      }, 5000);
    }
  };

  const toggleScreen = () => {
    setChangeScreen(!changeScreen);
  };

  /** on selected plan, adds a border to selected plan */
  const _selectedPlan = (_plan) => {
    selectedPlan(_plan);
    setSelectedPlan(true);
  };

  const checkForValidation = () => {
    if (state.cvc.length >= 3 && state.expiry.length >= 3 && state.name.length > 2 && state.number.length >= 16) {
      return true;
    }
    return false;
  };

  return changeScreen ? (
    <div className="col-12 mainScreen px-0">
      <div className="col-12 WNavbar_setting border-bottom px-0">
        <div className="py-3 row align-items-center mx-0">
          <div className="col-6">
            <div className="d-flex align-items-center">
              <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={toggleScreen}>
                <img src={Images.backButton} width={20} height={20} alt="left-arrow" />
              </div>
              <h5 className="m-0">
                <FormattedMessage id="message.paymentMethod" />
              </h5>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 p-5">
        <div id="PaymentForm">
          <Cards cvc={state.cvc} expiry={state.expiry} focused={state.focus} name={state.name} number={state.number} />
          <form className="col-12 pt-3" onSubmit={(e) => e.preventDefault()}>
            <div className="row mx-auto card-payment-section">
              <div className="card-fill-cell">
                <div className="credit-card-fill-text">Name:</div>
                <input type="text" name="name" placeholder="John Doe" onChange={handleInputChange} onFocus={handleInputFocus} />
              </div>
              <div className="card-fill-cell">
                <div className="credit-card-fill-text">Card No:</div>
                <input
                  type="tel"
                  name="number"
                  maxLength={17}
                  placeholder="4242424242424242"
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                />
              </div>
              <div className="card-fill-cell">
                <div className="credit-card-fill-text"> CVV:</div>
                <input type="tel" name="cvc" maxLength={4} placeholder="000" onChange={handleInputChange} onFocus={handleInputFocus} />
              </div>
              <div className="card-fill-cell">
                <div className="credit-card-fill-text"> Expiry:</div>
                <input type="tel" name="expiry" maxLength={4} placeholder="0324" onChange={handleInputChange} onFocus={handleInputFocus} />
              </div>
              <button
                className={checkForValidation() ? "d_makePayment mt-3" : "d_makePayment_disabled mt-3"}
                disabled={!checkForValidation()}
                onClick={() => purchasePlan(plan)}
              >
                {processing ? "Please wait, while payment is processing" : "Make Payment"}
              </button>
              {isPaymentDone.length > 0 ? <div className="text-center w-100">{isPaymentDone}</div> : ""}
            </div>
          </form>
        </div>
      </div>
    </div>
  ) : (
    <div className="col-12 mainScreen" id="innerVideoContent">
      <Helmet>
        <meta
          name="og_title"
          property="og:title"
          content={`${keys.AppName} Online Dating, Credits, online, wallet, Credits, dating, relationship, premium plans`}
        ></meta>
        <meta charSet="utf-8" />
        <meta
          name="Description"
          content="Buy Premium Plans Select a plan from one to the below to unlock features on the app like unlimited super likes, unlimited likes, unlimited rewinds."
        ></meta>
        <meta
          name="Keywords"
          content="Use Credits to buy credits and subscription, online, wallet, Credits, dating, relationship, premium plans"
        ></meta>
        <meta
          property="og:description"
          content="Use Credits to buy credits and subscription, online, wallet, Credits, dating, relationship, premium plans"
        ></meta>
      </Helmet>
      <title>Buy Credits</title>
      <div className="row">
        <h4 className="col-12 pt-4 d_buyCoins_header">
          <FormattedMessage id="message.buyCoins" />
        </h4>
        <div className="col-12 pt-3">
          <div className="row">
            <div className="col-7 buyCoinsText mb-4">
              <FormattedMessage id="message.selectPlan" />
            </div>
            <div className="col-5 d-flex align-items-start justify-content-end">
              <Button
                text={<FormattedMessage id="message.makePayment" />}
                className={ifSelectedPlan ? "d_coinBalance_buyCoins_btn" : "d_coinBalance_buyCoins_btn_disabled"}
                handler={toggleScreen}
                disabled={!ifSelectedPlan}
              />
            </div>
          </div>
        </div>
        <div className="col-12">
          <Button
            text={<FormattedMessage id="message.recentTransactions" />}
            className="recent_transactions"
            handler={() => props.history.push("/app/top-up")}
          />
        </div>
        <div className="col-12 px-3 pt-5">
          <div className="col-12 border-bottom"></div>
        </div>
        <div className="w-100 coinCardsContainer">
          <div className="col-12 coinCardsLayout">
            <div className="row" style={{ justifyContent: "space-evenly" }}>
              {!loaded
                ? [...new Array(3)].map((k, planIndex) => (
                    <div key={planIndex} className="col-4 coinCard py-4">
                      <div className="text-center">
                        <Skeleton circle={true} width={80} height={80} />
                      </div>
                      <div className="text-center py-3 d-flex justify-content-center">
                        <div className="planName">
                          <Skeleton width={100} />
                        </div>
                      </div>

                      <div className="actualPrice">
                        <Skeleton width={100} /> <Skeleton width={50} />
                      </div>
                    </div>
                  ))
                : coinData.map((k, planIndex) => (
                    <section>
                      <div key={planIndex} className={"newCoinCard px-0"}>
                        <div style={{ background: colors[planIndex] }}>{k.planName}</div>
                        <div>Buy Credits</div>
                        <div style={{ borderTop: `2px solid ${colors[planIndex]}`, borderBottom: `2px solid ${colors[planIndex]}` }}>
                          {k.baseCurrencySymbol} {k.baseCost}
                        </div>
                        <div>One Time Payment</div>
                        <button className="my-4" onClick={() => _selectedPlan(k)} style={{ background: `${colors[planIndex]}` }}>
                          Buy Now
                        </button>
                        <div className="pb-5" style={planIndex === 0 ? { opacity: 0 } : {}}>
                          {discount[planIndex] ? `Save ${discount[planIndex]}` : ""}
                        </div>
                      </div>
                    </section>
                  ))}
              <section className="credits-info">
                <div className="placeholder__text pt-3 pb-1">Add 25 Daily Additional Likes - 1 Credit Per Day</div>
                <div className="placeholder__text py-1">Add Superlikes - 1 Credit Per Super Like</div>
                <div className="placeholder__text py-1">Boost Profile (Profile Added to Top of Search) - 2 Credits per 30 minutes</div>
                <div className="placeholder__text py-1">Test Drive and Unlock Plus Feature for 3 days - 10 Credits</div>
                <div className="placeholder__text pb-3 pt-1">*Credits must be utilized within 6 months</div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCoins: (coins) => dispatch(addCoinsToExistingCoins(coins)),
  };
};

export default withRouter(connect(null, mapDispatchToProps)(TopUp));
