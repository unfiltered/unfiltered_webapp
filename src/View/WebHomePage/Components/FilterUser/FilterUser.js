import React, { Component } from "react";
import { connect } from "react-redux";
import "./FilterUser.scss";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import MapHome from "../Maps/Home";
import { discoverPeople } from "../../../../actions/DiscoverPeople";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { setCookie } from "../../../../lib/session";
import { searchprefenceuser, Searchpostprefenceuser, NearbyPeople, setLocation } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import * as filterByValues from "../../../../actions/UserProfile";
import { FormattedMessage } from "react-intl";
import Slider, { Range } from "rc-slider";
import "rc-slider/assets/index.css";

const styles = {
  root: {
    width: 300,
  },
  slider: {
    padding: "5px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  progress: {
    color: "#e31b1b",
    margin: "25px",
    // marginTop: "10vh",
    // textAlign: "center",
  },
  formControl: {
    margin: "10px",
  },
  group: {
    margin: `10px`,
  },
  radioButtonSelect: {
    display: "flex",
    flexDirection: "row",
  },
  fontLabel: {
    fontFamily: "Product Sans",
  },
  radio: {
    "&$checked": {
      color: "#f80402",
    },
  },
  checked: {},
};

let DeafultPayload = [];

class FilterUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      valueage: 20,
      valuecity: 80,
      condition: false,
      value: { min: null, max: null },
      valueHeight: { min: null, max: null },
      valueDistance: { min: null, max: null },
      Genderid: "",
      Heightid: "",
      openMap: false,
      Ageid: "",
      HeightRangeid: "",
      DistanceRangeid: "",
      lookingForSelection: "",
      lookingForId: "",
      searchPreferencesuser: "",
      tabselectedvalue: 1,
      distancetabselectedvalue: 0,
      SelectedHeightOptions: "",
      CurrentDeafultValueid: "",
      distanceToShow: "Km",
      isHeightInFtOrCm: "", // this is unit Ft/Cm
      isDistanceInKmOrMi: "", // this if meter/miles
      selectGenderOnMount: "", // m / f / both to searh users
      linksDistance: [
        {
          id: 0,
          name: "Mi",
        },
      ],
      activeLinkdistance: 1,
      linksHeight: [
        {
          id: 0,
          name: "Ft",
        },
        {
          id: 1,
          name: "Cm",
        },
      ],
      activeLinkHeight: 1,
      locationToShow: "",
      heightValue: "Cm", // will show cm/ft,
      multiselectchecbox: [],
      multiSelectId: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFilterUser = this.handleFilterUser.bind(this);
  }

  handlemultiselect = (event) => {
    let multiselectchecbox = [...this.state.multiselectchecbox];
    if (multiselectchecbox.includes(event)) {
      let index = multiselectchecbox.indexOf(event);
      multiselectchecbox.splice(index, 1);
    } else {
      multiselectchecbox.push(event);
    }
    this.setState({ multiselectchecbox: multiselectchecbox });
  };

  setLocationOfCookie = (obj) => {
    console.log("{setLocationOfCookie}", obj);
    setCookie("selectedLocation", JSON.stringify(obj));
    this.setState({ locationToShow: obj }, () => {});
  };

  // componentWillMount() {

  // }

  componentDidMount() {
    this.setState({ loader: true });
    let dummyLoc = [];
    if (getCookie("selectedLocation") != null) {
      console.log("A");
      dummyLoc = JSON.parse(getCookie("selectedLocation"));
      console.log("dummyLoc", dummyLoc);
      this.setState({
        locationToShow: JSON.parse(getCookie("selectedLocation")),
      });
    } else if (getCookie("locations") != null) {
      console.log("B");
      let _loc = JSON.parse(getCookie("locations"));
      this.setState({ locationToShow: _loc[0] });
    } else if (getCookie("location") != null && dummyLoc.length === 1) {
      console.log("C");
      let obj = [
        {
          address: getCookie("location"),
          city: getCookie("citylocation"),
          lat: getCookie("lat"),
          lng: getCookie("lng"),
        },
      ];
      setCookie("locations", JSON.stringify(obj));
      this.setState({
        locationToShow: {
          address: getCookie("location"),
          city: getCookie("citylocation"),
          lat: getCookie("lat"),
          lng: getCookie("lng"),
        },
      });
    }
    searchprefenceuser().then((data) => {
      console.log("searchprefenceuser", data.data.data[0]);
      // To Get a Current id , if User Not Select Any Filter Option then this id will be take the id from APi Via State Module
      // let FilterUser = data.data.data[0].searchPreferences
      //   ? data.data.data[0].searchPreferences.map((data) => {
      //       PayloadObject = {
      //         pref_id: data._id,
      //         selectedValue: data.selectedValue,
      //         selectedUnit: data.selectedUnit,
      //       };
      //       DeafultPayload.push(PayloadObject);
      //     })
      //   : "";

      // For the Gender Module
      let GenderDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Gender");
      let genderDataId = data.data.data[0].searchPreferences[GenderDataIndex]["_id"];

      let multiSelectIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Ethnicity");
      let multiSelectId = data.data.data[0].searchPreferences[multiSelectIndex]["_id"];

      this.setState({
        selectGenderOnMount: data.data.data[0].searchPreferences[GenderDataIndex].selectedValue[0],
        multiselectchecbox: data.data.data[0].searchPreferences[multiSelectIndex].selectedValue,
      });

      // For the Height Module
      let HeightDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Height");
      let heightDataId = data.data.data[0].searchPreferences[HeightDataIndex]["_id"];
      let minHeightValue = data.data.data[0].searchPreferences[HeightDataIndex].selectedValue[0];
      let maxHeightValue = data.data.data[0].searchPreferences[HeightDataIndex].selectedValue[1];
      this.setState({ isHeightInFtOrCm: data.data.data[0].searchPreferences[HeightDataIndex].selectedUnit });

      this.props.heightFilterByValues(minHeightValue, maxHeightValue);
      let apiObjectForHeightRange =
        HeightDataIndex > -1
          ? {
              min: minHeightValue,
              max: maxHeightValue,
            }
          : {
              min: null,
              max: null,
            };

      // For the Age Module
      let AgeDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Age");
      let ageDataId = data.data.data[0].searchPreferences[AgeDataIndex]["_id"];
      let minValue = data.data.data[0].searchPreferences[AgeDataIndex].selectedValue[0];
      let maxValue = data.data.data[0].searchPreferences[AgeDataIndex].selectedValue[1];
      this.props.ageFilterByValues(minValue, maxValue);

      let apiObjectForAgeRange =
        AgeDataIndex > -1
          ? {
              min: minValue,
              max: maxValue,
            }
          : {
              min: null,
              max: null,
            };

      // For the Distance Module
      let DistanceDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Distance");
      let distanceDataId = data.data.data[0].searchPreferences[DistanceDataIndex]["_id"];
      let minDistanceValue = data.data.data[0].searchPreferences[DistanceDataIndex].selectedValue[0];
      // let maxDistanceValue = data.data.data[0].searchPreferences[DistanceDataIndex].selectedValue[1];
      let maxDistanceValue = data.data.data[0].searchPreferences[DistanceDataIndex].selectedValue[1];
      this.props.distanceFilterByValues(minDistanceValue, maxDistanceValue);
      this.setState({ isDistanceInKmOrMi: data.data.data[0].searchPreferences[DistanceDataIndex].selectedUnit });
      let apiObjectForDistanceRange =
        AgeDataIndex > -1
          ? {
              min: minDistanceValue,
              max: maxDistanceValue,
            }
          : {
              min: null,
              max: null,
            };

      // let LookingForIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Looking For");
      // let LookingForIndexData = data.data.data[0].searchPreferences[LookingForIndex].selectedValue[0];
      this.setState({
        searchPreferencesuser: data.data.data,
        valueDistance: apiObjectForDistanceRange,
        valueHeight: apiObjectForHeightRange,
        value: apiObjectForAgeRange,
        Genderid: genderDataId,
        Ageid: ageDataId,
        multiSelectId: multiSelectId,
        HeightRangeid: heightDataId,
        DistanceRangeid: distanceDataId,
        loader: false,
      });
    });
  }

  // Used to get switch tabs in Height Module
  handleChange = (event, tabselectedvalue) => {
    this.setState({ tabselectedvalue });
  };

  // Used to get switch tabs in Distance Module
  handleChangeDistance = (event, distancetabselectedvalue) => {
    this.setState({ distancetabselectedvalue });
  };

  // selects gender from radio button, and applies active
  selectGenderHandler = (gender, data) => {
    console.log("select gener handler", gender, data);
    this.setState({ selectGenderOnMount: gender });
  };

  getDistanceMinValue = (min) => {
    return this.state.activeLinkdistance === 0 ? this.convertkmtome(min, this.state.activeLinkdistance === 0) : min;
  };

  getDistanceMaxValue = (max) => {
    return this.state.activeLinkdistance === 0 ? this.convertkmtome(max, this.state.activeLinkdistance === 0) : max;
  };

  // Converting Cemi to Feet
  convertkmtome = (cemi, isFeet) => {
    return isFeet ? parseInt(cemi / 1.6) : cemi;
  };

  getHeightMinValue = (min) => {
    return this.state.activeLinkHeight === 0 ? this.convertCemitoFit(min, this.state.activeLinkHeight === 0) : min;
  };

  getHeightMaxValue = (max) => {
    return this.state.activeLinkHeight === 0 ? this.convertCemitoFit(max, this.state.activeLinkHeight === 0) : max;
  };

  // Converting Cemi to Feet
  convertCemitoFit = (cemi, isFeet) => {
    return isFeet ? parseFloat(cemi / 30.48).toFixed(1) : cemi;
  };

  handleLookingFor = (event) => {
    this.setState({
      lookingForSelection: event.label,
      lookingForId: event.id,
    });
  };

  // Function for the Age Range Select
  handleAgeRange = (value, data) => {
    setCookie("AgeRange", value);
    this.setState({
      value,
      Ageid: data._id,
    });
    this.props.ageFilterByValues(value.min, value.max);
  };

  // Function for the Height Range Select
  handleDistanceRange = (valueDistance, data) => {
    this.setState({
      valueDistance: valueDistance,
      DistanceRangeid: data._id,
    });
    this.props.distanceFilterByValues(valueDistance.min, valueDistance.max);
  };

  // Function for the Height Range Select
  handleHeightRange = (valueHeight, data) => {
    this.setState({
      valueHeight,
      HeightRangeid: data._id,
    });
    this.props.heightFilterByValues(valueHeight.min, valueHeight.max);
  };

  // To Switch Tabs Distance
  handleClickDistance = (id, name) => {
    this.setState({ activeLinkdistance: id, distanceToShow: name, isDistanceInKmOrMi: name });
  };

  // To Switch Tabs Height
  handleClickHeight = (id, name) => {
    this.setState({ activeLinkHeight: id, heightValue: name, isHeightInFtOrCm: name });
  };

  // Accroding the Map it render the Screen Dta
  getInput = (data) => {
    const { classes } = this.props;
    switch (data.Priority) {
      case 2:
        // For the Age
        let SelectUserRange = (
          <div className="p-0 py-2 col-12 Msliderbg">
            <div className="row">
              <div className="col-6 text-left text-muted">
                <h5 className="filterParameters">{data.PreferenceTitle}</h5>
              </div>
            </div>
            <div className="row py-2">
              <div className="col-2">
                <span className="d_filterUnits">
                  {this.props.filterByAgeValue.min} <FormattedMessage id="message.years" />
                </span>
              </div>
              <div className="col-6 d_filterUser">
                <InputRange
                  maxValue={data.OptionsValue[1]}
                  minValue={data.OptionsValue[0]}
                  value={this.props.filterByAgeValue}
                  onChange={(value) => this.handleAgeRange(value, data)}
                />
              </div>
              <div className="col-4 ">
                <span className="d_filterUnits">
                  {this.props.filterByAgeValue.max} <FormattedMessage id="message.years" />
                </span>
              </div>
            </div>
          </div>
        );
        return SelectUserRange;

      case 3:
        // For the Distance
        let SelectUserDistanceRange = (
          <div className="py-2 p-0 col-12 Msliderbg">
            <div className="row">
              <div className="col-4 text-left text-muted">
                <h5 className="filterParameters">{data.PreferenceTitle} :</h5>
              </div>
              <div className="col-8 text-right text-muted">
                <div className="row">
                  <div className="p-0 col-6"></div>
                </div>
              </div>
            </div>

            <div className="row py-2">
              <div className="col-2">
                <span className="d_filterUnits">
                  {this.state.isDistanceInKmOrMi === "Km"
                    ? this.getDistanceMinValue(parseInt(this.props.filterByDistanceValue.min))
                    : this.convertkmtome(this.props.filterByDistanceValue.min, true)}{" "}
                  <span className="pl-1">{this.state.isDistanceInKmOrMi}</span>
                </span>
              </div>
              <div className="col-6 d_filterUser">
                <InputRange
                  maxValue={100}
                  minValue={0}
                  value={this.props.filterByDistanceValue}
                  onChange={(valueDistance) => this.handleDistanceRange(valueDistance, data)}
                />
              </div>
              <div className="col-4 d-flex">
                <span className="d_filterUnits">
                  {this.state.isDistanceInKmOrMi === "Mi"
                    ? parseInt(this.props.filterByDistanceValue.max / 1.6)
                    : parseInt(this.props.filterByDistanceValue.max)}
                  {/* {this.state.isDistanceInKmOrMi === "Km"
                    ? this.getDistanceMinValue(parseInt(this.props.filterByDistanceValue.max))
                    : this.convertkmtome(this.props.filterByDistanceValue.max, true)} */}
                </span>
                <div className="Mdistance_tabs distanceToggler">
                  <div className="d-inline-flex row">
                    {this.state.linksDistance.map((link) => {
                      return (
                        <div key={link.id} className="p-0 col-6 MSwitch_distancecontainer">
                          <ul>
                            <li
                              onClick={() => this.handleClickDistance(link.id, link.name)}
                              className={
                                link.name.toLowerCase() === this.state.isDistanceInKmOrMi.toLowerCase() ? " activeitem_distance" : ""
                              }
                            >
                              {link.name}
                            </li>
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
        return SelectUserDistanceRange;

      case 17:
        // For the Height
        let SelectUserHeightRange = (
          <div className="py-2 p-0 col-12 Msliderbg">
            <div className="row">
              <div className="col-4 text-left text-muted">
                <h5 className="filterParameters">{data.PreferenceTitle} :</h5>
              </div>
              <div className="col-8 text-right text-muted">
                <div className="row">
                  <div className="p-0 col-6"></div>
                </div>
              </div>
            </div>
            <div className="row py-2">
              <div className="col-2">
                <span className="d_filterUnits">
                  {this.state.isHeightInFtOrCm === "Cm"
                    ? this.getHeightMinValue(this.props.filterByHeightValue.min)
                    : this.convertCemitoFit(this.props.filterByHeightValue.min, true)}
                  <span className="pl-1">{this.state.isHeightInFtOrCm}</span>
                </span>
              </div>
              <div className="col-6 d_filterUser">
                <InputRange
                  //  maxValue={100}
                  //  minValue={0}
                  maxValue={data.OptionsValue[1]}
                  minValue={data.OptionsValue[0]}
                  value={this.props.filterByHeightValue}
                  onChange={(valueHeight) => this.handleHeightRange(valueHeight, data)}
                />
              </div>
              <div className="col-4 d-flex">
                <span className="d_filterUnits">
                  {this.state.isHeightInFtOrCm === "Cm"
                    ? this.getHeightMinValue(this.props.filterByHeightValue.max)
                    : this.convertCemitoFit(this.props.filterByHeightValue.max, true)}
                </span>
                <div className="Mheight_tabs distanceToggler">
                  <div className="d-inline-flex row">
                    {this.state.linksHeight.map((link) => {
                      return (
                        <div key={link.id} className="p-0 col-6 MSwitch_heightcontainer">
                          <ul>
                            <li
                              onClick={() => this.handleClickHeight(link.id, link.name)}
                              className={
                                link && link.name && link.name.toLowerCase() === this.state.isHeightInFtOrCm.toLowerCase()
                                  ? " activeitem_height"
                                  : ""
                              }
                            >
                              {link.name}
                            </li>
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
        return SelectUserHeightRange;

      case 4:
        // For the Gender
        let Ageoptions = [];
        data.OptionsValue.map((data) =>
          Ageoptions.push({
            label: data,
            value: data,
          })
        );

        let SelectUserRange1 = (
          // For the Gender
          <div className="pt-2 filter_gender">
            <h5 className="genderParameter">{data.PreferenceTitle}</h5>
            <FormControl component="div" className={"gender_title"}>
              <RadioGroup aria-label="gender" name="gender2" className={classes.radioButtonSelect}>
                {Ageoptions.map((k) => (
                  <FormControlLabel
                    value={k.value}
                    control={
                      <Radio
                        value={k.value}
                        onChange={() => this.selectGenderHandler(k.value, data)}
                        checked={this.state.selectGenderOnMount === k.value}
                        classes={{ root: classes.radio, checked: classes.checked }}
                      />
                    }
                    label={k.value}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          </div>
        );
        return SelectUserRange1;

      case 5:
        let active = {
          padding: "5px 8px",
          margin: "5px 5px",
          borderRadius: "5px",
          border: "2px solid #e31b1b",
          color: "#e31b1b",
          cursor: "pointer",
          fontSize: "12px",
        };
        let deactive = {
          padding: "5px 8px",
          borderRadius: "5px",
          margin: "5px 5px",
          border: "2px solid #DBDBDB",
          color: "#484848",
          cursor: "pointer",
          fontSize: "12px",
        };
        return (
          <div className="pt-2 filter_gender">
            <h5 className="genderParameter">{data.PreferenceTitle}</h5>
            {/* <FormControl component="div" className={"gender_title w-100"}> */}
            <div className="d-flex flex-wrap">
              {data.OptionsValue &&
                data.OptionsValue.map((k, i) => (
                  <div
                    key={i}
                    style={this.state.multiselectchecbox.includes(k) ? active : deactive}
                    onClick={() => this.handlemultiselect(k)}
                  >
                    {k}
                  </div>
                ))}
            </div>

            {/* </FormControl> */}
          </div>
        );

      default: {
      }
    }
  };

  openLocation = () => {
    this.props.history.push("/map");
  };

  setNewPassportLocation = (lat, lng, addr) => {
    setLocation(
      {
        latitude: lat,
        ip: getCookie("ipAddress"),
        timeZone: getCookie("timezone"),
        isPassportLocation: true,
        longitude: lng,
        address: addr,
      },
      getCookie("token")
    )
      .then((res) => {
        console.log("setLocation success", res);
      })
      .catch((err) => console.log("setLocation err", err));
  };

  // API Call to Filter Search User
  handleFilterUser = (lat, lng) => {
    let newlat = this.state.locationToShow.lat;
    let newlng = this.state.locationToShow.lng;
    try {
      let l = JSON.parse(getCookie("selectedLocation"));
      newlat = this.state.locationToShow.lat;
      newlng = this.state.locationToShow.lng;
    } catch (e) {
      console.log("%c broken cookie selectedLocation", "background: #000; color: #FFF; font-size: 20");
    }
    newlat = this.state.locationToShow.lat;
    newlng = this.state.locationToShow.lng;
    let FilterPayload = "";
    let Filteruserpayload = {
      preferences: [
        {
          pref_id: this.state.Genderid,
          selectedValue: [this.state.selectGenderOnMount],
        },
        { pref_id: this.state.multiSelectId, selectedValue: this.state.multiselectchecbox },
        {
          pref_id: this.state.Ageid ? this.state.Ageid : this.state.CurrentDeafultValueid,
          selectedValue: [this.props.filterByAgeArr[0], this.props.filterByAgeArr[1]],
          selectedUnit: "year",
        },
        {
          pref_id: this.state.DistanceRangeid,
          selectedValue: [parseInt(this.props.filterByDistanceValue.min), parseInt(this.props.filterByDistanceValue.max)],
          selectedUnit: this.state.isDistanceInKmOrMi,
        },
        {
          pref_id: this.state.HeightRangeid,
          selectedValue:
            this.state.isHeightInFtOrCm === "Cm"
              ? [this.props.filterByHeightArr[0], this.props.filterByHeightArr[1]]
              : [
                  parseInt(this.convertCemitoFit(this.props.filterByHeightArr[0])),
                  parseInt(this.convertCemitoFit(this.props.filterByHeightArr[1])),
                ],

          selectedUnit: this.state.isHeightInFtOrCm,
        },
      ],
      latitude: parseFloat(newlat),
      longitude: parseFloat(newlng),
    };

    if (this.state.Genderid || this.state.Ageid || this.state.DistanceRangeid || this.state.HeightRangeid) {
      console.log("filterPayload inside filter user if ", Filteruserpayload);
      FilterPayload = Filteruserpayload;
      this.setNewPassportLocation(newlat, newlng, this.state.locationToShow.address);
    } else {
      FilterPayload = {
        latitude: parseFloat(newlat),
        longitude: parseFloat(newlng),
        preferences: DeafultPayload,
      };
      this.setNewPassportLocation(newlat, newlng, this.state.locationToShow.address);
      // console.log("filterPayload inside filter user else ", FilterPayload);
    }
    console.log(newlat, newlng);
    Searchpostprefenceuser(FilterPayload)
      .then((data) => {
        NearbyPeople().then((res) => {
          console.log("{Searchpostprefenceuser}", res.data.data);
          this.props.setAllDiscoverPeople(res.data.data);
        });
        this.props.onClose();
        this.props.modalResetCount();
      })
      .catch((err) => console.log("fail search pref()", err));
  };

  // the screen sent as argument to this function will be rendered, or make it false to render default screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  handleFilterScreen = () => {
    this.updateScreen(
      <div className="d_map_pref">
        <MapHome
          modalUpdateCount={this.props.modalUpdateCount}
          updateScreen={this.updateScreen}
          setLocationOfCookie={this.setLocationOfCookie}
          handleFilterUser={this.handleFilterUser}
          modalDecrementCount={this.props.modalDecrementCount}
          onClose={this.closeModal}
          modalResetCount={this.props.modalResetCount}
        />
      </div>
    );
    this.props.modalUpdateCount();
  };

  // close modal and set the state of modal back to 1
  closeModal = () => {
    this.props && this.props.onClose();
    setTimeout(() => {
      this.props.modalResetCount();
    }, 200);
  };

  render() {
    // console.log("lookingForSelection", this.state.lookingForSelection);
    const fontSize = { fontSize: "13px" };
    const { classes } = this.props;
    return (
      <div>
        {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div>
            {!this.state.currentScreen ? (
              <div className="filter_body">
                <div className="py-5 px-4">
                  <div className="setPreferences">
                    <FormattedMessage id="message.preferences" />
                  </div>
                  <div className="col-12 px-0" style={fontSize}>
                    <div className="row mx-0">
                      <div className="col-4 px-0">
                        <FormattedMessage id="message.location" />:
                      </div>

                      <div
                        className="col-7 text-right px-0 onClickSearchLocation"
                        onClick={
                          this.props.checkIfUserIsProUserVariable &&
                          this.props.checkIfUserIsProUserVariable.ProUserDetails &&
                          this.props.checkIfUserIsProUserVariable.ProUserDetails.subscriptionId === "Free Plan"
                            ? () => {
                                this.props.onClose();
                                setTimeout(() => {
                                  this.props.upgradePlanModal();
                                }, 200);
                              }
                            : this.handleFilterScreen
                        }
                      >
                        {/* {getCookie("selectedLocation") != null
                          ? JSON.parse(getCookie("selectedLocation")).address
                          : getCookie("location") != null
                          ? getCookie("location")
                          : JSON.parse(getCookie("baseAddress")).address} */}
                        {this.props.checkIfUserIsProUserVariable &&
                        this.props.checkIfUserIsProUserVariable.ProUserDetails &&
                        this.props.checkIfUserIsProUserVariable.ProUserDetails.subscriptionId === "Free Plan"
                          ? JSON.parse(getCookie("baseAddress")).address
                          : JSON.parse(getCookie("selectedLocation")).address}
                      </div>
                      <div className="pl-2 d-flex align-items-center">
                        <i class="chevright fas fa-chevron-right"></i>
                      </div>
                    </div>
                  </div>

                  {/* mapping loop on the api data for rendering ui screens */}
                  {this.state.searchPreferencesuser &&
                  this.state.searchPreferencesuser[0] &&
                  this.state.searchPreferencesuser[0].searchPreferences
                    ? this.state.searchPreferencesuser[0].searchPreferences.map((data, index) => (
                        <div key={index}>{this.getInput(data)}</div>
                      ))
                    : ""}
                  {/* Save & Close Button Module */}
                  <div className="pt-3 filter_closebtn text-center">
                    <button onClick={this.closeModal}>
                      <FormattedMessage id="message.cancel" />
                    </button>
                    <button onClick={() => this.handleFilterUser(this.state.locationToShow.lat, this.state.locationToShow.lng)}>
                      <FormattedMessage id="message.apply" />
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              this.state.currentScreen
            )}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // age
    filterByAgeValue: state.UserProfile.filterByAgeValue,
    filterByAgeArr: state.UserProfile.filterByAgeArr,
    // height
    filterByHeightValue: state.UserProfile.filterByHeightValue,
    filterByHeightArr: state.UserProfile.filterByHeightArr,
    // distance
    filterByDistanceValue: state.UserProfile.filterByDistanceValue,
    filterByDistanceArr: state.UserProfile.filterByDistanceArr,
    checkIfUserIsProUserVariable: state.ProUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAllDiscoverPeople: (data) => dispatch(discoverPeople(data)),
    ageFilterByValues: (min, max) => dispatch(filterByValues.filterByAgeObjectVal(min, max)),
    heightFilterByValues: (min, max) => dispatch(filterByValues.filterByHeightObjectVal(min, max)),
    distanceFilterByValues: (min, max) => dispatch(filterByValues.filterByDistanceObjectVal(min, max)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(FilterUser));
