import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./Discover.scss";
import { withStyles } from "@material-ui/core/styles";
import Carousel from "./CustomCarousel";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import { FormattedMessage } from "react-intl";
// import { InitDoc } from "../../../../lib/CardsLib";
import { connect } from "react-redux";
import {
  NearbyPeople,
  ActivateBoost,
  MyDisLikeUser,
  SuperLikeNearPeople,
  LikeNearPeople,
  GetRewindDetails,
  OtherUserProfile,
  unmatchuser,
  BlockUser,
  ReasonblockUser,
  SuperLikeUser,
} from "../../../../controller/auth/verification";
import * as actionChat from "../../../../actions/chat";
import { Images } from "../../../../Components/Images/Images";
import CustomButton from "../../../../Components/Button/Button";
import { getBoostDetailsOnWindowRefresh } from "../../../../actions/boost";
import Menu from "@material-ui/core/Menu";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
// import MaterialDropdown from "../../../../Components/Dropdown/MaterialDropdown";

const ITEM_HEIGHT = 48;

const convertSecondsToMinutesTwoPads = (totalSeconds) => {
  // let hours = Math.floor(totalSeconds / 3600);
  totalSeconds %= 3600;
  let minutes = Math.floor(totalSeconds / 60);
  let seconds = totalSeconds % 60;

  // If you want strings with leading zeroes:
  minutes = String(minutes).padStart(2, "0");
  // hours = String(hours).padStart(2, "0");
  seconds = String(seconds).padStart(2, "0");

  return minutes + ":" + seconds;
};

const convertSecondsToMinutesUsingTimestamp = (startTime, currentTime) => {
  var secondsLeft = Math.round(startTime - currentTime) / 1000;
  if (secondsLeft >= 100) {
    return Math.round(secondsLeft.toFixed(3));
  } else {
    return Math.round(secondsLeft.toFixed(2));
  }
};

const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "#e31b1b",
  },
  disabledButton: {
    backgroundColor: "#f8f8f8",
  },
  Verified: {
    display: "flex",
  },
});

class Discover extends Component {
  constructor(state) {
    super(state);
    this.state = {
      loader: true,
      UserSearchresult: "",
      variant: "",
      countDown: "",
      startIndex: 0,
      UnLikeArr: [],
      LikeArr: [],
      remainsLikesInString: "",
      SuperLikeArr: [],
      userProfileInfo: "",
      reportedUserReason: "",
    };
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  handleblockreason = () => {
    ReasonblockUser().then((data) => {
      this.setState({ blockreason: data.data.data });
    });
  };

  componentDidMount() {
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    this.props.handleWebsiteActiveValue("/app/discover");
    this.getAllUserData();
    this.handleblockreason();
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevState.startIndex !== this.state.startIndex) {
  //     this.handleUserInfo(this.props.discoverPeopleList[this.state.startIndex]);
  //   }
  // }

  // Left Action Type - 1 (DisLike User)
  // Right Action Type - 2 (Like User)
  // Top Action Type - 3 (SuperLike User)

  handleUserCardId = (UserCardid, type) => {
    if (type === 1) {
      this.setState(
        {
          rewind: true,
          disliked: true,
          dislikedId: UserCardid,
          startIndex: this.state.startIndex + 1,
        },
        () => {
          console.log("sdad", this.state.rewind, this.state.disliked);
        }
      );

      let Unlikepayload = {
        targetUserId: UserCardid,
      };
    }

    if (type === 2) {
      if (this.state.remainsLikesInString > 0 || this.state.remainsLikesInString === "unlimited") {
        LikeNearPeople({ targetUserId: UserCardid })
          .then((data) => {
            this.setState(
              {
                startIndex: this.state.startIndex + 1,
                open: true,
                variant: "success",
                usermessage: "Your match interest has been sent.",
              },
              () => this.handleUserInfo(this.state.UserSearchresult[this.state.startIndex])
            );
          })
          .catch((error) => {
            this.setState({
              open: true,
              variant: "error",
              usermessage: "Some error occured.",
            });
          });
      } else {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Your likes have exhausted.",
        });
      }
    }

    if (type === 3) {
      SuperLikeUser({ targetUserId: UserCardid }, getCookie("token"))
        .then((data) => {
          if (data.status === 200) {
            if (
              this.props.checkIfUserIsProUser &&
              this.props.checkIfUserIsProUser.ProUserDetails &&
              this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
              this.props.UserCoin > 0
            ) {
              this.props.coinReduce(parseInt(this.props.superLikeCost.Coin));
            }
            this.setState({
              startIndex: this.state.startIndex + 1,
              open: true,
              variant: "success",
              usermessage: `${this.state.UserSearchresult[this.state.startIndex].firstName} Superliked successfully.`,
            });
          } else {
            this.setState({ open: true, usermessage: data.data.message, variant: "error" });
          }
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Something went wrong.",
          });
        });
    }
  };

  /** function to block user */
  // BlockUserFn = (targetUserId) => {
  //   BlockUser(targetUserId)
  //     .then((data) => {
  //       this.setState({
  //         open: true,
  //         variant: "success",
  //         usermessage: `${this.state.userProfileInfo.firstName} Blocked successfully.`,
  //       });
  //       this.handleClosemenu();
  //     })
  //     .catch((err) => {
  //       this.setState({
  //         open: true,
  //         variant: "error",
  //         usermessage: `${this.state.userProfileInfo.firstName} Blocked successfully.`,
  //       });
  //       this.handleClosemenu();
  //     });
  // };

  /** function to umatch user */
  unmatchuser = (userid) => {
    let obj = { targetUserId: userid };
    unmatchuser(obj)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "UnMatch Successfull",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Something went wrong when unmatching user.",
        });
      });
  };

  openReportModal = () => {
    this.setState({ reportopen: true });
    this.handleClosemenu();
  };

  // To Close Menu
  handleClosemenu = () => {
    this.setState({ anchorEl: null });
    setTimeout(() => {
      this.setState({ open: false });
    }, 1000);
  };

  handleClickmenu = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // Checking User has Coin Avabile or not
  rewindCards = (UserCardid) => {
    // if (this.props.UserCoin > 10) {
    if (
      this.props.checkIfUserIsProUser &&
      this.props.checkIfUserIsProUser.ProUserDetails &&
      this.props.checkIfUserIsProUser.ProUserDetails.rewindCount === "unlimited"
    ) {
      this.handleRewindUser(UserCardid);

      // let userData = this.state.rewind && this.state.disliked ? InitDoc() : "";
      console.log("sdad", this.state.rewind, this.state.disliked);
    } else {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Rewind not allowed.",
      });
      setTimeout(() => {
        this.setState({ open: false });
      }, 1500);
    }
  };

  // API Call for the ReWind User
  handleRewindUser = (UserCardid) => {
    GetRewindDetails(UserCardid)
      .then((data) => {
        this.props.coinReduce(parseInt(this.props.rewind.Coin));
        this.setState({ startIndex: this.state.startIndex - 1 });
        console.log("[handleRewindUser data]", data);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  /** gets all users data for discover people */
  getAllUserData = () => {
    NearbyPeople()
      .then((data) => {
        this.setState(
          {
            UserSearchresult: data.data.data,
            loader: false,
            remainsLikesInString: data.data.remainsLikesInString,
          },
          () => {
            console.log("[nearby people cb]");
            this.handleUserInfo(data.data.data[0]);
            this.props && this.props.getBoostDetailsOnWindowRefresh(data.data.boost);
          }
        );
      })
      .catch(() => {
        this.setState({ loader: true });
      });
  };

  /** function to navigate to target users page */
  navToOtherUsersPage = (name, id) => {
    this.props.history.push(`/app/user/${name}/${id}`);
  };

  /** function set report users -> report [...] array */
  getReasonForReport = (data) => {
    this.setState({ reportedUserReason: data });
    this.closeReportModal();
    this.toggleSubDialog();
  };

  // toggle sub dialog
  toggleSubDialog = () => {
    this.setState({ subDialog: !this.state.subDialog });
  };

  // close confirmation dialog on report dialog
  closeConfirmationDialogWithMainDialog = () => {
    this.toggleSubDialog();
    this.handlereportuser(this.state.reportedUserReason);
    setTimeout(() => {
      this.closeReportModal();
    }, 500);
  };

  // close report modal
  closeReportModal = () => {
    this.setState({ reportopen: false });
  };

  // API to recommend friend
  recommendFriend = () => {
    navigator.clipboard.writeText(`Get yourself into world of dating. Let's connect on https://app.unfiltered.love`);
    this.setState({
      open: true,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends now",
    });
    this.handleClosemenu();
  };

  /** fn to get users info on click on target users profile, and it is stored in redux store  */
  handleUserInfo = (data) => {
    console.log("{handleUserInfo}", data);
    let otheruserpayload = {
      targetUserId: (data && data.opponentId) || (data && data._id),
    };
    OtherUserProfile(otheruserpayload).then((data) => {
      try {
        console.log("[handleUserInfo]", data.data.data);
        this.setState({ userProfileInfo: data.data.data });
        this.props.selectUserToChat(data.data.data);
      } catch (e) {
        console.log("no users", e);
      }
    });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const { discoverPeopleList, classes } = this.props;
    return (
      <div className="WDisMaster h-100">
        <div className="col-12 px-0 h-100">
          {this.state.startIndex >= this.state.UserSearchresult.length ? (
            <div className="col-12 h-100 d-flex justify-content-center align-items-center">
              <div className="row h-100">
                <div className="col-12 d-flex justify-content-center align-items-end">
                  <img src={Icons.NoUsers} alt="NoData" title="NoData" height={300} />
                </div>
                <h5 className="py-3 pt-2 text-muted w-100 text-center">Oops! Nobody found!</h5>
              </div>
            </div>
          ) : (
            /** here goes the shadow */

            <div>
              {/* <div className="col-12 pt-5"></div> */}

              <div className="row" id="innerDiscoverContent">
                <div className="col-6 px-0 ">
                  <div></div>
                  <Carousel userProfileInfo={this.state.UserSearchresult && this.state.UserSearchresult[this.state.startIndex]} />
                  {/* <div className="col-12 rev_global_actions pt-1 w-100"> */}
                  <div className="button-actions row mt-4 justify-content-center align-items-center ml-2 w-100">
                    {/* <CustomButton
                      handler={() => this.rewindCards(discoverPeopleList[this.state.startIndex].opponentId)}
                      className="revert-action mr-2"
                      text={<img src={Images.P_Undo} width={22} height={22} alt="undo" />}
                    /> */}
                    <CustomButton
                      handler={() => this.handleUserCardId(this.state.UserSearchresult[this.state.startIndex].opponentId, 1)}
                      className="left-action mr-3"
                      text={<img src={Icons.NewDislike} width={75} height={75} alt="dislike" />}
                    />

                    <CustomButton
                      handler={() => this.handleUserCardId(this.state.UserSearchresult[this.state.startIndex].opponentId, 2)}
                      className="right-action"
                      text={<img src={Icons.NewLike} width={75} height={75} alt="right-action" />}
                    />
                    <CustomButton
                      text={<img src={Icons.NewSuperLike} width={75} height={75} alt="superlike" />}
                      className="right-action ml-3"
                      handler={() => this.handleUserCardId(this.state.UserSearchresult[this.state.startIndex].opponentId, 3)}
                    />
                  </div>
                  {/* </div> */}
                  {/** shows the countdown timer */}
                  {this.state.countDown > 0 ? (
                    <div className="d-flex pt-2 justify-content-center align-items-center">
                      <div className="timer">
                        <div className="text-center">{convertSecondsToMinutesTwoPads(this.state.countDown)}</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="col-6 discoverPage_details">
                  <div className="row mx-0">
                    <div className="col-12 pt-5 heading">
                      <div className="row">
                        <div className="col-10 d-flex px-0">
                          <div className="onlineStatusOfUser">
                            {this.state.UserSearchresult && this.state.UserSearchresult[this.state.startIndex].onlineStatus === 1 ? (
                              <div className="online"></div>
                            ) : (
                              <div className="offline"></div>
                            )}
                          </div>
                          <h3
                            style={{ cursor: "pointer" }}
                            className="pl-3"
                            onClick={() =>
                              this.navToOtherUsersPage(
                                this.state.UserSearchresult[this.state.startIndex].firstName,
                                this.state.UserSearchresult[this.state.startIndex].opponentId
                              )
                            }
                          >
                            {this.state.UserSearchresult && this.state.UserSearchresult[this.state.startIndex].firstName},{" "}
                            {this.state.UserSearchresult && this.state.UserSearchresult[this.state.startIndex].age.value}
                          </h3>
                          <div className={`pl-3 ${classes.Verified}`}>
                            <img src={Images.P_Verified} height={30} width={30} alt="verified" />
                          </div>
                        </div>
                        {/* <div className="col-2">
                          <MaterialDropdown
                            onClick={this.handleClickmenu}
                            icon={Images.P_AlertBox}
                            anchorEl={anchorEl}
                            onClose={this.handleClosemenu}
                            PaperProps={{
                              style: {
                                maxHeight: ITEM_HEIGHT * 6,
                                width: 200,
                              },
                            }}
                          >
                            <MenuItem onClick={this.recommendFriend}>
                              <input type="text" hidden value="https://app.unfiltered.love" id="recommend" />
                              <p className="m-0" style={{ fontFamily: "Product Sans" }}>
                                <FormattedMessage id="message.recommendFriend" />
                              </p>
                            </MenuItem>
                            <MenuItem onClick={this.openReportModal}>
                              <p className="m-0" style={{ fontFamily: "Product Sans" }}>
                                <FormattedMessage id="message.report" /> {this.state.userProfileInfo.firstName}
                              </p>
                            </MenuItem>
                            {this.state.userProfileInfo && this.state.userProfileInfo.isMatched ? (
                              <MenuItem onClick={() => this.unmatchuser(this.state.userProfileInfo._id)}>
                                <p className="m-0">
                                  <span style={{ fontFamily: "Product Sans" }}>
                                    <FormattedMessage id="message.report" /> {this.state.userProfileInfo.firstName}
                                  </span>
                                </p>
                              </MenuItem>
                            ) : (
                              <span />
                            )}
                          </MaterialDropdown>
                        </div> */}
                        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClickmenu}>
                          <img src={Images.P_AlertBox} />
                        </Button>
                        <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={this.handleClosemenu}>
                          <MenuItem onClick={this.recommendFriend}>
                            <input type="text" hidden value="https://app.unfiltered.love" id="recommend" />
                            <p className="m-0" style={{ fontFamily: "Product Sans" }}>
                              <FormattedMessage id="message.recommendFriend" />
                            </p>
                          </MenuItem>
                          <MenuItem onClick={this.openReportModal}>
                            <p className="m-0" style={{ fontFamily: "Product Sans" }}>
                              <FormattedMessage id="message.report" /> {this.state.userProfileInfo.firstName}
                            </p>
                          </MenuItem>
                          {this.state.userProfileInfo && this.state.userProfileInfo.isMatched ? (
                            <MenuItem onClick={() => this.unmatchuser(this.state.userProfileInfo._id)}>
                              {" "}
                              <p className="m-0">
                                <span style={{ fontFamily: "Product Sans" }}>
                                  <FormattedMessage id="message.report" /> {this.state.userProfileInfo.firstName}
                                </span>
                              </p>
                            </MenuItem>
                          ) : (
                            ""
                          )}
                        </Menu>
                      </div>
                    </div>
                  </div>

                  <h5 className="pt-5 about_heading">
                    <FormattedMessage id="message.discover_aboutMe" />
                  </h5>
                  <div>
                    <p className="pt-1 pr-4 label">
                      {this.state.UserSearchresult &&
                      this.state.UserSearchresult[this.state.startIndex] &&
                      this.state.UserSearchresult[this.state.startIndex].about &&
                      this.state.UserSearchresult[this.state.startIndex].about.length > 0 ? (
                        this.state.UserSearchresult[this.state.startIndex].about
                      ) : (
                        <FormattedMessage id="message.notAvailable" />
                      )}
                    </p>
                  </div>
                  <h5 className="pt-5 about_heading">{this.state.userProfileInfo && this.state.userProfileInfo.myPreferences[1].title}</h5>
                  <div className="pt-2">
                    {this.state.userProfileInfo &&
                      this.state.userProfileInfo.myPreferences &&
                      this.state.userProfileInfo.myPreferences[1] &&
                      this.state.userProfileInfo.myPreferences[1].data.map((k) => (
                        <div className="row py-1">
                          <div className="col-3 col-md-5 col-lg-4 col-xl-4 col-sm-6">
                            <span className="label">{k.label}:</span>
                          </div>
                          <div className="col-9 col-md-7 col-lg-8 col-xl-8 col-sm-6">
                            {k.selectedValues.length === 0 || k.selectedValues === null ? (
                              <span>None</span>
                            ) : (
                              k.selectedValues.map((v) => <span>{v}</span>)
                            )}
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          )}

          <div className="final-state text-center hidden">
            <h2>
              <FormattedMessage id="message.noNearbyPeople" />.
            </h2>
          </div>
        </div>

        {/* Report User Module (POP-UP) */}
        <Dialog
          open={this.state.reportopen}
          onClose={this.closeReportModal}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
            <p style={{ fontFamily: "Product Sans" }}>Report User</p>
          </DialogTitle>
          <DialogContent className="py-0 px-3">
            <DialogContentText id="alert-dialog-description">
              {this.state.blockreason
                ? this.state.blockreason.map((data, index) => (
                    <div
                      style={{ cursor: "pointer", fontFamily: "Product Sans" }}
                      key={index}
                      className="py-2 d-block text-center border-bottom"
                      onClick={() => this.getReasonForReport(data)}
                    >
                      {data}
                    </div>
                  ))
                : ""}
            </DialogContentText>
          </DialogContent>
          <DialogActions className="py-2 m-auto">
            <Button
              className="text-center"
              onClick={this.closeReportModal}
              style={{ fontFamily: "Product Sans" }}
              color="primary"
              autoFocus
            >
              <FormattedMessage id="message.cancel" />
            </Button>
          </DialogActions>
        </Dialog>

        {/** opens this dialog modal when selecting option of report user */}
        <Dialog
          open={this.state.subDialog}
          onClose={this.toggleSubDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
            {`Are you sure, You want to report ${this.state.userProfileInfo.firstName}`}
          </DialogTitle>

          <DialogActions className="py-2 m-auto">
            <Button className="text-center" onClick={this.closeConfirmationDialogWithMainDialog} color="primary" autoFocus>
              <FormattedMessage id="message.yes" />
            </Button>
            <Button className="text-center" onClick={this.toggleSubDialog} color="primary" autoFocus>
              <FormattedMessage id="message.no" />
            </Button>
          </DialogActions>
        </Dialog>

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    UserCoin: state.UserProfile.CoinBalance,
    discoverPeopleList: state.discoverPeople.discoverPeopleList,
    checkIfUserIsProUser: state.ProUser,
    rewind: state.CoinConfig.CoinConfig.rewind,
    superLikeCost: state.CoinConfig.CoinConfig.superLike,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    selectUserToChat: (data) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", data)),
    getBoostDetailsOnWindowRefresh: (data) => dispatch(getBoostDetailsOnWindowRefresh(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Discover)));
