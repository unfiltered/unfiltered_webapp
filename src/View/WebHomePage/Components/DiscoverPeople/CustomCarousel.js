import React from "react";
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators } from "reactstrap";
import { Images } from "../../../../Components/Images/Images";

class CustomCarousel extends React.Component {
  state = {
    activeIndex: 0,
    animating: false,
    items: [],
    indexOfCarousel: 0,
  };

  componentDidMount() {
    console.log("UserFinalData", this.props.userProfileInfo);
    let profilePic = this.props.userProfileInfo.profilePic;
    this.setState({ items: this.props && [profilePic, ...this.props.userProfileInfo.otherImages] });
  }

  setActiveIndex = (index) => {
    this.setState({ activeIndex: index });
  };

  setAnimating = (bool) => {
    this.setState({ animating: bool });
  };

  next = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === this.state.items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setActiveIndex(nextIndex);
  };

  previous = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.items.length - 1 : this.state.activeIndex - 1;
    this.setActiveIndex(nextIndex);
  };

  goToIndex = (newIndex) => {
    if (this.state.animating) return;
    this.setActiveIndex(newIndex);
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  slides = () => {
    let slides = [
      this.props.userProfileInfo && this.props.userProfileInfo.profilePic,
      ...(this.props.userProfileInfo && this.props.userProfileInfo.otherImages),
    ].map((item, key) => {
      return (
        <CarouselItem onExiting={() => this.setAnimating(true)} onExited={() => this.setAnimating(false)} key={key}>
          <img src={item} style={{ objectFit: "cover" }} alt={item} height={510} onError={this.onError} width={"100%"} />
        </CarouselItem>
      );
    });
    return slides;
  };

  render() {
    return (
      <Carousel activeIndex={this.state.activeIndex} next={this.next} previous={this.previous} className="UserImage">
        <CarouselIndicators items={this.state.items} activeIndex={this.state.activeIndex} onClickHandler={this.goToIndex} />
        {this.slides()}
        {this.state.items.length > 1 ? <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} /> : ""}
        {this.state.items.length > 1 ? <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} /> : ""}
      </Carousel>
    );
  }
}

export default CustomCarousel;
