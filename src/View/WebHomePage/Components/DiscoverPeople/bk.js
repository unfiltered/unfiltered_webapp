// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "./Discover.scss";

// Re-Usuable Components
import WebHeader from "../../../../Components/WebHeader/index";
import WebTabs from "../../../../Components/WebTabs/WebTabs";
import Snackbar from "../../../../Components/Snackbar/Snackbar";

// Reactstrap Components
import { Card, Button } from "reactstrap";

// Material-UI Components
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";

// React Swiple Card
import Swipeable from "react-swipy";

// imported Images
import dislike from "../../../../asset/images/error.png";
import like from "../../../../asset/images/like.png";
import repeat from "../../../../asset/images/repeat.png";
import thunder from "../../../../asset/images/bolt.png";
import star from "../../../../asset/images/star.png";
import Boy from "../../../../asset/WebUserCard/boy.png";
import Girl from "../../../../asset/WebUserCard/girl.png";

// Redux Components
import {
  NearbyPeople,
  LikeNearPeople,
  SuperLikeNearPeople,
  MyunlikeUser,
  MyDisLikeUser
} from "../../../../controller/auth/verification";

let Userid = "";

const discverpeople = "Discover";
const discovernearpeople = "Nearby People";

const styles = theme => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center"
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "#e31b1b"
  },
  disabledButton: {
    backgroundColor: "#f8f8f8"
  }
});

class Discover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      cards: ["Alex Demon, 21"],
      UserSearchresult: "",
      // cardslocation: ["England"],
      peopleimg: "https://cdn.kapwing.com/Group_565-Kgyd0cXCv.png",
      open: false
    };
  }

  // it Call after the rander Method
  componentDidMount() {
    this.setState({ loader: true });
    NearbyPeople()
      .then(data => {
        this.setState({ UserSearchresult: data.data.data });
      })
      .catch(() => {
        this.setState({ loader: false });
      });
  }

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // API Call to UnLike the User
  handledunlikeuser = data => {
    let Unlikepayload = {
      targetUserId: data.opponentId
    };
    MyDisLikeUser(Unlikepayload)
      .then(data => {
        console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Unlike successfully.!!"
        });
      })
      .catch(error => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!"
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = data => {
    let likepayload = {
      targetUserId: data.opponentId
    };

    LikeNearPeople(likepayload)
      .then(data => {
        console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Like successfully.!!"
        });
      })
      .catch(error => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!"
        });
      });
  };

  // API Call to SuperLike the User
  handlesuperlikeuser = data => {
    let superlikepayload = {
      targetUserId: data.opponentId
    };

    SuperLikeNearPeople(superlikepayload)
      .then(data => {
        console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Superlike successfully.!!"
        });
      })
      .catch(error => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!"
        });
      });
  };

  render() {
    const { cards } = this.state;
    // const { cards, cardslocation } = this.state;
    const { peopleimg } = this.state;
    const { classes } = this.props;

    const discverpeople1 = (
      <div className="py-4 col-12">
        <div className="row justify-content-center">
          <div className="py-3 col-10 col-sm-6 col-md-5 col-lg-5 col-xl-5 MSwipycard">
            {cards.length > 0 ? (
              <div>
                <Swipeable
                  buttons={({ left, right }) => (
                    <div className="swipe-buttons">
                      <Button onClick={left}>
                        <img src={repeat} alt="repeat" title="repeat" />
                      </Button>
                      <Button onClick={left}>
                        <img src={dislike} alt="dislike" title="dislike" />
                      </Button>
                      <Button onClick={left}>
                        <img src={star} alt="star" title="star" />
                      </Button>
                      <Button onClick={right}>
                        <img src={like} alt="like" title="like" />
                      </Button>
                      <Button onClick={right}>
                        <img src={thunder} alt="thunder" title="thunder" />
                      </Button>
                    </div>
                  )}
                  onAfterSwipe={this.remove}
                >
                  <Card>
                    {/* {cards[0]}, */}
                    <img
                      src={peopleimg}
                      // src={data.profilePic}
                      className="mainpeopelimg"
                      alt="peopleimgs"
                      title="peopleimgs"
                    />
                  </Card>
                </Swipeable>
                {/* {cards.length > 1 &&
                        <Card zIndex={-1}>
                          {cards[1]}
                      </Card>} */}
              </div>
            ) : (
              <Card zIndex={-2}>No More Around People</Card>
            )}
          </div>
        </div>
      </div>
    );

    const discovernearpeople1 =
      this.state.UserSearchresult && this.state.UserSearchresult
        ? this.state.UserSearchresult.map(data => (
            <div className="pl-md-0 px-3 mt-4 col-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
              <Link to={`/app/user/${data.firstName}/${data.opponentId}`}>
                <div className="wnearpeople_card">
                  <div className="py-3 Wuser_img">
                    <img src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
                  </div>
                  <div className="pb-2 Wuser_name">
                    <p className="m-0">
                      {data.firstName}, {data.age ? data.age.value : ""}
                    </p>
                  </div>
                  <div className="Wuser_action">
                    <i className="fas fa-times" onClick={this.handledunlikeuser.bind(this, data)} />
                    <i className={"fas fa-heart " + data.opponentId} onClick={this.handlelikeuser.bind(this, data)} />
                    <i className="fas fa-star" onClick={this.handlesuperlikeuser.bind(this, data)} />
                  </div>
                </div>
              </Link>
            </div>
          ))
        : " ";

    return (
      <div className="WDisMaster">
        {/* Header Module */}
        <WebHeader headername="Discover" boost="bOost" />

        {/* Inner Tabs Module */}

        {/* {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : ( */}
        <div className="Wcommon_tabs">
          <WebTabs
            tabs={[
              { label: discverpeople }

              // { label: discovernearpeople }
            ]}
            tabContent={[
              { content: discverpeople1 }
              // { content: discovernearpeople1 }
            ]}
          />
        </div>
        {/* )} */}

        {/* Snakbar Components */}
        <Snackbar
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleClose}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Discover);
