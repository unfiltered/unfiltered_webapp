import React from "react";

export const FAQ = () => {
  const openLink = (link) => {
    window.open(link, "blank");
  };
  return (
    <div style={{ height: "100vh", color: "white", padding: "20px", color: "#000" }}>
      <h2>What is UnFiltered?</h2>
      <ul>
        <li>It's the dating app for people that are looking for a Real opportunity for Meaningful Relationships.</li>
        <li>
          <strong>Disruptor. </strong>UnFiltered provides REAL Solutions beyond industry standard. We are the disruptor of all Dating Apps.
        </li>
        <li>Our Developers have over 20 Years of Combined Experience and originated the Algorithm Code used by Most Dating Apps Today.</li>
        <li>
          <strong>Safe.</strong>
          Our User and Financial Data is protected by patented encryption processes.
        </li>
        <li>The app is free to use.  However, we do have extra functionalities if you purchase the membership or credits..</li>
        <li>Download the UnFiltered app for iOS and Android.</li>
        <li> Have more questions?Submit a request</li>
      </ul>
      <h2>How do I start using UnFiltered? </h2>
      <ul>
        <li>You can download the UnFiltered for iOS or Android.</li>
      </ul>

      <h2>Can I use Unfiltered for free?</h2>
      <ul>
        <li>Absolutely! Just download the UnFiltered app for iOS or Android and start using it right away.</li>
        <li>
          If you'd like to access advanced features, you can subscribe to our Preferred Membership right in the app. Tap the Settings icon
          at the top right corner of the main screen, then tap Account and Upgrade to Preferred Membership.
        </li>
      </ul>

      <h2>Do you have a minimum or maximum age requirement to use Unfiltered?</h2>
      <ul>
        <li>Although we do not have a maximum age requirement, you must be at least 18 years old to be part of UnFiltered.</li>
        <li>Download the UnFiltered app for iOS and Android</li>
      </ul>

      <h2>Where can I use UnFiltered? </h2>
      <ul>
        <li>UnFiltered is United States focused.</li>
        <li>Download the UnFiltered app for iOS and Android.</li>
      </ul>
      <h2>Membership</h2>
      <h2>I want to request a refund</h2>
      <ul>
        <li>
          Apple handles all refunds and cancellations for UnFiltered subscriptions or Boost purchases on iOS devices. All refunds are
          granted entirely at the discretion of Apple. To request a refund from Apple, please contact iTunes Customer Support directly at{" "}
          <span
            style={{ color: "blue", textDecoration: "underline", cursor: "pointer" }}
            onClick={() => openLink("https://getsupport.apple.com/")}
          >
             https://getsupport.apple.com/
          </span>
        </li>
        <li>
          For instructions on how to cancel your subscription through Apple, please see this support article:
          <span
            style={{ color: "blue", textDecoration: "underline", cursor: "pointer" }}
            onClick={() => openLink("http://support.apple.com/kb/ht4098")}
          >
            http://support.apple.com/kb/ht4098
          </span>
        </li>
        <li>If you subscribed through Google Play:</li>
        <li>
          The fastest way to get a refund for a subscription or Boost purchase on your Google Play account is to request it directly from
          Google. You can find instructions at Google's support{" "}
          <span
            style={{ color: "blue", textDecoration: "underline", cursor: "pointer" }}
            onClick={() => openLink("https://support.google.com/googleplay/answer/7205930")}
          >
            https://support.google.com/googleplay/answer/7205930
          </span>
        </li>
        <li>
          For instructions on how to cancel your subscription through Google, please see this support article:
          <span
            style={{ color: "blue", textDecoration: "underline", cursor: "pointer" }}
            onClick={() => openLink("https://support.google.com/googleplay/answer/7018481")}
          >
            https://support.google.com/googleplay/answer/7018481
          </span>
        </li>
      </ul>
    </div>
  );
};

export default FAQ;
