import React from "react";
import { Link } from "react-router-dom";
import { Icons } from "../../../../Components/Icons/Icons";
import { keys } from "../../../../lib/keys";

class CookiePolicy extends React.Component {
  render() {
    return (
      <div className="Wsignup">
        <div className="container">
          <div className="row">
            <div className="pt-1 col-4 col-lg-5 col-xl-5 text-left header_lang"></div>
            <div className="col-4 col-md-3 col-lg-2 col-xl-2 text-center header_logo">
              <Link to="/">
                <img src={keys.AppLogo} alt={keys.AppName} title={keys.AppName} className="img-fluid" width="46px" />
              </Link>
            </div>
            <div className="col-4 col-md-5 col-lg-5 col-xl-5 text-right"></div>
          </div>
          <div className="mt-5 ">
            <ul>
              <li>
                {keys.AppName} brings people together. With more than 20 billion matches made to date and millions of new matches made
                daily, our community is constantly growing. With so many people on {keys.AppName}, user safety is a priority. We understand
                that meeting someone for the first time whether online, through an acquaintance or on an outing is intriguing and exciting.
                However, your safety is very important, and because you are in control of your {keys.AppName} experience, there are certain
                safety steps that you should follow while dating – both online and offline. We ask you to read the tips and information
                below, and strongly urge you to follow these guidelines in the interest of your personal safety and well-being. However, you
                are always the best judge of your own safety, and these guidelines are not intended to be a substitute for your own
                judgment. Online Behavior Protect Your Finances & Never Send Money or Financial Information Never respond to any request to
                send money, especially overseas or via wire transfer, and report it to us immediately – even if the person claims to be in
                an emergency. Wiring money is like sending cash: the sender has no protections against loss and it’s nearly impossible to
                reverse the transaction or trace the money. For more information, click on the video below to the U.S. Federal Trade
                Commission's advice to avoid online dating scams, also available here. Protect Your Personal Information Never give personal
                information, such as: your social security number, credit card number or bank information, or your work or home address to
                people you don’t know or haven’t met in person. Note: {keys.AppName} will never send you an email asking for your username
                and password information. Any such communication should be reported immediately. Be Web Wise Block and report suspicious
                users. You can block and report concerns about any suspicious user anonymously at any time on {keys.AppName} – while swiping
                or after you’ve matched. Keep conversations on the platform. Bad actors will try to move the conversation to text, personal
                email or phone conversations. Report All Suspicious Behavior Additionally, please report anyone who violates our terms of
                use. Examples of terms of use violations include: Asks you for money or donations Requesting photographs. Minors using the
                platform Users sending harassing or offensive messages Users behaving inappropriately after meeting in person Fraudulent
                registration or profiles. Spam or solicitation, such as invitations to call 1-900 numbers or attempts to sell products or
                services. Offline Behavior First meetings are exciting, but always take precautions and follow these guidelines to help you
                stay safe: Get to Know the Other Person Keep your communications limited to the platform and really get to know users
                online/using the app before meeting them in person. Bad actors often push people to communicate off the platform
                immediately. It’s up to you to research and do your due diligence. Always Meet and Stay in Public Meet for the first time in
                a populated, public place – never in a private or remote location, and never at your home or apartment. If your date
                pressures you, end the date and leave at once. Tell Your Friends and Family Members of Your Plans Inform a friend or family
                member of your plans and when and where you’re going. Make sure you have your cell phone charged and with you at all times.
                Transport Yourself to and from the Meeting You need to be independent and in control of your own transportation, especially
                in case things don’t work out. Stay Sober Consumption of alcohol and/or other drugs can impair your judgment and potentially
                put you in danger. It’s important to keep a clear mind and avoid anything that might place you at risk. Be aware that bad
                actors might try to take advantage of you by altering your beverage(s) with synthetic substances. Health {keys.AppName}{" "}
                welcomes everyone and empowers our community of users to create and cultivate relationships. An important aspect of any
                healthy relationship though – whether formed on {keys.AppName} or otherwise – is ensuring proper sexual health and safety.
                And as a member of the {keys.AppName} community it is your responsibility to make sure you do the following, if you choose
                to engage in sexual activity. Protect Yourself. You and your partner should use proper protection. Condoms and other
                mechanisms can significantly reduce the risk of contracting or passing on an STI, such as HIV. To be effective, however,
                protective measures must be used consistently. Please keep in mind, you can still get certain STIs, like herpes or HPV from
                contact with your partner’s skin even when using a condom. Be Open and Honest It is completely reasonable to have a
                conversation with your partner regarding sex and sexual contact before actually having it. All issues ranging from the
                number of partners each of you has had, to the last time each of you was tested for STIs are fair game. Many STIs are
                curable or treatable. If either you or your partner has an STI that is curable, you both need to start treatment to avoid
                becoming re-infected. It is important to be completely honest in these conversations. Vaccinate The risk of contracting some
                STIs can be reduced through vaccination. Talk to your doctor or a professional at a sexual health clinic to learn more.`
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default CookiePolicy;
