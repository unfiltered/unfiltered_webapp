// Main React Components
import React, { useState, useEffect } from "react";
import ReactPlayer from "react-player";
import Viewer from "react-viewer";
import "./OtherUserProfile.scss";

function ProfileInput({ otheruserdata }) {
  const [visible, setVisible] = React.useState(false);
  const [imageUrl, setUrl] = useState("");
  const [images, setImagesArr] = useState([]);

  useEffect(() => {
    let img = otheruserdata && otheruserdata.profilePic;
    let otherImg = otheruserdata && otheruserdata.otherImages;
    try {
      if (otherImg === undefined || !("otherImages" in otheruserdata)) {
        setImagesArr([img]);
      } else {
        let findIndex = otherImg.findIndex((k) => k === img);
        if (findIndex > -1) {
          setImagesArr(otherImg);
        } else {
          setImagesArr([img, ...otherImg]);
        }
      }
    } catch (e) {}
  }, []);

  return (
    <div className="col-12">
      <div className="row">
        {otheruserdata && otheruserdata.profileVideo.length > 0 ? (
          <div className="other_images_preview_video">
            <ReactPlayer url={otheruserdata && otheruserdata.profileVideo} playing={true} controls={true} />
          </div>
        ) : (
          <span />
        )}
        {/* <div className="other_images_preview">
          <img src={otheruserdata && otheruserdata.profilePic} alt={otheruserdata.firstName} title={otheruserdata.firstName} />
        </div> */}
        {images && images.length > 0 ? (
          images.map((k, i) => (
            <div
              className="other_images_preview"
              key={i}
              onClick={() => {
                setUrl(k);
                setVisible(!false);
              }}
            >
              <img src={k} style={{ objectFit: "cover" }} alt={otheruserdata.firstName} title={otheruserdata.firstName} />
            </div>
          ))
        ) : (
          <span />
        )}
      </div>
      {images.length > 0 ? (
        <Viewer
          className="viewer"
          drag={false}
          visible={visible}
          onClose={() => {
            setVisible(false);
          }}
          images={images}
        />
      ) : (
        <span />
      )}
    </div>
  );
}

export default ProfileInput;
