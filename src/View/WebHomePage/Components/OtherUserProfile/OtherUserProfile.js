// Main React Components
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import moment from "moment";
// Scss
import "./OtherUserProfile.scss";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
// Reusable Components
import Snackbar from "../../../../Components/Snackbar/Snackbar";

// imported Components
import ProfileInput from "./ProfileInput";
import { getCookie, setCookie } from "../../../../lib/session";
// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import CButton from "../../../../Components/Button/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
// Redux Components
import { connect } from "react-redux";
import {
  NearbyPeople,
  getCommentsForSelectedPost,
  ReportUser,
  OtherUserProfile,
  BlockUser,
  UnfriendUser,
  GiveThumbsUp,
  ReasonblockUser,
  LikeNearPeople,
  SuperLikeNearPeople,
  unmatchuser,
  MyDisLikeUser,
  SuperLikeUser,
} from "../../../../controller/auth/verification";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import MainModal from "../../../../Components/Model/model";
import ChatIcon from "../../../../asset/images/chatIcon.svg";
import More from "../../../../asset/images/more.svg";
import * as actionChat from "../../../../actions/chat";
import { Images } from "../../../../Components/Images/Images";
import { Icons } from "../../../../Components/Icons/Icons";
import Moments from "../UserProfile/UserMoments";
import { FormattedMessage } from "react-intl";
import MaterialDialog from "../../../../Components/Dialog/MaterialDialog";
import { __callInit } from "../../../../actions/webrtc";
import { keys } from "../../../../lib/keys";

const styles = (theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

let userid = "";
const ITEM_HEIGHT = 48;

class Otheruserprofile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatdrawer: false,
      loader: true,
      Userreport: "",
      Userreportmessage: "",
      otheruserfinaldata: "",
      reportopen: false,
      blockreason: "",
      opensnakbar: false,
      togglemodel: "",
      reportedUserReason: "",
      subDialog: false,
      allDiscoverPeople: "",
      changeInProfile: false,
      currentIndex: "",
      commentsSection: false,
      postDetails: "",
      comments: "",
      userIdOfCurrentUser: "",
    };
    this.togglemodel = this.togglemodel.bind(this);
  }

  /** open comments modal with details filled in it. non-editable */
  openPost = (details) => {
    this.setState({ postDetails: details, commentsSection: true }, () => {
      this.getCommentsFunc(details.postId);
    });
  };

  closePost = () => {
    this.setState({ postDetails: "", commentsSection: false, comments: "" });
  };

  getCommentsFunc = (navPostId) => {
    getCommentsForSelectedPost(getCookie("token"), navPostId)
      .then((res) => {
        this.setState({ comments: res.data.data });
      })
      .catch((err) => console.log("err"));
  };

  componentDidMount() {
    let urlParts = window.location.href.split("/");
    console.log("Urlid", urlParts[urlParts.length - 1]);
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    let userid = urlParts[urlParts.length - 1];
    this.getUserInfoOnInit(userid);
    this.setState({ loader: true });

    this.handleblockreason();
  }

  getUserInfoOnInit = (userid) => {
    OtherUserProfile({ targetUserId: userid })
      .then((data) => {
        console.log("current user data", data.data.data);
        this.setState({
          otheruserfinaldata: data.data.data,
          loader: false,
        });
        this.props.selectedProfileForChat(data.data.data);
      })
      .catch(() => {
        this.setState({ loader: false });
      });
  };

  static getDerivedStateFromProps(props, state) {
    return {
      allDiscoverPeople: props.discoverPeopleList,
    };
  }

  navToNextScreen = (fn, id) => {
    this.props.history.push(`/app/user/${fn}/${id}`);
    setCookie("nextUserId", id);
    OtherUserProfile({ targetUserId: id }).then((data) => {
      this.setState({
        otheruserfinaldata: data.data.data,
        loader: false,
      });
      this.props.selectedProfileForChat(data.data.data);
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentIndex !== this.state.currentIndex && this.state.currentIndex < this.state.allDiscoverPeople.length) {
      this.navToNextScreen(
        this.state.allDiscoverPeople[this.state.currentIndex].firstName,
        this.state.allDiscoverPeople[this.state.currentIndex].opponentId
      );
    }
  }

  // Function for the Toggle Error Model
  togglemodel() {
    this.setState((prevState) => ({
      togglemodel: !prevState.togglemodel,
    }));
  }

  giveThumbsUp = (targetId) => {
    GiveThumbsUp(getCookie("token"), targetId)
      .then((res) => {
        if (res.status === 409) {
          this.setState({
            usermessage: `You have already sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            open: true,
          });
        }
        if (res.status === 200) {
          let userData = { ...this.props.selectedProfile };
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userData.isThumbsUpProfile = 1;
          userDataFromState.isThumbsUpProfile = 1;
          this.setState({ otheruserfinaldata: userDataFromState });
          this.props.selectedProfileForChat(userData);
          console.log("on success", userData);
          this.setState({
            usermessage: `You have sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            open: true,
          });
        }
        // setTimeout(() => {
        //   this.setState({ open: false });
        // }, 2000);
      })
      .catch((err) => console.log("[thumbs up fail]", err));
  };

  // Used to get Reason to Reason User Data
  handleblockreason = () => {
    ReasonblockUser().then((data) => {
      this.setState({ blockreason: data.data.data });
    });
  };

  // To Open Menu
  handleClickmenu = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // To Close Menu
  handleClosemenu = () => {
    this.setState({ anchorEl: null });
  };

  // Used to Open Report User Dialog
  openReportModal = () => {
    this.setState({ reportopen: true });
  };

  // Used to Close Report User Dialog
  closeReportModal = () => {
    this.setState({ reportopen: false });
  };

  // To Open a Chat Drawer
  openchatDrawer = () => {
    let userData = { ...this.props.selectedProfile };
    userData.initiated = true;
    this.props.selectedProfileForChat(userData);
    this.props.CoinBalance > 0 ? this.setState({ chatdrawer: true }) : this.togglemodel();
  };

  // Function for the Notification ( Snackbar )
  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // To Close a Chat Drawer
  closechatDrawer = () => {
    this.setState({ chatdrawer: false });
  };

  // Fumction for Report Module
  handlereasonuser = (event) => {
    this.setState({ Userreport: event.target.value });
  };

  // Function for Message Module
  handlemesuser = (event) => {
    this.setState({ Userreportmessage: event.target.value });
  };

  getReasonForReport = (data) => {
    this.setState({ reportedUserReason: data });
    this.closeReportModal();
    this.toggleSubDialog();
  };

  unfriendUser = (targetUserId) => {
    UnfriendUser(getCookie("token"), targetUserId)
      .then((res) => {
        this.setState({ usermessage: "UnFriend user successfull", variant: "success", open: true, anchorEl: null });
      })
      .catch((err) => {
        console.log("error unfriending user");
      });
    setTimeout(() => {
      this.setState({ open: false, usermessage: "" });
    }, 2000);
  };

  // API Call for to Report a User
  handlereportuser = (data) => {
    let UserReportPayload = {
      targetUserId: userid,
      reason: data,
      message: "Report",
    };

    ReportUser(UserReportPayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: `${this.props.selectedProfile.firstName} Reported successfully`,
        });
        this.closeReportModal();
        this.handleClosemenu();
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
        this.closeReportModal();
        this.handleClosemenu();
      });
  };

  // API Call for to Block a User
  BlockUserFn = (targetUserId) => {
    this.setState({ loader: true });
    BlockUser(targetUserId)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: `${this.props.selectedProfile.firstName} Blocked successfully.`,
          loader: false,
        });
        this.handleClosemenu();
      })
      .catch((err) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
        this.handleClosemenu();
      });
  };
  //UserProfile
  // API to like User
  likeUser = (userid) => {
    let index = this.state.allDiscoverPeople.findIndex((k) => k.opponentId === userid);
    if (this.props.UserProfile.remainsLikesInString > 0 || this.props.UserProfile.remainsLikesInString === "unlimited") {
      LikeNearPeople({ targetUserId: userid })
        .then((data) => {
          let userData = { ...this.props.selectedProfile };
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userData.liked = 1;
          userDataFromState.liked = 1;
          this.setState({
            otheruserfinaldata: userDataFromState,
            open: true,
            variant: "success",
            usermessage: "Profile has been liked successfully",
          });
          if (index > -1) {
            this.setState({ currentIndex: index + 1 });
          }
          setTimeout(() => {
            this.setState({ changeInProfile: false });
          }, 800);
          setTimeout(() => {
            this.setState({ open: false });
          }, 2000);
        })
        .catch((error) => {
          if (error.status === 405) {
            this.setState({
              open: true,
              variant: "error",
              usermessage: `You have already superliked ${this.state.otheruserfinaldata.firstName}`,
            });
          } else {
            this.setState({
              open: true,
              variant: "error",
              usermessage: "You",
            });
          }
          setTimeout(() => {
            this.setState({ open: false });
          }, 2000);
        });
    } else {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Your Likes count has exhausted",
      });
    }
    setTimeout(() => {
      this.setState({ open: false });
    }, 2000);
  };
  // API to superlike User
  superLikeUser = (userid) => {
    this.setState({ changeInProfile: true });
    let index = this.state.allDiscoverPeople.findIndex((k) => k.opponentId === userid);
    SuperLikeUser({ targetUserId: userid }, getCookie("token"))
      .then((data) => {
        console.log("data", data);
        if (data.status === 200) {
          let userData = { ...this.props.selectedProfile };
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userData.superliked = 1;
          userDataFromState.superliked = 1;

          if (
            this.props.checkIfUserIsProUser &&
            this.props.checkIfUserIsProUser.ProUserDetails &&
            this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
            this.props.UserCoin > 0
          ) {
            this.props.coinReduce(this.props.superLikeCoinConfig.Coin);
          }

          this.setState({
            otheruserfinaldata: userDataFromState,
            open: true,
            variant: "success",
            usermessage: "Superliked successfull",
          });
          if (index > -1) {
            this.setState({ currentIndex: index + 1 });
          }
          setTimeout(() => {
            this.setState({ changeInProfile: false });
          }, 800);
          setTimeout(() => {
            this.setState({ open: false });
          }, 2000);
        } else {
          this.setState({ open: true, usermessage: data.data.message, variant: "error" });
        }
      })
      .catch((error) => {
        console.log("error", error);
        if (error.status === 405) {
          this.setState({
            open: true,
            variant: "error",
            usermessage: `You have already liked ${this.state.otheruserfinaldata.firstName}`,
          });
        } else {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Something went wrong.",
          });
        }
        setTimeout(() => {
          this.setState({ open: false });
        }, 1300);
      });
  };
  unmatchuser = (userid) => {
    unmatchuser(userid)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "UnMatch Successfull",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "You Want Coin to Superlike User.",
        });
      });
  };
  // API to match User

  toggleSubDialog = () => {
    this.setState({ subDialog: !this.state.subDialog });
  };

  closeConfirmationDialogWithMainDialog = () => {
    this.toggleSubDialog();
    this.setState({
      currentIndex: this.state.currentIndex + 1,
      changeInProfile: true,
    });
    this.handlereportuser(this.state.reportedUserReason);
    setTimeout(() => {
      this.closeReportModal();
    }, 500);
    setTimeout(() => {
      this.setState({ changeInProfile: false });
    }, 300);
  };

  recommendFriend = () => {
    navigator.clipboard.writeText(`Recommend your friend ${window.location.href}`);
    this.setState({
      open: true,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends now",
    });
    this.handleClosemenu();
    setTimeout(() => {
      this.setState({ open: false });
    }, 2000);
  };

  dislikeUser = (userId) => {
    let index = this.state.allDiscoverPeople.findIndex((k) => k.opponentId === userId);
    MyDisLikeUser({
      targetUserId: userId,
    })
      .then((data) => {
        this.setState({
          open: true,
          changeInProfile: true,
          variant: "success",
          usermessage: "UserName Passed successfully.",
        });
        if (index > -1) {
          this.setState({ currentIndex: index + 1 });
        }
        setTimeout(() => {
          this.setState({ changeInProfile: false });
        }, 800);
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  render() {
    const { otheruserfinaldata, anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const { classes } = this.props;
    return (
      <div>
        {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div className="col-12 Wotheruser_Scroll pl-5 border-left border-right mt-5" id="innerVideoContent">
            <div className="otheruser_profile">
              {/* Header Module */}
              <div className="pb-3 pt-5 align-items-center row header" style={{ position: "sticky" }}>
                <div className="col-4 user_headername">
                  <div className="row align-items-center">
                    <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={() => this.props.history.goBack()}>
                      <img src={require("../../../../asset/images/black-left-arrow.svg")} width={20} height={20} alt="left-arrow" />
                    </div>
                    <div className="ml-4">
                      {otheruserfinaldata.onlineStatus === 1 ? <div className="online"></div> : <div className="offline"></div>}
                    </div>
                    <h3 className="m-0 ml-3 pt-1 name">
                      {otheruserfinaldata.firstName},{" "}
                      {otheruserfinaldata.age && otheruserfinaldata.age.isHidden === 1
                        ? ""
                        : otheruserfinaldata.age
                        ? otheruserfinaldata.age.value
                        : ""}
                    </h3>
                  </div>
                </div>
                <div className="col-4">
                  <div className="row justify-content-center align-items-center">
                    <div className="buttons ml-3" onClick={() => this.dislikeUser(otheruserfinaldata._id)}>
                      <img src={Icons.NewDislike} width={65} height={65} alt="dislike" />
                    </div>
                    <div className="buttons" onClick={() => this.likeUser(otheruserfinaldata._id)}>
                      <img src={Icons.NewLike} width={65} height={65} alt="like" />
                    </div>
                    <div className="buttons mr-3" onClick={() => this.superLikeUser(otheruserfinaldata._id)}>
                      <img src={Icons.NewSuperLike} width={65} height={65} alt="like" />
                    </div>
                  </div>
                </div>
                {/* <div className="col-4">
                  {otheruserfinaldata.isMatched || otheruserfinaldata.isFriend ? (
                    <div className="row justify-content-center align-items-center">
                      {otheruserfinaldata.isThumbsUpProfile === 1 ? (
                        <>
                          <div className="alredyEnabled_button" onClick={() => this.giveThumbsUp(otheruserfinaldata._id)}>
                            <img src={Icons.alreadyThumbsUpped} width={22} height={22} alt="other-thumbs-up" />
                          </div>
                          {otheruserfinaldata.isFriend === 1 || otheruserfinaldata.friend === 1 ? (
                            <div className="buttons" onClick={() => this.props.__callInit(true)}>
                              <img src={Icons.FaceTime} width={25} height={25} alt="video-call" />
                            </div>
                          ) : (
                            ""
                          )}
                        </>
                      ) : (
                        <>
                          <div className="buttons" onClick={() => this.giveThumbsUp(otheruserfinaldata._id)}>
                            <img src={Icons.otherUsersThumbsUp} width={22} height={22} alt="other-thumbs-up" />
                          </div>
                          {otheruserfinaldata.isFriend === 1 || otheruserfinaldata.friend === 1 ? (
                            <div className="buttons" onClick={() => this.props.__callInit(true)}>
                              <img src={Icons.FaceTime} width={25} height={25} alt="video-call" />
                            </div>
                          ) : (
                            ""
                          )}
                        </>
                      )}
                    </div>
                  ) : (
                    <div className="row justify-content-center align-items-center">
                      {otheruserfinaldata.liked === 1 ? (
                        <div className="alredyEnabled_button">
                          <img src={Icons.otherUsersAlreadyLiked} width={25} height={25} alt="already-liked" />
                        </div>
                      ) : (
                        <div className="buttons" onClick={() => this.likeUser(otheruserfinaldata._id)}>
                          <img src={Images.otherProfileLike} width={25} height={25} alt="like" />
                        </div>
                      )}
                      <div className="buttons" onClick={() => this.dislikeUser(otheruserfinaldata._id)}>
                        <img src={Images.otherProfileDislike} width={25} height={25} alt="dislike" />
                      </div>
                      {otheruserfinaldata.superliked === 1 ? (
                        <div className="alredyEnabled_button">
                          <img src={Icons.alreadySuperLiked} width={25} height={25} alt="already-superliked" />
                        </div>
                      ) : (
                        <div className="buttons" onClick={() => this.superLikeUser(otheruserfinaldata._id)}>
                          <img src={Images.otherProfileSuperlike} width={22} height={22} alt="superlike" />
                        </div>
                      )}
                      {otheruserfinaldata.isThumbsUpProfile === 1 ? (
                        <div className="alredyEnabled_button" onClick={() => this.giveThumbsUp(otheruserfinaldata._id)}>
                          <img src={Icons.alreadyThumbsUpped} width={22} height={22} alt="other-thumbs-up" />
                        </div>
                      ) : (
                        <div className="buttons" onClick={() => this.giveThumbsUp(otheruserfinaldata._id)}>
                          <img src={Icons.otherUsersThumbsUp} width={22} height={22} alt="other-thumbs-up" />
                        </div>
                      )}
                    </div>
                  )}
                </div> */}
                <div className="col-4 text-right">
                  <div className="userprofile_setting">
                    {this.props.CoinBalance ? (
                      <Link to="/app/chat" onClick={this.openchatDrawer}>
                        <img src={ChatIcon} alt="chat-icon" width={20} height={20} className="mr-2" />
                        <span>
                          <FormattedMessage id="message.chatNow" />
                        </span>
                      </Link>
                    ) : (
                      <Link onClick={this.openchatDrawer}>
                        <img src={ChatIcon} alt="chat-icon" width={20} height={20} className="mr-2" />
                        <span>
                          <FormattedMessage id="message.chatNow" />
                        </span>
                      </Link>
                    )}

                    <IconButton
                      aria-label="More"
                      aria-owns={open ? "long-menu" : undefined}
                      aria-haspopup="true"
                      onClick={this.handleClickmenu}
                      style={{ color: "f#74167" }}
                    >
                      <img src={More} alt="more" width={20} height={20} />
                    </IconButton>
                    <Menu
                      id="long-menu"
                      anchorEl={anchorEl}
                      open={open}
                      className="d_oup_menu"
                      onClose={this.handleClosemenu}
                      PaperProps={{
                        style: {
                          maxHeight: ITEM_HEIGHT * 6,
                          width: 200,
                        },
                      }}
                    >
                      <MenuItem onClick={this.recommendFriend}>
                        <p className="m-0">
                          <FormattedMessage id="message.recommendFriend" />
                        </p>
                      </MenuItem>
                      <MenuItem onClick={this.openReportModal}>
                        <p className="m-0">
                          <FormattedMessage id="message.report" /> {otheruserfinaldata.firstName}
                        </p>
                      </MenuItem>
                      {/* <MenuItem onClick={() => this.BlockUserFn(this.props.selectedProfile._id)}>
                        <p className="m-0 text-danger">
                          <FormattedMessage id={otheruserfinaldata.blocked === 1 ? "message.youBlocked" : "message.block"} /> {otheruserfinaldata.firstName}
                        </p>
                      </MenuItem> */}
                      {otheruserfinaldata && otheruserfinaldata.isMatched ? (
                        <MenuItem onClick={() => this.unmatchuser(otheruserfinaldata._id)}>
                          <p className="m-0">
                            <FormattedMessage id="message.unMatch" /> {otheruserfinaldata.firstName}
                          </p>
                        </MenuItem>
                      ) : (
                        ""
                      )}
                      {otheruserfinaldata && otheruserfinaldata.isFriend ? (
                        <MenuItem onClick={() => this.unfriendUser(otheruserfinaldata._id)}>
                          <p className="m-0">
                            <FormattedMessage id="message.unfriend" /> {otheruserfinaldata.firstName}
                          </p>
                        </MenuItem>
                      ) : (
                        ""
                      )}
                    </Menu>
                  </div>
                </div>
              </div>

              {/* OtherUser Slider Photo's Module */}
              <div className="otheruser_profile">
                <ProfileInput otheruserdata={this.state.otheruserfinaldata} />
              </div>

              {/* OtherUser Profile Location Module */}
              {otheruserfinaldata.distance && otheruserfinaldata.distance.isHidden === 1 ? (
                ""
              ) : (
                <div className="m-0 py-3 d-block row border-bottom">
                  <div className="userpro_header py-1">
                    <h5>
                      <FormattedMessage id="message.userlocation" /> :
                    </h5>
                  </div>
                  <div className="user_answer py-1">
                    <p className="m-0">
                      <i className="fas fa-map-marker-alt pr-2" style={{ color: "#e31b1b" }} />
                      {otheruserfinaldata.distance ? parseFloat(otheruserfinaldata.distance.value / 1.6).toFixed(2) : ""}{" "}
                      <FormattedMessage id="Miles Away" />.
                    </p>
                  </div>
                  <div className="user_answer py-1">
                    <p className="m-0">
                      <span style={{ color: "#484848", fontFamily: "Circular Air Book" }}>{keys.AppName} ID: </span>
                      <span className="pl-1" style={{ color: "#e31b1b", fontFamily: "Circular Air Bold" }}>
                        {otheruserfinaldata.findMateId}
                      </span>
                    </p>
                  </div>
                </div>
              )}

              {/* OtherUser Profile Details Module */}
              <div className="m-0 py-3 d-block row border-bottom">
                <div className="userpro_header">
                  <h5>
                    <FormattedMessage id="message.userdetails" /> :
                  </h5>
                </div>
                {/* OtherUser Profile Name Module */}
                <div className="pt-2 user_answer">
                  <div className="m-0 row">
                    <div className="p-0 py-1 col-4 coi-sm-4 col-md-4 col-lg-3 col-xl-3 userpro_header">
                      <p className="m-0 label">
                        <FormattedMessage id="message.name" /> :
                      </p>
                    </div>
                    <div className="p-0 py-1 text-left col-8 coi-sm-8 col-md-8 col-lg-7 col-xl-7 user_answer">
                      <p className="m-0">{otheruserfinaldata.firstName}</p>
                    </div>
                  </div>
                </div>

                {/* OtherUser Profile Gender Module */}
                <div className="pt-2 py-1 user_answer">
                  <div className="m-0 row">
                    <div className="p-0 py-1 col-4 col-sm-4 col-md-4 col-lg-3 col-xl-3 userpro_header">
                      <p className="m-0 label">
                        <FormattedMessage id="message.gender" /> :
                      </p>
                    </div>
                    <div className="p-0 py-1 text-left col-8 coi-sm-8 col-md-8 col-lg-7 col-xl-7 user_answer">
                      <p className="m-0">{otheruserfinaldata.gender}</p>
                    </div>
                  </div>
                </div>
              </div>

              {/* OtherUser Prefence Module */}
              {this.state.otheruserfinaldata &&
                this.state.otheruserfinaldata.myPreferences &&
                this.state.otheruserfinaldata.myPreferences.map((data, index) => (
                  <div key={index} className="m-0 py-3 border-bottom">
                    {/* OtherUser Prefrence Header Module */}
                    <div className="userpro_header py-1">
                      <h5>{data.title} :</h5>
                    </div>
                    {data.data.map((pref, data) => (
                      // Otheruser prefrence Body Module
                      <div key={"OtherUserProfile" + data} className="py-2 user_answer">
                        <div className="m-0 row">
                          <div className="p-0 col-4 py-1 col-sm-4 col-md-4 col-lg-3 col-xl-3 userpro_header">
                            <p className="m-0 label">{pref.label} :</p>
                          </div>
                          <div className="p-0 text-left py-1 col-8 coi-sm-8 col-md-8 col-lg-7 col-xl-7 user_answer">
                            <p className="m-0">
                              {pref.selectedValues && pref.selectedValues.length > 0 ? (
                                pref.selectedValues.join(",")
                              ) : (
                                <FormattedMessage id="message.notAvailable" />
                              )}
                            </p>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                ))}

              {/** Moments */}
              {this.state.otheruserfinaldata.moments.length > 0 ? (
                <div className="m-0 py-3 border-bottom">
                  <div className="userpro_header">
                    <h5>
                      <FormattedMessage id="message.moments" /> :
                    </h5>
                  </div>
                  <Moments UserFinalData={this.state.otheruserfinaldata} openPost={this.openPost} isOtherUser={true} />
                </div>
              ) : (
                <span />
              )}
            </div>

            {/* <MatUiModal toggle={toggleLikesSection} isOpen={likesSection} width={500}>
              <div className="col-12 text-center likesModal p-4" style={{ position: "relative" }}>
                <div className="d_newsfeed_modal_closeBtn" onClick={toggleLikesSection}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
                </div>
                <div className="d_newsFeed_header">Likes</div>
                <div className="d_comments_section">
                  {allLikers.length > 0 ? (
                    allLikers.map((k) => (
                      <div>
                        <div className="col-12 px-0 d-flex comments">
                          <div className="post_image">
                            <img src={k.profilePic} alt={k.likersName} />
                          </div>
                          <div className="d-flex align-items-center">
                            <div>{k.likersName}</div>
                          </div>
                        </div>
                      </div>
                    ))
                  ) : (
                    <div className="col-12">
                      <div>Oops! You haven't got any likes yet.</div>
                    </div>
                  )}
                </div>
              </div>
            </MatUiModal> */}
            <MatUiModal toggle={this.openPost} isOpen={this.state.commentsSection} width={"75%"} toggle={this.closePost}>
              <div className="col-12 px-0 commentsModal">
                <div className="d_oup_moments_closeBtn" onClick={this.closePost}>
                  <span>
                    <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
                  </span>
                </div>
                <div className="row mx-0">
                  <div className="col-7 px-0 d_post_comment_image_section">
                    {this.state.postDetails && this.state.postDetails.url[0].includes("mp4") ? (
                      <video width={"100%"} height={600} controls loop autoPlay>
                        <source src={this.state.postDetails && this.state.postDetails.url[0]} type="video/mp4" />
                      </video>
                    ) : (
                      <img
                        src={this.state.postDetails && this.state.postDetails.url[0]}
                        alt={this.state.postDetails.description}
                        width={"100%"}
                        height={"100%"}
                      />
                    )}
                  </div>
                  <div className="col-5 px-0 d_post_comment_section">
                    <div className="p-3">
                      <div>“{this.state.postDetails.description}”</div>
                      <div className="otherUsersProfile_likes_n_comment">
                        <span>
                          <img
                            src={this.state.postDetails.liked ? Icons.likedLogo1 : Icons.likedLogo}
                            height={23}
                            width={23}
                            alt="Heart-Outline"
                          />
                        </span>
                        {this.state.postDetails.likeCount} Like(s) {this.state.comments.length} Comment(s)
                      </div>
                    </div>
                    <div>
                      {this.state.comments.length > 0 ? (
                        this.state.comments
                          .map((k) => (
                            <div className="py-2 px-3" key={k._id}>
                              <div className="col-12 px-0 d-flex comments">
                                <div className="post_image">
                                  <img src={k.profilePic} alt={k.commenterName} />
                                </div>
                                <div>
                                  <div>{k.commenterName}</div>
                                  <div>{k.comment}</div>
                                  <div className="comment_post_date">{moment(k.commentedOn).fromNow()}</div>
                                </div>
                                <div className="pl-2">{}</div>
                                <div></div>
                              </div>
                            </div>
                          ))
                          .reverse()
                      ) : (
                        <div className="px-2">
                          <div>No Comments.</div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                {/* <div className="d_newsFeed_header">Comments</div> */}
              </div>
            </MatUiModal>

            <MaterialDialog
              open={this.state.reportopen}
              closeModal={this.closeReportModal}
              title={<FormattedMessage id="message.reportUser" />}
            >
              <DialogContent className="py-0 px-3">
                <DialogContentText id="alert-dialog-description">
                  {this.state.blockreason
                    ? this.state.blockreason.map((data, index) => (
                        <div
                          style={{ cursor: "pointer" }}
                          key={index}
                          className="py-2 d-block text-center border-bottom"
                          onClick={() => this.getReasonForReport(data)}
                        >
                          {data}
                        </div>
                      ))
                    : ""}
                </DialogContentText>
              </DialogContent>
              <DialogActions className="py-2 m-auto">
                <Button className="text-center" onClick={this.closeReportModal} color="primary" autoFocus>
                  <FormattedMessage id="message.cancel" />
                </Button>
              </DialogActions>
            </MaterialDialog>

            {/** opens this dialog modal when selecting option of report user */}
            <MaterialDialog
              open={this.state.subDialog}
              closeModal={this.toggleSubDialog}
              title={`Are you sure, You want to report ${otheruserfinaldata.firstName}`}
            >
              <DialogActions className="py-2 m-auto">
                <Button className="text-center" onClick={this.closeConfirmationDialogWithMainDialog} color="primary" autoFocus>
                  <FormattedMessage id="message.yes" />
                </Button>
                <Button className="text-center" onClick={this.toggleSubDialog} color="primary" autoFocus>
                  <FormattedMessage id="message.no" />
                </Button>
              </DialogActions>
            </MaterialDialog>

            {/* Error Message For not Resgister Number Model */}
            <MainModal isOpen={this.state.togglemodel} type="ActivedPlanModal">
              <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div className="pt-3 Werror_signin">
                  <i className="far fa-times-circle" onClick={this.togglemodel} />
                  <div className="WCharge_coins">
                    <i className="fas fa-coins" />
                  </div>
                  <h4>Insufficient Coin Balance.</h4>
                  <p className="m-0">You need at least 25 coin to chat with user and your current balance is {this.props.CoinBalance}.</p>
                  <CButton className="OUP_Wcharge_wallet" text="Buy Coins" handler={() => this.props.history.push("/app/coinbalance")} />
                </div>
              </div>
            </MainModal>

            {/* Snakbar Components */}
            <Snackbar
              timeout={2500}
              type={this.state.variant}
              message={this.state.usermessage}
              open={this.state.open}
              onClose={this.handleClosesnakbar}
            />
          </div>
        )}
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    discoverPeopleList: state.discoverPeople.discoverPeopleList,
    selectedProfile: state.selectedProfile.selectedProfile.selectedProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    superLikeCoinConfig: state.CoinConfig.CoinConfig.superLike,
    readOnlyProfile: state.UserSelectedToChat.readOnlyUser,
    checkIfUserIsProUser: state.ProUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
    __callInit: (data) => dispatch(__callInit(data)),
  };
}

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Otheruserprofile)));
