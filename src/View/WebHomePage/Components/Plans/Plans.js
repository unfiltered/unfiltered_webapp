import React, { Component } from "react";
import { connect } from "react-redux";
import "./Plans.scss";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
import { BuyNewPlan } from "../../../../controller/auth/verification";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

class plans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planupgrade: "",
      coinupgrade: "",
      loader: true,
    };
  }

  componentDidMount() {
    BuyNewPlan().then((data) => {
      this.setState({ planupgrade: data.data.data });
    });
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 1200);
  }

  render() {
    const { classes } = this.props;

    const Coinbalance = this.state.planupgrade
      ? this.state.planupgrade.map((data) => (
          <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
            <div className="upgrade_balance">
              <div className="plan_media">
                <i className="fas fa-coins" />
              </div>
              <div className="plan_data">
                <p className="m-0">Period : {data.description}</p>
                <p className="m-0">Rewind Count : {data.rewindCount}</p>
                <p className="m-0">SuperLike Count : {data.superLikeCount ? data.superLikeCount : "None"}</p>
              </div>
              <div className="plan_amount">
                <p className="m-0">Cost : {data.baseCurrencySymbol + "  " + data.cost}</p>
              </div>
              <div className="plan_buy">
                <button>Buy Now</button>
              </div>
            </div>
          </div>
        ))
      : "";

    return (
      <div>
        {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div>
            {/* Header Module */}
            <div className="WCommonbox">
              {/* Meet New People Navbar Start */}
              <div className="py-sm-2">
                <div className="m-0 row">
                  <div className="pt-sm-2 pt-md-2 pt-lg-2 col-sm-8 col-md-9 col-lg-10 text-left Wcommonheading">
                    <div className="pl-2">
                      <p className="m-0 text-white">Plans</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Coin Module */}
            <div className="col-lg-12 col-xs-12 cmn_container">
              {/* Heading Module */}
              <div className="py-3 text-center">
                <h3>Top up now – the more Plans you buy, the cheaper they Are :</h3>
              </div>

              {/* Coin-Data Module */}
              <div className="pt-3 m-0 row">{Coinbalance}</div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currencySymbol: state.UserProfile.currencySymbol,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(plans));
