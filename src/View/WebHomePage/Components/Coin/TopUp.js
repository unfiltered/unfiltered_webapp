import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./Coin.scss";
import { BuyNewCoins, GetWalletData, CurrentCoinBalance } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { Icons } from "../../../../Components/Icons/Icons";
import moment from "moment";
import { Images } from "../../../../Components/Images/Images";
import { FormattedMessage } from "react-intl";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir}>
      {children}
    </Typography>
  );
}

const styles = (theme) => ({
  root: {
    backgroundColor: "white",
    width: "inherit",
  },
  tabsRoot: {
    borderBottom: "none",
  },
  tabsIndicator: {
    borderBottom: "2px solid #f80402",
    color: "#f80402",
  },
  labelActive: {
    color: "#f80402",
    fontSize: "16px",
    fontFamily: "Product Sans",
    textTransform: "capitalize",
  },
  labelNotActive: {
    color: "#484848",
    fontSize: "16px",
    fontFamily: "Product Sans",
    textTransform: "capitalize",
  },
  tabRoot: {
    textTransform: "initial",
    fontWeight: 100,
    color: "#C8CFE0",
    fontFamily: "Product Sans",
    fontSize: "18px",
    "&:hover": {
      color: "#f80402",
    },
    "&$tabSelected": {
      color: "#484848",
      fontFamily: "Product Sans Bold",
      fontWeight: 900,
      borderBottom: "1px solid #f80402",
      fontSize: "18px",
    },
    "&:focus": {
      color: "#f80402",
    },
  },
});

class TopUp extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.inRef = React.createRef();
    this.state = {
      coinData: [],
      allCoinsData: [],
      inData: [],
      outData: [],
      referenceNode: "",
      pageState: "",
      viewMore: false,
      value: 0,
      allCoinsWithoutPageState: false,
    };
  }

  componentDidMount() {
    BuyNewCoins().then((data) => {
      this.setCoinData(data.data.data);
    });
    let token = getCookie("token");
    /** API to get wallet info */
    CurrentCoinBalance(getCookie("uid"), token).then((data) => {
      GetWalletData(data.data.walletData[0].walletid, token, 0, false).then((res) => {
        this.setAllCoinsData(res.data);
        this.setState({ allCoinsWithoutPageState: true });
      });
      // credit
      GetWalletData(data.data.walletData[0].walletid, token, 1).then((res) => {
        this.setInData(res.data);
      });
      // debit
      GetWalletData(data.data.walletData[0].walletid, token, 2).then((res) => {
        this.setOutData(res.data);
      });
    });
    this.paneDidMount(this.myRef.current); // <div>
    this.props.renderBgColorBasedOnLink(window.location.href);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = (index) => {
    this.setState({ value: index }, () => {});
  };

  componentWillUnmount() {
    this.paneDidUnmount(this.myRef.current);
  }

  setCoinData = (data) => {
    this.setState({ coinData: data });
  };
  setAllCoinsData = (data) => {
    this.setState({ allCoinsData: data });
  };
  setInData = (data) => {
    this.setState({ inData: data });
  };
  setOutData = (data) => {
    this.setState({ outData: data });
  };

  /** on scroll, on pagitation the data gets overriden on the active tab */
  setViewMore = (boolean, coinType, cbFucntion, value) => {
    if (boolean && coinType.data.length < coinType.totalCount) {
      GetWalletData(this.props.walletId, getCookie("token"), value, coinType.pageState, this.state.allCoinsWithoutPageState)
        .then((res) => {
          let oldAllData = { ...coinType };
          let oldAllDataArr = [];
          oldAllDataArr.push(...oldAllData.data, ...res.data.data);
          oldAllData["data"] = oldAllDataArr;
          oldAllData["pageState"] = res.data.pageState;
          cbFucntion(oldAllData);
          this.setState({ viewMore: false });
        })
        .catch((err) => this.setState({ viewMore: false }));
    } else {
      this.setState({ viewMore: false });
    }
  };

  /** on scroll the API gets called (pagination) */
  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        if (this.state.value === 0) {
          this.setViewMore(true, this.state.allCoinsData, this.setAllCoinsData, this.state.value);
        } else if (this.state.value === 1) {
          this.setViewMore(true, this.state.inData, this.setInData, this.state.value);
        } else if (this.state.value === 2) {
          this.setViewMore(true, this.state.outData, this.setOutData, this.state.value);
        }
      }
    }
  };

  setReferenceNode = (node) => {
    this.setState({ referenceNode: node });
  };

  paneDidUnmount(node) {
    if (node) {
      node.childNodes[0].children[1].removeEventListener("scroll", this.handleScroll, true);
    }
  }

  /** fn to set ref on <div> on mount */
  paneDidMount = (node) => {
    if (node) {
      node.childNodes[0].children[1].addEventListener("scroll", this.handleScroll, true);
      this.setReferenceNode(node.childNodes[0].children[1]);
    }
  };
  render() {
    let { classes, theme } = this.props;
    // console.log("referenceNode", this.state.referenceNode);
    return (
      <div className="col-12 mainScreen px-0" id="innerVideoContent">
        <div className="col-12 WNavbar_setting px-0">
          <div className="py-4 row align-items-center mx-0">
            <div className="col-6">
              <div className="d-flex align-items-center">
                {/* <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={() => props.history.push("/app")}>
                <img src={require("../../../../asset/images/black-left-arrow.svg")} width={20} height={20} alt="left-arrow" />
              </div> */}
                <div className="pl-3">
                  <div className="topUp_walletBalance">
                    <FormattedMessage id="message.walletBalance" />
                  </div>
                  <div className="topUp_balance d-flex align-items-center">
                    <span>
                      <img src={Icons.BigHeart} height={40} width={40} alt="User" title="User" />
                    </span>
                    <span className="pl-1">
                      {this.props.currentCoinBalance} <FormattedMessage id="message.sidepanel_coins" />
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="px-3 topUp_tabs" ref={this.myRef}>
          <div className={classes.root}>
            <AppBar elevation={0} position="static" color="white">
              <Tabs
                classes={{
                  root: classes.tabsRoot,
                  indicator: classes.tabsIndicator,
                }}
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="secondary"
                textColor="secondary"
                variant="fullWidth"
              >
                <Tab
                  classes={{ label: this.state.value === 0 ? classes.labelActive : classes.labelNotActive }}
                  label={<FormattedMessage id="message.all" />}
                />
                <Tab
                  classes={{ label: this.state.value === 1 ? classes.labelActive : classes.labelNotActive }}
                  label={<FormattedMessage id="message.coinIn" />}
                />
                <Tab
                  classes={{ label: this.state.value === 2 ? classes.labelActive : classes.labelNotActive }}
                  label={<FormattedMessage id="message.coinOut" />}
                />
              </Tabs>
            </AppBar>
            <SwipeableViews
              axis={theme.direction === "rtl" ? "x-reverse" : "x"}
              index={this.state.value}
              onChangeIndex={this.handleChangeIndex}
            >
              <TabContainer dir={theme.direction} id={`sv0`}>
                {this.state.allCoinsData.data && this.state.allCoinsData.data.length > 0 ? (
                  this.state.allCoinsData.data.map((k) => (
                    <div className="col-12 py-4">
                      <div className="row">
                        <div className="col-1 d-flex justify-content-center align-items-center">
                          {k.txntypetext === "DEBIT" ? <div className="debitRound"></div> : <div className="creditRound"></div>}
                        </div>
                        <div className="col-7">
                          <div className="topUp_notes">{k.trigger}</div>
                          <div className="topUp_coins_received">
                            <span>
                              <img src={Icons.BigHeart} height={25} width={25} alt="gold-coin" title="User" />
                            </span>
                            {k.amount} <FormattedMessage id="message.sidepanel_coins" />
                          </div>
                        </div>
                        <div className="col-4 text-center topUpC_time">{moment(k.txntimestamp).format("LLL")}</div>
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="col-12 pt-5">
                    <div className="col-12 text-center">
                      <img src={Icons.ReplacementForCoins} height={250} width={200} alt="no-transactions-found" />
                    </div>
                    <div className="col-12 text-center noCoins_type pt-2">
                      <FormattedMessage id="message.all" />
                    </div>
                    <div className="col-12 text-center noCoins_text pt-2">
                      <FormattedMessage id="message.noTransactionHistory" />.
                    </div>
                  </div>
                )}
              </TabContainer>
              <TabContainer dir={theme.direction} id={`sv1`}>
                {this.state.inData.data && this.state.inData.data.length > 0 ? (
                  this.state.inData.data.map((k) => (
                    <div className="col-12 py-3" ref={this.inRef}>
                      <div className="row">
                        <div className="col-8">
                          <div className="topUp_notes">{k.trigger}</div>
                          <div className="topUp_coins_received">
                            <span>
                              <img src={Icons.BigHeart} height={25} width={25} alt="User" title="User" />
                            </span>
                            {k.amount} <FormattedMessage id="message.sidepanel_coins" />
                          </div>
                        </div>
                        <div className="col-4 text-center topUpC_time">{moment(k.txntimestamp).format("LLL")}</div>
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="col-12 pt-5">
                    <div className="col-12 text-center">
                      <img src={Icons.ReplacementForCoins} height={250} width={200} alt="no-transactions-found" />
                    </div>
                    <div className="col-12 text-center noCoins_type pt-2">
                      <FormattedMessage id="message.coinIn" />
                    </div>
                    <div className="col-12 text-center noCoins_text pt-2">
                      <FormattedMessage id="message.noTransactionHistory" />.
                    </div>
                  </div>
                )}
              </TabContainer>
              <TabContainer dir={theme.direction} id={`sv2`}>
                {this.state.outData.data && this.state.outData.data.length > 0 ? (
                  this.state.outData.data.map((k) => (
                    <div className="col-12 py-3">
                      <div className="row">
                        <div className="col-8">
                          {/* <div className="topUp_data_txnId">#{k.txnid}</div> */}
                          <div className="topUp_notes">{k.trigger}</div>
                          <div className="topUp_coins_received">
                            <span className="pr-1">
                              <img src={Icons.BigHeart} height={25} width={25} alt="User" title="User" />
                            </span>
                            {k.amount} <FormattedMessage id="message.sidepanel_coins" />
                          </div>
                        </div>
                        <div className="col-4 text-center topUpC_time">{moment(k.txntimestamp).format("LLL")}</div>
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="col-12 pt-5">
                    <div className="col-12 text-center">
                      <img src={Icons.ReplacementForCoins} height={250} width={200} alt="no-transactions-found" />
                    </div>
                    <div className="col-12 text-center noCoins_type pt-2">
                      <FormattedMessage id="message.coinOut" />
                    </div>
                    <div className="col-12 text-center noCoins_text pt-2">
                      <FormattedMessage id="message.noTransactionHistory" />.
                    </div>
                  </div>
                )}
              </TabContainer>
            </SwipeableViews>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentCoinBalance: state.UserProfile.CoinBalance,
    walletId: state.UserProfile.UserProfile.WalletData,
  };
};

export default withRouter(connect(mapStateToProps, null)(withStyles(styles, { withTheme: true })(TopUp)));
