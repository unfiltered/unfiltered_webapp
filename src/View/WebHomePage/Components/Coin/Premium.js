import React from "react";
import { connect } from "react-redux";
import { Images } from "../../../../Components/Images/Images";
import { Icons } from "../../../../Components/Icons/Icons";
import ActivatePlan from "./ActivatePlan";
import { FormattedMessage } from "react-intl";

function Premium(props) {
  const autoScrollRef = React.createRef();

  const scrollToBottom = () => {
    autoScrollRef.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div className="col-12 premiumUserScreen">
      <div ref={autoScrollRef}>
        <ActivatePlan existingPlanName={props.planName} allPlans={props.allPlans} />
      </div>
      <div className="col-12">
        <div className="text-center p-5 datumPremiumText2">
          <FormattedMessage id="message.text3" />
        </div>
        <div className="col-12">
          <div>
            <div className="row justify-content-around">
              <div>
                <div className="text-center pb-4 datumPremiumRow2">
                  <img src={Images.User4} height={100} width={100} alt="user4" />
                  <div>
                    <img src={Icons.datumIcon} height={30} width={30} alt="datum-icon" />
                  </div>
                </div>
                <div className="datumPremiumShortText">
                  <FormattedMessage id="message.text4" />
                </div>
              </div>
              <div>
                <div className="text-center pb-4">
                  <img
                    src={require("../../../../asset/images/joshua-rondeau-ZnHRNtwXg6Q-unsplash.jpg")}
                    style={{ borderRadius: "50%", objectFit: "cover" }}
                    height={100}
                    width={100}
                    alt="user5"
                  />
                </div>
                <div className="datumPremiumShortText">
                  <FormattedMessage id="message.text5" />
                </div>
              </div>
              <div>
                <div className="text-center pb-4 datumPremiumRow2">
                  <img src={Images.User6} height={100} width={100} alt="user6" />
                  <div>
                    <img src={Icons.actPlanUpArrow} height={30} width={30} alt="up-arrow" />
                  </div>
                </div>
                <div className="datumPremiumShortText">
                  <FormattedMessage id="message.text6" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    checkIfUserIsProUserVariable: state.ProUser,
    planName: state.ProUser.ProUserDetails.planName,
  };
};

export default connect(mapStateToProps, null)(Premium);
