import React, { useState } from "react";
import { purchaseSubscription, pruchasePlanFromAPI } from "../../../../controller/auth/verification";
import { connect } from "react-redux";
import { checkIfProUser } from "../../../../actions/ProUser";
import { withStyles } from "@material-ui/core/styles";
import { getCookie } from "../../../../lib/session";
import Cards from "react-credit-cards";
import MAT_UI_Modal from "../../../../Components/Model/MAT_UI_Modal";
import { withRouter } from "react-router-dom";
import "react-credit-cards/es/styles-compiled.css";

const styles = () => ({
  progress: {
    position: "absolute",
    zIndex: 999,
    left: " 45%",
    top: "40%",
  },
});

function ActivatePlan(props) {
  const [selectedPlan, setSelectedPlan] = useState("");
  const [changeScreen, setChangeScreen] = useState(false);
  const [processPlan, setProcessPlan] = useState(false);
  const [colors] = useState(["#19ACFF", "#E33022", "#9462FD"]);
  const [hardCodedPlans] = useState(["Elite", "Premium", "Plus"]);
  const [hardCodedPlansHeader] = useState(["Flexible", "Popular", "Value"]);
  const [discount] = useState(["1%", "45%", "62%"]);
  const [texts] = useState(["Billed every month", "Billed every 3 months", "Billed every 6 months"]);
  const [planFeatures] = useState([
    {
      plan: "Elite",
      array: [
        { line: "All “Premium” Features" },
        { line: "5 Boost Profile Feature per month" },
        { line: "Additional Filters – Ethnicity, Deal Breakers, Interest" },
        { line: "Unlimited Matches" },
        { line: "" },
      ],
    },
    {
      plan: "Elite First Year",
      array: [
        { line: "All “Premium” Features" },
        { line: "5 Boost Profile Feature per month" },
        { line: "Additional Filters – Ethnicity, Deal Breakers, Interest" },
        { line: "Unlimited Matches" },
        { line: "" },
      ],
    },
    {
      plan: "Premium",
      array: [
        { line: "All “Plus” Features" },
        { line: "Rewind Feature" },
        { line: "Setup Date Feature" },
        { line: "Control Your Profile" },
        { line: "Enable Video Chat Feature" },
      ],
    },
    {
      plan: "Plus",
      array: [
        { line: "Unlimited Likes" },
        { line: "See Who Likes you before you Swipe" },
        { line: "Global Passport – Match with anyone in the US" },
        { line: "5 Super Likes Per Day" },
        { line: "5 Additional Matches" },
      ],
    },
  ]);
  const [modal, setModal] = useState(false);
  const [isPaymentDone, setIsPaymentDone] = useState("");
  const [processing, isProcessing] = useState(false);
  const [state, setState] = useState({
    cvc: "",
    expiry: "",
    name: "",
    number: "",
    focus: "",
  });
  const [subsDone, setIsSubsDone] = useState(false);
  const [subPlan, setSubPlan] = useState({});

  const toggleModal = (k) => {
    if (k) {
      setSubPlan(k);
    }
    setModal(!modal);
  };

  const handleInputFocus = (e) => {
    setState({ ...state, focus: e.target.name });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const checkForValidation = () => {
    if (state.cvc.length >= 3 && state.expiry.length >= 3 && state.name.length > 2 && state.number.length >= 16) {
      return true;
    }
    return false;
  };

  const purchasePlan = async (plan) => {
    isProcessing(true);
    let data = await pruchasePlanFromAPI(getCookie("token"), {
      cardNumber: state.number,
      expireDate: state.expiry,
      cardCode: state.cvc,
      planId: plan._id,
      type: "1",
    });
    if (data.status === 200) {
      let obj = {
        planId: plan._id,
        paymentGatewayTxnId: data.data.message,
        paymentGateway: "paypal",
        userPurchaseTime: new Date().getTime.toString(),
        type: "1",
        currencyCode: "USD",
      };
      let responseForSubscription = await purchaseSubscription(getCookie("token"), obj);
      console.log("API called", responseForSubscription);
      props.checkIfUserIsProUser(responseForSubscription.data.data);
      setIsSubsDone(true);
      setIsPaymentDone("Payment is successfull");
      setTimeout(() => {
        isProcessing(false);
      }, 2000);
    } else {
      setIsPaymentDone(data.data.message);
      isProcessing(false);
      setTimeout(() => {
        setIsPaymentDone("");
      }, 5000);
    }
  };

  const openSubPlans = (k, i) => {
    if (i === 0) {
      setSelectedPlan(Object.keys(props.allPlans)[2]);
    } else if (i === 2) {
      setSelectedPlan(Object.keys(props.allPlans)[0]);
    } else {
      setSelectedPlan(k);
    }
    setProcessPlan(true);
    setChangeScreen(!changeScreen);
  };

  const space = { paddingBottom: "25px" };
  return changeScreen ? (
    <div className="col-12 activatePlanScreen">
      <div className="col-12 WNavbar_setting border-bottom px-0">
        <div className="py-3 row align-items-center mx-0">
          <div className="col-6">
            <div className="d-flex align-items-center">
              <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={() => setChangeScreen(!changeScreen)}>
                <img src={require("../../../../asset/images/black-left-arrow.svg")} width={20} height={20} alt="left-arrow" />
              </div>
              <h5 className="m-0">Select Plan</h5>
            </div>
          </div>
        </div>
      </div>
      <div className="text-center pt-5">
        <h3 style={{ fontFamily: "Product Sans Bold" }}>{selectedPlan}</h3>
      </div>
      <div className="col-12 p-5 row mx-auto" style={{ justifyContent: "space-evenly" }}>
        {props.allPlans &&
          props.allPlans[selectedPlan].map((k, planIndex) => {
            return (
              <div key={planIndex} className={"plansCoinCard px-0 mt-4"}>
                <div style={{ background: colors[planIndex] }}>{hardCodedPlansHeader[planIndex]}</div>
                <div style={{ fontSize: "20px", fontFamily: "Product Sans Bold" }}>
                  <div className="text-center py-3">MEMBER</div>
                  <div className="text-center"></div>
                </div>
                <div
                  className="mx-auto"
                  style={{
                    borderTop: `2px solid ${colors[planIndex]}`,
                    borderBottom: `2px solid ${colors[planIndex]}`,
                    color: colors[planIndex],
                    fontSize: "20px",
                    fontFamily: "Product Sans Bold",
                    width: "60%",
                  }}
                >
                  {k.currencySymbol} {k.cost}
                </div>
                <div className="text-center py-2">{texts[planIndex]}</div>
                <button style={{ background: `${colors[planIndex]}` }} onClick={() => toggleModal(k)} className="my-4">
                  Buy Now
                </button>
                <div style={planIndex === 0 ? { opacity: 0 } : {}} className="mt-2 mb-5">
                  {discount[planIndex] ? `Save ${discount[planIndex]}` : ""}
                </div>
              </div>
            );
          })}
      </div>
      <MAT_UI_Modal isOpen={modal} toggle={toggleModal} width={"500px"}>
        {isPaymentDone.length > 0 && subsDone ? (
          <div className="col-12 p-5">
            <div>Payment is successfull. Your UnFiltered Plan has been activated</div>
            <button
              className={"d_makePayment mt-3"}
              onClick={() => {
                props.history.push("/app");
              }}
            >
              Ok
            </button>
          </div>
        ) : (
          <div className="col-12 p-5">
            <h3 className="text-center pb-2">Activate UnFiltered Plan</h3>
            <div id="PaymentForm">
              <Cards cvc={state.cvc} expiry={state.expiry} focused={state.focus} name={state.name} number={state.number} />
              <form className="col-12 pt-3" onSubmit={(e) => e.preventDefault()}>
                <div className="row mx-auto card-payment-section">
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text">Name:</div>
                    <input type="text" name="name" placeholder="John Doe" onChange={handleInputChange} onFocus={handleInputFocus} />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text">Card No:</div>
                    <input
                      type="tel"
                      name="number"
                      maxLength={17}
                      placeholder="4242424242424242"
                      onChange={handleInputChange}
                      onFocus={handleInputFocus}
                    />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text"> CVV:</div>
                    <input type="tel" name="cvc" maxLength={4} placeholder="000" onChange={handleInputChange} onFocus={handleInputFocus} />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text"> Expiry:</div>
                    <input
                      type="tel"
                      name="expiry"
                      maxLength={4}
                      placeholder="0324"
                      onChange={handleInputChange}
                      onFocus={handleInputFocus}
                    />
                  </div>
                  <button
                    className={checkForValidation() ? "d_makePayment mt-3" : "d_makePayment_disabled mt-3"}
                    disabled={!checkForValidation()}
                    onClick={() => purchasePlan(subPlan)}
                  >
                    {processing ? "Please wait, while payment is processing" : "Make Payment"}
                  </button>
                  {isPaymentDone.length > 0 ? <div className="text-center w-100">{isPaymentDone}</div> : ""}
                </div>
              </form>
            </div>
          </div>
        )}
      </MAT_UI_Modal>
    </div>
  ) : (
    <div className="col-12 activatePlanScreen p-5">
      <div className="col-12 text-center">
        <div className="planHeader">UnFiltered Experience</div>
        <hr />
        <div className="col-12 text-center pt-2">
          <div className="row justify-content-center" style={space}>
            <div className="activate_plan_text">Upgrade your account for Enhanced Features</div>
            <div className="col-12 py-4 px-0">
              <div className="row mx-0" style={{ justifyContent: "space-evenly" }}>
                {props.allPlans &&
                  Object.keys(props.allPlans)
                    .map((k, planIndex) => (
                      <div
                        onClick={() => openSubPlans(k, planIndex)}
                        key={planIndex}
                        className={"infoCard px-0"}
                        style={planIndex != 1 ? { marginTop: "25px" } : {}}
                      >
                        <div
                          className="py-2"
                          style={{
                            background: colors[planIndex],
                            borderTopLeftRadius: "12px",
                            borderTopRightRadius: "12px",
                            color: "#FFF",
                            fontSize: "18px",
                            fontFamily: "Product Sans Bold",
                          }}
                        >
                          {planIndex === 1 ? "Popular Plan" : ""}
                        </div>
                        <div>UnFiltered {hardCodedPlans[planIndex]}</div>

                        <div className="mb-4">
                          {planFeatures[planIndex].array.map((v, w) =>
                            v.line === "" ? <div key={w}>{v.line}</div> : <div key={w}>{v.line}</div>
                          )}
                        </div>
                        <button style={{ background: `${colors[planIndex]}`, marginBottom: "25px" }}>Check Plans</button>
                      </div>
                    ))
                    .reverse()}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    durationOfMonthsFromProps: state && state.ProUser && state.ProUser.ProUserDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    checkIfUserIsProUser: (data) => dispatch(checkIfProUser(data)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ActivatePlan)));
