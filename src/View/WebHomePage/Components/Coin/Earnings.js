import React from "react";
import { connect } from "react-redux";
import "./Coin.scss";
import "../Prospects/Prospects.scss";
import {
  CurrentCoinBalance,
  GetEarningWalletTransactionWithTxn,
  withdrawCoins,
  walletEarning,
  getEstimatedCoins,
} from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import CreditLogs from "./WalletLogs/CreditLogs";
import DebitLogs from "./WalletLogs/DebitLogs";
import FullWidthTabs from "../../../../Components/WebTabs/FullWidthTabs";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";
import MATTextField from "../../../../Components/Input/TextField";

const styles = () => ({
  input: {
    width: "100%",
    margin: "10px 0",
  },
  accountInput: { width: "100%", margin: "10px 0" },
  inputLabel: {
    fontFamily: "Product Sans",
    color: "#e31b1b",
  },
  underline: {
    "&:before": {
      borderBottom: `1px solid #e31b1b`,
    },
  },
});

class Earnings extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      coinsEarned: "",
      balance: "",
      earningWalletCreditData: [],
      earningWalletWithdrawData: [],
      open: false,
      inputCoin: "",
      accountID: "",
      outputCoin: "",
      value: 0,
      viewMore: false,
      loader: false,
      successfullWithdrawal: false,
      allCoinsWithoutPageState: false,
    };
  }

  /** updates the wallet data (earning data) */
  updateEarningWalletCreditData = (data) => {
    this.setState({ earningWalletCreditData: data });
  };

  /** updates the wallet data (withdrawal data) */
  updateEarningWalletWithdrawData = (data) => {
    this.setState({ earningWalletWithdrawData: data });
  };

  /** API called to withdraw coins into real currency -> request goes to admin  */
  withdrawCoinsAPI = (currency) => {
    if (this.state.inputCoin > 0 && this.state.inputCoin <= this.state.coinsEarned && this.state.accountID.length > 3) {
      withdrawCoins(this.state.inputCoin, this.state.outputCoin, getCookie("token"), this.state.accountID.toString(), currency, getCookie("uid"))
        .then((res) => {
          if (res.status === 200) {
            this.setState({ open: false, inputCoin: 0, outputCoin: 0, accountID: "", successfullWithdrawal: true });
            setTimeout(() => {
              this.setState({ transactionDoneModal: true });
              this.callEarningWalletAPI();
            }, 500);
          } else {
            this.setState({ open: false, successfullWithdrawal: false });
            setTimeout(() => {
              this.setState({ transactionDoneModal: true });
            }, 500);
          }
        })
        .catch((err) => this.setState({ open: false, transactionDoneModal: true, successfullWithdrawal: false }));
    }
  };

  /** toggles modal */
  toggleModal = () => {
    this.setState({ open: !this.state.open });
  };

  /** API called to get all details about earning wallet, includes withdrawal and earnings */
  callEarningWalletAPI = () => {
    this.setState({ loader: true });
    CurrentCoinBalance(getCookie("uid"), getCookie("token")).then((res) => {
      if (res.data.walletEarningData.length > 0) {
        this.setState({ coinsEarned: res.data.walletEarningData[0].balance });
        /** This API gets Earning wallet info, not array of object data */
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 1, false).then((res) => {
          this.setState({ earningWalletCreditData: res.data, allCoinsWithoutPageState: true, loader: false });
        });
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 2, false).then((res) => {
          this.setState({ earningWalletWithdrawData: res.data, loader: false });
        });
        walletEarning(getCookie("token"), getCookie("uid"), "user", "INR")
          .then((res) => {
            this.setState({ balance: res.data.userPreferedCurrency.withdrawAmount });
          })
          .catch((err) => console.log("[walletEarning] err", err));
      }
    });
  };

  componentDidMount() {
    this.props.handleWebsiteActiveValue("/app/earnings");
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    this.paneDidMount(this.myRef.current); // <div>
    /** API to get wallet info */
    this.callEarningWalletAPI();
  }

  /** changes index of tabs and updates to child component  */
  handleChangeIndex = (index) => {
    this.setState({ value: index });
  };

  /** if any API is called, it will update the UI for live updates */
  componentDidUpdate(prevProps, prevState) {
    if (prevState.inputCoin !== this.state.inputCoin) {
      getEstimatedCoins(getCookie("token"), getCookie("uid"), this.state.inputCoin, "user", "INR")
        .then((res) => {
          this.setState({ outputCoin: res.data.data.userPreferedCurrency.withdrawAmount.toFixed(2) });
        })
        .catch((err) => this.setState({ outputCoin: "Max withdrawal exceeded" }));
    }
  }

  /** on scroll, API called (pagination) on the selected tab and data is concatinated */
  setViewMore = (boolean, coinType, cbFucntion, value) => {
    console.log("coinType", coinType);
    if (boolean && coinType.data.length < coinType.totalCount) {
      GetEarningWalletTransactionWithTxn(this.props.earningWalletId, getCookie("token"), value, coinType.pageState, this.state.allCoinsWithoutPageState)
        .then((res) => {
          let oldAllData = { ...coinType };
          let oldAllDataArr = [];
          oldAllDataArr.push(...oldAllData.data, ...res.data.data);
          oldAllData["data"] = oldAllDataArr;
          oldAllData["pageState"] = res.data.pageState;
          cbFucntion(oldAllData);
          this.setState({ viewMore: false });
        })
        .catch((err) => this.setState({ viewMore: false }));
    } else {
      this.setState({ viewMore: false });
    }
  };

  /** function for scroll pagination */
  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        if (this.state.value === 0) {
          this.setViewMore(true, this.state.earningWalletCreditData, this.updateEarningWalletCreditData, 1);
        } else if (this.state.value === 1) {
          this.setViewMore(true, this.state.earningWalletWithdrawData, this.updateEarningWalletWithdrawData, 2);
        }
      }
    }
  };

  /** ref is removed on leaving page */
  paneDidUnmount(node) {
    if (node) {
      node.childNodes[0].children[1].removeEventListener("scroll", this.handleScroll, true);
    }
  }

  /** ref is passed to handle scroll mounted on <div> */
  paneDidMount = (node) => {
    if (node) {
      node.childNodes[0].children[1].addEventListener("scroll", this.handleScroll, true);
    }
  };

  successFullTransactionModal = () => {
    this.setState({ transactionDoneModal: !this.state.transactionDoneModal });
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  componentWillUnmount() {
    this.paneDidUnmount(this.myRef.current);
  }

  updateInputValue = (input) => {
    try {
      let val = input.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");
      return val;
    } catch (e) {
      console.log("error occured");
    }
  };

  render() {
    // console.log(this.state.inputCoin, this.state.outputCoin);
    const { classes } = this.props;
    let { coinsEarned, balance, open, transactionDoneModal, outputCoin, inputCoin, accountID, earningWalletWithdrawData, earningWalletCreditData } = this.state;
    let { toggleModal, updateInputValue, withdrawCoinsAPI, successFullTransactionModal } = this;
    return (
      <div className="col-12 userInfoContainer px-0" style={{ height: "calc(100vh - 30px)" }}>
        {/* {this.state.loader ? (
          <div className="loader">
            <img src={Icons.CMeLoader} alt="datum-loader" />
          </div>
        ) : (
          <span />
        )} */}
        <div className="page_header py-3 px-3 border-bottom">
          <FormattedMessage id="message.sidepanel_earnings" />
        </div>
        <div className="row mx-0">
          <div className="col-12 mb-3">
            <div className="row px-1">
              <div className="col-6 px-0">
                <div className="col-12 mt-4 d-flex">
                  <div>
                    <img src={require("./Assets/coin.svg")} alt="coins" height={42} width={42} />
                  </div>
                  <div className="ml-2 coinBalance">
                    {coinsEarned} <FormattedMessage id="message.sidepanel_coins" />
                  </div>
                </div>
                {balance ? (
                  <div className="col-12 mt-1 realCoinBalance">
                    <FormattedMessage id="message.earningsYourBalance" />: <span>{parseFloat(balance).toFixed(2)} INR</span>
                  </div>
                ) : (
                  ""
                )}
              </div>
              {/* <div className="col-6 d-flex justify-content-end align-items-end">
                <Button
                  className="text-right py-2 withdrawEarningsButton"
                  text={<FormattedMessage id="message.earningsWithdrawEarnings" />}
                  handler={toggleModal}
                />
              </div> */}
            </div>
          </div>
        </div>
        <MatUiModal isOpen={open} toggle={toggleModal} width={500}>
          <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
            <div className="referralModalCancelButton" onClick={toggleModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="d_friends_modal_header">
              <FormattedMessage id="message.earningsModalHeader" />
            </div>
            <div className="row">
              <div className="col-12">
                <MATTextField
                  className={"withdrawCoins-input"}
                  onChange={this.handleChange("accountID")}
                  value={accountID}
                  label={<FormattedMessage id="message.earningsModal_input1" />}
                />
              </div>
            </div>
            <div className="row pt-2">
              <div className="col-6">
                <MATTextField
                  className={"withdrawCoins-input"}
                  onChange={this.handleChange("inputCoin")}
                  value={updateInputValue(inputCoin)}
                  label={<FormattedMessage id="message.earningsModal_enterCoins" />}
                />
              </div>
              <div className="col-6">
                <MATTextField
                  className={"withdrawCoins-input"}
                  value={outputCoin}
                  label={<FormattedMessage id="message.earningsModal_estimatedAmt" />}
                  disabled={true}
                />
              </div>
            </div>
            <div className="earningsModalMaxAmount pt-2 text-left">
              <FormattedMessage id="message.earningsModal_maxWithdraw" />: <span>{coinsEarned}</span>
            </div>
            <div
              className={
                this.state.inputCoin > 0 && this.state.inputCoin <= this.state.coinsEarned && this.state.accountID.length > 3
                  ? "d_newsFeed_modal_post_btn"
                  : "d_newsFeed_modal_post_btn_disabled "
              }
              onClick={() => withdrawCoinsAPI("INR")}
            >
              <FormattedMessage id="message.earningsModal_submit" />
            </div>
          </div>
        </MatUiModal>
        <MatUiModal isOpen={transactionDoneModal} toggle={successFullTransactionModal} width={500}>
          <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
            <div className="referralModalCancelButton" onClick={successFullTransactionModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="d_friends_modal_header">
              {this.state.successfullWithdrawal ? <FormattedMessage id="message.reqWithdraw" /> : "Insufficient funds for withdrawal"}
            </div>
            <Button text="Ok" className="okButton mt-5" handler={successFullTransactionModal} />
          </div>
        </MatUiModal>
        {/* here chats are equal to in coins, and match equals to coins out */}
        <div className="d_earning_tabs px-3 pt-3" ref={this.myRef}>
          <FullWidthTabs
            value={this.state.value}
            handleChangeIndex={this.handleChangeIndex}
            chats={<DebitLogs data={earningWalletCreditData.data} />}
            match={<CreditLogs data={earningWalletWithdrawData.data} />}
            label1={<FormattedMessage id="message.sidepanel_earnings" />}
            label2={<FormattedMessage id="message.earningsWithdrawal" />}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentCoinBalance: state.UserProfile.CoinBalance,
    earningWalletId: state.UserProfile.UserProfile.EarningWalletId,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(Earnings));
