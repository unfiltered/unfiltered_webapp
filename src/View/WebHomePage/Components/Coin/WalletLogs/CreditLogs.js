import React from "react";
import moment from "moment";
import NoTransaction from "../Assets/no_wallet.svg";
import { FormattedMessage } from "react-intl";

/** things have been fucked up from my side. this is debit  */

function CreditLogs({ data }) {
  let plusAmount = { fontSize: 14, fontWeight: 500, color: "#6d6d6d", fontFamily: "Product Sans" };
  let el = { border: "1px solid #E5E5E5", padding: "10px" };
  let label = { color: "#474747", fontFamily: "Product Sans" };
  return (
    <div>
      {data && data.length > 0 ? (
        data.map((k) => (
          <div className="col-12 my-3">
            <div className="row px-3">
              <div style={el} className="col-12">
                <div className="col-12">
                  <span style={plusAmount}>
                    <FormattedMessage id="message.wallet_coinsWithdrawed" />:
                  </span>
                  <span style={label} className="pl-1"></span>
                  {k.amount}
                </div>
                <div className="col-12 text-left">
                  <span style={plusAmount}>
                    <FormattedMessage id="message.date" />:
                  </span>
                  <span style={label} className="pl-1">
                    {moment(k.txntimestamp).format("LLL")}
                  </span>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="col-12 text-center h-100 d-flex justify-content-center align-items-center flex-column">
          <img src={NoTransaction} width={200} height={200} alt="no-transaction" />
          <div className="paymentMethod_premium">
            <FormattedMessage id="message.noTransactions" />.
          </div>
        </div>
      )}
    </div>
  );
}

export default CreditLogs;
