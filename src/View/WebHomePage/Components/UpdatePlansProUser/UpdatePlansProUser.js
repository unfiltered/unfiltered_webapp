import React from "react";
import { Images } from "../../../../Components/Images/Images";
import { Icons } from "../../../../Components/Icons/Icons";

function UpdatePlansProUser(props) {
  const navigateToActivatePlan = () => {
    props.history.push("/app/activate-plan");
  };
  return (
    <div className="col-12 mainScreen">
      <div className="col-12 premium_info1 pb-5">
        <div className="p-5">
          <div className="row">
            <div className="col-7">
              <div className="row">
                <div className="col-3">
                  <img src={Icons.actPlanDiamond} height={100} width={100} alt="diamond-with-stars" />
                </div>
                <div className="col-9 d-flex align-items-center paymentMethod_premium">
                  You are not a premium user yet, Please consider becoming a premium user by clicking on the Activate Button.
                </div>
              </div>
            </div>
            <div className="col-5 d-flex justify-content-center align-items-center">
              <div className="tabActive" onClick={() => navigateToActivatePlan()}>
                Activate Now
              </div>
            </div>
          </div>
        </div>
        <div className="col-12">
          <div className="row">
            <div className="col-lg-6 col-xl-6   col-md-12">
              <div className="user_images">
                <div className="row justify-content-center">
                  <div>
                    <img src={Images.User1} height={100} width={100} alt="user1" />
                  </div>
                  <div>
                    <img src={Images.User2} height={100} width={100} alt="user2" />
                    <div>
                      <img src={Icons.actPlanLove} height={30} width={30} alt="love" />
                    </div>
                  </div>
                  <div>
                    <img src={Images.User3} height={100} width={100} alt="user3" />
                  </div>
                </div>
              </div>
              <div className="text-center pt-4">Access the guys who liked you instantly</div>
            </div>
            <div className="col-lg-6 col-xl-6 col-md-12">
              <div className="user_images">
                <div className="row justify-content-center">
                  <div>
                    <img src={Images.User4} height={100} width={100} alt="user4" />
                  </div>
                  <div>
                    <img src={Images.User5} height={100} width={100} alt="user5" />
                    <div>
                      <img src={Icons.actStar} height={30} width={30} alt="star" />
                    </div>
                  </div>
                  <div>
                    <img src={Images.User6} height={100} width={100} alt="user6" />
                  </div>
                </div>
              </div>
              <div className="text-center pt-4">Find out who added you as a Favourite</div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12">
        <div className="text-center p-5 datumPremiumText2">With UnFiltered Premium you can:</div>
        <div className="col-12">
          <div>
            <div className="row justify-content-around">
              <div>
                <div className="text-center pb-4 datumPremiumRow2">
                  <img src={Images.User4} height={100} width={100} alt="user4" />
                  <div>
                    <img src={Icons.datumIcon} height={30} width={30} alt="datum-icon" />
                  </div>
                </div>
                <div className="datumPremiumShortText">See who Likes You</div>
              </div>
              <div>
                <div className="text-center pb-4">
                  <img src={Images.User5} height={100} width={100} alt="user5" />
                </div>
                <div className="datumPremiumShortText">Be able to undo your “No” Votes</div>
              </div>
              <div>
                <div className="text-center pb-4 datumPremiumRow2">
                  <img src={Images.User6} height={100} width={100} alt="user6" />
                  <div>
                    <img src={Icons.actPlanUpArrow} height={30} width={30} alt="up-arrow" />
                  </div>
                </div>
                <div className="datumPremiumShortText">Have your messages read first</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UpdatePlansProUser;
