import React from "react";
import ReactDOM from "react-dom";
import scriptLoader from "react-async-script-loader";

class PaypalButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showButton: false,
    };
    window.React = React;
    window.ReactDOM = ReactDOM;
  }
  componentDidMount() {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ showButton: true });
    }
  }
  /** */
  /** if the paypal script is loaded, and if inputs are same, the checkout button is unchanged(rendered) */
  componentWillReceiveProps(nextProps) {
    const { isScriptLoaded, isScriptLoadSucceed } = nextProps;
    const isLoadedButWasntLoadedBefore = !this.state.showButton && !this.props.isScriptLoaded && isScriptLoaded;
    if (isLoadedButWasntLoadedBefore) {
      if (isScriptLoadSucceed) {
        this.setState({ showButton: true });
      }
    }
  }

  render() {
    const paypal = window.PAYPAL;
    const { total, currency, env, commit, client, onSuccess, onError, onCancel } = this.props;
    console.log("input currency ", currency, total);
    const { showButton } = this.state;
    /** adds currency on selecting the selection(dropdown) as for now */
    const payment = () =>
      paypal.rest.payment.create(env, client, {
        intent: "authorize",
        payer: {
          payment_method: "paypal",
        },
        transactions: [
          {
            amount: {
              total,
              currency,
            },
          },
        ],
      });
    const onAuthorize = (data, actions) => {
      actions.payment.execute().then(() => {
        if (!this.props.isMobile) {
          this.props.toggleConfirmPaymentModal();
        }

        const payment = {
          paid: true,
          cancelled: false,
          payerID: data.payerID,
          paymentID: data.paymentID,
          paymentToken: data.paymentToken,
          returnUrl: data.returnUrl,
          met: actions.payment.execute(data.paymentID, data.payerID, function (error, payment) {
            console.log("inside payment", payment);
            if (error) {
              console.error(JSON.stringify(error));
            } else {
              console.log("inside payment", payment);
              if (payment.state === "approved" && payment.transactions && payment.transactions[0].related_resources && payment.transactions[0].related_resources[0].authorization) {
                return payment.transactions[0].related_resources[0].authorization.id;
              } else {
                console.log("payment not successful");
              }
            }
          }),
        };
        onSuccess(payment);
      });
    };
    /** shows the -> show button only when the paypal library has loaded completely */
    return (
      <div id="paypal_button">
        {showButton && <paypal.Button.react env={env} client={client} commit={commit} payment={payment} onAuthorize={onAuthorize} onCancel={onCancel} onError={onError} />}
      </div>
    );
  }
}
export default scriptLoader("https://www.paypalobjects.com/api/checkout.js")(PaypalButton);
