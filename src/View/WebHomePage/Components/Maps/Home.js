import { GoogleApiWrapper, Map } from "google-maps-react";
import { getCookie, setCookie } from "../../../../lib/session";
import { geolocated } from "react-geolocated";
import { withRouter } from "react-router-dom";
import React, { Component } from "react";
import "./Home.scss";
import { keys } from "../../../../lib/keys";
import { Icons } from "../../../../Components/Icons/Icons";
import Button from "../../../../Components/Button/Button";
import FilterMap from "./FilterMap";
import FilterUser from "../FilterUser/FilterUser";
import { FormattedMessage } from "react-intl";

let lat = getCookie("lat");
let long = getCookie("long");

class Home extends Component {
  state = {
    position: null,
    city: "",
    multiselectchecbox: [],
    selectedAddressToShow: "",
    selectedLocation: "", // this will store object of selected location
    markers: [
      {
        name: "Current position",
        position: {
          lat: lat,
          lng: long,
        },
      },
    ],
    lat: "",
    lng: "",
    currentPlaceToShow: "",
    baseAddress: "",
    locationSelected: "", // this will store the {String} address of selected location
  };

  componentDidMount() {
    if (getCookie("locations") != null) {
      let loc = getCookie("locations");
      let _loc = JSON.parse(loc);
      if (_loc.length > 0) {
        this.setState({
          multiselectchecbox: _loc,
          selectedAddressToShow: _loc[0].address,
        });
      }
      if (getCookie("selectedLocation") != null) {
        this.setState({
          selectedLocation: JSON.parse(getCookie("selectedLocation")),
          currentPlaceToShow: JSON.parse(getCookie("selectedLocation")).city,
        });
      }
    } else if (getCookie("selectedLocation") != null) {
      let selectedLocation = JSON.parse(getCookie("selectedLocation"));
      this.setState({
        selectedLocation: selectedLocation,
        currentPlaceToShow: selectedLocation.city,
      });
    } else if (getCookie("location") != null) {
      this.setState({ currentPlaceToShow: getCookie("location") });
    }
    this.setState({ baseAddress: JSON.parse(getCookie("baseAddress")) });
  }

  // function to select exisitng object for selected location
  selectLocationFn = (obj) => {
    console.log("{selectLocationFn}", obj);
    this.props.setLocationOfCookie(obj);
    this.setState({ selectedLocation: obj });
  };

  // function to, click done -> go back and refresh new users
  handleFilterAndUpdateData = () => {
    this.props.setLocationOfCookie(this.state.selectedLocation);
    setCookie("selectedLocation", JSON.stringify(this.state.selectedLocation));
    this.props.handleFilterUser(this.state.selectedLocation.lat, this.state.selectedLocation.lng);
  };

  // deletes location, if already saved locations [{...obj}] has more than 1 objects, than the first
  // index + 1 is deleted, else the index received for click listener, the location is deleted and saved into cookie,
  delete = (index) => {
    let { multiselectchecbox } = this.state;
    multiselectchecbox.splice(index, 1);
    this.setState({ multiselectchecbox });
    setCookie("locations", JSON.stringify(multiselectchecbox));
  };

  // add new location object into the [{...locations}] array, and saves into cookie.
  addNewLocation = (address, city, lat, lng) => {
    console.log("{addNewLocation}", address, city, lat, lng);
    let obj = [...this.state.multiselectchecbox];
    if (address !== "" && city !== "" && lat !== "" && lng !== "") {
      if (obj.includes(address)) {
        let index = obj.indexOf(address);
        obj.splice(index, 1);
      } else if (obj.length >= 5) {
        obj.pop();
      } else {
        obj.push({
          address: address,
          city: city,
          lat: lat,
          lng: lng,
        });
        setCookie("locations", JSON.stringify(obj));
        this.setState({ multiselectchecbox: obj });
      }
    }
  };

  handleMapScreen = () => {
    this.props.updateScreen(
      <div className="d_map_pref d_filter_modal">
        <FilterMap
          addNewLocation={this.addNewLocation}
          modalResetCount={this.props.modalResetCount}
          modalDecrementCount={this.props.modalDecrementCount}
          modalUpdateCount={this.props.modalUpdateCount}
          updateScreen={this.props.updateScreen}
          onClose={this.props.onClose}
          setLocationOfCookie={this.props.setLocationOfCookie}
          handleFilterUser={this.props.handleFilterUser}
        />
      </div>
    );
    this.props.modalUpdateCount(); // count 3 for modal
  };

  handleBackScreen = () => {
    this.props.updateScreen(
      <FilterUser
        onClose={this.props.onClose}
        modalDecrementCount={this.props.modalDecrementCount}
        modalUpdateCount={this.props.modalUpdateCount}
        updateScreen={this.props.updateScreen}
        modalResetCount={this.props.modalResetCount}
        setLocationOfCookie={this.props.setLocationOfCookie}
        handleFilterUser={this.props.handleFilterUser}
      />
    );
    this.props.modalDecrementCount();
  };

  render() {
    console.log("[multiselectchecbox]", this.state.multiselectchecbox);
    return (
      <div className="col-12 bg-white d_map px-0">
        <div className="d_newsFeed_modal_close_btn" onClick={this.handleBackScreen}>
          <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
        </div>
        <div className="text-center d_map_header py-4">
          <FormattedMessage id="message.location" />
        </div>
        <div className="row mx-0">
          <div className="d_location_modal_subHeader w-100">
            <FormattedMessage id="message.currentLocation" />
          </div>
          <div className="col-1 d-flex align-items-center justify-content-center px-0">
            <img src={Icons.passportIcon} alt="passport-location" />
          </div>
          <div className="col-9 d_location_modal_currentLocation px-0 py-3">{this.state.baseAddress.address}</div>
          <div className="col-2 d-flex justify-content-center align-items-center px-0">
            <div
              className={
                this.state.selectedLocation.address === this.state.baseAddress.address
                  ? "col-6 px-0  location_select_option"
                  : "col-6 px-0  location_select_option"
              }
            >
              {this.state.selectedLocation.address === this.state.baseAddress.address ? (
                <img src={Icons.tickSvg} height={25} width={25} alt="tick" />
              ) : (
                <img
                  src={Icons.circleSvg}
                  height={25}
                  alt="circle"
                  width={25}
                  onClick={() => this.selectLocationFn(this.state.baseAddress)}
                />
              )}
            </div>
          </div>
          <div className="d_location_modal_subHeader w-100">
            <FormattedMessage id="message.recentSearches" />
          </div>
        </div>
        <div className="row mx-0">
          <div className="d_map_select_options">
            {this.state.multiselectchecbox.length > 0 &&
              this.state.multiselectchecbox.map((k, i) => (
                <div className="col-12 border-bottom px-0 py-2" key={i}>
                  <div className="row mx-0">
                    <div className="col-1 d-flex align-items-center justify-content-center">
                      <img src={Icons.passportIcon} alt="passport-location" />
                    </div>
                    <div className="col-9 px-0">
                      <div className="m_loc_city">{k.city}</div>
                      <div className="m_loc_addr">{k.address}</div>
                      <div className="col-6 px-0" onClick={() => this.delete(i)}>
                        {this.state.selectedLocation.address === k.address ? "" : <div className="m_loc_addr_delete">Delete Location</div>}
                      </div>
                    </div>
                    <div className="col-2 px-0">
                      <div className="row mx-0 h-100 justify-content-center align-items-center">
                        <div
                          className={
                            this.state.selectedLocation.address === k.address
                              ? "col-6 px-0  location_select_option"
                              : "col-6 px-0  location_select_option"
                          }
                        >
                          {this.state.selectedLocation.address === k.address ? (
                            <img src={Icons.tickSvg} height={25} width={25} alt="tick" />
                          ) : (
                            <img src={Icons.circleSvg} height={25} alt="circle" width={25} onClick={() => this.selectLocationFn(k)} />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
        <div className="row mx-0 justify-content-center py-4">
          <div className="col-9">
            <Button
              className="d_location_modal_add_new_location"
              text={<FormattedMessage id="message.addNewLocation" />}
              handler={this.handleMapScreen}
            />
          </div>
          <div className="col-3">
            <Button
              className="d_location_modal_save"
              text={<FormattedMessage id="message.done" />}
              handler={() => {
                this.handleFilterAndUpdateData();
                this.handleBackScreen();
              }}
            />
          </div>

          {/* <div className="col-8">
            <div className="row">
              <div
                className="col-6"
                style={{ cursor: "pointer" }}
                onClick={() => this.addNewLocation(this.state.locationSelected, this.state.city, this.state.lat, this.state.lng)}
              >
                Add a new location
              </div>
              <div className="col-6 text-right doneButton" onClick={this.handleFilterAndUpdateData}>
                <button>Done</button>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    );
  }
}

const MapWrapper = (props) => (
  <Map google={props.google} visible={false}>
    <Home {...props} />
  </Map>
);

export default withRouter(
  GoogleApiWrapper({
    apiKey: keys.googleApiKey,
  })(
    geolocated({
      positionOptions: { enableHighAccuracy: false },
      userDecisionTimeout: 5000,
    })(MapWrapper)
  )
);
