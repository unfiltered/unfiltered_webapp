import React from "react";
import GoogleMapReact from "google-map-react";

const AnyReactComponent = ({ text }) => (
  <div>
    <img src={require("./pin.png")} width={50} alt="pin" height={50} />
  </div>
);

class ChatMapView extends React.Component {
  static defaultProps = {
    zoom: 6,
  };

  render() {
    return (
      <div style={{ height: "200px", width: this.props.mobile ? "240px" : "280px" }} className="__map">
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyB5Xj_33Ld1cVJeUoZzzNMkSiAto_CCZrA" }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          zoom={6}
          center={{ lat: parseInt(this.props.lat), lng: parseInt(this.props.lng) }}
        >
          <AnyReactComponent lat={this.props.lat} lng={this.props.lng} text="marker" />
        </GoogleMapReact>
      </div>
    );
  }
}

export default ChatMapView;
