import React from "react";
import { GoogleApiWrapper, Marker, Map } from "google-maps-react";
import { getCookie } from "../../../../lib/session";
import { geolocated } from "react-geolocated";
import { keys } from "../../../../lib/keys";
import { withRouter } from "react-router-dom";
import { Icons } from "../../../../Components/Icons/Icons";
import Button from "../../../../Components/Button/Button";
import Home from "./Home";
import { FormattedMessage } from "react-intl";

let lat = getCookie("lat");
let long = getCookie("lng");

class FilterMap extends React.Component {
  state = {
    position: null,
    city: "",
    multiselectchecbox: [],
    selectedAddressToShow: "",
    selectedLocation: "", // this will store object of selected location
    markers: [
      {
        name: "Current position",
        position: {
          lat: lat,
          lng: long,
        },
      },
    ],
    lat: "",
    lng: "",
    currentPlaceToShow: "'",
    locationSelected: "", // this will store the {String} address of selected location
  };

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps.map) this.renderAutoComplete();
  }

  onSubmit(e) {
    e.preventDefault();
  }

  renderAutoComplete() {
    const { google, map } = this.props;
    if (!google || !map) return;
    const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
    autocomplete.bindTo("bounds", map);
    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) return;
      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(9);
      }
      this.setState({ position: place.geometry.location }, () => {
        this.getAddressHelper(place.geometry.location.lat(), place.geometry.location.lng());
      });
    });
  }

  // function generates {lat, lng, address, and city name which is saved in cookies}
  getAddressHelper = (lat, lng) => {
    let google = window.google;
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ latLng: latlng }, (results, status) => {
      console.log("results, status", results, status);
      let resLength = results && results[0].address_components.length;
      if (status === google.maps.GeocoderStatus.OK) {
        if (resLength <= 5) {
          console.log("#1");
          this.setState({
            lat,
            lng,
            locationSelected: results[0].formatted_address,
            city: results[0].address_components[resLength - 3].long_name,
          });
        } else {
          console.log("#2");
          this.setState({
            lat,
            lng,
            locationSelected: results[0].formatted_address,
            city: results[0].address_components[resLength - 5].long_name,
          });
        }
      }
    });
  };

  onMarkerDragEnd = (coord, index) => {
    const { latLng } = coord;
    let lat = latLng.lat();
    let lng = latLng.lng();
    this.getAddressHelper(lat, lng);
    this.setState((prevState) => {
      const markers = [...this.state.markers];
      markers[index] = { ...markers[index], position: { lat, lng } };
      return { markers };
    });
  };

  componentDidMount() {
    this.setState({ locationSelected: getCookie("location") });
    this.renderAutoComplete();
  }

  handlePreviousScreen = () => {
    this.props.modalDecrementCount();
    this.props.updateScreen(
      <div className="d_map_pref">
        <Home
          modalResetCount={this.props.modalResetCount}
          updateScreen={this.props.updateScreen}
          modalUpdateCount={this.props.modalUpdateCount}
          modalDecrementCount={this.props.modalDecrementCount}
          addNewLocation={this.props.addNewLocation}
          setLocationOfCookie={this.props.setLocationOfCookie}
          handleFilterUser={this.props.handleFilterUser}
          onClose={this.props.onClose}
        />
      </div>
    );
  };

  render() {
    let { position } = this.state;
    return (
      <div className="col-12 px-0">
        <div className="d_newsFeed_modal_close_btn" onClick={this.handlePreviousScreen}>
          <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
        </div>
        <div className="text-center d_map_header py-4">
          <FormattedMessage id="message.dragMarker" />
        </div>
        <form onSubmit={this.onSubmit} className="m_location_searchBox">
          <input placeholder="Enter a location" ref={(ref) => (this.autocomplete = ref)} type="text" />
        </form>
        <div>
          <Map
            {...this.props}
            center={position}
            initialCenter={{
              lat: getCookie("lat"),
              lng: getCookie("lng"),
            }}
            centerAroundCurrentLocation={false}
            containerStyle={{
              height: "370px",
              position: "relative",
              width: "100%",
            }}
          >
            <Marker position={position} draggable={true} onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)} />
          </Map>
        </div>
        <div className="col-12 py-2 d_filter_modal_address">
          <FormattedMessage id="message.address" />
        </div>
        <div className="col-12">
          <textarea value={this.state.locationSelected} className="d_filtermap_textarea" />
        </div>
        <div className="col-12 py-5">
          <Button
            className="d_location_modal_add_new_location"
            text={<FormattedMessage id="message.addNewLocation" />}
            handler={() => {
              this.props.addNewLocation(this.state.locationSelected, this.state.city, this.state.lat, this.state.lng);
              this.handlePreviousScreen();
            }}
          />
        </div>
      </div>
    );
  }
}

const MapWrapper = (props) => (
  <Map google={props.google} visible={false}>
    <FilterMap {...props} />
  </Map>
);

export default withRouter(
  GoogleApiWrapper({
    apiKey: keys.googleApiKey,
  })(
    geolocated({
      positionOptions: { enableHighAccuracy: false },
      userDecisionTimeout: 5000,
    })(MapWrapper)
  )
);
