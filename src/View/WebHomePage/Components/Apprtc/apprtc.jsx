import React from "react";
import "./mainCss.css";
import { AppController, Call } from "../../../../lib/apprtc/apprtc.debug";
import { initVideoCallOverMqtt } from "../../../../lib/MqttHOC/utils";
import { __callInit, __updateMqttMessage } from "../../../../actions/webrtc";
import SimpleDialog from "../../../../Components/Dialogs/SimpleDialog";
import { getCookie } from "../../../../lib/session";

var uniqid = require("uniqid");

var appController;

class AppRtc extends React.Component {
  state = {
    receivedMessage: "",
    isCallConnected: false,
  };
  static getDerivedStateFromProps(props, state) {
    // console.log('props are', props)
    let { webrtc } = props;
    // console.log("Latest Calling Props", webrtc);
    if (webrtc && webrtc.lastMqttMsg && webrtc.lastMqttMsg.payload && !state.callEnded) {
      return {
        receivedMessage: {
          ...state.receivedMessage,
          ...webrtc.lastMqttMsg.payload,
          // ...props.opponentData,
        },
        isCallTopic: (webrtc.lastMqttMsg.topic && webrtc.lastMqttMsg.topic.indexOf("Calls/") > -1) || webrtc.callInit,
        text: !webrtc.callInit ? webrtc.lastMqttMsg.payload.roomId || webrtc.lastMqttMsg.payload.callId : state.text,
        roomKey: !webrtc.callInit ? webrtc.lastMqttMsg.payload.roomId || webrtc.lastMqttMsg.payload.callId : state.roomKey,
      };
    }

    return {
      callEnded: false,
    };
  }

  componentDidUpdate = () => {
    let { webrtc, currentUserData } = this.props;
    const { appRtcInit, isCallTopic } = this.state;
    if (webrtc && webrtc.lastMqttMsg && webrtc.lastMqttMsg.payload) {
      const { lastMqttMsg } = webrtc || {};
      const { payload } = lastMqttMsg || {};
      console.log("payload", payload);
      // stop media streaming + hangup call using apprtc ( in case of type is 2 )
      if (payload.type === 2) {
        console.log("currentUserData", currentUserData);
        // console.log("appController", appController.localVideo);
        try {
          appController.hangupSvg_ && appController.hangupSvg_.dispatchEvent(new Event("click"));
        } catch (e) {
          console.log("it crashed");
        }

        return;
      }

      if ((webrtc.callInit && !appRtcInit) || (payload.type == "1" && payload.callId && !appRtcInit)) {
        this.initCall();
      }

      if ((payload && payload.type == "2" && this.state.appRtcInit && isCallTopic) || (payload && payload.type == "7")) {
        const { userId } = currentUserData || {};
        this.props.dispatch && this.props.dispatch(__callInit(false));

        setTimeout(() => {
          initVideoCallOverMqtt(
            {
              // type: 90909
            },
            payload.userId
          );
          this.setState(
            {
              manualClose: true,
              appRtcInit: false,
              isCallTopic: false,
              callActivated: false,
              receivedMessage: {},
            },
            () => {
              // this.setState({ manualClose: false });
            }
          );
        }, 1500);
      }
    }
  };

  initCall = (room) => {
    if (!this.state.appRtcInit) {
      this.setState({ appRtcInit: true }, () => {
        setTimeout(() => {
          this.initApprtc(this.initCall);
        }, 500);
      });
      return;
    }
    console.log("callInit()");
    if (!this.state.callActivated) {
      const randomCode = uniqid() + Math.random().toString(36).substring(7);
      let roomId = room || randomCode;

      const { opponentData, currentUserData } = this.props;
      const { firstName, senderId, recipientId, requestedFor, dateId } = opponentData || {};
      const { userId, UserProfileName, UserProfilePic } = currentUserData || {};
      console.log("0000066666", opponentData);
      initVideoCallOverMqtt(
        {
          type: 0,
          callType: requestedFor === "0" ? "0" : "1",
          callerName: UserProfileName,
          receiverId: recipientId,
          callerId: userId,
          callInit: true,
          callerImage: UserProfilePic,
          callId: roomId,
          roomId: roomId,
          dateId: dateId && dateId.length > 0 ? dateId : "",
          callerIdentifier: userId,
        },
        recipientId
      );
      let action;
      let callBox = document.getElementById("call-box");
      action = callBox ? (callBox.style.display = "block") : "";

      let inp = document.getElementById("room-id-input");
      action = inp ? (inp.value = roomId) : "";

      let joinBtn = document.getElementById("join-button");
      action = joinBtn ? joinBtn.click() : "";

      this.setState({ callActivated: true }, () => {});
    }
  };

  handleCallAccept = () => {
    if (!this.state.appRtcInit) {
      this.setState({ appRtcInit: true }, () => {
        setTimeout(() => {
          this.initApprtc(this.handleCallAccept);
        }, 500);
      });
      return;
    }

    if (!this.state.callActivated) {
      const randomCode = uniqid() + Math.random().toString(36).substring(7);

      const { receivedMessage } = this.state;
      const { opponentData, currentUserData, webrtc } = this.props;
      const { firstName, senderId, recipientId, requestedFor, dateId } = opponentData || {};
      const { userId, UserProfileName, UserProfilePic } = currentUserData || {};
      const { lastMqttMsg } = webrtc || {};
      const { payload } = lastMqttMsg || {};

      let roomId = payload.callId || randomCode;

      console.log("Going to Publish On:: ", receivedMessage, payload, currentUserData, roomId);
      // return;
      console.log("0000077777", opponentData);
      initVideoCallOverMqtt(
        {
          type: 1,
          callType: requestedFor === "0" ? "0" : "1",
          callerName: UserProfileName,
          // receiverId: recipientId,
          callerId: userId,
          callInit: true,
          callerImage: UserProfilePic,
          callId: roomId,
          roomId: roomId,
          dateId: dateId && dateId.length > 0 ? dateId : "",
          callerIdentifier: userId,
        },
        receivedMessage.callerId
      );

      let action;
      let callBox = document.getElementById("call-box");
      action = callBox ? (callBox.style.display = "block") : "";

      let inp = document.getElementById("room-id-input");
      action = inp ? (inp.value = roomId) : "";

      let joinBtn = document.getElementById("join-button");
      action = joinBtn ? joinBtn.click() : "";

      console.log("Call Joined On:: ", roomId);

      this.setState({ callActivated: true, roomId: roomId }, () => {});
    }
  };

  handleCallReject = () => {
    const { receivedMessage } = this.state;
    const { opponentData, currentUserData, webrtc } = this.props;
    const { firstName, senderId, recipientId, requestedFor, dateId } = opponentData || {};
    const { userId } = currentUserData || {};
    const { lastMqttMsg } = webrtc || {};
    const { payload } = lastMqttMsg || {};

    let roomId = payload.callId || this.state.roomId;

    // console.log("Going to Publish On:: ", receivedMessage, payload, currentUserData, roomId);
    // return;
    console.log("0000088888", opponentData);
    initVideoCallOverMqtt(
      {
        type: 2,
        callType: requestedFor === "0" ? "0" : "1",
        callerId: userId,
        roomId: roomId,
        callerIdentifier: userId,
        dateId: dateId && dateId.length > 0 ? dateId : "",
      },
      receivedMessage.callerId
    );

    initVideoCallOverMqtt(
      {
        type: 2,
        callType: requestedFor === "0" ? "0" : "1",
        roomId: roomId,
        dateId: dateId && dateId.length > 0 ? dateId : "",
      },
      userId
    );
  };

  initApprtc = (callback) => {
    const randomCode = uniqid() + Math.random().toString(36).substring(7);
    const { opponentData } = this.props;
    const { requestedFor } = opponentData || {};
    console.log("0000099999", opponentData);
    const { receivedMessage } = this.state;
    let dataToRender = "";
    if (receivedMessage && !("callType" in receivedMessage)) {
      if (opponentData.isFriend == 1) {
        opponentData["callType"] = "1";
        opponentData["type"] = 0;
      }
      dataToRender = opponentData;
    } else {
      dataToRender = receivedMessage;
    }
    var loadingParams = {
      errorMessages: [],
      isLoopback: false,
      warningMessages: [],
      mediaConstraints:
        dataToRender && dataToRender.callType === "0"
          ? { audio: true, video: false }
          : {
              audio: true,
              video: {
                optional: [{ minWidth: "1280" }, { minHeight: "720" }],
                mandatory: {},
              },
            },
      offerOptions: {},
      peerConnectionConfig: {
        rtcpMuxPolicy: "require",
        bundlePolicy: "max-bundle",
        iceServers: [],
      },
      peerConnectionConstraints: { optional: [] },
      // buildConfigField "String", "GOOGLE_API_KEY", "\"AIzaSyBl3tlMAKk2c6mqtWZKwue_PLwsber7wLU\""
      iceServerRequestUrl: "https://networktraversal.googleapis.com/v1alpha/iceconfig?key=AIzaSyA2WoxRAjLTwrD7upuk9N2qdlcOch3D2wU",
      iceServerTransports: "",
      wssUrl: "wss://apprtc-ws.webrtc.org:443/ws",
      wssPostUrl: "https://apprtc-ws.webrtc.org:443",
      bypassJoinConfirmation: false,
      versionInfo: {
        gitHash: "7341b731567cfcda05079363fb27de88c22059cf",
        branch: "master",
        time: "Mon Sep 23 10:45:26 2019 +0200",
      },
    };

    const initialize = () => {
      // We don't want to continue if this is triggered from Chrome prerendering,
      // since it will register the user to GAE without cleaning it up, causing
      // the real navigation to get a "full room" error. Instead we'll initialize
      // once the visibility state changes to non-prerender.
      if (document.visibilityState === "prerender") {
        document.addEventListener("visibilitychange", onVisibilityChange);
        return;
      }

      appController = new AppController(loadingParams);
      if (appController) {
        // this.setState({ appRtcInit: true }, () => {
        setTimeout(() => {
          if (callback) {
            console.log("Call Init", appController);
            callback();
          }
        }, 500);
        // })
      }
    };

    function onVisibilityChange() {
      if (document.visibilityState === "prerender") {
        return;
      }
      document.removeEventListener("visibilitychange", onVisibilityChange);
      initialize();
    }

    initialize();
  };

  // callType "1" -> video , callType "0" -> audio
  // type -> [0 to call, 1, to accept, 2 to reject]
  render() {
    const { receivedMessage, appRtcInit, isCallTopic } = this.state;
    const payload = receivedMessage;

    const ApprtcVideo = (
      <div id="call-box" className="call-wrapper apprtc" style={{ display: "block" }}>
        <div id="videos">
          <video id="mini-video" autoPlay playsInline muted />
          <video id="remote-video" autoPlay playsInline />
          <video id="local-video" autoPlay playsInline muted />
        </div>

        <div id="room-selection" className="hidden" style={{ visibility: "hidden" }}>
          <h1>AppRTC</h1>
          <p id="instructions">Please enter a room name.</p>
          <div>
            <div id="room-id-input-div">
              <input type="text" id="room-id-input" autofocus />
              <label className="error-label hidden" htmlFor="room-id-input" id="room-id-input-label">
                Room name must be 5 or more characters and include only letters, numbers, underscore and hyphen.
              </label>
            </div>
            <div id="room-id-input-buttons">
              <button id="join-button">JOIN</button>
              <button id="random-button">RANDOM</button>
            </div>
          </div>
          <div id="recent-rooms">
            <p>Recently used rooms:</p>
            <ul id="recent-rooms-list" />
          </div>
        </div>
        <div id="confirm-join-div" className="hidden" style={{ visibility: "hidden" }}>
          <div>
            Ready to join
            <span id="confirm-join-room-span" />?
          </div>
          <button id="confirm-join-button">JOIN</button>
        </div>
        <footer style={{ visibility: "hidden" }}>
          <div id="sharing-div">
            <div id="room-link">
              Waiting for someone to join this room: <a id="room-link-href" target="_blank" />
            </div>
          </div>
          <div id="info-div">
            Code for AppRTC is available from{" "}
            <a href="http://github.com/webrtc/apprtc" title="GitHub repo for AppRTC">
              github.com/webrtc/apprtc
            </a>
          </div>
          <div id="status-div" />
          <div id="rejoin-div" className="hidden">
            <span>You have left the call.</span> <button id="rejoin-button">REJOIN</button>
            <button id="new-room-button">NEW ROOM</button>
          </div>
        </footer>
        <div id="icons" className="hidden">
          <svg id="mute-audio" xmlns="http://www.w3.org/2000/svg" width={48} height={48} viewBox="-10 -10 68 68">
            <title>title</title>
            <circle cx={24} cy={24} r={34}>
              <title>Mute audio</title>
            </circle>
            <path
              className="on"
              transform="scale(0.6), translate(17,18)"
              d="M38 22h-3.4c0 1.49-.31 2.87-.87 4.1l2.46 2.46C37.33 26.61 38 24.38 38 22zm-8.03.33c0-.11.03-.22.03-.33V10c0-3.32-2.69-6-6-6s-6 2.68-6 6v.37l11.97 11.96zM8.55 6L6 8.55l12.02 12.02v1.44c0 3.31 2.67 6 5.98 6 .45 0 .88-.06 1.3-.15l3.32 3.32c-1.43.66-3 1.03-4.62 1.03-5.52 0-10.6-4.2-10.6-10.2H10c0 6.83 5.44 12.47 12 13.44V42h4v-6.56c1.81-.27 3.53-.9 5.08-1.81L39.45 42 42 39.46 8.55 6z"
              fill="white"
            />
            <path
              className="off"
              transform="scale(0.6), translate(17,18)"
              d="M24 28c3.31 0 5.98-2.69 5.98-6L30 10c0-3.32-2.68-6-6-6-3.31 0-6 2.68-6 6v12c0 3.31 2.69 6 6 6zm10.6-6c0 6-5.07 10.2-10.6 10.2-5.52 0-10.6-4.2-10.6-10.2H10c0 6.83 5.44 12.47 12 13.44V42h4v-6.56c6.56-.97 12-6.61 12-13.44h-3.4z"
              fill="white"
            />
          </svg>
          <svg id="mute-video" xmlns="http://www.w3.org/2000/svg" width={48} height={48} viewBox="-10 -10 68 68">
            <circle cx={24} cy={24} r={34}>
              <title>Mute video</title>
            </circle>
            <path
              className="on"
              transform="scale(0.6), translate(17,16)"
              d="M40 8H15.64l8 8H28v4.36l1.13 1.13L36 16v12.36l7.97 7.97L44 36V12c0-2.21-1.79-4-4-4zM4.55 2L2 4.55l4.01 4.01C4.81 9.24 4 10.52 4 12v24c0 2.21 1.79 4 4 4h29.45l4 4L44 41.46 4.55 2zM12 16h1.45L28 30.55V32H12V16z"
              fill="white"
            />
            <path
              className="off"
              transform="scale(0.6), translate(17,16)"
              d="M40 8H8c-2.21 0-4 1.79-4 4v24c0 2.21 1.79 4 4 4h32c2.21 0 4-1.79 4-4V12c0-2.21-1.79-4-4-4zm-4 24l-8-6.4V32H12V16h16v6.4l8-6.4v16z"
              fill="white"
            />
          </svg>
          <svg id="fullscreen" xmlns="http://www.w3.org/2000/svg" width={48} height={48} viewBox="-10 -10 68 68">
            <circle cx={24} cy={24} r={34}>
              <title>Enter fullscreen</title>
            </circle>
            <path
              className="on"
              transform="scale(0.8), translate(7,6)"
              d="M10 32h6v6h4V28H10v4zm6-16h-6v4h10V10h-4v6zm12 22h4v-6h6v-4H28v10zm4-22v-6h-4v10h10v-4h-6z"
              fill="white"
            />
            <path
              className="off"
              transform="scale(0.8), translate(7,6)"
              d="M14 28h-4v10h10v-4h-6v-6zm-4-8h4v-6h6v-4H10v10zm24 14h-6v4h10V28h-4v6zm-6-24v4h6v6h4V10H28z"
              fill="white"
            />
          </svg>
          <svg
            onClick={this.handleCallReject}
            id="hangup"
            className="hidden"
            xmlns="http://www.w3.org/2000/svg"
            width={48}
            height={48}
            viewBox="-10 -10 68 68"
          >
            <circle cx={24} cy={24} r={34}>
              <title>Hangup</title>
            </circle>
            <path
              transform="scale(0.7), translate(11,10)"
              d="M24 18c-3.21 0-6.3.5-9.2 1.44v6.21c0 .79-.46 1.47-1.12 1.8-1.95.98-3.74 2.23-5.33 3.7-.36.35-.85.57-1.4.57-.55 0-1.05-.22-1.41-.59L.59 26.18c-.37-.37-.59-.87-.59-1.42 0-.55.22-1.05.59-1.42C6.68 17.55 14.93 14 24 14s17.32 3.55 23.41 9.34c.37.36.59.87.59 1.42 0 .55-.22 1.05-.59 1.41l-4.95 4.95c-.36.36-.86.59-1.41.59-.54 0-1.04-.22-1.4-.57-1.59-1.47-3.38-2.72-5.33-3.7-.66-.33-1.12-1.01-1.12-1.8v-6.21C30.3 18.5 27.21 18 24 18z"
              fill="white"
            />
          </svg>
        </div>
        <div id="privacy">
          {/* <a href="//www.google.com/accounts/TOS">Terms</a>|
          <a href="//www.google.com/policies/privacy/">Privacy</a>|
          <a href="//github.com/webrtc/apprtc">Code repo</a> */}
        </div>
      </div>
    );

    return (
      <React.Fragment>
        {this.props.children}

        <SimpleDialog open={isCallTopic && (appRtcInit || (payload && Number(payload.type) >= 0 && Number(payload.type) != 2))}>
          {payload ? (
            payload.type == "0" ? (
              <div className="call-wrapper d-flex justify-content-end align-items-center flex-column">
                <p className="caller-info d-flex justify-content-center align-items-center flex-column">
                  {/* {<img src={"https://thumbs.gfycat.com/AgonizingQuestionableBorer-size_restricted.gif"} /> } */}
                  <img
                    style={{
                      borderRadius: "50%",
                      objectFit: "cover",
                      height: "100px",
                      width: "100px",
                    }}
                    src={payload.callerImage}
                  />
                  Call from {payload.callerName}
                </p>
                <div className="call-action">
                  <button className="btn btn-success" onClick={this.handleCallAccept}>
                    Accept
                  </button>
                  <button className="btn btn-danger" onClick={this.handleCallReject}>
                    Reject
                  </button>
                </div>
              </div>
            ) : payload.type == "2" ? (
              <div
                style={{ height: "400px", width: "400px" }}
                className="d-flex align-items-center justify-content-center flex-column h-100"
              >
                <p className="caller-info ">
                  <img
                    style={{
                      borderRadius: "50%",
                      height: "100%",
                      width: "100px",
                    }}
                    src={
                      "https://previews.123rf.com/images/lkeskinen/lkeskinen1707/lkeskinen170711292/82549020-call-ended-rubber-stamp.jpg"
                    }
                  />
                </p>
                <button
                  className="btn btn-success mb-3"
                  onClick={() => {
                    window.location.reload();
                  }}
                >
                  Close
                </button>
              </div>
            ) : (
              ""
            )
          ) : (
            ""
          )}

          {!payload || (payload && payload.type != "2") ? ApprtcVideo : ""}
        </SimpleDialog>
      </React.Fragment>
    );
  }
}

export default AppRtc;
