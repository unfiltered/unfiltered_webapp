import React from "react";
import "./NewsFeed.scss";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import { getAllPosts, likeOrDislikePost, addNewPost, deletePost, getReportPostReasons, EditPost } from "../../../../controller/auth/verification";
import { __BlobUpload, singleVideoUpload } from "../../../../lib/cloudinary-image-upload";
import { getCookie } from "../../../../lib/session";

import { connect } from "react-redux";
import CloseBtn_Pink from "./assets/close.svg";
import ImageCropperUploadImage from "../../../../Components/ImageCropper_UploadImage/App";
import Post from "./Post";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";
import MaterialInput from "../../../../Components/Input/MatUI_Input";

const styles = () => ({
  textField: {
    marginTop: "0px",
    marginBottom: "0px",
    width: "100%",
    fontFamily: "Product Sans",
  },
  input: {
    fontFamily: "Product Sans",
  },
  labelRoot: {
    fontFamily: "Product Sans",
    color: "#f5326f",
  },
  labelFocused: {
    fontFamily: "Product Sans",
    color: "#f5326f",
  },
  filled: {
    fontFamily: "Product Sans",
    color: "#f5326f",
  },
  outlined: {
    fontFamily: "Product Sans",
    color: "#f5326f",
  },
});

class NewsFeed extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.myRef = React.createRef();
    this.state = {
      start: 0,
      end: 20,
      posts: [],
      imageArray: [],
      description: "",
      openModal: false,
      open: false,
      uploadedImageState: false, // in order to call API again.
      snackbarType: "warning",
      openImageCropperModalForImageUpload: false,
      croppedImageObjectToUploadAfterClickingPinkBox: "",
      deletingPostState: false,
      loader: false, // to show CME loader
      uploadingImageState: false, // loader
      reportReasons: [],
      disableBtnForApiCall: false,
      editPostDetails: "",
      isPostInEditMode: false,
      viewMore: false,
    };
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  editPost = () => {
    let type;
    let allPosts = [...this.state.posts];
    this.setState({ uploadingImageState: true });
    if (this.state.imageArray.length > 0 || this.state.imageArray[0].length > 0) {
      try {
        if (
          (this.state.imageArray &&
            this.state.imageArray[0] &&
            this.state.imageArray[0].fileDetails &&
            this.state.imageArray[0].fileDetails.type === "image/png") ||
          (this.state.imageArray &&
            this.state.imageArray[0] &&
            this.state.imageArray[0].fileDetails &&
            this.state.imageArray[0].fileDetails.type === "image/jpg") ||
          (this.state.imageArray &&
            this.state.imageArray[0] &&
            this.state.imageArray[0].fileDetails &&
            this.state.imageArray[0].fileDetails.type === "image/jpeg") ||
          (this.state.imageArray && this.state.imageArray[0] && this.state.imageArray[0][0] && this.state.imageArray[0][0].includes(".jpg"))
        ) {
          type = 3;
        } else {
          type = 4;
        }
      } catch (e) {}
      console.log("[inside type]", type);
      let index = allPosts.findIndex((k) => k.postId === this.state.editPostDetails.postId);
      if (this.state.imageArray[0].fileDetails) {
        __BlobUpload(this.state.imageArray[0].fileDetails).then((res) => {
          let obj = {
            type: type,
            description: this.state.description,
            url: res.body.secure_url,
            postId: this.state.editPostDetails.postId,
          };
          console.log("edit blob upload url", res.body.secure_url);
          allPosts[index].description = this.state.description;
          allPosts[index].type = type;
          allPosts[index].url = [res.body.secure_url];
          allPosts[index].postId = this.state.editPostDetails.postId;
          EditPost(obj, getCookie("token"))
            .then((res) => {
              console.log("allPosts", allPosts);
              this.setState({
                posts: allPosts,
                openModal: false,
                imageArray: [],
                description: "",
                editPostDetails: "",
                uploadingImageState: false,
              });
              console.log("[EDIT POST] success", res);
            })
            .catch((err) => this.setState({ uploadingImageState: false }));
        });
      } else {
        let obj = {
          type: type,
          description: this.state.description,
          url: this.state.imageArray[0],
          postId: this.state.editPostDetails.postId,
        };
        console.log("sending obj", obj);
        allPosts[index].description = this.state.description;
        allPosts[index].type = type;
        allPosts[index].url = this.state.imageArray[0];
        allPosts[index].postId = this.state.editPostDetails.postId;
        EditPost(obj, getCookie("token"))
          .then((res) => {
            console.log("allPosts", allPosts);
            this.setState({
              posts: allPosts,
              openModal: false,
              imageArray: [],
              description: "",
              editPostDetails: "",
              uploadingImageState: false,
            });
            console.log("[EDIT POST] success", res);
          })
          .catch((err) => this.setState({ uploadingImageState: false }));
      }
    }
  };

  // fn -> to like or dislike post , on click on like button, API call triggers.
  functionToLikeOrDislike = (type, postId) => {
    let allPosts = [...this.state.posts];
    let index = allPosts.findIndex((k) => k.postId === postId);
    let prevLiked = allPosts[index].liked;
    console.log(prevLiked);
    likeOrDislikePost(getCookie("token"), type, postId)
      .then((res) => {
        allPosts[index].liked = !prevLiked;
        console.log("after liked", allPosts[index].liked);
        if (allPosts[index].liked) {
          allPosts[index].likeCount = ++allPosts[index].likeCount;
        } else {
          allPosts[index].likeCount = --allPosts[index].likeCount;
        }
        this.setState({ posts: allPosts });
        console.log("functionToLikeOrDislike success", res);
      })
      .catch((err) => console.log("functionToLikeOrDislike", err));
  };

  functionToUpdateComments = (postId, commentLength) => {
    let allPosts = [...this.state.posts];
    let index = allPosts.findIndex((k) => k.postId === postId);
    allPosts[index].commentCount = commentLength;
    this.setState({ posts: allPosts });
  };

  captureEditPostDetails = (data) => {
    this.setState({
      editPostDetails: data,
      openModal: !this.state.openModal,
      description: data.description,
      isPostInEditMode: true,
      imageArray: [data.url],
    });
  };

  // closes modal, and removes selected images from array.
  toggleModal = () => {
    this.setState({
      openModal: !this.state.openModal,
      imageArray: [],
      description: "",
    });
  };

  // opens modal for cropping image
  _openImageCropperModalForImageUploadModal = (data) => {
    if (
      data &&
      data.current &&
      data.current.attributes &&
      data.current.attributes[0] &&
      data.current.attributes[0].ownerElement &&
      data.current.attributes[0].ownerElement.files &&
      data.current.attributes[0].ownerElement.files[0] &&
      data.current.attributes[0].ownerElement.files[0].name &&
      data.current.attributes[0].ownerElement.files[0].name.includes("mp4")
    ) {
      let iArr = { ...this.state };
      let arr = [...iArr.imageArray];
      arr.push({
        fileDetails: data.current.attributes[0].ownerElement.files[0],
        blobUrl: URL.createObjectURL(data.current.attributes[0].ownerElement.files[0]),
      });
      this.setState({ imageArray: arr });
    } else {
      this.setState({
        openImageCropperModalForImageUpload: !this.state.openImageCropperModalForImageUpload,
        croppedImageObjectToUploadAfterClickingPinkBox: data,
      });
    }
  };

  // add's a image preview which is blob (not uploaded image on web). it will only select one image
  // irrespective of changing images (selecting images)
  addNewPostPreview = (event) => {
    let iArr = { ...this.state };
    let arr = [...iArr.imageArray];
    if (event.target.files && event.target.files[0]) {
      console.log("len before", arr);
      if (arr.length === 0) {
        arr.push({
          fileDetails: event.target.files[0],
          blobUrl: URL.createObjectURL(event.target.files[0]),
        });
      } else {
        arr[0] = {
          fileDetails: event.target.files[0],
          blobUrl: URL.createObjectURL(event.target.files[0]),
        };
      }
      this.setState({
        imageArray: arr,
      });
    }
  };

  // add's a image preview which is blob (not uploaded image on web). it will only select one image
  // irrespective of changing images (selecting images)
  addNewPostPreviewForCustomModal = (blobUrl, blobObj) => {
    let arr = [...this.state.imageArray];
    if (arr.length === 0) {
      arr.push({
        fileDetails: blobObj,
        blobUrl: blobUrl,
      });
    } else {
      arr[0] = {
        fileDetails: blobObj,
        blobUrl: blobUrl,
      };
    }
    this.setState({
      imageArray: arr,
    });
  };

  // function to delete post delete's post
  deletePostFunc = (obj, index) => {
    this.setState({ deletingPostState: true });
    deletePost(getCookie("token"), obj.postId)
      .then((res) => {
        let iArr = { ...this.state };
        let arr = [...iArr.posts];
        arr.splice(index, 1);
        this.setState({ posts: arr });
        console.log("success delete post", res);
        this.setState({ deletingPostState: false });
      })
      .catch((err) => console.log("err delete post", err));
  };

  // fn -> to add a new post/story on click of button. if conditions match, then the
  // fn will proceed, it will upload the blob image, and get url from cloudinary
  // and uploads the url in database.
  postNewStory = () => {
    this.setState({ uploadingImageState: true });
    if (this.state.imageArray && this.state.imageArray.length > 0) {
      let type;
      if (
        this.state.imageArray[0].fileDetails.type === "image/png" ||
        this.state.imageArray[0].fileDetails.type === "image/jpg" ||
        this.state.imageArray[0].fileDetails.type === "image/jpeg"
      ) {
        type = 3;
      } else {
        type = 4;
      }
      // commentCount: 0
      // date: "2020-07-09T13:05:40.447Z"
      // description: "old 2"
      // distance: "0.00"
      // likeCount: 0
      // liked: false
      // postId: "5f07162404c8ba58ecd96be7"
      // postedOn: 1594299940447
      // profilePic: "https://randomuser.me/api/portraits/men/13.jpg"
      // status: 0
      // type: "photo Post"
      // typeFlag: 1
      let allPosts = [...this.state.posts];
      this.setState({ disableBtnForApiCall: true });
      if (
        this.state.imageArray &&
        this.state.imageArray[0] &&
        this.state.imageArray[0].fileDetails &&
        this.state.imageArray[0].fileDetails.name &&
        this.state.imageArray[0].fileDetails.name.includes("mp4")
      ) {
        singleVideoUpload(this.state.imageArray[0].fileDetails)
          .then((response) => {
            addNewPost(getCookie("token"), type, this.state.description, response.body.secure_url)
              .then((res) => {
                console.log("add new post response", res);
                allPosts.unshift({
                  commentCount: 0,
                  description: this.state.description,
                  likeCount: 0,
                  liked: false,
                  postId: res.data.postId,
                  postedOn: new Date().getTime(),
                  userName: this.props && this.props.userName,
                  profilePic: this.props && this.props.userPic,
                  url: [response.body.secure_url],
                  typeFlag: type,
                });
                this.setState({
                  posts: allPosts,
                  disableBtnForApiCall: false,
                  openModal: !this.state.openModal,
                  imageArray: [],
                  uploadingImageState: false,
                  likeUserPost: false,
                  description: "",
                });
                setTimeout(() => {
                  this.setState({ open: false });
                }, 700);
              })
              .catch((err) => {
                console.log("err", err);
                this.setState({
                  errorMessage: "Oops! Something went wrong while posting story.",
                  type: "error",
                  open: true,
                  uploadingImageState: false,
                  openModal: !this.state.openModal,
                  imageArray: [],
                  disableBtnForApiCall: false,
                });
                setTimeout(() => {
                  this.setState({ open: false });
                }, 1500);
              });
          })
          .catch((err) => {
            this.setState({
              errorMessage: "Failed to upload image",
              type: "error",
              open: true,
              disableBtnForApiCall: false,
              uploadingImageState: false,
              openModal: !this.state.openModal,
              imageArray: [],
            });
            setTimeout(() => {
              this.setState({ open: false });
            }, 1500);
          });
      } else {
        __BlobUpload(this.state.imageArray[0].fileDetails)
          .then((response) => {
            addNewPost(getCookie("token"), type, this.state.description, response.body.secure_url)
              .then((res) => {
                console.log("add new post response", res);
                allPosts.unshift({
                  commentCount: 0,
                  description: this.state.description,
                  likeCount: 0,
                  liked: false,
                  postId: res.data._id,
                  postedOn: new Date().getTime(),
                  userName: this.props && this.props.userName,
                  profilePic: this.props && this.props.userPic,
                  url: [response.body.secure_url],
                  typeFlag: type,
                });
                this.setState({
                  posts: allPosts,
                  disableBtnForApiCall: false,
                  openModal: !this.state.openModal,
                  imageArray: [],
                  uploadingImageState: false,
                  likeUserPost: false,
                  description: "",
                });
                setTimeout(() => {
                  this.setState({ open: false });
                }, 700);
              })
              .catch((err) => {
                console.log("err", err);
                this.setState({
                  errorMessage: "Oops! Something went wrong while posting story.",
                  type: "error",
                  open: true,
                  uploadingImageState: false,
                  openModal: !this.state.openModal,
                  imageArray: [],
                  disableBtnForApiCall: false,
                });
                setTimeout(() => {
                  this.setState({ open: false });
                }, 1500);
              });
          })
          .catch((err) => {
            this.setState({
              errorMessage: "Failed to upload image",
              type: "error",
              open: true,
              disableBtnForApiCall: false,
              uploadingImageState: false,
              openModal: !this.state.openModal,
              imageArray: [],
            });
            setTimeout(() => {
              this.setState({ open: false });
            }, 1500);
          });
      }
    } else {
      this.setState({
        open: true,
        errorMessage: "Please select image/video to upload.",
        type: "warning",
        disableBtnForApiCall: false,
      });
      setTimeout(() => {
        this.setState({ open: false });
      }, 1500);
    }
  };

  // fn -> to get all posts on componentDidMount()
  GetAllPosts = (start, end) => {
    getAllPosts(getCookie("token"), start, end)
      .then((res) => {
        let oldData = [...this.state.posts, ...res.data.data];
        this.setState({ posts: oldData, loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
      });
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
    this.props.handleWebsiteActiveValue("/app/newsFeed");
    let arr = [];
    this.setState({ loader: true });
    this.props.renderBgColorBasedOnLink(window.location.href);
    this.scrollRef.current.addEventListener("scroll", this.handleScroll, true);
    this.GetAllPosts(this.state.start, this.state.end);
    getReportPostReasons(getCookie("token"))
      .then((res) => {
        res.data.data.map((k) => arr.push({ value: k, label: k }));
        this.setState({ reportReasons: arr });
      })
      .catch((err) => console.log("err"));
  }

  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        if (this.state.posts % 10 == 0) {
          console.log("i am bottom");
          this.setState(
            {
              start: this.state.start + 20,
              end: this.state.end + 20,
              viewMore: false,
            },
            () => {
              this.GetAllPosts(this.state.start, this.state.end);
            }
          );
        }
      }
    }
  };

  componentWillUnmount() {
    this.scrollRef.current.removeEventListener("scroll", this.handleScroll, true);
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  // added a custom modal (image cropper)
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        openImageCropperModalForImageUpload: !this.state.openImageCropperModalForImageUpload,
      });
    }
  }

  closeImageCropModal = () => {
    this.setState({
      openImageCropperModalForImageUpload: !this.state.openImageCropperModalForImageUpload,
    });
  };

  // input handler
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  // delete's preview image when modal is open
  deletePreviewImage = (event) => {
    let iArr = { ...this.state };
    let arr = [...iArr.imageArray];
    let index = arr.indexOf(event);
    arr.splice(index, 1);
    this.setState({ imageArray: arr });
  };

  // fn -> shows warnings if text length is 0 (description) or image is not uploaded
  showWarning = () => {
    this.setState({
      open: true,
      errorMessage: "Select image and write description, in order to post story",
      type: "warning",
    });
  };

  render() {
    let { classes } = this.props;
    let { openModal, posts, snackbarType, errorMessage, deletingPostState } = this.state;
    return (
      <div className="col-12 px-0" id="innerVideoContent">
        {deletingPostState ? (
          <div className="progressBar">
            <CircularProgress />{" "}
            <span className="pl-2">
              <FormattedMessage id="message.deletingPost" />
            </span>
          </div>
        ) : (
          ""
        )}
        <div className="col-12">
          <div className="row border-bottom">
            <div className="col-10 pt-3">
              <h3 className="headerName">
                <FormattedMessage id="UnFiltered News" />
              </h3>
              <div className="">
                <h5 className="subHeaderName">Share You UnFiltered Experience</h5>
              </div>
            </div>
            <div className="col-2 py-3">
              <Button text={"Create Post"} className="d_newsFeed_post_btn" handler={this.toggleModal} />
            </div>
          </div>
        </div>
        <div className="d_newsFeed_scrollable_area" ref={this.scrollRef}>
          {!!posts && posts.length > 0 ? (
            posts.map((k, i) => (
              <Post
                allPosts={posts}
                uploadedImageState={this.state.uploadedImageState}
                reportReasons={this.state.reportReasons}
                k={k}
                userPic={this.props.userPic}
                deletePostFunc={this.deletePostFunc}
                i={i}
                functionToLikeOrDislike={this.functionToLikeOrDislike}
                likeUserPost={this.state.likeUserPost}
                userName={this.props.userName}
                functionToUpdateComments={this.functionToUpdateComments}
                token={getCookie("token")}
                captureEditPostDetails={this.captureEditPostDetails}
              />
            ))
          ) : (
            <div className="col-12 h-75 w-100 d-flex justify-content-center align-items-center flex-column">
              <img src={Icons.NewNewsFeedIcon} alt="no-posts" height={300} width={300} />
              <div className="d_newsFeed_no_posts">No Posts Yet.</div>
            </div>
          )}
        </div>
        <MatUiModal width={500} isOpen={openModal} toggle={this.toggleModal}>
          <div className="col-12 d_newsFeed_modal">
            <div className="d_newsFeed_modal_close_btn" onClick={this.toggleModal}>
              <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
            </div>
            {this.state.uploadingImageState ? (
              <div className="d_newsFeed_uploading_loader">
                <span>
                  <CircularProgress />
                </span>
                <span>
                  <FormattedMessage id="message.pleaseWaitUploadingImage" />
                </span>
              </div>
            ) : (
              <span />
            )}

            <h3 className="d_newsFeed_modal_header py-4">{this.state.isPostInEditMode ? "Edit Post" : <FormattedMessage id="message.createPost" />}</h3>
            <div className="row" style={{ height: "95px" }}>
              <div className="col-2">
                <div className="d_newsFeed_modal_user_image">
                  <img src={this.props.userPic} alt={this.props.userPic} />
                </div>
              </div>
              <div className="col-10">
                <MaterialInput
                  label={<FormattedMessage id="message.enterDesc" />}
                  maxRows={2}
                  value={this.state.description}
                  onChange={this.handleChange("description")}
                  InputProps={{ classes: { input: classes.input } }}
                  className={classes.textField}
                  InputLabelProps={{
                    classes: {
                      root: classes.labelRoot,
                      focused: classes.labelFocused,
                      filled: classes.filled,
                      outlined: classes.outlined,
                    },
                  }}
                />
              </div>
            </div>
            <div
              className={
                this.state.imageArray.length > 0
                  ? "d_newsFeed_modal_upload_image_btn_disabled col-12 "
                  : "col-12 border-bottom d_newsFeed_modal_upload_image_btn"
              }
            >
              <label>
                <FormattedMessage id="message.uploadPhotoVideo" />
                {this.state.imageArray.length > 0 ? (
                  <span />
                ) : (
                  <input
                    type="file"
                    ref={this.myRef}
                    onChange={() => this._openImageCropperModalForImageUploadModal(this.myRef)}
                    id="my_file"
                    accept="image/png, image/jpeg, video/mp4"
                    hidden
                  />
                )}
              </label>
            </div>
            {this.state.imageArray.length > 0 ? (
              <div className="col-12 px-0 pt-4 d_newsFeed_modal_preview_image_box">
                {this.state.imageArray.map((k, i) => {
                  return (
                    <div className="d_newsFeed_modal_preview_image">
                      {(k && k.fileDetails && k.fileDetails.name && k.fileDetails.name.includes(".mp4")) || (k && k[0] && k[0].includes(".mp4")) ? (
                        <video src={k.blobUrl || k} width={150} height={150} alt="blog-video" key={i} autoPlay={true} controls={true} />
                      ) : (
                        <img src={k.blobUrl || k} width={150} alt="blog-img" height={150} key={i} />
                      )}

                      <div onClick={() => this.deletePreviewImage(i)}>
                        <img alt="close-btn" src={CloseBtn_Pink} height={13} width={13} />
                      </div>
                    </div>
                  );
                })}
              </div>
            ) : (
              ""
            )}
            <Button
              className={
                (this.state.imageArray.length === 0 && this.state.description.length < 2) ||
                (this.state.imageArray.length === 1 && this.state.description.length < 2)
                  ? "col-12 d_newsFeed_modal_post_btn_disabled"
                  : "col-12 d_newsFeed_modal_post_btn"
              }
              handler={
                (this.state.imageArray.length === 0 && this.state.description.length < 2) ||
                (this.state.imageArray.length === 1 && this.state.description.length < 2)
                  ? this.showWarning
                  : this.state.isPostInEditMode
                  ? this.editPost
                  : this.postNewStory
              }
              text={<FormattedMessage id="message.newsFeed_Post" />}
              disabled={this.state.disableBtnForApiCall}
            />
          </div>
        </MatUiModal>
        {this.state.openImageCropperModalForImageUpload ? (
          <div className="d_newsFeed_custom_modal" ref={this.setWrapperRef}>
            <ImageCropperUploadImage
              propImage={this.state.croppedImageObjectToUploadAfterClickingPinkBox}
              toggle={this._openImageCropperModalForImageUploadModal}
              signup={false}
              onlyGenerateBlob={true}
              closeImageCropModal={this.closeImageCropModal}
              addNewPostPreviewForCustomModal={this.addNewPostPreviewForCustomModal}
            />
          </div>
        ) : (
          ""
        )}
        <Snackbar open={this.state.open} type={snackbarType} message={errorMessage} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userPic: state.UserProfile.UserProfile.UserProfilePic,
    userName: state.UserProfile.UserProfile.UserProfileName,
  };
};

export default withStyles(styles)(connect(mapStateToProps, null)(NewsFeed));
