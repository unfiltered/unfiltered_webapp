import React, { useEffect, useState, useRef } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import {
  sumbitPostReport,
  getCommentsForSelectedPost,
  addCommentForSelectedPost,
  getLikersOfThePost,
  getCommentsForSelectedPostWithPagination,
} from "../../../../controller/auth/verification";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import moment from "moment";
import { getCookie } from "../../../../lib/session";
import CircularProgress from "@material-ui/core/CircularProgress";
import Select from "react-select";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";
import MaterialInput from "../../../../Components/Input/MatUI_Input";
import Button from "../../../../Components/Button/Button";
import MaterialDropdown from "../../../../Components/Dropdown/MaterialDropdown";

const useStyles = () => ({
  formControl: {
    minWidth: "100%",
  },
  textField: {
    width: "100%",
  },
  menu: {
    right: "10px",
  },
});

function Post({
  allPosts,
  k,
  functionToUpdateComments,
  deletePostFunc,
  i,
  userPic,
  reportReasons,
  captureEditPostDetails,
  uploadedImageState,
  userName,
  functionToLikeOrDislike,
}) {
  const classes = useStyles();
  const scrollRef = useRef(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [commentsSection, setCommentsSection] = useState(false);
  const [comments, setComments] = useState([]);
  const [textComment, setTextComment] = useState("");
  const [PostId, setPostId] = useState("");
  const [postedCommentTrue, setPostedComment] = useState(false);
  const [allLikers, setAllLikers] = useState([]);
  const [likesSection, setLikesSection] = useState(false);
  const [reportPostModal, setReportPostModal] = useState(false);
  const [reason, setReason] = React.useState("");
  const [postDetails, setPostDetails] = useState("");
  const [confirmDeletePost, setConfirmDeletePost] = useState(false);
  const [storePostDetails, setStorePostDetails] = useState({});
  const [indexOfOpenComment, setIndexOfOpenComment] = useState("");
  const [viewMore, setViewMore] = useState(false);
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(20);
  // const [currentLikeState, setCurrentLikeState] = useState("");
  const [values, setValues] = React.useState({
    message: "",
  });

  const toggleDeletePostModal = () => {
    setConfirmDeletePost(!confirmDeletePost);
    handleClose();
  };

  // API call gets triggered on reporting user, and closes modal
  const submitReport = (postId) => {
    sumbitPostReport(getCookie("token"), postId, reason, values.message)
      .then((res) => {
        console.log("[sumbitPostReport]", res);
        closeReportPostModal();
      })
      .catch((err) => console.log("sumbitPostReport err", err));
  };

  // event handler for text <input>
  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
  };

  // selects reason for reporting
  const handleReportChange = (_reason) => {
    setReason(_reason.value);
  };

  // closes dropdown
  const handleClose = () => {
    setAnchorEl(null);
  };

  // delete post from [...posts]
  const deletePost = (obj, index) => {
    deletePostFunc(obj, index);
    handleClose();
    setStorePostDetails({});
  };

  // open dropdown
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  // closes report modal
  const closeReportPostModal = () => {
    setAnchorEl(null);
    setReportPostModal(!reportPostModal);
  };

  // toggles report post modal
  const toggleReportPostModal = (postId) => {
    setAnchorEl(null);
    setPostId(postId);
    setReportPostModal(!reportPostModal);
  };

  // fn -> to get all likers of the post
  const getAllLikers = (postId) => {
    toggleLikesSection();
    getLikersOfThePost(getCookie("token"), postId)
      .then((res) => {
        setAllLikers(res.data.data);
      })
      .catch((err) => console.log("err", err));
  };

  // opens comment section, and calls API to fetch all comments for selected post
  const toggleCommentsSection = (postId, details) => {
    console.log("<toggleCommentsSection>", postId, details);
    setPostId(postId);
    setPostDetails(details);
    setCommentsSection(!commentsSection);
    setTimeout(() => {
      scrollRef.current.addEventListener("scroll", handleScroll, true);
    }, 300);

    if (commentsSection) {
      setPostId("");
      setPostDetails("");
      setCommentsSection(!commentsSection);
    }
  };

  const handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (comments.length % 20 === 0 && comments.length >= 20) {
      if (bottom && !viewMore) {
        if (!viewMore) {
          setOffset(offset + 1);
          setViewMore(false);
          scrollRef.current.scroll({ top: 0, behavior: "smooth" });
        }
      }
    }
  };

  // closes comment modal
  const closeCommetSection = () => {
    scrollRef.current.removeEventListener("scroll", handleScroll, true);
    setPostId("");
    setPostDetails("");
    setComments([]);
    setOffset(0);
    setCommentsSection(!commentsSection);
  };

  // toggles like modal
  const toggleLikesSection = () => {
    setLikesSection(!likesSection);
  };

  useEffect(() => {
    if (commentsSection) {
      try {
        getCommentsForSelectedPostWithPagination(getCookie("token"), PostId, offset, limit)
          .then((res) => {
            // console.log("[RES--- calling API inside", res.data.data);
            if (comments.length % 20 === 0) {
              setComments([...comments, ...res.data.data]);
            } else {
              setComments([...res.data.data]);
              functionToUpdateComments(PostId, res.data.data.length);
            }
          })
          .catch((err) => console.log("err"));
      } catch (e) {}
    }
  }, [PostId && PostId.length > 3, offset, postedCommentTrue]);

  // a form to handle the submitting of the post. API call gets triggered
  const handleSubmit = (event) => {
    event.preventDefault();
    setPostedComment(true);
    addCommentForSelectedPost(getCookie("token"), PostId, textComment)
      .then((res) => {
        setTextComment("");
        setPostedComment(false);
        console.log("post comment success");
      })
      .catch((err) => console.log("post comment error"));
  };
  useEffect(() => {}, [uploadedImageState]);

  /** function to go ahead of post and go back */
  const preNavigatePost = (status) => {
    if (status === 1 && indexOfOpenComment < allPosts.length - 1) {
      if (indexOfOpenComment === 0) return;
      setIndexOfOpenComment(indexOfOpenComment - 1);
      setPostDetails(allPosts[indexOfOpenComment - 1]);
      getCommentsOnNavigation(allPosts[indexOfOpenComment - 1].postId);
    } else if (status === 2 && indexOfOpenComment < allPosts.length - 1) {
      setIndexOfOpenComment(indexOfOpenComment + 1);
      setPostDetails(allPosts[indexOfOpenComment + 1]);
      getCommentsOnNavigation(allPosts[indexOfOpenComment + 1].postId);
    }
    if (indexOfOpenComment === 5 && status === 1) {
      setIndexOfOpenComment(indexOfOpenComment - 1);
      setPostDetails(allPosts[indexOfOpenComment - 1]);
    }
  };

  /** function to get comments on navigation of posts */
  const getCommentsOnNavigation = (navPostId) => {
    getCommentsForSelectedPost(getCookie("token"), navPostId)
      .then((res) => {
        setComments(res.data.data);
        console.log("res", res);
      })
      .catch((err) => console.log("err"));
  };

  /** function stores the index and sets index */
  const __SetIndexOfOpenComment = (id) => {
    let indexOfPost = allPosts.findIndex((k) => k.postId === id);
    setIndexOfOpenComment(indexOfPost);
  };

  return (
    <div className="col-12 border-bottom py-3" key={k.date}>
      {postedCommentTrue ? (
        <div className="progressBar">
          <CircularProgress /> <span className="pl-2">Uploading Post...</span>
        </div>
      ) : (
        ""
      )}
      <div className="row">
        <div className="col-11 d-flex">
          <div className="d_newsFeed_post_user_image">
            <img src={k.profilePic} alt={k.userName} />
          </div>
          <div className="d_newsFeed_post_info">
            <div style={{ textTransform: "capitalize" }}>{k.userName}</div>
            <div>{moment(k.postedOn).fromNow()}</div>
          </div>
        </div>
        <div className="col-1 d-flex justify-content-center align-items-center">
          <MaterialDropdown
            onClick={handleClick}
            icon={Icons.newsFeedThreeDots}
            menuClass={classes.menu}
            anchorEl={anchorEl}
            onClose={handleClose}
            open={Boolean(anchorEl)}
            PaperProps={{
              style: {
                maxHeight: 40 * 3,
                width: 100,
              },
            }}
          >
            {k.userName === userName ? (
              <MenuItem
                onClick={() => {
                  toggleDeletePostModal();
                  setStorePostDetails({ postDetail: k, index: i });
                }}
                key={i}
              >
                <p className="menuItem">
                  <FormattedMessage id="message.deletePost" />
                </p>
              </MenuItem>
            ) : (
              <span />
            )}
            {k.userName !== userName ? (
              <MenuItem onClick={() => toggleReportPostModal(k)}>
                <p className="menuItem">
                  <FormattedMessage id="message.reportPost" />
                </p>
              </MenuItem>
            ) : (
              <span />
            )}
            {k.userName === userName ? (
              <MenuItem
                onClick={() => {
                  captureEditPostDetails(k);
                  setAnchorEl(null);
                }}
              >
                <p className="menuItem">Edit Post</p>
              </MenuItem>
            ) : (
              <span />
            )}
          </MaterialDropdown>
        </div>
      </div>
      <div className="row">
        <div className="col-12 d_newsFeed_post_image py-2">
          {k && k.url && k.url[0].includes("mp4") ? (
            <video width={"100%"} height="500" controls>
              <source src={k && k.url && k.url[0]} type="video/mp4" />
            </video>
          ) : (
            <img src={k && k.url && k.url[0]} alt={k.description} />
          )}
        </div>
        <div className="col-12 d-flex pt-2">
          <div className="postClicks">
            <div className="d-flex">
              <img
                src={k.liked ? Icons.likedLogo1 : Icons.likedLogo}
                height={20}
                width={20}
                alt="Heart-Outline"
                onClick={() => (k.liked ? functionToLikeOrDislike(2, k.postId) : functionToLikeOrDislike(1, k.postId))}
              />
              <span className="pl-2 cursor-pointer" onClick={() => getAllLikers(k.postId)}>
                {k.likeCount} <FormattedMessage id="message.likes" />
              </span>
            </div>
          </div>
          <div
            className="pl-3 postClicks"
            onClick={() => {
              toggleCommentsSection(k.postId, k);
              __SetIndexOfOpenComment(k.postId);
            }}
          >
            <div className="d-flex">
              <img className="d_newsFeed_comment_btn" src={Icons.commentLogo} height={20} width={20} alt="comment" />
              <span className="pl-2">
                {k.commentCount > 0 ? k.commentCount : ""} <FormattedMessage id={k.commentCount > 0 ? "message.comments" : "message.noComments"} />
              </span>
            </div>
          </div>
        </div>
        <div className="col-12">{k.description}</div>
      </div>
      <MatUiModal toggle={toggleDeletePostModal} isOpen={confirmDeletePost} width={500}>
        <div className="col-12 text-center p-4" style={{ position: "relative" }}>
          <div className="d_newsfeed_modal_closeBtn" onClick={toggleDeletePostModal}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
          </div>
          <div className="d_friends_modal_header text-center">Are you sure you want to delete this post?</div>
          <div className="col-12">
            <div className="row">
              <div className="col-6">
                <Button text="Cancel" className="col-12 mt-4 d_friends_modal_accepted_friend" handler={() => toggleDeletePostModal()} />
              </div>
              <div className="col-6">
                <Button
                  text="Yes"
                  className="col-12 mt-4 d_friends_modal_accepted_friend"
                  handler={() => {
                    deletePost(storePostDetails.postDetail, storePostDetails.index);
                    toggleDeletePostModal();
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </MatUiModal>
      <MatUiModal toggle={closeReportPostModal} isOpen={reportPostModal} width={500}>
        <div className="col-12 text-center p-4" style={{ position: "relative" }}>
          <div className="d_newsfeed_modal_closeBtn" onClick={closeReportPostModal}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
          </div>
          <div className="d_friends_modal_header text-center">Report Post</div>
          <div className="col-12 mt-3">
            <div className="row">
              <div className="col-7 d-flex align-items-center">Select Reason for reporting post:</div>
              <div className="col-5">
                <Select onChange={handleReportChange} options={reportReasons}></Select>
              </div>
            </div>
          </div>
          <div className="col-12 mt-2">
            <MaterialInput fullWidth={true} label="Message" className={classes.textField} value={values.message} onChange={handleChange("message")} />
          </div>
          <Button text="Submit Report" className="col-12 mt-4 d_friends_modal_accepted_friend" handler={() => submitReport(PostId.postId)} />
        </div>
      </MatUiModal>
      <MatUiModal toggle={toggleLikesSection} isOpen={likesSection} width={500}>
        <div className="col-12 text-center likesModal p-4" style={{ position: "relative" }}>
          <div className="d_newsfeed_modal_closeBtn" onClick={toggleLikesSection}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
          </div>
          <div className="d_newsFeed_header">Likes</div>
          <div className="d_comments_section">
            {allLikers.length > 0 ? (
              allLikers.map((k) => (
                <div>
                  <div className="col-12 px-0 d-flex comments">
                    <div className="post_image">
                      <img src={k.profilePic} alt={k.likersName} />
                    </div>
                    <div className="d-flex align-items-center">
                      <div>{k.likersName}</div>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <div className="col-12">
                <div>Oops! You haven't got any likes yet.</div>
              </div>
            )}
          </div>
        </div>
      </MatUiModal>
      <MatUiModal toggle={toggleCommentsSection} isOpen={commentsSection} width={"75%"}>
        <div className="col-12 px-0 commentsModal">
          <div
            className="d_comment_modal_close_btn"
            onClick={() => {
              setTextComment("");
              closeCommetSection();
            }}
          >
            <img src={Icons.closeBtn} height={13} width={13} alt="close-button" />
          </div>
          <div className="row mx-0">
            <div className="col-7 px-0 d_post_comment_image_section">
              <span className="left" onClick={() => preNavigatePost(1)}>
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
              </span>
              {postDetails && postDetails.url && postDetails.url[0].includes("mp4") ? (
                <video width={"100%"} height={600} controls loop autoPlay>
                  <source src={postDetails && postDetails.url && postDetails.url[0]} type="video/mp4" />
                </video>
              ) : (
                <img src={postDetails && postDetails.url && postDetails.url[0]} alt={k.description} width={"100%"} height={"100%"} />
              )}
              {/* <img src={postDetails && postDetails.url[0]} alt={postDetails.description} width={"100%"} height={"100%"} /> */}
              <span className="right" onClick={() => preNavigatePost(2)}>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </span>
            </div>
            <div className="col-5 px-0 d_post_comment_section">
              <div className="p-3">
                <div>“{postDetails.description}”</div>
                <div>-{postDetails.userName}</div>
                <div className="likes_n_comments">
                  <span>
                    <img
                      src={postDetails.liked ? Icons.likedLogo1 : Icons.likedLogo}
                      height={23}
                      width={23}
                      alt="Heart-Outline"
                      onClick={() => (postDetails.liked ? functionToLikeOrDislike(2, postDetails.postId) : functionToLikeOrDislike(1, postDetails.postId))}
                    />
                  </span>
                  {postDetails.likeCount} Like(s) {comments && comments.length} Comment(s)
                </div>
              </div>
              <div ref={scrollRef}>
                {comments.length > 0 ? (
                  comments
                    .map((k) => (
                      <div className="py-2 px-3" key={k._id}>
                        <div className="col-12 px-0 d-flex comments">
                          <div className="post_image">
                            <img src={k.profilePic} alt={k.commenterName} />
                          </div>
                          <div>
                            <div>{k.commenterName}</div>
                            <div>{k.comment}</div>
                            <div className="comment_post_date">{moment(k.commentedOn).fromNow()}</div>
                          </div>
                          <div className="pl-2">{}</div>
                          <div></div>
                        </div>
                      </div>
                    ))
                    .reverse()
                ) : (
                  <div className="px-2">
                    <div>Be the first one to comment.</div>
                  </div>
                )}
              </div>
              <div className="submitComment col-12 px-0 d-flex">
                <div>
                  <img src={userPic} alt={userName} />
                </div>
                <form onSubmit={handleSubmit} className="d_post_sumbit_comment_form">
                  <label>
                    <input placeholder="Your Comment" type="text" value={textComment} onChange={(e) => setTextComment(e.target.value)} />
                  </label>
                  <input type="submit" value="Submit" hidden={true} />
                </form>
                <div onClick={handleSubmit}>
                  <img src={textComment.length > 0 ? Icons.postComment : Icons.disabledComment} alt="post-comment" height="50px" width="50px" />
                </div>
              </div>
            </div>
          </div>
          {/* <div className="d_newsFeed_header">Comments</div> */}
        </div>
      </MatUiModal>
    </div>
  );
}

export default Post;
