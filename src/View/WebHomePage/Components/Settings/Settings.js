import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutFromApp, goOfflineSubscribeToTopic } from "../../../../lib/MqttHOC/utils";
import Select from "react-select";
import "./settings.scss";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { removeCookie } from "../../../../lib/session";
import { UserProfiledelete, UserProfileData } from "../../../../controller/auth/verification";
import PrivacyPolicy from "./StaticPages/PrivacyPolicy";
import Licenses from "./StaticPages/Licenses";
import SafetyTips from "./StaticPages/SafetyTips";
import Guidelines from "./StaticPages/Guidelines";
import TermsAndConditions from "./StaticPages/TermsAndConditions";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { localeset } from "../../../../actions/locale";
import { FormattedMessage } from "react-intl";

function styleFn(provided, state) {
  return { ...provided, color: state.isFocused ? "#e31b1b" : "#484848" };
}

const styles = (theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsebasicinfo: false,
      UserFinalData: "",
      confirmDeleteModal: false,
      userID: "",
      viewContent1: [
        {
          name: <FormattedMessage id="message.appsettingtitle1firsttitle" />,
          content: <Guidelines />,
          link: "/app/communityGuidelines",
        },
        {
          name: <FormattedMessage id="message.appsettingtitle1secoundttitle" />,
          content: <SafetyTips />,
          link: "/app/safetyTips",
        },
      ],
      viewContent2: [
        {
          name: <FormattedMessage id="message.appsettingtitle2mainttitle" />,
          content: <PrivacyPolicy />,
          link: "/app/privacyPolicy",
        },
        {
          name: <FormattedMessage id="message.appsettingtitle2firsttitle" />,
          content: <TermsAndConditions />,
          link: "/app/termsAndConditions",
        },
        {
          name: <FormattedMessage id="message.appsettingtitle2secoundtitle" />,
          content: <Licenses />,
          link: "/app/licences",
        },
      ],
      options: [
        {
          value: "en",
          label: "English",
        },
      ],
      content: "",
      selectedOption: { value: "en", label: "English" },
    };
    this.HandleBack = this.HandleBack.bind(this);
  }

  // toggles confirm modal
  toggleConfirmModal = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  componentDidMount() {
    this.props.handleWebsiteActiveValue("/app/settings");
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    UserProfileData().then((data) => {
      this.setState({
        UserFinalData: data.data.data,
        userID: data.data.data._id,
      });
    });
  }

  // fn -> to deactivate account
  handleprofiledelete = () => {
    this.toggleConfirmModal();
    console.log("account deleted");
    UserProfiledelete()
      .then((data) => {
        console.log("account deleted");
        this.handlesignoutsession();
        this.props.history.push("/");
      })
      .catch((err) => {
        console.log("error deleting account");
      });
  };

  // fn to select language, that will change the lang in whole app.
  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.changeLang(selectedOption.value);
    console.log(`Option selected:`, selectedOption);
  };

  // Function to Remove all Saved Cookies
  handlesignoutsession = () => {
    goOfflineSubscribeToTopic(this.state.userID);
    removeCookie("token");
    removeCookie("SignUpseemeid");
    removeCookie("findMateId");
    removeCookie("location");
    removeCookie("citylocation");
    removeCookie("uid");
    localStorage.removeItem("firstName");
    localStorage.removeItem("height");
    localStorage.removeItem("email");
    localStorage.removeItem("picture");
    localStorage.setItem("facebookLogin", false);
    logoutFromApp(this.state.userID);
  };

  // on clicking of view, stores the component into content state
  storeContent = (content) => {
    console.log("content", content);
    this.setState({ content: content });
  };

  // navigates back to profile screen
  HandleBack() {
    this.props.history.push("/app/profile");
  }

  render() {
    return (
      <div className="p-0 col-12 Wsettings_main" id="innerVideoContent">
        {/* Header Module */}
        <div className="col-12 WNavbar_setting border-bottom">
          <div className="py-3 row align-items-center">
            <div className="col-6">
              {this.state.content ? (
                <div className="d-flex align-items-center">
                  <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={() => this.setState({ content: null })}>
                    <img src={Icons.backArrow} width={20} height={20} alt="left-arrow" />
                  </div>
                  <h5 className="m-0">
                    <FormattedMessage id="message.appsettingtitle" />
                  </h5>
                </div>
              ) : (
                <div className="d-flex align-items-center">
                  {/* <div className="left_arrow_border ml-3" style={{ cursor: "pointer" }} onClick={() => this.props.history.push("/app")}>
                    <img src={Icons.backArrow} width={20} height={20} alt="left-arrow" />
                  </div> */}
                  <h5 className="m-0">
                    <FormattedMessage id="message.appsettingtitle" />
                  </h5>
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.content ? (
          <div className="d_settings_selected_content">{this.state.content}</div>
        ) : (
          <div className="col-12 d_settings_content pt-4 mb-5">
            <div className="row pb-3">
              <div className="col-8 d_settings_label">
                <FormattedMessage id="message.changeLang" />
              </div>
              <div className="col-4 d_settings_dropdown">
                <Select styles={styleFn} value={this.state.selectedOption} onChange={this.handleChange} options={this.state.options} />
              </div>
            </div>
            <div className="row py-2 border-bottom">
              <div className="d_setting_dark_label col-12">
                <FormattedMessage id="message.appsettingtitle1" />
              </div>
            </div>
            <div className="row pt-3">
              {this.state.viewContent1.map((k, i) => (
                <div className="py-2 col-12 content_links" onClick={() => this.storeContent(k.content)}>
                  <div className="row">
                    <div className="col-10 d_settings_label pt">{k.name}</div>
                    <div className="col-2 d_settings_label_pink">
                      <FormattedMessage id="message.appsetting_view" /> <i class="fas fa-chevron-right d_settings_chevron"></i>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="row pt-5 pb-2 border-bottom">
              <div className="d_setting_dark_label col-12">
                <FormattedMessage id="message.appsettingtitle2" />
              </div>
            </div>
            <div className="row pt-3">
              {this.state.viewContent2.map((k, i) => (
                <div className="py-2 col-12 content_links" onClick={() => this.storeContent(k.content)}>
                  <div className="row">
                    <div className="col-10 d_settings_label pt">{k.name}</div>
                    <div className="col-2 d_settings_label_pink">
                      <FormattedMessage id="message.appsetting_view" /> <i class="fas fa-chevron-right d_settings_chevron"></i>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <Button text={<FormattedMessage id="message.appsettingdeleteaccount" />} className="d_settings_delete_acc" handler={this.toggleConfirmModal} />
            <MatUiModal toggle={this.toggleConfirmModal} isOpen={this.state.confirmDeleteModal} width={600}>
              <div className="col-12 py-5 deleteAccModal">
                <div className="d_newsFeed_modal_close_btn" onClick={this.toggleConfirmModal}>
                  <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
                </div>
                <div className="text-center">
                  <FormattedMessage id="message.confirmDeleteAccount" /> {this.state.UserFinalData.firstName} UnFiltered{" "}
                  <FormattedMessage id="message.account" />?
                </div>
                <div className="col-12 px-5">
                  <div className="row justify-content-around pt-5">
                    <Button handler={this.toggleConfirmModal} text="Cancel" className="cancelModalButton" />
                    <Button handler={this.handleprofiledelete} text="Confirm" className="confirmModalButton" />
                  </div>
                </div>
              </div>
            </MatUiModal>
          </div>
        )}
      </div>
    );
  }
}

Settings.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeLang: (lang) => dispatch(localeset(lang)),
  };
};

export default connect(null, mapDispatchToProps)(withRouter(withStyles(styles)(Settings)));
