import React from "react";

function Licenses() {
  return (
    <div>
      Below is a non-exhaustive list of trademarks and service marks owned by ChoiceAdz.com, Inc, related to the UnFiltered brand. You may not use the below
      marks without authorization from UnFiltered. If granted authorization and you use the below marks in publications distributed only in the United States,
      include the appropriate TM or ® symbol on at least the first use and on those subsequent uses where the marks appear prominently. For publications
      distributed outside the United States, use of the TM notice symbol is acceptable. It is also appropriate to use, instead of the trademark symbols, the
      trademark legends in the forms listed below: Sample trademark legends: 1) For registered trademarks: UnFiltered is the exclusive registered trademark of
      ChoiceAdz.com, Inc, and is used with permission. 2) For unregistered trademarks: UnFiltered is the exclusive trademark of ChoiceAdz.com, Inc, and is used
      with permission.
    </div>
  );
}

export default Licenses;
