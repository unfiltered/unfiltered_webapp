import React from "react";

const PrivacyPolicy = () => {
  return (
    <div>
      <h2>Privacy Policy</h2>
      <p>
        https://www.unfiltered.com (the “Site”) is a website owned and provided by Unfiltered, Inc. (“UNFILTERED”). The information exchanged and collected on
        or through the Site is governed by this Privacy Policy.  This Privacy Policy describes how your Personal Information (defined below) is collected, used
        and shared when you visit or make purchases from the Site. This Privacy Policy does not cover websites or web pages that you access or link to from this
        Site. If you do not agree with this Privacy Policy, you may not use this Site.
      </p>

      <h3>INFORMATION WE COLLECT</h3>
      <p>
        We do not collect personally identifiable information from you via this Site other than that necessary to create an account, receive, process and
        deliver orders and/or to communicate with you. We may ask you to provide us with certain personally identifiable information that can be used to contact
        or identify you (“Personal Information”). Personally identifiable information may include, but is not limited to: Email Address, First Name and Last
        Name, Phone Number Address, State, Province, ZIP/Postal code, City and payment method information.
      </p>
      <p>
        Our Site, applications, email messages, and advertisements may use cookies and other technologies such as pixel tags and web beacons.  These
        technologies are used for analytic purposes to help us understand usage of our Site.  They also help us to measure the effectiveness of our
        advertisements, content and digital services.  Cookies also provide functional purposes to remember what choices you make on our Site so we can enhance
        our Site, services and/or products.
      </p>

      <h3>Social Media</h3>
      <p>
        You may be able to use your social media login (such as Facebook Login) to create and log into your UnFiltered account. This saves you from having to
        remember yet another user name and password and allows you to share some information from your social media account with us. Other Partners We may
        receive info about you from our partners, for instance where UnFiltered ads are published on a partner’s websites and platforms (in which case they may
        pass along details on a campaign’s success). Information collected when you use our services When you use our services, we collect information about
        which features you’ve used, how you’ve used them and  the devices you use to access our services.
      </p>
      <p>
        See below for more details:  Usage Information We collect information about your activity on our services, for instance how you use them (e.g., date and
        time you logged in, features you’ve been using, searches, clicks and pages which have been shown to you, referring webpage address, advertising that you
        click on) and how you interact with other users (e.g., users you connect and interact with, time and date of your exchanges, number of messages you send
        and receive).
      </p>
      <p>
        Our Site, applications, email messages, and advertisements may use cookies and other technologies such as pixel tags and web beacons.  These
        technologies are used for analytic purposes to help us understand usage of our Site.  They also help us to measure the effectiveness of our
        advertisements, content and digital services.  Cookies also provide functional purposes to remember what choices you make on our Site so we can enhance
        our Site, services and/or products.
      </p>

      <h3>How We Use Your Information</h3>
      <p>
        UNFILTERED may use non-personally identifiable information to improve the content of our Site to address your preferences and attract new users, to
        track user trends and patterns on the Internet so we may inform users of information that may be of interest to them and, in some instances, to monitor
        third parties that have referred you to the Site. rly, if you consent, we may collect your photos and videos (for instance, if you want to publish a
        photo, video or streaming on the services).
      </p>
      <p>
        UNFILTERED may use non-personally identifiable information to improve the content of our Site to address your preferences and attract new users, to
        track user trends and patterns on the Internet so we may inform users of information that may be of interest to them and, in some instances, to monitor
        third parties that have referred you to the Site.
      </p>

      <h3>Sharing Your Personal Information</h3>
      <p>
        We share your Personal Information with third parties to help us use your Personal Information, as described above. For example, we use Shopify to power
        our online store – you can read more about how Shopify uses your Personal Information here: https://www.shopify.com/legal/privacy. We also use Google
        Analytics to help us understand how our customers use the Site – you can read more about how Google uses your Personal Information here:
        https://www.google.com/intl/en/policies/privacy/. You can also opt-out of Google Analytics here: https://tools.google.com/dlpage/gaoptout.
      </p>

      <h3>Third Party Websites</h3>
      <p>
        This Privacy Policy applies solely to information collected on the Site. The Site may contain links to other websites. We are not responsible for the
        privacy practices or content of these other websites; therefore, we ask that you carefully review the policies of any website that you visit through a
        link on the Site.
      </p>
      <h3>Payments</h3>
      <p>
        We may provide paid products and/or services within the Site. In that case, we use third-party services for payment processing (e.g. payment
        processors). We will not store or collect your payment card details. That information is provided directly to our third-party payment processors whose
        use of your personal information is governed by their Privacy Policy. The payment processors we work with are:
      </p>
      <p>Authorize.net. Their Privacy Policy can be viewed at https://www.authorize.net/about-us/privacy.html</p>

      <h3>Security</h3>
      <p>
        To help prevent unauthorized access, help maintain data accuracy, and help ensure the correct use of any information, UNFILTERED uses appropriate
        industry standard procedures designed to safeguard the confidentiality of any information. Unfortunately, no data transmission over the Internet can be
        guaranteed to be 100% secure. As a result, while we are committed to protecting your information, we cannot ensure or warrant the security of any
        information you may transmit to us.
      </p>

      <h3>Privacy of Minors</h3>
      <p>
        The Site does not address anyone under the age of 18 (“Children”). We do not knowingly collect personally identifiable information from anyone under the
        age of 18. If you are a parent or guardian and you are aware that your minor child has provided us with Personal Information, please contact us
        (info@unfiltered.com). If we become aware that we have collected Personal Information from children without verification of parental consent, we will
        take steps to remove that information from our servers.
      </p>
      <h3>Links</h3>
      <p>
        The Site may contain links to other sites, including among others, those of advertisers, other third parties, and companies whose trademarks may appear
        on the Site. We are not responsible for the information collection practices or the content of the sites to which we link
      </p>
      <h3>Data Retention</h3>
      <p>
        We will retain your Personal Information for as long as needed to provide the services available through the Site.  If, at any time after agreeing to
        this Privacy Policy, you: (1) change your mind about receiving information from us; (2) wish to revoke permission for us to retain and use your Personal
        Information; (3) wish to object to processing of your Personal Information; or (4)  wish for us to erase a copy of your data, please make a request to
        UNFILTERED at info@unfiltered.com. If you request erasure of your data, we may retain some of your Personal Information only for legitimate business
        interests, such as fraud detection, prevention, and enhancing the safety of our Website; and to comply with our legal obligations, specifically our tax,
        legal reporting, and auditing obligations.
      </p>

      <h3>Governing Law</h3>
      <p>
        Your access to and use of this Site is governed by and will be construed in accordance with the law of the State of California, without regard to
        principles of conflicts of laws.
      </p>

      <h3>Changes to the Privacy Policy</h3>
      <p>
        We reserve the right to change the terms of this Privacy Policy. Since any changes will be posted on this page, we encourage you to check this page
        regularly. Your continued use of the Site following any changes to this Privacy Policy will constitute your acceptance of such changes.
      </p>

      <h3>Questions & Contact Information</h3>

      <p>2082 Business Center Drive, Ste 180, Irvine, CA 92612</p>
      <p>Effective Date: January 1, 2021 .</p>
    </div>
  );
};

export default PrivacyPolicy;
