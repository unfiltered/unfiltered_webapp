import React from "react";

function TermsAndConditions() {
  return (
    <div>
      <p>
        Unfiltered, Inc. (the “Company,” “we,” or “us”) is making available the website at https://www.unfiltered.com, including all of its content and services
        listed and contained therein, subject to the these terms of use (“Terms” and/or “Terms of Use” and/or this “Agreement”).  Please read these Terms of Use
        carefully before using the services of this website listed above or any affiliated website or mobile application of the Company (the “Site”). PLEASE
        READ THESE TERMS CAREFULLY AND IN THEIR ENTIRETY BEFORE USING OUR SITE AND THE RELATED SERVICES, FEATURES, CONTENT, APPS, WIDGETS OFFERED OR ANY
        PURCHASES YOU MAKE, OR ANY SERVICES OFFERED VIA THE SITE. WE ARE ONLY WILLING TO MAKE OUR SITE AND SERVICES AVAILABLE TO YOU IF YOU ACCEPT ALL OF THESE
        TERMS. BY USING OUR SITE OR BY CLICKING “I ACCEPT” BELOW OR CLICKING A BOX INDICATING YOUR ACCEPTANCE, YOU ARE CONFIRMING THAT YOU UNDERSTAND AND AGREE
        TO BE BOUND BY ALL OF THESE TERMS AND ALL TERMS AND AGREEMENT INCORPORATED HEREIN. IF YOU ARE ACCEPTING THESE TERMS ON BEHALF OF A COMPANY OR OTHER
        LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE LEGAL AUTHORITY TO ACCEPT THESE TERMS ON THAT ENTITY’S BEHALF, IN WHICH CASE “YOU” WILL MEAN THAT ENTITY.
        IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT ACCEPT ALL OF THESE TERMS, THEN WE ARE UNWILLING TO MAKE THIS SITE (INCLUDING OUR SERVICES AND
        PRODUCTS FOUND HEREIN) AVAILABLE TO YOU. IF YOU DO NOT AGREE TO THESE TERMS, YOU MAY NOT ACCESS OR USE OUR SITE OR SERVICES.
      </p>
      <p>
        These Terms of Use apply to all users of the Site. Your access and use of the Site will be subject to the version of the Terms of Use posted on the Site
        at the time of use. We may at our sole discretion change, add, or delete portions of these Terms of Use at any time on a going-forward basis. It is your
        responsibility to check these Terms of Use for changes prior to use of the Site, and in any event your continued use of the Site following the posting
        of changes to these Terms of Use constitutes your acceptance of any changes.
      </p>
      <p>
        We currently make services, products and features available through our Sites, applications, which include mobile software and applications available on
        social networking sites and other platforms, and other downloadable products (the Sites, the applications, the downloadable products and all products,
        services and features provided by us in connection therewith shall be referred to collectively as, the “Services”).  We may offer additional services or
        products or modify or revise any of the Services at our discretion, and this Agreement will apply to all additional services or products and all
        modified or revised Services unless otherwise indicated.  We also reserve the right to cease offering any of the Services.  You agree that we shall not
        be liable to you or any third party for any modification, revision, suspension or discontinuance of any of the Services.
      </p>

      <p>
        THESE TERMS CONTAINS AN ARBITRATION CLAUSE AND CLASS ACTION WAIVER. BY AGREEING TO THESE TERMS, YOU AGREE TO RESOLVE ALL DISPUTES THORUGH INDIVIDUAL
        ARBITRATION, WHICH MEANS THAT YOU WAIVE ANY RIGHTS TO HAVE THOSE DISPUTES DECIDED BY A JUDGE OR JURY, AND THAT YOU WAIVE YOUR RIGHT TO PARTICIPATE IN
        CLASS ACTIONS, CLASS ARBITRATIONS, OR REPRESENTATIVE ACTIONS.
      </p>

      <p>
        IF YOU BECOME A COMPANY SUBSCRIBER AND PAY BY CREDIT OR DEBIT CARD (OR OTHER PAYMENT METHOD ASSOCIATED WITH AN AUTOMATICALLY RENEWING SUBSCRIPTION),
        YOUR SUBSCRIPTION WILL AUTOMATICALLY RENEW FOR CERTAIN PERIODS OF TIME IF YOU DO NOT CANCEL PRIOR TO THE END OF THE TERM.  SEE SECTION 7(a) MORE
        INFORMATION ON THE AUTOMATIC RENEWAL TERMS APPLICABLE TO SUBSCRIPTIONS.
      </p>
      <h3>
        <strong>1. Privacy</strong>
      </h3>
      <p>
        We have developed a Privacy Policy in order to inform you of our practices with respect to the collection, use, disclosure and protection of your
        information. Please refer to our Privacy Policy for information about how the Company collects, uses, stores and discloses personally identifiable
        information from its users.  You understand and agree that if you post any content, information or material of a personal or private nature in your
        profile or in any public areas of the Company or post or provide to the Company any information or content which is intended to be shared, or has been
        shared, with other users, such content, information and materials will be shared with others accordingly, and you hereby consent to such sharing.  You
        understand that by using the Services you consent to the collection, use and disclosure of your personally identifiable information and aggregate data
        as set forth in our Privacy Policy, and to have your personally identifiable information collected, used, transferred to and processed in the United
        States or any other country in which we process your data or make the Services available.  You also consent to receive emails from us in connection with
        the use or promotion of the Services. You can find the Privacy Policy, which is incorporated into this Agreement, on our home
        page https://www.unfiltered.com and by using this Site you agree to the terms of the Privacy Policy.
      </p>
      <p>
        Unless otherwise noted, the Company’s products and Services are intended for personal, non-commercial purposes only. You agree to use the Site and
        Services only for lawful, noncommercial purposes and in compliance with all international, federal, state and local laws. Except as expressly permitted
        in these Terms of Use, you may not use, reproduce, distribute, reverse engineer, modify, copy, publish, display, transmit, adapt, frame, link, rent,
        lease, loan, sell, license or in any way exploit the content of the Site. Use of the Services is void where prohibited.
      </p>
      <p>
        By accessing or using the Site or Services, you represent and warrant that you (a) are above the legal age of majority in your jurisdiction of
        residence, (b) you will only provide us with true, accurate, current and complete information if you register for an Account and/or Orders (defined
        below), (c) you have never been convicted of a felony or any criminal offense characterized as a sexual offense and are not required to register as a
        sex offender with any governmental entity; (d) you have the right, authority and capacity to enter into this Agreement and to abide by all of the terms
        and conditions of this Agreement; and (e) you are not a competitor of us and are not using the Services for reasons that are in competition with us or
        other than for its intended purpose. If we believe or suspect that your information is not true, accurate, current or complete, we may deny or terminate
        your access to the Site or Services (or any portion thereof).
      </p>
      <p>
        You agree that you will only use the Site and Services, including the posting of any content through the Services, in a manner consistent with this
        Agreement and any and all applicable local, state, national and international laws and regulations, including, but not limited to, United States export
        control laws.  Use of the Services is void where prohibited.
      </p>
      <p>
        <b>a. Account Security / Authorization.</b> When you set up an Account, you are required to provide your name and email address and select a password
        (collectively, your “Account Information”), which you may not transfer to or share with any third parties. By creating an account, you agree to provide
        accurate, current and complete account information about yourself, and to maintain and promptly update as necessary your account information. If someone
        accesses our Site or Services using your Account Information, we will rely on that Account Information and will assume that it is in fact you or your
        representative who is accessing the Site and Services. You are solely responsible for any and all use of your Account Information and all orders,
        subscriptions and activities that occur under or in connection with the Account. Without limiting any rights which we may otherwise have, we reserve the
        right to take any and all action, as it deems necessary or reasonable, to ensure the security of the Site and your Account, including without
        limitation, terminating your Account, changing your password, or requesting additional information to authorize transactions on your Account. You agree
        to be responsible for any act or omission of any users that access the Site or services under your Account Information that, if undertaken by you, would
        be deemed a violation of these Terms of Use or applicable law. You may not use anyone else’s Account at any time. Please notify us immediately if you
        become aware that your Account Information is being used without authorization.
      </p>
      <ol type="I">
        <li>use the Service in any unlawful manner or in a manner that is harmful to or violates the rights of others</li>
        <li> engage in any unlawful, harassing, obscene, intimidating, threatening or unwanted conduct</li>
        <li>
          use the Services in any manner that could disrupt, damage, disable, overburden, impair or affect the performance of the Services or interfere with or
          attempt to interfere with any other user’s use of the Services
        </li>
        <li>attempt to interfere with, compromise the system integrity or security or decipher any transmissions to or from the servers running the Service</li>
        <li>impersonate any person or entity, or misrepresent your age, identity, affiliation, connection or association with, any person or entity</li>
        <li>make any commercial use of the Services or promote or solicit involvement in or support of a political platform or religions organization</li>
        <li>
          disseminate another person’s personal information without his or her permission, or collect or solicit another person’s personal information for
          commercial or unlawful purposes
        </li>
        <li>
          use any scripts, bots or other automated technology to crawl, scrape or access the Services or take any action that imposes, or may impose at our sole
          discretion an unreasonable or disproportionately large burden on our infrastructur
        </li>
        <li>collect or solicit personal information about anyone under 18 years of age</li>
        <li>use the Service to redirect users to other sites or encourage users to visit other sites</li>
        <li>
          obtain, harvest or collect email addresses or other contact information of other users from the Services by electronic or other means or use the
          Services to send, either directly or indirectly, any unsolicited bulk e-mail or communications, unsolicited commercial e-mail or communications or
          other spamming or spimming activities
        </li>
        <li>attempt to access any Services or area of the Sites that you are not authorized to access</li>
        <li>attempt to access any Services or area of the Sites that you are not authorized to access</li>
        <li>use another user’s account or permit or allow other people or third parties to access and use the Services via your account; or</li>
        <li>upload invalid data, viruses, worms, or other software agents through the Services.</li>
      </ol>
      <ol>
        <li>
          <b>Your Obligations</b> You will not post any inaccurate, misleading, incomplete or false information or User Content to us or to any other user.  You
          agree that all images posted to your dating profile are of you and were taken within the last 2 years and agree to update your dating profile
          accordingly.  You may be required to supply certain information and post a photo of yourself to use the Services. You will not post, copy, transfer,
          create any derivative works from, distribute, reproduce or show in any manner any copyrighted or trademarked or other proprietary information or
          materials, including any User Content posted by other users, without the prior consent of the owner of such proprietary rights.  You acknowledge that
          information or materials available through the Site or Services may have copyright protection whether or not it is identified as being copyrighted.
        </li>
        <li>
          <b>No Duty to Review User Content</b> Although you understand and acknowledge that the Company has no duty to prescreen, review, control, monitor or
          edit the User Content posted by users and is not liable for User Content that is provided by others, you agree that the Company may, at its sole
          discretion, review, edit, refuse to accept or delete User Content at any time and for any reason or no reason without notice, and you are solely
          responsible for creating backup copies and replacing any User Content you post or store on the Services at your sole cost and expense.  This includes
          the Company’s right to modify, crop or “photoshop” any photos you submit to comply with the Company’s policies, practices and procedures.
        </li>
        <li>
          <b>License Granted to Company</b> By creating an account, you grant to Company a worldwide, perpetual, transferable, sub-licensable, royalty-free
          right and license to host, store, use, copy, display, reproduce, adapt, edit, publish, translate, modify, and distribute your User Content, including
          any information you authorize us to access from Facebook or other third-party source (if applicable), in whole or in part, and in any format or medium
          currently known or developed in the future. Company’s license to your User Content shall be non-exclusive, except that Company’s license shall be
          exclusive with respect to derivative works created through use of our Services. Additionally, so that Company can, at its own discretion, prevent the
          use of your User Content outside of our Services, you authorize Company to act on your behalf with respect to infringing uses of your User Content
          taken from our Site or Services by other users or third parties. This expressly includes the authority, but not the obligation, to send notices
          pursuant to 17 U.S.C. § 512(c)(3) (i.e., DMCA Takedown Notices) on your behalf if your User Content is taken and used by third parties outside of our
          Site or Services. Company is not obligated to take any action with regard to use of your User Content by other users or third parties.
        </li>
        <li>
          <b>Restricted Content</b> You agree that you will not post, transmit or deliver to any other user, either directly or indirectly, any User Content
          that violates any third-party rights or any applicable law, rule or regulation or is prohibited under this Agreement or any other Company policy
          governing your use of the Services and/or is prohibited by any applicable law in the jurisdiction in which you or the Company are located (“Prohibited
          Content”).  Prohibited Content includes, but is not limited to, User Content that:
        </li>
        <li>
          is obscene, pornographic, profane, defamatory, abusive, offensive, indecent, sexually oriented, threatening, harassing, inflammatory, inaccurate,
          misrepresentative, fraudulent or illegal;
          <ol type="I">
            <li>promotes physical harm of any kind against any group or individual</li>
            <li>is intended to, or does, harass, or intimidate any other user or third party</li>
            <li>
              may infringe or violate any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party, including User
              Content that contains others’ copyrighted content (e.g., photos, images, music, movies, videos, etc.) without first obtaining proper permission;
            </li>
            <li>
              contains video, audio, photographs, or images of another person without his or her express written consent (or in the case of a minor, the minor’s
              legal guardian) or otherwise violates anyone’s right of privacy or publicity;
            </li>
            <li>promotes or enables illegal or unlawful activities, such as instructions on how to make or buy illegal weapons or drugs;</li>
            <li>violates someone’s data privacy or data protection rights;</li>
            <li>contains viruses, time bombs, trojan horses, cancelbots, worms or other harmful, or disruptive codes, components or devices;</li>
            <li>contains any advertising, fundraising or promotional content; or</li>
            <li>
              is, in the sole judgment of the Company, objectionable or restricts or inhibits any person from using or enjoying the Services or exposes the
              Company or its users to harm or liability of any type.
            </li>
          </ol>
        </li>
      </ol>
      <p>
        <b>Automatic Renewal of Subscriptions</b> IF YOU PAY FOR A SUBSCRIPTION BY CREDIT OR DEBIT CARD (OR OTHER PAYMENT METHOD IDENTIFIED ON OUR SERVICES OR A
        SOCIAL NETWORKING SITE AS INVOLVING AN AUTOMATICALLY RENEWING SUBSCRIPTION) AND YOU DO NOT CANCEL YOUR SUBSCRIPTION AT LEAST 24 HOURS  PRIOR TO THE END
        OF THE SUBSCRIPTION TERM, YOUR SUBSCRIPTION WILL BE AUTOMATICALLY EXTENDED AT THE END OF EACH TERM FOR SUCCESSIVE RENEWAL PERIODS OF THE SAME DURATION
        AS THE SUBSCRIPTION TERM ORIGINALLY SELECTED.  Unless otherwise indicated in any applicable additional terms or communications we send to your
        registered email address, such renewal will be at the same subscription fee as when you first subscribed, plus any applicable taxes, unless we notify
        you at least 10 days prior to the end of your current term that the subscription fee will increase.  You acknowledge and agree that your payment method
        will be automatically charged for such subscription fees, plus any applicable taxes, upon each such automatic renewal.  You acknowledge that your
        subscription is subject to automatic renewals and you consent to and accept responsibility for all recurring charges to your credit or debit card (or
        other payment method, as applicable) based on this automatic renewal feature without further authorization from you and without further notice except as
        required by law.  You further acknowledge that the amount of the recurring charge may change if the applicable tax rates change or if you are notified
        that there will be an increase in the applicable subscription fees. You agree to provide updated and accurate billing information to the Company.
      </p>
      <p>
        <b>Mobile Software or Applications from iTunes or the App Store</b> The following applies to any Mobile Software you acquire or download from the iTunes
        Store or the App Store provided by Apple (“iTunes-Sourced Software”): You acknowledge and agree that this Agreement is solely between you and the
        Company, not Apple, and that Apple has no responsibility for the iTunes-Sourced Software or content thereof.  Your use of the iTunes-Sourced Software
        must comply with the App Store Terms of Service.  You acknowledge that Apple has no obligation whatsoever to furnish any maintenance and support
        services with respect to the iTunes-Sourced Software.  In the event of any failure of the iTunes-Sourced Software to conform to any applicable warranty,
        you may notify Apple, and Apple will refund the purchase price for the iTunes-Sourced Software to you; to the maximum extent permitted by applicable
        law, Apple will have no other warranty obligation whatsoever with respect to the iTunes-Sourced Software, and any other claims, losses, liabilities,
        damages, costs or expenses attributable to any failure to conform to any warranty will be solely governed by this Agreement and any law applicable to
        the Company as provider of the software.  You acknowledge that Apple is not responsible for addressing any claims of you or any third party relating to
        the iTunes-Sourced Software or your possession and/or use of the iTunes-Sourced Software, including, but not limited to: (i) product liability claims;
        (ii) any claim that the iTunes-Sourced Software fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under
        consumer protection or similar legislation; and all such claims are governed solely by this Agreement and any law applicable to the Company as provider
        of the software.  You acknowledge that, in the event of any third party claim that the iTunes-Sourced Software or your possession and use of that
        iTunes-Sourced Software infringes that third party’s intellectual property rights, the Company, not Apple, will be solely responsible for the
        investigation, defense, settlement and discharge of any such intellectual property infringement claim to the extent required by this Agreement.  You and
        the Company acknowledge and agree that Apple, and Apple’s subsidiaries, are third party beneficiaries of this Agreement as relates to your license of
        the iTunes-Sourced Software, and that, upon your acceptance of the terms and conditions of this Agreement, Apple will have the right (and will be deemed
        to have accepted the right) to enforce this Agreement as relates to your license of the iTunes-Sourced Software against you as a third party beneficiary
        thereof.  Without limiting any other terms of this Agreement, you must comply with all applicable third party terms of agreement when using
        iTunes-Sourced Software.
      </p>
      <h3>Disclaimers.</h3>
      <p>
        <b>“AS IS” AND “AS AVAILABLE”</b>
      </p>
      <p>
        YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR ACCESS TO AND USE OF THIS SITE AND COMPANY’S SERVICES IS AT YOUR SOLE RISK, AND THAT THE SERVICES AND
        PRODUCTS ARE PROVIDED “AS IS” AND “AS AVAILABLE” WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT
        TO APPLICABLE LAW, THE COMPANY MAKES NO EXPRESS WARRANTIES AND HEREBY DISCLAIMS ALL IMPLIED WARRANTIES REGARDING THE SERVICES AND PRODUCTS AND ANY PART
        OF ANY OF THEM, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, CORRECTNESS, ACCURACY, OR
        RELIABILITY. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE COMPANY AND ITS SUBSIDIARIES, AFFILIATES, AND LICENSORS DO NOT REPRESENT OR WARRANT
        TO YOU THAT (A) YOUR ACCESS TO OR USE OF THE SITE OR SERVICES WILL MEET YOUR REQUIREMENTS, (B) YOUR ACCESS TO OR USE OF THE SITE OR SERVICES WILL BE
        UNINTERRUPTED, TIMELY, SECURE OR FREE FROM ERROR, (C) THE SERVICES OR ANY CONTENT, SERVICES, OR FEATURES MADE AVAILABLE ON OR THROUGH THE SITE ARE FREE
        OF VIRUSES OR OTHER HARMFUL COMPONENTS, OR (D) THAT ANY DATA THAT YOU DISCLOSE WHEN YOU USE THE SITE OR SERVICES WILL BE SECURE. SOME JURISDICTIONS DO
        NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES IN CONTRACTS WITH CONSUMERS, SO SOME OR ALL OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.
      </p>
      <p>
        <b>PERSONAL RISKS</b>
      </p>
      <p>
        YOU ACCEPT THE INHERENT SECURITY RISKS OF PROVIDING INFORMATION AND DEALING ONLINE OVER THE INTERNET AND AGREE THAT WE HAVE NO LIABILITY OR
        RESPONSIBILITY FOR ANY BREACH OF SECURITY UNLESS IT IS DUE TO THE COMPANY’S GROSS NEGLIGENCE. YOU WILL IMPLEMENT REASONABLE AND APPROPRIATE MEASURES
        DESIGNED TO SECURE ACCESS TO (A) ANY DEVICE ASSOCIATED WITH YOU AND UTILIZED IN CONNECTION WITH YOUR PURCHASE OF PRODUCTS OR USE OF THE SITE AND
        SERVICES; AND (B) ANY OTHER USERNAME, PASSWORDS OR OTHER LOGIN OR IDENTIFYING CREDENTIALS. IF YOU ARE NO LONGER IN POSSESSION OF ANY DEVICE ASSOCIATED
        WITH ANY ACCOUNT USED FOR THE SITE OR SERVICES OR ARE NOT ABLE TO PROVIDE YOUR LOGIN OR IDENTIFYING CREDENTIALS, THEN YOU MAY LOSE ACCESS TO YOUR
        ACCOUNT. THE COMPANY IS UNDER NO OBLIGATION TO RECOVER ANY INFORMATION RELATING TO THE SITE, SERVICES OR YOUR ACCOUNT.
      </p>
      <ol>
        <li>
          <b>Third Party Websites and Links</b> You may be able to link to third party Websites, services or resources on the Internet from the Site or within
          the Services, and third party Websites, services or resources may contain links to the Site (collectively, “Linked Sites”). We are not responsible for
          the content, availability, advertising, products, services or other materials of any such Linked Sites, or any additional links contained therein, and
          our inclusion of links to the Linked Sites on the Sites does not imply that we endorse or approve of any materials contained on, or accessible
          through, the Linked Sites. In no event shall we be liable, directly or indirectly, to you or any other person or entity for any loss or damage arising
          from or occasioned by the creation or use of the Linked Sites or the information or material accessed through these Linked Sites.  
        </li>
        <li>
          <b>Third Party Merchants</b> The Site may enable you to order and receive products, information and services from businesses that are not owned or
          operated by us. The purchase, payment, warranty, guarantee, delivery, maintenance, and all other matters concerning the merchandise, services or
          information, opinion or advice ordered or received from such businesses are solely between you and such businesses. We do not endorse, warrant, or
          guarantee such products, information, or services. We will not be a party to or in any way responsible for monitoring any transaction between you and
          third-party providers of such products, services, or information, or for ensuring the confidentiality of your transactions.
        </li>
      </ol>
      <h3>Limitation of Liability</h3>
      <p>
        YOU UNDERSTAND AND AGREE THAT THE COMPANY WILL NOT BE LIABLE TO YOU OR TO ANY THIRD PARTY FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR
        EXEMPLARY DAMAGES THAT YOU MAY INCUR, HOWSOEVER CAUSED AND UNDER ANY THEORY OF LIABILITY, INCLUDING, WITHOUT LIMITATION, ANY LOSS OF PROFITS (WHETHER
        INCURRED DIRECTLY OR INDIRECTLY), LOSS OF GOODWILL OR BUSINESS REPUTATION, LOSS OF DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR ANY
        OTHER INTANGIBLE LOSS, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
      </p>
      <p>
        YOU AGREE THAT THE COMPANY’S TOTAL, AGGREGATE LIABILITY TO YOU FOR ANY AND ALL CLAIMS ARISING OUT OF OR RELATING TO THESE TERMS OR YOUR ACCESS TO OR USE
        OF (OR YOUR INABILITY TO ACCESS OR USE) ANY PORTION OF THE SITE OR SERVICES, WHETHER IN CONTRACT, TORT, STRICT LIABILITY, OR ANY OTHER LEGAL THEORY, IS
        LIMITED TO THE GREATER OF (A) THE AMOUNTS YOU ACTUALLY PAID TO US UNDER THESE TERMS FOR THE SERVICES PURCHASED 30 DAYS PRIOR TO THE DATE THE CLAIM
        AROSE, OR (B) ONE HUNDRED DOLLARS ($100), WHICHEVER IS LESS.
      </p>
      <h3>Dispute Resolution</h3>
      <p>
        <b>Agreement to Arbitrate</b>
      </p>
      <p>
        You and the Company agree to resolve any claims relating to these Terms through final and binding arbitration, except that, to the extent you have in
        any manner made unauthorized use or abuse of the Site or Services or violated or threatened to violate any Intellectual Property, we may bring a lawsuit
        solely for injunctive relief to stop unauthorized use or abuse or actual or threatened violation.
      </p>
      <p>
        <b>Arbitration Procedures</b>
      </p>
      <p>
        Arbitration shall be initiated through JAMS. Any dispute, controversy, or claim arising out of or relating to these Terms shall be referred to and
        finally determined by arbitration in accordance with the JAMS Streamlined Arbitration Rules and Procedures in front of one arbitrator. If there is a
        conflict between JAMS Rules and the rules set forth in these Terms, then the rules set forth in these Terms will govern. The JAMS Rules and instructions
        for how to initiate an arbitration are available from JAMS at http://www.jamsadr.com or 1-800-352-5267. Payment of all filing, administration and
        arbitrator fees will be governed by the JAMS Rules. Arbitration under these Terms shall be held in the United States in Orange County, California, under
        California law without regard to its conflict of laws provisions. The arbitration may award on an individual basis the same damages and relief as a
        court (including injunctive relief). Any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction.
      </p>
      <p>
        <b>Authority of Arbitrator</b>
      </p>
      <p>
        The arbitrator will decide the rights and liabilities, if any, of you and the Company, and the dispute will not be consolidated with any other matters
        or joined with any other cases or parties. The arbitrator shall have the authority to grant motions dispositive of all or part of any claim. The
        arbitrator shall have the authority to award monetary damages and to grant any non-monetary remedy or relief available to an individual under applicable
        law, the Arbitration Rules, and these Terms. The arbitrator shall issue a written award and statement of decision describing the essential findings and
        conclusions on which the award is based, including the calculation of any damages awarded. The arbitrator has the same authority to award relief on an
        individual basis as judge in a court of law under the same circumstances. The award of the arbitrator is final and binding upon you and the Company.
      </p>
      <p>
        <b>No Class Actions</b>
      </p>
      <p>
        You may only resolve disputes with us on an individual basis and may not bring a claim as a plaintiff or a class member in a class, consolidated, or
        representative action. Class arbitrations, class actions, private attorney general actions, and consolidation with other arbitrations are not allowed.
      </p>
      <p>
        Waiver of Jury Trial: TO THE FULLEST EXTENT PERMITTED BY LAW, THE PARTIES HEREBY WAIVE THEIR CONSTITUTIONAL AND STATUTORY RIGHTS TO GO TO COURT AND HAVE
        A TRIAL IN FRONT OF A JUDGE OR A JURY, instead electing that all claims and disputes shall be resolved by arbitration. Arbitration procedures are
        typically more limited, more efficient and less costly than rules applicable in court and are subject to very limited review by a court. In the event
        any litigation should arise between you and the Company in any state or federal court in a suit to vacate or enforce an arbitration award or otherwise,
        you and the Company, to the fullest extent permitted by law, waive all rights to a jury trial instead electing that the dispute be resolved by a judge.
        YOU ACKNOWLEDGE THAT YOU HAVE BEEN ADVISED THAT YOU MAY CONSULT WITH AN ATTORNEY IN DECIDING TO ACCEPT THESE TERMS TO ARBITRATE.
      </p>
      <p>
        Opt-Out of Agreement to Arbitrate. You can decline this agreement to arbitrate by emailing us at info@unfiltered.com and providing the requested
        information as follows: (a) your name; (b) the URL of these Terms; (c) your address; (d) your phone number; and (e) a clear statement that you wish to
        opt out of the arbitration agreement in these Terms. The email must be sent no later than thirty (30) days after the date you first accept these Terms.
      </p>
      <h3>Governing Law; General Information</h3>
      <p>
        We control and operate the Site from our offices in the State of California, United States of America. While we invite visitors from all parts of the
        world to visit the Site, visitors acknowledge that the Site, and all activities available on and through the Site, are governed by the laws of the
        United States of America and the laws of the State of California. We do not represent that materials on the Site are appropriate or available for use in
        other locations. Persons who choose to access the Site from other locations do so on their own initiative, and are responsible for compliance with local
        laws. 
      </p>
      <p>
        You agree that the laws of the State of California, excluding its conflict of laws rules, and these Terms of Use, our Privacy Policy and any other
        policies posted from time to time on the Site applicable to your use of the Site shall govern your use of the Site. Please note that your use of the
        Site may be subject to other local, state, national, and international laws. You expressly agree that exclusive jurisdiction for any claim or dispute
        with us (or any of our affiliates) or relating in any way to your use of the Site resides in the courts of the County of Orange, State of California,
        and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of the County of Orange, State of California, in
        connection with any such dispute and including any claim involving us or our affiliates, subsidiaries, employees, contractors, officers, directors,
        attorneys, telecommunication providers and content providers.
      </p>
      <p>
        A printed version of the Terms of Use and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based
        upon or relating to the Terms of Use to the same extent and subject to the same conditions as other business documents and records originally generated
        and maintained in printed form.
      </p>
      <p>
        These Terms of Use are the entire agreement between you and us with respect to the Site and Services, and supersede all prior or contemporaneous
        communications and proposals (whether oral, written or electronic) between you and us with respect to those matters. If any provision of these Terms of
        Use is found to be illegal or unenforceable, the remainder of the Terms of Use shall be unaffected and shall continue to be fully valid, binding, and
        enforceable. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights
        hereunder. No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Use, and neither party has any
        authority of any kind to bind the other in any respect.
      </p>
      <ul>
        <li>
          <b>Rights of Withdrawal and/or Cancellation</b> In addition to the cancellation procedure set forth elsewhere herein, if you are a Company subscriber
          in one of the following states (as determined by the zip code / postal code you use at the time of your subscription), you have the right to
          withdrawal of your subscription in accordance with the applicable terms described below. The date of your subscription is the date that you sign up
          for the subscription through our Services.  Upon withdrawal of your subscription, your subscription benefits will terminate immediately.
        </li>
        <li>
          <b>Arizona</b> You have the right to cancel your subscription and/or upgrade(s) (including upgrades to a subscription and upgrades without a
          subscription), without any penalty or obligation, within three business days, excluding Sundays and holidays, following the date you purchased a
          subscription and/or upgrade(s).  A signed written notice of cancellation (which includes your Company username and the email address used to register
          for the Services) must be sent by certified mail to Unfiltered – Account Services, 2082 Business Center Drive, Ste 180, Irvine, CA 92612, or
          personally delivered to our offices at that address.  Monies paid pursuant to any subscription and/or upgrade(s) for dating services shall be refunded
          within 30 days of receipt of the notice of cancellation.
        </li>
        <li>
          <b>Ohio</b> You may cancel your subscription and/or upgrade(s) (including upgrades to a subscription and upgrades without a subscription) for any
          reason, without any penalty or obligation, until midnight of the third business day after the date you purchased a subscription and/or upgrade(s), or
          if the Services are not available when you purchased a subscription and/or upgrade(s), you may cancel your subscription and/or upgrade(s) prior to
          midnight of the seventh business day after the date on which you receive your first Service.  A written notice of cancellation (which includes your
          Company user name and the email address used to register for the Services) must be sent by certified mail (return receipt requested) or delivered to
          Unfiltered – Account Services, 2082 Business Center Drive, Ste 180, Irvine, CA 92612, or sent by email to support@unfiltered.com or via telegram. 
          Notice of cancellation by certified mail is effective upon the date of post marking.  Delivery is effective when delivered to the address above.  When
          notice is sent by electronic mail, notice is effective when the electronic mail is sent to the Company’s electronic mail address.
        </li>
      </ul>
      <p>
        <b>Miscellaneous State-Specific Provisions</b>If you are a Company subscriber in one of the following states (as determined by the zip code you use at
        the time of your subscription), the provision(s) listed below for such state will apply.  Unless otherwise stated, you may exercise any rights
        applicable to you by providing written notice to us (which includes your Company user name and the email address used to register for the Services) by
        mail at Unfiltered – Account Services, 2082 Business Center Drive, Ste 180, Irvine, CA 92612
      </p>
      <p>
        <b>California</b> The following additional provisions(s) apply if you are a California subscriber (as determined by the zip code you use at the time of
        your subscription):
      </p>
      <ol>
        <li>
          If by reason of death or Disability (as defined below) you are unable to receive all services for which you have contracted, you and your estate may
          elect to be relieved of the obligation to make payments for the Services other than those received before death or the onset of disability. If you
          have prepaid any amount for Services, so much of the amount prepaid that is allocable to Services that you have not received shall be promptly
          refunded to you and your representative.  “Disability” means a condition which precludes you from physically using the Services specified in the
          contract during the term of disability and the condition is verified in writing by a physician designated and remunerated by you.  The written
          verification of the physician shall be presented to the Company.  If the physician determines that the duration of the disability will be less than
          six months, we may extend the term of the contract for a period of six months at no additional charge to you in lieu of cancellation.
        </li>
        <li>
          You acknowledge that the Services are accessible online and are offered in many locations internationally and that therefore, there is no physical
          dating service office. Thus, you acknowledge and agree that for purposes of California Civil Code¬§ 1694.3(b), the term “Dating Service Office” shall
          mean any location where the Services are available.  If you relocate your primary residence further than 50 miles from the Company’s Dating Service
          Office and you are unable to transfer the contract to a comparable facility, you may elect to be relieved of the obligation to make payment for
          services, other than those received prior to that relocation.  Upon such election, if you have prepaid any amount for dating services, so much of the
          amount prepaid that is allocable to services that you have not received shall be promptly refunded to you If you elect to be relieved of further
          obligation pursuant to this subdivision, the Company may charge you a fee of $100.00 or, if more than half the life of the contract has expired, a fee
          of $50.00, not to exceed the amount of the refund to which you are entitled.  Such fee shall be deducted from any refund which the Company is required
          to make to you.
        </li>
        <li>
          You acknowledge that the Services are accessible online and are offered in many locations internationally and that therefore, there is no physical
          dating service office. Thus, you acknowledge and agree that for purposes of the Illinois Dating Referral Services Act, the location of an “enterprise”
          shall mean any location where the Services are available.  If you relocate your primary residence to a location that is more than 25 miles from where
          our Services are comparably offered, you may cancel this contract and shall be liable only for that portion of the charges allocable to the time
          before reasonable evidence of the relocation is presented to the Company plus a fee equal to the lesser of (1) 10% of the unused balance or (2) $50. 
          Such fee shall be deducted from any refund which the Company is required to make to you.
        </li>
        <li>
          If by reason of death you are unable to receive all services for which you have contracted, your estate may elect to be relieved of the obligation to
          make payments for the Services other than those received before death. We shall have the right to require and verify reasonable evidence of the death.
        </li>
        <li>
          If you subscribe for any Paid Services, the Company will provide a minimum of one match to you each month. In the event we do not provide at least one
          match for two or more successive months, you shall have the option to cancel this agreement by notifying us in writing at the address stated in this
          Agreement and to receive a refund of all monies paid pursuant to the cancelled contract; provided, however, that the Company shall retain as a
          cancellation fee 15% of the cash price or a pro rata amount for the number of referrals furnished to you, whichever is greater.  This shall be your
          sole remedy for failure to provide the minimum number of referrals.
        </li>
        <li>
          Except in connection with any merger, sale of company assets, reorganization, financing, change of control or acquisition of all or a portion of the
          Company’s business by another company or third party or in the event of bankruptcy, the Company will not without the prior written consent of the
          purchaser sell, assign or otherwise transfer for business or for any other purpose to any person any information and material of a personal or private
          nature acquired from a purchaser directly or indirectly including but not limited to answers to tests and questionnaires, photographs or background
          information. You acknowledge and agree that if you post any information, including photographs, to the Services for posting on your profile or other
          areas of the Services, such information will be publicly accessible, and you are consenting to the display of such information on the Services.
        </li>
        <li>
          If you permanently relocate your primary residence further than 50 miles from any area in which the Company offers the Services, you may elect to
          terminate your subscription by notifying us in writing at Unfiltered – Account Services, 2082 Business Center Drive, Ste 180, Irvine, CA 92612.  Upon
          such election, your subscription benefits will cease and you will receive a prorated refund of the Subscription Fee paid, less a termination fee of
          $50.00, not to exceed the amount of the refund to which you are entitled.
        </li>
        <li>
          You have the right to place your subscription on hold for a period of up to one year at any time. To do this, you must notify the Company in writing
          (which includes your Company user name and the email address used to register for the Services) at Unfiltered – Account Services, 2082 Business Center
          Drive, Ste 180, Irvine, CA 92612.
        </li>
        <li>
          If by reason of death or disability you are unable to receive the benefits from the Services, the contract shall be proportionally divided by all of
          the days in which the Services were made available to you as part of the contract offering, and you shall be liable for payments only for that portion
          of the contract that can be attributed to the period prior to your actual death or disability, exclusive of any period of time in which the Services
          were made available to you free of charge as part of the contract offering, and within 30 days after receiving notice of your death or disability, we
          shall refund your representative or you the amount paid in excess of the proportional amount. We shall have the right to require and verify reasonable
          evidence of the death or disability.
        </li>
        <li>
          If by reason of death or disability you are unable to receive the benefits from the Services, the contract shall be proportionally divided by all of
          the days in which the Services were made available to you as part of the contract offering, and you shall be liable for payments only for that portion
          of the contract that can be attributed to the period prior to your actual death or disability, exclusive of any period of time in which the Services
          were made available to you free of charge as part of the contract offering, and within 30 days after receiving notice of your death or disability, we
          shall refund your representative or you the amount paid in excess of the proportional amount. We shall have the right to require and verify reasonable
          evidence of the death or disability.
        </li>
        <li>
          <b>Notices</b> Unless otherwise specified in these Terms of Use, all notices under these Terms of Use will be in writing and will be deemed to have
          been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is
          electronically confirmed, if transmitted by facsimile or email; or the day after it is sent, if sent for next day delivery by recognized overnight
          delivery service.
        </li>
      </ol>
      <p>These Terms of Use were last updated January 1, 2021.</p>
    </div>
  );
}

export default TermsAndConditions;
