import React from "react";

function SafetyTips() {
  return (
    <div>
      <h4>Dating Safely</h4>
      <p>
        UnFiltered Brings People Together. With More Matches being Made Daily, Our Community Is Constantly Growing. With So Many People On UnFiltered, User
        Safety Is A Priority. We Understand That Meeting Someone For The ﬁrst Time Whether Online, Through An Acquaintance Or On An Outing Is Intriguing And
        Exciting.
      </p>
      <p>
        However, Your Safety Is Very Important, And Because You Are In Control Of Your UnFiltered Experience, There Are Certain Safety Steps That You Should
        Follow While Dating – Both Online And Offline.
      </p>
      <p>
        We Ask You To Read The Tips And Information Below, And Strongly Urge You To Follow These Guidelines In The Interest Of Your Personal Safety And
        Well-Being. However, You Are Always The Best Judge Of Your Own Safety, And These Guidelines Are Not Intended To Be A Substitute For Your Own Judgment
        And Online Behavior.
      </p>

      <p>Protect Your Finances & Never Send Money Or Financial Information</p>
      <p>
         Never Respond To Any Request To Send Money, Especially Overseas Or Via Wire Transfer, And Report It To Us Immediately – Even If The Person Claims To Be
        In An Emergency. Wiring Money Is Like Sending Cash: The Sender Has No Protections Against Loss And It’s Nearly Impossible To Reverse The Transaction Or
        Trace The Money. For More Information, Click On The Video Below To The U.S. Federal Trade Commission's Advice To Avoid Online Dating Scams, Also
        Available Here, Protect Your Personal Information.
      </p>
      <p>
        Never Give Personal Information, Such As: Your Social Security Number, Credit Card Number Or Bank Information, Or Your Work Or Home Address To People
        You Don’t Know Or Haven’t Met In Person. Note: UnFiltered Will Never Send You An Email Asking For Your Username And Password Information. Any Such
        Communication Should Be Reported Immediately.
      </p>
      <p>
        Be App/Web Wise Block And Report Suspicious Users. You Can Block And Report Concerns About Any Suspicious User Anonymously At Any Time On UnFiltered –
        While Swiping Or After You’ve Matched. Keep Conversations On The Platform. Bad Actors Will Try To Move The Conversation To Text, Personal Email Or Phone
        Conversations.
      </p>
      <p>
        Report All Suspicious Behavior Additionally, Please Report Anyone Who Violates Our Terms Of Use. Examples Of Terms Of Use Violations Include:  Asks You
        For Money Or Donations Requesting Photographs. Minors Using The Platform Users Sending Harassing Or Offensive Messages. Users Behaving Inappropriately
        After Meeting In Person Fraudulent Registration Or Proﬁles.
      </p>
      <p>Spam Or Solicitation, Such As Invitations To Call 1-900 Numbers Or Attempts To Sell Products Or Services.</p>
      <p>
        Offline Behavior First Meetings Are Exciting, But Always Take Precautions And Follow These Guidelines To Help You Stay Safe: Get To Know The Other
        Person Keep Your Communications Limited To The Platform And Really Get To Know Users Online/ Using The App Before Meeting Them In Person. Bad Actors
        Often Push People To Communicate Off The Platform Immediately. It’s Up To You To Research And Do Your Due Diligenc
      </p>
    </div>
  );
}

export default SafetyTips;
