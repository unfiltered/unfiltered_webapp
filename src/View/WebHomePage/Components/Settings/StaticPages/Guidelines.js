import React from "react";

function Guidelines() {
  return (
    <div>
      <h3>Welcome to the UnFiltered community.</h3>
      <p>
        If You’re Honest, Kind And Respectful To Others, You’ll Always Be Welcome Here. If You Choose Not To Be, You May Not Last. Our Goal Is To Allow Users To
        Express Themselves Freely As Long As It Doesn’t Offend Others. Everyone Is Held To The Same Standard On UnFiltered. We’re Asking You To Be Considerate,
        Think Before You Act, And Abide By Our Community Guidelines Both On And Oﬄine. Seriously, Don’t Make Us Swipe Left On You—Because There Will Be No
        Do-Overs Once We Do Because You’re Blocked Forever.
      </p>
      <h3>Nudity or Sexual Content</h3>
      <p>
        UnFiltered Wasn’t Invented To Show Off Your Six Most Provocative Photos. Seriously We’re Not Asking You To Comb Your Hair To One Side And Put On Your
        Sunday Best, Just Try To Keep  It Classy And  Appropriate For Public Consumption. No Nudity, No Sexually Explicit Content, No Sex Toys. Also, If You
        Choose To Sport A Thong, Please Note: Suggestive Poses Are Not Allowed On UnFiltered. (If It’s In The Kama Sutra, It Probably Shouldn’t Be In Your
        Proﬁle Pic.) Photos That Violate These Guidelines May Be Deleted, And The Most Severe Cases May Result In Account Removal. Inappropriateness If You
        Wouldn’t Say It To Someone’s Face, Don’t Say It On UnFiltered. Keep In Mind, There Is A Human Being On The Other End Of Your Screen. Be Respectful With
        Anyone You Interact With In The UnFiltered Community. If You Are Disrespectful, Offensive, Threatening Or Rude To Another User, We Reserve The Right To
        Ban You From UnFiltered.
      </p>
      <h3>Violent or Graphic Content</h3>
      <p>
        We Do Not Tolerate Violent, Graphic Or Obscene Photos Or Proﬁles Containing Hate Speech On UnFiltered, And Will Remove Any Such Content That We
        Identify. Any Content That We Deem Disrespectful To Others In The Community May Be Removed, And The User Banned, Depending On Its Severity. With This In
        Mind, Please Exercise Restraint And Respect For Others In The Community When Posting Hunting Photos, As Many  Users ﬁnd Images Of Dead Animals
        Disturbing.
      </p>
      <h3>Hate</h3>
      <p>
        We Have A Zero-Tolerance Policy On Hate Speech. Any Content That Promotes Or Condones Violence Against Individuals Or Groups Based On Race Or Ethnicity,
        Religion, Disability, Gender, Age, Nationality, Sexual Orientation Or Gender Identity Is Strictly Forbidden And May Result In You Being Permanently
        Banned. Children Unattended Standard Supermarket Rules Apply. No Children Unaccompanied By An Adult — In This Case, We’re Talking About Your Proﬁle
        Photos. Even If It’s You At Age Seven And It’s The Cutest You Ever. This Is To Protect Kids. Their Safety Is More Important Than Your Adorableness Of
        Years Past. We Reserve The Right To Remove These Photos.
      </p>
      <h3>Harassment</h3>
      <p>
        Any Reports Of Stalking, Threats, Bullying, Intimidation, Invading Privacy, Or Revealing Other People's Personal Information Are Taken Very Seriously.
        That Means No Taking Screenshots Of Other People’s Proﬁles Or Conversations And Posting Or Distributing Them Publicly. UnFiltered Reserves The Right To
        Ban Any User That Has Exhibited A Behavior That Is Threatening, Intentionally Malicious, Or Harmful To Other Users. Children Unattended Standard
        Supermarket Rules Apply. No Children Unaccompanied By An Adult — In This Case, We’re Talking About Your Proﬁle Photos. Even If It’s You At Age Seven And
        It’s The Cutest You Ever Had. This Is To Protect Kids. Their Safety Is More Important Than Your Adorableness Of Years Past. We Reserve The Right To
        Remove These Photos.
      </p>
      <h3>Scamming</h3>
      <p>
        Any Reports Of Stalking, Threats, Bullying, Intimidation, Invading Privacy, Or Revealing Other People's Personal Information Are Taken Very Seriously.
        That Means No Taking Screenshots Of Other People’s Proﬁles Or Conversations And Posting Or Distributing Them Publicly. UnFiltered Reserves The Right To
        Ban Any User That Has Exhibited A Behavior That Is Threatening, Intentionally Malicious, Or Harmful To Other Users. Children Unattended Standard
        Supermarket Rules Apply. No Children Unaccompanied By An Adult — In This Case, We’re Talking About Your Proﬁle Photos. Even If It’s You At Age Seven And
        It’s The Cutest You Ever Had. This Is To Protect Kids. Their Safety Is More Important Than Your Adorableness Of Years Past. We Reserve The Right To
        Remove These Photos.
      </p>
      <h3>Copyright and Infringement</h3>
      <p>
        If It Doesn’t Belong To You, It Doesn’t Belong On Your UnFiltered Proﬁle. Any Of Your Proﬁle Content That Someone Else Owns The Copyright To (And Has
        Not Granted You Authorization To Use) May Be Deleted. Phone Numbers Or Private Information No, We Don’t Need Your Digits. Please Do Not Publish A Phone
        Number On Your Proﬁle. UnFiltered Utilizes The Double Opt-In, (Two People Have To Swipe Right On Each Other To Connect), So That Personal Information
        Can Be Shared Privately, If Desired, And Not Broadcast Publicly. Private Information Of Any Sort, Such As Phone Numbers, Social Security Numbers,
        Addresses, Passwords Or ﬁnancial Information Is Subject To Removal And May Result In You Being Banned From UnFiltered
      </p>
    </div>
  );
}

export default Guidelines;
