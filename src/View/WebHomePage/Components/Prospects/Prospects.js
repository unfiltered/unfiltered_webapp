import React, { useState, useEffect } from "react";
import { USER_PROFILE_ACTION_FUN } from "../../../../actions/UserProfile";
import * as actions from "../../../../actions/UserSelectedToChat";
import { connect } from "react-redux";
import "./Prospects.scss";
import "../Chat/Chat.scss";
import { withStyles } from "@material-ui/core/styles";

// Redux Components
import {
  matchFound,
  Profilelikedby,
  UserOnline,
  Superlikedby,
  RecentVisitors,
  MyLikes,
  MySuperLike,
  PassedUser,
  ThumbsUpMe,
  IThumbsUpped,
  History,
} from "../../../../controller/auth/verification";
import ProspectsCard from "./ProspectsCard";
import { Images } from "../../../../Components/Images/Images";
import { FormattedMessage } from "react-intl";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },

  progress: {
    position: "absolute",
    zIndex: 999,
    left: " 45%",
    top: "35%",
  },
});

const onlinefriend = <FormattedMessage id="message.tabpropects1" />;
const superlikedfriend = <FormattedMessage id="message.tabpropects2" />;
const likesmefriend = <FormattedMessage id="message.tabpropects3" />;
const recentvisitorefriend = <FormattedMessage id="message.tabpropects4" />;
const passed = <FormattedMessage id="message.tabpropects5" />;
const mylike = <FormattedMessage id="message.tabpropects6" />;
const mysuperlike = <FormattedMessage id="message.tabpropects7" />;
const myThumbsUp = <FormattedMessage id="message.tabpropects8" />;
const thumbsUppedMe = <FormattedMessage id="message.tabpropects9" />;
const swipeHistory = <FormattedMessage id="message.tabpropects10" />;

function Prospects(props) {
  const [Userlikeme, SetUserlikeme] = useState("");
  const [Usermylike, SetUsermylike] = useState("");
  const [Superlikedbyother, SetSuperlikedbyother] = useState("");
  const [_PassedUser, SetPassedUser] = useState("");
  const [Mysuperlike, SetMysuperlike] = useState("");
  const [recentvisitor, Setrecentvisitor] = useState("");
  const [onlineuser, Setonlineuser] = useState("");

  const [igotThumbsUpped, SetIgotThumbsUpped] = useState("");
  const [thumbsuppedme, SetThumbsUppedMe] = useState("");
  const [history, SetHistory] = useState("");

  // const [onlineuser, Setonlineuser] = useState("");

  const [modelplan, _tooglecoinmodelplan] = useState(false);
  const [loader, setLoader] = useState(true);
  const [content, setContent] = useState("");
  const [label, setLabel] = useState("");
  const [filteredList, setFilteredList] = useState("");
  const [indexOfLabel, setIndexOfLabel] = useState(0);

  const [tabs, setTabs] = useState([
    { label: onlinefriend, content: onlineuser },
    { label: superlikedfriend, content: Superlikedbyother },
    { label: likesmefriend, content: Userlikeme },
    { label: recentvisitorefriend, content: recentvisitor },
    { label: passed, content: _PassedUser },
    { label: mylike, content: Usermylike },
    { label: mysuperlike, content: Mysuperlike },

    { label: myThumbsUp, content: igotThumbsUpped },
    { label: thumbsUppedMe, content: thumbsuppedme },
    { label: swipeHistory, content: history },
  ]);

  // modify the empty content and fill it with API data when component mounts
  const addApiData = (label, data) => {
    const index = tabs.findIndex((emp) => emp.label === label);
    let copiedData = [...tabs];
    copiedData[index].content = data;
    setTabs(copiedData);
  };

  // on click dynamically change data
  const loadData = (content, label, index) => {
    console.log(content, label);
    setContent(content);
    setLabel(label);
    setFilteredList(content);
    setIndexOfLabel(index);
  };

  // componentDidMount
  useEffect(() => {
    props.handleWebsiteActiveValue("/app/prospects");
    props.renderBgColorBasedOnLink(window.location.pathname);
    // Online Users API
    UserOnline()
      .then((data) => {
        Setonlineuser(data.data.data);
        addApiData(onlinefriend, data.data.data);
        loadData(data.data.data, onlinefriend, 0);
        setLoader(false);
      })
      .catch((err) => {
        Setonlineuser([]);
        setLoader(false);
      });
    // Superlikedby API
    Superlikedby()
      .then((data) => {
        SetSuperlikedbyother(data.data.data);
        addApiData(superlikedfriend, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        SetSuperlikedbyother([]);
        setLoader(false);
      });

    // Profilelikedby API
    Profilelikedby()
      .then((data) => {
        SetUserlikeme(data.data.data);
        addApiData(likesmefriend, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    // RecentVisitors API
    RecentVisitors()
      .then((data) => {
        Setrecentvisitor(data.data.data);
        addApiData(recentvisitorefriend, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    // PassedUser API
    PassedUser()
      .then((data) => {
        SetPassedUser(data.data.data);
        addApiData(passed, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    // MyLikes API
    MyLikes()
      .then((data) => {
        SetUsermylike(data.data.data);
        addApiData(mylike, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    // MySuperLike APi
    MySuperLike()
      .then((data) => {
        SetMysuperlike(data.data.data);
        addApiData(mysuperlike, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    IThumbsUpped()
      .then((data) => {
        SetIgotThumbsUpped(data.data.data);
        addApiData(thumbsUppedMe, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    ThumbsUpMe()
      .then((data) => {
        SetThumbsUppedMe(data.data.data);

        addApiData(myThumbsUp, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    History()
      .then((data) => {
        SetHistory(data.data.data);
        addApiData(swipeHistory, data.data.data);
        setLoader(false);
      })
      .catch((err) => {
        setLoader(false);
      });

    matchFound()
      .then((data) => {
        props.dispatch(USER_PROFILE_ACTION_FUN("MatchedUsersData", data.data.data.matchedUsers));
      })
      .catch((err) => {
        setLoader(false);
      });
  }, []);

  const tooglecoinmodelplan = () => {
    _tooglecoinmodelplan(!modelplan);
  };

  // filter users
  const filterList = (event) => {
    var updatedList = content;
    updatedList = updatedList.filter((item) => item.firstName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
    setFilteredList(updatedList);
  };

  const { classes } = props;
  return (
    <div id="innerVideoContent">
      <div className="col-12 custom_border">
        <div className="row">
          <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 py-3 page_header">
            <h3>
              <FormattedMessage id="message.sidepanel_prospects" />
            </h3>
          </div>
          <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 py-3 custom_border_left">
            <div className="row">
              <div className="col-sm-6 col-md-8 col-lg-8 col-xl-8"></div>
              <div className="col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <form className="search_user">
                  <div class="form-group mb-0 ChatPage_FilterUser_Search">
                    <div className="magnifyingGlass_prospects">
                      <img src={Images.searchIcon} alt="search-icon" />
                    </div>
                    <input
                      onChange={(e) => filterList(e)}
                      type="text"
                      class="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Search"
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 px-0">
        <div className="row mx-0">
          <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 px-0 prospects_tabs">
            {tabs.map((k, i) => (
              <div
                className={label === k.label ? "options_selected" : "options"}
                key={k.label}
                onClick={() => loadData(k.content, k.label, i)}
              >
                {k.label}
              </div>
            ))}
          </div>
          <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 custom_border_left px-0 prospects_scrollbar">
            <div className="px-3">
              <ProspectsCard
                indexOfLabel={indexOfLabel}
                label={label}
                data={filteredList}
                loader={loader}
                classes={classes.progress}
                justViewProfileActive={props.justViewProfileActive}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function mapstateToProps(state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    checkIfUserIsProUser: state.ProUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    justViewProfileActive: () => dispatch(actions.justViewProfileActive()),
  };
}

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(Prospects));
