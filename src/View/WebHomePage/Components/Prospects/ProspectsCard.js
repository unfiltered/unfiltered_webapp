import React from "react";
import { Link } from "react-router-dom";
import "./Prospects.scss";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
import { Images } from "../../../../Components/Images/Images";
import { FormattedMessage } from "react-intl";

function ProspectsCard({
  data,
  loader,
  classes,
  label,
  type,
  acceptOrRejectReq,
  storeFriendRequestCount,
  friendReqCount,
  toggleFriendRequestAccepted,
  justViewProfileActive,
  setSelectedUserData,
  setAcceptedFriend,
  indexOfLabel,
}) {
  const renderText = (indexOfLabel) => {
    if (indexOfLabel === 0) {
      return "No online users yet";
    } else if (indexOfLabel === 1) {
      return "You have no superlikes yet..!!";
    } else if (indexOfLabel === 2) {
      return "You haven't received any likes yet..!!";
    } else if (indexOfLabel === 3) {
      return "You have no recent visitors..!!";
    } else if (indexOfLabel === 4) {
      return "You have not passed any users yet!";
    } else if (indexOfLabel === 5) {
      return "You haven’t received any likes yet!";
    } else if (indexOfLabel === 6) {
      return "You have not sent out any superlikes yet!";
    } else if (indexOfLabel === 7) {
      return "You have not received any thumbs up yet..!!";
    } else if (indexOfLabel === 8) {
      return "You have not thumbs upped any user yet..!!";
    } else if (indexOfLabel === 9) {
      return "You haven’t received any swipes yet!";
    }
  };
  const onError = (e) => {
    e.target.src = Images.placeholder;
  };
  if (type === "friends" && label === "Requests") {
    return loader ? (
      <div className={classes}>{/* <img src={Icons.CMeLoader} alt="datum-loader" /> */}</div>
    ) : data && data.length > 0 ? (
      data.map((k, i) => (
        <div key={i}>
          <div index={i} className="prospect_card pl-md-0 px-3 mt-3 col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 float-left">
            {/* <Link to={`/app/user/${k.firstName}/${k._id}`}> */}
            <img src={k.profilePic} onError={onError} alt={k.firstName} title={k.firstName} className="img-fluid" />
            <div className="name_age">{k.firstName}</div>
            <div className="distance">{k.distance ? k.distance.value : ""} Km</div>
            <div className="location">{getCookie("citylocation")}</div>
            {/* </Link> */}
            <div className="row mx-0 d_friends_accept_or_reject_btn">
              <div
                onClick={() => {
                  acceptOrRejectReq(k._id, 1, i);
                  toggleFriendRequestAccepted();
                  setAcceptedFriend(true);
                  setSelectedUserData(k.profilePic);
                }}
              >
                Confirm
              </div>
              <div
                onClick={() => {
                  acceptOrRejectReq(k._id, 2, i);
                  storeFriendRequestCount(friendReqCount - 1);
                }}
              >
                Reject
              </div>
            </div>
          </div>
        </div>
      ))
    ) : (
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
        <div className="Wcommon_nodata">
          <img src={Icons.NoUsers} alt="NoData" title="NoData" height={300} />
          <h5 className="py-3 text-muted">You have no friend requests yet!</h5>
        </div>
      </div>
    );
  } else if (type === "friends" && label === "Friends") {
    return loader ? (
      <div className={classes}>{/* <img src={Icons.CMeLoader} alt="datum-loader" /> */}</div>
    ) : data && data.length > 0 ? (
      data.map((k, i) => (
        <div key={i}>
          <Link to={`/app/user/${k.firstName}/${k.opponentId || k._id}`}>
            <div
              onClick={() => justViewProfileActive()}
              index={i}
              className="prospect_card pl-md-0 px-3 mt-3 col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 float-left"
            >
              <img src={k.profilePic} onError={onError} alt={k.firstName} title={k.firstName} className="img-fluid" />

              <div className="name_age">{k.firstName}</div>

              {/* <div className="distance">{k.distance ? k.distance.value : ""} Km</div> \*/}
              <div className="distance">{`${k && k.distance ? k.distance.value + "Km" : ""}`}</div>
              <div className="location">{getCookie("citylocation")}</div>
            </div>
          </Link>
        </div>
      ))
    ) : (
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
        <div className="Wcommon_nodata">
          <img src={Icons.NoUsers} alt="NoData" title="NoData" height={300} />
          <h5 className="py-3 text-muted">You have no friends yet!</h5>
        </div>
      </div>
    );
  } else {
    return loader ? (
      <div className={classes}>{/* <img src={Icons.CMeLoader} alt="datum-loader" /> */}</div>
    ) : data && data.length > 0 ? (
      data.map((k, i) => (
        <Link to={`/app/user/${k.firstName}/${k.opponentId || k._id}`}>
          <div
            onClick={() => justViewProfileActive()}
            index={i}
            className="prospect_card pl-md-0 px-3 mt-3 col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 float-left"
            key={i}
          >
            <img src={k.profilePic} alt={k.firstName} title={k.firstName} onError={onError} className="img-fluid" />
            <div className="name_age">{k.age ? k.firstName + "," + k.age.value : k.firstName}</div>
            <div className="distance">{k.distance ? k.distance.value + " Km" : ""} </div>
            <div className="location">{getCookie("citylocation")}</div>
          </div>
        </Link>
      ))
    ) : (
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
        <div className="Wcommon_nodata">
          <img src={Icons.NoUsers} alt="NoData" title="NoData" height={300} />
          <h5 className="py-3 text-muted">{renderText(indexOfLabel)}</h5>
        </div>
      </div>
    );
  }
}

export default ProspectsCard;
