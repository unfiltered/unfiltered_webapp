
import Paho from "paho-mqtt";
import MQTT from "paho-mqtt";

// Redux Components
import { getCookie } from "./session";

window.Paho = { MQTT };

let client = new Paho.Client("wss://devmqtt.cmeedating.com:2083/mqtt", "clientID-" + new Date().getTime().toString());

let uniqueId = "";
let mqttStatusConnected = true;
let x = 0;

let glob = 0;



const hasUndefined = (target) => {
    for (var member in target) {
        if (target[member] == undefined) return true;
    }
    return false;
};

const options = {
    userName: "3embed",
    password: "3embed",
    useSSL: true,
    keepAliveInterval: 10,
    mqttVersion: 3,
    reconnect: true,
    cleanSession: true,
    onSuccess: (message) => {
        console.log("mqtt connected");
        setTimeout(() => {
            console.log("key --> ", glob);
            // client.subscribe("#");

            var subscribeOptions = {
                invocationContext: { foo: glob }, // Passed to success / failure callback
                onSuccess: (message) => {
                    console.log("mqtt subscribed to :: ", message);
                },
                onFailure: (message) => {
                    console.log("mqtt subscribe  failed", message);
                },
            };

            client.subscribe("Calls" + "/" + glob, subscribeOptions);
            // client.subscribe("Acknowledgement" + "/" + getCookie("uid"));
            // client.subscribe("Calls" + "/" + getCookie("uid"));
            // client.subscribe("UserUpdates" + "/" + getCookie("uid"));
            // client.subscribe("FetchMessages" + "/" + getCookie("uid"));
            // client.subscribe(getCookie("uid"));
            // publish("Login");
        }, 1500);
    },
    onFailure: (message) => {
        console.log("Connection failed:.", message);
    },
};

const logs = {
    invocationContext: {},
    timeout: 10000000,
    onSuccess: (message) => {
        console.log("success", message);
    },
    onFailure: (message) => {
        console.log("error", message);
    },
};

export const Mqtt = (callbackOnMessage, key) => {
    console.log("value of x: " + x);
    glob = key;
    if (mqttStatusConnected == true) {
        // to ensure it fires only once

        if (!client.isConnected()) {
            client.connect(options);
            mqttStatusConnected = false;
        }
    }

    client.onMessageArrived = (message) => {
        let mqttData = JSON.parse(message.payloadString);
        let CallBack = callbackOnMessage ? callbackOnMessage({ payload: mqttData, topic: message.topic }) : "";
    };
};

const publish = (type) => {
    let publishObj = {};
    let uid = getCookie("uid");
    if (type === "Login") {
        publishObj = {
            status: 1,
            onlineStatus: 1,
            userId: uid,
            _id: uid,
            lastSeenEnabled: true,
            lastOnline: new Date().getTime(),
        };
        if (hasUndefined(publishObj)) {
            console.log("cannot publish onlineStatus on undefined");
        } else {
            client.publish();
        }
    }
};

export const publishTopic = (key, publishObj = {}) => {
    console.log("Publishing On:: ", `Calls/${key}`);
    client.publish(`Calls/${key}`, JSON.stringify(publishObj), 0, false);
}

// export const sendMessageUsingMQTT = (senderId, userImage, chatId, isMatchedUser, payload, timestamp, name) => {
export const sendMessageUsingMQTT = (obj) => {
    console.log("obj ->", obj);
    if (hasUndefined(obj)) {
        console.log("cannot send any message of sendMessageUsingMQTT on undefined");
    } else {
        // funcToGenerateUniqueId(obj.id);
        client.publish(`Message/${obj.to}`, JSON.stringify(obj), 1, false);
        client.publish();
    }
};

export const ackOfMessage = (receiverId, senderId, msgId) => {
    let arr = [];
    arr.push(msgId);
    if (receiverId !== undefined || senderId !== undefined) {
        let obj2 = {};
        obj2["from"] = senderId;
        obj2["msgIds"] = arr;
        obj2["to"] = receiverId;
        obj2["status"] = "2";
        obj2["deliveryTime"] = new Date().getTime();
        if (hasUndefined(obj2)) {
            console.log("cannot publish ackOfMessage");
        } else {
            client.publish(`Acknowledgement/${receiverId}`, JSON.stringify(obj2), 2, false);
        }
    }
};

export const LogoutMQTTCalled = (userID) => {
    let publishObj = {};
    publishObj["status"] = 0;
    publishObj["onlineStatus"] = 0;
    publishObj["_id"] = userID;
    publishObj["timestamp"] = new Date().getTime();
    publishObj["userId"] = userID;
    publishObj["lastSeenEnabled"] = true;
    publishObj["lastOnline"] = new Date().getTime();
    if (hasUndefined(publishObj)) {
        console.log("cannot logout on undefined object LogoutMQTTCalled");
    } else {
        client.unsubscribe("Message" + "/" + userID, logs);
        client.unsubscribe("Acknowledgement" + "/" + userID, logs);
        client.unsubscribe("Calls" + "/" + userID, logs);
        client.unsubscribe("UserUpdates" + "/" + userID, logs);
        client.unsubscribe("FetchMessages" + "/" + userID, logs);
        client.unsubscribe(userID, logs);
        client.publish(`OnlineStatus/${userID}`, JSON.stringify(publishObj), 0, true);
        console.log("LogoutMQTTCalled(id)", userID);
        setTimeout(() => {
            client.disconnect();
        }, 300);
    }
};
