import React, { Component } from "react";
import RTCVideo from "./RTCVideo.jsx";
import Form from "./Form.jsx";
import Websocket from "./Websocket.jsx";
import PeerConnection from "./PeerConnection.jsx";

// import swal from "sweetalert";
import CallEndIcon from "@material-ui/icons/CallEnd";
import ShareScreenIcon from "./ShareScreenIcon";
import ShareOffIcon from "./ShareOffIcon";
import { Button } from "@material-ui/core";
import "./css/RTCMesh.css";

import {
  DEFAULT_CONSTRAINTS,
  DEFAULT_ICE_SERVERS,
  TYPE_ROOM,
  TYPE_ANSWER,
  TYPE_PEER_CONN,
} from "./functions/constants";

/*
* // Utils makes communication between client and server
 * @createPayload: creates payload for @createMessage with roomKey, socketId and message
 * @createMessage: creates message with message type and payload
 * socket.send() sends this message to server where
 * @socket is = new WebSocket(URL) websocket connection
 */
import {
  // buildServers,
  generateRoomKey,
  createMessage,
  createPayload,
} from "./functions/utils";
// import { Mqtt, publishTopic } from "./mqtt-connector";
import { Mqtt, publishTopic } from "../../../../lib/Mqtt/Mqtt";
import CustomizedDialogs from "../../../../Components/Dialogs/Dialog.js";
import { getIceServers } from "../../../../services/webrtc.js";

var uniqid = require('uniqid');


const DATUM_API_ICE = [
  {
    "urls": [
      "stun:bturn2.xirsys.com"
    ]
  },
  {
    "username": "AjxdMUu_pSHgQBzBgc4jTOMcV1Z-_I1ozjQIoggBpNBBToJdD8ERSrluHqLuVR8hAAAAAF5DnDlkdWJseQ==",
    "credential": "95331758-4d61-11ea-bd26-9646de0e6ccd",
    "urls": [
      "turn:bturn2.xirsys.com:80?transport=udp",
      "turn:bturn2.xirsys.com:3478?transport=udp",
      "turn:bturn2.xirsys.com:80?transport=tcp",
      "turn:bturn2.xirsys.com:3478?transport=tcp",
      "turns:bturn2.xirsys.com:443?transport=tcp",
      "turns:bturn2.xirsys.com:5349?transport=tcp"
    ]
  }
]

class RTCMesh extends Component {
  constructor(props) {
    super(props);

    /*
    /* # props are not is use it can valued from app.js
    * @mediaConstrains: media access fro html video tag 
    * @iceServers: iceServers url, which is assigning from constant.js(default)
    * @URL: backend server
    */
    const { mediaConstraints, iceServers } = props; // URL was also there
    /*
     * build iceServers config for RTCPeerConnection
     * @iceServerURLs: are the STUN and TURN server credentials
     * @DEFAULT_ICE_SERVERS: default ice server url (stun)
     */

    // const iceServerURLs = buildServers(iceServers);
    const iceServerURLs = iceServers;

    this.state = {
      iceServers: DATUM_API_ICE || iceServerURLs || DEFAULT_ICE_SERVERS,
      mediaConstraints: mediaConstraints || DEFAULT_CONSTRAINTS,
      localMediaStream: null,
      remoteMediaStream: null,
      roomKey: null,
      socketID: null,
      connectionStarted: false,
      text: "",
      isShare: false,
      userName: "",
      receivedMessage: {}
    };

    // asking for video cam(true) or display share(false)
    this.wantCamera = true;

    // creating websocket connection to send data to server
    this.socket = new WebSocket(this.props.URL);
    // // making rtcPeerConnection with ice server to stream data
    this.rtcPeerConnection = new RTCPeerConnection({
      iceServers: this.state.iceServers,
    });
  }

  /*
   * after entering to room it will start localStreaming and open camera
   * @formHandleOffer: boolean value true->mediaStream, false->localStream
   * for only entering to room audio is muted and after connecting to peer audio is true
   */

  openCamera = async (fromHandleOffer) => {
    const { mediaConstraints, localMediaStream } = this.state;
    this.setState({ showVideo: true });
    try {
      if (!localMediaStream) {
        let mediaStream;
        if (this.wantCamera)
          mediaStream = await navigator.mediaDevices.getUserMedia(
            mediaConstraints
          );
        else
          mediaStream = await navigator.mediaDevices.getDisplayMedia(
            mediaConstraints
          );
        console.log(mediaStream.getTracks());
        return fromHandleOffer === true
          ? mediaStream
          : this.setState({ localMediaStream: mediaStream });
      }
    } catch (error) {
      console.error("getUserMedia Error: ", error);
    }
  };

  // method invoked when peer entered in your room
  // data coming from Websocket.jsx
  handleOffer = async (data) => {
    const { localMediaStream, roomKey, socketID } = this.state;

    console.log("Inside handleConnectionReady: ", roomKey, socketID);

    const { payload } = data;
    await this.rtcPeerConnection.setRemoteDescription(payload.message);

    let mediaStream = localMediaStream;

    if (!mediaStream) mediaStream = await this.openCamera(true);
    // console.log(mediaStream.getTracks())
    this.setState(
      { connectionStarted: true, localMediaStream: mediaStream },
      async function () {
        const answer = await this.rtcPeerConnection.createAnswer();
        await this.rtcPeerConnection.setLocalDescription(answer);
        const payload = await createPayload(roomKey, socketID, answer);
        const answerMessage = await createMessage(TYPE_ANSWER, payload);
        this.socket.send(JSON.stringify(answerMessage));
      }
    );
  };

  handleAnswer = async (data) => {
    const { payload } = data;
    await this.rtcPeerConnection.setRemoteDescription(payload.message);
  };

  handleIceCandidate = async (data) => {
    const { message } = data.payload;
    const candidate = JSON.parse(message);
    await this.rtcPeerConnection.addIceCandidate(candidate);
  };

  handleShareDisplay = async () => {
    // console.log('sharing display')
    this.wantCamera = !this.wantCamera;
    if (this.state.connectionStarted) {
      const { mediaConstraints, localMediaStream } = this.state;
      let mediaStream;
      if (this.wantCamera) {
        mediaStream = await navigator.mediaDevices.getUserMedia(
          mediaConstraints
        );
        this.setState({ isShare: false });
      } else {
        mediaStream = await navigator.mediaDevices.getDisplayMedia(
          mediaConstraints
        );

        this.setState({ isShare: true });
      }

      // console.log(localMediaStream.getAudioTracks())

      //current mediaStream video track
      let screenStream = await mediaStream.getVideoTracks()[0];
      // console.log("RTCMesh -> handleShareDisplay -> screenStream", mediaStream.getTracks(),screenStream)

      // it has both sender and receivers data
      const transceiver = await this.rtcPeerConnection.getTransceivers()[0];
      // console.log(this.rtcPeerConnection.getTransceivers())

      //removing video from localMediaStream to show display sharing screen
      await localMediaStream.removeTrack(localMediaStream.getTracks()[0]);
      // console.log(localMediaStream.getTracks())

      //for the first time localMediaStream has two elements (cond to remove that)
      if (localMediaStream.getTracks()[0]) {
        await localMediaStream.removeTrack(localMediaStream.getTracks()[0]);
      }

      //adding sharing display media to localMediaStream
      await localMediaStream.addTrack(screenStream);

      //replacing peer's display with sender's display stream
      await transceiver["sender"].replaceTrack(screenStream);
    }
  };

  //set roomKey to state and send to server by creating message
  sendRoomKey = () => {
    const { roomKey, socketID } = this.state;
    console.log("Inside handleConnectionReady: ", socketID);

    if (!roomKey) {
      const key = generateRoomKey();
      const roomData = createMessage(TYPE_ROOM, createPayload(key, socketID));
      this.setState({ roomKey: key });
      this.socket.send(JSON.stringify(roomData));
      alert(key);
    }
  };

  //socket id is provided by server after creating room, set to state
  handleSocketConnection = (socketID) => {
    console.log("Inside handleConnectionReady: ", socketID);
    this.setState({ socketID });
  };

  // connection message coming from server to start streaming
  handleConnectionReady = (message) => {
    console.log("Inside handleConnectionReady: ", message);
    if (message.startConnection) {
      this.setState({ connectionStarted: message.startConnection });
    }
  };

  addRemoteStream = (remoteMediaStream) => {
    console.log(remoteMediaStream.getTracks())
    this.setState({ remoteMediaStream });
  };

  /*
   * method invoke on clicking call end button
   * closing peers connection
   * alerting peer that call ended
   * getting user back to form page
   */

  handleCloseDisplay = async () => {
    // console.log('closing display')
    await this.rtcPeerConnection.close();
    const answer = { connection: "closed", message: "connection closed" };
    const { opponentData } = this.props;
    const { firstName } = opponentData || {};
    publishTopic(opponentData, {
      type: 2,
      message: "byy byy"
    })
    const payload = await createPayload(
      this.state.roomKey,
      this.state.socketID,
      answer
    );
    const answerMessage = createMessage(TYPE_PEER_CONN, payload);
    this.socket.send(JSON.stringify(answerMessage));
    window.location.reload();
  };

  /*
   * alerting peer that call ended
   * getting user back to form page
   */

  handleCloseConnection = async (e) => {
    console.log("call disconnected !!!!");
    // await swal(
    //   "Call ended !",
    //   "Please enter OK button to go back to Home page!",
    //   "success"
    // );
    await window.location.reload();
  };

  //invoked after submitting room id
  handleSubmit = async (room) => {
    // let e = event ? event.preventDefault() : '';
    const { text, socketID } = this.state;
    const { opponentData, currentUserData } = this.props;
    const { firstName, senderId, recipientId } = opponentData || {};
    const { userId, UserProfileName, UserProfilePic } = currentUserData || {};

    let roomId = "656315109" || text && text.trim() || room;
    // send the roomKey
    // Remove leading and trailing whitespace
    if (roomId) {
      const roomKeyMessage = await createMessage(
        TYPE_ROOM,
        createPayload(roomId, socketID)
      );

      publishTopic(recipientId, {
        type: 0,
        callType: "1",
        callerName: UserProfileName,
        receiverId: recipientId,
        callerId: userId,
        callInit: true,
        callerImage: UserProfilePic,
        callId: roomId,
        roomId: roomId,
        dateId: "",
        callerIdentifier: userId
      })
      // publishTopic(userId, {
      //   // type: 0,
      //   callInit: true
      // })
      // publishTopic(recipientId, {})

      console.log("ping on submit --> ", roomKeyMessage, opponentData, currentUserData);

      if (this.state.socketID) {
        this.socket.send(JSON.stringify(roomKeyMessage));
      }
    }
    this.setState({ text: "", roomKey: roomId, callEnded: false, receivedMessage: { ...this.state.receivedMessage, type: 99 } });
    this.openCamera();
  };

  handleCallAccept = async (event) => {
    let e = event ? event.preventDefault() : '';
    const { text, socketID, receivedMessage } = this.state;
    // send the roomKey
    // Remove leading and trailing whitespace

    const { opponentData, currentUserData } = this.props;
    const { firstName, senderId } = opponentData || {};
    const { userId } = currentUserData || {};

    if (text.trim()) {
      const roomKeyMessage = await createMessage(
        TYPE_ROOM,
        createPayload("656315109" || text, socketID)
      );

      console.log("accepting --> ", socketID, opponentData, currentUserData, receivedMessage)

      publishTopic(receivedMessage.callerId, {
        ...receivedMessage,
        type: 1,
        callInit: true,
        callId: text,
        roomId: "656315109" || text
        // callerName: firstName,
        // receiverId: senderId,
        // callerId: userId,
        // callerImage: "https://thumbs.gfycat.com/AgonizingQuestionableBorer-size_restricted.gif"
      })

      publishTopic(userId, {
        type: 1,
        callInit: true,
        callId: text,
        roomId: "656315109" || text
        // callerName: firstName,
        // receiverId: senderId,
        // callerId: userId,
        // callerImage: "https://thumbs.gfycat.com/AgonizingQuestionableBorer-size_restricted.gif"
      })

      if (this.state.socketID) {
        this.socket.send(JSON.stringify(roomKeyMessage));
      }
    }
    this.setState({
      text: "", roomKey: "656315109" || text.trim(), callAccepted: true
    });
    this.openCamera();
  };

  handleCallReject = () => {

    const { receivedMessage, text } = this.state;
    const { opponentData, currentUserData } = this.props;
    const { firstName, senderId, recipientId } = opponentData || {};
    const { userId, UserProfileName, UserProfilePic } = currentUserData || {};

    publishTopic(receivedMessage.callerId, {
      type: 2,
      callerName: UserProfileName,
      // receiverId: recipientId,
      callerId: userId,
      callInit: false,
      callRejected: true,
      callType: "1",
      callerName: UserProfileName,
      // receiverId: recipientId,
      callerImage: UserProfilePic,
      callId: text,
      roomId: text
      // callerImage: UserProfilePic
    });
    this.setState({ receivedMessage: {}, showVideo: false, callEnded: true });
  }

  handleChange = (event) => {
    this.setState({
      text: event.target.value,
    });
  };

  handleNameChange = (event) => {
    this.setState({
      userName: event.target.value,
    });
  };

  static getDerivedStateFromProps(props, state) {
    console.log("Latest Calling Props", props, state);
    let { webrtc } = props;
    if (webrtc && webrtc.lastMqttMsg && webrtc.lastMqttMsg.payload && !state.callEnded) {
      return {
        receivedMessage: { ...state.receivedMessage, ...webrtc.lastMqttMsg.payload },
        isCallTopic: webrtc.lastMqttMsg.topic && webrtc.lastMqttMsg.topic.indexOf('Calls/') > -1,
        text: !webrtc.callInit ? webrtc.lastMqttMsg.payload.roomId || webrtc.lastMqttMsg.payload.callId : state.text,
        roomKey: !webrtc.callInit ? webrtc.lastMqttMsg.payload.roomId || webrtc.lastMqttMsg.payload.callId : state.roomKey
      }
    }
    return {
      callEnded: false
    };
  }

  checkForCallStatus = () => {
    const { webrtc } = this.props;
    const { receivedMessage } = this.state;
    const { lastMqttMsg } = webrtc || {};

    // check if the call is ended by either user
    if (lastMqttMsg && lastMqttMsg.payload && lastMqttMsg.payload.type === 2 && receivedMessage.type === 2) {
      setTimeout(() => {
        this.setState({ receivedMessage: {}, callEnded: true });
      }, 5000);
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    // console.log("this.props", this.props);
    this.checkForCallStatus(prevProps);
    const { webrtc } = this.props;
    if (webrtc && webrtc.callInit && !this.state.roomKey || webrtc && webrtc.callInit && this.state.callEnded) {
      const randomCode = uniqid() + Math.random().toString(36).substring(7);
      // this.setState({ text: randomCode }, () => {
      // setTimeout(() => {
      this.handleSubmit(randomCode);
      // }, 1000);
      // })
    }

    // // call timeout after 60 seconds
    // setTimeout(() => {
    //   const { currentUserData, opponentData } = this.props;
    //   const { userId } = currentUserData || {};
    //   const { recipientId, firstName } = opponentData || {};
    //   if (webrtc && webrtc.lastMqttMsg && webrtc.lastMqttMsg.payload &&
    //     webrtc.lastMqttMsg.payload.callInit && !this.state.callAccepted) {
    //     publishTopic(userId, {
    //       type: 3,
    //       callInit: false,
    //       callerName: firstName
    //     });

    //     publishTopic(recipientId, {
    //       type: 2,
    //       callInit: false,
    //     });

    //     this.setState({ receivedMessage: {} });
    //     return;
    //   }
    // }, 60000);

  }

  // setting random room Id to input
  // and set to state
  componentDidMount() {
    getIceServers()
      .then(({ data }) => {
        console.log("Ice Server Before", this.state.iceServers);
        // making rtcPeerConnection with ice server to stream data
        // this.rtcPeerConnection = new RTCPeerConnection({
        //   iceServers: data.data[0] && data.data[0].iceServers || this.state.iceServers,
        // });
        this.setState({ iceServers: data.data[0] && data.data[0].iceServers || this.state.iceServers }, () => {
          console.log("Ice Server After", this.state.iceServers);
        })
      })
    return;
    try {
      const randomCode = uniqid() + Math.random().toString(36).substring(7);
      console.log("this.props.randomCode", randomCode);
      this.setState({ text: randomCode }, () => {
        return;
        const { currentUserData } = this.props;
        const { userId, UserProfileName } = currentUserData;
        Mqtt((data) => {
          console.log("rtc msg --> ", data, this.props.currentUserData);
          if (data.payload) {
            this.setState({ receivedMessage: data.payload }, () => {

              // set room id into local state
              if (this.state.receivedMessage.type === 0 && this.state.receivedMessage.callInit) {
                this.setState({ text: this.state.receivedMessage.callId }, () => {
                  console.log("Now my Room id --> ", this.state.text);
                });
              }

              // call timeout after 30 seconds
              setTimeout(() => {
                if (this.state.receivedMessage.type === 0 && !this.state.callAccepted) {
                  publishTopic(this.state.receivedMessage.callerId, {
                    type: 3,
                    callerName: this.props.currentUserData && this.props.currentUserData.UserProfileName,
                    callInit: false,
                  });

                  this.setState({ receivedMessage: {} });
                  return;
                }
              }, 60000);

              // close calling popup, if opponet declined the call.
              if (this.state.receivedMessage.type === 2 && this.state.receivedMessage.callRejected) {
                setTimeout(() => {
                  this.setState({ receivedMessage: {}, showVideo: false });
                }, 5000);
              }



            });



          }
        }, userId);
      });

      // let setRef = this.props.onRef ? this.props.onRef(this) : '';

    } catch (ex) {
      console.log("mqtt exceptions --> ", ex);
    }

  }

  render() {
    const {
      localMediaStream,
      remoteMediaStream,
      text,
      roomKey,
      socketID,
      iceServers,
      connectionStarted,
      receivedMessage,
      userName,
      showVideo,
      isCallTopic
    } = this.state;

    const { userId, opponentData } = this.props;
    const { firstName, senderId } = opponentData || {};

    const sendMessage = this.socket.send.bind(this.socket);

    console.log("Local", localMediaStream, remoteMediaStream, sendMessage, receivedMessage);

    return (
      <>
        {/* return children as first thing */}
        {this.props.children}

        <Websocket
          socket={this.socket}
          setSendMethod={this.setSendMethod}
          handleSocketConnection={this.handleSocketConnection}
          handleConnectionReady={this.handleConnectionReady}
          handleOffer={this.handleOffer}
          handleAnswer={this.handleAnswer}
          handleIceCandidate={this.handleIceCandidate}
          handleCloseConnection={this.handleCloseConnection}
        />

        <CustomizedDialogs open={isCallTopic && receivedMessage && Object.keys(receivedMessage).length > 0}>
          <div className="dialog-wrapper">


            <PeerConnection
              rtcPeerConnection={this.rtcPeerConnection}
              iceServers={iceServers}
              localMediaStream={localMediaStream}
              addRemoteStream={this.addRemoteStream}
              startConnection={connectionStarted}
              sendMessage={sendMessage}
              roomInfo={{ socketID, roomKey }}
            />
            {/* Form page to show the dashboard */}

            {/* show calling popup, when ping comes with 0 */}
            {receivedMessage && receivedMessage.type == 0 ?
              <div className="call-wrapper">
                <p className="caller-info">
                  {/* <img src={"https://thumbs.gfycat.com/AgonizingQuestionableBorer-size_restricted.gif"} /> */}
                  <img style={{ borderRadius: "50%", height: "100%", width: "100px" }} src={receivedMessage.callerImage} />
                  Call from {receivedMessage.callerName}
                </p>
                <div className="call-action">
                  <button className="accept common" onClick={() => this.handleCallAccept()}>Accept</button>
                  <button className="reject common" onClick={() => this.handleCallReject()}>Reject</button>
                </div>
              </div>
              // :

              // !this.state.roomKey || this.state.isClosed ? (
              //   <Form
              //     handleSubmit={this.handleSubmit}
              //     handleChange={this.handleChange}
              //     handleNameChange={this.handleNameChange}
              //     hasRoomKey={roomKey}
              //     text={text}
              //     userName={userName}
              //   />
              // ) 
              : ''
            }
            {
              // showVideo ?

              receivedMessage && receivedMessage.type == 1 ?
                (
                  <div className="video-wrapper">
                    <div>
                      <RTCVideo
                        mute={false}
                        style={{
                          height: `${window.innerHeight - 90}px`,
                          width: "100%",
                          // mixBlendMode: "color-dodge",
                          backgroundColor: "black",
                        }}
                        mediaStream={remoteMediaStream}
                      />
                    </div>
                    <div>
                      <RTCVideo
                        mute={true}
                        style={{
                          position: "absolute",
                          left: 10,
                          top: 10,
                          height: "200px",
                          transform: `scaleX(-1)`
                        }}
                        mediaStream={localMediaStream}
                      />
                    </div>
                    <section id="icon-container">
                      <Button
                        className="share-button icon"
                        onClick={this.handleShareDisplay}
                      >
                        {!this.state.isShare ? <ShareScreenIcon /> : <ShareOffIcon />}
                      </Button>

                      <CallEndIcon
                        className="call-end icon"
                        onClick={this.handleCloseDisplay}
                      />
                    </section>
                  </div>
                ) :
                <div className="call-wrapper">
                  {receivedMessage && (receivedMessage.type == 3 || receivedMessage.type == 7) ?
                    <p>{`No answer from ${receivedMessage.callerName} ...`}</p> :
                    receivedMessage && receivedMessage.type == 2 ?
                      <p>{`Call Ended !`}</p> :
                      <p>{`Calling to ${firstName} ...`}</p>
                  }
                </div>
              // : ''
            }
          </div>
        </CustomizedDialogs>
      </>
    );
  }
}

export default RTCMesh;
