import React, { PureComponent } from "react";
import "./css/RTCVideo.css";


class RTCVideo extends PureComponent {
  // constructor(props) {
  //   super(props);
  // }

  addMediaStream = (video) => {
    // @mediaStream is coming from RTCMesh component 
    const { mediaStream } = this.props;
    console.log("video checkeer ", mediaStream.getTracks(), video)

    // Prevents throwing error upon a setState change when mediaStream is null
    // upon initial render
    if (mediaStream && video) video.srcObject = mediaStream;
  };

  render() {
    const { mediaStream, style, mute } = this.props;
    return (
      <div>
        <video
          style={style}
          className="rtc__video"
          autoPlay
          muted={mute}
          ref={mediaStream ? this.addMediaStream : null}
          id="video-tag"
          transform="rotateY(180deg)"
        >
          <track default kind="captions" />
        </video>
      </div>
    );
  }
}


/* exported to RTCMesh component */
export default RTCVideo;
