require("dotenv").config();
export const DEFAULT_CONSTRAINTS = {
  video: true,
  audio: false,
};

export const DEFAULT_ICE_SERVERS = [{ urls: "stun:stun.l.google.com:19302" }];
export const TYPE_ROOM = "ROOM";
export const TYPE_CONNECTION = "CONNECTION";
export const TYPE_OFFER = "OFFER";
export const TYPE_ANSWER = "ANSWER";
export const TYPE_ICECANDIDATE = "ICE CANDIDATE";
export const TYPE_NEW_USER = "NEW USER";
export const TYPE_PEER_CONN = "CONN";

export const ICE_SERVER_URLS = [
  { urls: process.env.REACT_APP_STUN_SERVERS },
  {
    username: process.env.REACT_APP_TURN_USERNAME,
    credential: process.env.REACT_APP_TURN_CREDENTIAL,
    urls: process.env.REACT_APP_TURN_SERVERS.split(","),
  },
];
