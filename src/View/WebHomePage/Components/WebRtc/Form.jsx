import React from "react";
import { TextField, Button, Card, Divider } from "@material-ui/core";
import "./css/Form.css";

// dummy room list to show already visited rooms
var roomList = [8937483297, 7589347834, 3847287482, 7483279274];

// function to set a new user into roomList
const setOldRooms = (room) => {
  roomList.push(room);
  console.log(roomList);
};

// this function copies the room id to clipboard
const copyTextHandler = (text) => {
  navigator.clipboard.writeText(text); // copy the room id (text) to clipboard

  /* it will change the hover text (copy room id --> copied: <room id>) after copied */
  document.getElementById("myTooltip").innerHTML = `Copied: ${text}`;
};

/*
* here data coming from RTCMesh.jsx 
* @handleChange: its a method to change text(room id)
* @handleSubmit: method to submit room id(text)
* @text: it is the room ID, string
* @hasRoomKey: boolean value, true or false
*/
const Form = ({ handleChange, handleSubmit, text, hasRoomKey, userName, handleNameChange }) => {
  return (
    <div id="form-div">
      <Card id="card">
        <form onSubmit={handleSubmit}>
          <h2>Homepage</h2>
          <Divider id="divider" />
          <div style={{ textAlign: "left" }}>
            <b>Enter your Room code here</b>
            {/*---- code to copy room id -----*/}
            <div className="tooltip">
              <span className="tooltiptext" id="myTooltip">
                Copy Room Id
              </span>
              <img
                // copy room id
                onClick={() => {
                  copyTextHandler(text);
                }}
                // reset icon data after mouse out
                onMouseOut={() => {
                  document.getElementById("myTooltip").innerHTML =
                    "Copy Room Id";
                }}
                style={{
                  marginLeft: "10px",
                  marginBottom: "-3px",
                  cursor: "pointer",
                }}
                src="https://img.icons8.com/material-rounded/16/000000/copy.png"
                alt="copy"
              />
            </div>
          </div>

          <TextField
            style={{ margin: "10px" }}
            variant="outlined"
            label="Room id"
            error={text.length < 5}
            onChange={handleChange}
            value={text}
          />
          <TextField
            style={{ margin: "10px" }}
            variant="outlined"
            label="Enter Your Name"
            // error={text.length < 5}
            onChange={handleNameChange}
            value={userName}
          />
          <Button
            variant="contained"
            color="primary"
            type="submit"
            id="join-button"
            disabled={hasRoomKey ? true : false}
            onClick={() => {
              setOldRooms(text);
            }}
            style={{ margin: "0 10px" }}
          >
            Join
          </Button>
        </form>

        {/* this will show the visited rooms */}
        {/* <h3>Recently used rooms:</h3>
        {roomList.map((e, i) => {
          return (
            <ol key={i}>
              <li>{e}</li>
            </ol>
          );
        })} */}
      </Card>
    </div>
  );
};

export default Form;
