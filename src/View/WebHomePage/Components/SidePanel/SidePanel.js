import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import "./SidePanel.scss";
import { connect } from "react-redux";
import {
  UserProfileData,
  CurrentCoinBalance,
  GetActivePlan,
  GetBoostDetails,
  getCoinConfig,
} from "../../../../controller/auth/verification";
import { checkIfProUser } from "../../../../actions/ProUser";
import { LogoutMQTTCalled } from "../../../../lib/Mqtt/Mqtt";
import { USER_PROFILE_ACTION_FUN, storeCoins } from "../../../../actions/UserProfile";
import * as actions from "../../../../actions/UserSelectedToChat";
import * as coinCfg from "../../../../actions/coinConfig";
import { __mqttData } from "../../../../actions/mqtt-data";
import { getBoostDetailsOnWindowRefresh } from "../../../../actions/boost";
import { setCookie, getCookie, removeCookie } from "../../../../lib/session";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import Button from "../../../../Components/Button/Button";
import { Images } from "../../../../Components/Images/Images";
import { webRoutes } from "../../../../Components/Routes/Routes";
import FilterUser from "../FilterUser/FilterUser";
import { FormattedMessage } from "react-intl";
import { keys } from "../../../../lib/keys";

class SidePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      modalOpenCount: 1,
      UserFinalData: "",
      Currentcoinbalance: "",
      modelcoin: false,
      coinupgrade: "",
      WithdrawW: "",
      WalletDataHistory: "",
      variant: "success",
      open: false,
      upgradePlan: false,
      activeLink: "",
      windowWidth: null,
      referralModal: false,
      referralCode: "",
      logoutModal: false,
      nonLinkSelected: false,
    };
  }

  toggleLogoutModal = () => {
    this.setState({ logoutModal: !this.state.logoutModal });
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  // Function for the Notification ( Snackbar )
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // fn -> to navigate to buy coins page
  tooglecoinmodel() {
    this.props.history.push("/app/top-up");
  }

  // toggle nearby people modal
  togglemodel = () => {
    try {
      let selectedLocation = getCookie("selectedLocation");
      let homeAddr = getCookie("baseAddress");
      if (
        JSON.parse(selectedLocation) == null ||
        (JSON.parse(selectedLocation) == undefined && JSON.parse(homeAddr) == null) ||
        JSON.parse(homeAddr) == undefined
      ) {
        this.setState({ variant: "error", usermessage: "Please clear your cookies, and login and back / refresh the page", open: true });
      } else {
        this.setState((prevState) => ({
          modal: !prevState.modal,
        }));
      }
    } catch (e) {
      this.setState({ variant: "error", usermessage: "Please clear your cookies, and log back in / refresh the page", open: true });
      console.log("prevented app from crashing");
    }
  };

  // fn -> to update the modal count (search pref -> passport location -> select random location)
  modalUpdateCount = () => {
    this.setState({ modalOpenCount: this.state.modalOpenCount + 1 });
  };

  // fn -> to reset modal count to search pref
  modalResetCount = () => {
    this.setState({ modalOpenCount: 1 });
  };

  // fn -> to decrement modal count on close of existing modal
  modalDecrementCount = () => {
    this.setState({ modalOpenCount: this.state.modalOpenCount - 1 });
  };

  upgradePlanModal = () => {
    this.setState({ upgradePlan: !this.state.upgradePlan });
  };

  // fn -> to navigate to buy plans page, if user is trying to purchase plan or access premium features
  navigateToProPlans = () => {
    this.props.history.push("/app/premium");
    this.upgradePlanModal();
  };

  // fn -> to logout of the app
  handlesignoutsession = () => {
    let uid = getCookie("uid");
    LogoutMQTTCalled(uid);
    removeCookie("token");
    removeCookie("SignUpseemeid");
    removeCookie("findMateId");
    removeCookie("location");
    removeCookie("citylocation");
    removeCookie("uid");
    localStorage.removeItem("firstName");
    localStorage.removeItem("height");
    localStorage.removeItem("email");
    localStorage.removeItem("picture");
    localStorage.setItem("facebookLogin", false);
    this.props.history.push("/");
  };

  // renders modal width based on modal type [1,2,3]
  returnModalWidthBasedOnScreen = () => {
    if (this.state.modalOpenCount === 1) {
      return 485;
    } else if (this.state.modalOpenCount === 2) {
      return 700;
    } else if (this.state.modalOpenCount === 3) {
      return 700;
    }
  };

  // componentWillUnmount() {
  //   this.props.leftSideChatLogsFN([]);
  // }

  componentDidMount() {
    let { saveCoinConfig, checkIfUserIsProUser, boostDetailsOnRefresh } = this.props;
    let currentPath = this.props.history.location.pathname; // getting current page path
    this.setState({ activeLink: currentPath });
    this.setState({ windowWidth: window.innerWidth });
    UserProfileData()
      .then((data) => {
        this.setState({ referralCode: data.data.data.referralCode });
        setCookie("uid", data.data.data._id);
        this.setState({}, () => {
          let { profilePic, firstName, gender, findMateId, age } = data.data.data;
          this.props.updateProfilePhoto(profilePic);
          this.props.storeProfileName(firstName);
          this.props.storeGender(gender);
          this.props.storeFindMateId(findMateId);
          this.props.storeAge(age.value);
          setCookie("AgeDetails", data.data.data.age.isHidden);
          setCookie("DistanceDetails", data.data.data.distance.isHidden);
        });
        GetActivePlan()
          .then((res) => {
            checkIfUserIsProUser(res.data.data);
          })
          .catch((err) => {
            console.log("err", err);
          });
      })
      .catch((err) => {});
    getCoinConfig(getCookie("token"))
      .then((res) => {
        if (res.status !== 401) {
          saveCoinConfig(res.data.data[0]);
        } else if (res.status === 401) {
          setCookie("uid", "");
          setCookie("token", "");
        }
      })
      .catch((err) => console.log("getCoinConfig err", err));

    CurrentCoinBalance(getCookie("uid"), getCookie("token")) // 200
      .then((data) => {
        if (data.data.walletData.length > 0) {
          this.props.storeCoinBalance(data.data.walletData[0].balance);
          this.props.storeWalletID(data.data.walletData[0].walletid);
          this.props.storeEarningWalletId(data.data.walletEarningData[0].walletearningid);
        } else {
          this.props.storeCoinBalance(0);
          this.props.storeEarningWalletId(data.data.walletEarningData[0].walletearningid);
        }
      })
      .catch((err) => {});

    GetBoostDetails()
      .then((res) => {
        boostDetailsOnRefresh(res.data.data);
      })
      .catch((err) => {
        console.log("GetBoostDetails false", err);
      });
  }

  // fn -> to store referral code into clipboard, that can be shared
  shareReferralCode = () => {
    navigator.clipboard.writeText(
      `Check out the UnFiltered Dating App. Click on the link to find Real People and Real Love. ${this.state.referralCode}`
    );
    this.setState({ open: true, usermessage: "Code Copied" });
    setTimeout(() => {
      this.setState({ open: false });
    }, 2000);
  };

  // fn -> to navigate to different links on sidepanel
  handleNav = (link) => {
    if (
      link === "/app/prospects" &&
      this.props &&
      this.props.checkIfSubscribedUser &&
      this.props.checkIfSubscribedUser.ProUserDetails &&
      this.props.checkIfSubscribedUser.ProUserDetails.subscriptionId === "Free Plan"
    ) {
      this.upgradePlanModal();
      return;
    }
    if (link === "/app/logout") {
      this.toggleLogoutModal();
      this.setState({ activeLink: link, nonLinkSelected: true });
    } else if (link === "/app/referral") {
      this.setState({ activeLink: link, nonLinkSelected: true });
      this.toggleReferralModal();
    } else {
      this.setState({ activeLink: link, nonLinkSelected: false });
      this.props.history.push(link);
      this.props.selectUserToChat("");
      this.props.chatSessionEnded();
    }
  };

  // fn -> to navigate to logged in user profile
  navToProfilePage = () => {
    this.props.history.push("/app/profile");
    this.props.selectUserToChat("");
  };

  toggleCurrentPlanModal = () => {
    this.props.history.push("/app/premium");
  };

  // toggles referral modal
  toggleReferralModal = () => {
    this.setState({ referralModal: !this.state.referralModal });
  };

  getActivePlan = (obj, planId) => {
    let arr = Object.keys(obj);
    // return new Promise((resolve, reject) => {
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < Object.keys(obj[arr[i]]).length; j++) {
        if (obj[arr[i]][j]._id == planId) {
          return obj[arr[i]][j].tag.trim();
        }
      }
    }
    // });
  };

  renderSubscriptionPlanIcon = (object) => {
    // blue -> plus
    // green -> premium
    // red -> elite
    let activePlan = this.getActivePlan(this.props.allPlans, object.planId);
    if (activePlan == "UnFiltered Elite") {
      console.log("activePlan", activePlan);
      return <img src={Icons.RedHeart} height={28} width={28} />;
    } else if (activePlan == "UnFiltered Premium") {
      return <img src={Icons.GreenHeart} height={28} width={28} />;
    } else if (activePlan == "UnFiltered Plus") {
      return <img src={Icons.BlueHeart} height={28} width={28} />;
    } else if (activePlan == undefined || activePlan == null || activePlan == "") {
      return <img src={Icons.VipIcon} height={28} width={28} />;
    }
  };

  renderPlanBasedOnMonths = (object) => {
    let activePlan = this.getActivePlan(this.props.allPlans, object.planId);
    if (activePlan === "UnFiltered Elite") {
      return "UnFiltered Elite";
    } else if (activePlan === "UnFiltered Premium") {
      return "UnFiltered Premium";
    } else if (activePlan === "UnFiltered Plus") {
      return "UnFiltered Plus";
    } else {
      return "Free";
    }
  };

  render() {
    let coins = {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      color: "#21242A",
      position: "relative",
      fontSize: "14px",
      fontFamily: "Product Sans",
      paddingTop: "3px",
    };
    let { UserProfileName, UserProfilePic, UserProfileFindMateId } = this.props.UserProfile;
    return this.state.windowWidth > 990 ? (
      <div className="WAsidebox">
        <div className="WMain">
          <div className="col-12 bg-white pb-3 new__sidepanel__info__section">
            <Link onClick={() => this.navToProfilePage()}>
              <div className="col-12 text-center sidepanel-1">
                <img
                  className="new__sidepanel__user__image"
                  src={localStorage.getItem("picture") ? localStorage.getItem("picture") : UserProfilePic}
                  alt={UserProfileName}
                  title={UserProfileName}
                  onError={this.onError}
                />
              </div>
            </Link>
            <div className="col-12 my__account text-center py-1">{UserProfileName}</div>
            <div className="col-12 my__account text-center py-1">Online</div>
            <div className="col-12 my__account text-center py-1">
              {keys.AppName} ID: <span className="d_sidepanel_userId">{UserProfileFindMateId}</span>
            </div>
            {/* <div className="col-12  my__account text-center py-1">
              Plan:
              <span className="d_sidepanel_userId pl-2">
                {this.renderPlanBasedOnMonths(this.props.checkIfSubscribedUser.ProUserDetails)}
              </span>
            </div> */}
            <div className="col-12 sidepanel-2">
              <div className="row align-items-center">
                <div className="col-6">
                  <div style={coins} className="text-center">
                    <div
                      className="d-flex align-items-center justify-content-center"
                      style={{
                        paddingRight: "3px",
                      }}
                    >
                      <img src={Icons.SmallHeart} height={15} width={15} alt="User" title="User" />
                    </div>
                    <div>{this.props.CoinBalance}</div>
                  </div>
                  <div onClick={() => this.props.history.push("/app/coinbalance")} className="sidepanel_buy_coins text-center">
                    <div>
                      <FormattedMessage id="message.buyCoins" />
                    </div>
                  </div>
                </div>
                <div className="col-6" onClick={() => this.toggleCurrentPlanModal()} style={{ cursor: "pointer" }}>
                  <div className="d-flex justify-content-center align-items-center flex-column pb-1">
                    {this.renderSubscriptionPlanIcon(this.props.checkIfSubscribedUser && this.props.checkIfSubscribedUser.ProUserDetails)}
                    {this.renderPlanBasedOnMonths(this.props.checkIfSubscribedUser.ProUserDetails)}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="WUsermenu bg-white mt-2">
            <div className="pt-3 WUsermenu_sub">
              <div className="sidePanel_discoverPeople" onClick={this.togglemodel}>
                <img src={Icons.preferences} height="17" width="17" alt="preferences" />
              </div>
              {webRoutes.map((k, i) => (
                <div className="pl-4 col-11 col-sm-11 col-md-10 col-lg-10 col-xl-10 demo py-2" key={i}>
                  <p className="m-0 WMenu">
                    {k.url ? (
                      <Link onClick={() => this.handleNav(k.urlName)}>
                        {k.urlName === this.props.websiteActive ? (
                          <span className={this.state.nonLinkSelected ? "" : "activeLink"}>
                            <img src={k.srcActive} height={k.hw} width={k.hw} alt={k.linkName} className="mr-3" />
                            {k.linkName}
                            {k.urlName === "/app/friends" ? (
                              <span class="badge badge-pill ml-1 badge-danger">{this.props.friendReqCount}</span>
                            ) : (
                              <span />
                            )}
                            {k.urlName === "/app/chat" ? (
                              <span class="badge badge-pill ml-1 badge-danger">{this.props.unreadChatCount}</span>
                            ) : (
                              <span />
                            )}
                          </span>
                        ) : (
                          <span>
                            <img src={k.srcActive} height={k.hw} width={k.hw} alt={k.linkName} className="mr-3" />
                            {k.linkName}{" "}
                            {k.urlName === "/app/friends" ? (
                              <span class="badge badge-pill ml-1 badge-danger">{this.props.friendReqCount}</span>
                            ) : (
                              <span />
                            )}
                            {k.urlName === "/app/chat" ? (
                              <span class="badge badge-pill ml-1 badge-danger">{this.props.unreadChatCount}</span>
                            ) : (
                              <span />
                            )}
                          </span>
                        )}
                      </Link>
                    ) : k.urlName === this.props.websiteActive ? (
                      // <img src={k.srcActive} height={k.hw} width={k.hw} alt={k.linkName} />
                      <span className={this.state.nonLinkSelected ? "" : "activeLink"}>{k.linkName} </span>
                    ) : (
                      <span
                        className={k.urlName === this.state.activeLink ? "activeLink" : "referral"}
                        onClick={() => this.handleNav(k.urlName)}
                      >
                        <img src={k.srcActive} height={k.hw} width={k.hw} alt={k.linkName} className="mr-3" />
                        {k.linkName}
                      </span>
                    )}
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>

        {/** Opens Referral Modal */}
        <MatUiModal toggle={this.toggleLogoutModal} width={500} isOpen={this.state.logoutModal}>
          <div className="col-12 py-5 deleteAccModal">
            <div className="d_newsFeed_modal_close_btn" onClick={this.toggleLogoutModal}>
              <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
            </div>
            <div className="text-center">Are You Sure you want to logout ?</div>
            <div className="col-12 px-5">
              <div className="row justify-content-around pt-5">
                <Button handler={this.toggleLogoutModal} text="No" className="confirmModalButton" />
                <Button
                  handler={() => {
                    this.toggleLogoutModal();
                    this.handlesignoutsession();
                  }}
                  text="Yes"
                  className="cancelModalButton"
                />
              </div>
            </div>
          </div>
        </MatUiModal>
        <MatUiModal isOpen={this.state.referralModal} toggle={this.toggleReferralModal} width={500}>
          <div className="referralModal col-12 text-center p-5">
            <div className="referralModalCancelButton" onClick={this.toggleReferralModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="px-3">
              <img src={Icons.NewReferIcon} height={130} width={130} alt="referral-logo" />
              <div className="referralText">
                <FormattedMessage id="message.referral1" />
              </div>
              <div className="referralCodeCopy" onClick={this.shareReferralCode}>
                <img src={Icons.referralBg} height={60} width={200} alt="referral-code" />
                <div className="refKey">{this.state.referralCode}</div>
                <div className="refTap">
                  <FormattedMessage id="message.referralCopy" />
                </div>
              </div>
            </div>
          </div>
        </MatUiModal>
        <MatUiModal toggle={this.togglemodel} isOpen={this.state.modal} width={this.returnModalWidthBasedOnScreen()}>
          <FilterUser
            modalResetCount={this.modalResetCount}
            modalDecrementCount={this.modalDecrementCount}
            modalUpdateCount={this.modalUpdateCount}
            modalOpenCount={this.state.modalOpenCount}
            onClose={this.togglemodel}
            handleNearPeople={this.props.handleNearPeople}
            upgradePlanModal={this.upgradePlanModal}
          />
        </MatUiModal>
        <MatUiModal width={600} toggle={this.upgradePlanModal} isOpen={this.state.upgradePlan}>
          <div className="col-12 p-5 preview_Gif_Modal">
            <div className="cancelButton" onClick={this.upgradePlanModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            {/* <div className="col-12 text-center">
              <img src={Icons.revDiamond} width={80} height={80} alt="diamond" />
            </div> */}
            <div className="col-12 text-center">
              <h3>
                <FormattedMessage id="message.premimUser" />.
              </h3>
            </div>
            <div className="col-12 text-center pt-2">
              <FormattedMessage id="message.premiumUserPlanBuy" />.
            </div>
            <div className="col-12 previewButtonAlignment">
              <button type="button" onClick={this.navigateToProPlans}>
                <FormattedMessage id="message.continue" />
              </button>
            </div>
          </div>
        </MatUiModal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    ) : (
      <div className="d-flex-column text-center pt-5">
        <div className="py-2">
          <Link to="/app">
            <img src={keys.AppLogo} className="img-fluid" alt={keys.AppName} title="Unfiltered" width={55} height={55} />
          </Link>
        </div>
        <div className="py-2">
          <Link onClick={() => this.navToProfilePage()}>
            <img
              width={55}
              height={55}
              className="rounded-circle"
              src={localStorage.getItem("picture") ? localStorage.getItem("picture") : UserProfilePic}
              alt={UserProfileName}
              title={UserProfileName}
            />
          </Link>
        </div>
        <div className="buyCoins_tab_view py-1">CREDITS</div>
        <div className="pt-3">
          <img src={this.props.CoinBalance > 0 ? Icons.hasMoney : Icons.noMoney} height="30" width="30" alt="money" />
        </div>
        <div className="tab_view_coins">{this.props.CoinBalance}</div>
        <div className="tab_view_credits">Credits</div>
        <div onClick={() => this.toggleCurrentPlanModal()}>
          <div className="d-flex justify-content-center py-2">
            <img
              src={
                this.props.checkIfSubscribedUser.ProUserDetails &&
                this.props.checkIfSubscribedUser.ProUserDetails.subscriptionId === "Free Plan"
                  ? Icons.noPremiumPlan
                  : Icons.premiumPlan
              }
              height="30"
              width="30"
              alt="pro-user"
            />
          </div>
          <div className="tab_view_coins">
            {this.props.checkIfSubscribedUser.ProUserDetails &&
            this.props.checkIfSubscribedUser.ProUserDetails.subscriptionId === "Free Plan"
              ? "Off"
              : "On"}
          </div>
        </div>
        {/** SVG Section */}
        {webRoutes.map((k, i) => (
          <div className="col-12 demo my-2" key={i}>
            <p className="m-0 WMenu">
              {k.url ? (
                <Link to={k.urlName} onClick={() => this.handleNav(k.urlName)}>
                  {k.urlName === this.props.websiteActive ? (
                    <img src={k.srcActive} height={k.hw} width={k.hw} alt={k.linkName} />
                  ) : (
                    <img src={k.src} height={k.hw} width={k.hw} alt={k.linkName} />
                  )}
                </Link>
              ) : k.urlName === this.props.websiteActive ? (
                <img src={k.src} alt={k.linkName} height={k.hw} width={k.hw} />
              ) : (
                <span
                  className="referral"
                  onClick={() => {
                    k.urlName === "/app/logout" ? this.handlesignoutsession() : this.handleNav(k.urlName);
                    this.toggleReferralModal();
                  }}
                >
                  <img src={k.src} alt={k.linkName} height={k.hw} width={k.hw} />
                </span>
              )}
            </p>
          </div>
        ))}
        <MatUiModal isOpen={this.state.referralModal} toggle={this.toggleReferralModal} width={500}>
          <div className="referralModal col-12 text-center p-5">
            <div className="referralModalCancelButton" onClick={this.toggleReferralModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="px-3">
              <img src={Images.ReferralLogo} height="130px" width="130px" alt="referral-img" />
              <div className="referralText">
                <FormattedMessage id="message.referral1" />
              </div>
              <div className="referralCodeCopy" onClick={this.shareReferralCode}>
                <img src={Icons.NewReferIcon} alt="referral-banner" />
                <div className="refKey">{this.state.referralCode}</div>
                <div className="refTap">
                  <FormattedMessage id="message.referralCopy" />
                </div>
              </div>
              {/* <div className="referralCodeReward">
                <FormattedMessage id="message.referral2" />
              </div> */}
              {/* <div className="w-100 px-3 d-flex justify-content-around align-items-center py-3">
                <img src={Icons.facebook} height={50} width={50} alt="Facebook" />
                <img src={Icons.twitter} height={50} width={50} alt="Twitter" />
                <img src={Icons.whatsapp} height={50} width={50} alt="Whatsapp" />
                <img src={Icons.googlePlus} height={50} width={50} alt="GooglePlus" />
              </div> */}
            </div>
          </div>
        </MatUiModal>
        <MatUiModal toggle={this.togglemodel} isOpen={this.state.modal} width={this.returnModalWidthBasedOnScreen()}>
          <FilterUser
            modalResetCount={this.modalResetCount}
            modalDecrementCount={this.modalDecrementCount}
            modalUpdateCount={this.modalUpdateCount}
            modalOpenCount={this.state.modalOpenCount}
            onClose={this.togglemodel}
            handleNearPeople={this.props.handleNearPeople}
            upgradePlanModal={this.upgradePlanModal}
          />
        </MatUiModal>
        <MatUiModal width={600} toggle={this.upgradePlanModal} isOpen={this.state.upgradePlan}>
          <div className="col-12 p-5 preview_Gif_Modal">
            <div className="cancelButton" onClick={this.upgradePlanModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            {/* <div className="col-12 text-center">
              <img src={Icons.revDiamond} width={80} height={80} alt="diamond" />
            </div> */}
            <div className="col-12 text-center">
              <h3>
                <FormattedMessage id="message.premimUser" />.
              </h3>
            </div>
            <div className="col-12 text-center pt-2">
              <FormattedMessage id="message.premiumUserPlanBuy" />.
            </div>
            <div className="col-12 previewButtonAlignment">
              <button type="button" onClick={this.navigateToProPlans}>
                <FormattedMessage id="message.continue" />
              </button>
            </div>
          </div>
        </MatUiModal>
        <Snackbar
          timeout={2500}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleClose}
        />
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    UserSelectedToChat: state.UserSelectedToChat.UserSelectedToChat,
    SingleUserAllMessages: state.UserSelectedToChat.SingleUserAllMessages,
    chatStarted: state.UserSelectedToChat.chatStarted,
    checkIfSubscribedUser: state.ProUser,
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    friendReqCount: state.Friends.FriendRequest,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    storeCoinBalance: (coins) => dispatch(storeCoins(coins)),
    chatSessionEnded: () => dispatch(actions.chatSessionEnded()),
    checkIfUserIsProUser: (data) => dispatch(checkIfProUser(data)),
    boostDetailsOnRefresh: (data) => dispatch(getBoostDetailsOnWindowRefresh(data)),
    selectUserToChat: (data) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", data)),
    saveCoinConfig: (data) => dispatch(coinCfg.getCoinConfig(data)),
    chatSessionStarted: () => dispatch(actions.chatSessionStarted()),
    updateProfilePhoto: (image) => dispatch(USER_PROFILE_ACTION_FUN("UserProfilePic", image)),
    storeWalletID: (id) => dispatch(USER_PROFILE_ACTION_FUN("WalletData", id)),
    storeEarningWalletId: (id) => dispatch(USER_PROFILE_ACTION_FUN("EarningWalletId", id)),
    storeProfileName: (name) => dispatch(USER_PROFILE_ACTION_FUN("UserProfileName", name)),
    storeGender: (gender) => dispatch(USER_PROFILE_ACTION_FUN("UserProfileGender", gender)),
    storeFindMateId: (id) => dispatch(USER_PROFILE_ACTION_FUN("UserProfileFindMateId", id)),
    storeAge: (age) => dispatch(USER_PROFILE_ACTION_FUN("UserProfileAge", age)),
  };
};

export default withRouter(connect(mapstateToProps, mapDispatchToProps)(SidePanel));
