import React, { Component } from "react";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import "./UserProfile.scss";
import { withStyles } from "@material-ui/core/styles";
import ReactPlayer from "react-player";
import UserPersonalData from "./UserPersonalData";
import UserAbout from "./UserAbout";
import UserMoreDetail from "./UserMoreDetail";
import { __setUrlAfterCropping } from "../../../../actions/User";
import { setCookie } from "../../../../lib/session";
import { connect } from "react-redux";
import { UserProfileData, UserProfileNewData, ActivateBoost, UserdataPrefence } from "../../../../controller/auth/verification";
import { UpdateProfilePictureOnProfileImageUpdate } from "../../../../actions/UserProfile";
import MaterialUiModal from "../../../../Components/Model/MAT_UI_Modal";
import CircularProgress from "@material-ui/core/CircularProgress";
import { uploadVideoToCloudinaryPromise, __BlobUpload, singleImageToCloudinaryPromise } from "../../../../lib/cloudinary-image-upload";
import { Icons } from "../../../../Components/Icons/Icons";
import Moments from "./UserMoments";
import Switch from "react-switch";
import { boostActivated } from "../../../../actions/boost";
import * as actionChat from "../../../../actions/chat";
import "react-html5-camera-photo/build/css/index.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";

const styles = () => ({
  commonwidth: {
    width: " 100%",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  disbtn: {
    border: "none",
    fontSize: "13px",
    fontWeight: "700",
    color: "#212121",
    padding: "8px 25px",
    borderRadius: "25px",
  },
  enblebtn: {
    border: "none",
    fontSize: "13px",
    fontFamily: "Product Sans Bold",
    color: "#fff",
    padding: "8px 25px",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    borderRadius: "25px",
  },
  discancelbtn: {
    border: "none",
    fontSize: "13px",
    fontFamily: "Product Sans Bold",
    color: "#fff",
    padding: "8px 25px",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    borderRadius: "25px",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
});

const convertSecondsToMinutesTwoPads = (totalSeconds) => {
  totalSeconds %= 3600;
  let minutes = Math.floor(totalSeconds / 60);
  let seconds = totalSeconds % 60;
  minutes = String(minutes).padStart(2, "0");
  seconds = String(seconds).padStart(2, "0");
  return minutes + ":" + seconds;
};

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      uploadingVideo: false,
      collapse: false,
      selectedOption: "",
      UserFinalData: "",
      loader: true,
      size: 46447850,
      updateprofilepic: "",
      currentprofilepicarr: "",
      open: false,
      _otherImages: [],
      uploadingImagesArray: [],
      openImageCropperModal: false,
      croppedImageObjectToUploadAfterClickingPinkBox: "",
      openImageCropperModalForImageUpload: false,
      propImage: "",
      uploadingImageState: false,
      switch: false,
      boostModal: false,
      countDown: 0,
      isTimerRunning: false,
      intervalId: "",
      endTime: "",
      doYouHaveCamera: false,
      cameraModal: false,
      realImage: "",
      editModal: false,
      editMode: false,
      isTakenPhoto: false,
      dataToModify: {},
    };
    this.files = [];
    this.toggle = this.toggle.bind(this);
    this.toggleGallery = this.toggleGallery.bind(this);
  }

  updateProfileData = async () => {
    let obj = {
      pref_id: this.state.dataToModify.pref_id,
      values: this.state.dataToModify.type === 2 ? this.state.selectedOption : [this.state.selectedOption],
    };
    let data = await UserdataPrefence(obj);
    if (data != undefined && data.status === 200) {
      let res = await UserProfileData();
      this.setState({
        ...this.state,
        editMode: false,
        editModal: false,
        selectedOption: "",
        UserFinalData: res.data.data,
        open: true,
        variant: "success",
        usermessage: "Preferences Updated Successfully...",
      });
    } else {
      this.setState({ open: true, usermessage: "Something went wrong...", variant: "success" });
    }
  };

  handleMultiSelect = (event) => {
    let exData = [...this.state.selectedOption];
    if (exData.includes(event)) {
      let index = exData.indexOf(event);
      exData.splice(index, 1);
    } else {
      exData.push(event);
    }
    this.setState({ ...this.state, selectedOption: exData });
  };

  switchToEditMode = () => this.setState({ ...this.state, editMode: !this.state.editMode });

  toogleView = () => this.setState({ ...this.state, switchView: !this.state.switchView });

  toggleEditModal = (data) => {
    if (data.type === null) {
      this.setState({
        ...this.state,
        editModal: !this.state.editModal,
      });
    }
    if (data.type === 1 || data.type === 5) {
      this.setState({
        ...this.state,
        editModal: !this.state.editModal,
        dataToModify: data ? data : {},
        selectedOption: data ? data && data.selectedValues && data.selectedValues[0] : "",
      });
    } else if (data.type === 2) {
      this.setState({
        ...this.state,
        editModal: !this.state.editModal,
        dataToModify: data ? data : {},
        selectedOption: data && data.selectedValues && data.selectedValues.length > 0 ? data.selectedValues : [],
      });
    }
  };

  editModeView = (k) => {
    let { classes } = this.props;
    return k.data.map((v, w) => (
      <div key={w} className="user__profile__location__header py-2 col-12 px-0">
        <div className="row mx-0">
          <div className={`col-5 px-0 ${classes.label}`}>{v.label}:</div>
          {v && v.selectedValues && v.selectedValues.length > 0 ? (
            <span className="col-7 user__profile__pref__selected__val px-0">
              <button className="diff-submitBtn" onClick={() => this.toggleEditModal(v)}>
                Change
              </button>
            </span>
          ) : (
            <div className="col-7 px-0 user__profile__pref__selected__val">
              <button className="diff-submitBtn" onClick={() => this.toggleEditModal(v)}>
                Edit
              </button>
            </div>
          )}
        </div>
      </div>
    ));
  };

  toggleCameraModal = () => this.setState({ cameraModal: !this.state.cameraModal });

  handleSwitch = (checked) => {
    this.setState({ switch: checked });
    if (checked) {
      // var intervalId = setInterval(this.timer, 1000);
      this.ActivateBoost();
      this.setState({ boostModal: !this.state.boostModal });
    }
  };

  ActivateBoost = () => {
    if (parseInt(this.props.UserCoin) > 0) {
      ActivateBoost()
        .then((res) => {
          if (res.status === 200) {
            this.props.coinReduce(5);
            this.setState({
              open: true,
              variant: "success",
              usermessage: "Boost Activated for 5 minutes",
              isTimerRunning: true,
            });
            this.props.boostActivated(res.data);
            var intervalId = setInterval(this.timer, 1000);
            setTimeout(() => {
              this.setState({ open: false, intervalId: intervalId, countDown: 300 });
            }, 1500);
          }
        })
        .catch((err) => {
          console.log("something went wrong", err);
        });
    } else {
      this.setState({
        open: true,
        variant: "success",
        usermessage: "Oops ! You don't have any Credits to use boost",
      });
      setTimeout(() => {
        this.setState({ open: false });
      }, 1500);
    }
  };

  timer = () => {
    var newCount = this.state.countDown - 1;
    if (newCount >= 0) {
      this.setState({ countDown: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  // After Render this will call the api
  componentDidMount() {
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    UserProfileData().then((data) => {
      setCookie("uid", data.data.data._id);
      this.setState({
        UserFinalData: data.data.data,
        _otherImages: [{url: data.data.data.profilePic, timestamp: new Date().getTime()}, ...data.data.data.otherImages],
        uploadingImagesArray: data.data.data && data.data.data.otherImages.map((k) => k.url),
      });
    });
    if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
      navigator.mediaDevices
        .enumerateDevices()
        .then((res) => {
          let index = res.filter((k) => k.kind == "videoinput");
          if (index.length > 0) {
            this.setState({ doYouHaveCamera: true });
          }
        })
        .catch((err) => {});
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 1200);
  }

  toggleGallery() {
    this.setState((state) => ({ collapse: !state.collapse }));
  }

  // Function to get User Updated Data from API
  handleupdatedata = () => {
    UserProfileData().then((data) => {
      setCookie("uid", data.data.data._id);
      setCookie("AgeDetails", data.data.data.age.isHidden);
      setCookie("DistanceDetails", data.data.data.distance.isHidden);
      this.setState({
        _otherImages: data.data.data.otherImages,
        UserFinalData: data.data.data,
      });
    });
  };

  /** API to delete existing video */
  deleteVideo = () => {
    let existingObj = this.state.UserFinalData;
    existingObj["profileVideo"] = "";
    UserProfileNewData({ profileVideo: "", otherImages: this.state._otherImages })
      .then((data) => {
        this.toggleGallery();
        this.setState({
          open: true,
          variant: "success",
          UserFinalData: existingObj,
          textToRender: "Please wait while video being deleted",
          usermessage: "Please wait while video being deleted",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
      });
    this.setState({ textToRender: "" });
  };

  // Function to toggle the Module
  toggle() {
    this.setState((state) => ({ collapse: !state.collapse }));
  }

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  renderByType = (data) => {
    let active = {
      padding: "8px 16px",
      margin: "5px 5px",
      borderRadius: "5px",
      border: "2px solid #e31b1b",
      color: "#e31b1b",
      cursor: "pointer",
    };
    let deactive = {
      padding: "8px 16px",
      borderRadius: "5px",
      margin: "5px 5px",
      border: "2px solid #DBDBDB",
      color: "#484848",
      cursor: "pointer",
    };
    if (data.type === 1) {
      return (
        <div className="col-12">
          <div className="text-center py-2 edit__commonHeader1">{data.title}</div>
          <div className="text-center py-2 edit__commonHeader2">Select {data.label}</div>
          <div>
            {data &&
              data.options &&
              data.options.map((k, index) =>
                k === this.state.selectedOption ? (
                  <div className="row py-2" key={index}>
                    <div className="col-8 edit__selected__option">{k}</div>
                    <div className="col-4">
                      <img src={Icons.activateProfilePic} height={25} width={35} alt="yellow-tick" />
                    </div>
                  </div>
                ) : (
                  <div className="row py-2" key={index} onClick={() => this.setState({ ...this.state, selectedOption: k })}>
                    <div className="col-7 edit__not__selected__option">{k}</div>
                    <div className="col-5"></div>
                  </div>
                )
              )}
          </div>
          <div className="col-12 text-center">
            <button className="main-submitBtn" onClick={this.updateProfileData}>
              Save
            </button>
          </div>
        </div>
      );
    } else if (data.type === 2) {
      return (
        <div className="col-12">
          <div className="text-center py-2 edit__commonHeader1">{data.title}</div>
          <div className="text-center py-2 edit__commonHeader2">Select {data.label}</div>
          <div className="edit__multiple__select pt-4">
            {data &&
              data.options &&
              data.options.map((k, index) => (
                <div
                  key={index}
                  style={this.state.selectedOption && this.state.selectedOption.includes(k) ? active : deactive}
                  onClick={() => this.handleMultiSelect(k)}
                >
                  {k}
                </div>
              ))}
          </div>
          <div className="col-12 text-center">
            <button className="main-submitBtn" onClick={this.updateProfileData}>
              Save
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="col-12">
          <div className="text-center py-2 edit__commonHeader1">{data.title}</div>
          <div className="text-center py-2 edit__commonHeader2">Select {data.label}</div>
          <div className="edit__input">
            <input
              type="text"
              placeholder="Microsoft"
              value={this.state.selectedOption}
              onChange={(e) => this.setState({ ...this.state, selectedOption: e.target.value })}
            />
          </div>
          <div className="col-12 text-center">
            <button className="main-submitBtn" onClick={this.updateProfileData}>
              Save
            </button>
          </div>
        </div>
      );
    }
  };

  startImageCapture = (reset) => {
    console.log("clicked");
    this.setState({
      imageCaptureView: !this.state.imageCaptureView,
      realImage: reset ? "" : this.state.realImage,
    });
  };

  handleTakePhoto = (dataUri) => {
    this.files[0] = dataUri;
    this.startImageCapture(false);
    if (dataUri) {
      this.setState({ realImage: dataUri, valid: true });
    }
  };

  /** this func is to upload image */
  uploadImagePhotoImage = async () => {
    if (this.state.UserFinalData.otherImages.length >= 5) {
      this.setState({ open: true, usermessage: "Cannot upload more than 5 photos", variant: "error" });
    } else {
      let upload = await __BlobUpload(this.state.realImage);
      this.toggleCameraModal();
      this.setState({ realImage: "", isTakenPhoto: false });
      this.uploadImage(upload.body.secure_url, 1);
    }
  };

  deleteImageFromOtherImages = (file) => {
    this.setState({ textToRender: "Deleting picture...", uploadingImageState: true });
    let arrayImages = [...this.state._otherImages];
    let uploadImages = [...this.state._otherImages];
    let index = uploadImages.findIndex((k) => k.url === file);
    let index1 = arrayImages.findIndex((k) => k.url  === file);
    let index2 = uploadImages.findIndex((k) => k.url  === this.props.UserProfile.UserProfilePic);
    uploadImages.splice(index, 1);
    uploadImages.splice(index2, 1);
    arrayImages.splice(index1, 1);
    UserProfileNewData({
      profilePic: this.props.UserProfile.UserProfilePic,
      otherImages: uploadImages,
    }).then((result) => {
      this.setState({ textToRender: "", uploadingImageState: false, _otherImages: arrayImages });
    });
  };

  uploadImage = (file, type) => {
    /** type 1 for other images */
    /** type 2 for updating profile picture */
    if (type === 1) {
      let arrayImages = [...this.state._otherImages];
      let imagesToUpload = [...this.state._otherImages];
      let index = imagesToUpload.findIndex((k) => k.url === this.props.UserProfile.UserProfilePic);
      imagesToUpload.splice(index, 1);
      this.setState({ textToRender: "Please wait while we add new image", uploadingImageState: true });
    
        arrayImages.push({url: file, timestamp: new Date().getTime()});

        UserProfileNewData({
          profilePic: this.props.UserProfile.UserProfilePic,
          otherImages: [file],
        }).then((result) => {
          imagesToUpload = [];
          this.setState({ textToRender: "", uploadingImageState: false, _otherImages: arrayImages });
        });
    
    }  else if (type === 2) {
      this.setState({ textToRender: "Updating profile picture...", uploadingImageState: true });
      let imagesToUpload = [...this.state._otherImages];
      let index = imagesToUpload.findIndex((k) => k.url === file);
      console.log("image before updating", imagesToUpload);
      imagesToUpload.splice(index, 1);
      console.log("image after updating", imagesToUpload);
      UserProfileNewData({
        profilePic: file,
        otherImages: imagesToUpload.map(k => k.url),
      }).then((result) => {
        imagesToUpload = [];
        this.props._UpdateProfilePictureOnProfileImageUpdate(file);
        this.setState({ textToRender: "", uploadingImageState: false, textToRender: "" });
      });
    }
  };

  changeProfileImage = (image) => {
    UserProfileNewData({ profilePic: image }).then((res) => {
      this.props._UpdateProfilePictureOnProfileImageUpdate(image);
    });
  };

  handleTakePhotoAnimationDone = (dataUri) => {
    this.setState({ isTakenPhoto: true });
    console.log("takePhoto");
  };

  handleCameraError = (error) => {
    console.log("handleCameraError", error);
  };

  handleCameraStart = (stream) => {
    console.log("handleCameraStart", stream);
  };

  handleCameraStop = () => {
    console.log("handleCameraStop");
  };

  render() {
    let { classes } = this.props;
    return (
      <div className="userInfoContainer">
        {/* <div className={"main_profile border-left border-right"}></div> */}
        <div className={this.state.collapse ? "border-left border-right" : "main_profile border-left border-right"}>
          {/* Header Module */}
          <div className="pt-3 col-12 user_header">
            <div className="row">
              <div className="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                <h4 className="pt-2 m-0 pl-2" style={{ textTransform: "capitalize" }}>
                  {this.props.UserProfile.UserProfileName}
                </h4>
                <div className="pt-2 m-0 pl-2">
                  <span className={"id-pinkBox"}>ID:</span>
                  <span className={"d_userProfile_ID"}>{this.state.UserFinalData.findMateId}</span>
                </div>
              </div>
              <div className="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-right">
                {this.state.uploadingImageState ? (
                  <div>
                    <span>
                      <CircularProgress />
                    </span>
                    <span className="pl-2">{this.state.textToRender}</span>
                  </div>
                ) : (
                  <span />
                )}
              </div>
            </div>
          </div>

          <div className="d_userinfo_scoll_area">
            {/* User Photo Input Module */}
            <div className="col-12 Wuser_profileshow">
              <div className="row w-100">
                {this.state.UserFinalData && this.state.UserFinalData.profileVideo ? (
                  <div className="profile_page_video_boxes">
                    <span className="userProfile_CancelButton" onClick={() => this.deleteVideo()}>
                      <img src={Icons.deleteImage} alt="delete" />
                    </span>
                    <ReactPlayer url={this.state.UserFinalData.profileVideo} playing={false} controls={true} />
                  </div>
                ) : (
                  <span />
                )}


{this.state._otherImages.length > 0 &&
                  this.state._otherImages.map((k, i) => (
                    <div className="profile_page_image_boxes" key={i}>
                      {k.url !== this.props.UserProfile.UserProfilePic ? (
                        <span className="userProfile_CancelButton" onClick={() => this.deleteImageFromOtherImages(k)}>
                          <img src={Icons.deleteImage} alt="delete" />
                        </span>
                      ) : (
                        ""
                      )}
                      <img src={k.url} alt={this.props.UserProfile.UserProfileName} title={this.props.UserProfile.UserProfileName} />
                      {k.url === this.props.UserProfile.UserProfilePic ? (
                        <div className="setProfilePicture">
                          <img src={Icons.profilePicActive} alt="pink-tick" />
                          <span>Make profile photo</span>
                        </div>
                      ) : (
                        <div className="setProfilePicture" onClick={() => this.uploadImage(k.url, 2)}>
                          <img src={Icons.activateProfilePic} alt="grey-tick" />
                          <span>Make profile photo</span>
                        </div>
                      )}
                    </div>
                  ))}
                <div className="profile_page_image_boxes_upload_image" onClick={this.toggleCameraModal}>
                  <form encType="multipart/form-data">
                    <div>
                      <label className="col-auto" htmlFor="File">
                        <div>
                          <img src={Icons.camera} alt="video" />
                        </div>
                        <div>
                          <p className="m-0">Change Photo</p>
                        </div>
                      </label>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            {/* User Bio-Data Module */}
            <div className="col-12 border-top border-bottom">
              <div className="row w-100">
                {/* User Left Profile Module */}
                <div className="col-6 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  {/* User-Personal-Data Module */}
                  <UserPersonalData UserFinalData={this.state.UserFinalData} handleupdatedata={this.handleupdatedata} />

                  {/* User-About-Data Module */}
                  <UserAbout UserFinalData={this.state.UserFinalData} handleupdatedata={this.handleupdatedata} />
                </div>

                {/* Right-Side-Panel Module */}
                <div className="col-6 col-sm-12 col-md-12 col-lg-12 col-xl-12 user_verification">
                  <div className="row m-0 pb-4">
                    <div className="profile_header col-12 pt-4 pb-3 px-0">
                      <h5>
                        My Details
                        <i className="fas fa-pencil-alt mr-1" onClick={this.switchToEditMode} />
                      </h5>
                    </div>

                    {this.state.UserFinalData.myPreferences &&
                      this.state.UserFinalData.myPreferences.map((k, i) => (
                        <div className="col-4" key={i}>
                          <div className="row mx-0">
                            {!this.state.editMode
                              ? k.data.map((v, w) => (
                                  <div key={w} className="user__profile__location__header py-2 col-12 px-0">
                                    <div className="row mx-0">
                                      <div className={`col-5 px-0 ${classes.label}`}>{v.label}:</div>
                                      <div className="col-7 user__profile__pref__selected__val px-0" style={{ whiteSpace: "pre-wrap" }}>
                                        {v && v.selectedValues && v.selectedValues.length > 0 ? (
                                          v.selectedValues.map((x) => x).join(", ")
                                        ) : (
                                          <div className="px-0 user__profile__pref__selected__val">None</div>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                ))
                              : this.editModeView(k)}
                          </div>
                        </div>
                      ))}
                  </div>

                  {this.state.UserFinalData && this.state.UserFinalData.moments && this.state.UserFinalData.moments.length > 0 ? (
                    <Moments UserFinalData={this.state.UserFinalData} isOtherUser={false} />
                  ) : (
                    <></>
                  )}

                  {/** Switch for boosting account */}
                  <div className="row mb-5 pt-3 border-top">
                    <div className="col-5 boost">Boost Your Profile and Show Up on the Top of the Searches</div>
                    <div className="col-7">
                      <Switch
                        onChange={this.handleSwitch}
                        checked={
                          this.props && this.props.boostOnRefresh && this.props.boostOnRefresh.expire > new Date().getTime() ? true : false || this.state.switch
                        }
                        disabled={this.state.isTimerRunning}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <MaterialUiModal
          isOpen={this.state.boostModal}
          toggle={() => this.setState({ boostModal: this.props.CoinBalance > 0 && !this.state.boostModal })}
          width={400}
        >
          <div className="col-12 p-4">
            <div className="moments_cancel_button" onClick={() => this.setState({ boostModal: !this.state.boostModal })}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="boostText">Boost will end in:</div>

            <div className="boostText">
              {this.state.countDown > 0 ? (
                <div className="d-flex pt-2 justify-content-center align-items-center">
                  <div className="text-center">{convertSecondsToMinutesTwoPads(this.state.countDown)}</div>
                </div>
              ) : (
                <span />
              )}
            </div>
          </div>
        </MaterialUiModal>
        <MaterialUiModal isOpen={this.state.cameraModal} toggle={this.toggleCameraModal} width={this.state.isTakenPhoto ? "480px" : "83vw"}>
          <div className="p-5">
            {this.state.realImage ? (
              <div className="text-center">
                <img
                  src={this.state.realImage}
                  alt="image"
                  height={300}
                  width={200}
                  style={{ objectFit: "cover", borderRadius: "5px", marginBottom: "20px" }}
                />
                <div className="uploadButton">
                  <button
                    onClick={() => {
                      this.setState({ isTakenPhoto: false });
                      this.startImageCapture(true);
                    }}
                  >
                    Try Again
                  </button>
                  <button onClick={this.uploadImagePhotoImage}>Upload</button>
                </div>
              </div>
            ) : (
              <section>
                <div className="moments_cancel_button" onClick={this.toggleCameraModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                {this.state.doYouHaveCamera ? (
                  <div className="camera-module">
                    <Camera
                      onTakePhoto={(dataUri) => {
                        this.handleTakePhoto(dataUri);
                      }}
                      onTakePhotoAnimationDone={(dataUri) => {
                        this.handleTakePhotoAnimationDone(dataUri);
                      }}
                      onCameraError={(error) => {
                        this.handleCameraError(error);
                      }}
                      idealFacingMode={FACING_MODES.ENVIRONMENT}
                      idealResolution={{ width: 768, height: 1366 }}
                      imageType={IMAGE_TYPES.JPG}
                      imageCompression={0.97}
                      isMaxResolution={true}
                      isImageMirror={false}
                      isSilentMode={false}
                      isDisplayStartCameraError={true}
                      isFullscreen={false}
                      sizeFactor={1}
                      onCameraStart={(stream) => {
                        this.handleCameraStart(stream);
                      }}
                      onCameraStop={() => {
                        this.handleCameraStop();
                      }}
                    />
                  </div>
                ) : (
                  <div className="text-center py-4">
                    <h3>Please Enable Webcam if you want to continue clicking photos</h3>
                  </div>
                )}
              </section>
            )}
          </div>
        </MaterialUiModal>
        <MaterialUiModal isOpen={this.state.editModal} toggle={this.toggleEditModal} width={"35vw"}>
          <section className="col-12 p-5">
            <div onClick={() => this.toggleEditModal({ type: null })}>
              <div className="d_comment_modal_close_btn">
                <img src={Icons.closeBtn} alt="close-btn" className="modal__dark__close__right" height={13} />
              </div>
            </div>
            <div className="row">{this.renderByType(this.state.dataToModify)}</div>
          </section>
        </MaterialUiModal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapstateToProps = (state) => {
  return {
    UserData: state.Main.UserData,
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    checkIfUserIsProUserVariable: state.ProUser,
    urlAfterCropping: state.UserProfile.urlAfterCropping,
    superLikeCost: state.CoinConfig.CoinConfig.superLike,
    UserCoin: state.UserProfile.CoinBalance,
    boostOnRefresh: state.Boost.BoostDetailsOnRefresh,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    boostActivated: (data) => dispatch(boostActivated(data)),
    __setUrlAfterCropping: (url) => dispatch(__setUrlAfterCropping(url)),
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    _UpdateProfilePictureOnProfileImageUpdate: (url) => dispatch(UpdateProfilePictureOnProfileImageUpdate(url)),
  };
};

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(UserProfile));
