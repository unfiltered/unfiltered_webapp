// Main React Components
import React, { useState, useEffect, useRef } from "react";
// Re-Usuable COmponents
import Snackbar from "../../../../Components/Snackbar/Snackbar";
// Material-UI COmponents
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
// React Slick Slider Components
import Slider from "react-slick";
import ReactPlayer from "react-player";
// Redeux Components
import { connect } from "react-redux";
import { Icons } from "../../../../Components/Icons/Icons";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  Newimgprogess: {
    margin: "0 auto",
    color: "#e31b1b",
    marginTop: "8.5vh",
    position: "relative",
    left: "65px",
  },
});

function UserProfileInput(props) {
  const [isPhotoLoading, setisPhotoLoading] = useState(false);
  const [maxLength, setLength] = useState(5);
  const [arr, setArr] = useState(new Array(5).fill(0));

  const setLengthOfImages = (obj) => {
    let c = 5 - obj.length;
    setArr(arr.splice(0, c));
    setLength(c);
  };
  /** gather resources to display */
  let otherImages = props && props._otherImages;
  let profileVideo = props && props.profileVideo;

  // componentWillUpdate based on other images changes(update / delete).
  useEffect(() => {
    setLengthOfImages(otherImages);
  }, [otherImages]);

  const fileUploaderRef = React.createRef();

  let { classes } = props;
  return (
    <div className="col-12 pl-3 pr-3 w-100">
      {/* <div className="row justify-content-between"></div> */}
      <div className="row">
        <div className="profile_page_image_boxes_upload_image">
          <form encType="multipart/form-data">
            {isPhotoLoading ? (
              <CircularProgress className={classes.Newimgprogess} />
            ) : (
              <div>
                {otherImages.length <= 5 ? (
                  <label className="col-auto" htmlFor="File">
                    <div>
                      <img src={Icons.camera} alt="camera" />
                    </div>
                    <div>
                      <p className="m-0"> Add photo</p>
                    </div>
                    <input
                      type="file"
                      id="File"
                      className="Wuser_input d-none"
                      accept="images/*"
                      ref={fileUploaderRef}
                      onChange={() => props._openImageCropperModalForImageUploadModal(fileUploaderRef)}
                    />
                  </label>
                ) : (
                  <h6>You Uploaded Max Number of Images.</h6>
                )}
              </div>
            )}
          </form>
        </div>
        {/** show profile video if available */}
        {profileVideo && profileVideo.length > 0 ? (
          <div className="profile_page_image_boxes">
            <ReactPlayer url={profileVideo} playing={true} controls={true} />
          </div>
        ) : (
          ""
        )}
        {otherImages.length > 0 &&
          otherImages.map((k, i) => (
            <div className="profile_page_image_boxes" key={i}>
              <img src={k} alt={props.UserProfile.UserProfileName} title={props.UserProfile.UserProfileName} />
            </div>
          ))}
        {arr.map((k, i) => (
          <div className="profile_page_image_boxes_upload_more_images" key={i}>
            <div>Add More Photos</div>
            <img src={Icons.camera} alt="camera" />
          </div>
        ))}
      </div>
    </div>
  );
}

function mapstateToProps(state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
  };
}

export default connect(mapstateToProps, null)(withStyles(styles)(UserProfileInput));
