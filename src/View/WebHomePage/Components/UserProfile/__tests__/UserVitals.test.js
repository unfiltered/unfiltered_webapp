// Main React Components
import React from "react";
import ReactDOM from "react-dom";

// Enzyme TestCase Module Components
import { shallow, configure, render } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

// PropType Component
import checkPropTypes from "check-prop-types";

// Imported Components
import MyComponent from "../UserVitals";

configure({ adapter: new Adapter() });

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<MyComponent {...props} />);
  if (state) wrapper.setstate(state);
  return wrapper;
};

describe("<UserVitals />", () => {
  it("should be Defined", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper).toBeDefined();
  });

  it("should render correctly", () => {
    const wrapper = setup()
      .first()
      .shallow();
  });

  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.unmountComponentAtNode(div);
  });

  it("renders component with main one '<div>' tag", () => {
    const wrapper = setup()
      .first()
      .shallow();
    const SignupComponent = wrapper.find('[data-test="component-mainVitals"]');
    expect(SignupComponent.length).toBe(1);
  });


  it("does not throw warning with expected props", () => {
    const expectedProps = { success: false };
    const propError = checkPropTypes(
      MyComponent.propTypes,
      expectedProps,
      "prop"
    );
    expect(propError).toBeUndefined();
  });
});
