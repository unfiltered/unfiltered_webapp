import React, { Component } from "react";
import Skeleton from "react-loading-skeleton";
import "./UserProfile.scss";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../actions/User";
import { UserProfileNewData } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import Button from "../../../../Components/Button/Button";

const styles = () => ({
  commonwidth: {
    width: " 100%",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
  },
});

class UserPersonalData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseuserdata: false,
      selectedOption: null,
      selectedOldGender: "",
      NA: "N/A",
      hover: false,
      shown: true,
      newvalue: null,
      newemailvalue: null,
      firstName: "",
      email: "",
      open: false,
      loader: false,
      savedState: "Save",
    };
    this.radioChange = this.radioChange.bind(this);
  }

  // Function for the Hover on
  hoverOn = () => {
    this.setState({ hover: true });
  };

  // Function for the Hover off
  hoverOff = () => {
    this.setState({ hover: false });
  };

  // Function for the Toggle Module
  toggle() {
    this.setState({
      shown: !this.state.shown,
    });
  }

  // Function for the Name Module
  handlename = (event) => {
    this.setState({ firstName: event.target.value, savedState: "Done Editing" });
    this.props.dispatch(MOBILE_ACTION_FUNC("firstName", this.state.firstName));
  };

  // Function for the Email Module
  handleemail = (event) => {
    this.setState({ email: event.target.value, savedState: "Done Editing" });
    this.props.dispatch(MOBILE_ACTION_FUNC("email", this.state.email));
  };

  // Function for the Radio Button Module
  radioChange(event) {
    this.setState(
      {
        selectedOption: event.target.value,
        savedState: "Done Editing",
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("gender", this.state.selectedOption));
      }
    );
  }

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the Show & Change Gender
  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.selectedOption) {
      if (nextProps.UserFinalData && nextProps.UserFinalData.gender) {
        let radioCheck = nextProps.UserFinalData.gender === "Male" ? document.getElementById("Male") : document.getElementById("Female");
        radioCheck.checked = true;
      }
      return {
        selectedOption: nextProps.UserFinalData.gender,
        firstName: nextProps.UserFinalData.firstName,
        email: nextProps.UserFinalData.email,
      };
    }
    return {};
  }

  // API Call the UserPersonal Details Module
  handleclick = () => {
    let Userprofileupdatedpayload = {
      firstName: this.state.firstName,
      email: this.state.email,
      gender: this.state.selectedOption,
    };

    UserProfileNewData(Userprofileupdatedpayload)
      .then((data) => {
        this.props.handleupdatedata();
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Data Update successfully.!!",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
      });

    this.toggle();
  };

  render() {
    var shown = {
      display: this.state.shown ? "block" : "none",
    };

    var hidden = {
      display: this.state.shown ? "none" : "block",
    };
    const { classes } = this.props;
    return (
      <div data-test="component-mainPersoanlData">
        <div className={"pt-4 pb-3 border-bottom"}>
          <div className="profile_header">
            <h5>
              My Details -
              <i className="fas fa-pencil-alt mr-1" onClick={this.toggle.bind(this)} /> {!this.state.shown ? "" : ""}
            </h5>

            <div className="col-12 user_module bg-white" style={shown}>
              <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>Gender :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <p>{this.props.UserFinalData.gender || <Skeleton duration={1} width={200} />}</p>
                </div>
              </div>

              <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>Age :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <p>
                    {(this.props.UserFinalData && this.props.UserFinalData.age && this.props.UserFinalData.age.value) || <Skeleton duration={1} width={200} />}
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>Phone Number :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <p>{this.props.UserFinalData.mobileNumber || <Skeleton duration={1} width={200} />}</p>
                </div>
              </div>

              <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>Email :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <p>{this.props.UserFinalData.emailId || <Skeleton duration={1} width={200} />}</p>
                </div>
              </div>

              {/* <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>Location :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <p>{getCookie("selectedLocation") != null ? JSON.parse(getCookie("selectedLocation")).address : getCookie("location")}</p>
                </div>
              </div> */}
            </div>

            {/* On User Edit Panel Data Module */}
            <div className="col-12 edit_info bg-white" style={hidden}>
              {/* Name Module */}
              <div className="py-2 row">
                <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                  <label className={classes.label}>Name :</label>
                </div>
                <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                  <form id="Userquestion_common">
                    <input
                      refs="name"
                      disabled
                      name="name"
                      type="text"
                      size="30"
                      value={this.state.firstName || this.props.UserFinalData.firstName}
                      onChange={this.handlename}
                      style={{ textTransform: "capitalize" }}
                    />
                  </form>
                </div>
              </div>

              {/* Gender Module */}
              <div className="py-1 row">
                <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                  <label className={classes.label}>Gender :</label>
                </div>
                <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                  <form id="Userquestion_common">
                    <div className="row">
                      <div className="col-6 col-lg-5 col-xl-5">
                        <div className="Wsignupradiochecked_btn">
                          <input disabled type="radio" id="Male" value={"Male"} name="radio-group" />
                          <label htmlFor="Male">Male</label>
                        </div>
                      </div>
                      <div className="col-6 col-lg-5 col-xl-5">
                        <div className="Wsignupradiochecked_btn">
                          <input disabled type="radio" id="Female" value={"Female"} name="radio-group" />
                          <label htmlFor="Female">Female</label>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              {/* Email Module */}
              <div className="py-2 row">
                <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                  <label className={classes.label}>Email :</label>
                </div>
                <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                  <form id="Userquestion_common">
                    <input
                      refs="name"
                      name="name"
                      type="text"
                      disabled
                      size="30"
                      value={this.state.email || this.props.UserFinalData.emailId}
                      onChange={this.handleemail}
                    />
                  </form>
                </div>
              </div>

              {/* <div className="py-2 row">
                <div className="col-4" />
                <div className="pl-0 col-6 col-lg-8 col-xl-8">
                  <Button handler={this.handleclick.bind(this)} text={this.state.savedState} />
                  <Button handler={this.toggle.bind(this)} text="Cancel" />
                </div>
              </div> */}
            </div>
          </div>

          {/* Snakbar Components */}
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(UserPersonalData));
