import React from "react";
import "./UserProfile.scss";
import "../SidePanel/SidePanel.scss";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import { Images } from "../../../../Components/Images/Images";

class Moments extends React.PureComponent {
  state = {
    modalOpen: false,
  };
  toggleModal = () => {
    this.setState({ modalOpen: !this.state.modalOpen });
  };
  render() {
    return (
      <div className="mb-5">
        {!this.props.isOtherUser ? (
          <div className="profile_header mt-3">
            <h5>Moments</h5>
          </div>
        ) : (
          <span />
        )}
        <div className="col-12 mb-5">
          <div className="row">
            {this.props.UserFinalData &&
              this.props.UserFinalData.moments
                .slice(0, 4)
                .map((k) => (
                  <img
                    src={k.url ? k.url[0] : Images.placeholder}
                    alt={k.description}
                    className="moments_images"
                    onClick={() => (this.props.isOtherUser ? this.props.openPost(k) : "")}
                  />
                ))}
            {this.props.UserFinalData && this.props.UserFinalData.moments.length > 4 ? (
              <div className="view_more" onClick={this.toggleModal}>
                View More...
              </div>
            ) : (
              <span />
            )}
          </div>
        </div>
        <MatUiModal width={650} isOpen={this.state.modalOpen} toggle={this.toggleModal}>
          <div className="col-12 p-4 moments_modal">
            <div className="moments_cancel_button" onClick={this.toggleModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="moments_header col-12 text-center pb-3">Moments</div>
            <div>
              {this.props.UserFinalData &&
                this.props.UserFinalData.moments &&
                this.props.UserFinalData.moments.map((k) => (
                  <img
                    src={k.url ? k.url[0] : Images.placeholder}
                    onClick={() => (this.props.isOtherUser ? this.props.openPost(k) : "")}
                    alt={k.description}
                    className="moments_images"
                  />
                ))}
            </div>
          </div>
        </MatUiModal>
      </div>
    );
  }
}

export default Moments;
