// Main React Components
import React, { Component } from "react";

// Scss
import "./UserProfile.scss";

// Re-Usuable Components
import Snackbar from "../../../../Components/Snackbar/Snackbar";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// MultiSelect Dropdown Components
import Select from "react-select";

// Redux Components
import { UserdataPrefence } from "../../../../controller/auth/verification";
import Button from "../../../../Components/Button/Button";

const styles = () => ({
  commonwidth: {
    width: "100%",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
  },
});

let selectuserinput = "";

class UserMoreDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsedetail: false,
      novalue: "N/A",
      hover: false,
      namePayload: {},
      multiSelect: [],
      open: false,
      inputVal: "",
      inputData: {},
      shown: false,
      selectedOption: [],
    };
  }

  // Function for the Toggle Module
  toggle() {
    this.setState({
      shown: !this.state.shown,
    });
    // this.handleuserinput();
  }

  // Function for the Hover on
  hoverOn = () => {
    this.setState({ hover: true });
  };

  // Function for the Hover off
  hoverOff = () => {
    this.setState({ hover: false });
  };

  // Function for the Input Value
  handlename = (val, event) => {
    let tempState = this.state.inputData;
    tempState[event.target.name] = event.target.value;
    this.setState({ inputData: tempState });
  };

  handleMultiSelect = (event) => {
    let exData = [...this.state.selectedOption];
    if (exData.includes(event)) {
      let index = exData.indexOf(event);
      exData.splice(index, 1);
    } else {
      exData.push(event);
    }
    this.setState({ ...this.state, selectedOption: exData });
  };

  getInput = (type, prefData, pref_id) => {
    let active = {
      padding: "8px 16px",
      margin: "5px 5px",
      borderRadius: "5px",
      border: "2px solid #C80098",
      color: "#C80098",
      cursor: "pointer",
    };
    let deactive = {
      padding: "8px 16px",
      borderRadius: "5px",
      margin: "5px 5px",
      border: "2px solid #DBDBDB",
      color: "#484848",
      cursor: "pointer",
    };
    switch (type) {
      case 5:
        let inputBox = (
          <form id="Userquestion_common" onSubmit={this.handleUserInput.bind(this, pref_id, prefData.label)}>
            <input
              refs={prefData.label}
              name={prefData.label}
              type="text"
              size="30"
              value={this.state["inputData"][prefData.label] || prefData.selectedValues[0]}
              onChange={this.handlename.bind(this, prefData.label)}
            />
            <Button type="submit" className="moreDetailsBtn" text="Save" />
          </form>
        );
        return inputBox;

      case 10:
        let options = [];
        prefData.options.map((data) =>
          options.push({
            label: data,
            value: data,
            controlLable: prefData.label,
          })
        );

        let SelectedUserValue = {
          label: selectuserinput,
          value: selectuserinput,
        };

        let multiCheckInput = (
          <div>
            <div className="select_dropdownedit">
              <Select onChange={this.handleUserSelects.bind(this, pref_id)} options={options} defaultValue={[SelectedUserValue]} />
            </div>
          </div>
        );
        return multiCheckInput;
      case 2:
        return (
          <div className="col-12">
            <div className="text-center py-2 edit__commonHeader1">{prefData.title}</div>
            <div className="text-center py-2 edit__commonHeader2">Select {prefData.label}</div>
            <div className="edit__multiple__select pt-4">
              {prefData &&
                prefData.options &&
                prefData.options.map((k, index) => (
                  <div
                    key={index}
                    style={this.state.selectedOption && this.state.selectedOption.includes(k) ? active : deactive}
                    onClick={() => this.handleMultiSelect(k)}
                  >
                    {k}
                  </div>
                ))}
            </div>
            <div className="col-12 text-center">
              <button className="user__info__btn2" onClick={this.updateProfileData}>
                Save
              </button>
            </div>
          </div>
        );

      case 1:
        let options1 = [];
        prefData.options.map((data) =>
          options1.push({
            label: data,
            value: data,
            controlLable: prefData.label,
          })
        );

        let SelectedUserValue1 = {
          label: selectuserinput,
          value: selectuserinput,
        };

        let SelectDropDown = (
          <div>
            <div className="select_dropdownedit">
              <Select onChange={this.handleUserSelects.bind(this, pref_id)} options={options1} defaultValue={[SelectedUserValue1]} />
            </div>
          </div>
        );
        return SelectDropDown;
      default:
        console.log("none");
    }
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // API Call the UserDetails DropDown Module
  handleUserSelects = (pref_id, event) => {
    let updatedPreferences = {
      pref_id: pref_id,
      values: [event.value],
    };

    UserdataPrefence(updatedPreferences)
      .then((data) => {
        this.props.handleupdatedata();

        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Preference Update successfully.!!",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
      });
  };

  updateProfileData = async () => {
    // let obj = {
    //   pref_id: state.dataToModify.pref_id,
    //   values: state.dataToModify.type === 2 ? state.selectedOption : [state.selectedOption],
    // };
    // let data = await ToastPromise(UserdataPrefence(obj), [
    //   "Updating Preferences...",
    //   "Preferences Updated Successfully...",
    //   "Failed to update preferences...",
    // ]);
    // if (data != undefined && data.status === 200) {
    //   let res = await UserProfileData();
    //   setState({ ...state, editMode: false, editModal: false, selectedOption: "" });
    //   setUserFinalData(res.data.data);
    // }
  };

  // API Call the UserDetails Save Button Module
  handleUserInput = (pref_id, labelType, event) => {
    event.preventDefault();

    if (this.state.inputData[labelType]) {
      let updatedPreferencesInput = {
        pref_id: pref_id,
        values: [this.state.inputData[labelType]],
      };

      UserdataPrefence(updatedPreferencesInput)
        .then((data) => {
          this.props.handleupdatedata();

          this.setState({
            open: true,
            variant: "success",
            usermessage: "User Preference Update successfully.!!",
          });
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Some Error Occured.!!",
          });
        });
    }
  };

  render() {
    var hidden = {
      display: this.state.shown ? "block" : "none",
    };

    var shown = {
      display: this.state.shown ? "none" : "block",
    };
    const { classes } = this.props;
    return (
      <div data-test="component-mainMoreDetail" className={"pt-4 pb-3 border-bottom"}>
        {this.props.UserFinalData &&
          this.props.UserFinalData.myPreferences.map((data, index) =>
            index === 0 ? (
              <div key={index}>
                <div className="profile_header">
                  <h5>
                    {data.title} -
                    <i className="fas fa-pencil-alt mr-1" onClick={this.toggle.bind(this)} /> {this.state.shown ? "Update" : ""}
                  </h5>
                </div>
                {data.data.map(
                  (pref, index) => (
                    (selectuserinput = pref.selectedValues[0]),
                    pref.isDone || this.state.shown ? (
                      <div key={"UserDetails" + index}>
                        {/* Used to show UserDetail Module */}
                        <div className="col-12 user_module" style={shown}>
                          <div className="row">
                            <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                              <p className={classes.label}>{pref.label} :</p>
                            </div>
                            <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                              <p>{pref.selectedValues[0]}</p>
                            </div>
                          </div>
                        </div>
                        {/* User for edit UserDetails */}
                        <div className="ppp">
                          <div className="col-12" style={hidden}>
                            <div className="py-2 row">
                              <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <p className={classes.label}>{pref.label} :</p>
                              </div>
                              <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                {this.getInput(pref.type, pref, pref.pref_id)}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )
                  )
                )}
              </div>
            ) : (
              ""
            )
          )}
        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default withStyles(styles)(UserMoreDetail);
