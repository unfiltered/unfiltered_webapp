// Main React Components
import React, { Component } from "react";

// Scss
import "./UserProfile.scss";

// Re-Usuable Components
import Snackbar from "../../../../Components/Snackbar/Snackbar";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// Redux Components
import { UserProfileNewData, UserProfileData } from "../../../../controller/auth/verification";
import Button from "../../../../Components/Button/Button";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "5vh",
    textAlign: "center",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
  },
});

class UserAbout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseaboutdata: false,
      about:
        "Include something unique about yourself— Are you an identical twin? Have you been to every baseball stadium in the country? Say that! Can’t think of anything unusual? Ask a fun question.",
      hover: false,
      aboutpayload: [],
      open: false,
      loader: false,
      savedState: "Save",
      shown: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  // After Render this will call the api
  componentDidMount() {
    UserProfileData().then((data) => {
      this.setState({ aboutpayload: { about: data.data.data.about } });
    });
  }

  // Function to toggle Module
  toggle() {
    this.setState({
      shown: !this.state.shown,
    });
  }

  // Function for the Hover on
  hoverOn = () => {
    this.setState({ hover: true });
  };

  // Function for the Hover off
  hoverOff = () => {
    this.setState({ hover: false });
  };

  // Function for the Input Module
  handlename = (event) => {
    console.log("handle name called:-", event.target.value);
    this.setState({ aboutpayload: { about: event.target.value }, savedState: "Done Editing" });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // API Call the UserAbout Module
  handleclick = () => {
    UserProfileNewData(this.state.aboutpayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Preference Update successfully.!!",
        });
      })
      .catch((error) => {
        if (this.state.aboutpayload.about.length === 0) {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Please enter something.!!",
          });
        } else {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Some Error Occured.!!",
          });
        }
      });
    this.toggle();
  };

  render() {
    const { classes } = this.props;

    var hidden = {
      display: this.state.shown ? "block" : "none",
    };

    var shown = {
      display: this.state.shown ? "none" : "block",
    };

    return (
      <div data-test="component-mainabout">
        <div
          className={"pt-4 pb-3 border-bottom"}
          // onMouseEnter={this.hoverOn}
          // onMouseLeave={this.hoverOff}
        >
          <div className="profile_header">
            <h5>
              About Me -{/* {this.state.hover ? ( */}
              <i className="fas fa-pencil-alt" onClick={this.toggle.bind(this)} /> <span className="mr-1">{this.state.shown ? "Update" : ""}</span>
              {/* ) : (
                ""
              )} */}
            </h5>
            {/* By Deafult User Data Module */}
            <div className="col-12 user_module" style={shown}>
              <div className="row">
                <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                  <p className={classes.label}>About :</p>
                </div>
                <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                  <textarea
                    className={
                      this.state.aboutpayload &&
                      this.state.aboutpayload.about &&
                      this.state.aboutpayload.about ===
                        "Include something unique about yourself— Are you an identical twin? Have you been to every baseball stadium in the country? Say that! Can’t think of anything unusual? Ask a fun question."
                        ? "editProfile_textAreaGrey"
                        : "editProfile_textArea"
                    }
                    placeholder="Include something unique about yourself— Are you an identical twin? Have you been to every baseball stadium in the country? Say that! Can’t think of anything unusual? Ask a fun question."
                    value={this.state.aboutpayload.about}
                    disabled
                    style={{
                      overflow: "hidden",
                      width: "100%",
                      background: "transparent",
                      border: "none",
                    }}
                  />
                </div>
              </div>
            </div>
            {/* On User Edit Panel Data Module */}
            <div className="col-12 edit_info" style={hidden}>
              <div className="py-2 row">
                <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                  <label>About :</label>
                </div>
                <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                  <form id="Userquestion_common">
                    <textarea
                      className="editProfile_textArea"
                      value={this.state.aboutpayload.about}
                      onChange={this.handlename}
                      placeholder="The placeholder attribute specifies a short hint that describes the expected value."
                    />
                  </form>
                </div>
              </div>
              <div className="py-2 row">
                <div className="col-4" />
                <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                  <Button data-test="component-aboutbtn" handler={this.handleclick.bind(this)} text={this.state.savedState} />
                  <Button handler={this.toggle.bind(this)} text="Cancel" />
                </div>
              </div>
            </div>
          </div>
          {/* Snakbar Components */}
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(UserAbout);
