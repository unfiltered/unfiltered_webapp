// Main React Components
import React, { Component } from "react";

// Scss
import "./UserProfile.scss";
// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
// import Switch from "@material-ui/core/Switch";
import { getCookie, setCookie } from "../../../../lib/session";
import { connect } from "react-redux";
// Switch Components
import Switch from "react-switch";
import { UserProfileNewData } from "../../../../controller/auth/verification";
import Snackbar from "../../../../Components/Snackbar/Snackbar";

const styles = () => ({
  commonwidth: {
    width: "100%",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
  },
});

class UserOtherDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dontShowMyAge: false,
      dontShowMyDist: false,
      Otherdetailpayload: {},
    };
    this.handleChangeAge = this.handleChangeAge.bind(this);
    this.handleChangeDist = this.handleChangeDist.bind(this);
  }

  returnTrueOrFalseBasedOnToggle = (val) => {
    if (val === true) {
      return 1;
    } else if (val === false) {
      return 0;
    }
  };

  // Function for the Age Switch
  handleChangeAge(dontShowMyAge) {
    this.setState({ dontShowMyAge });

    let dontShowMyAgePayload = {
      dontShowMyAge: this.returnTrueOrFalseBasedOnToggle(dontShowMyAge),
    };

    UserProfileNewData(dontShowMyAgePayload)
      .then((data) => {
        console.log("data", data);
        this.props.handleupdatedata();
        // this.setState({
        //   open: true,
        //   variant: "success",
        //   usermessage: "User Preference Update successfully.!!"
        // });
      })
      .catch((error) => {
        console.log("error", error);
        // this.setState({
        //   open: true,
        //   variant: "error",
        //   usermessage: "Some Error Occured.!!"
        // });
      });
  }

  // Function for the Distance Switch
  handleChangeDist(dontShowMyDist) {
    this.setState({ dontShowMyDist });

    let dontShowMyDistPayload = {
      dontShowMyDist: this.returnTrueOrFalseBasedOnToggle(dontShowMyDist),
    };

    UserProfileNewData(dontShowMyDistPayload)
      .then((data) => {
        console.log("data", data);
        this.props.handleupdatedata();
        // this.setState({
        //   open: true,
        //   variant: "success",
        //   usermessage: "User Preference Update successfully.!!"
        // });
      })
      .catch((error) => {
        console.log("error", error);
        // this.setState({
        //   open: true,
        //   variant: "error",
        //   usermessage: "Some Error Occured.!!"
        // });
      });
  }

  componentDidMount() {
    if (getCookie("AgeDetails") === 1) {
      this.setState({ dontShowMyAge: true }, () => {
        setCookie("AgeDetails", 1);
      });
    } else if (getCookie("AgeDetails") === 0) {
      this.setState({ dontShowMyAge: false }, () => {
        setCookie("AgeDetails", 0);
      });
    }
    if (getCookie("DistanceDetails") === 1) {
      this.setState({ dontShowMyDist: true }, () => {
        setCookie("DistanceDetails", 1);
      });
    } else if (getCookie("DistanceDetails") === 0) {
      this.setState({ dontShowMyDist: false }, () => {
        setCookie("DistanceDetails", 0);
      });
    }
  }

  render() {
    const { classes } = this.props;
    console.log('isPremiumUser', this.props.isPremiumUser)
    // return this.props.isPremiumUser.ProUserDetails.subscriptionId !== "Free Plan" ? (
    return (
      <div className="py-4 border-bottom" data-test="component-mainOtherDetail">
        <div className="profile_header">
          <h5>Other -</h5>
        </div>

        <div className="col-12">
          <div className="row">
            <div className="col-5">
              <p className={classes.label}>Don't show my Age :</p>
            </div>
            <div className="col-7 text-left">
              <Switch
              disabled={this.props.isPremiumUser && this.props.isPremiumUser.ProUserDetails && this.props.isPremiumUser.ProUserDetails.subscriptionId === "Free Plan" ? true: false}
                onChange={this.handleChangeAge}
                checked={this.state.dontShowMyAge}
                onColor="#EF4077"
                onHandleColor="#fff"
                handleDiameter={25}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={48}
                className="react-switch"
                id="material-switch"
              />
            </div>
          </div>

          {/* Distance Module */}
          <div className="row">
            <div className="col-5">
              <p className={classes.label}>Don't show my Distance :</p>
            </div>
            <div className="col-7 text-left">
              <Switch
                disabled={this.props.isPremiumUser && this.props.isPremiumUser.ProUserDetails && this.props.isPremiumUser.ProUserDetails.subscriptionId === "Free Plan" ? true: false}
                onChange={this.handleChangeDist}
                checked={this.state.dontShowMyDist}
                onColor="#EF4077"
                onHandleColor="#fff"
                handleDiameter={25}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                height={20}
                width={48}
                className="react-switch"
                id="material-switch"
              />
            </div>
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
    // ) : (
    //   <div />
    // );
  }
}

const mapStateToProps = (state) => {
  return {
    isPremiumUser: state.ProUser,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(UserOtherDetail));
