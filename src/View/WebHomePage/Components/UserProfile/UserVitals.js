import React, { Component } from "react";
import "./UserProfile.scss";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MAT_UI_Modal from "../../../../Components/Model/MAT_UI_Modal";
import { withStyles } from "@material-ui/core/styles";
import Select from "react-select";
import Button from "../../../../Components/Button/Button";
import { UserdataPrefence } from "../../../../controller/auth/verification";

const styles = () => ({
  commonwidth: {
    width: " 100%",
  },
  label: {
    fontSize: "15px",
    color: "#AFAFAF",
    fontFamily: "Product Sans",
  },
});

let selectuserinput = "";
class UserVitals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      novalue: "N/A",
      hover: false,
      namePayload: {},
      multiSelect: [],
      open: false,
      newSelectedHooby: null,
      savedState: "Save",
      UserFinalData: {},
      selectedOption: [],
      work: "",
      componentUpdated: false,
      modal: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggleModal = () => this.setState({ modal: !this.state.modal });
  // Function to toggle Module
  toggle() {
    this.setState({
      shown: !this.state.shown,
    });
  }

  // Function for the Hover on
  hoverOn = () => {
    this.setState({ hover: true });
  };

  // Function for the Hover off
  hoverOff = () => {
    this.setState({ hover: false });
  };

  handleUserSelectsVitals = (pref_id, event) => {
    let updatedPreferences = {
      pref_id: pref_id,
      values: [event.value],
    };

    UserdataPrefence(updatedPreferences)
      .then((data) => {
        this.props.handleupdatedata();

        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Preference Update successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.",
        });
      });
  };

  submitWork = (pref_id, value) => {
    let updatedPreferences = {
      pref_id: pref_id,
      values: [value],
    };
    console.log("submit work", value);
    UserdataPrefence(updatedPreferences)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Preference Update successfully.",
          savedState: "Save",
        });
        this.toggle();
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.",
        });
      });
    this.setState({ componentUpdated: false });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.componentUpdated !== this.state.componentUpdated) {
      this.props.handleupdatedata();
    }
  }

  handleWork = (event) => {
    this.setState({ work: event.target.value, savedState: "Done Editing" });
  };

  static getDerivedStateFromProps(props, state) {
    if (props.UserFinalData && props.UserFinalData._id !== state.UserFinalData._id) {
      return {
        UserFinalData: props.UserFinalData,
        work: props.UserFinalData.myPreferences[2].data[1].selectedValues[0],
      };
    }
    return null;
  }

  handleMultiSelect = (event) => {
    let exData = [...this.state.selectedOption];
    if (exData.includes(event)) {
      let index = exData.indexOf(event);
      exData.splice(index, 1);
    } else {
      exData.push(event);
    }
    this.setState({ ...this.state, selectedOption: exData });
  };

  updateProfileData = async (prefData) => {
    let obj = {
      pref_id: prefData.pref_id,
      values: this.state.selectedOption,
    };
    let data = await UserdataPrefence(obj);
    if (data != undefined && data.status === 200) {
      this.setState({
        ...this.state,
        selectedOption: [],
        shown: false,
        open: true,
        variant: "success",
        usermessage: "Updated Preferences Successfully...",
      });
      this.props.handleupdatedata();
    }
  };

  getInput = (type, prefData, pref_id) => {
    let active = {
      padding: "8px 16px",
      margin: "5px 5px",
      borderRadius: "5px",
      border: "2px solid #e31b1b",
      color: "#e31b1b",
      cursor: "pointer",
    };
    let deactive = {
      padding: "8px 16px",
      borderRadius: "5px",
      margin: "5px 5px",
      border: "2px solid #DBDBDB",
      color: "#484848",
      cursor: "pointer",
    };
    switch (type) {
      case 1:
        let options1 = [];
        prefData.options.map((data) =>
          options1.push({
            label: data,
            value: data,
            controlLable: prefData.label,
          })
        );

        let SelectedUserValue1 = {
          label: selectuserinput,
          value: selectuserinput,
        };
        let SelectDropDown = (
          <div>
            <div className="select_dropdownedit">
              <Select onChange={this.handleUserSelectsVitals.bind(this, pref_id)} options={options1} defaultValue={[SelectedUserValue1]} />
            </div>
          </div>
        );
        return SelectDropDown;
      case 2:
        return (
          <div className="col-12">
            <div className="row pt-4 align-items-center">
              {prefData &&
                prefData.options &&
                prefData.options.map((k, index) => (
                  <div
                    key={index}
                    style={this.state.selectedOption && this.state.selectedOption.includes(k) ? active : deactive}
                    onClick={() => this.handleMultiSelect(k)}
                  >
                    {k}
                  </div>
                ))}
              <Button className="submitBtn ml-2" handler={() => this.updateProfileData(prefData)} text="Save"></Button>
            </div>
            {/* <div className="col-12 text-center"></div> */}
          </div>
        );
      case 5:
        return (
          <div className="col-12 px-0">
            <div className="row mx-0">
              <div class="col-8 px-0 form-group">
                <Button className="submitBtn" handler={() => this.updateProfileData(prefData, false)} text="Update" />
                {/* <input
                  value={(this.state && this.state.body) || (prefData && prefData.selectedValues && prefData.selectedValues[0])}
                  type="text"
                  onChange={(e) => this.handler(e)}
                  class="form-control"
                  name={}
                  placeholder={prefData && prefData.options && prefData.options[0]}
                /> */}
              </div>
            </div>
          </div>
        );
      default:
        console.log("none");
    }
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the Hoobies OnChange Value
  handlehobbies = (newArray) => {
    this.setState({ newSelectedHooby: newArray });
  };

  // API Call the UserHobies Module
  handleUserSelects = (pref_id, event) => {
    let arr = [];
    // console.log("arr", arr);
    let updatedPreferences = {
      pref_id: pref_id,
      values: arr,
    };
    // console.log("chnagedvalue", updatedPreferences);

    UserdataPrefence(updatedPreferences)
      .then((data) => {
        this.props.handleupdatedata();

        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Preference Update successfully.!!",
        });
        this.toggle();
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
        this.toggle();
      });
  };

  render() {
    const { classes } = this.props;

    var hidden = {
      display: this.state.shown ? "block" : "none",
    };

    var shown = {
      display: this.state.shown ? "none" : "block",
    };

    return (
      <div data-test="component-mainVitals" className={"pt-4 pb-3 border-bottom"}>
        {this.props.UserFinalData &&
          this.props.UserFinalData.myPreferences &&
          this.props.UserFinalData.myPreferences.map((data, index) =>
            index === 2 ? (
              <div key={index}>
                <div className="profile_header">
                  <h5>
                    {data.title} -
                    <i className="fas fa-pencil-alt" onClick={this.toggle.bind(this)} /> {this.state.shown ? "Editing" : ""}
                  </h5>
                </div>
                {data.data.map(
                  (pref, index) => (
                    (selectuserinput = pref.selectedValues[0]),
                    pref.isDone || this.state.shown ? (
                      <div key={"UserVitals" + index}>
                        {/* Used to show UserDetail Module */}
                        <div className="col-12 user_module" style={shown}>
                          <div className="row">
                            <div className="col-6 col-sm-6 col-md-6 col-lg-5 col-xl-5">
                              <p>{pref.label} :</p>
                            </div>
                            <div className="text-left col-6 col-sm-6 col-md-6 col-lg-7 col-xl-7">
                              <p>{pref.selectedValues.join(",")}</p>
                            </div>
                          </div>
                        </div>
                        {/* User for edit UserDetails */}
                        <div className="ppp">
                          <div className="col-12" style={hidden}>
                            <div className="py-2 row">
                              <div className="pt-1 text-left col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <p className={classes.label}>{pref.label} :</p>
                              </div>
                              <div className="pl-0 col-6 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                                {this.getInput(pref.type, pref, pref.pref_id)}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )
                  )
                )}
              </div>
            ) : (
              ""
            )
          )}
        {/* Snakbar Components */}
        <MAT_UI_Modal></MAT_UI_Modal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default withStyles(styles)(UserVitals);
