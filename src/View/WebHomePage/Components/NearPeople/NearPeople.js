// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./NearPeople.scss";
import Webheader from "../../../../Components/WebUserHeader/index";
import UserCard from "../../../../Components/WebUserCard/UserCard";
import { connect } from "react-redux";
import { discoverPeople } from "../../../../actions/DiscoverPeople";
import { withStyles } from "@material-ui/core/styles";
import { NearbyPeople } from "../../../../controller/auth/verification";
import { searchUserByPagination } from "../../../../services/auth";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";
import { getBoostDetailsOnWindowRefresh } from "../../../../actions/boost";
import { USER_PROFILE_ACTION_FUN } from "../../../../actions/UserProfile";

const styles = (theme) => ({
  progress: {
    position: "absolute",
    zIndex: 999,
    left: " 45%",
    top: "35%",
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "#e31b1b",
  },
  disabledButton: {
    backgroundColor: "#f8f8f8",
  },
});

class Main extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      collapse: false,
      shown: true,
      hidden: SVGComponentTransferFunctionElement,
      modalmodel: false,
      UserSearchresult: [],
      loader: false,
      start: 0,
      end: 20,
      loaderStatus: "Show More",
      endTime: "",
      remainsLikesInString: "",
    };

    this.toggle = this.toggle.bind(this);
    this.togglemodel = this.togglemodel.bind(this);
  }

  getDataWithAndWithoutScroll = (start, end) => {
    let usersArr = [...this.state.UserSearchresult];
    searchUserByPagination(start, end)
      .then((res) => {
        let newData = [...usersArr, ...res.data.data];
        this.props.setAllDiscoverPeople(newData);
        this.props.getBoostDetailsOnWindowRefresh(res.data.boost);
        this.props.saveRemainingLikes(res.data.remainsLikesInString);
        this.setState({
          loader: false,
          UserSearchresult: newData,
          loaderStatus: "Show More",
          remainsLikesInString: res.data.remainsLikesInString,
        });
      })
      .catch((err) => {
        this.setState({ loader: true, loaderStatus: "Show More" });
      });
  };

  // On Components Load this Will be Called
  componentDidMount() {
    this.setState({ loader: true });
    this.props.handleWebsiteActiveValue("/app");
    this.props.renderBgColorBasedOnLink(window.location.pathname);
    this.getDataWithAndWithoutScroll(this.state.start, this.state.end);
  }

  // Function to Get Instant API Call for After Clicking the Like & Dislike & Superlike Button Press
  handleNearPeopleUpdated = () => {
    NearbyPeople()
      .then((data) => {
        console.log("handleNearPeopleUpdated called");
        this.props.setAllDiscoverPeople(data.data.data);
        this.setState({
          UserSearchresult: [...data.data.data],
        });
      })
      .catch(() => {
        this.setState({ loader: false });
      });
  };

  // FUnction to Toggle the Collapse Module
  toggle() {
    this.setState((state) => ({ collapse: !state.collapse }));
  }

  // FUnction to Toggle the Model
  togglemodel() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  loadMore = () => {
    this.setState({ loaderStatus: "Loading..." });
    if (this.state.UserSearchresult.length % 20 === 0) {
      this.setState({ start: this.state.start + 20, end: this.state.end }, () => {
        this.getDataWithAndWithoutScroll(this.state.start, this.state.end);
      });
    } else {
      this.setState({ loaderStatus: "No More Users" });
    }
  };

  render() {
    return (
      <div id="innerVideoContent">
        <Webheader
          endTimer={
            this.props && this.props.onRefreshTime && Object.keys(this.props.onRefreshTime).length > 0
              ? this.props.onRefreshTime
              : this.props.onFlyBoostActivated
          }
          onclick={this.toggle}
          BoostActivatedSessionIncomingData={this.props.BoostActivatedSessionIncomingData}
          handleNearPeople={this.handleNearPeople}
        />
        {/* Meet New User Banner Section Start */}
        <div className="WMeetuserscroll" id="WMeetuserscroll" ref={this.myRef}>
          <UserCard
            loaderStatus={this.state.loaderStatus}
            loadMore={this.loadMore}
            UserSearchresult={this.props.discoverPeopleList}
            remainsLikesInString={this.state.remainsLikesInString}
            handleNearPeopleUpdated={this.handleNearPeopleUpdated}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    discoverPeopleList: state.discoverPeople.discoverPeopleList,
    onRefreshTime: state.Boost.BoostDetailsOnRefresh,
    onFlyBoostActivated: state.Boost.BoostProfile,
    BoostActivatedSessionIncomingData: state.Boost.BoostActivatedSessionIncomingData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBoostDetailsOnWindowRefresh: (data) => dispatch(getBoostDetailsOnWindowRefresh(data)),
    setAllDiscoverPeople: (data) => dispatch(discoverPeople(data)),
    saveRemainingLikes: (data) => dispatch(USER_PROFILE_ACTION_FUN("remainsLikesInString", data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withStyles(styles)(Main)));
