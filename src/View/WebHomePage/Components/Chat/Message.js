// Main React Components
import React, { useState } from "react";
import Viewer from "react-viewer";
// Moment Components
import { Images } from "../../../../Components/Images/Images";
import moment from "moment-timezone";

// imported Components
import doubleTick from "../../../../asset/svg/double-tick-indicator.svg";
import blueTick from "../../../../asset/svg/double-tick-indicator-blue.svg";
import singleTick from "../../../../asset/svg/check-symbol.svg";
import ReactPlayer from "react-player";
import ChatMapView from "../Maps/ChatMapView";

const Message = ({ chat, sendersName, mobile, textdecode }) => {
  const [visible, setVisible] = useState(false);
  const [imageUrl, setUrl] = useState("");

  let extractLatAndLng = (str) => {
    let i = 0;
    let arr = "";
    while (str[i] !== "@") {
      arr += str[i++];
    }
    let _string = arr.substr(1, arr.length - 2);
    let s = _string.split(",");
    return { lat: s[0], lng: s[1] };
  };

  let renderByStatus = (status) => {
    if (status === 1 || status === "1") {
      return <img src={singleTick} alt="single-tick" className="ml-2" width="20px" />;
    } else if (status === 2 || status === "2") {
      return <img src={doubleTick} alt="double-tick" className="ml-2" width="20px" />;
    } else if (status === 3 || status === "3") {
      return <img src={blueTick} alt="double-blue-tick" className="ml-2" width="20px" />;
    }
  };

  const onErrorM = (e) => {
    e.target.src = Images.brokenImage;
  };

  let returnMessageWithType = "";
  if (chat.messageType === "1" || chat.type === "1") {
    let simpleMsg = chat.payload;
    let encodeMsg = atob(simpleMsg);
    returnMessageWithType = (
      <div
        className={`image ${chat.name === sendersName ? "right" : "left"}`}
        onClick={() => {
          setUrl(encodeMsg);
          setVisible(!visible);
        }}
      >
        <div>
          <img
            src={encodeMsg}
            onError={onErrorM}
            alt={chat.payload}
            style={{ height: "200px", width: mobile ? "240px" : "280px", objectFit: "cover" }}
          />
        </div>
        {chat.name === sendersName ? (
          <span className={chat && chat.payload && chat.payload.includes("https") ? "col-12 text-right p-0" : "pl-1 attachment_ack"}>
            <span className={`right`}>
              {moment.unix(chat.timestamp / 1000).format("LT")}
              {renderByStatus(chat.status)}
            </span>
          </span>
        ) : (
          <span
            className={
              chat && chat.payload && chat.payload.includes("https")
                ? "col-12 text-right p-0"
                : mobile
                ? "attachment_ack_m_l"
                : "pl-1 attachment_ack"
            }
          >
            <span className={"ack left"}>{moment.unix(chat.timestamp / 1000).format("LT")}</span>
          </span>
        )}

        <Viewer
          drag={false}
          visible={visible}
          onClose={() => {
            setVisible(false);
          }}
          images={[{ src: imageUrl, alt: imageUrl }]}
        />
      </div>
    );
  } else if (chat.messageType === "8" || chat.type === "8") {
    returnMessageWithType = (
      <div className={`image ${chat.name === sendersName ? "right" : "left"}`}>
        <div>
          <img
            alt={chat.payload}
            src={Buffer.from(chat && chat.payload, "base64").toString("ascii")}
            style={{ height: "200px", width: mobile ? "240px" : "280px", objectFit: "cover" }}
          />
        </div>
        {chat.name === sendersName ? (
          <span className={chat && chat.payload && chat.payload.includes("https") ? "col-12 text-right p-0" : "pl-1 attachment_ack"}>
            <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>
              {moment.unix(chat.timestamp / 1000).format("LT")}
              {renderByStatus(chat.status)}
            </span>
          </span>
        ) : (
          <span
            className={
              chat && chat.payload && chat.payload.includes("https")
                ? "col-12 text-right p-0"
                : mobile
                ? "attachment_ack_m_l"
                : "pl-1 attachment_ack"
            }
          >
            <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>{moment.unix(chat.timestamp / 1000).format("LT")}</span>
          </span>
        )}
      </div>
    );
  } else if (chat.messageType === "6" || chat.type === "6") {
    returnMessageWithType = (
      <div className={`image ${chat.name === sendersName ? "right" : "left"}`}>
        <div>
          <img
            alt={chat && chat.payload && chat.payload}
            src={Buffer.from(chat && chat.payload && chat.payload, "base64").toString("ascii")}
            style={{ height: "200px", width: mobile ? "240px" : "280px", objectFit: "cover" }}
          />
        </div>
        {chat.name === sendersName ? (
          <span className={chat && chat.payload && chat.payload.includes("https") ? "col-12 text-right p-0" : "pl-1 attachment_ack"}>
            <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>
              {moment.unix(chat.timestamp / 1000).format("LT")}
              {renderByStatus(chat.status)}
            </span>
          </span>
        ) : (
          <span
            className={
              chat && chat.payload && chat.payload.includes("https")
                ? "col-12 text-right p-0"
                : mobile
                ? "attachment_ack_m_l"
                : "pl-1 attachment_ack"
            }
          >
            <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>{moment.unix(chat.timestamp / 1000).format("LT")}</span>
          </span>
        )}
      </div>
    );
  } else if (chat.messageType === "3" || chat.type === "3") {
    let simpleMsg = chat && chat.payload && chat.payload;
    let encodeMsg = extractLatAndLng(atob(simpleMsg));
    let returnMessageWithType;
    {
      encodeMsg.lat !== "" && encodeMsg.lng !== ""
        ? (returnMessageWithType = (
            <div className={`image ${chat.name === sendersName ? "right" : "left"}`}>
              <div>
                <ChatMapView lat={encodeMsg.lat} lng={encodeMsg.lng} mobile={mobile} />
              </div>
              {chat.name === sendersName ? (
                <span className={chat && chat.payload && chat.payload.includes("https") ? "col-12 text-right p-0" : "pl-1 attachment_ack"}>
                  <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>
                    {moment.unix(chat.timestamp / 1000).format("LT")}
                    {renderByStatus(chat.status)}
                  </span>
                </span>
              ) : (
                <span
                  className={
                    chat && chat.payload && chat.payload.includes("https")
                      ? "col-12 text-right p-0"
                      : mobile
                      ? "attachment_ack_m_l"
                      : "pl-1 attachment_ack"
                  }
                >
                  <span className={`ack ${chat.name === sendersName ? "right" : "left"}`}>
                    {moment.unix(chat.timestamp / 1000).format("LT")}
                  </span>
                </span>
              )}
            </div>
          ))
        : (returnMessageWithType = (
            <div className={`normalText ${chat.name === sendersName ? "right" : "left"}`}>
              <div>
                <span>
                  Your location message got corrupted.
                  {chat.name === sendersName ? (
                    <span className="pl-1">
                      {moment.unix(chat.timestamp / 1000).format("LT")}
                      {renderByStatus(chat.status)}
                    </span>
                  ) : (
                    <span className="pl-1">{moment.unix(chat.timestamp / 1000).format("LT")}</span>
                  )}
                </span>
              </div>
            </div>
          ));
      return returnMessageWithType;
    }
  } else if (chat.messageType === "2" || chat.type === "2") {
    let simpleMsg = chat && chat.payload && chat.payload;
    let encodeMsg = atob(simpleMsg);
    returnMessageWithType = (
      <div className={`image ${chat.name === sendersName ? "right" : "left"}`}>
        <div className="row mx-0" style={{ height: "inherit", width: "inherit" }}>
          <ReactPlayer width={mobile ? 240 : 280} height={mobile ? 240 : 200} url={encodeMsg} playing={true} controls={true} />
        </div>
      </div>
    );
  } else if (chat.messageType === "15" || chat.type === "15" || chat.messageType === 15 || chat.type === 15) {
    // senders name is my name -> logged in users name
    returnMessageWithType = (
      <div className={`normalText ${chat.name !== sendersName ? "right" : "left"}`}>
        <div>
          <span>
            {chat.name !== sendersName ? "You have sent " + chat.amount + " to " + chat.name : " You received " + chat.payload}
            {chat.name !== sendersName ? (
              <span className="pl-1">
                {moment.unix(chat.timestamp / 1000).format("LT")}
                {renderByStatus(chat.status)}
              </span>
            ) : (
              <span className="pl-1">{moment.unix(chat.timestamp / 1000).format("LT")}</span>
            )}
          </span>
        </div>
      </div>
    );
  } else {
    let simpleMsg = chat && chat.payload && chat.payload;
    let encodeMsg = textdecode(simpleMsg);
    returnMessageWithType = (
      <div className={`normalText ${chat.name === sendersName ? "right" : "left"}`}>
        <div>
          <span>
            {encodeMsg && encodeMsg.includes("https") ? (
              <a target="_blank" href={encodeMsg} style={{ cursor: "pointer" }}>
                {encodeMsg}
              </a>
            ) : (
              encodeMsg
            )}
            {chat.name === sendersName ? (
              <span className="pl-1">
                {moment.unix(chat.timestamp / 1000).format("LT")}
                {renderByStatus(chat.status)}
              </span>
            ) : (
              <span className="pl-1">{moment.unix(chat.timestamp / 1000).format("LT")}</span>
            )}
          </span>
        </div>
      </div>
    );

    return returnMessageWithType;
  }
  return returnMessageWithType;
};

export default Message;
