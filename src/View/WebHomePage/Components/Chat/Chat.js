import React, { Component } from "react";
import "./Chat.scss";
import WebHeader from "../../../../Components/WebHeader/index";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import { Media } from "reactstrap";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Chatroom from "./ChatList";
import moment from "moment";
import { withRouter } from "react-router-dom";
import * as actions from "../../../../actions/UserSelectedToChat";
import * as actionsUser from "../../../../actions/UserProfile";
import * as actionChat from "../../../../actions/chat";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import Fade from "react-reveal/Fade";
import {
  GetAllChats,
  GetAllChatsForSingleUser,
  CoinChat,
  DeleteSelectedMessages,
  BlockUser,
  unmatchuser,
  deletechatTotally,
  createDate,
  unblockuser,
  ReasonblockUser,
  ReportUser,
  UnfriendUser,
  uploadMediaOverLocalServer,
} from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import Skeleton from "react-loading-skeleton";
import { demoThumbNail } from "./DemoThumbnail";
// Redux Components
import { connect } from "react-redux";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Images } from "../../../../Components/Images/Images";
import { Icons } from "../../../../Components/Icons/Icons";
// import RTCMesh from "../WebRtc/RTCMesh";
// import { ICE_SERVER_URLS } from "../WebRtc/functions/constants";
import { __callInit } from "../../../../actions/webrtc";
import { keys } from "../../../../lib/keys";
import CustomButton from "../../../../Components/Button/Button";
import { FormattedMessage } from "react-intl";
import {
  subscribeToUserTopic,
  unSbscribeToUserTopic,
  sendMessageOverMqtt,
  sendAcknowledgementsOverMqtt,
} from "../../../../lib/MqttHOC/utils";

require("dotenv").config();

var foursquare = require("react-foursquare")({
  clientID: keys.foursquareClientID,
  clientSecret: keys.foursquareClientSecret,
});

const styles = (theme) => ({
  progress: {
    position: "absolute",
    zIndex: 999,
    left: " 45%",
    top: "35%",
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
  root: {
    fontFamily: "Product Sans",
    textAlign: "center",
  },
  header: {
    fontSize: "28px",
    fontFamily: "Product Sans Bold",
    fontWeight: 900,
    color: "#474747",
  },
  payload: {
    fontSize: "13px",
    fontFamily: "Product Sans",
    color: "#B4B9C4",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "12vw",
  },
  firstName: {
    fontSize: "15px",
    fontFamily: "Product Sans Bold",
    fontWeight: 900,
    color: "#484848",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  selectedUserToChat: {
    fontFamily: "Product Sans Bold",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    fontSize: "16px",
    color: "#484848",
    fontWeight: 900,
    maxWidth: "200px",
  },
  onlineOffline: {
    fontFamily: "Product Sans",
    fontSize: "14px",
    color: "#B4B9C4",
  },
  formControl: {
    width: "100%",
  },
  menuItem: {
    fontSize: "14px",
    fontFamily: "Product Sans",
    color: "#484848",
  },
  select: {
    fontSize: "14px",
    fontFamily: "Product Sans",
    color: "#e31b1b",
    background: "#FFF",
  },
  textField: {
    width: "100%",
  },
});

// const ITEM_HEIGHT = 48;

class ChatPage extends Component {
  constructor(props) {
    super(props);
    this.node = React.createRef();
    this.state = {
      value: null,
      valueOfTabs: 0,
      width: window.innerWidth,
      pageNo: 0,
      usermessage: "Message Sent Successfully",
      usersFiltered: [],
      usersMessagesOnMount: [],
      images: [],
      chats: [],
      MatchFoundData: [],
      items: [],
      lat: getCookie("lat"),
      long: getCookie("lng"),
      time: "",
      anchorEl: "",
      updateduserprofile: "",
      variant: "",
      selectedIdOfUserToMessage: "",
      locationSelected: "",
      reportReasons: "",
      reportedUserReason: "",
      textToRender: "",
      messagedrawer: false,
      attachmentOpen: false,
      loader: false, // to show circular loader on blocking/unblocking/reporting user.
      openAttachent: false,
      open: false,
      locationmodel: false,
      chatsLoaded: false,
      reportopen: false,
      subDialog: false,
      showCoinAnimation: false,
      purcahseCoin: false,
      loadingImageSkeleton: false,
      dateModal: false,
      physicalDateModal: false,
      videoDateModal: false,
      callDateModal: false,
      tooltipOpensettings2: false,
      tooltipOpensettings3: false,
      prevUserId: "",
      previewSendModal: false,
      openCoinPopup: false,
      attachmentAnchor: "",
      loadingChats: false, // will show the loader of all chats being loaded on didMount()
      isNewUserChatFocused: false, // to check if a new user -> coin user is to chat,
      chatDeleteSuccessfull: false, // to check if the chat has deleted, so can do API call,
      coinPopUpSuccessfull: false,
      confirmAudioDate: false, // confirm popup modal
      confirmPhysicalDate: false, // confirm popup modal
      confirmVideoDate: false, // confirm popup modal
      case: "",
      newFiles: "",
      lat: "",
      lng: "",
      placeName: "",
      onClickedAddress: "",
    };
    this.togglesetting2 = this.togglesetting2.bind(this);
    this.togglesetting3 = this.togglesetting3.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  confirmVideoDateModal = () => {
    this.setState({ confirmVideoDate: !this.state.confirmVideoDate });
  };

  confirmPhysicalDateDateModal = () => {
    this.setState({ confirmPhysicalDate: !this.state.confirmPhysicalDate });
  };

  confirmAudioDateModal = () => {
    this.setState({ confirmAudioDate: !this.state.confirmAudioDate });
  };

  // create date function
  createDateFn = (targetUserId, time, dateType, lat, lng, place) => {
    let PLAN =
      this.props.checkIfUserIsProUser &&
      this.props.checkIfUserIsProUser.ProUserDetails &&
      this.props.checkIfUserIsProUser.ProUserDetails.tag;
    let { physicalDate, audioDate, videoDate } = this.props.CoinConfig;
    if (this.props.CoinBalance > 4) {
      if (dateType === 3 || dateType === 1) {
        createDate(getCookie("token"), targetUserId, time, dateType, lat, lng, place, PLAN)
          .then((res) => {
            if (res.status === 200) {
              if (dateType === 1) {
                this.props.coinReduce(parseInt(videoDate.Coin));
                // this.toggleVideoCall();
              } else if (dateType === 3) {
                this.props.coinReduce(parseInt(audioDate.Coin));
                // this.toggleAudioCall();
              }
              this.setState({
                usermessage: "Date Created Successfully.",
                open: true,
                variant: "success",
              });
            } else if (res.status === 422) {
              this.setState({
                usermessage: res.message,
                open: true,
                variant: "error",
              });
            } else {
              this.setState({
                usermessage: res.message,
                open: true,
                variant: "error",
              });
            }
          })
          .catch((err) => {
            this.setState({
              usermessage: "Something went wrong...!",
              open: true,
              variant: "error",
            });
          });
      } else if (dateType === 2) {
        if (place === "" || place === undefined) {
          this.setState({
            usermessage: "Please select place.",
            open: true,
            variant: "warning",
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 2000);
        } else {
          createDate(getCookie("token"), targetUserId, time, dateType, lat, lng, place, PLAN)
            .then((res) => {
              if (res.status === 200) {
                this.props.coinReduce(parseInt(physicalDate.Coin));
                this.setState({
                  usermessage: "Date Created Successfully.",
                  open: true,
                  variant: "success",
                });
              } else if (res.status === 422) {
                this.setState({
                  usermessage: res.message,
                  open: true,
                  variant: "error",
                });
              } else {
                this.setState({
                  usermessage: res.message,
                  open: true,
                  variant: "error",
                });
              }
            })
            .catch((err) => {
              this.setState({
                usermessage: "Something went wrong...!",
                open: true,
                variant: "error",
              });
            });
        }
      }
    } else {
      this.setState({
        usermessage: "You do not have enough coins to create date.",
        open: true,
        variant: "error",
      });
    }
  };

  // function to unfriend user
  unfriendUser = (targetUserId) => {
    let leftSideData = [...this.props.leftSideChatLogs];
    let _unfriendUser = { ...this.props.UserSelectedToChat };
    let index = leftSideData.findIndex((k) => k.recipientId === targetUserId);
    if (index > -1) {
      UnfriendUser(getCookie("token"), targetUserId)
        .then((res) => {
          leftSideData[index].isFriend = 0;
          _unfriendUser["isFriend"] = 0;
          this.props.userSelectedToChat(_unfriendUser);
          this.props.leftSideChatLogsFN(leftSideData);
          console.log("unfriend user successfull");
          this.setState({
            usermessage: "UnFriend user successfull",
            variant: "success",
            open: true,
            anchorEl: null,
          });
        })
        .catch((err) => {
          console.log("error unfriending user");
        });
      setTimeout(() => {
        this.setState({ open: false, usermessage: "" });
      }, 2000);
    } else {
      let leftSideDataV2 = [...this.state.MatchFoundData];
      let newIndex = leftSideDataV2.findIndex((k) => k.recipientId === targetUserId);
      if (newIndex > -1) {
        UnfriendUser(getCookie("token"), targetUserId)
          .then((res) => {
            this.props.userSelectedToChat("");
            leftSideDataV2.splice(newIndex, 1);
            this.setState({
              MatchFoundData: leftSideDataV2,
              usermessage: "UnFriend user successfull",
              variant: "success",
              open: true,
              anchorEl: null,
            });
            this.props.chatSessionEnded();
          })
          .catch((err) => {
            console.log("error unfriending user");
          });
        setTimeout(() => {
          this.setState({ open: false, usermessage: "" });
        }, 2000);
      }
    }
  };

  // toggle physical call modal
  togglePhysicalDateModal = () => {
    if (this.state.dateModal) {
      this.setState({ dateModal: false });
    }
    setTimeout(() => {
      this.setState({ physicalDateModal: !this.state.physicalDateModal });
    }, 300);
  };

  // toggle physical audio modal
  toggleAudioCall = () => {
    if (this.state.dateModal) {
      this.setState({ dateModal: false });
    }
    setTimeout(() => {
      this.setState({ callDateModal: !this.state.callDateModal });
    }, 300);
  };

  // toggle video call modal
  toggleVideoCall = () => {
    if (this.state.dateModal) {
      this.setState({ dateModal: false });
    }
    setTimeout(() => {
      this.setState({ videoDateModal: !this.state.videoDateModal });
    }, 300);
  };

  // Used to Open Report User Dialog
  openReportModal = () => {
    this.setState({ reportopen: true, anchorEl: null });
  };

  // Used to Close Report User Dialog
  closeReportModal = () => {
    this.setState({ reportopen: false });
  };

  togglesetting2() {
    this.setState({
      tooltipOpensettings2: !this.state.tooltipOpensettings2,
    });
  }

  togglesetting3() {
    this.setState({
      tooltipOpensettings3: !this.state.tooltipOpensettings3,
    });
  }

  // close location modal
  closelocationmodel = () => {
    this.setState({ locationmodel: false });
  };

  // open location modal
  togglelocationmodel = () => {
    this.setState({ locationmodel: true });
  };

  // Coin chat
  CoinChat = (obj) => {
    console.log("coin chat", obj);
    obj["isMatchedUser"] = 0;
    let { perMsgWithoutMatch } = this.props.CoinConfig;
    let updateUserSelectedChat = { ...this.props.UserSelectedToChat };
    CoinChat(obj)
      .then((res) => {
        console.log("coin chat response", res);
        if (res.status === 200) {
          console.log("%c coin chat", "background: #000; color: #fff");
          this.setState({
            showCoinAnimation: true,
          });
        }
        if (
          this.props.selectedProfile.selectedProfile._id === this.props.UserSelectedToChat._id ||
          this.props.selectedProfile.selectedProfile._id === this.props.UserSelectedToChat.userId ||
          this.props.selectedProfile.selectedProfile._id === this.props.UserSelectedToChat.opponentId
        ) {
          obj["timestamp"] = obj.timestamp;
          obj["chatId"] = res.data.chatId;
          this.setState({ isNewUserChatFocused: true });
          updateUserSelectedChat["timestamp"] = obj.timestamp;
          updateUserSelectedChat["chatId"] = res.data.chatId;
          updateUserSelectedChat["isMatchedUser"] = 0;
          this.props.userSelectedToChat(updateUserSelectedChat);
          this.setState({
            showCoinAnimation: true,
          });
          this.addNewMessage(obj);
        }
        if (
          updateUserSelectedChat.initiated ||
          !("initiated" in updateUserSelectedChat) ||
          !("chatInitiatedBy" in updateUserSelectedChat)
        ) {
          this.setState({
            showCoinAnimation: true,
          });
          this.props.coinReduce(perMsgWithoutMatch.Coin);
          // this.addNewMessage(obj);
        }
      })
      .catch((err) => {
        if (
          updateUserSelectedChat.initiated ||
          !("initiated" in updateUserSelectedChat) ||
          !("chatInitiatedBy" in updateUserSelectedChat)
        ) {
          console.log("%c coin chat ERROR", "background: #000; font-size: 18px; color: #fff");
          this.setState({
            showCoinAnimation: true,
          });
          this.props.coinReduce(perMsgWithoutMatch.Coin);
        }
        console.log(err);
      });
    setTimeout(() => {
      this.setState({ showCoinAnimation: false });
    }, 1000);
  };

  componentDidMount() {
    this.setState({ loadingChats: true, userId: getCookie("uid") });
    this.props.handleWebsiteActiveValue("/app/chat");
    let params;
    if (getCookie("selectedLocation") != null) {
      let obj = JSON.parse(getCookie("selectedLocation"));
      console.log("111111");
      params = {
        ll: `${obj.lat}, ${obj.lng}`,
        query: "",
      };
      this.setState({ lat: obj.lat, lng: obj.lng });
    } else if (getCookie("lat") != null && getCookie("lng") != null) {
      console.log("222222");
      params = {
        ll: `${getCookie("lat")}, ${getCookie("lng")}`,
        query: "",
      };
    } else {
      console.log("333333");
      if (getCookie("baseAddress") != null) {
        params = {
          ll: `${getCookie("baseAddress").lat}, ${getCookie("baseAddress").lng}`,
          query: "",
        };
      }
    }
    let currentPath = window.location.pathname; // getting current page path
    window.onscroll = function () {
      if (window.pageYOffset === 0) {
        // alert("i am on top");
      }
    };
    this.props.renderBgColorBasedOnLink(currentPath);
    foursquare.venues.getVenues(params).then((res) => {
      console.log("FSQUARE", res);
      this.setState({
        items: res && res.response && res.response.venues,
        locationSelected: res && res.response && res.response.venues && res.response.venues[0].name,
      });
    });
    this.setState({ value: currentPath });
    let selectedUserData = this.props.selectedProfile.selectedProfile;
    /** this function gets the left side chats for the logged in user */
    GetAllChats(this.state.pageNo)
      .then((res) => {
        let allMessages = res.data.data;
        res.data.data.map((k) => {
          if (k.payload === "3embed test") {
            let arr = this.state.MatchFoundData.concat([k]);
            let result = arr.map(function (el) {
              var o = Object.assign({}, el);
              o.isInMatches = true;
              return o;
            });
            this.setState({ MatchFoundData: result });
          }
        });
        this.setState({ chatsLoaded: true, loadingChats: false });
        if (selectedUserData) {
          let foundUser = allMessages.find((k) => k.recipientId === selectedUserData._id || k.senderId === selectedUserData._id);
          if (foundUser) {
            let allUsersList = [...res.data.data];
            let result = allUsersList.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            this.props.leftSideChatLogsFN(result);
            this.handlemessage(foundUser._id, foundUser);
            console.log("[][][][][][][][] 1", foundUser._id);
            this.setState({
              usersFiltered: result,
              usersMessagesOnMount: result,
            });
            this.props.chatSessionStarted();
          } else {
            let allUsersList = [selectedUserData, ...res.data.data];
            let result = allUsersList.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            let id = result[0]._id || result[0].recipientId || result[0].receiverId || result[0].opponentId || result[0].receiverIdentifier;
            console.log("[][][][][][][][] 2", id);
            this.handlemessage(id, result[0]);
            this.props.leftSideChatLogsFN(result);
            this.setState({
              usersFiltered: result,
              usersMessagesOnMount: result,
            });
            this.props.chatSessionStarted();
          }
          if (allMessages.length === 0) {
            let result = selectedUserData.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            this.props.leftSideChatLogsFN([result]);
            this.setState({
              usersFiltered: [result],
              usersMessagesOnMount: [result],
            });
          }
        } else {
          let result = allMessages.map(function (el) {
            var o = Object.assign({}, el);
            o.checked = false;
            return o;
          });
          if (result.length > 0) {
            let id = result[0]._id || result[0].recipientId || result[0].receiverId || result[0].opponentId || result[0].receiverIdentifier;
            console.log("[][][][][][][][] 2", id);
            this.handlemessage(id, result[0]);
            this.props.leftSideChatLogsFN(result);
            this.setState({
              usersFiltered: result,
              usersMessagesOnMount: result,
            });
            this.props.chatSessionStarted();
          }
        }
      })
      .catch((err) => this.setState({ loadingChats: false }));
    document.addEventListener("mousedown", this.handleDropDown, true);
    // this.props.chatSessionStarted(); // chat started and 1st chat will be active
    ReasonblockUser().then((data) => {
      this.setState({ reportReasons: data.data.data });
    });
  }

  // update left side chat
  updateLeftSideChat = (obj) => {
    let leftSideAllChats = [...this.props.leftSideChatLogs];
    let index = leftSideAllChats.findIndex((k) => k.recipientId === obj.to || k._id === obj.to);
    if (index > -1) {
      leftSideAllChats[index].payload = obj.payload;
      leftSideAllChats[index].timestamp = obj.timestamp;
      if (index !== 0) {
        this.props.leftSideChatLogsFN(
          leftSideAllChats.sort(function (a, b) {
            return b.timestamp - a.timestamp;
          })
        );
      } else {
        this.props.leftSideChatLogsFN(leftSideAllChats);
      }
    } else {
      let leftSideAllChats = [...this.state.MatchFoundData];
      let index = leftSideAllChats.findIndex((k) => k.recipientId === obj.to);
      if (index > -1) {
        leftSideAllChats[index].payload = obj.payload;
        leftSideAllChats[index].timestamp = obj.timestamp;
        leftSideAllChats[index].isMatchedUser = 1;
        obj["isMatchedUser"] = 1;
        this.setState({ MatchFoundData: leftSideAllChats });
      }
    }
  };

  togglePreviewSendModal = () => {
    this.setState({ previewSendModal: !this.state.previewSendModal });
  };

  // toggle attachment dropdown
  toggleAttachmentDropdown = () => {
    this.setState({ attachmentOpen: !this.state.attachmentOpen });
  };

  // pull reasons from API for reporting user
  getReasonForReport = (data) => {
    this.setState({ reportedUserReason: data });
    this.closeReportModal();
    this.toggleSubDialog();
  };

  // toggles subdialog box in order to cancel or report the user
  toggleSubDialog = () => {
    this.setState({ subDialog: !this.state.subDialog });
  };

  // func to report user
  handlereportuser = (data) => {
    let UserReportPayload = {
      targetUserId: this.props.UserSelectedToChat.recipientId,
      reason: data,
      message: "Report",
    };

    ReportUser(UserReportPayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: `${this.props.UserSelectedToChat.firstName} Reported successfully`,
        });
        this.closeReportModal();
        this.handleClosemenu();
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
        this.closeReportModal();
        this.handleClosemenu();
      });
  };

  // if report user is success, it will close the confirm dialog box and other modal
  closeConfirmationDialogWithMainDialog = () => {
    this.toggleSubDialog();
    this.handlereportuser(this.state.reportedUserReason);
    setTimeout(() => {
      this.closeReportModal();
    }, 500);
  };

  // to get the right side chat logs of selected user
  GetAllChatsForSingleUser = (chatId, timeStamp, pageSize) => {
    this.props.chatSessionStarted();
    if (this.state.prevUserId !== this.state.selectedIdOfUserToMessage) {
      this.setState({ prevUserId: this.state.selectedIdOfUserToMessage });
      GetAllChatsForSingleUser(chatId, timeStamp, pageSize)
        .then((data) => {
          this.props.userSelectedToChat(this.state.updateduserprofile);
          let chatData = this.props.SingleUserAllMessages;
          let ChatLogs = [...this.props.leftSideChatLogs];
          let index = ChatLogs.findIndex((k) => k.chatId === chatId);
          // console.log("[before] 1", ChatLogs[index]);
          if (
            ChatLogs[index] &&
            ChatLogs[index]["checked"] === false &&
            ChatLogs[index].totalUnread !== 0 &&
            "totalUnread" in ChatLogs[index]
          ) {
            ChatLogs[index]["checked"] = true;
            this.props.updateUnreadChatCountFunc(this.props.unreadChatCount - 1);
            ChatLogs[index]["totalUnread"] = 0;
            // console.log("[before] 2", ChatLogs[index]);
            this.props.userSelectedToChat(ChatLogs[index]);
          }
          this.props.leftSideChatLogsFN(ChatLogs);
          let appendedChatData = chatData.concat(data.data.data);
          this.acknowledgementAllToBlueTick(appendedChatData, index);
        })
        .catch((err) => {
          this.props.chatSessionStarted();
          let emptyData = [];
          this.props.allMessagesForSelectedUser(emptyData);
        });
    }
  };

  acknowledgementAllToBlueTick = (appendedChatData, index) => {
    let arr = appendedChatData.filter((k) => k.status !== 3 && k.senderId !== this.props.UserProfile.userId);
    let ackArr = [...arr];
    for (let i = 0; i < ackArr.length; i++) {
      sendAcknowledgementsOverMqtt({ obj: ackArr[i], newStatus: "3" });
    }
    let ChatLogs = [...this.props.leftSideChatLogs];
    ChatLogs[index]["totalUnread"] = 0;
    this.props.leftSideChatLogsFN(ChatLogs);
    this.props.allMessagesForSelectedUser(appendedChatData);
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.isNewUserChatFocused !== this.state.isNewUserChatFocused || this.state.chatDeleteSuccessfull) {
      GetAllChats(this.state.pageNo).then((res) => {
        this.props.leftSideChatLogsFN([
          ...res.data.data.sort(function (a, b) {
            return b.timestamp - a.timestamp;
          }),
        ]);
      });
    }

    if (this.state.selectedIdOfUserToMessage !== prevState.selectedIdOfUserToMessage) {
      console.log("didUpdate", this.state.selectedIdOfUserToMessage, prevState.selectedIdOfUserToMessage);
      // unSbscribeToUserTopic(prevState.selectedIdOfUserToMessage);
      this.setState({ prevUserId: prevState.selectedIdOfUserToMessage });
      this.props.allMessagesForSelectedUser([]);
    }
  }

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  // gets text value from ref function
  getValueFromChild = (e) => {
    console.log("getValueFromChild", e);
    if (e === undefined || e === "") {
      return;
    } else {
      this.setState({ textValue: e });
    }
  };

  handleClickmenu = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // To Close Menu
  handleClosemenu = () => {
    this.setState({ anchorEl: null });
  };

  handleClickAttachmentMenu = (event) => {
    this.setState({ attachmentAnchor: event.currentTarget });
  };

  handleCloseAttachmentMenu = () => {
    this.setState({ attachmentAnchor: null });
  };

  // Used to Open Report User Dialog
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  // Used to Close Report User Dialog
  handleClosedialog = () => {
    this.setState({ open: false });
  };

  // adds a new message to user selected to chat
  addNewMessage = (obj) => {
    let { SingleUserAllMessages, addNewChatToExistingObject } = this.props;
    if (SingleUserAllMessages && SingleUserAllMessages.length >= 0) {
      addNewChatToExistingObject(obj);
      this.updateLeftSideChat(obj);
    } else {
      addNewChatToExistingObject(obj);
      this.updateLeftSideChat(obj);
    }
  };

  purchaseCoinModal = () => {
    this.setState({ purcahseCoin: !this.state.purcahseCoin });
  };

  // deletes user message logs from active chat and and removes from list
  deleteAllMessages = (msgIds) => {
    DeleteSelectedMessages(msgIds)
      .then((res) => {
        console.log("successfully deleted messages", res);
      })
      .catch((err) => {
        console.log("failed deleted messages", err);
      });
  };

  componentWillUnmount() {
    let emptyData = [];
    // this.props.readOnlyUserRemove();
    this.props.chatSessionEnded();
    this.props.allMessagesForSelectedUser(emptyData);
    this.props.userSelectedToChat("");
    window.onscroll = null;
    if (this.props.UserSelectedToChat) {
      unSbscribeToUserTopic(this.props.UserSelectedToChat.recipientId);
      // unsubscribeToUser(this.props.UserSelectedToChat.recipientId);
    }
    document.removeEventListener("mousedown", this.handleDropDown, true);
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handlemessage = (value, data) => {
    console.log("[HANDLE MESSAGE]", value);
    if (this.props.UserSelectedToChat._id !== data._id || this.props.UserSelectedToChat._id !== data.recipientId) {
      this.setState({ isNewUserChatFocused: false });
    }
    // console.log("handleMessage", data);
    let id = data._id || data.recipientId || data.receiverId || data.opponentId || data.receiverIdentifier;

    this.setState({ selectedIdOfUserToMessage: value || id, updateduserprofile: data });
    // this.props.userSelectedToChat(data);

    let TS = this.props.getLatestTimeStamp > data.timestamp ? this.props.getLatestTimeStamp + 4000 : data.timestamp + 10;
    this.setState({ updateduserprofile: data }, () => {
      this.GetAllChatsForSingleUser(data.chatId, TS + 4000, 20);
    });
    // console.log('handleMessage', value)
    subscribeToUserTopic(id || value);
    this.openmessagedrawerDrawer();
  };

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  openmessagedrawerDrawer = () => {
    this.setState({ messagedrawer: true });
  };

  closmessagedrawerDrawer = () => {
    this.setState({ messagedrawer: false });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleCloseSnacbkbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // blocks user function
  BlockUserFn = (targetUserId) => {
    BlockUser(targetUserId)
      .then((res) => {
        this.setState({ loader: false, textToRender: "" });
        console.log("blocked user success", res);
      })
      .catch((err) => {
        console.log("blocked user err", err);
      });
  };

  // generates substring for a image url, which we got from datum server.
  generateSubstring = (url) => {
    let res = url.substr(23, 41);
    return res;
  };

  // renders text based on blocked user by self or others
  blockRender = (obj, name) => {
    let text = "";
    if (obj.isBlockedByMe === 1) {
      text = "You have blocked " + name;
    } else {
      text = name;
    }
    if (obj.isBlocked === 1) {
      text = name + " has blocked you.";
    } else {
      text = name;
    }
    return text;
  };

  // unmounts the dropdown
  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  // mounts the dropdown
  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  blockUserOption = () => {
    this.setState({
      loader: true,
      textToRender: `Blocking ${this.props.UserSelectedToChat.firstName}`,
    });
    this.handleClosemenu();
    let leftSideData = [...this.props.leftSideChatLogs];
    let blockedUserChatId = this.props.UserSelectedToChat.chatId;
    let index = leftSideData.findIndex((k) => k.chatId === blockedUserChatId);
    if (index > -1) {
      leftSideData[index].isBlockedByMe = 1;
      this.BlockUserFn(this.props.UserSelectedToChat.recipientId);
      this.props.leftSideChatLogsFN(leftSideData);
    } else {
      let leftSideData = [...this.state.MatchFoundData];
      let blockedUserChatId = this.props.UserSelectedToChat.chatId;
      let index = leftSideData.findIndex((k) => k.chatId === blockedUserChatId);
      if (index > -1) {
        leftSideData[index].isBlockedByMe = 1;
        this.BlockUserFn(this.props.UserSelectedToChat.recipientId);
        this.setState({ MatchFoundData: leftSideData });
      }
    }
  };

  // api call to unblock user
  unblockuser = (token, targetUserId) => {
    this.setState({
      loader: true,
      textToRender: `Unblocking ${this.props.UserSelectedToChat.firstName}`,
    });
    let leftSideData = [...this.props.leftSideChatLogs];
    let blockedUserChatId = this.props.UserSelectedToChat.chatId;
    let index = leftSideData.findIndex((k) => k.chatId === blockedUserChatId);
    if (index > -1) {
      leftSideData[index].isBlockedByMe = 0;
      unblockuser(token, targetUserId).then((res) => {
        this.setState({ loader: false, textToRender: "", anchorEl: null });
      });
      this.props.leftSideChatLogsFN(leftSideData);
    } else {
      let leftSideData = [...this.state.MatchFoundData];
      let blockedUserChatId = this.props.UserSelectedToChat.chatId;
      let index = leftSideData.findIndex((k) => k.chatId === blockedUserChatId);
      if (index > -1) {
        leftSideData[index].isBlockedByMe = 0;
        unblockuser(token, targetUserId).then((res) => {
          this.setState({ loader: false, textToRender: "", anchorEl: null, MatchFoundData: leftSideData });
        });
      }
    }
  };

  // delete chats and removes the active state of chat,
  DeleteChatTotally = () => {
    deletechatTotally(this.props.UserSelectedToChat.chatId)
      .then((res) => {
        this.setState({ chatDeleteSuccessfull: true });
        console.log("deleted messages successfully");
        this.props.userSelectedToChat("");
        this.props.chatSessionEnded();
        this.props.deleteAllMessagesForSingleUserFunc(this.props.UserSelectedToChat.chatId);
        this.handleClosemenu();
        setTimeout(() => {
          this.setState({ chatDeleteSuccessfull: false });
        }, 1000);
      })
      .catch((err) => console.log("err", err));
  };

  // function to unmatch opponent
  unmatchUser = () => {
    this.setState({ loader: true, textToRender: `Unmatching ${this.props.UserSelectedToChat.firstName}`, anchorEl: null });
    unmatchuser(this.props.UserSelectedToChat.receiverId)
      .then((res) => {
        this.setState({ loader: false, textToRender: "", anchorEl: null });
        this.DeleteChatTotally();
      })
      .catch((err) => {
        console.log("failed unmatch ", err);
      });
    this.handleClosemenu();
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  isUserMatchedOrFriend = () => {
    let { isFriend, isMatchedUser, isMatched } = this.props.UserSelectedToChat;
    if (isMatchedUser == 1 || isMatched == 1 || isFriend == 1) {
      return 1;
    }
    return 0;
  };

  uploadImage = async (files) => {
    this.setState({ case: 3, newFiles: files });
    let size = "";
    if (this.props.chatStarted === true) {
      this.setState({ openAttachent: true });
      let uid = getCookie("uid");
      let typeOfData = files.type;
      let objToSendIfOnline = "";
      size = files.size;
      this.setState({ loadingImageSkeleton: true });
      let formData = new FormData();
      formData.append("photo", files, new Date().getTime().toString());
      let responseFileUrl = await uploadMediaOverLocalServer(formData);

      let url = responseFileUrl.data.url.substr(23, responseFileUrl.data.url.length - 1);

      let url2 = "https://fetch.unfiltered.love/" + url;
      console.log("responseFileUrl", url2);
      objToSendIfOnline = {
        chatId: this.props.UserSelectedToChat.chatId,
        from: uid,
        id: new Date().getTime().toString(),
        isMatchedUser: this.isUserMatchedOrFriend(),
        name: this.props.UserProfile.UserProfileName,
        payload: btoa(url2),
        receiverIdentifier: uid,
        timestamp: new Date().getTime(),
        to:
          this.props.UserSelectedToChat.recipientId ||
          this.props.UserSelectedToChat._id ||
          this.props.UserSelectedToChat.opponentId ||
          this.props.UserSelectedToChat.from,
        toDocId: "",
        type: typeOfData.includes("mp4") ? "2" : "1",
        dataSize: size,
        thumbnail: demoThumbNail,
        userImage: this.props.UserSelectedToChat.profilePic || this.props.UserSelectedToChat.userImage,
      };

      if (
        (parseInt(this.props.CoinBalance) === 0 &&
          "chatInitiatedBy" in this.props.UserSelectedToChat &&
          this.props.UserSelectedToChat.chatInitiatedBy !== getCookie("uid")) ||
        (parseInt(this.props.CoinBalance) === 0 && this.props.UserSelectedToChat.initiated === false)
      ) {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
      } else if (
        "chatInitiatedBy" in this.props.UserSelectedToChat &&
        (this.props.UserSelectedToChat.isMatched || this.props.UserSelectedToChat.isMatchedUser || this.props.UserSelectedToChat.isFriend)
      ) {
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
      } else if (parseInt(this.props.CoinBalance) > 0) {
        if (
          ((this.props.UserSelectedToChat.isMatched === 0 ||
            this.props.UserSelectedToChat.isMatchedUser === 0 ||
            this.props.UserSelectedToChat.isFriend === 0) &&
            !("chatInitiatedBy" in this.props.UserSelectedToChat)) ||
          !("initiated" in this.props.UserSelectedToChat)
        ) {
          if (this.state.coinPopUpSuccessfull === false) {
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.state.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.CoinChat(objToSendIfOnline);
            this.addNewMessage(objToSendIfOnline);
          }
        } else if ("initiated" in this.props.UserSelectedToChat && this.props.UserSelectedToChat.initiated === true) {
          if (this.state.coinPopUpSuccessfull === false) {
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.state.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.CoinChat(objToSendIfOnline);
            this.addNewMessage(objToSendIfOnline);
          }
        } else {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.addNewMessage(objToSendIfOnline);
          sendMessageOverMqtt(objToSendIfOnline);
        }
      } else if (
        parseInt(this.props.CoinBalance) === 0 &&
        (!("chatInitiatedBy" in this.props.UserSelectedToChat) || "chatInitiatedBy" in this.props.UserSelectedToChat)
      ) {
        this.purchaseCoinModal();
      }
    }
  };

  setLocationForSendingMessage = (lat, lng, city, locationSelected) => {
    this.setState({ lat: lat, lng: lng, city: city, locationSelected: locationSelected });
  };

  createLocationToSend = (lat, lng, placeName, completeAddress) => {
    return `(${lat},${lng})@@${placeName}@@${completeAddress}`;
  };

  // sends location to target user. (chat)
  getlocation = () => {
    this.setState({ case: 4 });
    let uid = getCookie("uid");
    let buff = new Buffer(this.createLocationToSend(this.state.lat, this.state.lng, this.state.placeName, this.state.onClickedAddress));
    let base64data = buff.toString("base64");
    let objToSendIfOnline = {
      chatId: this.props.UserSelectedToChat.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props.UserProfile.UserProfileName,
      payload: base64data,
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to:
        this.props.UserSelectedToChat.recipientId ||
        this.props.UserSelectedToChat._id ||
        this.props.UserSelectedToChat.opponentId ||
        this.props.UserSelectedToChat.from,
      toDocId: "",
      type: "3",
      status: 1,
      userImage: this.props.UserSelectedToChat.profilePic || this.props.UserSelectedToChat.userImage,
    };
    // console.log("sending location", objToSendIfOnline);
    if (this.props.chatStarted) {
      if (
        (parseInt(this.props.CoinBalance) === 0 &&
          "chatInitiatedBy" in this.props.UserSelectedToChat &&
          this.props.UserSelectedToChat.chatInitiatedBy !== getCookie("uid")) ||
        (parseInt(this.props.CoinBalance) === 0 && this.props.UserSelectedToChat.initiated === false)
      ) {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.closelocationmodel();
      } else if (
        "chatInitiatedBy" in this.props.UserSelectedToChat &&
        (this.props.UserSelectedToChat.isMatched || this.props.UserSelectedToChat.isMatchedUser || this.props.UserSelectedToChat.isFriend)
      ) {
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.closelocationmodel();
      } else if (parseInt(this.props.CoinBalance) >= 0) {
        if (
          ((this.props.UserSelectedToChat.isMatched === 0 ||
            this.props.UserSelectedToChat.isMatchedUser === 0 ||
            this.props.UserSelectedToChat.isFriend === 0) &&
            !("chatInitiatedBy" in this.props.UserSelectedToChat)) ||
          !("initiated" in this.props.UserSelectedToChat)
        ) {
          if (this.state.coinPopUpSuccessfull === false) {
            this.closelocationmodel();
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.state.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.CoinChat(objToSendIfOnline);
            this.addNewMessage(objToSendIfOnline);
            this.closelocationmodel();
          }
        } else if ("initiated" in this.props.UserSelectedToChat && this.props.UserSelectedToChat.initiated === true) {
          if (this.state.coinPopUpSuccessfull === false) {
            this.closelocationmodel();
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.state.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.CoinChat(objToSendIfOnline);
            this.addNewMessage(objToSendIfOnline);
            this.closelocationmodel();
          }
        } else {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.addNewMessage(objToSendIfOnline);
          sendMessageOverMqtt(objToSendIfOnline);
          this.closelocationmodel();
        }
      }
    }
  };

  // toggles date modal
  openDateModal = () => {
    this.setState({ dateModal: !this.state.dateModal });
  };

  // function to filter users from existing user
  filterList = (event) => {
    event.preventDefault();
    var updatedList = this.state.usersFiltered;
    updatedList = updatedList.filter((item) => item.firstName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
    this.props.leftSideChatLogsFN(updatedList);
    // this.setState({ usersFiltered: updatedList });
  };

  navigateToUserProfile = (name, id) => {
    this.props.justViewProfileActive();
    this.props.history.push(`/app/user/${name}/${id}`);
  };

  textdecode = (str) => {
    try {
      return decodeURIComponent(escape(atob(str)));
    } catch (e) {}
  };

  // on left side of messages, the function will render if the message is normal message
  // or image/gif/sticker
  messageFilterRenderer = (obj) => {
    if (obj.messageType === "3" || obj.type === "3") {
      return "Sent Location";
    } else if (obj.messageType === "2" || obj.type === "2") {
      return "Sent Video";
    } else if (obj.messageType === "1" || obj.type === "1") {
      return "Sent Photo";
    } else if (obj.messageType === "0" || obj.type === "0") {
      if (obj.payload === "3embed test") {
        return "";
      } else {
        return atob(obj.payload);
      }
    } else if (obj.messageType === "8" || obj.type === "8") {
      return "Sent GIF";
    } else if (obj.messageType === "15" || obj.type === "15") {
      return "Credits Transfer";
    }
  };

  handleChangeIndex = (index) => {
    this.setState({ valueOfTabs: index });
  };

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
      this.togglePreviewSendModal();
    }
  }

  handleDropDown = (e) => {
    if (this.state.previewSendModal) {
      if (this.node.contains(e.target)) {
        return;
      }
      this.togglePreviewSendModal();
    }
  };

  caseHandler = (data) => {
    this.setState({ case: data });
  };

  // type 1 -> handler with ref
  // type 2 -> handler with child ref
  acceptButtonClick = (fun1, fun2, type) => {
    let { CoinBalance } = this.props;

    // if (this.props.chatStarted) {
    //   if (
    //     (CoinBalance > 0 && !this.props.UserSelectedToChat.initiated) ||
    //     (CoinBalance >= 0 && this.props.UserSelectedToChat.isMatchedUser) ||
    //     (CoinBalance >= 0 && this.props.UserSelectedToChat.isMatched) ||
    //     (CoinBalance > 24 && this.props.UserSelectedToChat.initiated) ||
    //     (CoinBalance > 24 && !this.props.UserSelectedToChat.initiated)
    //   ) {
    //     fun1();
    //     if (type === 1) {
    //       fun2.click();
    //     } else if (type === 2) {
    //       fun2();
    //     }
    //   } else {
    //     console.log("acceptButtonClick else");
    //     this.purchaseCoinModal();
    //   }
    // }
    if (this.props.chatStarted) {
      fun1();
      if (type === 1) {
        fun2.click();
      } else if (type === 2) {
        fun2();
      }
    }
  };

  classRenderForAttachment = () => {
    if (this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1) {
      return "c-button-disabled";
    } else if (
      (this.props.chatStarted && this.props.CoinBalance >= 0) ||
      this.props.UserSelectedToChat.initiated ||
      this.props.UserSelectedToChat.isMatched ||
      this.props.UserSelectedToChat.isMatchedUser
    ) {
      return "send_location";
    } else {
      return "send_location_disabled";
    }
  };

  render() {
    const { classes } = this.props;
    const { width, anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const isMobile = width <= 576;
    // console.log("this.props.UserProfile", this.props.UserProfile);
    const UserMessages = !this.state.chatsLoaded ? (
      [...new Array(5)].map((k, i) => (
        <div className="py-2 col-lg-12 simple_mess" key={i}>
          <Media className="media_main">
            <Media left className="ChatPage_LeftSide_User_Image">
              <Skeleton circle={true} height={60} width={60} />
            </Media>
            <Media body className="col-lg-12 col-xl-12 user_media">
              <Media>
                <h6 className="m-0">
                  <Skeleton width={120} />
                </h6>
              </Media>
              <p className="p-0">
                <Skeleton width={200} />
              </p>
              <span className="total_messages"></span>
            </Media>
          </Media>
        </div>
      ))
    ) : this.props.leftSideChatLogs && this.props.leftSideChatLogs.length > 0 ? (
      this.props.leftSideChatLogs
        .map((usermessages, index) => (
          <div
            className={
              (usermessages.recipientId === this.state.selectedIdOfUserToMessage ||
                usermessages._id === this.state.selectedIdOfUserToMessage ||
                usermessages.recipientId === this.state.selectedIdOfUserToMessage) &&
              this.props.chatStarted
                ? "py-2 col-lg-12 simple_mess UserMessageactive"
                : "py-2 col-lg-12 simple_mess"
            }
            key={index}
            onClick={() => this.handlemessage(usermessages.recipientId || usermessages._id || usermessages.receiverId, usermessages)}
          >
            <Media className="media_main">
              <Media left className="ChatPage_LeftSide_User_Image">
                <Media object src={usermessages.profilePic || usermessages.userImage} onError={this.onError} alt={usermessages.firstName} />
              </Media>
              <div className={usermessages.onlineStatus === 1 ? "onlineDot" : ""}></div>
              <div className="matchedUserIcon">
                {usermessages.isMatchedUser === 1 ? <img src={Icons.friendsChat} alt="friends-chat" className="d_matched_heart" /> : ""}
              </div>

              {usermessages.isFriend ? (
                <div className="friendUserIcon">
                  <img src={Icons.twoFriends} alt="two-users" />
                </div>
              ) : (
                ""
              )}
              {(usermessages.isMatchedUser === 0 && usermessages.isFriend === 0) ||
              (usermessages.isMatched === 0 && usermessages.isFriend === 0) ? (
                <img src={Icons.goldCoin} alt="gold-coin" className="d_notMatched_coin" />
              ) : (
                ""
              )}

              <Media body className="col-lg-12 col-xl-12 user_media">
                <div className="row">
                  <div className="col-9">
                    <Media>
                      <div className={`m-0 ${classes.firstName}`}>{usermessages.firstName || usermessages.name}</div>
                    </Media>
                  </div>
                  <div className="col-3">
                    {usermessages.totalUnread > 0 ? (
                      <span class="badge badge-pill badge-danger">{usermessages.totalUnread}</span>
                    ) : (
                      <span />
                    )}
                  </div>
                </div>
                <div className="col-9 px-0">
                  <p className={`m-0 ${classes.payload}`}>{this.messageFilterRenderer(usermessages)}</p>
                </div>

                {/* <p className="m-0">{usermessages.payload ? (usermessages.payload === "3embed test" ? "" : atob(usermessages.payload)) : ""}</p> */}
                <span className="total_messages"></span>
              </Media>
            </Media>
          </div>
        ))
        .sort(function (a, b) {
          return a.timestamp - b.timestamp;
        })
    ) : this.props.chatStarted ? (
      <></>
    ) : (
      <div className="col-12 h-75 d-flex justify-content-center align-items-center">
        <div className="row">
          <div className="col-12 text-center">
            <img src={Icons.UpdateNoChats} className="no__chats" alt="No Chats" />
          </div>
          <div className="col-12 text-center noDataFountText">You haven't Chatted with anyone.</div>
        </div>
      </div>
    );

    // Mobile View Module
    if (isMobile) {
      return (
        <div>
          {this.state.loader ? (
            <div className="text-center">
              <CircularProgress className={classes.progress} /> <span>{this.state.textToRender}</span>
            </div>
          ) : (
            <div>
              <MainDrawer
                width={400}
                onClose={this.closeprofiledrawerDrawer}
                onOpen={this.openprofiledrawerDrawer}
                open={this.state.messagedrawer}
              >
                {/* {ProfileDrawer} */}
              </MainDrawer>

              {/* Header Module */}
              <WebHeader headername="Messages" boost="bOost" />

              {/* Main Messages Module */}
              <div className="col-lg-12">
                <div className="row">
                  {/* Total People Module */}
                  <div className="p-0 col-lg-4 user_messages">{UserMessages}</div>
                </div>
              </div>
            </div>
          )}
        </div>
      );
    }

    // Desktop View Module
    else {
      return (
        <div id="innerChatContent">
          <input
            style={{ width: "15px", display: "none" }}
            type="file"
            id="add_image"
            accept="image/png, image/jpeg, video/mp4"
            ref={(fileInputEl) => (this.fileInputEl = fileInputEl)}
            onChange={(event) => this.uploadImage(event.target.files[0])}
          />
          <div className="pb-3 pl-3">
            <h3 className="headerName">
              <FormattedMessage id="UnFiltered Chat" />
            </h3>
            <div className="subHeaderName">
              <h5 className="mb-0">Let's Get To Know Each Other</h5>
            </div>
          </div>

          {/* Main Messages Module */}
          <div className="w-100 updated__chat__view">
            {this.state.loader ? (
              <div className="chat_screen_progressBar">
                <CircularProgress />
                <span className="pl-2">{this.state.textToRender}</span>
              </div>
            ) : (
              ""
            )}
            <div className="row mx-0">
              {/* Total People Module */}
              {/* <div className="border-left border-top p-0 col-sm-4 col-md-4 col-lg-4 col-xl-4 user_messages"> */}
              <div className="user_messages bg-white">
                <div className="row mx-0 basic__user__info pt-3 px-3 align-items-center">
                  <div>
                    <img
                      src={this.props.UserProfile && this.props.UserProfile.UserProfilePic}
                      alt={this.props.UserProfile && this.props.UserProfile.UserProfileName}
                      height={60}
                      width={60}
                    />
                  </div>
                  <div>{this.props.UserProfile && this.props.UserProfile.UserProfileName}</div>
                </div>
                {this.state.MatchFoundData && this.state.MatchFoundData.length > 0 ? (
                  <>
                    <div className="col-12 d_newMatchesText pt-2">New Matches</div>
                    <div className="row mx-0">
                      {this.state.MatchFoundData.slice(0, 3).map((k, i) => (
                        <div className="pl-3 pt-1" onClick={() => this.handlemessage(k.recipientId, k)}>
                          <div key={i} className="w_matchFoundData">
                            {k.isFriend ? (
                              <div className="w_common_friends">
                                <img src={Icons.twoFriends} alt="two-users" />
                              </div>
                            ) : (
                              <></>
                            )}
                            <div className="w_common_matched">
                              {k.isMatchedUser === 1 || k.isMatched === 1 ? (
                                <img src={Icons.friendsChat} alt="friends-chat" className="d_matched_heart" />
                              ) : (
                                <span />
                              )}
                            </div>
                            <img onError={this.onError} src={k.profilePic} alt={k.firstName} />
                          </div>
                          <div className="w_matchFoundData_WName">{k.firstName}</div>
                        </div>
                      ))}
                      <div className="pl-3 pt-1">
                        <div className="d_matched_viewMore">
                          <img src={Icons.MatchedPlus} alt="more" />
                        </div>
                        <div className="w_matchFoundData_WName">View More</div>
                      </div>
                    </div>
                  </>
                ) : (
                  <></>
                )}

                <form className="search_user bg-white">
                  <div class="form-group p-3 mb-0 border-bottom ChatPage_FilterUser_Search">
                    <div className="magnifyingGlass">
                      <img src={Images.search} alt="magnifying-glass" />
                    </div>
                    <input
                      onChange={(e) => this.filterList(e)}
                      type="text"
                      className="form-control searchText"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Search"
                    />
                  </div>
                </form>
                <div className="user_messages_list">{UserMessages}</div>
              </div>

              {/* Messages Module */}
              <div className="rightSideChatMessages">
                <header className="col-12">
                  <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 border-bottom">
                      <div className="m-0 row chat_message_section_header">
                        <div className="col-6 text-left d-flex align-items-center">
                          {this.props.chatStarted === true ? (
                            <Media className="pt-1 pb-1">
                              <Media
                                left
                                className="ChatPage_RightSide_Selected_User"
                                onClick={() =>
                                  this.navigateToUserProfile(
                                    this.props.UserSelectedToChat.firstName || this.props.UserSelectedToChat.name,
                                    this.props.UserSelectedToChat._id || this.state.selectedIdOfUserToMessage
                                  )
                                }
                              >
                                <Media
                                  object
                                  onError={this.onError}
                                  src={this.state.updateduserprofile.profilePic || this.props.UserSelectedToChat.userImage}
                                  alt="User-Image"
                                />
                              </Media>

                              <Media body className="col-lg-12 pt-2 user_media">
                                <div className={`m-0 ${classes.selectedUserToChat}`}>
                                  {this.props.UserSelectedToChat.isBlockedByMe
                                    ? "You have blocked " + this.state.updateduserprofile.firstName
                                    : this.state.updateduserprofile.firstName || this.props.UserSelectedToChat.name}
                                </div>
                                <div className={`m-0 ${classes.onlineOffline}`}>
                                  {this.props.UserSelectedToChat.onlineStatus === 1 ? "Online" : "Offline"}
                                </div>
                              </Media>
                            </Media>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="col-6 text-right">
                          <div className="h-100 user_chatsetting d-flex justify-content-end align-items-center">
                            {(this.props.UserSelectedToChat.isMatchedUser || this.props.UserSelectedToChat.isMatched) &&
                            this.props.UserSelectedToChat.isFriend ? (
                              <div className="video-camera">
                                <img
                                  style={{ cursor: "pointer" }}
                                  src={Icons.videoCameraEnabled}
                                  alt="video-camera"
                                  width={18}
                                  height={18}
                                  onClick={() => {
                                    this.props.__callInit(true);
                                  }}
                                />
                              </div>
                            ) : this.props.UserSelectedToChat.isMatchedUser ? (
                              <CustomButton
                                disabled={
                                  this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1
                                }
                                className={
                                  this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1
                                    ? "c-button-disabled"
                                    : "c-button"
                                }
                                handler={this.openDateModal}
                                text={
                                  <div className="fixDate">
                                    <div>
                                      <img alt="date" src={Icons.fixDate} width={30} height={30} />
                                    </div>
                                    <div>
                                      <div>FIX A DATE</div>
                                    </div>
                                  </div>
                                }
                              />
                            ) : this.props.UserSelectedToChat.isFriend ? (
                              <CustomButton
                                handler={() => this.props.__callInit(true)}
                                className={"c-button"}
                                disabled={
                                  this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1
                                }
                                text={
                                  <div className="video-camera">
                                    <img
                                      style={{
                                        cursor:
                                          this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1
                                            ? "not-allowed"
                                            : "pointer",
                                      }}
                                      src={Icons.videoCameraEnabled}
                                      alt="video-camera"
                                      width={18}
                                      height={18}
                                    />
                                  </div>
                                }
                              />
                            ) : (
                              ""
                            )}
                            <CustomButton
                              className={this.classRenderForAttachment()}
                              handler={this.props.chatStarted ? this.toggleAttachmentDropdown : ""}
                              text={
                                <>
                                  <IconButton
                                    className="px-0 attachmentButton"
                                    aria-label="More"
                                    aria-owns={Boolean(this.state.attachmentOpen) ? "long-menu" : undefined}
                                    aria-haspopup="true"
                                    disabled={
                                      this.props.UserSelectedToChat.isBlocked === 1 || this.props.UserSelectedToChat.isBlockedByMe === 1
                                    }
                                    onClick={this.props.chatStarted ? this.togglePreviewSendModal : ""}
                                  >
                                    <img
                                      src={
                                        (this.props.chatStarted && this.props.CoinBalance >= 0) ||
                                        this.props.UserSelectedToChat.initiated ||
                                        this.props.UserSelectedToChat.isMatched ||
                                        this.props.UserSelectedToChat.isMatchedUser
                                          ? Icons.enabledAttachment
                                          : Icons.disabledAttachment
                                      }
                                      height={35}
                                      width={35}
                                      alt="attachment"
                                    />
                                  </IconButton>
                                  <div ref={(node) => (this.node = node)}>
                                    <Fade top cascade when={this.state.previewSendModal} duration={200}>
                                      {this.state.previewSendModal ? (
                                        <div className="c_dropdown">
                                          <div
                                            className="c_dropdown_location"
                                            onClick={() => this.acceptButtonClick(this.togglelocationmodel, this.togglePreviewSendModal, 2)}
                                          >
                                            <img src={Icons.SendLocation} alt="send-location" height={40} width={40} />
                                          </div>
                                          <div onClick={() => this.acceptButtonClick(this.togglePreviewSendModal, this.fileInputEl, 1)}>
                                            <img src={Icons.SendAttachment} alt="send-attachment" height={40} width={40} />
                                          </div>
                                          <div
                                            onClick={() =>
                                              this.acceptButtonClick(this.togglePreviewSendModal, this.childref.toggleStickerModal, 2)
                                            }
                                          >
                                            <img src={Icons.SendSticker} alt="send-sticker" height={40} width={40} />
                                          </div>
                                          <div
                                            onClick={() =>
                                              this.acceptButtonClick(this.togglePreviewSendModal, this.childref.toggleTransferCoinModal, 2)
                                            }
                                          >
                                            <img src={Icons.transferCoins} alt="send-coins" height={40} width={40} />
                                          </div>
                                        </div>
                                      ) : (
                                        <span />
                                      )}
                                    </Fade>
                                  </div>
                                </>
                              }
                            />

                            <CustomButton
                              disabled={this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe}
                              className={
                                (this.props.chatStarted && this.props.CoinBalance >= 0) ||
                                this.props.UserSelectedToChat.isMatched ||
                                this.props.UserSelectedToChat.isMatchedUser
                                  ? "more_options"
                                  : "more_options_disabled"
                              }
                              text={
                                <>
                                  <IconButton
                                    aria-label="More"
                                    aria-owns={open ? "long-menu" : undefined}
                                    aria-haspopup="true"
                                    onClick={
                                      (this.props.chatStarted && this.props.CoinBalance >= 0) ||
                                      this.props.UserSelectedToChat.isMatched ||
                                      this.props.UserSelectedToChat.isMatchedUser
                                        ? this.handleClickmenu
                                        : ""
                                    }
                                    style={{ color: "#f74167" }}
                                  >
                                    <img
                                      src={
                                        (this.props.chatStarted && this.props.CoinBalance >= 0) ||
                                        this.props.UserSelectedToChat.isMatched ||
                                        this.props.UserSelectedToChat.isMatchedUser
                                          ? Icons.enabledThreeDots
                                          : Icons.disabledThreeDots
                                      }
                                      height={35}
                                      width={35}
                                      alt="attachment"
                                    />
                                  </IconButton>
                                  <Menu
                                    id="long-menu"
                                    anchorEl={anchorEl}
                                    open={open}
                                    onClose={this.handleClosemenu}
                                    PaperProps={{
                                      style: {
                                        maxHeight: 40 * 5,
                                        width: 200,
                                      },
                                    }}
                                  >
                                    <MenuItem onClick={this.DeleteChatTotally}>
                                      <p className="menuItem">
                                        <FormattedMessage id="message.deleteChat" />
                                      </p>
                                    </MenuItem>

                                    {(this.props.UserSelectedToChat.isMatchedUser === 1 || this.props.UserSelectedToChat.isMatched === 1) &&
                                    this.props.UserSelectedToChat.isFriend === 1 ? (
                                      <MenuItem
                                        classes={classes.root}
                                        onClick={() => this.unfriendUser(this.props.UserSelectedToChat.recipientId)}
                                      >
                                        <p className="menuItem">
                                          <FormattedMessage id="message.unfriend" /> {this.props.UserSelectedToChat.firstName}
                                        </p>
                                      </MenuItem>
                                    ) : this.props.UserSelectedToChat.isMatchedUser === 1 ? (
                                      <MenuItem onClick={this.unmatchUser}>
                                        <p className="menuItem">
                                          <FormattedMessage id="message.unMatch" /> {this.props.UserSelectedToChat.firstName}
                                        </p>
                                      </MenuItem>
                                    ) : this.props.UserSelectedToChat.isFriend === 1 ? (
                                      <MenuItem
                                        classes={classes.root}
                                        onClick={() => this.unfriendUser(this.props.UserSelectedToChat.recipientId)}
                                      >
                                        <p className="menuItem">
                                          <FormattedMessage id="message.unfriend" /> {this.props.UserSelectedToChat.firstName}
                                        </p>
                                      </MenuItem>
                                    ) : (
                                      ""
                                    )}

                                    {this.props.UserSelectedToChat.isBlockedByMe ? (
                                      <MenuItem
                                        classes={classes.root}
                                        onClick={() => this.unblockuser(getCookie("token"), this.props.UserSelectedToChat.recipientId)}
                                      >
                                        <p className="menuItem">Unblock {this.props.UserSelectedToChat.firstName}</p>
                                      </MenuItem>
                                    ) : (
                                      <MenuItem classes={classes.root} onClick={this.blockUserOption}>
                                        <p className="menuItem">
                                          <FormattedMessage id="message.block" /> {this.props.UserSelectedToChat.firstName}
                                        </p>
                                      </MenuItem>
                                    )}

                                    <MenuItem onClick={this.openReportModal}>
                                      <p className="menuItem">
                                        <FormattedMessage id="message.report" /> {this.props.UserSelectedToChat.firstName}
                                      </p>
                                    </MenuItem>
                                  </Menu>
                                </>
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </header>
                <div className="chat_message_section">
                  <div className="chat_messages">
                    <Chatroom
                      reff={(rf) => {
                        this.childref = rf;
                      }}
                      coinReduce={this.props.coinReduce}
                      CoinChat={this.CoinChat}
                      case={this.state.case}
                      caseHandler={this.caseHandler}
                      UserSelectedToChat={this.props.UserSelectedToChat}
                      sendersName={this.props.UserProfile.UserProfileName}
                      myPic={this.props.UserProfile.UserProfilePic}
                      coins={this.props.CoinBalance}
                      allUserMessages={this.props.SingleUserAllMessages}
                      addNewMessage={this.addNewMessage}
                      chatStarted={this.props.chatStarted}
                      serverImageFolderLocation={this.props.serverImageFolderLocation}
                      openAttachent={this.openAttachent}
                      selectedProfile={this.props.selectedProfile}
                      messageAcknowledgementId={this.props.messageAcknowledgementId}
                      closelocationmodel={this.closelocationmodel}
                      locationmodel={this.state.locationmodel}
                      getlocation={this.getlocation}
                      loadingImageSkeleton={this.state.loadingImageSkeleton}
                      GetAllChatsForSingleUser={this.GetAllChatsForSingleUser}
                      showCoinAnimation={this.state.showCoinAnimation}
                      coinToastShow={this.props.coinToastShow}
                      leftSideChatLogs={this.props.leftSideChatLogs}
                      coinDialogShow={this.props.coinDialogShow}
                      coinToastDismiss={this.props.coinToastDismiss}
                      coinPopUpSuccessfull={this.state.coinPopUpSuccessfull}
                      setLocationForSendingMessage={this.setLocationForSendingMessage}
                    />
                  </div>
                </div>
              </div>
            </div>
            {/** Physical Date Confirmation Modal */}
            <MatUiModal isOpen={this.state.confirmPhysicalDate} toggle={this.confirmPhysicalDateDateModal} width={500}>
              <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
                <div className="dialogPrev_cancelButton" onClick={this.confirmPhysicalDateDateModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="previewAttachmentText">
                  Are you sure, you want to confirm Physical Date for{" "}
                  {this.props.CoinConfig && this.props.CoinConfig.physicalDate && this.props.CoinConfig.physicalDate.Coin} Credits.
                </div>
                <CustomButton
                  className="coinPopupBtn"
                  text="Confirm"
                  handler={() => {
                    this.createDateFn(
                      this.props.UserSelectedToChat.recipientId,
                      new Date(this.state.time).getTime(),
                      2,
                      this.state.lat,
                      this.state.long,
                      this.state.locationSelected
                    );
                    this.confirmPhysicalDateDateModal();
                  }}
                />
                <CustomButton className="coinPopupBtn" text="Cancel" handler={this.confirmPhysicalDateDateModal} />
              </div>
            </MatUiModal>
            {/** Audio Date Confirmation Modal */}
            <MatUiModal isOpen={this.state.confirmAudioDate} toggle={this.confirmAudioDateModal} width={500}>
              <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
                <div className="dialogPrev_cancelButton" onClick={this.confirmAudioDateModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="previewAttachmentText">Are you sure, you want to confirm Audio Date.</div>
                <CustomButton
                  className="coinPopupBtn"
                  text="Confirm"
                  handler={() => {
                    this.createDateFn(
                      this.props.UserSelectedToChat.recipientId,
                      new Date(this.state.time).getTime(),
                      3,
                      this.state.lat,
                      this.state.long,
                      ""
                    );
                    this.confirmAudioDateModal();
                  }}
                />
                <CustomButton className="coinPopupBtn" text="Cancel" handler={this.confirmAudioDateModal} />
              </div>
            </MatUiModal>
            {/** Video Date Confirmation Modal */}
            <MatUiModal isOpen={this.state.confirmVideoDate} toggle={this.confirmVideoDateModal} width={500}>
              <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
                <div className="dialogPrev_cancelButton" onClick={this.confirmVideoDateModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="previewAttachmentText">
                  Are you sure, you want to confirm Video Date for{" "}
                  {this.props.CoinConfig && this.props.CoinConfig.videoDate && this.props.CoinConfig.videoDate.Coin} Credits.
                </div>
                <CustomButton
                  className="coinPopupBtn"
                  text="Confirm"
                  handler={() => {
                    this.createDateFn(
                      this.props.UserSelectedToChat.recipientId,
                      new Date(this.state.time).getTime(),
                      1,
                      this.state.lat,
                      this.state.long,
                      this.state.locationSelected
                    );
                    this.confirmVideoDateModal();
                  }}
                />
                <CustomButton className="coinPopupBtn" text="Cancel" handler={this.confirmVideoDateModal} />
              </div>
            </MatUiModal>
            {/*********************************************************************************************************** */}
            <MatUiModal isOpen={this.props.coinDialogShow} toggle={this.props.coinToastDismiss} width={500}>
              <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
                <div className="dialogPrev_cancelButton" onClick={this.props.coinToastDismiss}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="col-12">
                  <img
                    src={this.props.UserSelectedToChat.profilePic || Images.placeholder}
                    height={70}
                    width={70}
                    alt={this.props.UserSelectedToChat.firstName || this.props.UserSelectedToChat.name}
                    style={{ borderRadius: "50%", objectFit: "cover" }}
                  />
                </div>
                <div className="previewAttachmentText">Give away Credits to connect.</div>
                <div className="get_chance_to_connect">
                  Are you sure you want to spend Credits and get a chance to connect with{" "}
                  {this.props.UserSelectedToChat.firstName || this.props.UserSelectedToChat.name}
                </div>
                <CustomButton
                  className="coinPopupBtn"
                  text={`I am OK to spend ${
                    this.props.CoinConfig && this.props.CoinConfig.perMsgWithoutMatch && this.props.CoinConfig.perMsgWithoutMatch.Coin
                  } Credits`}
                  handler={(e) => {
                    this.setState({ coinPopUpSuccessfull: true }, () => {
                      console.log("%c handler clicked", "background: #000; color: #fff");
                      // this.GetAllChatsForSingleUser(this.props.UserSelectedToChat.chatId, TS, 20);
                      if (this.state.case === 0) {
                        console.log("%c case 1", "background: #000; color: #fff");
                        this.childref.submitMessage(e);
                        this.setState({ showCoinAnimation: true });
                      } else if (this.state.case === 1) {
                        console.log("gifhandler");
                        this.setState({ showCoinAnimation: true });
                        this.childref.sendGifHandler();
                      } else if (this.state.case === 3) {
                        this.setState({ showCoinAnimation: true });
                        this.uploadImage(this.state.newFiles);
                      } else if (this.state.case === 4) {
                        this.setState({ showCoinAnimation: true });
                        this.getlocation();
                      }
                    });
                    this.props.coinToastDismiss();
                    // this.openmessagedrawerDrawer();
                  }}
                />
              </div>
            </MatUiModal>

            <MatUiModal isOpen={this.state.purcahseCoin} toggle={this.purchaseCoinModal} width={500}>
              <div className="col-12 text-center p-5">
                <div>
                  <img src={Icons.BigHeart} height={180} width={180} alt="wallet" />
                </div>
                <h4 className="pt-3">
                  <FormattedMessage id="message.purchaseCoin" />.
                </h4>
                <div className="pt-2 purchaseCoinButton" onClick={this.purchaseCoinModal}>
                  OK
                </div>
              </div>
            </MatUiModal>
            <MatUiModal isOpen={this.state.dateModal} toggle={this.openDateModal} width={650}>
              <div className="col-12 p-5" style={{ position: "relative" }}>
                <div className="cancelButton" onClick={this.openDateModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="row">
                  <div className="col-4" onClick={this.props.CoinBalance >= 5 ? this.toggleAudioCall : this.purchaseCoinModal}>
                    <div className="dateOptions_circle">
                      <img src={Icons.callDate} width={170} height={170} alt="call-date" />
                    </div>
                    <div className="pt-3 date_options">
                      <FormattedMessage id="message.audioDate" />
                    </div>
                  </div>
                  <div className="col-4" onClick={this.props.CoinBalance >= 500 ? this.togglePhysicalDateModal : this.purchaseCoinModal}>
                    <div className="dateOptions_circle">
                      <img src={Icons.physicalDate} width={170} height={170} alt="physical-date" />
                    </div>
                    <div className="pt-3 date_options">
                      <FormattedMessage id="message.inPersonDate" />
                    </div>
                  </div>
                  <div className="col-4" onClick={this.props.CoinBalance >= 500 ? this.toggleVideoCall : this.purchaseCoinModal}>
                    <div className="dateOptions_circle">
                      <img src={Images.videoCall} width={170} height={170} alt="video-call" />
                    </div>
                    <div className="pt-3 date_options">
                      <FormattedMessage id="message.videoDate" />
                    </div>
                  </div>
                </div>
              </div>
            </MatUiModal>
            <MatUiModal isOpen={this.state.callDateModal} toggle={this.toggleAudioCall} width={500}>
              <div className="col-12 p-5" style={{ position: "relative" }}>
                <div className="cancelButton" onClick={this.toggleAudioCall}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="physicalDateModal_header text-center">
                  <FormattedMessage id="message.planAudioCallDate" />
                </div>
                <div className="physicalDateModal_text pt-2 px-5 text-center">
                  <FormattedMessage id="message.suggestPlaceAndTime" />
                  {this.props.UserSelectedToChat.firstName} <FormattedMessage id="message.confirm" />.
                </div>
                <div className="col-12 d-flex py-3 d_physical_couples_img">
                  <div>
                    <img
                      src={this.props.UserProfile.UserProfilePic}
                      alt={this.props.UserProfile.UserProfilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                  <div>
                    <img
                      src={this.props.UserSelectedToChat.profilePic}
                      alt={this.props.UserSelectedToChat.profilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                </div>
                <div className="physicalDateModal_cat_text pt-3">
                  <FormattedMessage id="message.suggestTime" />
                </div>
                <div>
                  <TextField
                    id="datetime-local"
                    type="datetime-local"
                    onChange={this.handleChange("time")}
                    defaultValue={moment(new Date().getTime()).format("YYYY-MM-DDTHH:mm")}
                    className={classes.textField}
                  />
                </div>
                <div
                  className="date_confirm"
                  onClick={() => {
                    this.toggleAudioCall();
                    setTimeout(() => {
                      this.confirmAudioDateModal();
                    }, 300);
                  }}
                >
                  Proceed
                </div>
              </div>
            </MatUiModal>
            <MatUiModal isOpen={this.state.physicalDateModal} toggle={this.togglePhysicalDateModal} width={500}>
              <div className="col-12 p-5" style={{ position: "relative" }}>
                <div className="cancelButton" onClick={this.togglePhysicalDateModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="physicalDateModal_header text-center">Plan Physical Date</div>
                <div className="physicalDateModal_text pt-2 px-5 text-center">
                  <FormattedMessage id="message.suggestPlaceAndTime" />
                  {this.props.UserSelectedToChat.firstName} <FormattedMessage id="message.confirm" />.
                </div>
                <div className="col-12 d-flex py-3 d_physical_couples_img">
                  <div>
                    <img
                      src={this.props.UserProfile.UserProfilePic}
                      alt={this.props.UserProfile.UserProfilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                  <div>
                    <img
                      src={this.props.UserSelectedToChat.profilePic}
                      alt={this.props.UserSelectedToChat.profilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                </div>
                <div className="physicalDateModal_cat_text mt-4 mb-1">
                  <FormattedMessage id="message.suggestPlace" />
                </div>
                <FormControl className={classes.formControl}>
                  <Select
                    value={this.state.locationSelected && this.state.locationSelected}
                    onChange={this.handleChange("locationSelected")}
                    className={classes.select}
                  >
                    {this.state.items &&
                      this.state.items.map((k, i) => (
                        <MenuItem key={k.name} value={k.name} className={classes.menuItem}>
                          {k.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>

                <div className="physicalDateModal_cat_text pt-3">
                  <FormattedMessage id="message.suggestTime" />
                </div>
                <div>
                  <TextField
                    id="datetime-local"
                    type="datetime-local"
                    onChange={this.handleChange("time")}
                    defaultValue={moment(new Date().getTime()).format("YYYY-MM-DDTHH:mm")}
                    className={classes.textField}
                  />
                </div>
                <div
                  className="date_confirm"
                  onClick={() => {
                    this.togglePhysicalDateModal();
                    setTimeout(() => {
                      this.confirmPhysicalDateDateModal();
                    }, 300);
                  }}
                >
                  Proceed
                </div>
              </div>
            </MatUiModal>
            <MatUiModal isOpen={this.state.videoDateModal} toggle={this.toggleVideoCall} width={500}>
              <div className="col-12 p-5" style={{ position: "relative" }}>
                <div className="cancelButton" onClick={this.toggleVideoCall}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                <div className="physicalDateModal_header text-center">
                  <FormattedMessage id="message.planVideoCallDate" />
                </div>
                <div className="physicalDateModal_text pt-2 px-5 text-center">
                  <FormattedMessage id="message.suggestPlaceAndTime" />
                  {this.props.UserSelectedToChat.firstName} <FormattedMessage id="message.confirm" />.
                </div>
                <div className="col-12 d-flex py-3 d_physical_couples_img">
                  <div>
                    <img
                      src={this.props.UserProfile.UserProfilePic}
                      alt={this.props.UserProfile.UserProfilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                  <div>
                    <img
                      src={this.props.UserSelectedToChat.profilePic}
                      alt={this.props.UserSelectedToChat.profilePic}
                      className="d_physical_date_img"
                    />
                  </div>
                </div>
                <div className="physicalDateModal_cat_text pt-3">
                  <FormattedMessage id="message.suggestTime" />
                </div>
                <div>
                  <TextField
                    id="datetime-local"
                    type="datetime-local"
                    onChange={this.handleChange("time")}
                    defaultValue={moment(new Date().getTime()).format("YYYY-MM-DDTHH:mm")}
                    className={classes.textField}
                  />
                </div>
                <div
                  className="date_confirm"
                  onClick={() => {
                    this.toggleVideoCall();
                    setTimeout(() => {
                      this.confirmVideoDateModal();
                    }, 300);
                  }}
                >
                  Proceed
                </div>
              </div>
            </MatUiModal>
          </div>
          <Dialog
            open={this.state.reportopen}
            onClose={this.closeReportModal}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
              <FormattedMessage id="message.reportUser" />
            </DialogTitle>
            <DialogContent className="py-0 px-3">
              <DialogContentText id="alert-dialog-description">
                {this.state.reportReasons
                  ? this.state.reportReasons.map((data, index) => (
                      <div
                        style={{ cursor: "pointer" }}
                        key={index}
                        className="py-2 d-block text-center border-bottom"
                        onClick={() => this.getReasonForReport(data)}
                      >
                        {data}
                      </div>
                    ))
                  : ""}
              </DialogContentText>
            </DialogContent>
            <DialogActions className="py-2 m-auto">
              <Button className="text-center" onClick={this.closeReportModal} color="primary" autoFocus>
                <FormattedMessage id="message.cancel" />
              </Button>
            </DialogActions>
          </Dialog>

          {/** opens this dialog modal when selecting option of report user */}
          <Dialog
            open={this.state.subDialog}
            onClose={this.toggleSubDialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
              <FormattedMessage id="message.areYouSureReport" /> {this.props.UserSelectedToChat.firstName}
            </DialogTitle>

            <DialogActions className="py-2 m-auto">
              <Button className="text-center" onClick={this.closeConfirmationDialogWithMainDialog} color="primary" autoFocus>
                <FormattedMessage id="message.yes" />
              </Button>
              <Button className="text-center" onClick={this.toggleSubDialog} color="primary" autoFocus>
                <FormattedMessage id="message.no" />
              </Button>
            </DialogActions>
          </Dialog>

          <Snackbar
            type={this.state.variant}
            message={this.state.usermessage}
            open={this.state.open}
            timeout={2500}
            onClose={this.handleCloseSnacbkbar}
          />
        </div>
      );
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    coinToastShow: () => dispatch(actionChat.showUpCoinChatToast()),
    coinToastDismiss: () => dispatch(actionChat.dismissCoinChatToast()),
    userSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
    addNewChatToExistingObject: (msg) => dispatch(actionChat.addNewChatToExistingObject(msg)),
    allMessagesForSelectedUser: (msgs) => dispatch(actions.allMessagesForSelectedUser(msgs)),
    leftSideChatLogsFN: (allChatLogs) => dispatch(actionsUser.leftSideChatLogs(allChatLogs)), // to get the left side chat logs of logged in user
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    chatSessionStarted: () => dispatch(actions.chatSessionStarted()),
    chatSessionEnded: () => dispatch(actions.chatSessionEnded()),
    imageUploadFn: (img) => dispatch(actions.uploadImage(img)),
    deleteAllMessagesForSingleUserFunc: (id) => dispatch(actionChat.deleteAllMessagesForSingleUser(id)),
    justViewProfileActive: () => dispatch(actions.justViewProfileActive()),
    __callInit: (data) => dispatch(__callInit(data)),
    readOnlyUserRemove: () => dispatch(actions.justViewProfileRemove()),
    updateUnreadChatCountFunc: (count) => dispatch(actionChat.storeUnreadChatCount(count)),
  };
};

function mapstateToProps(state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    UserSelectedToChat: state.UserSelectedToChat.UserSelectedToChat,
    SingleUserAllMessages: state.UserSelectedToChat.SingleUserAllMessages,
    selectedProfile: state.selectedProfile.selectedProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    imageUploaded: state.UserSelectedToChat.imageUploaded,
    chatStarted: state.UserSelectedToChat.chatStarted,
    getLatestTimeStamp: state.UserSelectedToChat.getLatestTimeStamp,
    serverImageFolderLocation: state.UserSelectedToChat.serverImageFolderLocation,
    messageAcknowledgement: state.UserSelectedToChat.doubleTickAck,
    coinDialogShow: state.UserSelectedToChat.showCoinToast,
    CoinConfig: state.CoinConfig.CoinConfig,
    __mqttdata: state.mqttData.mqttData,
    __onSearchSendLocation: state.UserSelectedToChat.onSearchSendLocation,
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    checkIfUserIsProUser: state.ProUser,
  };
}

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(ChatPage)));
