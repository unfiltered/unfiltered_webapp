// import React, { Component } from "react";
// import Map from "./Current-Location";
// import { getCookie, setCookie } from "../../../../lib/session";
// import { connect } from "react-redux";
// import { addNewLocationToSearch, setDefaultLocation } from "../../../../actions/User";
// import { withStyles } from "@material-ui/core/styles";

// import green from "@material-ui/core/colors/green";
// import { onSearchSendLocation } from "../../../../actions/UserSelectedToChat";
// // import "./Home.scss";

// const styles = (theme) => ({
//   paper: {
//     position: "relative",
//     width: theme.spacing.unit * 50,
//     backgroundColor: theme.palette.background.paper,
//     boxShadow: theme.shadows[5],
//     padding: theme.spacing.unit * 4,
//     outline: "none",
//   },
//   root: {
//     color: green[600],
//     "&$checked": {
//       color: green[500],
//     },
//   },
//   checked: {},
// });

// class LocationHome extends Component {
//   state = {
//     value: getCookie("UserSearchLocation") ? getCookie("UserSearchLocation") : "",
//   };

//   handleChange = (event) => {
//     this.setState({ value: event.target.value });
//     this.props.setDefaultLocation(event.target.value);
//     setCookie("UserSearchLocation", event.target.value);
//   };

//   addNewLocation = (location) => {
//     console.log("location is", location);
//     this.setState({ Usernewloc: location });
//     this.handleLocation();
//     // this.props.addNewLocationFn([location]);
//   };

//   componentDidMount() {
//     let getLocationFromCookies = getCookie("selectedLocation");
//     let parsedLocation = JSON.parse(getLocationFromCookies);
//     // console.log("getLocationFromCookies", getLocationFromCookies);
//     this.props.addNewLocationFn(getLocationFromCookies);
//   }

//   handleLocation = () => {
//     this.props.getlocation();
//   };

//   render() {
//     return (
//       <div>
//         <div className="col-12">
//           <div className="row">
//             <div className="col-12 google_map_chat">
//               <Map
//                 google={this.props.google}
//                 center={{ lat: parseFloat(getCookie("lat")), lng: parseFloat(getCookie("long")) }}
//                 height="300px"
//                 zoom={15}
//                 isDesktop={true}
//                 addNewLocation={this.addNewLocation}
//                 closelocationmodel={this.props.closelocationmodel}
//                 __onSearchSendLocation={this.props.__onSearchSendLocation}
//               />
//             </div>
//             {/* <div className="col-4">
//               <div style={otherLocationRadioButtons}>
//                 <div>OTHER LOCATIONS</div>
//               </div>
//               <div className="col-12 pt-2">
//                 <div className="row">
//                   <div className="col-2 pr-0">
//                     <i
//                       className="fas fa-map-marker-alt"
//                       style={{
//                         fontSize: "29px",
//                         paddingTop: "5px",
//                         paddingLeft: "7px",
//                         color: "rgb(247, 65, 103)"
//                       }}
//                     />
//                   </div>
//                   <div className="col-10 pl-0 Muserlocation">
//                     <p className="m-0">My Current Location :</p>
//                     <p className="m-0">{this.state.Usernewloc}</p>
//                   </div>
//                 </div>
//               </div>
//               <div>
//                 <button className="Sendlocation_btn" onClick={this.handleLocation}>
//                   Send
//                 </button>
//               </div>
//             </div> */}
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     currentLocation: state.UserProfile.UserData.currentLocation,
//     locations: state.UserProfile.UserData.locations,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     addNewLocationFn: (loc) => dispatch(addNewLocationToSearch(loc)),
//     setDefaultLocation: (loc) => dispatch(setDefaultLocation(loc)),
//     __onSearchSendLocation: (loc) => dispatch(onSearchSendLocation(loc)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LocationHome));

import React from "react";
import { GoogleApiWrapper, Marker, Map } from "google-maps-react";
import { getCookie } from "../../../../lib/session";
import Button from "../../../../Components/Button/Button";
import { geolocated } from "react-geolocated";
import { keys } from "../../../../lib/keys";
import { withRouter } from "react-router-dom";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";

// let lat = getCookie("lat");
// let long = getCookie("long");

class LocationHome extends React.Component {
  state = {
    position: null,
    multiselectchecbox: [],
    selectedAddressToShow: "",
    selectedLocation: "", // this will store object of selected location
    markers: [
      {
        name: "Current position",
        position: {
          lat: "",
          lng: "",
        },
      },
    ],
    lat: "",
    lng: "",
    city: "",
    locationSelected: "", // this will store the {String} address of selected location
  };
  componentDidUpdate(prevProps) {
    if (this.props !== prevProps.map) this.renderAutoComplete();
  }

  onSubmit(e) {
    e.preventDefault();
  }

  renderAutoComplete() {
    const { google, map } = this.props;
    if (!google || !map) return;
    const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
    autocomplete.bindTo("bounds", map);
    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) return;
      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(9);
      }
      this.setState({ position: place.geometry.location }, () => {
        this.getAddressHelper(place.geometry.location.lat(), place.geometry.location.lng());
      });
    });
  }

  // function generates {lat, lng, address, and city name which is saved in cookies}
  getAddressHelper = (lat, lng) => {
    let google = window.google;
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ latLng: latlng }, (results, status) => {
      console.log("results, status", results, status);
      let resLength = results && results[0].address_components.length;
      if (status === google.maps.GeocoderStatus.OK) {
        if (resLength <= 5) {
          console.log("#1");
          this.setState(
            {
              lat,
              lng,
              locationSelected: results[0].formatted_address,
              city: results[0].address_components[resLength - 3].long_name,
            },
            () => {
              this.props.setLocationForSendingMessage(
                lat,
                lng,
                results[0].address_components[resLength - 3].long_name,
                results[0].formatted_address
              );
            }
          );
        } else {
          console.log("#2");
          this.setState(
            {
              lat,
              lng,
              locationSelected: results[0].formatted_address,
              city: results[0].address_components[resLength - 5].long_name,
            },
            () => {
              this.props.setLocationForSendingMessage(
                lat,
                lng,
                results[0].address_components[resLength - 5].long_name,
                results[0].formatted_address
              );
            }
          );
        }
      }
    });
  };

  onMarkerDragEnd = (coord, index) => {
    const { latLng } = coord;
    let lat = latLng.lat();
    let lng = latLng.lng();
    this.getAddressHelper(lat, lng);
    this.setState((prevState) => {
      const markers = [...this.state.markers];
      markers[index] = { ...markers[index], position: { lat, lng } };
      return { markers };
    });
  };

  createLocationToSend = (lat, lng, address, completeAddress) => {
    console.log("completeAddress", completeAddress);
    return `(${lat},${lng})@@${address}@@${completeAddress}`;
  };

  componentDidMount() {
    let locationSelected = getCookie("selectedLocation");
    let onMountAddress = JSON.parse(locationSelected);
    const markers = [...this.state.markers];
    const position = { ...this.state.position };
    markers[0] = { ...markers[0], position: { lat: onMountAddress.lat, lng: onMountAddress.lng } };
    position["lat"] = onMountAddress.lat;
    position["lng"] = onMountAddress.lng;
    this.setState({
      locationSelected: onMountAddress.address,
      lat: onMountAddress.lat,
      lng: onMountAddress.lng,
      markers,
      position,
      city: onMountAddress.city,
    });
    this.props.setLocationForSendingMessage(onMountAddress.lat, onMountAddress.lng, onMountAddress.city, onMountAddress.address);
    this.renderAutoComplete();
  }

  render() {
    let { position, locationSelected } = this.state;
    return (
      <div className="col-12 px-0 bg-white">
        <div className="row mx-0 py-3 m_location_send_input">
          <div className="col-3">
            <img src={Icons.closeBtn} alt="back" height={20} width={25} onClick={this.props.closelocationmodel} />
          </div>
          <div className="col-6">Send Location</div>
          <div className="col-3"></div>
        </div>
        <form onSubmit={this.onSubmit} className="m_location_searchBox">
          <input placeholder="Enter a location" ref={(ref) => (this.autocomplete = ref)} type="text" />
        </form>
        <div>
          <Map
            {...this.props}
            center={position}
            initialCenter={{
              lat: locationSelected && locationSelected.lat,
              lng: locationSelected && locationSelected.lng,
            }}
            centerAroundCurrentLocation={false}
            containerStyle={{
              height: "370px",
              position: "relative",
              width: "100%",
            }}
          >
            <Marker position={position} draggable={true} onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)} />
          </Map>
        </div>
        <div className="col-12 py-2 m_filter_modal_address">
          <FormattedMessage id="message.address" />
        </div>
        <div className="col-12">
          <textarea value={this.state.locationSelected} className="m_filtermap_textarea" />
        </div>
        <div className="col-12 py-3">
          <Button
            className="d_location_modal_add_new_location"
            text="Send Location"
            handler={() => {
              console.log(this.state.lat, this.state.lng, this.state.city, this.state.locationSelected);
              this.props.setLocationForSendingMessage(this.state.lat, this.state.lng, this.state.city, this.state.locationSelected);
              this.props.closelocationmodel();
              this.props.sendLocation();
              // setTimeout(() => {
              //   this.props.setLocationForSendingMessage("", "", "", "");
              // }, 1000);
            }}
          />
        </div>
      </div>
    );
  }
}

const MapWrapper = (props) => (
  <Map google={props.google} visible={false}>
    <LocationHome {...props} />
  </Map>
);

export default withRouter(
  GoogleApiWrapper({
    apiKey: keys.googleApiKey,
  })(
    geolocated({
      positionOptions: { enableHighAccuracy: false },
      userDecisionTimeout: 5000,
    })(MapWrapper)
  )
);
