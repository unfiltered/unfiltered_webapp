import React, { Component } from "react";
import { withGoogleMap, GoogleMap, withScriptjs, InfoWindow, Marker } from "react-google-maps";
import Geocode from "react-geocode";
import Autocomplete from "react-google-autocomplete";
import { getCookie, setCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
Geocode.setApiKey("AIzaSyB5Xj_33Ld1cVJeUoZzzNMkSiAto_CCZrA");
Geocode.enableDebug();

class Currentlocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      city: "",
      area: "",
      state: "",
      mapPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
      markerPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
    };
  }
  /**
   * Get the current address from the default map position and set those values in the state
   */
  componentDidMount() {
    Geocode.fromLatLng(this.state.mapPosition.lat, this.state.mapPosition.lng).then(
      (response) => {
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);
        this.props.__onSearchSendLocation(this.createLocationToSend(this.state.mapPosition.lat, this.state.mapPosition.lng, city, address));
        this.setState({
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : "",
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }
  /**
   * Component should only update ( meaning re-render ), when the user selects the address, or drags the pin
   *
   * @param nextProps
   * @param nextState
   * @return {boolean}
   */
  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.state.markerPosition.lat !== this.props.center.lat ||
      this.state.address !== nextState.address ||
      this.state.city !== nextState.city ||
      this.state.area !== nextState.area ||
      this.state.state !== nextState.state
    ) {
      return true;
    } else if (this.props.center.lat === nextProps.center.lat) {
      return false;
    }
  }
  /**
   * Get the city and set the city input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getCity = (addressArray) => {
    let city = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && "administrative_area_level_2" === addressArray[i].types[0]) {
        city = addressArray[i].long_name;
        return city;
      }
    }
  };
  /**
   * Get the area and set the area input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getArea = (addressArray) => {
    let area = "";
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0]) {
        for (let j = 0; j < addressArray[i].types.length; j++) {
          if ("sublocality_level_1" === addressArray[i].types[j] || "locality" === addressArray[i].types[j]) {
            area = addressArray[i].long_name;
            return area;
          }
        }
      }
    }
  };
  /**
   * Get the address and set the address input value to the one selected
   *
   * @param addressArray
   * @return {string}
   */
  getState = (addressArray) => {
    let state = "";
    for (let i = 0; i < addressArray.length; i++) {
      for (let i = 0; i < addressArray.length; i++) {
        if (addressArray[i].types[0] && "administrative_area_level_1" === addressArray[i].types[0]) {
          state = addressArray[i].long_name;
          return state;
        }
      }
    }
  };
  /**
   * And function for city,state and address input
   * @param event
   */
  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  /**
   * This Event triggers when the marker window is closed
   *
   * @param event
   */
  onInfoWindowClose = (event) => {
    console.log("event", event);
  };

  /**
   * When the marker is dragged you get the lat and long using the functions available from event object.
   * Use geocode to get the address, city, area and state from the lat and lng positions.
   * And then set those values in the state.
   *
   * @param event
   */
  onMarkerDragEnd = (event) => {
    Geocode.fromLatLng(event.latLng.lat(), event.latLng.lng()).then(
      (response) => {
        console.log("on drag 1", response.results[0]);
        const address = response.results[0].formatted_address,
          addressArray = response.results[0].address_components,
          city = this.getCity(addressArray),
          area = this.getArea(addressArray),
          state = this.getState(addressArray);
        console.log("city", city);
        console.log("area", area);
        if ((event.latLng.lat() !== "" || event.latLng.lat() !== undefined) && (event.latLng.lng() !== "" || event.latLng.lng() !== undefined)) {
          this.props.__onSearchSendLocation(
            this.createLocationToSend(event.latLng.lat(), event.latLng.lng(), response.results[0].address_components.long_name, address)
          );
        }
        this.setState({
          address: address ? address : "",
          area: area ? area : "",
          city: city ? city : "",
          state: state ? state : "",
          markerPosition: {
            lat: event.latLng.lat(),
            lng: event.latLng.lng(),
          },
          mapPosition: {
            lat: event.latLng.lat(),
            lng: event.latLng.lng(),
          },
        });
        setCookie("Chatlat", event.latLng.lat());
        setCookie("Chatlng", event.latLng.lng());
      },
      (error) => {
        console.error(error);
      }
    );
  };

  createLocationToSend = (lat, lng, address, completeAddress) => {
    console.log("completeAddress", completeAddress);
    return `(${lat},${lng})@@${address}@@${completeAddress}`;
  };

  /**
   * When the user types an address in the search box
   * @param place
   */
  onPlaceSelected = (place) => {
    const address = place.formatted_address,
      addressArray = place.address_components,
      city = this.getCity(addressArray),
      area = this.getArea(addressArray),
      state = this.getState(addressArray),
      latValue = place.geometry.location.lat(),
      lngValue = place.geometry.location.lng();
    // console.log("****on place selected *******", place.address_components[0].long_name, address);
    if ((latValue !== "" || latValue !== undefined) && (lngValue !== "" || lngValue !== undefined)) {
      this.props.__onSearchSendLocation(this.createLocationToSend(latValue, lngValue, place.address_components[0].long_name, address));
    }

    setCookie("Chatlat", latValue);
    setCookie("Chatlng", lngValue);

    // Set these values in the state.
    this.setState({
      address: address ? address : "",
      area: area ? area : "",
      city: city ? city : "",
      state: state ? state : "",
      markerPosition: {
        lat: latValue,
        lng: lngValue,
      },
      mapPosition: {
        lat: latValue,
        lng: lngValue,
      },
    });
  };

  render() {
    const AsyncMap = withScriptjs(
      withGoogleMap((props) => (
        <GoogleMap
          google={this.props.google}
          defaultZoom={this.props.zoom}
          defaultCenter={{
            lat: this.state.mapPosition.lat,
            lng: this.state.mapPosition.lng,
          }}
        >
          {/* InfoWindow on top of marker */}
          <InfoWindow
            onClose={this.onInfoWindowClose}
            position={{
              lat: this.state.markerPosition.lat + 0.0018,
              lng: this.state.markerPosition.lng,
            }}
          >
            <div>
              <span style={{ padding: 0, margin: 0 }}>{this.state.address}</span>
            </div>
          </InfoWindow>
          {/*Marker*/}
          <Marker
            google={this.props.google}
            name={"Dolores park"}
            draggable={true}
            onDragEnd={this.onMarkerDragEnd}
            position={{
              lat: this.state.markerPosition.lat,
              lng: this.state.markerPosition.lng,
            }}
          />
          <Marker />
          {/* For Auto complete Search Box */}
          <Autocomplete
            style={{
              width: "90%",
              height: "40px",
              paddingLeft: "16px",
              marginTop: "2px",
            }}
            onPlaceSelected={this.onPlaceSelected}
            types={["(regions)"]}
          />
        </GoogleMap>
      ))
    );
    let map;
    let currentLocation = { color: "#e31b1b", paddingLeft: "5px", fontWeight: 500 };
    let locationSection = { fontSize: "13px" };
    let closeBtn = { position: "absolute", right: "20px", top: "20px", zIndex: 999 };
    if (this.props.center.lat !== undefined) {
      map = (
        <div style={this.props && this.props.isDesktop ? { marginBottom: "70px" } : { marginBottom: 0 }}>
          <div onClick={() => this.props.closelocationmodel()} style={closeBtn}>
            <img src={Icons.closeBtn} alt="close-btn" width="20" height="20" />
          </div>
          <div className="col-12 text-center h5 pt-4 pb-4">Search Location</div>
          <div className="pb-3 locationSection" style={locationSection}>
            Current Location: <span style={currentLocation}>{getCookie("UserCurrentCity")}</span>
          </div>

          <AsyncMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKh4I1G_YylA0QqlJvKFoRshqPmY562s0&libraries=places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: this.props.height }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
          <div
            className={this.props && this.props.isDesktop ? "Dnewlocation mb-3 mt-3" : "Mnewlocation mb-3 mt-3"}
            onClick={() => this.props.addNewLocation(this.state.address)}
          >
            <p className="m-0">Share Location</p>
          </div>
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}

export default Currentlocation;
