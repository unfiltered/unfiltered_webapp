import "./Chat.scss";
import React from "react";
import "./ChatUpdated.scss";
import Message from "./Message";
import ReactDOM from "react-dom";
import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import LocationHome from "./Location-Home";
import { Icons } from "../../../../Components/Icons/Icons";
import { getCookie } from "../../../../lib/session";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { getGifs, getRandomGifs, sendCoins } from "../../../../controller/auth/verification";
import { Images } from "../../../../Components/Images/Images";
import Button from "../../../../Components/Button/Button";
import { FormattedMessage } from "react-intl";
import { sendMessageOverMqtt } from "../../../../lib/MqttHOC/utils";
import * as actions from "../../../../actions/UserSelectedToChat";
import { connect } from "react-redux";

class Chatroom extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      emojiicons: "",
      oldMessages: "",
      searchTerm: "",
      stickersList: [],
      gifModal: false, // for gif or sticker
      singleGif: "",
      randomGifs: "",
      locationmodel: false,
      preview: false,
      open: false,
      variant: "",
      usermessage: "",
      purcahseCoin: false,
      currentPage: 19,
      text: "",
      stickerModal: false,
      openTransferCoinsModal: false,
      coinsToTransfer: "",
      gifHandlerState: true, // by default false, if true => sticker
    };
    this.textInputRef = React.createRef();
    this.submitMessage = this.submitMessage.bind(this);
    this.toggle = this.toggle.bind(this);

    props.reff(this);
  }

  componentDidMount() {
    getRandomGifs().then((res) => {
      this.setState({ randomGifs: res.data.data });
    });
    this.scrollListener(this.myRef.current);
    this.scrollToBot();
    setTimeout(() => {
      if (this.props && this.props.chatStarted) {
        try {
          this.textInputRef.current.focus();
        } catch (e) {}
      }
    }, 1000);
  }

  coinsToTransferHandler = (e) => {
    this.setState({ coinsToTransfer: e.target.value });
  };

  handleScroll = (event) => {
    var node = event.target;
    if (node.scrollTop === 0 && node.scrollHeight > node.clientHeight) {
      console.log("am i top");
      if (this.props.allUserMessages.length % 10 === 0) {
        setTimeout(() => {
          this.props.GetAllChatsForSingleUser(
            this.props.UserSelectedToChat.chatId,
            this.props.allUserMessages[this.state.currentPage].timestamp,
            10
          );
          this.setState({ currentPage: this.state.currentPage + 10 });
        }, 300);
      }
    }
  };

  componentWillUpdate(prevProps, prevState) {
    if (this.props.UserSelectedToChat !== prevProps.UserSelectedToChat) {
      this.setState({ currentPage: 19 });
      // console.log("[prevProps]", prevProps);

      let ChatLogs = [...prevProps.leftSideChatLogs];
      let index = ChatLogs.findIndex((k) => k.chatId === prevProps.UserSelectedToChat.chatId);
      if (ChatLogs[index] && ChatLogs[index]["checked"] === true && index > -1) {
        ChatLogs[index]["checked"] = false;
        ChatLogs[index]["totalUnread"] = 0;
        prevProps._UserSelectedToChat(ChatLogs[index]);
      }

      console.log("**** changed user ****");
      this.textInputRef.current.focus();
    }
  }

  scrollListener = (node) => {
    node.addEventListener("scroll", this.handleScroll, true); // const scrollTop = document.getElementById("chats");
  };

  removeScrollListeber = (node) => {
    node.removeEventListener("scroll", this.handleScroll, true);
  };

  toggleTransferCoinModal = () => {
    this.setState({
      openTransferCoinsModal: !this.state.openTransferCoinsModal,
    });
  };

  componentDidUpdate() {
    this.scrollToBot();
  }

  componentWillUnmount() {
    this.removeScrollListeber(this.myRef.current);
    window.removeEventListener("scroll", this.scrollListener);
  }

  purchaseCoinModal = () => {
    this.setState({ purcahseCoin: !this.state.purcahseCoin });
  };

  closelocationmodel = () => {
    this.setState({ locationmodel: false });
  };

  toggleStickerModal = () => {
    this.setState({
      stickerModal: !this.state.stickerModal,
      gifHandlerState: false,
    });
  };

  togglePreviewModal = () => {
    this.setState({ preview: !this.state.preview });
  };

  encode = (str) => {
    return window.btoa(str);
  };

  toggleGifModal = () => {
    this.setState({ gifModal: !this.state.gifModal, gifHandlerState: true });
  };

  getGifDetails = (gif, bool) => {
    if (gif) {
      this.setState({
        singleGif: bool ? gif.images.downsized_large.url : gif.images.downsized_still.url,
        gifModal: false,
        stickerModal: false,
        preview: true,
      });
    }
  };

  getStickers = (searchTerm) => {
    getGifs(searchTerm)
      .then((res) => {
        this.setState({ randomGifs: res.data.data });
      })
      .catch((err) => {});
  };

  onChangeHandler = (e) => {
    this.setState({ searchTerm: e.target.value }, () => {
      this.getStickers(this.state.searchTerm);
    });
  };

  scrollToBot() {
    ReactDOM.findDOMNode(this.myRef.current).scrollTop = ReactDOM.findDOMNode(this.myRef.current).scrollHeight;
  }

  submitMessage(e) {
    this.props.caseHandler(0);
    let newMsg = this.state.text;
    e.preventDefault();
    if (newMsg.trim() === "" || newMsg.trim().length === 0) {
      e.preventDefault();
      return;
    }
    e.preventDefault();
    let uid = getCookie("uid");
    let objToSendIfOnline = {
      chatId: this.props.UserSelectedToChat.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props.sendersName,
      payload: this.textencode(newMsg),
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to:
        this.props.UserSelectedToChat.recipientId ||
        this.props.UserSelectedToChat._id ||
        this.props.UserSelectedToChat.opponentId ||
        this.props.UserSelectedToChat.from,
      toDocId: "",
      type: "0",
      status: 1,
      userImage: this.props.UserSelectedToChat.profilePic || this.props.UserSelectedToChat.userImage,
    };
    if (this.props.chatStarted) {
      if (
        (parseInt(this.props.coins) === 0 &&
          "chatInitiatedBy" in this.props.UserSelectedToChat &&
          this.props.UserSelectedToChat.chatInitiatedBy !== getCookie("uid")) ||
        (parseInt(this.props.coins) === 0 && this.props.UserSelectedToChat.initiated === false)
      ) {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.props.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.setState({ text: "" });
      } else if (
        "chatInitiatedBy" in this.props.UserSelectedToChat &&
        (this.props.UserSelectedToChat.isMatched || this.props.UserSelectedToChat.isMatchedUser || this.props.UserSelectedToChat.isFriend)
      ) {
        this.props.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.setState({ text: "" });
      } else if (parseInt(this.props.coins) >= 0) {
        if (
          ((this.props.UserSelectedToChat.isMatched === 0 ||
            this.props.UserSelectedToChat.isMatchedUser === 0 ||
            this.props.UserSelectedToChat.isFriend === 0) &&
            !("chatInitiatedBy" in this.props.UserSelectedToChat)) ||
          !("initiated" in this.props.UserSelectedToChat)
        ) {
          if (this.props.coinPopUpSuccessfull === false) {
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.props.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.props.CoinChat(objToSendIfOnline);
            this.props.addNewMessage(objToSendIfOnline);
            this.setState({ text: "" });
          }
        } else if ("initiated" in this.props.UserSelectedToChat && this.props.UserSelectedToChat.initiated === true) {
          if (this.props.coinPopUpSuccessfull === false) {
            console.log("%c false", "background: #000; color: #fff");
            this.props.coinToastShow();
          } else if (this.props.coinPopUpSuccessfull === true) {
            console.log("%c true", "background: #000; color: #fff");
            objToSendIfOnline["isMatchedUser"] = 0;
            this.props.CoinChat(objToSendIfOnline);
            this.props.addNewMessage(objToSendIfOnline);
            this.setState({ text: "" });
          }
        } else {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.props.addNewMessage(objToSendIfOnline);
          sendMessageOverMqtt(objToSendIfOnline);
          this.setState({ text: "" });
        }
      }
    }
  }

  isUserMatchedOrFriend = () => {
    let { isFriend, isMatchedUser, isMatched } = this.props.UserSelectedToChat;
    if (isMatchedUser == 1 || isMatched == 1 || isFriend == 1) {
      return 1;
    }
    return 0;
  };

  sendGifHandler = () => {
    this.props.caseHandler(1);
    let gif = this.state.singleGif;
    let bool = this.state.gifHandlerState;
    this.setState({ preview: false });
    const encodedString = new Buffer(gif).toString("base64");
    let uid = getCookie("uid");
    let objToSendIfOnline = {
      chatId: this.props.UserSelectedToChat.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props.sendersName,
      payload: encodedString,
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to:
        this.props.UserSelectedToChat.recipientId ||
        this.props.UserSelectedToChat._id ||
        this.props.UserSelectedToChat.opponentId ||
        this.props.UserSelectedToChat.from,
      toDocId: "",
      status: 1,
      type: bool ? "8" : "6",
      userImage: this.props.UserSelectedToChat.profilePic || this.props.UserSelectedToChat.userImage,
    };
    if (this.props.chatStarted) {
      if (
        (parseInt(this.props.coins) === 0 &&
          "chatInitiatedBy" in this.props.UserSelectedToChat &&
          this.props.UserSelectedToChat.chatInitiatedBy !== getCookie("uid")) ||
        (parseInt(this.props.coins) === 0 && this.props.UserSelectedToChat.initiated === false)
      ) {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.props.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.setState({ text: "" });
      } else if (
        "chatInitiatedBy" in this.props.UserSelectedToChat &&
        (this.props.UserSelectedToChat.isMatched || this.props.UserSelectedToChat.isMatchedUser || this.props.UserSelectedToChat.isFriend)
      ) {
        this.props.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.setState({ text: "" });
      } else if (parseInt(this.props.coins) >= 0) {
        if (
          ((this.props.UserSelectedToChat.isMatched === 0 ||
            this.props.UserSelectedToChat.isMatchedUser === 0 ||
            this.props.UserSelectedToChat.isFriend === 0) &&
            !("chatInitiatedBy" in this.props.UserSelectedToChat)) ||
          !("initiated" in this.props.UserSelectedToChat)
        ) {
          if (this.props.coinPopUpSuccessfull === false) {
            this.props.coinToastShow();
          } else if (this.props.coinPopUpSuccessfull === true) {
            objToSendIfOnline["isMatchedUser"] = 0;
            this.props.CoinChat(objToSendIfOnline);
            this.props.addNewMessage(objToSendIfOnline);
            this.setState({ text: "" });
          }
        } else if ("initiated" in this.props.UserSelectedToChat && this.props.UserSelectedToChat.initiated === true) {
          if (this.props.coinPopUpSuccessfull === false) {
            this.props.coinToastShow();
          } else if (this.props.coinPopUpSuccessfull === true) {
            objToSendIfOnline["isMatchedUser"] = 0;
            this.props.CoinChat(objToSendIfOnline);
            this.props.addNewMessage(objToSendIfOnline);
            this.setState({ text: "" });
          }
        } else {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.props.addNewMessage(objToSendIfOnline);
          sendMessageOverMqtt(objToSendIfOnline);
          this.setState({ text: "" });
        }
      }
    }
  };

  sendCoinsFn = () => {
    sendCoins(getCookie("token"), {
      amount: parseInt(this.state.coinsToTransfer),
      chatId: this.props.UserSelectedToChat.chatId ? this.props.UserSelectedToChat.chatId : "",
      from: getCookie("uid"),
      id: new Date().getTime().toString(),
      name: this.props.sendersName,
      messageId: new Date().getTime().toString(),
      payload: btoa("Sent Coins"),
      reason: btoa("Sent Coins"),
      receiverIdentifier: this.props.UserSelectedToChat.recipientId || this.props.UserSelectedToChat._id,
      receiverImage: this.props.UserSelectedToChat.profilePic || this.props.UserSelectedToChat.userImage,
      receiverName: this.props.UserSelectedToChat.firstName,
      targetUserId: this.props.UserSelectedToChat.recipientId || this.props.UserSelectedToChat._id,
      timestamp: new Date().getTime(),
      to: this.props.UserSelectedToChat.recipientId || this.props.UserSelectedToChat._id,
      toDocId: "xxx",
      type: 15,
      userImage: this.props.myPic,
    })
      .then((res) => {
        this.props.coinReduce(parseInt(this.state.coinsToTransfer));
        console.log("res", res);
      })
      .catch((err) => console.log("res", err));
    this.toggleTransferCoinModal();
  };

  toggle() {
    this.setState((state) => ({
      collapse: !state.collapse,
    }));
  }

  handleemoji = (e) => {
    console.log("[handleEmoji]", e);
    if (e.unified.length <= 5) {
      let emojiPic = String.fromCodePoint(`0x${e.unified}`);
      console.log("emojiPic", emojiPic);
      this.setState({
        text: this.state.text + emojiPic,
      });
    } else {
      let sym = e.unified.split("-");
      let codesArray = [];
      sym.forEach((el) => codesArray.push("0x" + el));
      let emojiPic = String.fromCodePoint(...codesArray);
      console.log("emojiPic", emojiPic);
      this.setState({
        text: this.state.text + emojiPic,
      });
    }
    this.toggle();
  };

  handleSubmit = () => {
    if (this.state.text === "" || this.state.text.trim() === "") {
      return;
    } else if (this.state.text !== "") {
      this.setState({
        text: "",
      });
    }
  };

  decode = (str) => {
    return window.atob(str);
  };

  handleChange = (e) => {
    this.setState({ text: e.target.value });
  };

  placeHolderRenderer = (obj) => {
    if (obj.isBlocked) {
      return `${obj.firstName} has blocked you and therefore you cannot message this user`;
    } else if (obj.isBlockedByMe) {
      return `You have blocked ${obj.firstName} and therefore cannot send this user a message`;
    } else if (obj.isBlocked === 0 && obj.isBlockedByMe === 0) {
      return "Type a Message...";
    }
  };

  textencode = (str) => {
    try {
      return btoa(unescape(encodeURIComponent(str)));
    } catch (e) {}
  };

  textdecode = (str) => {
    // console.log("text decode ", str);
    try {
      return decodeURIComponent(escape(atob(str)));
    } catch (e) {}
  };

  transferCoinsMoneyExeedModal = () => {
    this.setState({
      usermessage: "Transfer amount should be less than your current credit balance",
      open: true,
      variant: "error",
    });
    setTimeout(() => {
      this.setState({ open: false });
    }, 3000);
  };

  render() {
    let withData =
      this.props.allUserMessages && this.props.allUserMessages.length > 0
        ? this.props.allUserMessages &&
          this.props.allUserMessages
            .map((chat, index) => (
              <Message
                key={index}
                chat={chat}
                user={this.props.myName}
                sendersName={this.props.sendersName}
                loadingImageSkeleton={this.props.loadingImageSkeleton}
                textdecode={this.textdecode}
              />
            ))
            .reverse()
        : "No Messages Found";

    let withoutData =
      this.props.allUserMessages && this.props.allUserMessages
        ? this.props.allUserMessages
            .map((chat, index) => (
              <Message
                key={index}
                chat={chat}
                user={this.props.myName}
                sendersName={this.props.sendersName}
                loadingImageSkeleton={this.props.loadingImageSkeleton}
                textdecode={this.textdecode}
              />
            ))
            .reverse()
        : "No Messages Found";

    let chatNotYetStarted = (
      <div className="chatNotStarted">
        <div>
          <FormattedMessage id="message.chats_lets_start" />
        </div>
        <div>
          <FormattedMessage id="message.chats_why_not_send" />
        </div>
      </div>
    );

    let toRender = "";
    if (this.props.chatStarted) {
      if (this.props.allUserMessages && this.props.allUserMessages && this.props.allUserMessages.length >= 0) {
        toRender = withoutData;
      } else if (this.props.allUserMessages && this.props.allUserMessages.length > 0) {
        toRender = withData;
      }
    } else {
      toRender = chatNotYetStarted;
    }
    return (
      <div className="chatroom h-100" id="chatroom">
        <ul
          id="chats"
          className={this.props.chatStarted ? "chats" : "chats d-flex justify-content-center align-items-center"}
          ref={this.myRef}
        >
          {toRender}
        </ul>
        <div className="py-2 bg-white">
          <div className="d_chat_input_container">
            {this.props.showCoinAnimation ? (
              <lottie-player
                src="https://assets6.lottiefiles.com/packages/lf20_smGEjL.json"
                background="transparent"
                speed="1"
                repeat
                style={{
                  width: "200px",
                  height: "200px",
                  position: "absolute",
                  zIndex: 9999,
                  bottom: "-30px",
                  left: "-20px",
                }}
                autoplay
              ></lottie-player>
            ) : (
              <span />
            )}
            <Button
              className="chat_handlers ml-2 smiley"
              handler={this.props.chatStarted ? this.toggle : () => console.log("cannot 0")}
              text={<img src={Icons.GrinSmiley} height={18} width={18} alt="smiley" />}
              disabled={this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe}
            />
            <div className="d_chat_input_area">
              <form onSubmit={(e) => this.submitMessage(e)}>
                <input
                  className={
                    this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe ? "blockedUserInput" : ""
                  }
                  placeholder={this.placeHolderRenderer(this.props.UserSelectedToChat)}
                  disabled={
                    this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe || !this.props.chatStarted
                  }
                  type="text"
                  ref={this.textInputRef}
                  value={this.state.text}
                  onChange={this.handleChange}
                  onSubmit={this.handleSubmit}
                />
              </form>
              <div className="d_chat_options">
                {this.state.collapse ? (
                  <Picker
                    native={false}
                    set="google"
                    style={{
                      position: "absolute",
                      bottom: "30px",
                      right: "20px",
                    }}
                    onClick={this.handleemoji}
                  />
                ) : (
                  <span />
                )}
                {/* <Button
                  className="chat_handlers"
                  handler={this.props.chatStarted ? this.toggle : () => console.log("cannot 0")}
                  text={<img src={Icons.happySmiley} height="22" width="22" alt="smiley" />}
                  disabled={this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe}
                />
                <Button
                  className="chat_handlers"
                  handler={this.props.chatStarted ? this.toggleStickerModal : () => console.log("cannot 1")}
                  text={<img src={Icons.sticker} height="22" width="22" alt="gift" />}
                  disabled={this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe}
                />
                <Button
                  className="chat_handlers"
                  handler={this.props.chatStarted ? this.toggleGifModal : () => console.log("cannot 2")}
                  text={<img src={Icons.gifIcon} height="22" width="22" alt="gift" />}
                  disabled={this.props.UserSelectedToChat.isBlocked || this.props.UserSelectedToChat.isBlockedByMe}
                /> */}
              </div>
            </div>
            <Button
              className="d_chat_send_btn"
              text={<span>Send</span>}
              handler={this.props.chatStarted ? this.submitMessage : () => console.log("cannot 3")}
              disabled={!this.props.chatStarted && this.state.text.length === 0}
            />
          </div>
        </div>

        {/** Gif modal when toggled clicking on [GIF] */}
        <MatUiModal
          isOpen={this.state.gifHandlerState ? this.state.gifModal : this.state.stickerModal}
          toggle={this.state.gifHandlerState ? this.toggleGifModal : this.toggleStickerModal}
          width={600}
        >
          <div className="p-12 p-5">
            <div className="col-12" style={{ position: "relative" }}>
              <div className="gif_search_input">
                <img src={Images.Search} alt="search" />
              </div>
              <input onChange={this.onChangeHandler} placeholder="Search Sticker" className="gif_input_box" />               
            </div>
            <div className="gif_scroll_screen col-12">
              <div className="row ">
                {this.state.randomGifs &&
                  this.state.randomGifs.map((k, i) => (
                    <img
                      key={k.images.fixed_width_small.url}
                      src={this.state.gifHandlerState ? k.images.fixed_width_small.url : k.images.downsized_still.url}
                      width={80}
                      alt={k.images.fixed_width_small.url}
                      height={80}
                      className="selectedImage"
                      onClick={() => this.getGifDetails(k, this.state.gifHandlerState)}
                    />
                  ))}
              </div>
            </div>
          </div>
        </MatUiModal>
        <MatUiModal isOpen={this.state.purcahseCoin} toggle={this.purchaseCoinModal} width={500}>
          <div className="col-12 text-center p-5">
            <div>
              <img src={Icons.BigHeart} height={180} width={180} alt="wallet" />
            </div>
            <h4 className="pt-3">Please purchase Credits, to continue chatting.</h4>
            <div className="pt-2 purchaseCoinButton" onClick={this.purchaseCoinModal}>
              OK
            </div>
          </div>
        </MatUiModal>
        <MatUiModal isOpen={this.props.locationmodel} width={500} type="d_sendLocationFromChat" customClass="d_sendLocationFromChat">
          <LocationHome
            sendLocation={this.props.getlocation} // send message over mqtt
            setLocationForSendingMessage={this.props.setLocationForSendingMessage} // handler to store data {lat, lng, place, full address}
            closelocationmodel={this.props.closelocationmodel}
          />
        </MatUiModal>
        <MatUiModal isOpen={this.state.openTransferCoinsModal} width={500} toggle={this.toggleTransferCoinModal}>
          <div className="col-12 p-5" style={{ position: "relative" }}>
            <div className="cancelButton" onClick={this.toggleTransferCoinModal}>
              <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
            </div>
            <div className="text-center d_chat_transfer_coins_header">Transfer Credits</div>
            <div className="d_chat_transfer_coins_name pt-3">Name</div>
            <div className="d_chat_transfer_coins_user_name pt-1">{this.props.UserSelectedToChat.firstName}</div>
            <div className="d_chat_transfer_coins_name pt-3">Coins</div>
            <div className="d_chat_transfer_input_box">
              <input
                type="number"
                onChange={this.coinsToTransferHandler}
                placeholder="Please enter the number of Credits that you want to send"
              />
            </div>
            <Button
              handler={
                this.state.coinsToTransfer.length > 0 && parseInt(this.state.coinsToTransfer) <= parseInt(this.props.coins)
                  ? this.sendCoinsFn
                  : this.transferCoinsMoneyExeedModal
              }
              className={this.state.coinsToTransfer.length > 0 ? "d_chat_transfer_confirm" : "d_chat_transfer_confirm_disabled"}
              text="Confirm"
            />
          </div>
        </MatUiModal>
        {/** After clicking on gif, this modal opens to confirm GIF and send */}
        <MatUiModal isOpen={this.state.preview} toggle={this.togglePreviewModal} width={600}>
          <div className="col-12 p-5 preview_Gif_Modal">
            <div className="col-12 text-center">
              <div>Are you sure you want to send ?</div>
            </div>
            <div className="col-12 text-center p-3">
              <img src={this.state.singleGif} width={170} height={170} alt={this.state.singleGif} />
            </div>
            <div className="col-12 previewButtonAlignment">
              <button type="button" onClick={() => this.sendGifHandler(this.state.singleGif, this.state.gifHandlerState)}>
                Yes
              </button>
              <button type="button" onClick={() => this.togglePreviewModal()}>
                No
              </button>
            </div>
          </div>
        </MatUiModal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    _UserSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
  };
};

export default connect(null, mapDispatchToProps)(Chatroom);
