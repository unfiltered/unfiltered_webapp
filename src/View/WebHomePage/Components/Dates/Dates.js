import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import "../Prospects/Prospects.scss";
import "../Chat/Chat.scss";
import "./Dates.scss";
import moment from "moment";
import MenuItem from "@material-ui/core/MenuItem";
import UpdatedPlansProUser from "../UpdatePlansProUser/UpdatePlansProUser";
import { withStyles } from "@material-ui/core/styles";
import MainModal from "../../../../Components/Model/model";
import { PastDates, PendingDates, RescheduleDate, DateRating } from "../../../../controller/auth/verification";
import Search from "../../../../asset/images/_search.png";
import { Icons } from "../../../../Components/Icons/Icons";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { getCookie } from "../../../../lib/session";
import DateCards from "./DateCards";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import Rater from "react-rater";
import "react-rater/lib/react-rater.css";
import Button from "../../../../Components/Button/Button";
import { keys } from "../../../../lib/keys";
import { FormattedMessage } from "react-intl";
import Input from "../../../../Components/Input/Input";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MATTextField from "../../../../Components/Input/TextField";
import * as actions from "../../../../actions/UserSelectedToChat";
import { __callInit } from "../../../../actions/webrtc";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";

var foursquare = require("react-foursquare")({
  clientID: keys.foursquareClientID,
  clientSecret: keys.foursquareClientSecret,
});

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "30vh",
    textAlign: "center",
  },
  textField: {
    width: "100%",
  },
  formControl: {
    width: "100%",
  },
  select: {
    fontSize: "14px",
    fontFamily: "Product Sans",
    color: "#e31b1b",
    background: "#FFF",
  },
  menuItem: {
    fontSize: "14px",
    fontFamily: "Product Sans",
    color: "#484848",
  },
});

// const Pendinglabel = <FormattedMessage id="message.tabdate1" />;
// const UpCominglabel = <FormattedMessage id="message.tabdate2" />;
// const PastDateslabel = <FormattedMessage id="message.tabdate3" />;

const Pendinglabel = "Pending";
const UpCominglabel = "Upcoming";
const PastDateslabel = "Past Dates";

function Prospects(props) {
  const [upcomingDates, SetUpcomingDates] = useState("");
  const [pendingdates, setPendingDates] = useState("");
  const [pastDates, setPastDates] = useState("");
  const [content, setContent] = useState("");
  const [label, setLabel] = useState("");
  const [filteredList, setFilteredList] = useState("");
  const [tabs, setTabs] = useState([
    { label: Pendinglabel, content: pendingdates, count: 0 },
    { label: UpCominglabel, content: upcomingDates, count: 0 },
    { label: PastDateslabel, content: pastDates, count: 0 },
  ]);
  const [modelplan, _tooglecoinmodelplan] = useState(false);
  const [modal, setModal] = useState(false);
  const [stateOfClick, setStateOfClick] = useState("");
  const [dateDetails, setDateDetails] = useState("");
  const [callDateModal, setCallDateModal] = useState(false);
  const [videoDateModal, setToggleVideoCall] = useState(false);
  const [physicalDateModal, setPhysicalDateModal] = useState(false);
  const [items, setItems] = useState([]);
  const [values, setValues] = React.useState({
    time: "",
    locationSelected: "",
  });
  const [rate, setDateRate] = useState("1");
  const [rateModal, setRateModal] = useState(false);
  const [isReschedule, setIsReschedule] = useState(false);

  const [snackbarVariant, setSnackbarVariant] = useState("success");
  const [usermessage, setUsermessage] = useState("");
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackbarOpen(false);
  };

  console.log("dateDetails", dateDetails);

  // console.log("1...........date details", dateDetails);
  /************************* */
  // console.log("[[[[ stateOfClick", stateOfClick); // that means reschedule
  /************************* */

  // fn -> to set ratings for date
  const setRatingsForDate = (rating) => {
    setDateRate(rating.rating);
  };

  // fn -> updates text value
  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
  };

  // resets time for the date
  const resetTime = () => {
    setValues({ ...values, time: "" });
  };

  // resets location set for date.
  const resetLocation = () => {
    setValues({ ...values, locationSelected: "" });
  };

  const togglePhysicalDateModal = () => {
    setPhysicalDateModal(!physicalDateModal);
  };

  const toggleVideoCall = () => {
    setToggleVideoCall(!videoDateModal);
  };

  const toggleAudioCall = () => {
    setCallDateModal(!callDateModal);
  };

  // modify the empty content and fill it with API data when component mounts
  const addApiData = (label, data) => {
    const index = tabs.findIndex((emp) => emp.label === label);
    let copiedData = [...tabs];
    copiedData[index].content = data;
    setTabs(copiedData);
  };

  // this function stores the date type
  const setStateOfClickFn = (input) => {
    setStateOfClick(input);
  };

  // toggles confirmation moda;
  const toggleConfirmationModal = () => {
    setModal(!modal);
  };

  // on click dynamically change data
  const loadData = (content, label) => {
    console.log(content, label);
    setContent(content);
    setLabel(label);
    setFilteredList(content);
  };

  // fn to reschedule date
  // type={snackbarVariant} message={usermessage} open={snackbarOpen} onClose={handleClose}
  const reschedulateDate = (response, date_id, proposedOn, dateType, lat, lng, placeName) => {
    let PLAN = props.checkIfUserIsProUser && props.checkIfUserIsProUser.ProUserDetails && props.checkIfUserIsProUser.ProUserDetails.tag;
    // console.log("rescheduleDate handler", response, date_id, proposedOn, dateType, lat, lng, placeName);

    RescheduleDate(getCookie("token"), response, date_id, proposedOn, dateType, lat, lng, placeName, PLAN)
      .then((res) => {
        if (res.status === 200) {
          if (response === 3) {
            setUsermessage("Date Successfully Rescheduled...");
            setSnackbarOpen(true);
            componentDidMount();
          }
          if (response === 2) {
            setUsermessage("Date Successfully Deleted...");
            setSnackbarOpen(true);
            componentDidMount();
          }
          if (response === 1) {
            setUsermessage("Date Accepted...");
            setSnackbarOpen(true);
            componentDidMount();
          }
        }
      })
      .catch((err) => console.log("err rescheduling date", err));
  };

  // fn to give ratings for a date and API call triggers, updates locally first.
  const dateRatingFn = (dateId, rate) => {
    let a = [...tabs];
    let b = [...a[2].content];
    const index = b.findIndex((p) => p.data_id === dateId);
    b[index]["rate"] = rate;
    a[2].content = b;
    setTabs(a);
    DateRating(getCookie("token"), dateId, rate)
      .then((res) => {
        setDateRate("0");
        console.log("[date rate success]", res);
      })
      .catch((err) => console.log("[date rate error]", err));
  };

  // toggle ratings for date
  const toggleRateModal = () => {
    setDateRate("0");
    setRateModal(!rateModal);
  };

  // API call on after date successfull set / or on component mounts
  const componentDidMount = () => {
    props.handleWebsiteActiveValue("/app/dates");
    PendingDates()
      .then((data) => {
        SetUpcomingDates(data.data.data.upcomingDates);
        addApiData(UpCominglabel, data.data.data.upcomingDates);

        // second api
        setPendingDates(data.data.data.pendingDates);
        addApiData(Pendinglabel, data.data.data.pendingDates);
        loadData(data.data.data.pendingDates, Pendinglabel);
        filteredList(data.data.data.pendingDates);
      })
      .catch((err) => {
        SetUpcomingDates([]);
      });
    PastDates().then((data) => {
      setPastDates(data.data.data);
      addApiData(PastDateslabel, data.data.data);
      // loadData(data.data.data, PastDateslabel);
    });
  };

  // componentDidMount
  useEffect(() => {
    let params;
    if (getCookie("selectedLocation") != null) {
      let obj = JSON.parse(getCookie("selectedLocation"));
      params = {
        ll: `${obj.lat}, ${obj.lng}`,
        query: "",
      };
    } else if (getCookie("lat") != null && getCookie("long") != null) {
      params = {
        ll: `${getCookie("lat")}, ${getCookie("long")}`,
        query: "",
      };
    } else {
      if (getCookie("baseAddress") != null) {
        params = {
          ll: `${getCookie("baseAddress").lat}, ${getCookie("baseAddress").lng}`,
          query: "",
        };
      }
    }
    foursquare.venues.getVenues(params).then((res) => {
      console.log("[4SQUARE]", res);
      setItems(res && res.response && res.response.venues);
    });
    props.renderBgColorBasedOnLink(window.location.pathname);
    // Online Users API
    componentDidMount();
  }, []);

  const tooglecoinmodelplan = () => {
    _tooglecoinmodelplan(!modelplan);
  };

  // filter users
  const filterList = (event) => {
    var updatedList = content;
    updatedList = updatedList.filter((item) => item.opponentName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
    setFilteredList(updatedList);
  };

  // reusable function to set date <API Call> based on selection of date type [1,2,3]
  let executeAPI = (inputText) => {
    console.log("#### inputText", inputText);
    console.log("#### dateDetails", dateDetails);
    if (inputText === 1) {
      console.log("execute API 1");
      reschedulateDate(
        1,
        dateDetails.data_id,
        dateDetails.proposedOn,
        renderDateTypeBasedOnInputData(dateDetails.requestedFor),
        dateDetails.latitude,
        dateDetails.longitude,
        dateDetails.placeName
      );
      toggleConfirmationModal();
      // toggleAudioCall();
      // componentDidMount();
    } else if (inputText === 2) {
      console.log("execute API 2");
      reschedulateDate(
        2,
        dateDetails.data_id,
        dateDetails.proposedOn,
        renderDateTypeBasedOnInputData(dateDetails.requestedFor),
        dateDetails.latitude,
        dateDetails.longitude,
        dateDetails.placeName
      );
      toggleConfirmationModal();
      // componentDidMount();
    } else if (inputText === 3) {
      console.log("execute API 3");
      toggleConfirmationModal();
      if (dateDetails.dateType === 1 || dateDetails.requestedFor.includes("video")) {
        console.log("execute API 3-1");
        toggleVideoCall();
      } else if (dateDetails.dateType === 2 || dateDetails.requestedFor.includes("phy")) {
        console.log("execute API 3-2");
        togglePhysicalDateModal();
      } else if (dateDetails.dateType === 3 || dateDetails.requestedFor.includes("audio")) {
        console.log("execute API 3-3");
        toggleAudioCall();
      }
      console.log("execute API 4");
      // if (stateOfClick === 1) {
      //   toggleVideoCall();
      // } else if (stateOfClick === 2) {
      //   toggleAudioCall();
      // } else if (stateOfClick === 3) {
      //   togglePhysicalDateModal();
      // }
    }
  };

  // renders text based on if accepted/rejected or reschedule
  const renderText = (number) => {
    if (number === 1) {
      return "Accept";
    } else if (number === 2) {
      return "Reject";
    } else if (number === 3) {
      return "Reschedule";
    }
  };

  const renderDateTypeBasedOnInputData = (data) => {
    console.log("renderDateTypeBasedOnInputData", data);
    if (data.includes("video") || data.includes("Video")) {
      return 1;
    } else if (data.includes("audio") || data.includes("Audio")) {
      return 3;
    } else if (data.includes("phi") || data.includes("Phi")) {
      return 2;
    }
  };

  const webrtcCallingDispatchCorrectType = (data) => {
    console.log("%c WEB CALL INIT", "background: #e31b1b; color: #FFFFFF; font-size: 18px");
    if (data.requestedFor.includes("Audio") || data.requestedFor.includes("audio") || data.dateType === 3 || data.dateType === "3") {
      console.log("%c YES AUDIO", "background: #FC6404; color: #FFFFFF; font-size: 18px");
      return "0";
    } else {
      console.log("%c YES VIDEO", "background: #ACDCE4; color: #FFFFFF; font-size: 18px");
      return "1";
    }
  };

  const callInitiate = (data) => {
    let obj = {};
    obj["dateId"] = data.data_id;
    obj["recipientId"] = data.opponentId;
    obj["UserProfileName"] = props.UserProfile.UserProfileName;
    obj["userId"] = getCookie("uid");
    obj["callerImage"] = data.opponentProfilePic;
    obj["callerIdentifier"] = data.opponentId;
    obj["type"] = 0;
    obj["callType"] = webrtcCallingDispatchCorrectType(data);
    obj["requestedFor"] = webrtcCallingDispatchCorrectType(data);
    obj["isLoggedInUserCalling"] = true;
    props.userSelectedToChat(obj);
    props.selectedProfileForChat(obj);
    props.__callInit(true);
  };

  const { classes } = props;
  // console.log("values time", values.time, moment(values.time).valueOf());
  return (
    <div id="innerVideoContent">
      <div className="col-12 custom_border">
        <div className="row">
          <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 py-3">
            <div>
              <h3 className="headerName">
                <FormattedMessage id="UnFiltered Dates" />
              </h3>
              <div className="subHeaderName">
                <h5 className="mb-0">Live Your Best Life Now</h5>
              </div>
            </div>
          </div>
          <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 py-3 custom_border_left">
            <div className="row h-100 align-items-center">
              <div className="col-sm-6 col-md-8 col-lg-8 col-xl-8"></div>
              <div className="col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <form className="search_user">
                  <div class="form-group mb-0 ChatPage_FilterUser_Search">
                    <div className="magnifyingGlass_prospects">
                      <img src={Search} alt="search" />
                    </div>
                    <Input type="text" className="form-control" onChange={(e) => filterList(e)} placeholder="Search" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      {props.checkIfUserIsProUser.ProUserDetails.status !== "Free Plan" ? (
        <div className="col-12 px-0">
          <div className="row mx-0">
            <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 px-0 prospects_tabs">
              {tabs.map((k) => (
                <div
                  className={label === k.label ? "options_selected" : "options"}
                  key={k.label}
                  onClick={() => loadData(k.content, k.label)}
                >
                  {k.label} <span className="desktop_dates_count">{k.content.length}</span>
                </div>
              ))}
            </div>
            <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 custom_border_left px-0 prospects_scrollbar d-flex flex-wrap">
              {/* <div className="d-flex"> */}
              {filteredList.length > 0 ? (
                filteredList.map((k) => (
                  <DateCards
                    setStateOfClickFn={setStateOfClickFn}
                    setDateDetails={setDateDetails}
                    toggleConfirmationModal={toggleConfirmationModal}
                    k={k}
                    toggleRateModal={toggleRateModal}
                    label={label}
                    callInitiate={callInitiate}
                    setIsReschedule={setIsReschedule}
                    componentDidMount={componentDidMount}
                  />
                ))
              ) : (
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                  <div className="Wcommon_nodata">
                    <img src={Icons.NoUsers} height={300} alt="NoData" title="NoData" />
                    <h5 className="py-3 text-muted">
                      <FormattedMessage id="message.noDates" />.
                    </h5>
                  </div>
                </div>
              )}
              {/* </div> */}
            </div>
          </div>
        </div>
      ) : (
        <MainModal isOpen={modelplan} toggle={tooglecoinmodelplan}>
          <UpdatedPlansProUser openModal={modelplan} toggleModal={tooglecoinmodelplan} />
        </MainModal>
      )}
      <MatUiModal width={500} isOpen={rateModal} toggle={toggleRateModal}>
        <div className="col-12 p-4" style={{ position: "relative" }}>
          <div className="d_newsFeed_modal_close_btn" onClick={toggleRateModal}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="d_friends_modal_header text-center">Rate The Date</div>
          <div className="d-flex justify-content-around align-items-center py-4">
            <Rater total={5} rating={rate} interactive={true} onRate={setRatingsForDate} />
          </div>
          <div className="d-flex justify-content-around align-items-center py-4">
            <Button
              handler={() => {
                dateRatingFn(dateDetails.data_id, rate);
                toggleRateModal();
              }}
              text="Submit"
              className="sumbit_ratings_handler"
            />
          </div>
        </div>
      </MatUiModal>
      <MatUiModal
        width={550}
        isOpen={modal}
        toggle={() => {
          setIsReschedule(false);
          toggleConfirmationModal();
        }}
      >
        <div className="col-12 p-4" style={{ position: "relative" }}>
          <div
            className="d_newsFeed_modal_close_btn"
            onClick={() => {
              setIsReschedule(false);
              toggleConfirmationModal();
            }}
          >
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="d_friends_modal_header text-center">
            Are you sure you want to {renderText(stateOfClick)} the date{" "}
            {isReschedule ? `for ${props.CoinConfig && props.CoinConfig.Coin} Credits` : ""}
          </div>
          <div className="d-flex justify-content-around py-4">
            <Button
              handler={() => {
                console.log("reschedule date clicked");
                executeAPI(stateOfClick);
              }}
              className="modal_confirm_buttons"
              text={<FormattedMessage id="message.yes" />}
            />
            <Button handler={toggleConfirmationModal} className="modal_confirm_buttons" text={<FormattedMessage id="message.no" />} />
          </div>
        </div>
      </MatUiModal>
      <MatUiModal isOpen={callDateModal} toggle={toggleAudioCall} width={500}>
        <div className="col-12 p-5" style={{ position: "relative" }}>
          <div className="cancelButton" onClick={toggleAudioCall}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="physicalDateModal_header text-center">
            <FormattedMessage id="message.planAudioCallDate" />
          </div>
          <div className="physicalDateModal_text pt-2 px-5 text-center">
            <FormattedMessage id="message.suggestPlaceAndTime" />
            {dateDetails.firstName} <FormattedMessage id="message.confirm" />.
          </div>
          <div className="col-12 d-flex py-3 d_physical_couples_img">
            <div>
              <img src={props.UserProfile.UserProfilePic} className="d_physical_date_img" alt={props.UserProfile.UserProfilePic} />
            </div>
            <div>
              <img src={dateDetails.opponentProfilePic} className="d_physical_date_img" alt={dateDetails.profilePic} />
            </div>
          </div>
          <div className="physicalDateModal_cat_text pt-3">
            <FormattedMessage id="message.suggestTime" />
          </div>
          <div>
            <MATTextField
              id="datetime-local"
              type="datetime-local"
              onChange={handleChange("time")}
              defaultValue={moment(dateDetails.proposedOn).format("YYYY-MM-DDTHH:mm")}
              className={classes.textField}
            />
          </div>
          <Button
            className="dateConfirm"
            handler={() => {
              reschedulateDate(
                3,
                dateDetails.data_id,
                moment(values.time).valueOf(),
                renderDateTypeBasedOnInputData(dateDetails.requestedFor),
                getCookie("lat"),
                getCookie("lng"),
                dateDetails.placeName
              );
              componentDidMount();
              toggleAudioCall();
              resetTime();
            }}
            text={<FormattedMessage id="message.confirm" />}
          />
        </div>
      </MatUiModal>
      <MatUiModal isOpen={physicalDateModal} toggle={togglePhysicalDateModal} width={500}>
        <div className="col-12 p-5" style={{ position: "relative" }}>
          <div className="cancelButton" onClick={togglePhysicalDateModal}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="physicalDateModal_header text-center">
            <FormattedMessage id="message.inPersonDate" />
          </div>
          <div className="physicalDateModal_text pt-2 px-5 text-center">
            <FormattedMessage id="message.suggestPlaceAndTime" />
            {dateDetails.firstName} <FormattedMessage id="message.confirm" />.
          </div>
          <div className="col-12 d-flex py-3 d_physical_couples_img">
            <div>
              <img src={props.UserProfile.UserProfilePic} className="d_physical_date_img" alt={props.UserProfile.UserProfilePic} />
            </div>
            <div>
              <img src={dateDetails.opponentProfilePic} className="d_physical_date_img" alt={dateDetails.profilePic} />
            </div>
          </div>
          <div className="physicalDateModal_cat_text mt-4 mb-1">
            <FormattedMessage id="message.suggestPlace" />
          </div>
          {items && items.length > 0 ? (
            <FormControl className={classes.formControl}>
              <Select value={values.locationSelected} onChange={handleChange("locationSelected")} className={classes.select}>
                {items &&
                  items.map((k, i) => (
                    <MenuItem value={k.name} className={classes.menuItem} key={i}>
                      {k.name}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
          ) : (
            // <div>Your Location does not have </div>
            ""
          )}

          <div className="physicalDateModal_cat_text pt-3">
            <FormattedMessage id="message.suggestTime" />
          </div>
          <div>
            <MATTextField
              id="datetime-local"
              type="datetime-local"
              onChange={handleChange("time")}
              defaultValue={moment(dateDetails.proposedOn).format("YYYY-MM-DDTHH:mm")}
              className={classes.textField}
            />
          </div>
          <Button
            className="dateConfirm"
            text={<FormattedMessage id="message.confirm" />}
            handler={() => {
              reschedulateDate(
                3,
                dateDetails.data_id,
                moment(values.time).valueOf(),
                renderDateTypeBasedOnInputData(dateDetails.requestedFor),
                dateDetails.latitude,
                dateDetails.longitude,
                values.locationSelected ? values.locationSelected : dateDetails.placeName
              );
              componentDidMount();
              togglePhysicalDateModal();
              resetTime();
              resetLocation();
            }}
          />
        </div>
      </MatUiModal>
      <MatUiModal isOpen={videoDateModal} toggle={toggleVideoCall} width={500}>
        <div className="col-12 p-5" style={{ position: "relative" }}>
          <div className="cancelButton" onClick={toggleVideoCall}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="physicalDateModal_header text-center">
            <FormattedMessage id="message.planVideoCallDate" />
          </div>
          <div className="physicalDateModal_text pt-2 px-5 text-center">
            <FormattedMessage id="message.suggestPlaceAndTime" />
            {dateDetails.firstName} <FormattedMessage id="message.confirm" />.
          </div>
          <div className="col-12 d-flex py-3 d_physical_couples_img">
            <div>
              <img src={props.UserProfile.UserProfilePic} className="d_physical_date_img" alt={props.UserProfile.UserProfilePic} />
            </div>
            <div>
              <img src={dateDetails.opponentProfilePic} className="d_physical_date_img" alt={dateDetails.profilePic} />
            </div>
          </div>
          <div className="physicalDateModal_cat_text pt-3">
            <FormattedMessage id="message.suggestTime" />
          </div>
          <div>
            <MATTextField
              type="datetime-local"
              onChange={handleChange("time")}
              defaultValue={moment(dateDetails.proposedOn).format("YYYY-MM-DDTHH:mm")}
              className={classes.textField}
            />
          </div>
          <Button
            className="dateConfirm"
            text={<FormattedMessage id="message.confirm" />}
            handler={() => {
              reschedulateDate(
                3,
                dateDetails.data_id,
                moment(values.time).valueOf(),
                renderDateTypeBasedOnInputData(dateDetails.requestedFor),
                dateDetails.latitude,
                dateDetails.longitude,
                dateDetails.placeName
              );
              componentDidMount();
              toggleVideoCall();
              resetTime();
            }}
          />
        </div>
      </MatUiModal>
      <Snackbar timeout={2500} type={snackbarVariant} message={usermessage} open={snackbarOpen} onClose={handleClose} />
    </div>
  );
}

function mapstateToProps(state) {
  return {
    CoinConfig: state.CoinConfig.CoinConfig.resheduleDate,
    UserProfile: state.UserProfile.UserProfile,
    checkIfUserIsProUser: state.ProUser,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
    __callInit: (data) => dispatch(__callInit(data)),
  };
}

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(Prospects));
