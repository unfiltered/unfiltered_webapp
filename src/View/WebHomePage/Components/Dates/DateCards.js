import React from "react";
import moment from "moment";
import { Icons } from "../../../../Components/Icons/Icons";
import { Images } from "../../../../Components/Images/Images";
import Button from "../../../../Components/Button/Button";
import Rater from "react-rater";
import "react-rater/lib/react-rater.css";

const DateCards = ({
  k,
  label,
  setStateOfClickFn,
  setDateDetails,
  toggleConfirmationModal,
  toggleRateModal,
  setIsReschedule,
  componentDidMount,
  callInitiate,
}) => {
  const onError = (e) => {
    e.target.src = Images.placeholder;
  };
  if (label === "Pending") {
    return (
      <div key={k.createdTimestamp} className="dates_card_pending">
        <div>
          <img src={k.opponentProfilePic} alt={k.opponentProfilePic} onError={onError} />
        </div>
        <div>
        {k.isInitiatedByMe ? <span>{k.requestedFor} request sent to <span style={{ textTransform: "capitalize" }} />{k.opponentName}</span> : <span>Do you want to confirm date with <span style={{ textTransform: "capitalize" }} />{k.opponentName}</span>} 
        </div>
        <div>{moment(k.proposedOn).format("ddd MMM DD @ hh:mm A")}</div>
        <div>
          {k.isInitiatedByMe ? (
            <div className="d-flex date_options_pending justify-content-between">
              <div
                className="date_reschedule_I_pending"
                onClick={() => {
                  console.log("resch init");
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(3);
                  setIsReschedule(true);
                  // componentDidMount();
                }}
              >
                Reschedule
              </div>
              <div
                className="date_reject_I"
                onClick={() => {
                  console.log("delete clicked");
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(2);
                  componentDidMount();
                }}
              >
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div>
          ) : (
            <div className="d-flex justify-content-between date_options_pending">
              <div
                className="date_reschedule_non_init_pending"
                onClick={() => {
                  console.log("resch non init");
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(3);
                  // componentDidMount();
                }}
              >
                Reschedule
              </div>
              <div
                className="date_accept_non_init_pending"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(1);
                  // componentDidMount();
                }}
              >
                <img src={Icons.acceptDate} alt="thrash" width={20} height={20} />
              </div>
              <div
                className="date_reject_init_pending"
                onClick={() => {
                  console.log("delete clicked");
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(2);
                  // componentDidMount();
                }}
              >
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div>
          )}
        </div>
        <div>{k.placeName}</div>
        <div>{k.requestedFor}</div>
      </div>
    );
  } else if (label === "Up Coming") {
    return (
      <div key={k.createdTimestamp} className="dates_card">
        <div>
          <img src={k.opponentProfilePic} alt={k.opponentProfilePic} onError={onError} />
        </div>
        <div style={{ textTransform: "capitalize" }}>Date confirmed with {k.opponentName}</div>
        <div>{moment(k.proposedOn).format("ddd MMM DD @ hh:mm A")}</div>
        <div>
          {k.isInitiatedByMe ? (
            <div className="d-flex date_options justify-content-between">
              <Button
                className={new Date().getTime() < parseInt(k.proposedOn) ? "date_rescheduleV3-disabled" : "date_rescheduleV3"}
                disabled={new Date().getTime() < parseInt(k.proposedOn)}
                text={k.requestedFor}
                handler={() => {
                  setDateDetails(k);
                  callInitiate(k);
                }}
              />
              <div
                className="date_rescheduleV3"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(3);
                  setIsReschedule(true);
                  // componentDidMount();
                }}
              >
                Reschedule
              </div>
              <div
                className="upcoming_date_reject"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(2);
                  // componentDidMount();
                }}
              >
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div>
          ) : (
            <div className="d-flex justify-content-between date_options">
              <Button
                className={new Date().getTime() < parseInt(k.proposedOn) ? "date_rescheduleV3-disabled" : "date_rescheduleV3"}
                disabled={new Date().getTime() < parseInt(k.proposedOn)}
                text={k.requestedFor}
                handler={() => {
                  setDateDetails(k);
                  callInitiate(k);
                }}
              />

              <div
                className="date_rescheduleV3"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(3);
                  setIsReschedule(true);
                  // componentDidMount();
                }}
              >
                Reschedule
              </div>
              <div
                className="upcoming_date_reject"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(2);
                  // componentDidMount();
                }}
              >
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div>
          )}
        </div>
        <div style={{ textTransform: "capitalize" }}>{k.placeName}</div>
        <div style={{ textTransform: "capitalize" }}>{k.requestedFor}</div>
      </div>
    );
  } else if (label === "Past Dates") {
    return (
      <div key={k.createdTimestamp} className="dates_card">
        <div>
          <img src={k.opponentProfilePic} alt={k.opponentProfilePic} onError={onError} />
        </div>
        <div style={{ textTransform: "capitalize" }}>
          {k.status} with {k.opponentName}
        </div>
        <div>{moment(k.proposedOn).format("ddd MMM DD @ hh:mm A")}</div>
        <div>
          {/* {k.isInitiatedByMe ? (
            <div className="d-flex date_options justify-content-between">
              <div className="date_reschedule_I">Reschedule</div>
              <div className="date_reject_I">
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div>
          ) : (
            <div className="d-flex justify-content-between date_options">
              <div
                className="date_reschedule"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(3);
                }}
              >
                Reschedule
              </div>
              <div
                className="date_accept"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(1);
                }}
              >
                <img src={Icons.acceptDate} alt="thrash" width={20} height={20} />
              </div>
              <div
                className="date_reject"
                onClick={() => {
                  setDateDetails(k);
                  toggleConfirmationModal();
                  setStateOfClickFn(2);
                }}
              >
                <img src={Icons.deleteDate} alt="thrash" width={20} height={20} />
              </div>
            </div> */}
          {/* )} */}
        </div>
        <div>{k.placeName}</div>
        <div>{k.requestedFor}</div>
        {k && k.rate ? (
          <div className="show_ratings">
            <Rater total={5} interactive={false} rating={k.rate} />
          </div>
        ) : (
          <Button
            text="Submit Ratings"
            className="rate_dating"
            handler={() => {
              setDateDetails(k);
              toggleRateModal();
            }}
          />
        )}
      </div>
    );
  }
};

export default DateCards;
