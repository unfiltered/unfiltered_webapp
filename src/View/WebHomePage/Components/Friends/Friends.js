import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import "../Prospects/Prospects.scss";
import "../Chat/Chat.scss";
import "../Dates/Dates.scss";
import { getCookie } from "../../../../lib/session";
import { withStyles } from "@material-ui/core/styles";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import {
  GetAllFriendsRequest,
  GetAllFriends,
  findById,
  sendRequestToFriendById,
  sendRequestToFriendByName,
  acceptOrRejectFriendRequest,
} from "../../../../controller/auth/verification";
import ProspectsCard from "../Prospects/ProspectsCard";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import { FormattedMessage } from "react-intl";
import Input from "../../../../Components/Input/Input";
import { Images } from "../../../../Components/Images/Images";
import * as actions from "../../../../actions/UserSelectedToChat";
import CircularProgress from "@material-ui/core/CircularProgress";
import { FRIEND_REQUEST_COUNT_FUNC } from "../../../../actions/Friends";
import { keys } from "../../../../lib/keys";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "30vh",
    textAlign: "center",
  },
});

// const AllFriendsLabel = <FormattedMessage id="message.sidepanel_friends" />;
// const RequestsLabel = <FormattedMessage id="message.friends_requests" />;

const AllFriendsLabel = "Friends";
const RequestsLabel = "Requests";

function Friends(props) {
  const [allFriends, setAllFriends] = useState("");
  const [allRequests, setAllRequests] = useState("");
  const [content, setContent] = useState("");
  const [label, setLabel] = useState("");
  const [filteredList, setFilteredList] = useState("");
  const [tabs, setTabs] = useState([
    { label: AllFriendsLabel, content: allFriends },
    { label: RequestsLabel, content: allRequests },
  ]);
  const [loader, setLoader] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [name, setName] = useState("");
  const [foundData, setFoundData] = useState("Sorry, there are no results by that ID");
  const [foundBool, setFoundBool] = useState(false);
  const [friendRequestAccepted, setToggleFriendRequest] = useState(false);
  const [selectedUserData, setSelectedUserData] = useState("");
  const [acceptedFriend, setAcceptedFriend] = useState(false);
  const [textToRenderOnInit, setTextToRenderOnInit] = useState("Search In Friends");
  const [friendSearch, setFriendSearch] = useState(false);

  const [firstName, setFirstName] = useState("");
  const [storeSearchByFriendData, setStoreSearchByFriendData] = useState([]);
  const [searchByFriendModal, setSearchByFriendModal] = useState(false);

  useEffect(() => {}, [foundData]);
  useEffect(() => {}, [tabs]);
  useEffect(() => {
    if (props.FriendRequests) {
      let a = [...tabs];
      let filtered = [...filteredList];
      let b = [...a[1].content];
      if (label !== RequestsLabel) {
        // filtered.push(props.FriendRequests);
        b.push(props.FriendRequests);
        a[1].content = b;
        setTabs(a);
      } else {
        filtered.push(props.FriendRequests);
        b.push(props.FriendRequests);
        a[1].content = b;
        setTabs(a);
      }
      // a[1].content = b;
      // setTabs(a);
      setFilteredList(filtered);
    }
  }, [props.FriendRequests]);

  // to refresh users list after acccepting friend.
  useEffect(() => {
    // if (acceptedFriend) {
    GetAllFriends().then((data) => {
      setAllFriends(data.data.data);
      addApiData(AllFriendsLabel, data.data.data);
      loadData(data.data.data, AllFriendsLabel);
      setLoader(false);
    });
    setAcceptedFriend(false);
    // }
  }, [acceptedFriend]);

  // modify the empty content and fill it with API data when component mounts
  const addApiData = (label, data) => {
    const index = tabs.findIndex((emp) => emp.label === label);
    let copiedData = [...tabs];
    copiedData[index].content = data;
    setTabs(copiedData);
  };

  // fn -> to search user by CME ID
  const searchByInput = () => {
    setFriendSearch(true);
    findById(getCookie("token"), name)
      .then((res) => {
        console.log("search friend", res);
        setFoundBool(!foundBool);
        let response = res.data.data;
        if (Object.keys(response).length > 1) {
          setFriendSearch(false);
          setFoundData(res.data.data);
        } else {
          setFoundData("Sorry, there are no results by that ID");
          setFriendSearch(false);
        }
      })
      .catch((err) => {
        setFriendSearch(false);
      });
  };

  // toggles friend request modal
  const toggleFriendRequestAccepted = () => {
    setToggleFriendRequest(!friendRequestAccepted);
  };

  // after accepting friend request, API triggers and sets focus on friends tab
  const afterAcceptRequestNavToFriends = () => {
    toggleFriendRequestAccepted();
    loadData(tabs[0].content, AllFriendsLabel);
  };

  // sends request to target user -> API call gets triggered and closes active modal
  const sendRequest = (friendId, type) => {
    if (type === 1) {
      let data = { ...foundData };
      sendRequestToFriendById(getCookie("token"), friendId)
        .then((res) => {
          data["isPenddingFriendRequest"] = true;
          setFoundData(data);
          console.log("[sendRequest] res", res);
        })
        .catch((err) => {
          console.log("[sendRequest] err 1", err);
        });
    } else {
      let data = [...storeSearchByFriendData];
      let index = data.findIndex((k) => k._id === friendId);
      data[index]["isPenddingFriendRequest"] = true;
      sendRequestToFriendById(getCookie("token"), friendId)
        .then((res) => {
          data["isPenddingFriendRequest"] = true;
          setStoreSearchByFriendData(data);
          console.log("[sendRequest] res", res);
        })
        .catch((err) => {
          console.log("[sendRequest] err 2", err);
        });
    }
  };

  // toggles modal and removes all data (add friend modal)
  const toggleModal = () => {
    setName("");
    setFoundData("");
    setOpenModal(!openModal);
  };

  // event handler for text
  const eventHandler = (e) => {
    setName(e.target.value);
  };

  // on click dynamically change data
  const loadData = (content, label) => {
    setContent(content);
    setLabel(label);
    setFilteredList(content);
    label === AllFriendsLabel ? setTextToRenderOnInit("Search In Friends") : setTextToRenderOnInit("Search In Requests");
  };

  // componentDidMount
  useEffect(() => {
    props.handleWebsiteActiveValue("/app/friends");
    props.renderBgColorBasedOnLink(window.location.pathname);
    // Online Users API
    GetAllFriends().then((data) => {
      setAllFriends(data.data.data);
      addApiData(AllFriendsLabel, data.data.data);
      loadData(data.data.data, AllFriendsLabel);
      setLoader(false);
    });
    GetAllFriendsRequest().then((data) => {
      setAllRequests(data.data.data);
      addApiData(RequestsLabel, data.data.data);
      // loadData(data.data.data, RequestsLabel);
      // setLoader(false);
    });
  }, []);

  // filter users
  // const filterList = (event) => {
  //   var updatedList = content;
  //   updatedList = updatedList.filter((item) => item.firstName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
  //   setFilteredList(updatedList);
  // };

  // fn -> to accept or reject friend request, API triggers on this fn
  const acceptOrRejectReq = (id, code, index) => {
    let a = [...tabs];
    let filtered = [...filteredList];
    let b = [...a[1].content];
    filtered.splice(index, 1);
    b.splice(index, 1);
    a[1].content = b;
    setTabs(a);
    setFilteredList(filtered);
    acceptOrRejectFriendRequest(getCookie("token"), id, code)
      .then((res) => {
        console.log("[acceptOrRejectReq] res", res);
      })
      .catch((err) => console.log("[acceptOrRejectReq] err", err));
  };

  const hitFindUserApi = (event) => {
    event.preventDefault();

    sendRequestToFriendByName(getCookie("token"), firstName)
      .then((res) => {
        if (res.data.data.length > 0) {
          setStoreSearchByFriendData(res.data.data);
          setSearchByFriendModal(true);
        }
        console.log("success finding friend", res.data.data);
      })
      .catch((err) => console.log("failed finding friend", err));
  };

  const inputToRender = (input) => {
    if (typeof input === "object") {
      return (
        <div className="col-12 px-0">
          <div className="row mx-0 align-items-center">
            <div className="col-2">
              <img src={input.profilePic} alt={input.firstName} className="d_friends_modal_add_friend_img" />
            </div>
            <div className="col-7 text-left d_friends_modal_add_friend_details">
              <div>{input.firstName}</div>
              <div>{input.country}</div>
              <div>{input.findMateId}</div>
            </div>
            <Button
              className="col-3 d_friends_modal_add_friend"
              handler={() => (searchByFriendModal ? sendRequest(input._id, 2) : sendRequest(input._id, 1))}
              text={!input.isPenddingFriendRequest ? "Add" : "Request Sent"}
            />
          </div>
        </div>
      );
    } else {
      return <div>{input}</div>;
    }
  };

  const { classes } = props;
  return (
    <div id="innerVideoContent">
      <div className="col-12 custom_border">
        <div className="row h-100 align-items-center">
          <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 py-3 page_header">
            <div>
              <h3 className="headerName">
                <FormattedMessage id="UnFiltered Friends" />
              </h3>
              <div className="subHeaderName">
                <h5 className="mb-0">Meet Real People</h5>
              </div>
            </div>
          </div>
          <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 py-3 custom_border_left">
            <div className="row">
              <div className="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <form className="search_user">
                  <div class="form-group mb-0 ChatPage_FilterUser_Search">
                    <div className="magnifyingGlass_prospects" onClick={hitFindUserApi}>
                      <img src={Images.friendsSearch} alt="search" />
                    </div>
                    <form onSubmit={hitFindUserApi}>
                      <Input
                        onChange={(e) => setFirstName(e.target.value)}
                        type="text"
                        className="form-control"
                        placeholder={textToRenderOnInit}
                      />
                    </form>
                  </div>
                </form>
              </div>
              <div className="col-sm-3 col-md-3 col-lg-4 col-xl-5"></div>
              <div className="col-sm-5 col-md-5 col-lg-4 col-xl-3 text-center" onClick={toggleModal}>
                <Button text={<FormattedMessage id="message.friends_addFriend" />} handler={toggleModal} className="d_friends_add_friend" />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 px-0">
        <div className="row mx-0">
          <div className="col-sm-2 col-md-3 col-lg-3 col-xl-3 px-0 prospects_tabs">
            {tabs.map((k) => (
              <div
                className={label === k.label ? "options_selected" : "options"}
                key={k.label}
                onClick={() => loadData(k.content, k.label)}
              >
                {k.label} <span className="desktop_dates_count">{k.content.length}</span>
              </div>
            ))}
          </div>
          <div className="col-sm-10 col-md-9 col-lg-9 col-xl-9 custom_border_left px-0 prospects_scrollbar">
            <div className="px-3">
              <ProspectsCard
                justViewProfileActive={props.justViewProfileActive}
                toggleFriendRequestAccepted={toggleFriendRequestAccepted}
                setAcceptedFriend={setAcceptedFriend}
                data={filteredList}
                setSelectedUserData={setSelectedUserData}
                acceptOrRejectReq={acceptOrRejectReq}
                loader={loader}
                storeFriendRequestCount={props.storeFriendRequestCount}
                friendReqCount={props.friendReqCount}
                classes={classes.progress}
                type="friends"
                label={label}
              />
            </div>
          </div>
        </div>
      </div>
      <MatUiModal
        isOpen={searchByFriendModal}
        width={500}
        toggle={() => {
          setSearchByFriendModal(false);
          sendRequestToFriendById([]);
        }}
      >
        <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
          <div
            className="referralModalCancelButton"
            onClick={() => {
              setSearchByFriendModal(false);
              sendRequestToFriendById([]);
            }}
          >
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="d_friends_modal_header">{storeSearchByFriendData.length} Users Found</div>
          {storeSearchByFriendData.map((k) => (
            <div key={k._id} className="d_friends_modal_data_found">
              {inputToRender(k)}
            </div>
          ))}
        </div>
      </MatUiModal>
      <MatUiModal isOpen={friendRequestAccepted} width={500} toggle={toggleFriendRequestAccepted}>
        <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
          <div className="referralModalCancelButton" onClick={toggleFriendRequestAccepted}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="d_friends_modal_header">Friend Request Confirmed</div>
          <div className="col-12 d-flex py-3 d_physical_couples_img">
            <div>
              <img src={props.UserProfile.UserProfilePic} alt={props.UserProfile.UserProfilePic} className="d_physical_date_img" />
            </div>
            <div>
              <img src={selectedUserData} alt="physical-date" className="d_physical_date_img" />
            </div>
          </div>
          <div className="d_friends_modal_request_confirmed_with">Request confirmed with </div>
          <Button
            className="text-center d_friends_modal_accepted_friend"
            handler={() => afterAcceptRequestNavToFriends()}
            text="View Friends"
          />
        </div>
      </MatUiModal>
      <MatUiModal isOpen={openModal} width={500} toggle={toggleModal}>
        <div className="col-12 p-4 text-center" style={{ position: "relative" }}>
          <div className="referralModalCancelButton" onClick={toggleModal}>
            <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
          </div>
          <div className="d_friends_modal_header">Search by {keys.AppName} ID</div>
          <div classname="d_friends_modal_input">
            <div className="d_friends_friend_search" onClick={() => searchByInput()}>
              <img src={Images.friendsSearch} alt="search" />
            </div>
            <Input className="d_friends_modal_inputHandler" onChange={eventHandler} placeholder="john.doe123" />
          </div>
          {friendSearch ? (
            <div className="pt-3">
              <CircularProgress size={20} /> <span className="pl-2"> Finding User...</span>
            </div>
          ) : foundBool && foundData ? (
            <div className="d_friends_modal_data_found">{inputToRender(foundData)}</div>
          ) : (
            ""
          )}

          <div className="text-center d_friends_modal_my_id">
            My {keys.AppName} ID <span>{props.UserProfile.UserProfileFindMateId}</span>
          </div>
        </div>
      </MatUiModal>
    </div>
  );
}

function mapstateToProps(state) {
  return {
    FriendRequests: state.mqttData.mqttData,
    UserProfile: state.UserProfile.UserProfile,
    checkIfUserIsProUser: state.ProUser,
    friendReqCount: state.Friends.FriendRequest,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    justViewProfileActive: () => dispatch(actions.justViewProfileActive()),
    storeFriendRequestCount: (count) => dispatch(FRIEND_REQUEST_COUNT_FUNC(count)),
  };
}

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(Friends));
