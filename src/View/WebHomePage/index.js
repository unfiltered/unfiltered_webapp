// Main React Components
import React, { Component } from "react";
import userAction from "../../actions/User";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import { getCookie } from "../../lib/session";

const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
    };
  }

  componentDidMount() {
    this.props.setLocation(getCookie("UserCurrentCity"));
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 2500);
  }

  render() {
    return <div />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLocation: (loc) => dispatch(userAction.setDefaultLocation(loc)),
  };
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(Index));
