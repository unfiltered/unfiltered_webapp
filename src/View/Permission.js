import React from "react";
import { Paper, Button, Typography } from "@material-ui/core";
// import { askForPermissioToReceiveNotifications } from "../init-fcm";

function Permission() {
  const [values, setValues] = React.useState({
    permission: !!localStorage.tokenDevice,
    asking: false,
  });

  const callbackPermission = () => {
    setValues({ ...values, permission: true, asking: false });
  };

  const handleButtonClick = () => {
    console.log("handleButtonClick");
    setValues({ ...values, asking: true });
    // askForPermissioToReceiveNotifications(callbackPermission);
  };

  const labelButton = () => {
    const { permission, asking } = values;
    if (asking) return "Wait...";
    else if (permission) return "It's ready!";
    else return "Click Me!";
  };

  return (
    <div>
      <Paper elevation={3} className="m_permission">
        <Button disabled={values.permission || values.asking} onClick={handleButtonClick} variant="contained" color="primary">
          {labelButton()}
        </Button>
      </Paper>
    </div>
  );
}

export default Permission;
