// Main React Components
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

// Scss
import "./Index.scss";
// import { createBrowserHistory } from "history";
// Reactstrap Components
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";

// Material-UI Components
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
import { discoverPeople } from "../../actions/DiscoverPeople";
// Imported Components
import MobileHeaderBody from "./Components/Discover/Discover";
import MobileProspects from "./Components/Prospects/Prospects";
import MobileDates from "./Components/Dates/Dates";
import MobileChats from "./Components/Chats/Chats";
import MobileFooter from "./Components/Footer/Footer";
import MobileFriends from "./Components/Friends/Friends";
import OtherUserMobileProfile from "./Components/otherUserMobileProfile/otherUserMobileProfile";
// Imported Components for Routing
import SidePanel from "../WebHomePage/Components/SidePanel/SidePanel";
import NearPeople from "../WebHomePage/Components/NearPeople/NearPeople";
import Discover from "../WebHomePage/Components/DiscoverPeople/Discover";
import Dates from "../WebHomePage/Components/Dates/Dates";
import Chat from "../WebHomePage/Components/Chat/Chat";
import Premium from "../WebHomePage/Components/Coin/Premium";
import ActivatePlan from "../WebHomePage/Components/Coin/ActivatePlan";
import TopUp from "../WebHomePage/Components/Coin/TopUp";
import UserProfile from "../WebHomePage/Components/UserProfile/Index";
import Settings from "../WebHomePage/Components/Settings/Settings";
import OtherUserProfile from "../WebHomePage/Components/OtherUserProfile/OtherUserProfile";
import plans from "../WebHomePage/Components/Plans/Plans";
import Prospects from "../WebHomePage/Components/Prospects/Prospects";
import Coinbalance from "../WebHomePage/Components/CoinBalance/CoinBalance";
import { getCookie, removeCookie, setCookie } from "../../lib/session";
import { connect } from "react-redux";
import { setDefaultLocation } from "../../actions/User";
import { matchFound, USER_PROFILE_ACTION_FUN, storeCoins } from "../../actions/UserProfile";
import SmallDialogWithProps from "../../Components/Dialogs/SmallDialogWithProps";
import Footer from "../../View/LandingPage/Footer/Footer";
// import UpdatePlansProUser from "../WebHomePage/Components/UpdatePlansProUser/UpdatePlansProUser";
import {
  GetActivePlan,
  NearbyPeople,
  UserProfileData,
  CurrentCoinBalance,
  setLocation,
  BuyNewPlan,
  UserProfiledelete,
} from "../../controller/auth/verification";
import { checkIfProUser } from "../../actions/ProUser";
import * as actions from "../../actions/UserSelectedToChat";
import Earnings from "../WebHomePage/Components/Coin/Earnings";
import NewsFeed from "../WebHomePage/Components/NewsFeed/NewsFeed";
import Friends from "../WebHomePage/Components/Friends/Friends";
import { getBoostDetailsOnWindowRefresh } from "../../actions/boost";
// import WithdrawEarnings from "./Components/WithdrawEarnings/WithdrawEarnings";
import MatUiModal from "../../Components/Model/MAT_UI_Modal";
import Mobile_RecentTransactions from "./Components/Coins/WalletScreens/RecentTransactions";
import FriendChat from "./Components/Chats/FriendChat";
import { Icons } from "../../Components/Icons/Icons";
import Button from "../../Components/Button/Button";
import { LogoutMQTTCalled } from "../../lib/Mqtt/Mqtt";
import { FormattedMessage } from "react-intl";

const styles = {
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
};

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      test: 0,
      value: 0,
      width: window.innerWidth,
      modal: false,
      renderColor: false,
      premiumPlans: null,
      addClass: "",
      premiumUserModal: false,
      selectedPlan: "",
      UserFinalData: {},
      UserSearchresult: [],
      websiteActive: "",
      allPlans: [],
      dropDown: false,
      logoutModal: false,
      confirmDeleteModal: false,
    };
  }

  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  toggleConfirmModal = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  handleprofiledelete = () => {
    this.toggleConfirmModal();
    console.log("account deleted");
    UserProfiledelete()
      .then((data) => {
        console.log("account deleted");
        this.handlesignoutsession();
        this.props.history.push("/");
      })
      .catch((err) => {
        console.log("error deleting account");
      });
  };

  handlesignoutsession = () => {
    let uid = getCookie("uid");
    LogoutMQTTCalled(uid);
    removeCookie("token");
    removeCookie("SignUpseemeid");
    removeCookie("findMateId");
    removeCookie("location");
    removeCookie("citylocation");
    removeCookie("uid");
    localStorage.removeItem("firstName");
    localStorage.removeItem("height");
    localStorage.removeItem("email");
    localStorage.removeItem("picture");
    localStorage.setItem("facebookLogin", false);
    this.props.history.push("/");
  };

  toggleLogoutModal = () => {
    this.setState({ logoutModal: !this.state.logoutModal });
  };

  toggleDropDown = () => this.setState({ dropDown: !this.state.dropDown });

  USERSELECTEDTOCHAT = (object) => {
    console.log("user data", object);
    this.props.userSelectedToChat(object);
  };

  navigateToChat = () => {
    this.setState({ modal: !this.state.modal });
  };

  componentWillMount() {
    setTimeout(() => {
      // this.setState({ loader: false });
    }, 1500);
    let city = getCookie("UserCurrentCity");
    this.props.setLocation(city);

    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  closeModal = () => {
    this.setState({ modal: false });
    this.props.matchFoundFn(undefined);
  };

  toggleUpdateToPremiumUserModal = () => {
    this.setState({ premiumUserModal: !this.state.premiumUserModal });
  };

  handlevalue = (value) => {
    this.setState({ test: value });
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
    // window.removeEventListener("beforeunload", this.onUnload);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  updateProfilePicture = (image) => {
    let obj = { ...this.state.UserFinalData };
    obj.profilePic = image;
    this.setState({ UserFinalData: obj });
  };

  checkSession = () => {
    if (getCookie("token") && getCookie("uid")) {
      return true;
    }
    return false;
  };

  componentDidMount() {
    console.log("main index mounting");
    try {
      let proUser = getCookie("proUser");
      console.log("proUser", proUser);
      let baseAddress = getCookie("baseAddress");
      let base = JSON.parse(baseAddress);
      if (!proUser || proUser == null || proUser == undefined) {
        console.log("trespassed undefined");
        setLocation(
          {
            latitude: base.lat,
            ip: getCookie("ipAddress"),
            timeZone: getCookie("timezone"),
            isPassportLocation: false,
            longitude: base.lng,
            address: base.address,
          },
          getCookie("token")
        )
          .then((res) => {
            console.log("setLocation success", res);
          })
          .catch((err) => console.log("setLocation err", err));
      }
    } catch (e) {
      console.log("phat gaya....................", e);
    }

    this.setState({ addClass: "popularPlan" });
    NearbyPeople().then((data) => {
      this.setState({ UserSearchresult: data.data.data });
      this.props.setAllDiscoverPeople(data.data.data);
      this.props.getBoostDetailsOnWindowRefresh(data.data.boost);
      this.props.saveRemainingLikes(data.data.remainsLikesInString);
    });
    if (this.state.width <= 576) {
      UserProfileData().then((res) => {
        this.props.updateProfilePhoto(res.data.data.profilePic);
        this.setState({ UserFinalData: res.data.data, value: 0 });
      });
      CurrentCoinBalance(getCookie("uid"), getCookie("token")) // 200
        .then((data) => {
          if (data.data.walletData.length > 0) {
            this.props.storeCoinBalance(data.data.walletData[0].balance);
            // dynamically created earning wallet id -> prop.
            this.props.storeWalletID(data.data.walletData[0].walletid);
            this.props.storeEarningWalletId(data.data.walletEarningData[0].walletearningid);
          } else {
            this.props.storeCoinBalance(0);
            this.props.storeEarningWalletId(data.data.walletEarningData[0].walletearningid);
          }
        })
        .catch((err) => {});
    }
    GetActivePlan()
      .then((res) => {
        if (res.data.data.subscriptionId === "Free Plan") {
          setCookie("proUser", false);
        } else {
          let selectedLocation = getCookie("selectedLocation");
          let selLoc = JSON.parse(selectedLocation);
          console.log("selectedLocation", selLoc);
          setCookie("proUser", true);
          setLocation(
            {
              latitude: selLoc.lat,
              ip: getCookie("ipAddress"),
              timeZone: getCookie("timezone"),
              isPassportLocation: true,
              longitude: selLoc.lng,
              address: selLoc.address,
            },
            getCookie("token")
          )
            .then((res) => {
              console.log("setLocation success", res);
            })
            .catch((err) => console.log("setLocation err", err));
        }
        this.props.checkIfUserIsProUser(res.data.data);
      })
      .catch((err) => console.log("err in get active plan"));

    BuyNewPlan().then((res) => {
      this.setState({ allPlans: res.data.data });
    });
  }

  handleWebsiteActiveValue = (value) => {
    this.setState({ websiteActive: value });
  };

  renderBgColorBasedOnLink = (link) => {
    if (
      link === "/app/discover" ||
      link === "/app" ||
      link === "/app/" ||
      link === "/app/dates" ||
      link === "/app/prospects" ||
      link === "/app/profile" ||
      link === "/app/earnings" ||
      link === "/app/settings" ||
      link === "/app/newsFeed" ||
      link === "/app/friends"
    ) {
      this.setState({ renderColor: true });
    }
    if (link === "/app/chat/" || link === "/app/chat") {
      this.setState({ renderColor: true });
    }
    if (link.includes("/app/user/")) {
      this.setState({ renderColor: true });
    }
  };

  selectedPlan = (plan) => {
    this.setState({ selectedPlan: plan.planName });
  };

  render() {
    const { width } = this.state;
    const isMobile = width <= 576;
    // const { classes } = this.props;
    // this.switchScreen(this.state.test);

    // Mobile View Module
    if (isMobile) {
      return (
        <div>
          <div>
            <Switch>
              <Route
                path="/app"
                exact
                render={(props) => (
                  <MobileHeaderBody
                    {...props}
                    UserFinalData={this.state.UserFinalData}
                    UserSearchresult={this.state.UserSearchresult}
                    handlevalue={this.handlevalue}
                    USERSELECTEDTOCHAT={this.USERSELECTEDTOCHAT}
                  />
                )}
              />
              <Route
                path="/app/prospects"
                exact
                render={(props) => <MobileProspects UserFinalData={this.state.UserFinalData} handlevalue={this.handlevalue} />}
                USERSELECTEDTOCHAT={this.USERSELECTEDTOCHAT}
              />

              <Route
                path="/app/chat"
                exact
                render={(props) => (
                  <MobileChats
                    {...props}
                    UserFinalData={this.state.UserFinalData}
                    USERSELECTEDTOCHAT={this.USERSELECTEDTOCHAT}
                    handlevalue={this.handlevalue}
                  />
                )}
              />
              <Route
                path="/app/dates"
                exact
                render={(props) => (
                  <MobileDates
                    UserFinalData={this.state.UserFinalData}
                    {...props}
                    USERSELECTEDTOCHAT={this.USERSELECTEDTOCHAT}
                    handlevalue={this.handlevalue}
                  />
                )}
              />
              <Route
                path="/app/friends"
                exact
                render={(props) => (
                  <MobileFriends
                    UserFinalData={this.state.UserFinalData}
                    {...props}
                    USERSELECTEDTOCHAT={this.USERSELECTEDTOCHAT}
                    handlevalue={this.handlevalue}
                  />
                )}
              />
              <Route exact path="/app/walletLogs" render={(props) => <Mobile_RecentTransactions {...props} />} />
              <Route
                exact
                path="/app/mobile/user/:name/:id"
                render={(props) => <OtherUserMobileProfile {...props} handlevalue={this.handlevalue} />}
              />
              <Route
                path="/app/friendChat"
                exact
                render={(props) => <FriendChat {...props} UserFinalData={this.state.UserFinalData} handlevalue={this.handlevalue} />}
              />
              {/* <Route path="/app/earnings" exact render={(props) => <WithdrawEarnings {...props} handlevalue={this.handlevalue} />} /> */}
            </Switch>
            {/** custom dropdown loader starts */}
            {this.props.uploadingPostState ? (
              <div className="m_global_customLoader">
                <CircularProgress /> <span className="pl-2">{this.props.uploadingPostText}</span>
              </div>
            ) : (
              ""
            )}
          </div>

          {/** custom dropdown loader ends */}
          {window.location.pathname.includes("/app/mobile/user") || window.location.pathname.includes("/app/friendChat") ? (
            <></>
          ) : (
            <div>
              <MobileFooter
                ProUser={this.props.ProUser}
                handleChangevalue={this.handlevalue}
                isMobile={this.isMobile}
                value={this.state.test}
              />
            </div>
          )}
        </div>
      );
    }

    // Desktop View Module
    else {
      return (
        <section className="WebView w-100">
          <header className="col-12 px-0 border-bottom">
            <div className="row mx-0">
              <div className="col-3">
                <img src={Icons.cmeLogo} width={249} alt="App-Logo" />
              </div>
              <div className="col-9 px-0 d-flex justify-content-end align-items-center">
                {/* <div className="px-3 px-0">
                  <img src={Icons.NewBell} width={20} alt="bell" />
                </div> */}
                <div className="pl-3 px-0" onClick={() => this.props.history.push("/app/chat")} style={{ cursor: "pointer" }}>
                  <img src={Icons.NewMessenger} width={20} alt="chats" />
                </div>
                {/* <div className="px-3 px-0">
                  <img src={Icons.NewUser} width={20} alt="profile" />
                </div> */}
                <div className="px-3 px-0 my__account">
                  <Dropdown isOpen={this.state.dropDown} toggle={this.toggleDropDown}>
                    <DropdownToggle caret>My Account</DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem onClick={this.toggleLogoutModal}>Logout</DropdownItem>
                      <DropdownItem onClick={this.toggleConfirmModal}>Delete Account</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </div>
              </div>
            </div>
          </header>
          <section className="w-100">
            <div className="row mx-0">
              <div className="new__sidepanel">
                <SidePanel allPlans={this.state.allPlans} websiteActive={this.state.websiteActive} {...this.props} />
              </div>
              <div className="new__view__render">
                <Switch>
                  <Route
                    exact
                    path="/app/"
                    render={(props) => (
                      <NearPeople
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/discover"
                    render={(props) => (
                      <Discover
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/dates/"
                    render={(props) => (
                      <Dates
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/chat/"
                    render={(props) => (
                      <Chat
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/profile/"
                    render={(props) => <UserProfile renderBgColorBasedOnLink={this.renderBgColorBasedOnLink} {...props} {...this.props} />}
                  />
                  <Route
                    exact
                    path="/app/newsFeed"
                    render={(props) => (
                      <NewsFeed
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                        {...props}
                        {...this.props}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/settings/"
                    render={(props) => (
                      <Settings
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/user/:name/:id"
                    render={(props) => <OtherUserProfile renderBgColorBasedOnLink={this.renderBgColorBasedOnLink} />}
                  />
                  <Route exact path="/app/mobile/user/:name/:id" component={OtherUserMobileProfile} />
                  <Route exact path="/app/plans/" component={plans} />
                  <Route
                    exact
                    path="/app/prospects/"
                    render={(props) => (
                      <Prospects
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/coinbalance/"
                    render={(props) => <Coinbalance renderBgColorBasedOnLink={this.renderBgColorBasedOnLink} />}
                  />
                  <Route exact path="/app/top-up" render={(props) => <TopUp renderBgColorBasedOnLink={this.renderBgColorBasedOnLink} />} />
                  <Route exact path="/app/premium" render={() => <Premium allPlans={this.state.allPlans} />} />
                  <Route exact path="/app/activate-plan" render={(props) => <ActivatePlan />} />
                  <Route
                    exact
                    path="/app/earnings"
                    render={(props) => (
                      <Earnings
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/app/friends"
                    render={(props) => (
                      <Friends
                        handleWebsiteActiveValue={this.handleWebsiteActiveValue}
                        renderBgColorBasedOnLink={this.renderBgColorBasedOnLink}
                      />
                    )}
                  />
                </Switch>
              </div>
            </div>
          </section>

          {this.props && this.props.matchFound && Object.keys(this.props && this.props.matchFound).length > 0 ? (
            <SmallDialogWithProps
              text="Lets Chat"
              open={Object.keys(this.props && this.props.matchFound).length > 0}
              handleClose={this.toggleModal}
              closeModal={this.closeModal}
            >
              <div className="smallDialogWithProps">
                <div>
                  <h2>Match Found</h2>
                </div>
                <div className="row">
                  <div>
                    <img
                      alt={this.props.matchFound.firstLikedByPhoto}
                      src={this.props.matchFound && this.props.matchFound.firstLikedByPhoto}
                      className="matchFound_users_image"
                    />
                  </div>
                  <div>
                    <img
                      alt={this.props.matchFound.secondLikedByPhoto}
                      src={this.props.matchFound && this.props.matchFound.secondLikedByPhoto}
                      className="matchFound_users_image"
                    />
                  </div>
                </div>
              </div>
            </SmallDialogWithProps>
          ) : (
            ""
          )}
          <Footer />
          <MatUiModal toggle={this.toggleLogoutModal} width={500} isOpen={this.state.logoutModal}>
            <div className="col-12 py-5 deleteAccModal">
              <div className="d_newsFeed_modal_close_btn" onClick={this.toggleLogoutModal}>
                <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
              </div>
              <div className="text-center">Are You Sure you want to logout ?</div>
              <div className="col-12 px-5">
                <div className="row justify-content-around pt-5">
                  <Button handler={this.toggleLogoutModal} text="No" className="confirmModalButton" />
                  <Button
                    handler={() => {
                      this.toggleLogoutModal();
                      this.handlesignoutsession();
                    }}
                    text="Yes"
                    className="cancelModalButton"
                  />
                </div>
              </div>
            </div>
          </MatUiModal>
          <MatUiModal toggle={this.toggleConfirmModal} isOpen={this.state.confirmDeleteModal} width={600}>
            <div className="col-12 py-5 deleteAccModal">
              <div className="d_newsFeed_modal_close_btn" onClick={this.toggleConfirmModal}>
                <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
              </div>
              <div className="text-center">
                <FormattedMessage id="message.confirmDeleteAccount" /> {this.state.UserFinalData.firstName} UnFiltered{" "}
                <FormattedMessage id="message.account" />?
              </div>
              <div className="col-12 px-5">
                <div className="row justify-content-around pt-5">
                  <Button handler={this.toggleConfirmModal} text="Cancel" className="cancelModalButton" />
                  <Button handler={this.handleprofiledelete} text="Confirm" className="confirmModalButton" />
                </div>
              </div>
            </div>
          </MatUiModal>
        </section>
      );
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLocation: (loc) => dispatch(setDefaultLocation(loc)),
    matchFoundFn: (data) => dispatch(matchFound(data)),
    checkIfUserIsProUser: (details) => dispatch(checkIfProUser(details)),
    setAllDiscoverPeople: (data) => dispatch(discoverPeople(data)),
    getBoostDetailsOnWindowRefresh: (data) => dispatch(getBoostDetailsOnWindowRefresh(data)),
    saveRemainingLikes: (data) => dispatch(USER_PROFILE_ACTION_FUN("remainsLikesInString", data)),
    storeCoinBalance: (coins) => dispatch(storeCoins(coins)),
    userSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
    updateProfilePhoto: (image) => dispatch(USER_PROFILE_ACTION_FUN("UserProfilePic", image)),
    storeWalletID: (id) => dispatch(USER_PROFILE_ACTION_FUN("WalletData", id)),
    storeEarningWalletId: (id) => dispatch(USER_PROFILE_ACTION_FUN("EarningWalletId", id)),
  };
};

const mapStateToProps = (state) => {
  return {
    matchFound: state.UserProfile.matchFound,
    ProUser: state.ProUser.ProUserDetails,
    uploadingPostState: state.Posts.display,
    uploadingPostText: state.Posts.currentText,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Index));
