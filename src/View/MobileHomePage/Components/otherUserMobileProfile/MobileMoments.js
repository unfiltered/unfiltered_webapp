import React from "react";
import "./otherUserMobileProfile.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import { likeOrDislikePost } from "../../../../controller/auth/verification";
import * as PostActions from "../../../../actions/Posts";
import { getCookie } from "../../../../lib/session";
import { connect } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import "../NewsFeed/MobileNewsFeed.scss";
import moment from "moment";
import MobileComments from "../NewsFeed/MobileComments";
import MainDrawer from "../../../../Components/Drawer/Drawer";
// import Snackbar from "../../../../Components/Snackbar/Snackbar";

// UserFinalData

class MobileMoments extends React.Component {
  state = {
    allPosts: [],
    start: 0,
    end: 20,
    loader: true,
    heightWidth: "",
    commentsDrawer: false,
    singlePostData: {},
  };
  toggleCommentsDrawer = (data) => {
    if (data) {
      this.setState({ singlePostData: data, commentsDrawer: !this.state.commentsDrawer });
    }
    this.setState({ commentsDrawer: !this.state.commentsDrawer });
  };
  componentDidMount() {
    this.GetAllPosts();
  }

  updateCommentCount = (postId) => {
    this.props.m_updateCommentCount(postId);
  };

  functionToLikeOrDislike = (type, postId) => {
    likeOrDislikePost(getCookie("token"), type, postId)
      .then((res) => {
        this.props.m_likeDislikePost(postId);
      })
      .catch((err) => console.log("functionToLikeOrDislike", err));
  };

  GetAllPosts = () => {
    this.props.m_storeAllPosts(this.props.UserFinalData && this.props.UserFinalData.moments);
    this.setState({ loader: false });
  };
  render() {
    return (
      <div className="col-12 px-0 m_mobile_moments_screen">
        <MainDrawer width={400} onClose={this.toggleCommentsDrawer} onOpen={this.toggleCommentsDrawer} open={this.state.commentsDrawer}>
          <MobileComments
            updateCommentCount={this.updateCommentCount}
            UserFinalData={this.props.UserFinalData}
            singlePostData={this.state.singlePostData}
            onClose={this.toggleCommentsDrawer}
          />
        </MainDrawer>
        <div className="row mx-0">
          <div className="col-12 py-3 m_moments_fixed_header">
            <div className="row">
              <div className="col-3">
                <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.props.onClose} />
              </div>
              <div className="col-6 m_moments_header">Moments</div>
              <div className="col-3"></div>
            </div>
          </div>
          <div className="col-12 d_moments_scroll px-0">
            {this.state.loader ? (
              <div className="m_nf_loader">
                <CircularProgress /> <span>Loading...</span>
              </div>
            ) : (
              <span />
            )}
            {this.props.m_allPosts && this.props.m_allPosts.length > 0 ? (
              this.props.m_allPosts.map((k) => (
                <div className="col-12 px-0 py-2" key={k._id}>
                  <div className="row mx-0 pb-1 align-items-center">
                    <div className="col-1 m_nf_profilePic">
                      <img src={k.profilePic} alt={k.userName} />
                    </div>
                    <div className="col-9  m_nf_postDetails pl-4">
                      <div>{k.userName}</div>
                      <div>{moment(k.postedOn).fromNow()}</div>
                    </div>
                    {/* <div className="col-2 m_nf_menuDropDown">
                      <IconButton
                        aria-label="More"
                        onClick={() => {
                          this.toggleModal();
                          this.setState({ singlePostData: k });
                        }}
                      >
                        <MoreVertIcon />
                      </IconButton>
                    </div> */}
                  </div>
                  <div className="row mx-0 m_oup_moments_postImageVideo">
                    {k.url[0].includes("mp4") ? (
                      <video width={"100%"} height="500" controls>
                        <source src={k.url[0]} type="video/mp4" />
                      </video>
                    ) : (
                      <img src={k.url[0]} alt={k.description} />
                    )}
                  </div>
                  <div className="row mx-0 pt-2">
                    <div
                      className="pl-2"
                      onClick={() => (k.liked ? this.functionToLikeOrDislike(2, k.postId) : this.functionToLikeOrDislike(1, k.postId))}
                    >
                      <img src={k.liked ? Icons.likedLogo1 : Icons.likedLogo} height={20} width={20} alt="like" />
                    </div>
                    <div className="pl-2" onClick={() => this.toggleCommentsDrawer(k)}>
                      <img className="m_nf_likeBtn" src={Icons.commentLogo} height={19} width={20} alt="comment" />
                    </div>
                  </div>
                  <div className="row mx-0 pt-1">
                    <span className="pl-2 m_nf_commentNLikes">{k.likeCount} likes</span>{" "}
                    <span className="pl-2 m_nf_commentNLikes">{k.commentCount > 0 ? k.commentCount + " comments" : "No Comments"}</span>
                  </div>
                  <div className="row mx-0">
                    <span className="pl-2 m_nf_postDesc">{k.description}</span>
                  </div>
                </div>
              ))
            ) : this.state.loader ? (
              <span />
            ) : (
              <div className="col-12 h-75">
                <div className="row h-100 justify-content-center align-items-center flex-column">
                  <div>
                    <img src={Icons.NoNewsSad} alt="sad-face" height={130} width={100} />
                  </div>
                  <div className="m_nf_noNewsFeed1">You have no news feed.</div>
                  <div className="m_nf_noNewsFeed2">You have no News feed. Please post your moment.</div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    m_allPosts: state.Posts.Posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    m_storeAllPosts: (data) => dispatch(PostActions.storeAllPosts(data)),
    m_likeDislikePost: (postId) => dispatch(PostActions.likeDislikePost(postId)),
    m_updateCommentCount: (postId) => dispatch(PostActions.addComment(postId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MobileMoments);
