// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";

// Scss
import "./otherUserMobileProfile.scss";

// Resusable Components
import MainModel from "../../../../Components/Model/model";

// Material-UI Components
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

// Redux Components
import { OtherUserProfile } from "../../../../controller/auth/verification";

let userid = " ";

class otherUserMobileProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otheruserfinaldata: "",
      otheruserfindmateid: "",
      open: false,
      openreportmodel: false
    };
    this.handleBackHome = this.handleBackHome.bind(this);
  }

  componentDidMount() {
    // User to Get the Current Path URL
    let urlParts = window.location.href.split("/");
    console.log("Urlid", urlParts[urlParts.length - 1]);

    userid = urlParts[urlParts.length - 1];

    let otheruserpayload = {
      targetUserId: urlParts[urlParts.length - 1]
    };

    OtherUserProfile(otheruserpayload).then(data => {
      this.setState(
        {
          otheruserfinaldata: data.data.data,
          otheruserfindmateid: data.data.data.findMateId
        },
        () => {
          console.log("otheruserprofile", this.state.otheruserfinaldata);
        }
      );
    });
  }

  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  // Function for the Open Toggle
  handleClickOpen = () => {
    this.setState({ openreportmodel: true });
  };

  // Function for the Close Toggle
  handleClose = () => {
    this.setState({ openreportmodel: false });
  };

  // To Get back in Home Screen
  handleBackHome() {
    this.props.history.push("/app/");
  }

  // Function for the Notification ( Snackbar )
  handleClose = reason => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;

    return (
      <div>
        {/* Header Module */}
        <div
          className="col-12 py-2 position-absolute"
          style={{ zIndex: "9999", background: "rgba(0,0,0,0.4)" }}
        >
          <div className="row">
            <div className="col-12 text-left">
              <i
                className="fa fa-arrow-left"
                onClick={this.handleBackHome}
                style={{
                  fontSize: "24px",
                  color: "#fff"
                }}
              />
            </div>
          </div>
        </div>
        {/* OtherUser Profile Slider Module */}
        <div>
          <img
            src={this.state.otheruserfinaldata.profilePic}
            alt={this.state.otheruserfinaldata.firstName}
            title={this.state.otheruserfinaldata.firstName}
            className="img-fluid"
            width="100%"
          />
        </div>

        {/* OtherUser pRefrence Data Module */}
        <div className="col-12 Motheruser_info">
          {/* OtherUser Profile Name Module */}
          <div className="pt-3 row">
            <div className="col-9">
              <h4 className="m-0">
                <strong>
                  {this.state.otheruserfinaldata.firstName},{" "}
                  {this.state.otheruserfinaldata &&
                  this.state.otheruserfinaldata.age
                    ? this.state.otheruserfinaldata.age.value
                    : ""}
                </strong>
                {this.state.otheruserfinaldata.onlineStatus === 1 ? (
                  <i className="fas fa-circle" />
                ) : (
                  ""
                )}
              </h4>
            </div>
            <div className="col-3 text-right">
              <Button
                buttonRef={node => {
                  this.anchorEl = node;
                }}
                aria-owns={open ? "menu-list-grow" : undefined}
                aria-haspopup="true"
                onClick={this.handleToggle}
                className="user_settings"
              >
                <i className="fas fa-ellipsis-v" />
              </Button>
              <Popper
                open={open}
                anchorEl={this.anchorEl}
                transition
                disablePortal
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom"
                    }}
                  >
                    <Paper>
                      <ClickAwayListener onClickAway={this.handleClose}>
                        <MenuList>
                          <MenuItem onClick={this.handleClose}>
                            Recommnded to Friend
                          </MenuItem>
                          <MenuItem onClick={this.handleClickOpen}>
                            Report {this.state.otheruserfinaldata.firstName}
                          </MenuItem>
                          <MenuItem onClick={this.handleClose}>
                            Block {this.state.otheruserfinaldata.firstName}
                          </MenuItem>
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </div>
          </div>

          {/* OtherUser Profile Location Module */}
          <div className="pb-3 border-bottom">
            <p className="m-0" style={{ color: "#a3a3a3" }}>
              {this.state.otheruserfinaldata &&
              this.state.otheruserfinaldata.distance ? (
                <i className="fas fa-map-marker-alt pr-2">
                  {" "}
                  {this.state.otheruserfinaldata.distance.value}
                  {"  " + " "} Kilometer away.
                </i>
              ) : (
                ""
              )}{" "}
            </p>
          </div>

          {/* OtherUser Prefence Module */}
          {this.state.otheruserfinaldata &&
            this.state.otheruserfinaldata.myPreferences.map((data, index) => (
              <div key={index} className="m-0 py-3 border-bottom">
                {/* OtherUser Prefrence Header Module */}
                <div className="userpro_header">
                  <h5>{data.title}</h5>
                </div>
                {data.data.map((pref, data) => (
                  // Otheruser prefrence Body Module
                  <div
                    key={"OtherUserProfile" + data}
                    className="py-2 user_answer"
                  >
                    <div className="m-0 row">
                      <div className="p-0  col-6">
                        <p className="m-0">{pref.label} :</p>
                      </div>
                      <div className="p-0  col-6">
                        <p className="m-0 text-right">
                          {pref.selectedValues
                            ? pref.selectedValues.join(",")
                            : "N/A"}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            ))}

          {/* Share && Report User Profile Module */}
          <div className="m-0 py-3 d-block row">
            <div className="share_profile">
              <p className="m-0">
                <i className="fas fa-share-alt">
                  <span>Recommended to a Friend</span>
                </i>
              </p>
              <p className="m-0">
                <i className="fas fa-ban">
                  <span>Report {this.state.otheruserfinaldata.firstName}</span>
                </i>
              </p>
              {/* <p className="m-0">
                <i className="fas fa-user-times">
                  <span>
                    Unfriend {this.state.otheruserfinaldata.firstName}
                  </span>
                </i>
              </p> */}
            </div>
          </div>
        </div>

        {/* OtherUser Profile Thumpup */}
        <div className="pb-4 Motheruser_thumpup">
          <div className="text-center">
            <i className="fas fa-video" />
            <i className="fas fa-thumbs-up" />
          </div>
        </div>

        {/* Report User Module (POP-UP) */}
        <Dialog
          open={this.state.open}
          // TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent className="p-3">
            <DialogContentText id="alert-dialog-slide-description">
              Are you sure want to log out? you will continue to be seen by
              compatible users in your last known location
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.HandleSubmit} color="primary">
              <p className="m-0 text-danger">Logout</p>
            </Button>
            <Button onClick={this.handleClose} color="primary">
              <p className="m-0 text-secondary">Cancel</p>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withRouter(otherUserMobileProfile);
