import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import "./otherUserMobileProfile.scss";
import "../UserProfile/UserProfile.scss";
import MainModel from "../../../../Components/Model/model";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slider from "react-slick";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import {
  OtherUserProfile,
  ReasonblockUser,
  ReportUser,
  BlockUser,
  getAllPosts,
  MyDisLikeUser,
  LikeNearPeople,
  SuperLikeNearPeople,
  GiveThumbsUp,
} from "../../../../controller/auth/verification";
import FriendChat from "../Chats/FriendChat";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
import MobileMoments from "./MobileMoments";

let userid = " ";
const ITEM_HEIGHT = 48;

const styles = (theme) => ({
  dropdownOption: {
    background: "#161616",
    color: "#1976d2",
    fontFamily: "Circular Air Book",
  },
  menuRoot: {
    background: "#161616",
  },
});

class OtherUserMobileProfileFromChatView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otheruserfinaldata: "",
      otheruserfindmateid: "",
      open: false,
      openreportmodel: false,
      anchorEl: null,
      friendchat: false,
      _snackbar: false,
      blockreason: "",
      modelchat: false,
      allPosts: [],
      squareWH: "",
      postInfo: {},
      momentsScreen: false,
    };
    // this.videoRef = React.createRef()
    this.handleBackHome = this.handleBackHome.bind(this);
    this.togglemodel = this.togglemodel.bind(this);
    this.videoRef = React.createRef();
  }

  toggleMoments = (data) => {
    if (data) {
      this.setState({ postInfo: data });
    }
    this.setState({ momentsScreen: !this.state.momentsScreen });
  };

  componentDidMount() {
    let otheruserpayload = {
      targetUserId:
        (this.props && this.props.userInfo && this.props.userInfo.recipientId) ||
        (this.props && this.props.userInfo && this.props.userInfo._id) ||
        getCookie("userselectedtochat"),
    };

    OtherUserProfile(otheruserpayload).then((data) => {
      this.setState({
        otheruserfinaldata: data.data.data,
      });
      if (data.data.data && data.data.data.moments && data.data.data.moments.length > 0) {
        let w = document.getElementById("m_moments_boxes").offsetWidth;
        this.setState({ squareWH: w });
      }
    });

    this.handleblockreason();
  }

  // Used to get Reason to Reason User Data
  handleblockreason = () => {
    ReasonblockUser().then((data) => {
      this.setState({ blockreason: data.data.data });
    });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ _snackbar: true });
  };

  // Function for the Notification ( Snackbar )
  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ _snackbar: false });
  };

  // To Open Menu
  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // To Close Menu
  handleClosemenu = () => {
    this.setState({ anchorEl: null });
  };

  // To GO back in Home Screen
  handleBackHome = () => {
    this.props.onClose();
    // this.props.history.push("/app/");
  };

  // Function to Open a Chat Drawer (User has Coin)
  openchatdrawer = () => {
    this.setState({ friendchat: true });
  };

  // Function to Open a Model (User has No Coin)
  openchatdrawernoCoin = () => {
    this.togglemodel();
  };

  // TO Close a Chat Drawer
  closechatdrawer = () => {
    this.setState({ friendchat: false });
  };

  // Used to Open Report User Dialog
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  // Used to Close Report User Dialog
  handleClosedialog = () => {
    this.setState({ open: false });
  };

  // Function for the Model Toggle
  togglemodel() {
    this.setState((prevState) => ({
      modelchat: !prevState.modelchat,
    }));
  }

  // API Call for to Report a User
  handlereportuser = (data) => {
    let UserReportPayload = {
      targetUserId: userid,
      reason: data,
      message: "Report",
    };

    ReportUser(UserReportPayload)
      .then((data) => {
        this.handleClosedialog();
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User Report successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Block a User
  handleBlockUser = () => {
    let UserBlockPayload = {
      targetUserId: userid,
    };

    BlockUser(UserBlockPayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User Block successfully.",
          getblock: true,
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to UnLike the User
  handledunlikeuser = (data) => {
    let Unlikepayload = {
      targetUserId: data._id,
    };
    MyDisLikeUser(Unlikepayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User unlike successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = (data) => {
    let likepayload = {
      targetUserId: data._id,
    };

    LikeNearPeople(likepayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User like successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  dislikeUser = (userId) => {
    MyDisLikeUser({
      targetUserId: userId,
    })
      .then((data) => {
        let userDataFromState = { ...this.state.otheruserfinaldata };
        userDataFromState.dislike = 1;
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "Dislike Successfull.",
          otheruserfinaldata: userDataFromState,
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  superLikeUser = (userid) => {
    let { superLike } = this.props.CoinConfig;
    SuperLikeNearPeople({ targetUserId: userid })
      .then((data) => {
        let userDataFromState = { ...this.state.otheruserfinaldata };
        userDataFromState.superliked = 1;
        if (
          this.props.checkIfUserIsProUser &&
          this.props.checkIfUserIsProUser.ProUserDetails &&
          this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
          this.props.UserCoin > 0
        ) {
          this.props.coinReduce(parseInt(superLike.Coin));
        }
        this.setState({
          otheruserfinaldata: userDataFromState,
          _snackbar: true,
          variant: "success",
          usermessage: "Superliked successfull",
        });
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 2000);
      })
      .catch((error) => {
        if (error.status === 405) {
          this.setState({
            _snackbar: true,
            variant: "error",
            usermessage: `You have already liked ${this.state.otheruserfinaldata.firstName}`,
          });
        } else {
          this.setState({
            _snackbar: true,
            variant: "error",
            usermessage: "Something went wrong.",
          });
        }
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 1300);
      });
  };

  giveThumbsUp = (targetId) => {
    GiveThumbsUp(getCookie("token"), targetId)
      .then((res) => {
        if (res.status === 409) {
          this.setState({
            usermessage: `You have already sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            _snackbar: true,
          });
        }
        if (res.status === 200) {
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userDataFromState.isThumbsUpProfile = 1;
          this.setState({
            otheruserfinaldata: userDataFromState,
            usermessage: `You have sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            _snackbar: true,
          });
        }
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 2000);
      })
      .catch((err) => console.log("[thumbs up fail]", err));
  };

  likeUser = (userid) => {
    if (this.props.UserProfile.remainsLikesInString > 0 || this.props.UserProfile.remainsLikesInString === "unlimited") {
      LikeNearPeople({ targetUserId: userid })
        .then((data) => {
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userDataFromState.liked = 1;
          this.setState({
            otheruserfinaldata: userDataFromState,
            _snackbar: true,
            variant: "success",
            usermessage: "Profile has been liked successfully",
          });
          setTimeout(() => {
            this.setState({ _snackbar: false });
          }, 2000);
        })
        .catch((error) => {
          if (error.status === 405) {
            this.setState({
              _snackbar: true,
              variant: "error",
              usermessage: `You have already superliked ${this.state.otheruserfinaldata.firstName}`,
            });
          } else {
            this.setState({
              _snackbar: true,
              variant: "error",
              usermessage: "Something went wrong...",
            });
          }
          setTimeout(() => {
            this.setState({ _snackbar: false });
          }, 2000);
        });
    } else {
      this.setState({
        _snackbar: true,
        variant: "error",
        usermessage: "Your Likes count has exhausted",
      });
    }
    setTimeout(() => {
      this.setState({ _snackbar: false });
    }, 2000);
  };

  openChat = () => {
    this.props.dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", this.state.otheruserfinaldata));
    this.props.history.push("/app/chat");
    this.props.handlevalue(3);
  };

  recommendFriend = () => {
    navigator.clipboard.writeText(`Get yourself into world of dating. Let's connect on https://app.unfiltered.love`);
    this.setState({
      _snackbar: true,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends now",
    });
    this.handleClosemenu();
  };

  render() {
    let { classes } = this.props;
    // console.log("otheruserprofile", this.state.otheruserfinaldata);
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const FriendChatdrawer = <FriendChat onClose={this.closechatdrawer} />;
    let video = this.videoRef && this.videoRef.current;

    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      height: "350px",
      beforeChange: (oldIndex, newIndex) => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
      afterChange: (index) => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
      onSwipe: () => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
    };

    return (
      <div>
        <MainDrawer width={400} onClose={this.closechatdrawer} onOpen={this.openchatdrawer} open={this.state.friendchat}>
          {FriendChatdrawer}
        </MainDrawer>
        <MainDrawer width={400} onClose={this.toggleMoments} onOpen={this.toggleMoments} open={this.state.momentsScreen}>
          <MobileMoments onClose={this.toggleMoments} UserFinalData={this.state.otheruserfinaldata} selectedPost={this.postInfo} />
        </MainDrawer>
        {/* Header Module */}
        <div className="col-12 py-2 selfProfile-header" style={{ zIndex: "9999", background: "rgba(0,0,0,0.4)" }}>
          <div className="row">
            <div className="col-12 text-left">
              <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.handleBackHome} />
            </div>
          </div>
        </div>
        <div className="selfProfile-scrollView">
          {/* OtherUser Profile Slider Module */}
          <div style={{ height: "350px" }}>
            <Slider {...settings}>
              {/* Video Module */}
              {this.state.otheruserfinaldata ? (
                this.state.otheruserfinaldata && this.state.otheruserfinaldata.profileVideo.length > 0 ? (
                  <div className="p-0 col-12 MotherUser_Video">
                    <video controls ref={this.videoRef}>
                      <source src={this.state.otheruserfinaldata.profileVideo} />
                    </video>
                  </div>
                ) : (
                  " "
                )
              ) : (
                " "
              )}

              {/* Main Profile Pic Moduel */}
              {this.state.otheruserfinaldata ? (
                <img
                  src={this.state.otheruserfinaldata.profilePic}
                  alt={this.state.otheruserfinaldata.firstname}
                  title={this.state.otheruserfinaldata.firstname}
                  height="350px"
                  className="otherUserProfileFromChat-profilePic"
                  width="100%"
                />
              ) : (
                " "
              )}

              {/* Other Image Module */}
              {this.state.otheruserfinaldata &&
              this.state.otheruserfinaldata.otherImages &&
              this.state.otheruserfinaldata.otherImages.length > 0
                ? this.state.otheruserfinaldata.otherImages.map((res, index) => (
                    <div key={index} className="p-0">
                      <div className="user_photos">
                        <img
                          src={res}
                          alt={this.state.otheruserfinaldata.firstName}
                          title={this.state.otheruserfinaldata.firstName}
                          height="350px"
                          className="otherUserProfileFromChat-profilePic"
                          width="100%"
                        />
                      </div>
                    </div>
                  ))
                : " "}
            </Slider>
          </div>

          {/* OtherUser Prefrence Data Module */}
          <div className="col-12 Motheruser_info">
            {/* OtherUser Profile Name Module */}
            <div className="pt-2 row">
              <div className="col-9">
                <h4 className="m-0">
                  <strong style={{ textTransform: "capitalize", color: "#808080" }}>
                    {this.state.otheruserfinaldata.firstName}
                    {/* {this.state.otheruserfinaldata && this.state.otheruserfinaldata.age ? this.state.otheruserfinaldata.age.value : ""} */}
                  </strong>
                  {this.state.otheruserfinaldata.onlineStatus === 1 ? <i className="fas fa-circle" /> : ""}
                </h4>
              </div>

              <div className="col-3 text-right">
                <IconButton
                  aria-label="More"
                  aria-owns={open ? "long-menu" : undefined}
                  aria-haspopup="true"
                  onClick={this.handleClick}
                  className="py-2 px-0"
                  style={{ color: "#f80402" }}
                >
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="long-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={this.handleClosemenu}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: 200,
                      background: "#161616",
                    },
                  }}
                >
                  <MenuItem
                    onClick={this.recommendFriend}
                    classes={{
                      root: classes.dropdownOption,
                    }}
                  >
                    <p className="m-0">Recommend Friend</p>
                  </MenuItem>
                  <MenuItem
                    onClick={this.handleClickOpen}
                    classes={{
                      root: classes.dropdownOption,
                    }}
                  >
                    <p className="m-0">Report {this.state.otheruserfinaldata.firstName}</p>
                  </MenuItem>
                  <MenuItem
                    classes={{
                      root: classes.dropdownOption,
                    }}
                  >
                    <div className="m-0" onClick={this.handleBlockUser}>
                      {this.state.getblock === "true" ? (
                        <p className="m-0">UnBlock {this.state.otheruserfinaldata.firstName}</p>
                      ) : (
                        <p className="m-0">Block {this.state.otheruserfinaldata.firstName}</p>
                      )}
                    </div>
                  </MenuItem>
                </Menu>
              </div>
            </div>

            {/* OtherUser Profile Location Module */}
            <div className="pb-3 border-bottom">
              <p className="m-0" style={{ color: "#a3a3a3" }}>
                {this.state.otheruserfinaldata && this.state.otheruserfinaldata.distance ? (
                  <div className="m_oup_id">
                    <span>ID</span>
                    <span>{this.state.otheruserfinaldata.findMateId}</span>
                  </div>
                ) : (
                  ""
                )}
              </p>
            </div>

            {/* OtherUser Prefence Module */}
            {this.state.otheruserfinaldata &&
              this.state.otheruserfinaldata.myPreferences.map((data, index) => (
                <div key={index} className="m-0 py-3 border-bottom">
                  {/* OtherUser Prefrence Header Module */}
                  <div className="m_userpro_header">
                    <h5>{data.title}</h5>
                  </div>
                  {data.data.map((pref, data) => (
                    // Otheruser prefrence Body Module
                    <div key={"OtherUserProfile" + data} className="py-2">
                      <div className="m-0 row">
                        <div className="p-0 col-4 Mbasic">
                          <p className="m-0 ">{pref.label} :</p>
                        </div>
                        <div className="p-0 col-8 Mbasicanswer">
                          <p className="m-0 text-right ">
                            {pref.selectedValues && pref.selectedValues.length > 0 ? pref.selectedValues.join(",") : "N/A"}
                          </p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ))}

            <div className="py-3 border-bottom col-12 px-0">
              <div className="row py-2">
                <div className="col-6">
                  <div className="m_userpro_header">
                    <h5>Moments</h5>
                  </div>
                </div>
                <div className="col-6 m_moments_view_all" onClick={() => this.toggleMoments()}>
                  View All
                </div>
              </div>

              <div className="row mx-0">
                {this.state.otheruserfinaldata && this.state.otheruserfinaldata.moments.length > 0 ? (
                  this.state.otheruserfinaldata.moments.slice(0, 6).map((k, i) => (
                    <div
                      className="col-4 m_moments_boxes"
                      id="m_moments_boxes"
                      key={i}
                      style={{ height: this.state.squareWH }}
                      onClick={() => this.toggleMoments(k)}
                    >
                      {k.url[0].includes("mp4") ? (
                        <video src={k.url[0]} alt={k.description}></video>
                      ) : (
                        <img src={k.url[0]} alt={k.description} />
                      )}
                    </div>
                  ))
                ) : (
                  <div>No Moments</div>
                )}
              </div>
            </div>

            {/* Share && Report User Profile Module */}
            <div className="m-0 pt-3 d-block row m_rec_report_user">
              <div className="share_profile">
                <p className="m-0" onClick={this.recommendFriend}>
                  <i className="fas fa-share-alt pr-2" />
                  <span>RECOMMEND TO A FRIEND</span>
                </p>
                <p className="m-0" onClick={this.handleClickOpen}>
                  <img src={Icons.danger} alt="danger" height={18} />
                  <span>REPORT {this.state.otheruserfinaldata.firstName}</span>
                </p>
              </div>
            </div>

            <div className="col-12 d-flex justify-content-center" style={{ paddingBottom: "150px" }}>
              <div className="m_thumbsup_icon" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                <img src={Icons.otherUsersThumbsUp} width={25} alt="thumbs-up" />
              </div>
            </div>
          </div>
        </div>
        {/* Report User Dialog (POP-UP) */}
        <Dialog
          open={this.state.open}
          onClose={this.handleClosedialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
            {"Report User"}
          </DialogTitle>
          <DialogContent className="py-0 px-3">
            <DialogContentText id="alert-dialog-description">
              {this.state.blockreason
                ? this.state.blockreason.map((data, index) => (
                    <div key={index} className="py-2 d-block text-center border-bottom" onClick={this.handlereportuser.bind(this, data)}>
                      {data}
                    </div>
                  ))
                : ""}
            </DialogContentText>
          </DialogContent>
          <DialogActions className="py-2 m-auto">
            <Button oclassName="text-center" onClick={this.handleClosedialog} color="primary" autoFocus>
              Cancel
            </Button>
          </DialogActions>
        </Dialog>

        {/* Error Message For not Resgister Number Model */}
        <MainModel isOpen={this.state.modelchat}>
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="pt-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.togglemodel} />
              <div className="WCharge_coins">
                <i className="fas fa-coins" />
              </div>
              <h4>Insufficient Credits Balance.</h4>
              <p className="m-0">You need at least 25 credits to chat with user and your current balance is {this.props.CoinBalance}.</p>
              <div className="Wcharge_wallet">
                <button>
                  <Link to="/app/coinbalance">Buy Credits</Link>
                </button>
              </div>
            </div>
          </div>
        </MainModel>

        {/* Snakbar Components */}
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state._snackbar}
          timeout={2500}
          onClose={this.handleClosesnakbar}
        />
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    checkIfUserIsProUser: state.ProUser,
    CoinConfig: state.CoinConfig.CoinConfig,
  };
};

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(OtherUserMobileProfileFromChatView)));
