import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import "./otherUserMobileProfile.scss";
import MainModel from "../../../../Components/Model/model";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Slider from "react-slick";
import { connect } from "react-redux";
import {
  OtherUserProfile,
  ReasonblockUser,
  ReportUser,
  BlockUser,
  getAllPosts,
  MyDisLikeUser,
  LikeNearPeople,
  SuperLikeNearPeople,
  GiveThumbsUp,
} from "../../../../controller/auth/verification";
import FriendChat from "../Chats/FriendChat";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
import { Images } from "../../../../Components/Images/Images";
import MobileMoments from "./MobileMoments";
import { __callInit } from "../../../../actions/webrtc";
import { withStyles } from "@material-ui/core/styles";

let userid = " ";
const ITEM_HEIGHT = 48;

const styles = (theme) => ({
  dropdownOption: {
    background: "#161616",
    color: "#1976d2",
    fontFamily: "Circular Air Book",
  },
  menuRoot: {
    background: "#161616",
  },
});

class otherUserMobileProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otheruserfinaldata: "",
      otheruserfindmateid: "",
      open: false,
      openreportmodel: false,
      anchorEl: null,
      friendchat: false,
      _snackbar: false,
      blockreason: "",
      modelchat: false,
      allPosts: [],
      squareWH: "",
      postInfo: {},
      momentsScreen: false,
    };
    this.handleBackHome = this.handleBackHome.bind(this);
    this.togglemodel = this.togglemodel.bind(this);
    this.videoRef = React.createRef();
  }

  toggleMoments = (data) => {
    if (data) {
      this.setState({ postInfo: data });
    }
    this.setState({ momentsScreen: !this.state.momentsScreen });
  };

  componentDidMount() {
    // User to Get the Current Path URL
    let urlParts = window.location.href.split("/");
    console.log("Urlid", urlParts[urlParts.length - 1]);
    userid = urlParts[urlParts.length - 1];

    let otheruserpayload = {
      targetUserId: urlParts[urlParts.length - 1],
    };

    OtherUserProfile(otheruserpayload).then((data) => {
      this.setState({
        otheruserfinaldata: data.data.data,
      });
      if (data.data.data && data.data.data.moments && data.data.data.moments.length > 0) {
        let w = document.getElementById("m_moments_boxes").offsetWidth;
        this.setState({ squareWH: w });
      }
    });

    this.handleblockreason();
  }

  // Used to get Reason to Reason User Data
  handleblockreason = () => {
    ReasonblockUser().then((data) => {
      this.setState({ blockreason: data.data.data });
    });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ _snackbar: true });
  };

  // Function for the Notification ( Snackbar )
  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ _snackbar: false });
  };

  // To Open Menu
  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // To Close Menu
  handleClosemenu = () => {
    this.setState({ anchorEl: null });
  };

  // To GO back in Home Screen
  handleBackHome = () => {
    this.props.history.push("/app/");
  };

  // Function to Open a Chat Drawer (User has Coin)
  openchatdrawer = () => {
    this.setState({ friendchat: true });
  };

  // Function to Open a Model (User has No Coin)
  openchatdrawernoCoin = () => {
    this.togglemodel();
  };

  // TO Close a Chat Drawer
  closechatdrawer = () => {
    this.setState({ friendchat: false });
  };

  // Used to Open Report User Dialog
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  // Used to Close Report User Dialog
  handleClosedialog = () => {
    this.setState({ open: false });
  };

  // Function for the Model Toggle
  togglemodel() {
    this.setState((prevState) => ({
      modelchat: !prevState.modelchat,
    }));
  }

  // API Call for to Report a User
  handlereportuser = (data) => {
    let UserReportPayload = {
      targetUserId: userid,
      reason: data,
      message: "Report",
    };
    this.handleClosemenu();
    ReportUser(UserReportPayload)
      .then((data) => {
        this.handleClosedialog();
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User Report successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Block a User
  handleBlockUser = () => {
    let UserBlockPayload = {
      targetUserId: userid,
    };

    BlockUser(UserBlockPayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User Block successfully.",
          getblock: true,
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to UnLike the User
  handledunlikeuser = (data) => {
    let Unlikepayload = {
      targetUserId: data._id,
    };
    MyDisLikeUser(Unlikepayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User unlike successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = (data) => {
    let likepayload = {
      targetUserId: data._id,
    };

    LikeNearPeople(likepayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "User like successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  dislikeUser = (userId) => {
    MyDisLikeUser({
      targetUserId: userId,
    })
      .then((data) => {
        let userDataFromState = { ...this.state.otheruserfinaldata };
        userDataFromState.dislike = 1;
        this.setState({
          _snackbar: true,
          variant: "success",
          usermessage: "Dislike Successfull.",
          otheruserfinaldata: userDataFromState,
        });
      })
      .catch((error) => {
        this.setState({
          _snackbar: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  superLikeUser = (userid) => {
    let { superLike } = this.props.CoinConfig;
    SuperLikeNearPeople({ targetUserId: userid })
      .then((data) => {
        let userDataFromState = { ...this.state.otheruserfinaldata };
        userDataFromState.superliked = 1;
        if (
          this.props.checkIfUserIsProUser &&
          this.props.checkIfUserIsProUser.ProUserDetails &&
          this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
          this.props.UserCoin > 0
        ) {
          this.props.coinReduce(parseInt(superLike.Coin));
        }
        this.setState({
          otheruserfinaldata: userDataFromState,
          _snackbar: true,
          variant: "success",
          usermessage: "Superliked successfull",
        });
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 2000);
      })
      .catch((error) => {
        if (error.status === 405) {
          this.setState({
            _snackbar: true,
            variant: "error",
            usermessage: `You have already liked ${this.state.otheruserfinaldata.firstName}`,
          });
        } else {
          this.setState({
            _snackbar: true,
            variant: "error",
            usermessage: "Something went wrong.",
          });
        }
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 1300);
      });
  };

  giveThumbsUp = (targetId) => {
    GiveThumbsUp(getCookie("token"), targetId)
      .then((res) => {
        if (res.status === 409) {
          this.setState({
            usermessage: `You have already sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            _snackbar: true,
          });
        }
        if (res.status === 200) {
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userDataFromState.isThumbsUpProfile = 1;
          this.setState({
            otheruserfinaldata: userDataFromState,
            usermessage: `You have sent a Thumbs-Up to ${this.state.otheruserfinaldata.firstName}`,
            variant: "success",
            _snackbar: true,
          });
        }
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 2000);
      })
      .catch((err) => console.log("[thumbs up fail]", err));
  };

  likeUser = (userid) => {
    if (this.props.UserProfile.remainsLikesInString > 0 || this.props.UserProfile.remainsLikesInString === "unlimited") {
      LikeNearPeople({ targetUserId: userid })
        .then((data) => {
          let userDataFromState = { ...this.state.otheruserfinaldata };
          userDataFromState.liked = 1;
          this.setState({
            otheruserfinaldata: userDataFromState,
            _snackbar: true,
            variant: "success",
            usermessage: "Profile has been liked successfully",
          });
          setTimeout(() => {
            this.setState({ _snackbar: false });
          }, 2000);
        })
        .catch((error) => {
          if (error.status === 405) {
            this.setState({
              _snackbar: true,
              variant: "error",
              usermessage: `You have already superliked ${this.state.otheruserfinaldata.firstName}`,
            });
          } else {
            this.setState({
              _snackbar: true,
              variant: "error",
              usermessage: "Something went wrong...",
            });
          }
          setTimeout(() => {
            this.setState({ _snackbar: false });
          }, 2000);
        });
    } else {
      this.setState({
        _snackbar: true,
        variant: "error",
        usermessage: "Your Likes count has exhausted",
      });
    }
    setTimeout(() => {
      this.setState({ _snackbar: false });
    }, 2000);
  };

  openChat = () => {
    console.log("clicked");
    this.props.selectedProfileForChat(this.state.otheruserfinaldata);
    this.props.history.push("/app/chat");
    this.props.handlevalue(3);
  };

  recommendFriend = () => {
    navigator.clipboard.writeText(`Get yourself into world of dating. Let's connect on https://app.unfiltered.love`);
    this.setState({
      _snackbar: true,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends now",
    });
    this.handleClosemenu();
  };

  render() {
    const { anchorEl } = this.state;
    let { classes } = this.props;
    const open = Boolean(anchorEl);
    const FriendChatdrawer = <FriendChat onClose={this.closechatdrawer} />;
    let video = this.videoRef && this.videoRef.current;
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      beforeChange: (oldIndex, newIndex) => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
      afterChange: (index) => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
      onSwipe: () => {
        try {
          if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
            video.pause();
          }
        } catch (e) {
          console.log("error", e);
        }
      },
    };

    return (
      <div>
        <MainDrawer width={400} onClose={this.closechatdrawer} onOpen={this.openchatdrawer} open={this.state.friendchat}>
          {FriendChatdrawer}
        </MainDrawer>
        <MainDrawer width={400} onClose={this.toggleMoments} onOpen={this.toggleMoments} open={this.state.momentsScreen}>
          <MobileMoments onClose={this.toggleMoments} UserFinalData={this.state.otheruserfinaldata} selectedPost={this.postInfo} />
        </MainDrawer>
        {/* Header Module */}
        <div className="col-12 py-2 m_oup_header" style={{ zIndex: "9999" }}>
          <div className="row">
            <div className="col-12 text-left">
              <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.handleBackHome} />
            </div>
          </div>
        </div>
        <div className="m_otherUserProfile_afterImage">
          {/* OtherUser Profile Slider Module */}
          <div>
            <Slider {...settings}>
              {/* Video Module */}
              {this.state.otheruserfinaldata ? (
                this.state.otheruserfinaldata && this.state.otheruserfinaldata.profileVideo.length > 0 ? (
                  <div className="p-0 col-12 MotherUser_Video">
                    <video controls ref={this.videoRef}>
                      <source src={this.state.otheruserfinaldata.profileVideo} />
                    </video>
                  </div>
                ) : (
                  " "
                )
              ) : (
                " "
              )}

              {/* Main Profile Pic Moduel */}
              {this.state.otheruserfinaldata ? (
                <img
                  src={this.state.otheruserfinaldata.profilePic}
                  alt={this.state.otheruserfinaldata.firstname}
                  title={this.state.otheruserfinaldata.firstname}
                  height="350px"
                  width="100%"
                  className="otherUserProfileFromChat-profilePic"
                />
              ) : (
                " "
              )}

              {/* Other Image Module */}
              {this.state.otheruserfinaldata &&
              this.state.otheruserfinaldata.otherImages &&
              this.state.otheruserfinaldata.otherImages.length > 0
                ? this.state.otheruserfinaldata.otherImages.map((res, index) => (
                    <div key={index} className="p-0">
                      <div className="user_photos">
                        <img
                          src={res}
                          alt={this.state.otheruserfinaldata.firstName}
                          title={this.state.otheruserfinaldata.firstName}
                          height="350px"
                          className="otherUserProfileFromChat-profilePic"
                          width="100%"
                        />
                      </div>
                    </div>
                  ))
                : " "}
            </Slider>
          </div>

          {/* OtherUser Prefrence Data Module */}
          <div className="col-12 Motheruser_info">
            {/* OtherUser Profile Name Module */}
            <div className="pt-2 row">
              <div className="col-9">
                <h4 className="m-0">
                  <strong style={{ textTransform: "capitalize", color: "#808080" }}>
                    {this.state.otheruserfinaldata.firstName}
                    {/* {this.state.otheruserfinaldata && this.state.otheruserfinaldata.age ? this.state.otheruserfinaldata.age.value : ""} */}
                  </strong>
                  {this.state.otheruserfinaldata.onlineStatus === 1 ? <i className="fas fa-circle" /> : ""}
                </h4>
              </div>

              <div className="col-3 text-right">
                <IconButton
                  aria-label="More"
                  aria-owns={open ? "long-menu" : undefined}
                  aria-haspopup="true"
                  onClick={this.handleClick}
                  className="py-2 px-0"
                  style={{ color: "#f80402" }}
                >
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="long-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={this.handleClosemenu}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: 200,
                      background: "#161616",
                    },
                  }}
                >
                  <MenuItem
                    classes={{
                      root: classes.dropdownOption,
                    }}
                    onClick={this.recommendFriend}
                  >
                    <p className="m-0 m_menuItem">Recommended friend</p>
                  </MenuItem>
                  <MenuItem
                    classes={{
                      root: classes.dropdownOption,
                    }}
                    onClick={this.handleClickOpen}
                  >
                    <p className="m-0 m_menuItem">Report {this.state.otheruserfinaldata.firstName}</p>
                  </MenuItem>
                  <MenuItem>
                    <div
                      className="m-0"
                      classes={{
                        root: classes.dropdownOption,
                      }}
                      onClick={this.handleBlockUser}
                    >
                      {this.state.getblock === "true" ? (
                        <p className="m-0 m_menuItem">UnBlock {this.state.otheruserfinaldata.firstName}</p>
                      ) : (
                        <p className="m-0 m_menuItem">Block {this.state.otheruserfinaldata.firstName}</p>
                      )}
                    </div>
                  </MenuItem>
                </Menu>
              </div>
            </div>

            {/* OtherUser Profile Location Module */}
            <div className="pb-3 border-bottom">
              <p className="m-0" style={{ color: "#a3a3a3" }}>
                {this.state.otheruserfinaldata && this.state.otheruserfinaldata.distance ? (
                  <div className="m_oup_id">
                    <span>ID</span>
                    <span>{this.state.otheruserfinaldata.findMateId}</span>
                    {!this.state.momentsScreen ? (
                      <div className="col-12 pb-3 Motheruser_thumpup">
                        <div className="text-center">
                          <div className="row justify-content-center align-items-center">
                            <div className="m_oup_chat" onClick={this.openChat}>
                              <img src={Icons.NewChat} width={65} height={65} alt="dislike" />
                            </div>
                            <div className="buttons" onClick={() => this.dislikeUser(this.state.otheruserfinaldata._id)}>
                              <img src={Icons.NewDislike} width={65} height={65} alt="dislike" />
                            </div>
                            <div className="buttons" onClick={() => this.likeUser(this.state.otheruserfinaldata._id)}>
                              <img src={Icons.NewLike} width={65} height={65} alt="like" />
                            </div>
                            <div className="buttons" onClick={() => this.superLikeUser(this.state.otheruserfinaldata._id)}>
                              <img src={Icons.NewSuperLike} width={65} height={65} alt="superlike" />
                            </div>
                            <div className="buttons" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                              <img src={Icons.otherUsersThumbsUp} width={22} height={22} alt="other-thumbs-up" />
                            </div>
                          </div>
                          {/* {this.state.otheruserfinaldata.isMatched || this.state.otheruserfinaldata.isFriend ? (
                            <div className="row justify-content-center align-items-center">
                              {this.state.otheruserfinaldata.isThumbsUpProfile === 1 ? (
                                <>
                                  <div className="alredyEnabled_button" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                                    <img src={Icons.alreadyThumbsUpped} width={22} height={22} alt="other-thumbs-up" />
                                  </div>
                                  <div className="m_oup_chat" onClick={this.openChat}>
                                    <img src={Icons.M_Chat} width={25} height={25} alt="dislike" />
                                  </div>
                                  {this.state.otheruserfinaldata.isFriend === 1 || this.state.otheruserfinaldata.friend === 1 ? (
                                    <div className="buttons" onClick={() => this.props.__callInit(true)}>
                                      <img src={Icons.FaceTime} width={25} height={25} alt="video-call" />
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </>
                              ) : (
                                <>
                                  <div className="buttons" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                                    <img src={Icons.otherUsersThumbsUp} width={22} height={22} alt="other-thumbs-up" />
                                  </div>
                                  <div className="buttons" onClick={() => this.openChat()}>
                                    <img src={Icons.M_Chat} width={25} height={25} alt="open-chat" />
                                  </div>
                                  {this.state.otheruserfinaldata.isFriend === 1 || this.state.otheruserfinaldata.friend === 1 ? (
                                    <div className="buttons" onClick={() => this.props.__callInit(true)}>
                                      <img src={Icons.FaceTime} width={25} height={25} alt="video-call" />
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </>
                              )}
                            </div>
                          ) : (
                            <div className="row justify-content-center align-items-center">
                              {this.state.otheruserfinaldata.liked === 1 ? (
                                <div className="alredyEnabled_button">
                                  <img src={Icons.otherUsersAlreadyLiked} width={25} height={25} alt="already-liked" />
                                </div>
                              ) : (
                                <div className="buttons" onClick={() => this.likeUser(this.state.otheruserfinaldata._id)}>
                                  <img src={Images.otherProfileLike} width={25} height={25} alt="like" />
                                </div>
                              )}
                              <div className="m_oup_chat" onClick={this.openChat}>
                                <img src={Icons.M_Chat} width={25} height={25} alt="dislike" />
                              </div>
                              <div className="buttons" onClick={() => this.dislikeUser(this.state.otheruserfinaldata._id)}>
                                <img src={Images.otherProfileDislike} width={25} height={25} alt="dislike" />
                              </div>
                              {this.state.otheruserfinaldata.superliked === 1 ? (
                                <div className="alredyEnabled_button">
                                  <img src={Icons.alreadySuperLiked} width={25} height={25} alt="already-superliked" />
                                </div>
                              ) : (
                                <div className="buttons" onClick={() => this.superLikeUser(this.state.otheruserfinaldata._id)}>
                                  <img src={Images.otherProfileSuperlike} width={22} height={22} alt="superlike" />
                                </div>
                              )}
                              {this.state.otheruserfinaldata.isThumbsUpProfile === 1 ? (
                                <div className="alredyEnabled_button" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                                  <img src={Icons.alreadyThumbsUpped} width={22} height={22} alt="other-thumbs-up" />
                                </div>
                              ) : (
                                <div className="buttons" onClick={() => this.giveThumbsUp(this.state.otheruserfinaldata._id)}>
                                  <img src={Icons.otherUsersThumbsUp} width={22} height={22} alt="other-thumbs-up" />
                                </div>
                              )}
                            </div>
                          )} */}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                ) : (
                  ""
                )}
              </p>
            </div>

            {/* OtherUser Prefence Module */}
            {this.state.otheruserfinaldata &&
              this.state.otheruserfinaldata.myPreferences.map((data, index) => (
                <div key={index} className="m-0 py-3 border-bottom">
                  {/* OtherUser Prefrence Header Module */}
                  <div className="m_userpro_header">
                    <h5>{data.title}</h5>
                  </div>
                  {data.data.map((pref, data) => (
                    // Otheruser prefrence Body Module
                    <div key={"OtherUserProfile" + data} className="py-2">
                      <div className="m-0 row">
                        <div className="p-0 col-4 Mbasic">
                          <p className="m-0 ">{pref.label} :</p>
                        </div>
                        <div className="p-0 col-8 Mbasicanswer">
                          <p className="m-0 text-right ">
                            {pref.selectedValues && pref.selectedValues.length > 0 ? pref.selectedValues.join(",") : "N/A"}
                          </p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ))}

            <div className="py-3 border-bottom col-12 px-0">
              <div className="row py-2">
                <div className="col-6">
                  <div className="m_userpro_header">
                    <h5>Moments</h5>
                  </div>
                </div>
                <div className="col-6 m_moments_view_all" onClick={() => this.toggleMoments()}>
                  View All
                </div>
              </div>

              <div className="row mx-0">
                {this.state.otheruserfinaldata && this.state.otheruserfinaldata.moments.length > 0 ? (
                  this.state.otheruserfinaldata.moments.slice(0, 6).map((k, i) => (
                    <div
                      className="col-4 m_moments_boxes"
                      id="m_moments_boxes"
                      key={i}
                      style={{ height: this.state.squareWH }}
                      onClick={() => this.toggleMoments(k)}
                    >
                      {k.url[0].includes("mp4") ? (
                        <video src={k.url[0]} alt={k.description}></video>
                      ) : (
                        <img src={k.url[0]} alt={k.description} />
                      )}
                    </div>
                  ))
                ) : (
                  <div>No Moments</div>
                )}
              </div>
            </div>

            {/* Share && Report User Profile Module */}
            <div className="m-0 pt-3 d-block row m_rec_report_user">
              <div className="share_profile">
                <p className="m-0" onClick={this.recommendFriend}>
                  <i className="fas fa-share-alt pr-2" />
                  <span>RECOMMEND TO A FRIEND</span>
                </p>
                <p className="m-0" onClick={this.handleClickOpen}>
                  <img src={Icons.danger} alt="danger" height={18} />
                  <span>REPORT {this.state.otheruserfinaldata.firstName}</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <MaterialModal toggle={this.handleClosedialog} isOpen={this.state.open} width={"90%"}>
          <div className="col-12 m_reportReasonsModal">
            <div>Report User</div>
            {this.state.blockreason.length > 0
              ? this.state.blockreason.map((k) => (
                  <div key={k} onClick={() => this.handlereportuser(k)}>
                    {k}
                  </div>
                ))
              : ""}
            <div onClick={this.handleClosedialog}>Cancel</div>
          </div>
        </MaterialModal>
        {/* Error Message For not Resgister Number Model */}
        <MainModel isOpen={this.state.modelchat}>
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="pt-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.togglemodel} />
              <div className="WCharge_coins">
                <i className="fas fa-coins" />
              </div>
              <h4>Insufficient Coin Balance.</h4>
              <p className="m-0">You need at least 25 coin to chat with user and your current balance is {this.props.CoinBalance}.</p>
              <div className="Wcharge_wallet">
                <button>
                  <Link to="/app/coinbalance">Buy Credits</Link>
                </button>
              </div>
            </div>
          </div>
        </MainModel>

        {/* Snakbar Components */}
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state._snackbar}
          timeout={2500}
          onClose={this.handleClosesnakbar}
        />
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    checkIfUserIsProUser: state.ProUser,
    CoinConfig: state.CoinConfig.CoinConfig,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    __callInit: (data) => dispatch(__callInit(data)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(otherUserMobileProfile)));
