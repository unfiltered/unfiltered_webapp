import React from "react";
import {
  getAllPosts,
  likeOrDislikePost,
  deletePost,
  getReportPostReasons,
  sumbitPostReport,
} from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import { __BlobUpload } from "../../../../lib/cloudinary-image-upload";
import "./MobileNewsFeed.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import moment from "moment";
import MobileComments from "./MobileComments";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { FormattedMessage } from "react-intl";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
// import MaterialDropdown from "../../../../Components/Dropdown/MaterialDropdown";
import TextField from "../../../../Components/Input/TextField";
import { MenuItem } from "@material-ui/core";
import Button from "../../../../Components/Button/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import * as PostActions from "../../../../actions/Posts";
import { connect } from "react-redux";
import Loader from "../../../../Components/Loader/Loader";

class MobileNewsFeed extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.state = {
      start: 0,
      end: 10,
      posts: [],
      loader: true,
      viewMore: false,
      commentsDrawer: false,
      modal: null,
      singlePostData: {},
      reportReasons: [],
      reportReason: "", // single reason
      reportModal: false,
      reportWithText: "",
      reportWithReason: false, // final report modal
      loader: false,
    };
  }

  handleReportText = (e) => {
    this.setState({ reportWithText: e.target.value });
  };

  sumbitReport = () => {
    sumbitPostReport(getCookie("token"), this.state.singlePostData.postId, this.state.reportReason, this.state.reportWithText)
      .then((res) => {
        console.log("[sumbitPostReport]", res);
        this.toggleReportWithReason();
      })
      .catch((err) => console.log("sumbitPostReport err", err));
  };

  toggleReportWithReason = () => {
    this.setState({ reportWithReason: !this.state.reportWithReason });
  };

  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  functionToLikeOrDislike = (type, postId) => {
    likeOrDislikePost(getCookie("token"), type, postId)
      .then((res) => {
        this.props.m_likeDislikePost(postId);
      })
      .catch((err) => console.log("functionToLikeOrDislike", err));
  };

  toggleCommentsDrawer = (data) => {
    if (data) {
      this.setState({ singlePostData: data, commentsDrawer: !this.state.commentsDrawer });
    }
    this.setState({ commentsDrawer: !this.state.commentsDrawer });
  };

  updateCommentCount = (postId) => {
    this.props.m_updateCommentCount(postId);
  };

  componentDidMount() {
    let arr = [];
    this.setState({ loader: true });
    this.scrollRef.current.addEventListener("scroll", this.handleScroll, true);
    this.GetAllPosts(this.state.start, this.state.end);
    getReportPostReasons(getCookie("token"))
      .then((res) => {
        res.data.data.map((k) => arr.push({ value: k, label: k }));
        this.setState({ reportReasons: arr });
      })
      .catch((err) => console.log("err"));
  }

  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        this.setState({ start: this.state.start + 10, end: this.state.end + 10, viewMore: false }, () => {
          this.GetAllPosts(this.state.start, this.state.end);
        });
      }
    }
  };

  componentWillUnmount() {
    this.scrollRef.current.removeEventListener("scroll", this.handleScroll, true);
    this.props.m_storeAllPosts([]);
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  toggleDeletePostModal = () => {
    deletePost(getCookie("token"), this.state.singlePostData.postId)
      .then((res) => {
        this.props.m_deleteSinglePost(this.state.singlePostData.postId);
        this.setState({ modal: false });
        console.log("post deleted success");
      })
      .catch((err) => console.log("err deleting post"));
  };

  GetAllPosts = (start, end) => {
    getAllPosts(getCookie("token"), start, end)
      .then((res) => {
        let oldData = [...this.props.m_allPosts, ...res.data.data];
        this.props.m_storeAllPosts(oldData);
        this.setState({ loader: false });
      })
      .catch((err) => {
        this.setState({ loader: false });
      });
  };

  toggleReportPostModal = (singleReason) => {
    if (singleReason) {
      this.setState({ reportReason: singleReason, reportModal: !this.state.reportModal });
      setTimeout(() => {
        this.toggleReportWithReason();
      }, 200);
    } else {
      this.setState({ reportModal: !this.state.reportModal });
    }
  };

  render() {
    const { anchorEl } = this.state;
    return (
      <div ref={this.scrollRef} className="m_nf_view">
        <MainDrawer width={400} onClose={this.toggleCommentsDrawer} onOpen={this.toggleCommentsDrawer} open={this.state.commentsDrawer}>
          <MobileComments
            updateCommentCount={this.updateCommentCount}
            UserFinalData={this.props.UserFinalData}
            singlePostData={this.state.singlePostData}
            onClose={this.toggleCommentsDrawer}
          />
        </MainDrawer>
        {this.state.loader ? <Loader text={"Loading..."} /> : ""}
        {this.props.m_allPosts && this.props.m_allPosts.length > 0 ? (
          this.props.m_allPosts
            .map((k) => (
              <div className="col-12 px-0 py-2" key={k._id}>
                <div className="row mx-0 pb-1 align-items-center">
                  <div className="col-1 m_nf_profilePic">
                    <img src={k.profilePic} alt={k.userName} />
                  </div>
                  <div className="col-9  m_nf_postDetails pl-4">
                    <div>{k.userName}</div>
                    <div>{moment(k.postedOn).fromNow()}</div>
                  </div>
                  <div className="col-2 m_nf_menuDropDown">
                    <IconButton
                      aria-label="More"
                      onClick={() => {
                        this.toggleModal();
                        this.setState({ singlePostData: k });
                      }}
                    >
                      <MoreVertIcon />
                    </IconButton>
                  </div>
                </div>
                <div className="row mx-0 m_nf_postImageVideo">
                  {k.url[0].includes("mp4") ? (
                    <video width={"100%"} height="500" controls>
                      <source src={k.url[0]} type="video/mp4" />
                    </video>
                  ) : (
                    <img src={k.url[0]} alt={k.description} />
                  )}
                </div>
                <div className="row mx-0 pt-2">
                  <div
                    className="pl-2"
                    onClick={() => (k.liked ? this.functionToLikeOrDislike(2, k.postId) : this.functionToLikeOrDislike(1, k.postId))}
                  >
                    <img src={k.liked ? Icons.likedLogo1 : Icons.likedLogo} height={20} width={20} alt="like" />
                  </div>
                  <div className="pl-2" onClick={() => this.toggleCommentsDrawer(k)}>
                    <img className="m_nf_likeBtn" src={Icons.commentLogo} height={19} width={20} alt="comment" />
                  </div>
                </div>
                <div className="row mx-0 pt-1">
                  <span className="pl-2 m_nf_commentNLikes">{k.likeCount} likes</span>{" "}
                  <span className="pl-2 m_nf_commentNLikes">{k.commentCount > 0 ? k.commentCount + " comments" : "No Comments"}</span>
                </div>
                <div className="row mx-0">
                  <span className="pl-2 m_nf_postDesc">{k.description}</span>
                </div>
              </div>
            ))
            .sort(function (a, b) {
              return b.timstamp - a.timestamp;
            })
        ) : this.state.loader ? (
          <span />
        ) : (
          <div className="col-12 h-75">
            <div className="row h-100 justify-content-center align-items-center flex-column">
              <div>
                <img src={Icons.U_Alert} alt="sad-face" width={200} />
              </div>
              <div className="m_nf_noNewsFeed1">You have no news feed.</div>
              <div className="m_nf_noNewsFeed2">You have no News feed. Please post your moment.</div>
            </div>
          </div>
        )}
        <MaterialModal width={150} toggle={this.toggleModal} isOpen={this.state.modal}>
          {this.state.singlePostData.userName === this.props.UserFinalData.firstName ? (
            <MenuItem
              className="m_nf_menutItem border-bottom"
              onClick={() => {
                this.toggleDeletePostModal();
              }}
            >
              <span>
                <FormattedMessage id="message.deletePost" />
              </span>
            </MenuItem>
          ) : (
            <span />
          )}
          {this.state.singlePostData.userName !== this.props.UserFinalData.firstName ? (
            <>
              <MenuItem
                onClick={() => {
                  this.toggleModal();
                  setTimeout(() => {
                    this.toggleReportPostModal();
                  }, 200);
                }}
                className="m_nf_menutItem border-bottom"
              >
                <span>
                  <FormattedMessage id="message.reportPost" />
                </span>
              </MenuItem>
              <MenuItem onClick={() => this.toggleModal()} className="m_nf_menutItem border-bottom">
                <span>Cancel</span>
              </MenuItem>
            </>
          ) : (
            <span />
          )}
          {this.state.singlePostData.userName === this.props.UserFinalData.firstName ? (
            <MenuItem
              className="m_nf_menutItem"
              onClick={() => {
                this.setState({ anchorEl: false });
              }}
            >
              <span>Edit Post</span>
            </MenuItem>
          ) : (
            <span />
          )}
        </MaterialModal>
        <MaterialModal width={150} toggle={this.toggleReportPostModal} isOpen={this.state.reportModal}>
          {this.state.reportReasons.map((k) => (
            <MenuItem key={k.value} onClick={() => this.toggleReportPostModal(k.value)} className="m_nf_menutItem border-bottom">
              <span>{k.value}</span>
            </MenuItem>
          ))}
        </MaterialModal>
        <MaterialModal width={"70%"} toggle={this.toggleReportWithReason} isOpen={this.state.reportWithReason}>
          <div className="col-12 py-3">
            <div className="row m_nf_reportModal">
              <div className="col-12 text-center">
                Reason Selected: <span>{this.state.reportReason}</span>
              </div>
              <div className="col-12">
                <TextField onChange={this.handleReportText} placeholder="Enter Report Message" />
              </div>
              <div className="col-12">
                <div className="row">
                  <Button text="Submit" handler={this.sumbitReport} />
                  <Button text="Cancel" handler={this.toggleReportWithReason} />
                </div>
              </div>
            </div>
          </div>
        </MaterialModal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    m_allPosts: state.Posts.Posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    m_storeAllPosts: (data) => dispatch(PostActions.storeAllPosts(data)),
    m_deleteSinglePost: (postId) => dispatch(PostActions.deleteSinglePost(postId)),
    m_likeDislikePost: (postId) => dispatch(PostActions.likeDislikePost(postId)),
    m_updateCommentCount: (postId) => dispatch(PostActions.addComment(postId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MobileNewsFeed);
