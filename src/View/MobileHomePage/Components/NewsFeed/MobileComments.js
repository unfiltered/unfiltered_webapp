import React from "react";
import "./MobileNewsFeed.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import Button from "../../../../Components/Button/Button";
import Input from "../../../../Components/Input/Input";
import { getCookie } from "../../../../lib/session";
import moment from "moment";
import { getCommentsForSelectedPostWithPagination, addCommentForSelectedPost } from "../../../../controller/auth/verification";
import Snackbar from "../../../../Components/Snackbar/Snackbar";

class MobileComments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      offset: 0,
      limit: 20,
      comments: [],
      variant: "success",
      usermessage: "",
      open: false,
    };
  }
  onChangeListener = (e) => {
    this.setState({ text: e.target.value });
  };

  componentDidMount() {
    this.getCommentsFunc(this.props.singlePostData.postId, this.state.offset, this.state.limit);
  }

  getCommentsFunc = (PostId, offset, limit) => {
    getCommentsForSelectedPostWithPagination(getCookie("token"), PostId, offset, limit)
      .then((res) => {
        if (this.state.comments.length % 20 === 0) {
          this.setState({ comments: [...this.state.comments, ...res.data.data] });
        } else {
          this.setState({ comments: [...res.data.data] });
        }
      })
      .catch((err) => console.log("err"));
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  clearText = () => {
    this.setState({ text: "" });
  };

  submitComment = () => {
    let allComments = [...this.state.comments];
    if (this.state.text === "" || this.state.text.length === 0) {
      this.setState({ text: "", open: true, variant: "warning", usermessage: "Comment cannot be empty..." });
      return;
    } else if (this.state.text.length > 0) {
      addCommentForSelectedPost(getCookie("token"), this.props.singlePostData.postId, this.state.text)
        .then((res) => {
          allComments.push({
            comment: this.state.text,
            commentedOn: new Date().getTime(),
            commenterName: this.props.UserFinalData.firstName,
            profilePic: this.props.UserFinalData.profilePic,
          });
          this.clearText();
          document.getElementById("form").reset();
          this.props.updateCommentCount(this.props.singlePostData.postId);
          this.setState({ comments: allComments, open: true, usermessage: "Comment added successfully." });
        })
        .catch((err) => {});
    }
  };
  render() {
    return (
      <div className="col-12 px-0 m_nf_comments">
        <div className="row mx-0">
          <div className="m_comments_screen_header col-12 py-3">
            <div className="row">
              <div
                className="col-4"
                onClick={() => {
                  this.props.onClose();
                }}
              >
                <img src={Icons.PinkBack} width={25} height={20} alt="back-button" />
              </div>
              <div className="col-4 commentsScreenHeader">Comments</div>
              <div className="col-4"></div>
            </div>
          </div>
        </div>
        <div className="row m_nf_commentSection mx-0">
          <div className="col-12">
            <div className="row">
              {this.state.comments.length > 0 ? (
                this.state.comments
                  .map((k) => (
                    <div className="col-12 py-2" key={k.commentedOn}>
                      <div className="row m_nf_singleCommentsView">
                        <div>
                          <img src={k.profilePic} alt={k.commenterName} height={35} width={35} />
                        </div>
                        <div>
                          <div>
                            <span>{k.commenterName}</span>
                            <span>{k.comment}</span>
                          </div>
                          <div>{moment(k.commentedOn).fromNow()}</div>
                        </div>
                      </div>
                    </div>
                  ))
                  .reverse()
              ) : (
                <div className="row w-100 mx-0 h-100 m_nf_noComments justify-content-center align-items-center">No Comments Yet.</div>
              )}
            </div>
          </div>
        </div>
        <div className="row m_nf_commentPost mx-0">
          <div>
            <img
              src={this.props && this.props.UserFinalData && this.props.UserFinalData.profilePic}
              alt={this.props && this.props.UserFinalData && this.props.UserFinalData.firstName}
              height={40}
              width={40}
            />
          </div>
          <div>
            <form id="form">
              <Input placeholder="Add a comment..." onChange={this.onChangeListener} />
            </form>
          </div>
          <div className="m_nf_postButton">
            <Button
              text="Post"
              className="m_nf_submit_comment"
              handler={() => {
                this.submitComment();
              }}
            />
          </div>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          timeout={2000}
          onClose={this.handleClose}
        />
      </div>
    );
  }
}

export default MobileComments;
