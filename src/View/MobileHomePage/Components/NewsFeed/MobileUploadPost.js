import React from "react";
import ImageCropperUploadImage from "../../../../Components/ImageCropper_UploadImage/App";
import { Icons } from "../../../../Components/Icons/Icons";
import "./MobileNewsFeed.scss";
import { getCookie } from "../../../../lib/session";
import Input from "../../../../Components/Input/Input";
import Button from "../../../../Components/Button/Button";
import { addNewPost } from "../../../../controller/auth/verification";
import { __BlobUpload } from "../../../../lib/cloudinary-image-upload";
import * as PostActions from "../../../../actions/Posts";
import { connect } from "react-redux";

class MobileUploadPost extends React.Component {
  state = {
    finalUploadDrawer: true,
    gotData: false,
    postText: "",
  };

  onChangeListener = (e) => {
    this.setState({ postText: e.target.value });
  };

  setGotImageData = (bool) => {
    this.setState({ gotData: bool });
  };

  postNewStory = () => {
    this.props.m_updatePostStatus("Please wait while post is being uploaded...", true);
    if (this.props.imageArray && this.props.imageArray.length > 0) {
      let type;
      if (
        this.props.imageArray[0].fileDetails.type === "image/png" ||
        this.props.imageArray[0].fileDetails.type === "image/jpg" ||
        this.props.imageArray[0].fileDetails.type === "image/jpeg"
      ) {
        type = 3;
      } else {
        type = 4;
      }
      __BlobUpload(this.props.imageArray[0].fileDetails)
        .then((response) => {
          addNewPost(getCookie("token"), type, this.state.postText, response.body.secure_url)
            .then((res) => {
              console.log("success uploading post on server", res);
              let obj = {
                commentCount: 0,
                description: this.state.postText,
                likeCount: 0,
                liked: false,
                postId: res.data._id,
                postedOn: new Date().getTime(),
                userName: this.props && this.props.UserFinalData.firstName,
                profilePic: this.props && this.props.UserFinalData.profilePic,
                url: [response.body.secure_url],
                typeFlag: type,
              };
              this.props.m_addNewPost(obj);
              this.props.closeImageCropModal();
              this.props.m_updatePostStatus("", false);
            })
            .catch((err) => {
              console.log("err uploading post on server", err);
              this.props.m_updatePostStatus("", false);
            });
        })
        .catch((err) => {
          console.log("err uploading post on server", err);
          this.props.m_updatePostStatus("", false);
        });
    }
  };

  render() {
    return (
      <div className="col-12 px-0 h-100" style={{ overflow: "hidden" }}>
        <div className="row mx-0 py-2">
          <span className="pl-3" onClick={this.props.closeImageCropModal}>
            <img src={Icons.PinkCloseBtn} alt="close-btn" height={17} width={17} />
          </span>
          <span className="m_nf_createPostHeader pl-3">Create Post</span>
        </div>
        <ImageCropperUploadImage
          propImage={this.props && this.props.croppedImageObjectToUploadAfterClickingPinkBox} // image
          toggle={this.props.closeImageCropModal} // to close
          signup={false}
          isMobile={true}
          onlyGenerateBlob={true}
          gotData={this.state.gotData}
          setGotImageData={this.setGotImageData}
          addNewPostPreviewForCustomModal={this.props && this.props.addNewPostPreviewForCustomModal}
        />
        {this.state.gotData ? (
          <div className="uploadPostInput">
            <Input onChange={this.onChangeListener} placeholder="Write a caption" />
          </div>
        ) : (
          <span />
        )}
        {this.state.gotData ? (
          <div className="m_nf_postUpload">
            <Button handler={this.postNewStory} text={<img src={Icons.UploadPost} height={70} width={70} alt="send-btn" />} />
          </div>
        ) : (
          <span />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    m_addNewPost: (data) => dispatch(PostActions.addNewPost(data)),
    m_updatePostStatus: (text, bool) => dispatch(PostActions.uploadingPostStatus(text, bool)),
  };
};

export default connect(null, mapDispatchToProps)(MobileUploadPost);
