import React from "react";
import "./Friends.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import { findById, sendRequestToFriendById } from "../../../../controller/auth/verification";
import Input from "../../../../Components/Input/Input";
import { Images } from "../../../../Components/Images/Images";
import { getCookie } from "../../../../lib/session";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import Button from "../../../../Components/Button/Button";
import { keys } from "../../../../lib/keys";

class AddFriendWInput extends React.Component {
  state = {
    userId: "",
    userFound: false,
    userFoundOnSumbit: false,
    userFoundData: {},
  };
  onChangeListener = (e) => {
    this.setState({ userId: e.target.value });
  };
  onSubmit = (e) => {
    e.preventDefault();
    findById(getCookie("token"), this.state.userId)
      .then((res) => {
        let response = res.data.data;
        if (Object.keys(response).length > 1) {
          console.log("res.data.data", res.data.data);
          this.setState({ userFoundData: res.data.data, userFound: true });
        } else {
          this.setState({ userFound: false, userFoundOnSumbit: true });
        }
      })
      .catch((err) => {
        this.setState({ userFound: false, userFoundOnSumbit: true });
      });
  };

  sendFriendRequest = () => {
    sendRequestToFriendById(getCookie("token"), this.state.userFoundData._id)
      .then((res) => {
        if (res.status === 409) {
          this.setState({ variant: "success", usermessage: "You've already sent friend request", open: true });
        } else if (res.status === 200) {
          let obj = { ...this.state.userFoundData };
          obj["isPenddingFriendRequest"] = true;
          this.setState({ variant: "success", userFoundData: obj, usermessage: "Friend Request Sent", open: true });
        }
      })
      .catch((err) => {
        this.setState({ variant: "error", usermessage: "Cannot Send Friend Request", open: true });
      });
    setTimeout(() => {
      this.setState({ open: false });
    }, 2500);
  };

  render() {
    return (
      <div className="col-12 addFriendWithInput">
        <div className="row">
          <div className="col-12 py-3 m_addFriend_back_row">
            <div className="row">
              <div className="col-4" onClick={this.props.onClose}>
                <img src={Icons.PinkBack} alt="Back" height={20} width={25} />
              </div>
            </div>
          </div>
          <div className="m_addFriend_scrollView">
            <div className="col-12 m-label-common1">Add Friends</div>
            <div className="col-12 label-common2">by Unfiltered ID</div>
            <form className="col-12 pt-4 addFriendInput" onSubmit={this.onSubmit}>
              <span>
                <img src={Images.search} alt="search" />
              </span>
              <Input placeholder="Search ID" onChange={this.onChangeListener} />
            </form>
            {this.state.userFound ? (
              <div className="col-12 pt-3">
                <div className="row sendRequestInfo">
                  <div className="col-auto">
                    <img src={this.state.userFoundData.profilePic} alt={this.state.userFoundData.profilePic} />
                  </div>
                  <div className="col-5">{this.state.userFoundData.firstName}</div>
                  <div className="col-4">
                    {this.state.userFoundData.isPenddingFriendRequest && !this.state.userFoundData.IsFriend ? (
                      <div className="sentReq" onClick={this.sendFriendRequest}>
                        Requested
                      </div>
                    ) : (
                      <div className="reqSent" onClick={this.sendFriendRequest}>
                        Add Friend
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ) : (
              <div className="col-12 pt-3 text-center friendNotFound">
                {this.state.userFoundOnSumbit ? "Sorry, there are no results by that ID." : <span />}
              </div>
            )}
            {/* <div className="col-12 text-right">
              <Button
                text="Search"
                handler={this.onSubmit}
                className={this.state.userId.length < 3 ? "m_searchBtn-disabled" : "m_searchBtn"}
                disabled={this.state.userId.length < 3}
              />
            </div> */}
          </div>
        </div>

        <div className="myId">
          <span>My {keys.AppName} ID: </span> <span>{this.props.UserFinalData.findMateId}</span>
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default AddFriendWInput;
