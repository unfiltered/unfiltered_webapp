import React from "react";
import "./Friends.scss";
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";
import { Icons } from "../../../../Components/Icons/Icons";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import AddFriend from "./AddFriend";
import { GetAllFriendsRequest, GetAllFriends, acceptOrRejectFriendRequest } from "../../../../controller/auth/verification";
import { connect } from "react-redux";
import AppSettings from "../Settings/Setings";
import Button from "../../../../Components/Button/Button";
import { getCookie } from "../../../../lib/session";
import { __updateMqttMessage } from "../../../../actions/webrtc";
import { CircularProgress } from "@material-ui/core";
import { FRIEND_REQUEST_COUNT_FUNC } from "../../../../actions/Friends";
import Input from "../../../../Components/Input/Input";
import ReactCountryFlag from "react-country-flag";

class Friends extends React.Component {
  state = {
    allFriends: [],
    tempAllFriends: [],
    allRequests: [],
    tempAllRequests: [],
    loader: true,
    settingsDrawer: false,
    addFriendDrawer: false,
    value: 0,
    openSearch: false,
    searchedValue: "",
  };
  toggleAddFriendDrawer = () => {
    this.setState({ addFriendDrawer: !this.state.addFriendDrawer });
  };

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  setValue = (index) => {
    this.setState({ value: index });
  };

  componentDidMount() {
    this.props.handlevalue(4);
    GetAllFriends()
      .then((data) => {
        this.setState({
          allFriends: data.data.data,
          loader: false,
          tempAllFriends: data.data.data,
        });
      })
      .catch((err) => this.setState({ allFriends: [], loader: false }));
    GetAllFriendsRequest()
      .then((data) => {
        this.setState({
          allRequests: data.data.data,
          loader: false,
          tempAllRequests: data.data.data,
        });
      })
      .catch((err) => this.setState({ allRequests: [], loader: false }));
  }

  toggleSettingsDrawer = () => {
    this.setState({ settingsDrawer: !this.state.settingsDrawer });
  };

  toggleSearch = () => {
    console.log(this.nameInput);
    this.setState({ openSearch: !this.state.openSearch });
  };

  acceptOrRejectRequest = (findMateId, type) => {
    acceptOrRejectFriendRequest(getCookie("token"), findMateId, type)
      .then((res) => {
        GetAllFriendsRequest().then((data) => {
          this.setState({ allRequests: data.data.data });
        });
        GetAllFriends().then((data) => {
          this.setState({ allFriends: data.data.data });
        });
      })
      .catch((err) => console.log("err"));
  };

  componentWillReceiveProps(nextProps, prevState) {
    if (
      this.props.FriendRequests &&
      this.props.FriendRequests.payload &&
      this.props.FriendRequests.payload[0] &&
      this.props.FriendRequests.payload[0].senderdetails
    ) {
      GetAllFriendsRequest().then((data) => {
        console.log("execute");
        this.setState({ allRequests: data.data.data }, () => {
          this.props.__updateMqttMessageFunc("");
          return null;
        });
        return null;
      });
    }
  }

  filterList = (event) => {
    if (this.state.value === 0) {
      var updatedList = [...this.state.tempAllFriends];
      updatedList = updatedList.filter((item) => item.firstName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
      this.setState({ allFriends: updatedList });
    } else {
      var updatedList = [...this.state.tempAllRequests];
      updatedList = updatedList.filter((item) => item.firstName.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
      this.setState({ allRequests: updatedList });
    }
  };

  render() {
    const allFriends = this.state.loader ? (
      <div className="h-100 w-100 d-flex justify-content-center align-items-center flex-row">
        <CircularProgress />
        <span className="pl-2">Loading...</span>
      </div>
    ) : this.state.allFriends && this.state.allFriends.length > 0 ? (
      this.state.allFriends.map((k) => (
        <div className="col-12 py-2" key={k._id}>
          <div className="row m_all_request_row">
            <div className="friendsTab_picture">
              <img src={k.profilePic} alt={k.firstName} height={60} width={60} />
            </div>
            <div className="friendsTab_info">
              <div>{k.firstName}</div>
              <span>
                <ReactCountryFlag
                  countryCode={k && k.address && k.address.countryCode}
                  svg
                  title={k && k.address && k.address.countryCode}
                  style={{
                    width: "20px",
                    height: "20px",
                  }}
                />
              </span>
              <span className="pl-1">
                {k.address.city}, {k.address.country}
              </span>
              <div className="m_findMateID">{k.findMateId}</div>
            </div>
          </div>
        </div>
      ))
    ) : (
      <div className="col-12 text-center h-75 align-items-center flex-column d-flex justify-content-center">
        <div>
          <img src={Icons.U_Visitor} alt="no-friends" width={200} />
        </div>
        <div className="friendsTabNoData pt-2">You have no Friends.</div>
      </div>
    );
    const allRequests = this.state.loader ? (
      <div className="h-100 w-100 d-flex justify-content-center align-items-center flex-row">
        <CircularProgress />
        <span className="pl-2">Loading...</span>
      </div>
    ) : this.state.allRequests && this.state.allRequests.length > 0 ? (
      this.state.allRequests.map((k) => (
        <div className="col-12 py-2" key={k._id}>
          <div className="row m_all_request_row">
            <div className="friendsTab_picture">
              <img src={k.profilePic} alt={k.firstName} height={60} width={60} />
            </div>
            <div className="friendsTab_info">
              <div>{k.firstName}</div>
              <span>
                {" "}
                <ReactCountryFlag
                  countryCode={k && k.address && k.address.countryCode}
                  svg
                  style={{
                    width: "20px",
                    height: "20px",
                  }}
                  title={k && k.address && k.address.countryCode}
                />
              </span>
              <span className="pl-1">
                {k.address.city}, {k.address.country}
              </span>
              <div>{k.findMateId}</div>
            </div>
            <div className="m_f_acceptRejectReq">
              <Button
                text="Confirm"
                handler={() => {
                  this.acceptOrRejectRequest(k._id, 1);
                  this.props.storeFriendRequestCount(this.props.friendReqCount - 1);
                }}
              />
              <Button
                text="Ignore"
                handler={() => {
                  this.props.storeFriendRequestCount(this.props.friendReqCount - 1);
                  this.acceptOrRejectRequest(k._id, 2);
                }}
              />
            </div>
          </div>
        </div>
      ))
    ) : (
      <div className="col-12 text-center h-75 d-flex align-items-center flex-column justify-content-center">
        <div>
          <img src={Icons.U_Visitor} alt="no-friends" width={200} />
        </div>
        <div className="friendsTabNoData pt-2">You have no Friend requests yet.</div>
      </div>
    );
    return (
      <div className="col-12 friendsView">
        <MainDrawer onClose={this.toggleAddFriendDrawer} onOpen={this.toggleAddFriendDrawer} open={this.state.addFriendDrawer}>
          <AddFriend onClose={this.toggleAddFriendDrawer} UserFinalData={this.props.UserFinalData} />
        </MainDrawer>
        <MainDrawer onClose={this.toggleSettingsDrawer} onOpen={this.toggleSettingsDrawer} open={this.state.settingsDrawer}>
          <AppSettings onClose={this.toggleSettingsDrawer} />
        </MainDrawer>
        <div className="row">
          {this.state.openSearch ? (
            <div className="col-12 py-3 m_friends_screen_header">
              <div className="row">
                <div className="col-1">
                  <img src={Icons.TabDiscoverActive} alt="search" height={23} width={23} onClick={this.toggleSearch} />
                </div>
                <div className="col-11 m_hidden_search_slider">
                  <Input
                    autoFocus
                    placeholder={`Search from ${this.state.value === 0 ? "Friends" : "Requests"}`}
                    onChange={(e) => this.filterList(e)}
                  />
                </div>
              </div>
            </div>
          ) : (
            <div className="col-12 py-3 m_friends_screen_header">
              <div className="row">
                <div className="col-4">
                  <img src={Icons.TabDiscoverActive} alt="search" height={23} width={23} onClick={this.toggleSearch} />
                </div>
                <div className="col-4 friends_header">Friends</div>
                <div className="col-4">
                  <div className="row header_row">
                    <div className="col-8 px-0 d-flex justify-content-end" onClick={this.toggleAddFriendDrawer}>
                      <img
                        src={Icons.MobileAddFriend}
                        height={24}
                        width={24}
                        style={{ marginTop: "1px" }}
                        // className="img-fluid"
                        alt="dollarimg"
                        title="dollarimg"
                      />
                    </div>
                    <div className="col-4" onClick={this.toggleSettingsDrawer}>
                      <img
                        style={{ position: "absolute", bottom: "-2px" }}
                        src={this.props && this.props.userProfilePicture}
                        alt={this.props.UserFinalData.firstName}
                        height={30}
                        width={30}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="friendsTab w-100">
            <SimpleTabs
              mobile={true}
              setValue={this.setValue}
              value={this.state.value}
              allFriends={this.state.allFriends && this.state.allFriends.length}
              tabs={[{ label: "Friends" }, { label: "Requests" }]}
              tabContent={[{ content: allFriends }, { content: allRequests }]}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    FriendRequests: state.webrtc.lastMqttMsg,
    CoinBalance: state.UserProfile.CoinBalance,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
    friendReqCount: state.Friends.FriendRequest,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    __updateMqttMessageFunc: (data) => dispatch(__updateMqttMessage(data)),
    storeFriendRequestCount: (count) => dispatch(FRIEND_REQUEST_COUNT_FUNC(count)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Friends);
