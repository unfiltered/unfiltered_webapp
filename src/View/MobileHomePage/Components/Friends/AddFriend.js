import React from "react";
import "./Friends.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import AddFriendWInput from "./AddFriendWInput";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { keys } from "../../../../lib/keys";

class AddFriend extends React.Component {
  state = {
    addFriendWInputPage: false,
    open: false,
    usermessage: "",
    variant: "success",
  };
  toggleAddFriendWInputPage = () => {
    this.setState({ addFriendWInputPage: !this.state.addFriendWInputPage });
  };
  shareAppInfo = () => {
    navigator.clipboard.writeText(`Invite your friends into our new dating app, https://app.unfiltered.love`);
    this.setState({ open: true, usermessage: "Share Link with your friends." });
    setTimeout(() => {
      this.setState({ open: false });
    }, 2000);
  };
  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };
  render() {
    return (
      <div className="col-12 m_addFriendWOptions">
        <MainDrawer open={this.state.addFriendWInputPage} onOpen={this.toggleAddFriendWInputPage} onClose={this.toggleAddFriendWInputPage}>
          <AddFriendWInput onClose={this.toggleAddFriendWInputPage} UserFinalData={this.props.UserFinalData} />
        </MainDrawer>
        <div className="row">
          <div className="col-12 py-3 m_addFriendWOptionsHeader">
            <div className="row">
              <div className="col-4" onClick={this.props.onClose}>
                <img src={Icons.PinkBack} alt="Back" height={20} width={25} />
              </div>
            </div>
          </div>
          <div className="m_addFriendScreen">
            <div className="col-12 m-label-common1">Add</div>
            <div className="col-12 label-common2">Friend</div>
            <div className="col-12 pt-4">
              <div className="row py-2" onClick={this.toggleAddFriendWInputPage}>
                <div className="col-10 addFriendOptions">Add Friend by {keys.AppName} ID</div>
                <div className="col-2">
                  <img src={Icons.RightArrow} alt="right-arrow" width={10} />
                </div>
              </div>
              <div className="row py-2" onClick={this.shareAppInfo}>
                <div className="col-10 addFriendOptions">Invite By SMS</div>
                <div className="col-2">
                  <img src={Icons.RightArrow} alt="right-arrow" width={10} />
                </div>
              </div>
              <div className="row py-2" onClick={this.shareAppInfo}>
                <div className="col-10 addFriendOptions">Invite By Apps</div>
                <div className="col-2">
                  <img src={Icons.RightArrow} alt="right-arrow" width={10} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          timeout={2500}
          onClose={this.handleClosesnakbar}
        />
      </div>
    );
  }
}

export default AddFriend;
