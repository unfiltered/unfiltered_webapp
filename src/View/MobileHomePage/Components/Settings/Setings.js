import React, { Component } from "react";
import "./Setings.scss";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { withStyles } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import Checked from "../../../../asset/svg/checked.svg";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import Preferences from "../Preferences/Preferences";
import Appsettings from "../AppSetting/Appsetting";
import CoinBalances from "../Coins/CoinBalance";
import MainUserProfile from "../UserProfile/UserProfile";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { UserProfileData, ReportWithReason } from "../../../../controller/auth/verification";
import Close from "../../../../asset/images/close.png";
import { getCookie } from "../../../../lib/session";
import PremiumUserContent from "../ModalContent/PremiumUser";
import Payment from "../Payment/Payment";
import Referral from "../Referral/Referral";
import { Icons } from "../../../../Components/Icons/Icons";
import MATTextField from "../../../../Components/Input/TextField";
import { keys } from "../../../../lib/keys";

const styles = (theme) => ({
  slider: {
    padding: "22px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  rootrtabs: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  colorSwitchBase: {
    color: purple[300],
    "&$colorChecked": {
      color: purple[500],
      "& + $colorBar": {
        backgroundColor: purple[500],
      },
    },
  },
  colorBar: {},
  colorChecked: {},
  iOSSwitchBase: {
    "&$iOSChecked": {
      color: theme.palette.common.white,
      "& + $iOSBar": {
        backgroundColor: "rgb(247, 65, 103) ",
      },
    },
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.sharp,
    }),
  },
  iOSChecked: {
    transform: "translateX(15px)",
    "& + $iOSBar": {
      opacity: 1,
      border: "none",
    },
  },
  iOSBar: {
    borderRadius: 13,
    width: 42,
    height: 26,
    marginTop: -13,
    marginLeft: -21,
    border: "solid 1px",
    borderColor: theme.palette.grey[400],
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  iOSIcon: {
    width: 24,
    height: 24,
  },
  iOSIconChecked: {
    boxShadow: theme.shadows[1],
  },
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  textWidth: {
    width: "100%",
  },
  close: {
    top: "13px",
    right: "2px",
    zIndex: 999,
  },
  closeButton: {
    position: "absolute",
    top: "12px",
    height: "25px",
    width: "25px",
  },
});

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      premiumUserModal: false,
      Preferences: false,
      Appsetting: false,
      modal: false,
      checkedA: true,
      checkedB: true,
      CoinBalances: false,
      FinaluserProfile: false,
      loader: true,
      UserFinalData: "",
      needHelp: false,
      reportModal: false,
      multiline: "",
      variant: "",
      usermessage: "",
      open: false,
      proPlanModal: false,
      selectedReason: null,
      paymentView: false,
      reasons: [
        { type: 1, reason: "Report an issue" },
        { type: 2, reason: "Make a suggestion" },
        { type: 3, reason: "Ask a question" },
      ],
    };
  }

  componentDidMount() {
    this.setState({ loader: true });
    UserProfileData().then((data) => {
      this.setState({
        UserFinalData: data.data.data,
        loader: false,
      });
    });
    // this.props.dispatch(USER_PROFILE_ACTION_FUN("UserProfilePic", this.state.UserFinalData.profilePic));
    // console.log("userdata", this.state.UserFinalData);
    // this.props.dispatch(USER_PROFILE_ACTION_FUN("UserProfileName", this.state.UserFinalData.firstName));
  }

  openProfileDrawer = () => {
    this.setState({ Preferences: true });
  };

  closeProfileDrawer = () => {
    this.setState({ Preferences: false });
  };

  toggleUpdateToPremiumUserModal = () => {
    this.setState({ premiumUserModal: !this.state.premiumUserModal });
  };

  openAppSettingsDrawer = () => {
    this.setState({ Appsetting: true });
  };

  closeAppSettingsDrawer = () => {
    this.setState({ Appsetting: false });
    console.log("asdadasd");
  };

  openCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: true });
  };

  closeCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: false });
  };

  openFinalProfileDrawer = () => {
    this.setState({ FinaluserProfile: true });
  };

  closeFinalProfileDrawer = () => {
    this.setState({ FinaluserProfile: false });
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  needHelpModal = () => {
    this.setState({ needHelp: !this.state.needHelp });
  };

  // opens after need help modal
  openSuggestionBox = (obj) => {
    this.setState({ needHelp: !this.state.needHelp, selectedReason: obj });
    setTimeout(() => {
      this.setState({ reportModal: !this.state.reportModal });
    }, 500);
  };

  toggleReportModal = () => {
    this.setState({ reportModal: !this.state.reportModal });
  };

  submitReportModal = () => {
    this.reportWithReasonFn(this.state.selectedReason.type, this.state.multiline, getCookie("token"));
    this.toggleReportModal();
  };

  // handle input changes dynamically
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  recommendFriend = () => {
    navigator.clipboard.writeText(`Invite your friends into our new dating app, https://app.unfiltered.love/`);
    this.setState({
      open: true,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends now",
    });
    setTimeout(() => {
      this.setState({ open: false });
    }, 1500);
  };

  reportWithReasonFn = (type, text, token) => {
    ReportWithReason(type, text, token)
      .then((res) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: `${this.state.selectedReason.reason} successfully submitted`,
        });
      })
      .catch((err) => {
        console.log("err fail", err);
      });
  };

  togglePaymentView = () => {
    this.setState({ paymentView: !this.state.paymentView });
  };

  // close the modal after selecting plan and move forward to payment
  proceedToPayment = () => {
    this.toggleUpdateToPremiumUserModal();
    setTimeout(() => {
      this.togglePaymentView();
    }, 500);
  };

  toggleReferralCodeDrawer = () => {
    this.setState({ referralCode: !this.state.referralCode });
  };

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  render() {
    const { classes } = this.props;

    const Preferencesdrawer = <Preferences onClose={this.closeProfileDrawer} UserFinalData={this.state.UserFinalData} />;
    const Appsettingsdrawer = <Appsettings onClose={this.closeAppSettingsDrawer} />;
    const CoinBalancesdrawer = <CoinBalances onClose={this.closeCoinBalancesDrawer} />;
    const FianluserProfile = <MainUserProfile onClose={this.closeFinalProfileDrawer} />;
    const PaymentView = <Payment onClose={this.togglePaymentView} />;
    const { durationInDays, rewindCount, superLikeCount } = this.props.checkIfUserIsProUserVariable.ProUserDetails;
    return (
      <div>
        <MainDrawer width={400} onClose={this.closeProfileDrawer} onOpen={this.openProfileDrawer} open={this.state.Preferences}>
          {Preferencesdrawer}
        </MainDrawer>

        <MainDrawer width={400} onClose={this.closeAppSettingsDrawer} onOpen={this.openAppSettingsDrawer} open={this.state.Appsetting}>
          {Appsettingsdrawer}
        </MainDrawer>

        <MainDrawer width={400} onClose={this.closeCoinBalancesDrawer} onOpen={this.openCoinBalancesDrawer} open={this.state.CoinBalances}>
          {CoinBalancesdrawer}
        </MainDrawer>

        <MainDrawer
          width={400}
          onClose={this.openFinalProfileDrawer}
          onOpen={this.closeFinalProfileDrawer}
          open={this.state.FinaluserProfile}
        >
          {FianluserProfile}
        </MainDrawer>

        <MainDrawer width={400} onClose={this.togglePaymentView} onOpen={this.togglePaymentView} open={this.state.paymentView}>
          {PaymentView}
        </MainDrawer>
        <MainDrawer
          width={400}
          onClose={this.toggleReferralCodeDrawer}
          onOpen={this.toggleReferralCodeDrawer}
          open={this.state.referralCode}
        >
          <Referral onClose={this.toggleReferralCodeDrawer} refCode={this.state.UserFinalData.referralCode} />
        </MainDrawer>
        {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div>
            <div className="col-12 MUsersettings py-3 m_settings_header" onClick={this.props.onClose}>
              <img src={Icons.PinkBack} alt="back" height={20} width={25} />
            </div>
            <div className="m_setting_view">
              <div className="col-12 MUsersettings">
                <div className="row align-items-center pb-3 border-bottom">
                  <div className="col-9 text-left Mainsettings">
                    <span>{this.state.UserFinalData.firstName},</span>
                    <span>{this.state.UserFinalData.age ? this.state.UserFinalData.age.value : ""}</span>
                    <div>
                      <span>ID</span>
                      <span>{this.state.UserFinalData.findMateId}</span>
                    </div>
                  </div>
                  <div className="col-3 text-right MUserProfile" onClick={this.openFinalProfileDrawer}>
                    <img
                      src={this.props && this.props.userProfilePicture}
                      alt={this.state.UserFinalData.firstName}
                      style={{ objectFit: "cover", height: "70px", width: "70px" }}
                    />
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="col-12 py-3 border-bottom" onClick={this.openCoinBalancesDrawer}>
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="My Credits" />
                    </div>
                    <div className="col-3 text-right MUserProfile px-0">
                      <span className="gold-ring">
                        <img src={Icons.BigHeart} alt="multiple-users" height={20} width={20} /> {this.renderCoins(this.props.CoinBalance)}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="col-12 py-3 border-bottom" onClick={this.openProfileDrawer}>
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="message.settigs2" />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.SettingMyPref} alt="multiple-users" height={20} width={20} />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12">
                <div className="col-12 py-3 border-bottom" onClick={this.toggleReferralCodeDrawer}>
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="Referral Code" />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.M_Updated_Referrals} alt="settings" size={20} height={20} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12">
                <div className="col-12 py-3 border-bottom" onClick={this.recommendFriend}>
                  <input type="text" hidden value="https://app.unfiltered.love " id="m_recommend" />
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="message.settigs1" />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.SettingInvite} alt="add-user" height={20} width={20} />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12">
                <div className="col-12 py-3 border-bottom" onClick={this.openAppSettingsDrawer}>
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="message.settigs3" />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.SettingSettings} alt="settings" size={20} height={20} />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12" onClick={this.needHelpModal}>
                <div className="col-12 py-3 border-bottom">
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id="Need Help" />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.SettingHelp} alt="refresh" size={20} height={20} />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="col-12"
                onClick={
                  this.props.checkIfUserIsProUserVariable.ProUserDetails.subscriptionId !== "Free Plan"
                    ? this.toggleModal
                    : this.toggleUpdateToPremiumUserModal
                }
              >
                <div className="col-12 py-3 border-bottom">
                  <div className="row">
                    <div className="col-9 text-left Msettings">
                      <FormattedMessage tagName="p" id={`${keys.AppName} Plus`} />
                    </div>
                    <div className="col-3 text-right MUserProfile">
                      <img src={Icons.SettingCMEPlus} alt="refresh" size={20} height={20} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        <MatUiModal width={"95%"} isOpen={this.state.modal} toggle={this.toggleModal}>
          <div className="coinPlanModal pb-4" style={{ background: "#f1f1f1" }}>
            <div className="closeCoinHistoryModal" onClick={this.toggleModal}>
              <img src={Icons.closeBtn} alt="close-btn" height={18} />
            </div>
            <div className="col-12">
              <div className="h4 text-center pt-3">Current Active Plan</div>
              <div className="text-center pb-3">{parseInt(durationInDays / 30)} Months</div>

              <div className="row" style={{ justifyContent: "space-evenly" }}>
                <section className="upgrade-cards">
                  <div>
                    <img src={Icons.unlimitedLikes} alt="unlimited-likes" />
                  </div>
                  <p>Unlimited Likes.</p>
                </section>
                <section className="upgrade-cards">
                  <div>
                    <img src={Icons.unlimitedSuperlikes} alt="unlimited-likes" />
                  </div>
                  <p>Unlimited Superlikes.</p>
                </section>
              </div>

              <div className="row pt-3" style={{ justifyContent: "space-evenly" }}>
                <section className="upgrade-cards">
                  <div>
                    <img src={Icons.unlimited_rewinds} alt="unlimited-likes" />
                  </div>
                  <p>Unlimited Rewinds.</p>
                </section>
                <section className="upgrade-cards pt-3">
                  <div>
                    <img src={Icons.swipeAround} alt="unlimited-likes" />
                  </div>
                  <p style={{ marginTop: "20px" }}>Swipe around the world.</p>
                </section>
              </div>
            </div>
          </div>
        </MatUiModal>
        <MatUiModal
          type="m_settings_report"
          customClass="m_settings_report"
          isOpen={this.state.needHelp}
          toggle={this.needHelpModal}
          width={"95%"}
        >
          <div>
            <div className="col-12 px-0 settings-options bg-white text-center">
              {this.state.reasons.map((k) => (
                <div key={k.type} onClick={() => this.openSuggestionBox(k)}>
                  {k.reason}
                </div>
              ))}
            </div>
            <div className="mt-3" style={{ opacity: 0 }}></div>
            <div className="col-12 text-center settings-options bg-white" onClick={this.needHelpModal}>
              <div>Cancel</div>
            </div>
          </div>
        </MatUiModal>
        <MatUiModal type="m_settings_input_text" isOpen={this.state.reportModal} toggle={this.toggleReportModal} width={"95%"}>
          <div className="col-12">
            <div className={`col-12 px-0 text-right ` + classes.close} onClick={this.toggleReportModal}>
              <img src={Close} height={18} width={18} alt="close" />
            </div>
            <MATTextField
              label="Name"
              disabled={true}
              className={"m-report"}
              value={this.state.UserFinalData.firstName}
              onChange={this.handleChange("name")}
              margin="normal"
            />

            <MATTextField
              label="Email"
              disabled={true}
              className={"m-report"}
              value={this.state.UserFinalData.emailId}
              onChange={this.handleChange("email")}
              margin="normal"
            />

            <MATTextField
              label="Your Issue"
              multiline
              rowsMax="2"
              value={this.state.multiline}
              onChange={this.handleChange("multiline")}
              className={"m-report-textBox mt-2 mb-5"}
            />
            <div className="closeButton" onClick={this.submitReportModal}>
              <img src={Checked} alt="checked" width={28} className={classes.closeButton} />
            </div>
          </div>
        </MatUiModal>
        <MatUiModal
          onClose={this.toggleUpdateToPremiumUserModal}
          toggle={this.toggleUpdateToPremiumUserModal}
          isOpen={this.state.premiumUserModal}
          width={"95%"}
        >
          <PremiumUserContent
            toggleUpdateToPremiumUserModal={this.toggleUpdateToPremiumUserModal}
            proceedToPayment={this.proceedToPayment}
          />
        </MatUiModal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserData: state.Main.UserData,
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    checkIfUserIsProUserVariable: state.ProUser,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(Settings));
