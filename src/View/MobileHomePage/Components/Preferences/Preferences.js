import React, { Component } from "react";
import "./Preferences.scss";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { discoverPeople } from "../../../../actions/DiscoverPeople";
import Select from "react-select";
import Location from "../Location/Location";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { FormattedMessage } from "react-intl";
import { searchprefenceuser, Searchpostprefenceuser, setLocation } from "../../../../controller/auth/verification";
import { getCookie, setCookie } from "../../../../lib/session";
import { connect } from "react-redux";
import * as filterByValues from "../../../../actions/UserProfile";
import { Icons } from "../../../../Components/Icons/Icons";
import * as PostActions from "../../../../actions/Posts";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import PremiumUserContent from "../ModalContent/PremiumUser";
import MobilePayPalPayment from "../Coins/WalletScreens/MobilePayPalPayment";

const styles = {
  root: {
    width: 300,
  },
  slider: {
    padding: "5px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "10vh",
    textAlign: "center",
  },
  radioControl: {
    display: "flex",
  },
  ageNumbers: {
    fontSize: "13px",
    display: "flex",
    fontFamily: "Circular Air Book",
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
  },
};

let DeafultPayload = [];

const customStyles = {
  option: (provided, state) => ({
    color: "#d1d1d1",
    background: "#161616",
    padding: "5px",
  }),
};

class Prefernces extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectGender: "Male",
      loader: true,
      valueage: 20,
      valuecity: 80,
      condition: false,
      value: { min: null, max: null },
      valueHeight: { min: null, max: null },
      valueDistance: { min: null, max: null },
      valueGender: { Male: null, Female: null },
      Genderid: "",
      Heightid: "",
      openMap: false,
      Ageid: "",
      HeightRangeid: "",
      DistanceRangeid: "",
      searchPreferencesuser: "",
      tabselectedvalue: 1,
      distancetabselectedvalue: 0,
      SelectedOptions: "Male",
      SelectedHeightOptions: "",
      CurrentDeafultValueid: "",
      isHeightInFtOrCm: "",
      isDistanceInKmOrMi: "",
      locationToShow: "",
      paymentPage: false,
      plan: {},
      purchaseSubscriptionPlan: true,
      premiumUserModal: false,
      selectGenderOnMount: "", // m / f / both to searh users
      linksDistance: [
        {
          id: 0,
          name: "Mi",
        },
      ],
      activeLinkdistance: 1,
      linksHeight: [
        {
          id: 0,
          name: "Ft",
        },
        {
          id: 1,
          name: "Cm",
        },
      ],
      activeLinkHeight: 1,
      variant: "error",
      usermessage: "",
      _snackbar: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFilterUser = this.handleFilterUser.bind(this);
  }

  toggleUpdateToPremiumUserModal = () => {
    this.setState({ premiumUserModal: !this.state.premiumUserModal });
  };

  togglePaymentPage = () => {
    this.setState({ paymentPage: !this.state.paymentPage });
  };

  storePlanDetails = (plan) => {
    this.setState({ plan: plan });
  };

  setLocationOfCookie = (obj) => {
    console.log("%c setLocationOfCookie", "background:#e31b1b; color:#ffffff; font-size: 18px");
    console.log(obj);
    // this.setNewPassportLocation(obj.lat, obj.lng);
    this.setState({ locationToShow: obj }, () => {
      setCookie("selectedLocation", JSON.stringify(obj));
    });
  };

  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ _snackbar: false });
  };

  componentDidMount() {
    this.setState({ loader: true });
    let dummyLoc = [];

    if (getCookie("selectedLocation") != null) {
      console.log("A");
      console.log("dummyLoc", getCookie("selectedLocation"));
      dummyLoc = JSON.parse(getCookie("selectedLocation"));
      this.setState({
        locationToShow: JSON.parse(getCookie("selectedLocation")),
      });
    } else if (getCookie("locations") != null) {
      console.log("B");
      let _loc = JSON.parse(getCookie("locations"));
      this.setState({ locationToShow: _loc[0] });
    } else if (getCookie("location") != null && dummyLoc.length === 1) {
      console.log("C");
      let obj = [
        {
          address: getCookie("location"),
          city: getCookie("citylocation"),
          lat: getCookie("lan"),
          lng: getCookie("lng"),
        },
      ];
      setCookie("locations", JSON.stringify(obj));
      this.setState({
        locationToShow: {
          address: getCookie("location"),
          city: getCookie("citylocation"),
          lat: getCookie("lan"),
          lng: getCookie("lng"),
        },
      });
    }

    try {
      let baseAddr = JSON.parse(getCookie("baseAddress"));
      let selectedL0cation = "";
      try {
        if (getCookie("selectedLocation") != null) {
          selectedL0cation = JSON.parse(getCookie("selectedLocation"));
        }
      } catch (e) {
        console.log("selected location fail");
      }
      console.log("length", Object.keys(baseAddr).length, Object.keys(selectedL0cation).length);
      if (Object.keys(baseAddr).length > 0 || Object.keys(selectedL0cation).length > 0) {
        searchprefenceuser().then((data) => {
          // console.log("searchprefenceuser", data.data.data[0]);
          // For the Gender Module
          let GenderDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Gender");
          let genderDataId = data.data.data[0].searchPreferences[GenderDataIndex]["_id"];
          let Selectedvaluegender = data.data ? data.data.data[0].searchPreferences[0].selectedValue : "";

          // For the Height Module
          let HeightDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Height");
          let heightDataId = data.data.data[0].searchPreferences[HeightDataIndex]["_id"];
          let minHeightValue = data.data.data[0].searchPreferences[HeightDataIndex].selectedValue[0];
          let maxHeightValue = data.data.data[0].searchPreferences[HeightDataIndex].selectedValue[1];
          this.props.heightFilterByValues(minHeightValue, maxHeightValue);
          this.setState({ isHeightInFtOrCm: data.data.data[0].searchPreferences[HeightDataIndex].selectedUnit });

          let apiObjectForHeightRange =
            HeightDataIndex > -1
              ? {
                  min: minHeightValue,
                  max: maxHeightValue,
                }
              : {
                  min: null,
                  max: null,
                };

          // For the Age Module
          let AgeDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Age");
          let ageDataId = data.data.data[0].searchPreferences[AgeDataIndex]["_id"];
          let minValue = data.data.data[0].searchPreferences[AgeDataIndex].selectedValue[0];
          let maxValue = data.data.data[0].searchPreferences[AgeDataIndex].selectedValue[1];
          this.props.ageFilterByValues(minValue, maxValue);

          let apiObjectForAgeRange =
            AgeDataIndex > -1
              ? {
                  min: minValue,
                  max: maxValue,
                }
              : {
                  min: null,
                  max: null,
                };

          // For the Distance Module
          let DistanceDataIndex = data.data.data[0].searchPreferences.findIndex((item) => item.PreferenceTitle === "Distance");
          let distanceDataId = data.data.data[0].searchPreferences[DistanceDataIndex]["_id"];
          let minDistanceValue = data.data.data[0].searchPreferences[DistanceDataIndex].selectedValue[0];
          let maxDistanceValue = data.data.data[0].searchPreferences[DistanceDataIndex].selectedValue[1];
          this.setState({ isDistanceInKmOrMi: data.data.data[0].searchPreferences[DistanceDataIndex].selectedUnit });
          this.props.distanceFilterByValues(minDistanceValue, maxDistanceValue);

          let apiObjectForDistanceRange =
            AgeDataIndex > -1
              ? {
                  min: minDistanceValue,
                  max: maxDistanceValue,
                }
              : {
                  min: null,
                  max: null,
                };

          this.setState({
            searchPreferencesuser: data.data.data,
            valueDistance: apiObjectForDistanceRange,
            valueHeight: apiObjectForHeightRange,
            value: apiObjectForAgeRange,
            Genderid: genderDataId,
            Ageid: ageDataId,
            HeightRangeid: heightDataId,
            DistanceRangeid: distanceDataId,
            selectGender: Selectedvaluegender,
            loader: false,
          });
        });
      } else {
        this.setState({ _snackbar: true, usermessage: "GeoLocation key has exhausted, contact your admin." });
      }
    } catch (e) {
      console.log("error", e);
      this.setState({
        _snackbar: true,
        usermessage: "Could not fetch location for searching nearby users, Please clear cookies and login again.",
      });
    }
  }

  handleChange = (event, tabselectedvalue) => {
    this.setState({ tabselectedvalue });
  };

  // Used to get switch tabs in Distance Module
  handleChangeDistance = (event, distancetabselectedvalue) => {
    this.setState({ distancetabselectedvalue });
  };

  // Converting Cemi to Feet
  convertkmtome = (cemi, isFeet) => {
    return isFeet ? parseFloat(cemi / 1.6) : cemi;
  };

  getHeightMinValue = (min) => {
    return this.state.activeLinkHeight === 0 ? this.convertCemitoFit(min, this.state.activeLinkHeight === 0) : min;
  };

  getHeightMaxValue = (max) => {
    return this.state.activeLinkHeight === 0 ? this.convertCemitoFit(max, this.state.activeLinkHeight === 0) : max;
  };

  // Converting Cemi to Feet
  convertCemitoFit = (cemi, isFeet) => {
    return isFeet ? parseFloat(cemi / 30.48).toFixed(1) : cemi;
  };

  // Function for the Select Option Hobbies
  handleUserHeight = (data, event) => {
    this.setState({
      SelectedHeightOptions: event.value,
      Heightid: data._id,
    });
  };

  // Converting Cemi to Feet
  convertCemitoFit = (cemi, isFeet) => {
    return isFeet ? parseFloat(cemi / 30.48).toFixed(1) : cemi;
  };

  // Converting Cemi to Feet
  convertkmtome = (cemi, isFeet) => {
    return isFeet ? parseFloat(cemi * 1000) : cemi;
  };

  // Function for the Select Option Gender
  handleUserGender = (data) => {
    this.setState({
      selectGender: data.value,
    });
  };

  // Function for the Age Range Select
  handleAgeRange = (value, data) => {
    setCookie("AgeRange", value);
    this.setState({
      value,
      Ageid: data._id,
    });
    this.props.ageFilterByValues(value.min, value.max);
  };

  // Function for the Height Range Select
  handleDistanceRange = (valueDistance, data) => {
    this.setState({
      valueDistance: valueDistance,
      DistanceRangeid: data._id,
    });
    this.props.distanceFilterByValues(valueDistance.min, valueDistance.max);
  };

  // Function for the Height Range Select
  handleHeightRange = (valueHeight, data) => {
    this.setState({
      valueHeight,
      HeightRangeid: data._id,
    });
    this.props.heightFilterByValues(valueHeight.min, valueHeight.max);
  };

  // To Switch Tabs Height
  handleClickHeight = (id, name) => {
    this.setState({ activeLinkHeight: id, isHeightInFtOrCm: name });
  };

  // To Switch Tabs Distance
  handleClickDistance = (id, name) => {
    this.setState({ activeLinkdistance: id, isDistanceInKmOrMi: name });
  };

  getDistanceMinValue = (min) => {
    return this.state.activeLinkdistance === 0 ? this.convertkmtome(min, this.state.activeLinkdistance === 0) : min;
  };

  convertkmtome = (cemi, isFeet) => {
    return isFeet ? parseInt(cemi / 1.6) : cemi;
  };

  getDistanceMaxValue = (max) => {
    return this.state.activeLinkdistance === 0 ? this.convertkmtome(max, this.state.activeLinkdistance === 0) : max;
  };

  // Accroding the Map it render the Screen Dta
  getInput = (data) => {
    const { classes } = this.props;
    switch (data.Priority) {
      // For the Gender
      case 4:
        // For the Gender
        let genderOptions = [];
        data.OptionsValue.map((data) =>
          genderOptions.push({
            label: data,
            value: data,
          })
        );

        let SelectedUserAgeValue = {
          label: data.selectedValue,
          value: data.selectedValue,
        };
        let SelectUserRange1 = (
          // For the Gender
          <div className="py-3 col-12">
            <div className="row">
              <div className="col-6 d-flex align-items-center">
                <h5 className="m_filterParameters">{data.PreferenceTitle}</h5>
              </div>
              <div className="col-6 m_searchPref_genderOptions">
                <Select
                  options={genderOptions}
                  defaultValue={[SelectedUserAgeValue]}
                  onChange={this.handleUserGender}
                  styles={customStyles}
                  // onChange={(e) => this.setState({ selectGender: e.value })}
                />
              </div>
            </div>
          </div>
        );
        return SelectUserRange1;

      // For the Age
      case 2:
        let SelectUserRange = (
          <div className="py-2 p-0 col-12 Msliderbg">
            <div className="row mx-0 pb-3">
              <div className="col-5 text-left text-muted">
                <h5 className="m_filterParameters">{data.PreferenceTitle} :</h5>
              </div>
              <div className="col-7 text-right text-muted">
                <div className="pr-0 col-12 Mheight_tabs">
                  <div className="d-inline-flex row">
                    <div className={`col-6`}></div>
                    <div className={`col-6 ${classes.ageNumbers}`}>
                      {this.props.filterByAgeValue.min} - {this.props.filterByAgeValue.max}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="py-2 col-10 MFilter_user">
              {/* <span className="lowRange_height">{this.getHeightMinValue(this.props.filterByHeightValue.min)}</span> */}
              <InputRange
                maxValue={data.OptionsValue[1]}
                minValue={data.OptionsValue[0]}
                value={this.props.filterByAgeValue}
                onChange={(value) => this.handleAgeRange(value, data)}
              />
              {/* <span className="highRange_height">{this.getHeightMaxValue(this.props.filterByHeightValue.max)}</span> */}
            </div>
          </div>
        );
        return SelectUserRange;

      // For the Height
      case 17:
        let SelectUserHeightRange = (
          <div className="py-2 p-0 col-12 Msliderbg">
            <div className="row mx-0 pb-3">
              <div className="col-5 text-left text-muted">
                <h5 className="m_filterParameters">{data.PreferenceTitle} :</h5>
              </div>
              <div className="col-7 text-right text-muted">
                <div className="pr-0 col-12 Mheight_tabs">
                  <div className="d-inline-flex row">
                    <div className={`col-6 ${classes.ageNumbers}`}>
                      {this.state.isHeightInFtOrCm === "Cm"
                        ? this.getHeightMinValue(this.props.filterByHeightValue.min)
                        : this.convertCemitoFit(this.props.filterByHeightValue.min, true)}
                      -{" "}
                      {this.state.isHeightInFtOrCm === "Cm"
                        ? this.getHeightMinValue(this.props.filterByHeightValue.max)
                        : this.convertCemitoFit(this.props.filterByHeightValue.max, true)}
                    </div>
                    {this.state.linksHeight.map((link) => {
                      return (
                        <div key={link.id} className="p-0 col-3 MSwitch_heightcontainer">
                          <ul>
                            <li
                              onClick={() => this.handleClickHeight(link.id, link.name)}
                              className={link.name.toLowerCase() === this.state.isHeightInFtOrCm.toLowerCase() ? " activeitem_height" : ""}
                            >
                              {link.name}
                            </li>
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className="py-2 col-10 MFilter_user">
              {/* <span className="lowRange_height">{this.getHeightMinValue(this.props.filterByHeightValue.min)}</span> */}
              <InputRange
                maxValue={data.OptionsValue[1]}
                minValue={data.OptionsValue[0]}
                value={this.props.filterByHeightValue}
                onChange={(valueHeight) => this.handleHeightRange(valueHeight, data)}
              />
              {/* <span className="highRange_height">{this.getHeightMaxValue(this.props.filterByHeightValue.max)}</span> */}
            </div>
          </div>
        );
        return SelectUserHeightRange;

      // For the Distance
      case 3:
        let SelectUserDistanceRange = (
          <div className="py-2 p-0 col-12 Msliderbg">
            <div className="row mx-0 pb-3">
              <div className="col-5 text-left text-muted">
                <h5 className="m_filterParameters">{data.PreferenceTitle} :</h5>
              </div>
              <div className="col-7 text-right text-muted">
                <div className="pr-0 col-12 Mdistance_tabs">
                  <div className="d-inline-flex row">
                    <div className={`col-6 ${classes.ageNumbers}`}>
                      {this.state.isDistanceInKmOrMi === "Km"
                        ? this.getDistanceMinValue(parseInt(this.props.filterByDistanceValue.min))
                        : this.convertkmtome(this.props.filterByDistanceValue.min, true)}{" "}
                      -{" "}
                      {this.state.isDistanceInKmOrMi === "Mi"
                        ? parseInt(this.props.filterByDistanceValue.max / 1.6)
                        : parseInt(this.props.filterByDistanceValue.max)}
                    </div>
                    {this.state.linksDistance.map((link) => {
                      return (
                        <div key={link.id} className="p-0 col-3 MSwitch_distancecontainer">
                          <ul>
                            <li
                              onClick={() => this.handleClickDistance(link.id, link.name)}
                              className={
                                link.name.toLowerCase() === this.state.isDistanceInKmOrMi.toLowerCase() ? " activeitem_distance" : ""
                              }
                            >
                              {link.name}
                            </li>
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
            <div className="py-2 col-10 MFilter_user">
              {/* <span className="lowRange_distance">{this.getDistanceMinValue(this.props.filterByDistanceValue.min)}</span> */}
              <InputRange
                maxValue={data.OptionsValue[1]}
                minValue={data.OptionsValue[0]}
                value={this.props.filterByDistanceValue}
                onChange={(valueDistance) => this.handleDistanceRange(valueDistance, data)}
              />
              {/* <span className="highRange_distance">{this.getDistanceMaxValue(this.props.filterByDistanceValue.max)}</span> */}
            </div>
          </div>
        );
        return SelectUserDistanceRange;
      default: {
      }
    }
  };

  setNewPassportLocation = (lat, lng, addr) => {
    setLocation(
      {
        latitude: lat,
        ip: getCookie("ipAddress"),
        timeZone: getCookie("timezone"),
        isPassportLocation: true,
        longitude: lng,
        address: addr,
      },
      getCookie("token")
    )
      .then((res) => {
        console.log("setLocation success", res);
      })
      .catch((err) => console.log("setLocation err", err));
  };

  // API Call to Filter Search User
  handleFilterUser = () => {
    this.props.m_updatePostStatus("Finding new profiles based on updated preferences...", true);
    // console.log("preferences handleFilterUser #1");
    let FilterPayload = "";

    let Filteruserpayload = {
      preferences: [
        {
          pref_id: this.state.Genderid,
          selectedValue: [this.state.selectGender],
        },
        {
          pref_id: this.state.Ageid ? this.state.Ageid : this.state.CurrentDeafultValueid,
          selectedValue: [this.state.value.min, this.state.value.max],
          selectedUnit: "year",
        },
        {
          pref_id: this.state.DistanceRangeid,
          // selectedValue: [
          //   this.convertkmtome(this.state.valueDistance.min, this.state.activeLinkdistance === 0),
          //   this.convertkmtome(this.state.valueDistance.max, this.state.activeLinkdistance === 0),
          // ],
          selectedValue:
            this.state.isDistanceInKmOrMi === "Km"
              ? [this.state.valueDistance.min, this.state.valueDistance.max]
              : [parseInt(this.convertkmtome(this.state.valueDistance.min)), parseInt(this.convertkmtome(this.state.valueDistance.max))],
          selectedUnit: this.state.isDistanceInKmOrMi,
        },
        {
          pref_id: this.state.HeightRangeid,
          selectedValue:
            this.state.isHeightInFtOrCm === "Cm"
              ? [this.props.filterByHeightArr[0], this.props.filterByHeightArr[1]]
              : [
                  parseInt(this.convertCemitoFit(this.props.filterByHeightArr[0])),
                  parseInt(this.convertCemitoFit(this.props.filterByHeightArr[1])),
                ],
          selectedUnit: this.state.isHeightInFtOrCm,
        },
      ],
      latitude: this.state.locationToShow.lat ? this.state.locationToShow.lat : getCookie("lat"),
      longitude: this.state.locationToShow.lng ? this.state.locationToShow.lng : getCookie("lng"),
    };

    if (this.state.Genderid || this.state.Ageid || this.state.DistanceRangeid || this.state.HeightRangeid) {
      FilterPayload = Filteruserpayload;
      this.setNewPassportLocation(this.state.locationToShow.lat, this.state.locationToShow.lng, this.state.locationToShow.address);
    } else {
      FilterPayload = {
        latitude: this.state.locationToShow.lat ? this.state.locationToShow.lat : getCookie("lat"),
        longitude: this.state.locationToShow.lng ? this.state.locationToShow.lng : getCookie("lng"),
        preferences: DeafultPayload,
      };
      this.setNewPassportLocation(this.state.locationToShow.lat, this.state.locationToShow.lng, this.state.locationToShow.address);
    }

    // call nearby people API, to reload new data when you go back after selecting new location and go back.
    Searchpostprefenceuser(FilterPayload).then((data) => {
      // console.log("posting pref", FilterPayload);
      this.props.handlenearbypeople();
      this.props.onClose();
      this.props.m_updatePostStatus("", false);
    });
  };

  // To Open a Location Drawer
  openlocationdrawer = () => {
    this.setState({ location: true });
  };

  // To Close a Location Drawer
  closelocationdrawer = () => {
    this.setState({ location: false });
  };

  openDrawer = () => {
    if (this.props.ProUser && this.props.ProUser.subscriptionId === "Free Plan") {
      this.toggleUpdateToPremiumUserModal();
    } else {
      this.openlocationdrawer();
    }
  };

  render() {
    return (
      <div>
        <MainDrawer width={400} onClose={this.closelocationdrawer} onOpen={this.openlocationdrawer} open={this.state.location}>
          <Location setLocationOfCookie={this.setLocationOfCookie} onClose={this.closelocationdrawer} />
        </MainDrawer>

        <div className="pt-3 px-3 MCommonwidth m_pref_stickyHeader">
          <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.props.onClose} />
          <div className="py-3 MCommonheader">
            <FormattedMessage tagName="p" id="message.preferncestitle" />
            <FormattedMessage tagName="p" id="message.preferncessubtext" />
          </div>
        </div>

        <div className="py-2 col-12 m_pref_options">
          <div className="row">
            <div className="col-4 text-left">
              <h6 className="m-0 m_searchPref_location_header">Location</h6>
            </div>
            <div className="col-8 text-right m_searchPref_location">
              <FormattedMessage id="message.preferncescurrentlocation" /> <i class="chevright fas fa-chevron-right"></i>
              <p onClick={() => this.openDrawer()} className="m_pref_location">
                {getCookie("selectedLocation") != null
                  ? JSON.parse(getCookie("selectedLocation")).address
                  : getCookie("location") != null
                  ? getCookie("location")
                  : JSON.parse(getCookie("baseAddress")).address}
              </p>
            </div>
          </div>
        </div>

        {/* mapping loop on the api data for rendering ui screens */}
        {this.state.searchPreferencesuser && this.state.searchPreferencesuser[0] && this.state.searchPreferencesuser[0].searchPreferences
          ? this.state.searchPreferencesuser[0].searchPreferences.map((data, index) => <div key={index}>{this.getInput(data)}</div>)
          : ""}

        <Snackbar
          anchorOrigin={{ vertical: "botton", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state._snackbar}
          timeout={6000}
          onClose={this.handleClosesnakbar}
        />

        <div className="Mprefer">
          <i onClick={this.handleFilterUser} className="fas fa-check" />
        </div>
        <MatUiModal width={"90%"} toggle={this.toggleUpdateToPremiumUserModal} isOpen={this.state.premiumUserModal}>
          <PremiumUserContent
            storePlanDetails={this.storePlanDetails}
            togglePaymentPage={this.togglePaymentPage}
            toggle={this.toggleUpdateToPremiumUserModal}
          />
        </MatUiModal>
        <MainDrawer width={400} onClose={this.togglePaymentPage} onOpen={this.togglePaymentPage} open={this.state.paymentPage}>
          <MobilePayPalPayment
            purchaseSubscriptionPlan={this.state.purchaseSubscriptionPlan}
            isSubscription={true}
            plan={this.state.plan}
            onClose={this.togglePaymentPage}
          />
        </MainDrawer>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ProUser: state.ProUser.ProUserDetails,
    // age
    filterByAgeValue: state.UserProfile.filterByAgeValue,
    filterByAgeArr: state.UserProfile.filterByAgeArr,
    // height
    filterByHeightValue: state.UserProfile.filterByHeightValue,
    filterByHeightArr: state.UserProfile.filterByHeightArr,
    // distance
    filterByDistanceValue: state.UserProfile.filterByDistanceValue,
    filterByDistanceArr: state.UserProfile.filterByDistanceArr,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAllDiscoverPeople: (data) => dispatch(discoverPeople(data)),
    ageFilterByValues: (min, max) => dispatch(filterByValues.filterByAgeObjectVal(min, max)),
    heightFilterByValues: (min, max) => dispatch(filterByValues.filterByHeightObjectVal(min, max)),
    distanceFilterByValues: (min, max) => dispatch(filterByValues.filterByDistanceObjectVal(min, max)),
    m_updatePostStatus: (text, bool) => dispatch(PostActions.uploadingPostStatus(text, bool)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Prefernces));
