// // Main React Components
// import React, { Component } from "react";

// // Scss
// import "./Preferences.scss";

// // Reusable Components
// import MainDrawer from "../../../../Components/Drawer/Drawer";

// // Material-Ui Components
// import { withStyles } from "@material-ui/core/styles";
// import Tabs from "@material-ui/core/Tabs";
// import Tab from "@material-ui/core/Tab";

// // MultiSelect Dropdown Components
// import Select from "react-select";

// // Range-Slider Components
// import InputRange from "react-input-range";
// import "react-input-range/lib/css/index.css";

// // imported Components
// import Location from "../Location/Location";

// // Languges Components
// import { FormattedMessage } from "react-intl";

// // Redux Components
// import { getCookie } from "../../../../lib/session";
// import {
//   searchprefenceuser,
//   Searchpostprefenceuser
// } from "../../../../controller/auth/verification";

// const styles = theme => ({
//   slider: {
//     padding: "22px 0px"
//   },
//   thumbIcon: {
//     borderRadius: "50%"
//   },
//   thumbIconWrapper: {
//     backgroundColor: "#fff"
//   },
//   formControl: {
//     margin: theme.spacing.unit,
//     minWidth: 120
//   },
//   selectEmpty: {
//     marginTop: theme.spacing.unit * 2
//   }
// });

// let demo = "";

// class Preferences extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       loader: true,
//       valueage: 20,
//       valuecity: 80,
//       condition: false,
//       value: { min: null, max: null },
//       valuedistance: { min: null, max: null },
//       Genderid: "",
//       Heightid: "",
//       Ageid: "",
//       HeightRangeid: "",
//       searchPreferencesuser: "",
//       tabselectedvalue: 1,
//       SelectedOptions: "",
//       CurrentSelectedValue: "",
//       SelectedHeightOptions: ""
//     };
//   }

//   componentDidMount() {
//     this.setState({ loader: true });

//     searchprefenceuser().then(data => {
//       let HeightDataIndex = data.data.data[0].searchPreferences.findIndex(
//         item => item.PreferenceTitle === "Height"
//       );

//       let apiObjectForHeightRange =
//         HeightDataIndex > -1
//           ? {
//               min:
//                 data.data.data[0].searchPreferences[HeightDataIndex]
//                   .OptionsValue[0],
//               max:
//                 data.data.data[0].searchPreferences[HeightDataIndex]
//                   .OptionsValue[1]
//             }
//           : { min: null, max: null };

//       let AgeDataIndex = data.data.data[0].searchPreferences.findIndex(
//         item => item.PreferenceTitle === "Age"
//       );

//       let apiObjectForAgeRange =
//         AgeDataIndex > -1
//           ? {
//               min:
//                 data.data.data[0].searchPreferences[AgeDataIndex]
//                   .OptionsValue[0],
//               max:
//                 data.data.data[0].searchPreferences[AgeDataIndex]
//                   .OptionsValue[1]
//             }
//           : { min: null, max: null };

//       let FilterUser = data.data.data[0].searchPreferences
//         ? data.data.data[0].searchPreferences.map(data => {
//             this.setState({
//               CurrentSelectedValue: data.selectedValue
//             });
//           })
//         : "";

//       this.setState({
//         searchPreferencesuser: data.data.data,
//         valuedistance: apiObjectForHeightRange,
//         value: apiObjectForAgeRange
//       });
//     });
//   }

//   // Used to get switch tabs in homepage
//   handleChange = (event, tabselectedvalue) => {
//     this.setState({ tabselectedvalue });
//   };

//   // convering Cm to Fit
//   convertSemitoFit = cemis => {
//     {
//       const xyz =
//         this.state.valuedistance.max && this.state.valuedistance.min
//           ? (demo = cemis.selectedValue.map(data => {
//               return data / 30.48;
//             }))
//           : "";
//     }
//   };

//   // Function for the Select Option Gender
//   handleUserGender = (data, event) => {
//     this.setState({
//       SelectedOptions: event.value,
//       Genderid: data._id
//     });
//   };

//   // Function for the Select Option Hobbies
//   handleUserHeight = (data, event) => {
//     this.setState({
//       SelectedHeightOptions: event.value,
//       Heightid: data._id
//     });
//   };

//   // Converting Cemi to Feet
//   convertCemitoFit = (cemi, isFeet) => {
//     {
//       return isFeet ? parseFloat(cemi / 30.48).toFixed(2) : cemi;
//     }
//   };

//   // Assgning data to the Height Slider
//   getHeightValues = (min, max) => {
//     return this.state.tabselectedvalue == 0 ? (
//       <span>
//         {this.convertCemitoFit(min, this.state.tabselectedvalue === 0)} -{" "}
//         {this.convertCemitoFit(max, this.state.tabselectedvalue === 0)}
//       </span>
//     ) : (
//       <span>
//         {min} - {max}
//       </span>
//     );
//   };

//   // Function for the Age Range Select
//   handleAgeRange = (value, data) => {
//     this.setState({
//       value,
//       Ageid: data._id
//     });
//   };

//   // Function for the Age Range Select
//   handleHeightRange = (valuedistance, data) => {
//     this.setState({
//       valuedistance,
//       HeightRangeid: data._id
//     });
//   };

//   // Accroding the Map it render the Screen Dta
//   getInput = data => {
//     switch (data.Priority) {
//       case 1:
//         // For the Gender
//         let Ageoptions = [];
//         data.OptionsValue.map(data =>
//           Ageoptions.push({
//             label: data,
//             value: data
//           })
//         );

//         let SelectedUserAgeValue = {
//           label: data.selectedValue,
//           value: data.selectedValue
//         };

//         // For the Hobbies
//         let Heightoptions = [];
//         data.OptionsValue.map(data =>
//           Heightoptions.push({
//             label: data,
//             value: data
//           })
//         );

//         let SelectedUserHeightValue = {
//           label: data.selectedValue,
//           value: data.selectedValue
//         };

//         var Multiselectpriority =
//           // For the Gender
//           data.PreferenceTitle === "Gender" ? (
//             <div className="py-2 col-12">
//               <div className="py-2 col-12">
//                 <div className="row">
//                   <div className="p-0 col-8 text-left">
//                     <h6 className="m-0">{data.PreferenceTitle}</h6>
//                   </div>
//                   <div className="col-4 text-left Mgenderselect">
//                     <Select
//                       options={Ageoptions}
//                       defaultValue={[SelectedUserAgeValue]}
//                       onChange={this.handleUserGender.bind(this, data)}
//                     />
//                   </div>
//                 </div>
//               </div>
//             </div>
//           ) : (
//             // For the Gender
//             <div className="py-2 col-12">
//               <div className="py-2 col-12">
//                 <div className="row">
//                   <div className="p-0 col-8 text-left">
//                     <h6 className="m-0">{data.PreferenceTitle}</h6>
//                   </div>
//                   <div className="col-4 text-left Mgenderselect">
//                     <Select
//                       options={Heightoptions}
//                       defaultValue={[SelectedUserHeightValue]}
//                       onChange={this.handleUserHeight.bind(this, data)}
//                     />
//                   </div>
//                 </div>
//               </div>
//             </div>
//           );
//         return Multiselectpriority;

//       case 2:
//         console.log("ageid", data);
//         let SelectUserRange = (
//           <div className="pb-2 col-12 Msliderbg">
//             <div className="row">
//               <div className="col-6 text-left">
//                 <h6 className="m-0">{data.PreferenceTitle}</h6>
//               </div>
//               <div className="col-6 text-right text-muted">
//                 {this.state.value.max && this.state.value.min ? (
//                   <span className="m-0 mx-2">
//                     {this.state.value.min} - {this.state.value.max}
//                   </span>
//                 ) : data.OptionsValue ? (
//                   data.OptionsValue.map((data, index) => (
//                     <span key={index} className="m-0 mx-2">
//                       {data}
//                     </span>
//                   ))
//                 ) : (
//                   ""
//                 )}
//               </div>
//             </div>
//             <div className="py-2 col-12 MFilter_user">
//               <InputRange
//                 maxValue={data.OptionsValue[1]}
//                 minValue={data.OptionsValue[0]}
//                 value={this.state.value}
//                 onChange={value => this.handleAgeRange(value, data)}
//               />
//             </div>
//           </div>
//         );
//         return SelectUserRange;

//       case 3:
//         let SelectUserdistanceRange = (
//           <div className="py-2 col-12 Msliderbg">
//             <div className="row">
//               <div className="col-6 text-left">
//                 <h6 className="m-0">{data.PreferenceTitle}</h6>
//               </div>
//               <div className="col-6 text-right text-muted">
//                 <div className="row">
//                   <div className="p-0 col-7">
//                     {/* convering User Height from Cm to Feet */}
//                     <div className="WUser_height">
//                       <Tabs
//                         value={this.state.tabselectedvalue}
//                         onChange={this.handleChange}
//                         variant="fullWidth"
//                         indicatorColor="primary"
//                         textColor="primary"
//                       >
//                         <Tab
//                           onClick={this.convertSemitoFit.bind(this, data)}
//                           icon={<p>{data.optionsUnits[0]}</p>}
//                         />
//                         <Tab icon={<p>{data.optionsUnits[1]}</p>} />
//                       </Tabs>
//                     </div>
//                   </div>
//                   <div className="pl-0 col-5">
//                     {this.state.valuedistance.max &&
//                     this.state.valuedistance.min ? (
//                       <span className="m-0 mx-2">
//                         {this.getHeightValues(
//                           this.state.valuedistance.min,
//                           this.state.valuedistance.max
//                         )}
//                       </span>
//                     ) : data.OptionsValue ? (
//                       data.OptionsValue.map((data, index) => (
//                         <span key={index} className="m-0 mx-2">
//                           {data}
//                         </span>
//                       ))
//                     ) : (
//                       ""
//                     )}
//                   </div>
//                 </div>
//               </div>
//             </div>
//             <div className="py-2 col-12 MFilter_user">
//               <InputRange
//                 maxValue={data.OptionsValue[1]}
//                 minValue={data.OptionsValue[0]}
//                 value={this.state.valuedistance}
//                 onChange={valuedistance =>
//                   this.handleHeightRange(valuedistance, data)
//                 }
//               />
//             </div>
//           </div>
//         );
//         return SelectUserdistanceRange;
//     }
//   };

//   // API Call to Filter Search User
//   handleFilterUser = () => {
//     let Filteruserpayload = {
//       preferences: [
//         {
//           pref_id: this.state.Genderid,
//           selectedValue: [
//             this.state.SelectedOptions
//               ? this.state.SelectedOptions
//               : this.state.CurrentSelectedValue
//           ]
//         },
//         {
//           pref_id: this.state.Heightid,
//           selectedValue: [
//             this.state.SelectedHeightOptions
//               ? this.state.SelectedHeightOptions
//               : this.state.CurrentSelectedValue
//           ]
//         },
//         {
//           pref_id: this.state.Ageid,
//           selectedValue: [this.state.value.min, this.state.value.max],
//           selectedUnit: "year"
//         },
//         {
//           pref_id: this.state.HeightRangeid,
//           selectedValue: [
//             this.convertCemitoFit(
//               this.state.valuedistance.min,
//               this.state.tabselectedvalue === 0
//             ),
//             this.convertCemitoFit(
//               this.state.valuedistance.max,
//               this.state.tabselectedvalue === 0
//             )
//           ],
//           selectedUnit: this.state.tabselectedvalue === 0 ? "Ft" : "Cm"
//         }
//       ],
//       longitude: getCookie("long"),
//       latitude: getCookie("lat")
//     };

//     console.log("90900", Filteruserpayload);
//     this.props.onClose();

//     Searchpostprefenceuser(Filteruserpayload).then(data => {
//       console.log("finaldaaaaaaaaaaaaaaaaaaaaaaata", data);
//       // this.props.handleNearPeople();
//     });
//   };

//   render() {
//     const locationdrawer = <Location onClose={this.closelocationdrawer} />;

//     return (
//       <div style={{ height: "100vh" }}>
//         <MainDrawer
//           width={400}
//           onClose={this.closelocationdrawer}
//           onOpen={this.openlocationdrawer}
//           open={this.state.location}
//         >
//           {locationdrawer}
//         </MainDrawer>

//         <div className="pt-3 px-3 MCommonwidth">
//           <i
//             className="fa fa-arrow-left"
//             onClick={this.props.onClose}
//             style={{
//               fontSize: "24px",
//               color: "#F74167"
//             }}
//           />
//           <div className="py-3 MCommonheader">
//             <FormattedMessage tagName="p" id="message.preferncestitle" />
//             <FormattedMessage tagName="p" id="message.preferncessubtext" />
//           </div>
//         </div>

//         <div className="py-2 col-12">
//           <div className="row">
//             <div className="col-6 text-left">
//               <FormattedMessage tagName="h6" id="message.prefernceslocation" />
//             </div>
//             <div className="col-6 text-right">
//               {/* <FormattedMessage id="message.preferncescurrentlocation" /> */}
//               <p className="Mrightarrow" onClick={this.openlocationdrawer}>
//                 {getCookie("citylocation")}
//               </p>
//             </div>
//           </div>
//         </div>

//         {/* mapping loop on the api data for rendering ui screens */}
//         {this.state.searchPreferencesuser &&
//         this.state.searchPreferencesuser[0] &&
//         this.state.searchPreferencesuser[0].searchPreferences
//           ? this.state.searchPreferencesuser[0].searchPreferences.map(
//               (data, index) => <div key={index}>{this.getInput(data)}</div>
//             )
//           : ""}

//         <div className="Mprefer">
//           <i onClick={this.handleFilterUser} className="fas fa-check" />
//         </div>
//       </div>
//     );
//   }
// }

// export default withStyles(styles)(Preferences);

// Assgning data to the Height Slider
getDistanceValues = (min, max) => {
  return this.state.activeLinkdistance == 0 ? (
    <span>
      {this.convertKmtometer(min, this.state.activeLinkdistance === 0)} -{" "}
      {this.convertKmtometer(max, this.state.activeLinkdistance === 0)}
    </span>
  ) : (
    <span>
      {min} - {max}
    </span>
  );
};

// Converting Cemi to Feet
convertKmtometer = (cemi, isFeet) => {
  {
    return isFeet ? parseFloat(cemi * 1000) : cemi;
  }
};

// Function for the Height Range Select
handleDistanceRange = (valueDistance, data) => {
  this.setState({
    valueDistance,
    DistanceRangeid: data._id
  });
};

// To Switch Tabs Distance
handleClicDistance = id => {
  this.setState({ activeLinkdistance: id });
};

let SelectUserDistanceRange = (
  <div className="py-2 col-12 Msliderbg">
    <div className="row">
      <div className="col-6 text-left text-muted">
        <h6>{data.PreferenceTitle} :</h6>
      </div>
      <div className="col-6 text-right text-muted">
        <div className="row">
          <div className="p-0 col-5">
            <div className="WUser_height">
              {this.state.valueDistance.max && this.state.valueDistance.min ? (
                <span className="m-0 mx-2" style={{ fontSize: "12px" }}>
                  {this.getDistanceValues(
                    this.state.valueDistance.min,
                    this.state.valueDistance.max
                  )}
                </span>
              ) : data.OptionsValue ? (
                data.OptionsValue.map((data, index) => (
                  <span key={index} className="m-0 mx-2">
                    {data}
                  </span>
                ))
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="pr-0 col-6 Mdistance_tabs">
            <div class="d-inline-flex row">
              {this.state.linksDistance.map(link => {
                return (
                  <div
                    key={link.id}
                    className="p-0 col-6 MSwitch_distancecontainer"
                  >
                    <ul>
                      <li
                        onClick={() => this.handleClicDistance(link.id)}
                        className={
                          link.id === this.state.activeLinkdistance
                            ? " activeitem_distance"
                            : ""
                        }
                      >
                        {link.name}
                      </li>
                    </ul>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="py-2 col-12 MFilter_user">
      <InputRange
        maxValue={data.OptionsValue[1]}
        minValue={data.OptionsValue[0]}
        value={this.state.valueDistance}
        onChange={valueDistance =>
          this.handleDistanceRange(valueDistance, data)
        }
      />
    </div>
  </div>
);


return SelectUserDistanceRange;














    // For the Distance
      case 4:
        let SelectUserDistanceRange = (
          <div className="py-2 col-12 Msliderbg">
            <div className="row">
              <div className="col-6 text-left text-muted">
                <h6>{data.PreferenceTitle} :</h6>
              </div>
              <div className="col-6 text-right text-muted">
                <div className="row">
                  <div className="p-0 col-5">
                    <div className="WUser_height">
                      {this.state.valueDistance.max && this.state.valueDistance.min ? (
              <span className="m-0 mx-2" style={{ fontSize: "12px" }}>
                {this.getDistanceValues(
                  this.state.valueDistance.min,
                  this.state.valueDistance.max
                )}
              </span>
            ) : data.OptionsValue ? (
              data.OptionsValue.map((data, index) => (
                <span key={index} className="m-0 mx-2">
                  {data}
                </span>
              ))
            ) : (
              ""
            )}
                    </div>
                  </div>
                <div className="pr-0 col-6 Mdistance_tabs">
              <div class="d-inline-flex row">
                {this.state.linksDistance.map(link => {
                  return (
                    <div
                      key={link.id}
                      className="p-0 col-6 MSwitch_distancecontainer"
                    >
                      <ul>
                        <li
                          onClick={() => this.handleClickDistance(link.id)}
                          className={
                            link.id === this.state.activeLinkdistance
                              ? " activeitem_distance"
                              : ""
                          }
                        >
                          {link.name}
                        </li>
                      </ul>
                    </div>
                  );
                })}
              </div>
            </div>
                </div>
              </div>
            </div>
            <div className="py-2 col-12 MFilter_user">
               <InputRange
                maxValue={data.OptionsValue[1]}
                minValue={data.OptionsValue[0]}
                value={this.state.valueDistance}
                onChange={valueDistance =>
                  this.handleDistanceRange(valueDistance, data)
                }
              />
            </div>
          </div>
        );
        return SelectUserDistanceRange;
