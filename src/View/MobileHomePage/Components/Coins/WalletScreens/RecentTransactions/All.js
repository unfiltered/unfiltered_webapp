import React from "react";
import up_arrow from "../../../../../../asset/images/up-arrow.svg";
import down from "../../../../../../asset/images/down.svg";
import no_transaction from "../../../../../../asset/images/default_icon.png";
import "./MobileWallet.scss";

function Mobile_All_RecentTransactions({ m_withdraw_wallet_all_data }) {
  return m_withdraw_wallet_all_data && m_withdraw_wallet_all_data.length > 0 ? (
    m_withdraw_wallet_all_data.map((k, i) => (
      <div className="col-12 py-2 m_walletSpentCard" key={i}>
        <div className="row">
          <div className="col-1 d-flex align-items-center">
            {k.txntype === 1 ? (
              <img src={down} alt="up-arrow" height={18} width={18} />
            ) : (
              <img src={up_arrow} alt="down" height={18} width={18} />
            )}
          </div>
          <div className="col-8 m_wallet_trigger">{k.trigger}</div>
          <div className="col-2 px-0 d-flex align-items-center m_wallet_coins">{k.amount} Credits</div>
        </div>
      </div>
    ))
  ) : (
    <div className="col-12" style={{ height: "100vh", width: "100vw" }}>
      <div className="text-center">
        <img src={no_transaction} height={240} width={180} alt="no-transaction" />
      </div>
      <div className="text-center" style={{ fontFamily: "Circular Air Book", color: "#d1d1d1" }}>
        There is no withdraw happen yet.
      </div>
    </div>
  );
}

export default Mobile_All_RecentTransactions;
