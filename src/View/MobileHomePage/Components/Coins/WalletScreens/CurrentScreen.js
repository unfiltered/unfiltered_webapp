import React, { useState } from "react";
import { Media } from "reactstrap";
import MainDrawer from "../../../../../Components/Drawer/Drawer";
import MobileRecentTransactions from "./RecentTransactions";
import { Icons } from "../../../../../Components/Icons/Icons";
import MaterialModal from "../../../../../Components/Model/MAT_UI_Modal";
import Button from "../../../../../Components/Button/Button";
import MobilePayPalPayment from "./MobilePayPalPayment";

const Mobile_Wallet_CurrentScreen = ({ data, openRecentTransactionsScreen, CoinBalance }) => {
  const [openDrawer, setToggleDrawer] = useState(false);
  const [modal, setModal] = useState(false);
  const [plan, selectedPlan] = useState({});
  const [paymentDrawer, setPaymentDrawer] = useState(false);
  const [colors] = useState(["#19ACFF", "#E33022", "#9462FD"]);
  const [discount] = useState(["60%", "42%", "22%"]);
  const togglePaymentDrawer = () => setPaymentDrawer(!paymentDrawer);
  const toggleModal = (data) => {
    if (data) {
      selectedPlan(data);
      setModal(!modal);
    } else {
      setModal(!modal);
    }
  };
  const toggleDrawer = () => setToggleDrawer(!openDrawer);
  return (
    <div>
      <div className="col-12 py-2">
        <div className="row">
          <div className="col-9 px-0 d-flex align-items-center">
            <div>
              <img src={Icons.BigHeart} alt={"heart-icon"} height={30} width={30} />
            </div>
            <div className="coin_balance">{CoinBalance} Credits</div>
          </div>
          <div className="col-3 d-flex justify-content-end" onClick={toggleDrawer}>
            <span onClick={openRecentTransactionsScreen}>
              <img src={Icons.WalletTransactionInfo} alt="info" height={15} width={15} />
            </span>
          </div>
        </div>
      </div>
      <div className="col-12 py-4">
        <div className="row">
          <div className="col-2 px-0">
            <img src={Icons.BigHeart} alt={"heart-icon"} height={40} width={40} />
          </div>
          <div className="col-10 px-0">
            <div className="buy_coins">Buy Credits</div>
            <div className="buy_coins_helper_text">
              Credits unlock features like Superlike, 1. credit per Super Like and boost profile(profile to added to top), 2. Credits every
              30 minutes.
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 border-top">
        <div className="row pt-4" style={{ justifyContent: "space-evenly" }}>
          {data && data.length > 0 ? (
            data.map((d, planIndex) => (
              <div
                onClick={() => toggleModal(d)}
                key={planIndex}
                className={"mobileCoinCard px-0"}
                style={{ border: `2px solid ${colors[planIndex]}` }}
              >
                <div style={{ background: colors[planIndex] }}>Value</div>
                <div style={{ border: `2px solid ${colors[planIndex]}` }}>
                  <div>{d.noOfCoinUnlock.Coin}</div>
                  <div>Credits</div>
                </div>
                <div>Save {discount[planIndex]}</div>
                <div style={{ borderTop: `2px solid ${colors[planIndex]}` }}>
                  {d.baseCurrencySymbol} {d.baseCost}
                </div>
              </div>
            ))
          ) : (
            <div>No Current Plans Available</div>
          )}
        </div>
      </div>

      <MaterialModal width={"80%"} isOpen={modal} toggle={toggleModal}>
        <div className="col-12 p-3">
          <div className="row m_confirmCoinPurchaseModal">
            <div className="col-12">Coin Purchase</div>
            <div className="col-12 border-top">
              Are you sure you want to purchase {plan && plan.noOfCoinUnlock && plan.noOfCoinUnlock.Coin} Coin Plan?
            </div>
            <div className="col-12 border-top">
              <div className="row">
                <Button text="Cancel" handler={toggleModal} />
                <Button
                  text="Ok"
                  handler={() => {
                    toggleModal();
                    setTimeout(() => {
                      togglePaymentDrawer();
                    }, 200);
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </MaterialModal>
      <MainDrawer width={400} onClose={togglePaymentDrawer} onOpen={togglePaymentDrawer} open={paymentDrawer}>
        <MobilePayPalPayment onClose={togglePaymentDrawer} isCoinPlan={true} selectedCoinPlan={plan} />
      </MainDrawer>
      <MainDrawer width={400} onClose={toggleDrawer} onOpen={toggleDrawer} open={openDrawer}>
        <MobileRecentTransactions onClose={toggleDrawer} />
      </MainDrawer>
    </div>
  );
};

export default Mobile_Wallet_CurrentScreen;
