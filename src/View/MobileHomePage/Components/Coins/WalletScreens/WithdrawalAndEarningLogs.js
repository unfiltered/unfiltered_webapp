import React from "react";
import "../CoinBalance.scss";
import SimpleTabs from "../../../../../Components/ScrollbleTabs/Scrollble_tabs";
import MearningLogs from "./WithdrawalAndEarningLogs/EarningLogs";
import MwithdrawalLogs from "./WithdrawalAndEarningLogs/WithdrawalLogs";
import { GetEarningWalletTransactionWithTxn, CurrentCoinBalance, walletEarning } from "../../../../../controller/auth/verification";
import { getCookie } from "../../../../../lib/session";
import { Icons } from "../../../../../Components/Icons/Icons";

class Mobile_Withdraw_Earning_Logs extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      coinsEarned: "",
      loader: false,
      earningWalletCreditData: [],
      earningWalletWithdrawData: [],
      balance: "",
      earningwalletid: "",
      value: 0,
      allCoinsWithoutPageState: false,
    };
  }

  /** on scroll, API called (pagination) on the selected tab and data is concatinated */
  setViewMore = (boolean, coinType, cbFucntion, value) => {
    console.log("coinType", coinType);
    if (boolean && coinType.data.length < coinType.totalCount) {
      GetEarningWalletTransactionWithTxn(this.state.earningwalletid, getCookie("token"), value, coinType.pageState, this.state.allCoinsWithoutPageState)
        .then((res) => {
          let oldAllData = { ...coinType };
          let oldAllDataArr = [];
          oldAllDataArr.push(...oldAllData.data, ...res.data.data);
          oldAllData["data"] = oldAllDataArr;
          oldAllData["pageState"] = res.data.pageState;
          cbFucntion(oldAllData);
          this.setState({ viewMore: false });
        })
        .catch((err) => this.setState({ viewMore: false }));
    } else {
      this.setState({ viewMore: false });
    }
  };

  /** function for scroll pagination */
  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        if (this.state.value === 0) {
          this.setViewMore(true, this.state.earningWalletCreditData, this.updateEarningWalletCreditData, 1);
        } else if (this.state.value === 1) {
          this.setViewMore(true, this.state.earningWalletWithdrawData, this.updateEarningWalletWithdrawData, 2);
        }
      }
    }
  };

  componentWillUnmount() {
    this.paneDidUnmount(this.myRef.current);
  }

  /** ref is removed on leaving page */
  paneDidUnmount(node) {
    if (node) {
      node.childNodes[0].children[1].removeEventListener("scroll", this.handleScroll, true);
    }
  }

  /** ref is passed to handle scroll mounted on <div> */
  paneDidMount = (node) => {
    if (node) {
      node.childNodes[0].children[1].addEventListener("scroll", this.handleScroll, true);
    }
  };

  handleChangeIndex = (index) => {
    this.setState({ value: index });
  };

  /** updates the wallet data (earning data) */
  updateEarningWalletCreditData = (data) => {
    this.setState({ earningWalletCreditData: data });
  };

  /** updates the wallet data (withdrawal data) */
  updateEarningWalletWithdrawData = (data) => {
    this.setState({ earningWalletWithdrawData: data });
  };

  componentDidMount() {
    this.paneDidMount(this.myRef.current);
    this.callEarningWalletAPI();
  }

  callEarningWalletAPI = () => {
    this.setState({ loader: true });
    CurrentCoinBalance(getCookie("uid"), getCookie("token")).then((res) => {
      if (res.data.walletEarningData.length > 0) {
        this.setState({ coinsEarned: res.data.walletEarningData[0].balance, earningwalletid: res.data.walletEarningData[0].walletearningid });
        /** This API gets Earning wallet info, not array of object data */
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 1, false).then((res) => {
          this.setState({ earningWalletCreditData: res.data, allCoinsWithoutPageState: true, loader: false });
        });
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 2, false).then((res) => {
          this.setState({ earningWalletWithdrawData: res.data, loader: false });
        });
        // walletEarning(getCookie("token"), getCookie("uid"), "user", "INR")
        //   .then((res) => {
        //     this.setState({ balance: res.data.userPreferedCurrency.withdrawAmount });
        //   })
        //   .catch((err) => console.log("[walletEarning] err", err));
      }
    });
  };
  render() {
    return (
      <div style={{ width: "100vw" }}>
        <div className="col-12 py-3 mobile_recentTransactionsScreen">
          <img src={Icons.PinkBack} alt="left-arrow" height={20} width={25} onClick={this.props.onClose} />
          <div className="text-center">Withdrawal And Earning Logs</div>
        </div>
        <div ref={this.myRef} className="m_withdrawWallet_info">
          <SimpleTabs
            tabs={[{ label: "Withdrawal Logs" }, { label: "Earning Logs" }]}
            isCentered={true}
            tabContent={[
              { content: <MwithdrawalLogs m_earning_wallet_all_data={this.state.earningWalletWithdrawData.data} /> },
              {
                content: <MearningLogs m_earning_wallet_all_data={this.state.earningWalletCreditData.data} />,
              },
            ]}
          />
        </div>
      </div>
    );
  }
}

export default Mobile_Withdraw_Earning_Logs;
