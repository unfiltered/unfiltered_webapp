import React from "react";
import "../CoinBalance.scss";
import { withRouter } from "react-router-dom";
import { Icons } from "../../../../../Components/Icons/Icons";
// import PaypalButton from "../../../../WebHomePage/Components/PayPal/PayPalButton";
import { getCookie } from "../../../../../lib/session";
import { pruchasePlanFromAPI, buyCoinPlan, purchaseSubscription } from "../../../../../controller/auth/verification";
import { addCoinsToExistingCoins } from "../../../../../actions/UserProfile";
import { connect } from "react-redux";
// import MatUiModal from "../../../../../Components/Model/MAT_UI_Modal";
// import Button from "../../../../../Components/Button/Button";
import { checkIfProUser } from "../../../../../actions/ProUser";
import { indirectRouting } from "../../../../../actions/coinConfig";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";

class MobilePayPalPayment extends React.Component {
  state = {
    modal: false,
    isProcessing: "",
    processing: false,
    cvc: "",
    expiry: "",
    name: "",
    number: "",
    focus: "",
    isPaymentDone: "", // string
    selectSubscriptionBasedPlans: {},
    colors: ["#19ACFF", "#f80402", "#E33022", "#9462FD"],
    planFeatures: [
      {
        plan: "Elite",
        array: [
          { line: "All “Premium” Features + Boost Profile Feature" },
          { line: "Additional Filters – Ethnicity, Deal Breakers, Unlimited Matches" },
        ],
      },
      {
        plan: "Elite First Year",
        array: [
          { line: "All “Premium” Features + Boost Profile Feature" },
          { line: "Additional Filters – Ethnicity, Deal Breakers, Unlimited Matches" },
        ],
      },
      {
        plan: "Premium",
        array: [
          { line: "All “Plus” Features" },
          { line: "Rewind Feature" },
          { line: "Setup Date Feature" },
          { line: "Control Your Profile – Control what people see about you" },
          { line: "Enable Video Chat Feature" },
        ],
      },
      {
        plan: "Plus",
        array: [
          { line: "Unlimited Likes" },
          { line: "See Who Likes you before you Swipe" },
          { line: "Global Passport – Match with anyone in the US" },
          { line: "5 Super Likes Per Day" },
          { line: "5 Additional Matches" },
        ],
      },
    ],
  };

  selectSubscriptionBasedPlansFunc = (obj) => this.setState({ selectSubscriptionBasedPlans: obj });

  handleInputFocus = (e) => {
    this.setState({ ...this.state, focus: e.target.name });
  };

  handleInputChange = (e) => {
    const { name, value } = e.target;
    this.setState({ ...this.state, [name]: value });
  };

  backButtonHandler = () => {
    let { setIndirectRouting } = this.props;
    if (this.props && this.props.purchaseSubscriptionPlan) {
      if (this.state.isProcessing === "") {
        this.toggleModal(); // close modal
      }
    } else if (this.state.isProcessing === "Payment completed successfully. Please close the popup and go back") {
      setIndirectRouting(true);
      this.props.history.push("/app/walletLogs");
    }
  };

  headerBackButtonHandler = () => {
    if (this.props && this.props.purchaseSubscriptionPlan) {
      this.props.onClose();
    } else {
      this.props.onClose();
    }
  };

  checkForValidation = () => {
    if (this.state.cvc.length >= 3 && this.state.expiry.length >= 3 && this.state.name.length > 2 && this.state.number.length >= 16) {
      return true;
    }
    return false;
  };

  purchasePlan = async (plan) => {
    if (this.props.isCoinPlan) {
      this.setState({ processing: true });
      let data = await pruchasePlanFromAPI(getCookie("token"), {
        cardNumber: this.state.number,
        expireDate: this.state.expiry,
        cardCode: this.state.cvc,
        planId: plan.planId,
        type: "0",
      });
      if (data.status === 200) {
        let obj = {
          paymentGatewayTxnId: data.data.message,
          paymentGateway: "paypal",
          currencyCode: "USD",
          amount: plan.baseCost,
          coinValue: plan.noOfCoinUnlock.Coin,
        };
        let repsonseForCoinPlans = await buyCoinPlan(getCookie("token"), obj);
        this.setState({ isPaymentDone: `Payment is successfull. ${plan.noOfCoinUnlock.Coin} has been credited.` });
        this.props.addCoins(parseInt(plan.noOfCoinUnlock.Coin));
        setTimeout(() => {
          this.setState({ processing: false });
        }, 1000);
      } else {
        this.setState({ isPaymentDone: data.data.message });
        setTimeout(() => {
          this.setState({ processing: false });
        }, 1000);
        setTimeout(() => {
          this.setState({ isPaymentDone: "" });
        }, 5000);
      }
    } else {
      this.setState({ processing: true });
      let data = await pruchasePlanFromAPI(getCookie("token"), {
        cardNumber: this.state.number,
        expireDate: this.state.expiry,
        cardCode: this.state.cvc,
        planId: this.props.isSubscription ? this.props.UnFilteredPlus._id : this.state.selectSubscriptionBasedPlans._id,
        type: "1",
      });

      if (data.status === 200) {
        let obj = {
          planId: this.props.isSubscription ? this.props.UnFilteredPlus._id : this.props.directPaymentDetails_id,
          paymentGatewayTxnId: data.data.message,
          paymentGateway: "paypal",
          userPurchaseTime: new Date().getTime.toString(),
          type: "1",
          currencyCode: "USD",
        };
        let responseForSubscription = await purchaseSubscription(getCookie("token"), obj);
        console.log("API called", responseForSubscription);
        this.props.checkIfUserIsProUser(responseForSubscription.data.data);
        this.setState({ isPaymentDone: "Payment is successfull" });

        setTimeout(() => {
          this.setState({ processing: false });
        }, 2000);
        if (this.props.isSubscription) {
          this.props._storeElitePlanFunc(null);
          this.props.onClose();
        }
      } else {
        this.setState({ isPaymentDone: data.data.message, processing: false });
        setTimeout(() => {
          this.setState({ isPaymentDone: "" });
        }, 5000);
      }
    }
  };

  render() {
    let { allPlans, planName, index, isCoinPlan, selectedCoinPlan, isSubscription, UnFilteredPlus } = this.props;
    return isSubscription ? (
      <section className="col-12">
        <div className="row">
          <div className="col-12 py-3" onClick={() => this.headerBackButtonHandler()}>
            <img src={Icons.PinkBack} alt="back" height={20} width={25} />
          </div>
          <div className="col-12">
            <div className="m-label-common1 text-center">
              <span style={{ color: "f80402" }}>Selected Plan {this.props.UnFilteredPlus && this.props.UnFilteredPlus.tag}</span>
            </div>
          </div>
          <div className="col-12 py-3">
            <div id="PaymentForm" className="pt-4">
              <Cards
                cvc={this.state.cvc}
                expiry={this.state.expiry}
                focused={this.state.focus}
                name={this.state.name}
                number={this.state.number}
              />
              <form className="col-12 pt-3" onSubmit={(e) => e.preventDefault()}>
                <div className="row mx-auto card-payment-section">
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text">Name:</div>
                    <input
                      type="text"
                      name="name"
                      placeholder="John Doe"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text">Card No:</div>
                    <input
                      type="tel"
                      name="number"
                      maxLength={17}
                      placeholder="4242424242424242"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text"> CVV:</div>
                    <input
                      type="tel"
                      name="cvc"
                      maxLength={4}
                      placeholder="000"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                  </div>
                  <div className="card-fill-cell">
                    <div className="credit-card-fill-text"> Expiry:</div>
                    <input
                      type="tel"
                      name="expiry"
                      maxLength={4}
                      placeholder="0324"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                  </div>
                  <button
                    className={this.checkForValidation() ? "d_makePayment mt-3" : "d_makePayment_disabled mt-3"}
                    disabled={!this.checkForValidation()}
                    onClick={() => this.purchasePlan(selectedCoinPlan, 2)}
                  >
                    {this.state.processing ? "Please wait, while payment is processing" : "Make Payment"}
                  </button>
                  {this.state.isPaymentDone.length > 0 ? <div className="text-center w-100">{this.state.isPaymentDone}</div> : ""}
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    ) : (
      <div className="col-12">
        <div className="row">
          <div className="col-12 py-3" onClick={() => this.headerBackButtonHandler()}>
            <img src={Icons.PinkBack} alt="back" height={20} width={25} />
          </div>
          {isCoinPlan ? (
            <>
              <div className="col-12">
                <div className="m-label-common1 text-center">
                  Selected Plan <span style={{ color: "f80402" }}>{selectedCoinPlan.planName}</span>
                </div>
                <div id="PaymentForm" className="pt-4">
                  <Cards
                    cvc={this.state.cvc}
                    expiry={this.state.expiry}
                    focused={this.state.focus}
                    name={this.state.name}
                    number={this.state.number}
                  />
                  <form className="col-12 pt-3" onSubmit={(e) => e.preventDefault()}>
                    <div className="row mx-auto card-payment-section">
                      <div className="card-fill-cell">
                        <div className="credit-card-fill-text">Name:</div>
                        <input
                          type="text"
                          name="name"
                          placeholder="John Doe"
                          onChange={this.handleInputChange}
                          onFocus={this.handleInputFocus}
                        />
                      </div>
                      <div className="card-fill-cell">
                        <div className="credit-card-fill-text">Card No:</div>
                        <input
                          type="tel"
                          name="number"
                          maxLength={17}
                          placeholder="4242424242424242"
                          onChange={this.handleInputChange}
                          onFocus={this.handleInputFocus}
                        />
                      </div>
                      <div className="card-fill-cell">
                        <div className="credit-card-fill-text"> CVV:</div>
                        <input
                          type="tel"
                          name="cvc"
                          maxLength={4}
                          placeholder="000"
                          onChange={this.handleInputChange}
                          onFocus={this.handleInputFocus}
                        />
                      </div>
                      <div className="card-fill-cell">
                        <div className="credit-card-fill-text"> Expiry:</div>
                        <input
                          type="tel"
                          name="expiry"
                          maxLength={4}
                          placeholder="0324"
                          onChange={this.handleInputChange}
                          onFocus={this.handleInputFocus}
                        />
                      </div>
                      <button
                        className={this.checkForValidation() ? "d_makePayment mt-3" : "d_makePayment_disabled mt-3"}
                        disabled={!this.checkForValidation()}
                        onClick={() => this.purchasePlan(selectedCoinPlan)}
                      >
                        {this.state.processing ? "Please wait, while payment is processing" : "Make Payment"}
                      </button>
                      {this.state.isPaymentDone.length > 0 ? <div className="text-center w-100">{this.state.isPaymentDone}</div> : ""}
                    </div>
                  </form>
                </div>
              </div>
            </>
          ) : (
            <>
              <div className="col-12">
                <div className="m-label-common1 text-center">
                  <span style={{ color: "f80402" }}>Selected Plan {planName}</span>
                </div>
              </div>
              {Object.keys(this.state.selectSubscriptionBasedPlans).length > 0 ? (
                <div className="col-12 py-3">
                  <div id="PaymentForm" className="pt-4">
                    <Cards
                      cvc={this.state.cvc}
                      expiry={this.state.expiry}
                      focused={this.state.focus}
                      name={this.state.name}
                      number={this.state.number}
                    />
                    <form className="col-12 pt-3" onSubmit={(e) => e.preventDefault()}>
                      <div className="row mx-auto card-payment-section">
                        <div className="card-fill-cell">
                          <div className="credit-card-fill-text">Name:</div>
                          <input
                            type="text"
                            name="name"
                            placeholder="John Doe"
                            onChange={this.handleInputChange}
                            onFocus={this.handleInputFocus}
                          />
                        </div>
                        <div className="card-fill-cell">
                          <div className="credit-card-fill-text">Card No:</div>
                          <input
                            type="tel"
                            name="number"
                            maxLength={17}
                            placeholder="4242424242424242"
                            onChange={this.handleInputChange}
                            onFocus={this.handleInputFocus}
                          />
                        </div>
                        <div className="card-fill-cell">
                          <div className="credit-card-fill-text"> CVV:</div>
                          <input
                            type="tel"
                            name="cvc"
                            maxLength={4}
                            placeholder="000"
                            onChange={this.handleInputChange}
                            onFocus={this.handleInputFocus}
                          />
                        </div>
                        <div className="card-fill-cell">
                          <div className="credit-card-fill-text"> Expiry:</div>
                          <input
                            type="tel"
                            name="expiry"
                            maxLength={4}
                            placeholder="0324"
                            onChange={this.handleInputChange}
                            onFocus={this.handleInputFocus}
                          />
                        </div>
                        <button
                          className={this.checkForValidation() ? "d_makePayment mt-3" : "d_makePayment_disabled mt-3"}
                          disabled={!this.checkForValidation()}
                          onClick={() => this.purchasePlan(selectedCoinPlan, 2)}
                        >
                          {this.state.processing ? "Please wait, while payment is processing" : "Make Payment"}
                        </button>
                        {this.state.isPaymentDone.length > 0 ? <div className="text-center w-100">{this.state.isPaymentDone}</div> : ""}
                      </div>
                    </form>
                  </div>
                </div>
              ) : (
                <div className="col-12 py-3" onClick={this.toggleModal}>
                  {allPlans &&
                    allPlans[planName].map((k, i) => {
                      return (
                        <div className="coinCard m_planCard" key={i}>
                          <div className="pt-2 month_duration planInfo_desc">
                            <b>Plan Duration: {k.planName}</b>
                          </div>
                          <hr />
                          {this.state.planFeatures[index].array.map((v, w) => (
                            <div key={w} className="planInfo_desc py-2 border-bottom px-2">
                              {v.line}
                            </div>
                          ))}
                          <div className="actualPrice_premium mb-3">
                            Price: {k.currencySymbol} {k.cost}
                          </div>
                          <button onClick={() => this.selectSubscriptionBasedPlansFunc(k)} className="d_makePayment mt-2">
                            Select Plan
                          </button>
                        </div>
                      );
                    })}
                </div>
              )}
            </>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    UnFilteredPlus: state.Main.UserData.UnFilteredPlus,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCoins: (coins) => dispatch(addCoinsToExistingCoins(coins)),
    checkIfUserIsProUser: (data) => dispatch(checkIfProUser(data)),
    setIndirectRouting: (bool) => dispatch(indirectRouting(bool)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MobilePayPalPayment));
