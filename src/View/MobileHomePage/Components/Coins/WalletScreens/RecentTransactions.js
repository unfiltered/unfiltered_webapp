import React from "react";
import { GetWalletData, CurrentCoinBalance } from "../../../../../controller/auth/verification";
import ScrollableTabs from "../../../../../Components/ScrollbleTabs/Scrollble_tabs";
import MobileAllRecentTransactions from "./RecentTransactions/All";
import MobileCoinIn from "./RecentTransactions/CoinIn";
import MobileCoinOut from "./RecentTransactions/CoinOut";
import { getCookie } from "../../../../../lib/session";
import { Icons } from "../../../../../Components/Icons/Icons";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { indirectRouting } from "../../../../../actions/coinConfig";

class Mobile_RecentTransactions extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      coinData: [],
      allCoinsData: [],
      inData: [],
      outData: [],
      pageState: "",
      viewMore: false,
      value: 0,
      allCoinsWithoutPageState: false,
      walletId: "",
    };
  }

  setAllCoinsData = (data) => {
    this.setState({ allCoinsData: data });
  };
  setInData = (data) => {
    this.setState({ inData: data });
  };
  setOutData = (data) => {
    this.setState({ outData: data });
  };

  setValue = (input) => {
    this.setState({ value: input });
  };

  componentDidMount() {
    let token = getCookie("token");
    CurrentCoinBalance(getCookie("uid"), token).then((data) => {
      console.log("data.data.walletData[0]", data.data.walletData[0]);
      GetWalletData(data.data.walletData[0].walletid, token, 0, false).then((res) => {
        this.setAllCoinsData(res.data);
        this.setState({ allCoinsWithoutPageState: true, walletid: data.data.walletData[0].walletid });
      });
      // credit
      GetWalletData(data.data.walletData[0].walletid, token, 1).then((res) => {
        this.setInData(res.data);
      });
      // debit
      GetWalletData(data.data.walletData[0].walletid, token, 2).then((res) => {
        this.setOutData(res.data);
      });
    });
    this.paneDidMount(this.myRef.current); // <div>
  }

  paneDidMount = (node) => {
    if (node) {
      node.childNodes[0].children[1].addEventListener("scroll", this.handleScroll, true);
    }
  };

  paneDidUnmount(node) {
    if (node) {
      node.childNodes[0].children[1].addEventListener("scroll", this.handleScroll, true);
    }
  }

  setViewMore = (boolean, coinType, cbFucntion, value) => {
    if (boolean && coinType.data.length < coinType.totalCount) {
      GetWalletData(this.state.walletid, getCookie("token"), value, coinType.pageState, this.state.allCoinsWithoutPageState)
        .then((res) => {
          let oldAllData = { ...coinType };
          let oldAllDataArr = [];
          oldAllDataArr.push(...oldAllData.data, ...res.data.data);
          oldAllData["data"] = oldAllDataArr;
          oldAllData["pageState"] = res.data.pageState;
          cbFucntion(oldAllData);
          this.setState({ viewMore: false });
        })
        .catch((err) => this.setState({ viewMore: false }));
    } else {
      this.setState({ viewMore: false });
    }
  };

  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - node.scrollTop === node.clientHeight;
    if (bottom && !this.state.viewMore) {
      console.log("[btm]");
      // if (!this.state.viewMore) {
      if (this.state.value === 0) {
        this.setViewMore(true, this.state.allCoinsData, this.setAllCoinsData, this.state.value);
      } else if (this.state.value === 1) {
        this.setViewMore(true, this.state.inData, this.setInData, this.state.value);
      } else if (this.state.value === 2) {
        this.setViewMore(true, this.state.outData, this.setOutData, this.state.value);
      }
      // }
    }
  };

  componentWillUnmount() {
    this.paneDidUnmount(this.myRef.current);
  }

  renderLinksBasedOnProps = () => {
    let { checkForIndirectRouting, setIndirectRouting } = this.props;
    if (checkForIndirectRouting) {
      this.props.history.push("/app");
      setIndirectRouting(false);
    } else {
      this.props.onClose();
    }
  };

  render() {
    return (
      <div style={{ width: "100vw" }}>
        <div className="col-12 py-3 mobile_recentTransactionsScreen">
          <img src={Icons.PinkBack} alt="left-arrow" height={20} width={25} onClick={this.renderLinksBasedOnProps} />
          <div className="text-center">Recent Transactions</div>
        </div>
        <div className="m_wallet_info" ref={this.myRef}>
          <ScrollableTabs
            tabs={[{ label: "All" }, { label: "Bought" }, { label: "Spent" }]}
            setValue={this.setValue}
            mobile={true}
            tabContent={[
              {
                content: <MobileAllRecentTransactions m_withdraw_wallet_all_data={this.state.allCoinsData.data} />,
              },
              { content: <MobileCoinIn m_withdraw_wallet_all_data={this.state.inData.data} /> },
              {
                content: <MobileCoinOut m_withdraw_wallet_all_data={this.state.outData.data} />,
              },
            ]}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    checkForIndirectRouting: state.CoinConfig.indirectRouting,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setIndirectRouting: (bool) => dispatch(indirectRouting(bool)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Mobile_RecentTransactions));
