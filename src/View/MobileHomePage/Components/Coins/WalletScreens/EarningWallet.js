import React, { useState } from "react";
import MainDrawer from "../../../../../Components/Drawer/Drawer";
import MobileWithdrawEarningLogs from "./WithdrawalAndEarningLogs";
import { Icons } from "../../../../../Components/Icons/Icons";
import { Images } from "../../../../../Components/Images/Images";
import WithdrawEarnings from "../../WithdrawEarnings/WithdrawEarnings";

const Mobile_EarningWallet = ({ CoinBalance, balance, withdraw, earnings }) => {
  const [openDrawer, setToggleDrawer] = useState(false);
  const toggleDrawer = () => setToggleDrawer(!openDrawer);
  const [withdrawEarningDrawer, setWithdrawEarningDrawer] = useState(false);
  const toggleWithdrawEarningsDrawer = () => setWithdrawEarningDrawer(!withdrawEarningDrawer);
  return (
    <div>
      <div className="col-12 py-2">
        <div className="row">
          <div className="col-9 px-0 d-flex align-items-center">
            <div>
              <img src={Images.dollarIcon} alt="dollar-icon" height={30} width={30} />
            </div>
            <div className="coin_balance">{CoinBalance} Credits</div>
          </div>
          <div className="coin_balance_earning_wallet">
            Your Balance is: <span>{balance.toFixed(2)} INR</span>
          </div>
        </div>
      </div>
      <div className="col-12 withdrawal_and_earning_logs_button" onClick={toggleDrawer}>
        Withdrawal and Earning Logs
      </div>
      <div className="col-12 py-3 mt-2" style={{ background: "#e5e5e5" }}>
        <div className="row">
          <div className="col-2 d-flex justify-content-center align-items-center">
            <img src={Icons.money} alt="money-bag" width={45} height={45} />
          </div>
          <div className="col-10 px-0">
            <div className="buy_coins">Withdrawal Method</div>
            <div className="buy_coins_helper_text">Select any of the payment receiving method to withdraw the money.</div>
          </div>
        </div>
      </div>
      <div className="col-12 py-3" onClick={toggleWithdrawEarningsDrawer}>
        <div className="row">
          <div className="col-2 d-flex justify-content-center align-items-center">
            <img src={Icons.paypal} alt="paypal" width={37} height={37} />
          </div>
          <div className="col-10 px-0 d-flex align-items-center m_payment_paypal">PAYPAL</div>
        </div>
      </div>
      {/* <div className="col-12 py-3">
        <div className="row">
          <div className="col-2 d-flex justify-content-center align-items-center">
            <img src={require("../../../../../asset/mobile_img/jp_logo.png")} style={{ objectFit: "cover" }} width={37} height={37} />
          </div>
          <div className="col-10 px-0 d-flex align-items-center">JAMBOPAY</div>
        </div>
      </div> */}
      <MainDrawer width={400} onClose={toggleDrawer} onOpen={toggleDrawer} open={openDrawer}>
        <MobileWithdrawEarningLogs withdraw={withdraw} earnings={earnings} onClose={toggleDrawer} />
      </MainDrawer>
      <MainDrawer width={400} onClose={toggleWithdrawEarningsDrawer} onOpen={toggleWithdrawEarningsDrawer} open={withdrawEarningDrawer}>
        <WithdrawEarnings onClose={toggleWithdrawEarningsDrawer} />
      </MainDrawer>
    </div>
  );
};

export default Mobile_EarningWallet;
