import React from "react";
import moment from "moment";
import no_transaction from "../../../../../../asset/images/default_icon.png";

function M_WithdrawalLogs({ m_earning_wallet_all_data }) {
  return m_earning_wallet_all_data && m_earning_wallet_all_data.length > 0 ? (
    m_earning_wallet_all_data.map((k, i) => (
      <div className="col-12 py-2 mb-2 m_transaction_card" key={i}>
        <div className="row">
          <span className="m_transaction_status_label">Status: </span>
          <span className="m_transaction_status pl-1">New</span>
        </div>
        <div className="row">
          <div className="col-8 m_transaction_time">{moment(k.txntimestamp).format("LLL")}</div>
        </div>
        <div className="row pb-3 pt-2">
          <span className="m_transaction_id_withdraw_id">ID: </span>
          <span className="m_transaction_withdraw_txn_id pl-1">{k.txnid}</span>
        </div>
        <div className="row border-top pt-2">
          <div className="col-8 m_transaction_status_label">Amount:</div>
          <div className="col-4 m_transaction_amount">{k.amount}</div>
        </div>
      </div>
    ))
  ) : (
    <div className="col-12" style={{ height: "100vh", width: "100vw" }}>
      <div className="text-center">
        <img src={no_transaction} height={240} width={180} alt="no-transaction" />
      </div>
      <div className="text-center" style={{ fontFamily: "Circular Air Book", color: "#d1d1d1" }}>
        There is no withdraw happen yet.
      </div>
    </div>
  );
}

export default M_WithdrawalLogs;
