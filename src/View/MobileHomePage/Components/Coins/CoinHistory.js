// Main React Components
import React, { Component } from "react";

// Re-Usuable Components
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";

// Materail UI Components
import { withStyles } from "@material-ui/core/styles";

// Redux Components
import { CoinSpentHistory } from "../../../../controller/auth/verification";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "30vh",
    textAlign: "center",
  },
});

const coininpro = <p>Coin-In</p>;
const coinoutpro = <p>Coin-Out</p>;

class CoinHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserFinalData: "",
      modelcoin: false,
      coinupgrade: "",
      Coinspenthistroy: "",
    };
  }

  componentDidMount() {
    CoinSpentHistory().then((data) => {
      this.setState({ Coinspenthistroy: data.data.data });
    });
  }

  render() {
    const { classes } = this.props;

    // Coin-In
    const CoinIn = (
      <div className="m-0 row">
        {this.state.Coinspenthistroy.coinIn && this.state.Coinspenthistroy.allCoins.length > 0 ? (
          this.state.Coinspenthistroy.coinIn.map((data, index) => (
            <div key={index} className="m-0 p-0 row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border-bottom Wspent_Coin">
              <div className="py-2 col-1">
                {data.txnType == "DEBIT" ? <i className="fas fa-arrow-up Wcoindebit" /> : <i className="fas fa-arrow-down Wcoincredit" />}
              </div>
              <div className="py-2 col-9">
                <p className="m-0">{data.trigger} </p>
              </div>
              <div className="py-2 col-2 text-right">
                <span>{data.coinAmount}</span>
              </div>
            </div>
          ))
        ) : (
          <div className="m-auto">
            <h5 style={{ position: "relative", top: "10em" }}>No Credits-In History.</h5>
          </div>
        )}
      </div>
    );

    // Coin-Out
    const CoinOut = (
      <div className="m-0 row">
        {this.state.Coinspenthistroy.coinOut && this.state.Coinspenthistroy.allCoins.length > 0 ? (
          this.state.Coinspenthistroy.coinOut.map((data, index) => (
            <div key={index} className="m-0 p-0 row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border-bottom Wspent_Coin">
              <div className="py-2 col-1">
                {data.txnType == "DEBIT" ? <i className="fas fa-arrow-up Wcoindebit" /> : <i className="fas fa-arrow-down Wcoincredit" />}
              </div>
              <div className="py-2 col-9">
                <p className="m-0">{data.trigger} </p>
              </div>
              <div className="py-2 col-2 text-right">
                <span>{data.coinAmount}</span>
              </div>
            </div>
          ))
        ) : (
          <div className="m-auto">
            <h5 style={{ position: "relative", top: "10em" }}>No Credits-Out History.</h5>
          </div>
        )}
      </div>
    );

    return (
      <div style={{ width: "100vw", height: "100vh" }}>
        {/* Header Module */}
        <div className="pt-3 MCommonwidth">
          <div className="col-12">
            <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
            <div className="py-3 MCommonheader">
              <p>Recent Transactions</p>
            </div>
          </div>

          {/* Tabs Module */}
          <div className="MCoinhistory">
            <div className={classes.root}>  
              <SimpleTabs tabs={[{ label: coininpro }, { label: coinoutpro }]} tabContent={[{ content: CoinIn }, { content: CoinOut }]} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CoinHistory);
