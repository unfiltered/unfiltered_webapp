import React from "react";
import "./CoinBalance.scss";
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";
import { withStyles } from "@material-ui/core/styles";
// import CircularProgress from "@material-ui/core/CircularProgress";

// Languges Components
// import { FormattedMessage } from "react-intl";

import { connect } from "react-redux";
import {
  BuyNewCoins,
  CurrentCoinBalance,
  getWalletData_without_txnType,
  GetEarningWalletTransactionWithTxn,
  walletEarning,
} from "../../../../controller/auth/verification";
import MobileEarningWallet from "./WalletScreens/EarningWallet";
import MobileWalletCurrentScreen from "./WalletScreens/CurrentScreen";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "20vh",
    textAlign: "center",
  },
});

class CoinBalance extends React.Component {
  state = {
    openRecentTransaction: false,
    earningWalletWithdrawData: [],
    earningWalletCreditData: [],
    coinsEarned: "",
    balance: "",
    coinPlansToBuy: "",
    allCoinsWithoutPageState: false,
    value: 0,
  };
  openRecentTransactionsScreen = () => {
    this.setState({ openRecentTransaction: !this.state.openRecentTransaction });
  };

  setValue = (val) => this.setState({ value: val });

  componentDidMount() {
    BuyNewCoins().then((data) => {
      this.setState({ coinPlansToBuy: data.data.data });
    });
    CurrentCoinBalance(getCookie("uid"), getCookie("token")).then((res) => {
      if (res.data.walletData.length > 0) {
        getWalletData_without_txnType(res.data.walletData[0].walletid, getCookie("token")).then((res) => {
          this.setState({ m_withdraw_wallet_all_data: res.data.data });
        });
      }
      if (res.data.walletEarningData.length > 0) {
        this.setState({ coinsEarned: res.data.walletEarningData[0].balance });
        /** This API gets Earning wallet info, not array of object data */
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 1, false).then((res) => {
          this.setState({ earningWalletCreditData: res.data, allCoinsWithoutPageState: true });
        });
        GetEarningWalletTransactionWithTxn(res.data.walletEarningData[0].walletearningid, getCookie("token"), 2, false).then((res) => {
          this.setState({ earningWalletWithdrawData: res.data });
        });
        walletEarning(getCookie("token"), getCookie("uid"), "user", "INR")
          .then((res) => {
            this.setState({ balance: res.data.userPreferedCurrency.withdrawAmount });
          })
          .catch((err) => console.log("[walletEarning] err", err));
      }
    });
  }

  render() {
    return (
      <div style={{ width: "100vw" }}>
        <div className="col-12 py-3">
          <div className="row">
            <div className="col-10 coin_balance_header">Wallet</div>
            <div className="col-2" onClick={this.props.onClose}>
              <img src={Icons.PinkCloseBtn} width={17} height={17} alt="close-btn" />
            </div>
          </div>
        </div>
        <div className="m_wallet_tabs">
          <SimpleTabs
            // tabs={[{ label: "Wallet" }, { label: "Earnings" }]}
            tabs={[{ label: "Credits" }]}
            isCentered={true}
            mobile={true}
            setValue={this.setValue}
            tabContent={[
              {
                content: (
                  <MobileWalletCurrentScreen
                    CoinBalance={this.props.CoinBalance}
                    openRecentTransactionsScreen={this.openRecentTransactionsScreen}
                    data={this.state.coinPlansToBuy}
                  />
                ),
              },
              // {
              //   content: (
              //     <MobileEarningWallet
              //       CoinBalance={this.state.coinsEarned}
              //       balance={this.state.balance}
              //       withdraw={this.state.earningWalletWithdrawData.data}
              //       earnings={this.state.earningWalletCreditData.data}
              //     />
              //   ),
              // },
            ]}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    currencySymbol: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(CoinBalance));
