// Main React Components
import React from "react";
import { FormattedMessage } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import "react-input-range/lib/css/index.css";

const styles = {
  root: {
    width: 300,
  },
  slider: {
    padding: "5px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "10vh",
    textAlign: "center",
  },
  radioControl: {
    display: "flex",
  },
};

function Payment({ closelocationdrawer, openlocationdrawer, onClose }) {
  //   const locationdrawer = <Location onClose={closelocationdrawer} />;
  return (
    <div style={{ height: "100vh" }}>
      {/* <MainDrawer width={400} onClose={closelocationdrawer} onOpen={openlocationdrawer} open={this.state.location}>
        {locationdrawer}
      </MainDrawer> */}
      <div className="pt-3 px-3 MCommonwidth">
        <i
          className="fa fa-arrow-left"
          onClick={onClose}
          style={{
            fontSize: "24px",
            color: "#F74167",
          }}
        />
        <div className="py-3 MCommonheader">
          <FormattedMessage tagName="p" id="message.select" />
          <FormattedMessage tagName="p" id="message.paymentMethod" />
        </div>
      </div>
      <div className="col-12 py-3 border-bottom">
        <div className="row">
          <div className="col-2 d-flex justify-content-center align-items-center">
            <img src={require("../../../../asset/images/paypal.svg")} alt="paypal" width={37} height={37} />
          </div>
          <div className="col-10 px-0 d-flex align-items-center">PAYPAL</div>
        </div>
      </div>
      {/* <div className="col-12 py-3 border-bottom">
        <div className="row">
          <div className="col-2 d-flex justify-content-center align-items-center">
            <img src={require("../../../../asset/mobile_img/jp_logo.png")} alt="jamboPay" style={{ objectFit: "cover" }} width={37} height={37} />
          </div>
          <div className="col-10 px-0 d-flex align-items-center">JAMBOPAY</div>
        </div>
      </div> */}
    </div>
  );
}

export default withStyles(styles)(Payment);
