import React from "react";
import { withdrawCoins, CurrentCoinBalance, walletEarning, getEstimatedCoins } from "../../../../controller/auth/verification";
import "./WithdrawEarnings.scss";
import { withStyles } from "@material-ui/core/styles";
import { getCookie } from "../../../../lib/session";
import MATTextField from "../../../../Components/Input/TextField";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { Icons } from "../../../../Components/Icons/Icons";

const styles = () => ({
  input: {
    width: "100%",
    margin: "10px 0",
  },
  accountInput: { width: "100%", margin: "10px 0" },
  underline: {
    "&:before": {
      borderBottom: `1px solid #e31b1b`,
    },
  },
});

class WithdrawEarnings extends React.Component {
  state = {
    coinsEarned: "",
    balance: "",
    inputCoin: "",
    accountID: "",
    outputCoin: "",
    value: 0,
    variant: "success",
    usermessage: "",
    open: false,
  };
  withdrawCoinsAPI = (currency) => {
    if (this.state.inputCoin >= this.state.balance) {
      this.setState({ usermessage: "Withdraw amount should be less than maximum withdrawal", open: true });
      setTimeout(() => {
        this.setState({ usermessage: "", open: false });
      }, 2000);
    } else {
      withdrawCoins(this.state.inputCoin, this.state.outputCoin, getCookie("token"), this.state.accountID.toString(), currency, getCookie("uid"))
        .then((res) => {
          this.setState({ inputCoin: "", outputCoin: 0, accountID: "", usermessage: "Withdraw Coins Successfull", open: true });
          setTimeout(() => {
            this.setState({ inputCoin: "", transactionDone: false, transactionDoneModal: true, outputCoin: 0 });
            this.callEarningWalletAPI();
          }, 500);
          setTimeout(() => {
            this.setState({ inputCoin: "", usermessage: "", open: false });
          }, 2000);
        })
        .catch((err) => console.log("err", err));
    }
  };
  componentDidMount() {
    CurrentCoinBalance(getCookie("uid"), getCookie("token")).then((res) => {
      if (res.data.walletEarningData.length > 0) {
        this.setState({ coinsEarned: res.data.walletEarningData[0].balance });
        walletEarning(getCookie("token"), getCookie("uid"), "user", "INR")
          .then((res) => {
            this.setState({ balance: res.data.userPreferedCurrency.withdrawAmount });
          })
          .catch((err) => {
            console.log("[walletEarning] err", err);
            this.setState({ outputCoin: 0 });
          });
      }
    });
  }

  callEarningWalletAPI = () => {
    CurrentCoinBalance(getCookie("uid"), getCookie("token")).then((res) => {
      if (res.data.walletEarningData.length > 0) {
        this.setState({ coinsEarned: res.data.walletEarningData[0].balance, inputCoin: "" });
        walletEarning(getCookie("token"), getCookie("uid"), "user", "INR")
          .then((res) => {
            this.setState({ balance: res.data.userPreferedCurrency.withdrawAmount });
          })
          .catch((err) => {
            console.log("[walletEarning] err", err);
            this.setState({ outputCoin: 0 });
          });
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.inputCoin !== this.state.inputCoin) {
      getEstimatedCoins(getCookie("token"), getCookie("uid"), this.state.inputCoin, "user", "INR")
        .then((res) => {
          this.setState({ outputCoin: res.data.data.userPreferedCurrency.withdrawAmount.toFixed(2) });
        })
        .catch((err) => console.log("[GetEstimatedCoins] err", err));
    }
  }

  updateInputValue = (input) => {
    try {
      let val = input.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");
      console.log("val is ->>>", val);
      return val;
    } catch (e) {
      console.log("error occured");
    }
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    let { classes } = this.props;
    return (
      <div style={{ height: "100vh" }}>
        <div className="pt-3 px-3 MCommonwidth">
          <img src={Icons.PinkBack} alt="back" width={25} onClick={this.props.onClose} />
          <div className="py-3 MCommonheader">
            <div className="request">Request</div> <div className="withdrawDetails">Withdraw Details</div>
          </div>
        </div>
        <div className="col-12 py-3">
          <MATTextField
            onChange={this.handleChange("accountID")}
            value={this.state.accountID}
            className={classes.accountInput + " m_c_accountInput"}
            label={"Enter Account ID"}
          />
          <div className="row">
            <div className="col-6">
              <MATTextField
                type="number"
                onChange={this.handleChange("inputCoin")}
                value={this.state.inputCoin.length === 0 ? "" : this.updateInputValue(this.state.inputCoin)}
                className={classes.input + " m_c_accountInput"}
                label={"Enter Coin"}
              />
            </div>
            <div className="col-6">
              <MATTextField
                type="number"
                value={this.state.inputCoin.length === 0 ? 0 : this.state.outputCoin}
                className={classes.input + " m_c_accountInput"}
                label={"Estimated Amount"}
                disabled={true}
              />
            </div>
            <div className="earningsModalMaxAmount text-left pl-3">
              Max Withdrawal: <span>{this.state.coinsEarned}</span>
            </div>
          </div>
        </div>
        <div className="col-12 d-flex justify-content-center" onClick={() => this.withdrawCoinsAPI("INR")}>
          <div className="submitBtn1">SUBMIT</div>
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default withStyles(styles)(WithdrawEarnings);
