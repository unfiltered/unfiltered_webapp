import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Prospects.scss";
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";
import { Media } from "reactstrap";
import { withStyles } from "@material-ui/core/styles";
import CoinBalances from "../Coins/CoinBalance";
import MobileSettings from "../Settings/Setings";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { CircularProgress } from "@material-ui/core";
import {
  UserOnline,
  Superlikedby,
  Profilelikedby,
  RecentVisitors,
  PassedUser,
  MyLikes,
  MySuperLike,
  IThumbsUpped,
  ThumbsUpMe,
  History,
} from "../../../../controller/auth/verification";
import { Images } from "../../../../Components/Images/Images";
import { Icons } from "../../../../Components/Icons/Icons";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  progress: {
    color: "#e31b1b",
    marginTop: "30vh",
    textAlign: "center",
  },
});

const Onlinepro = <FormattedMessage id="message.tabpropects1" />;
const SuperLikespro = <FormattedMessage id="message.tabpropects2" />;
const Likespro = <FormattedMessage id="message.tabpropects3" />;
const Recentpro = <FormattedMessage id="message.tabpropects4" />;
const Passedpro = <FormattedMessage id="message.tabpropects5" />;
const Mylikepro = <FormattedMessage id="message.tabpropects6" />;
const MySuperlikepro = <FormattedMessage id="message.tabpropects7" />;

const IgotThumbsUpped = <FormattedMessage id="message.tabpropects8" />;
const Thumbsuppedme = <FormattedMessage id="message.tabpropects9" />;
const SwipeHistory = <FormattedMessage id="message.tabpropects10" />;

class Prospects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      onlineuser: "",
      Superlikedbyother: "",
      Userlikeme: "",
      recentvisitor: "",
      Passeduser: "",
      Usermylike: "",
      Mysuperlike: "",
      igotThumbsUpped: "",
      thumbsuppedme: "",
      swipeHistory: "",
      coinDrawer: false,
      settingsDrawer: false,
      value: 0,
    };
  }

  setValue = (val) => this.setState({ value: val });

  toggleSettingsDrawer = () => {
    this.setState({ settingsDrawer: !this.state.settingsDrawer });
  };

  toggleCoinDrawer = () => {
    this.setState({ coinDrawer: !this.state.coinDrawer });
  };

  componentDidMount() {
    // Online User API
    this.props.handlevalue(1);
    UserOnline()
      .then((data) => {
        this.setState({
          onlineuser: data.data.data,
          loader: false,
        });
      })
      .catch((err) => this.setState({ onlineuser: [], loader: false }));

    // Superlikedby API
    Superlikedby()
      .then((data) => {
        this.setState({
          Superlikedbyother: data.data.data,
        });
      })
      .catch((err) => this.setState({ Superlikedbyother: [] }));

    // Profilelikedby API
    Profilelikedby()
      .then((data) => {
        this.setState({
          Userlikeme: data.data.data,
        });
      })
      .catch((err) => this.setState({ Userlikeme: [] }));

    // RecentVisitors API
    RecentVisitors()
      .then((data) => {
        this.setState({
          recentvisitor: data.data.data,
        });
      })
      .catch((err) => this.setState({ recentvisitor: [] }));

    // PassedUser API
    PassedUser()
      .then((data) => {
        this.setState({
          Passeduser: data.data.data,
        });
      })
      .catch((err) => this.setState({ Passeduser: [] }));

    // MyLikes API
    MyLikes()
      .then((data) => {
        this.setState({
          Usermylike: data.data.data,
        });
      })
      .catch((err) => this.setState({ Usermylike: [] }));

    // MySuperLike APi
    MySuperLike()
      .then((data) => {
        this.setState({
          Mysuperlike: data.data.data,
        });
      })
      .catch((err) => this.setState({ Mysuperlike: [] }));

    IThumbsUpped()
      .then((data) => {
        this.setState({ igotThumbsUpped: data.data.data });
      })
      .catch((err) => {
        this.setState({ igotThumbsUpped: [] });
      });

    ThumbsUpMe()
      .then((data) => {
        this.setState({ thumbsuppedme: data.data.data });
      })
      .catch((err) => {
        this.setState({ thumbsuppedme: [] });
      });

    History()
      .then((data) => {
        this.setState({ swipeHistory: data.data.data });
      })
      .catch((err) => {
        this.setState({ swipeHistory: [] });
      });
  }

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  render() {
    const { classes } = this.props;

    const Online =
      this.state.onlineuser && this.state.onlineuser.length > 0 ? (
        this.state.onlineuser.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.U_Alert} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Online</div>
          <div className="m_prospects_subText">Sorry no potential matches are currently online.</div>
        </div>
      );

    const Superliked =
      this.state.Superlikedbyother && this.state.Superlikedbyother.length > 0 ? (
        this.state.Superlikedbyother.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.superLike} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Super Liked me</div>
          <div className="m_prospects_subText">
            Sorry no user has super liked you as yet Keep your profile fully updated to improve your chances to get matched.
          </div>
        </div>
      );

    const Likes =
      this.state.Userlikeme && this.state.Userlikeme.length > 0 ? (
        this.state.Userlikeme.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.likesMe} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Likes Me</div>
          <div className="m_prospects_subText">
            Sorry no user has liked your profile yet. Keep your profile fully updated to improve your chances to get liked.
          </div>
        </div>
      );

    const recent =
      this.state.recentvisitor && this.state.recentvisitor.length > 0 ? (
        this.state.recentvisitor.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.recentVisitor} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Recent Vistors</div>
          <div className="m_prospects_subText">Sorry You have not received any visits yet.</div>
        </div>
      );

    const Passed =
      this.state.Passeduser && this.state.Passeduser.length > 0 ? (
        this.state.Passeduser.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.U_Alert} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Passed</div>
          <div className="m_prospects_subText">Way to go! Seems like you dont't swipe left ever !.</div>
        </div>
      );

    const Mylike =
      this.state.Usermylike && this.state.Usermylike.length > 0 ? (
        this.state.Usermylike.map((data, index) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media key={index} className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.likesMe} alt="nolike" title="nolike" />
          <div className="m_prospects_label">My Likes</div>
          <div className="m_prospects_subText">
            Oops! Seems like you have not swiped right on any profile yet. Update your preferences to get better potential matches.
          </div>
        </div>
      );

    const MySuperlike =
      this.state.Mysuperlike && this.state.Mysuperlike.length > 0 ? (
        this.state.Mysuperlike.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.superLike} alt="nolike" title="nolike" />
          <div className="m_prospects_label">My Superlikes</div>
          <div className="m_prospects_subText">
            Oops! Seems like you have not used your super like power yet. Click on the little start button to super like.
          </div>
        </div>
      );

    const IGotThumbsUpped =
      this.state.igotThumbsUpped && this.state.igotThumbsUpped.length > 0 ? (
        this.state.igotThumbsUpped.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.likesMe} alt="nolike" title="nolike" />
          <div className="m_prospects_label">My Thumbs Up</div>
          <div className="m_prospects_subText">Oops! Seems like you have not thumbs up any profile yet.</div>
        </div>
      );

    const ThumbsUppedMe =
      this.state.thumbsuppedme && this.state.thumbsuppedme.length > 0 ? (
        this.state.thumbsuppedme.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.likesMe} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Thumbs Upped Me</div>
          <div className="m_prospects_subText">
            Sorry no user has thumbs up your profile yet. Keep your profile fully updated to improve your chances to get thumbs
          </div>
        </div>
      );

    const _SwipeHistory =
      this.state.swipeHistory && this.state.swipeHistory.length > 0 ? (
        this.state.swipeHistory.map((data) => (
          <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId ? data.opponentId : data._id}`}>
            <Media className="py-2 col-12 Mcmn_prospect">
              <Media left className="u_img">
                <Media object src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" />
              </Media>
              <Media body className="col-12 Mcardmain">
                <Media heading>{data.firstName}</Media>
              </Media>
            </Media>
          </Link>
        ))
      ) : (
        <div className="nomorelike text-center">
          <img className="img-fluid" width={130} height={130} src={Icons.recentVisitor} alt="nolike" title="nolike" />
          <div className="m_prospects_label">Swipe History</div>
          <div className="m_prospects_subText">Sorry You have not received any swipes yet.</div>
        </div>
      );

    return (
      <div>
        <div className="col-12 py-3 m_prospects_stickyHeader">
          <MainDrawer onClose={this.toggleSettingsDrawer} onOpen={this.toggleSettingsDrawer} open={this.state.settingsDrawer}>
            <MobileSettings onClose={this.toggleSettingsDrawer} UserFinalData={this.props.UserFinalData} />
          </MainDrawer>
          <MainDrawer onClose={this.toggleCoinDrawer} onOpen={this.toggleCoinDrawer} open={this.state.coinDrawer}>
            <CoinBalances onClose={this.toggleCoinDrawer} UserFinalData={this.props.UserFinalData} />
          </MainDrawer>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-4 friends_header">Prospects</div>
            <div className="col-4">
              <div className="row header_row">
                <div className="global_coinbalance col-8" onClick={this.toggleCoinDrawer}>
                  <img src={Images.dollarIcon} className="img-fluid" alt="dollarimg" title="dollarimg" />
                  <p className="m-0">{this.renderCoins(this.props.CoinBalance)}</p>
                </div>
                <div className="col-4" onClick={this.toggleSettingsDrawer}>
                  <img
                    style={{ position: "absolute", bottom: "-2px" }}
                    src={this.props && this.props.userProfilePicture}
                    alt={this.props.UserFinalData.firstName}
                    height={30}
                    width={30}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="MProspectstabs">
          <div className={classes.root}>
            <SimpleTabs
              mobile={true}
              setValue={this.setValue}
              tabs={[
                { label: Onlinepro },
                { label: SuperLikespro },
                { label: Likespro },
                { label: Recentpro },
                { label: Passedpro },
                { label: Mylikepro },
                { label: MySuperlikepro },
                { label: Thumbsuppedme },
                { label: IgotThumbsUpped },
                { label: SwipeHistory },
              ]}
              tabContent={[
                {
                  content: this.state.loader ? (
                    <div className="m_chat_Loader">
                      <CircularProgress />
                      <span className="pl-2">Loading...</span>
                    </div>
                  ) : (
                    Online
                  ),
                },
                { content: Superliked },
                { content: Likes },
                { content: recent },
                { content: Passed },
                { content: Mylike },
                { content: MySuperlike },
                { content: IGotThumbsUpped },
                { content: ThumbsUppedMe },
                { content: _SwipeHistory },
              ]}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    CoinBalance: state.UserProfile.CoinBalance,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(Prospects));
