import React, { Component } from "react";
import "./Footer.scss";
import { storeCoins, leftSideChatLogs, updateAcknowledgement } from "../../../../actions/UserProfile";
import { getCookie, setCookie } from "../../../../lib/session";
import { getCoinConfig, BuyNewPlan } from "../../../../controller/auth/verification";
import { boostActivatedSessionIncomingData } from "../../../../actions/boost";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { __mqttData } from "../../../../actions/mqtt-data";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { __updateMqttMessage } from "../../../../actions/webrtc";
// import { FormattedMessage } from "react-intl";
// import * as coinCfg from "../../../../actions/coinConfig";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { connect } from "react-redux";
import * as actionChat from "../../../../actions/chat";
import { Mqtt, ackOfMessage } from "../../../../lib/Mqtt/Mqtt";
import { Icons } from "../../../../Components/Icons/Icons";
import PremiumUserContent from "../../Components/ModalContent/PremiumUser";
import * as coinCfg from "../../../../actions/coinConfig";
import { FRIEND_REQUEST_COUNT_FUNC } from "../../../../actions/Friends";
// import * as func from "../../../../init-fcm";
import MobilePayPalPayment from "../Coins/WalletScreens/MobilePayPalPayment";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import { MOBILE_ACTION_FUNC } from "../../../../actions/User";

const styles = () => ({
  root: {
    flexGrow: 1,
    height: "inherit",
    width: "100%",
  },
  labelActive: {
    color: "#f80402",
    fontSize: "13px",
  },
  labelNotActive: {
    color: "#fff",
    fontSize: "13px",
  },
  tabIndicator: {
    borderBottom: "2px solid #f80402",
    color: "#f80402",
  },
});

const Discover = "Nearby";
const Prospects = "Prospects";
const Dates = "Dates";
const Matches = "Matches";
const Friends = "Friends";

class Footer extends Component {
  state = {
    premiumUserModal: false,
    paymentPage: false,
    plan: {},
    allPlans: "",
    index: "",
    purchaseSubscriptionPlan: true,
  };

  togglePaymentPage = () => {
    this.setState({ paymentPage: !this.state.paymentPage });
  };

  storePlanDetails = (planName, index) => {
    this.setState({ plan: planName, index: index });
  };

  handleChange = (event, value) => {
    if (this.props.ProUser && this.props.ProUser.subscriptionId === "Free Plan" && value === 1) {
      this.toggleUpdateToPremiumUserModal();
    } else if (this.props.ProUser && this.props.ProUser.subscriptionId !== "Free Plan" && value === 1) {
      this.props.history.push("/app/prospects");
      this.props.handleChangevalue(value);
    } else if (value === 0) {
      this.props.history.push("/app");
      this.props.handleChangevalue(value);
    } else if (value === 2) {
      this.props.history.push("/app/dates");
      this.props.handleChangevalue(value);
    } else if (value === 3) {
      this.props.history.push("/app/chat");
      this.props.handleChangevalue(value);
    } else if (value === 4) {
      this.props.history.push("/app/friends");
      this.props.handleChangevalue(value);
    }
  };

  toggleUpdateToPremiumUserModal = () => {
    this.setState({ premiumUserModal: !this.state.premiumUserModal });
  };

  componentDidMount() {
    getCoinConfig(getCookie("token")).then((res) => {
      if (res.status !== 401) {
        this.props.saveCoinConfig(res.data.data[0]);
      } else if (res.status === 401) {
        setCookie("uid", "");
        setCookie("token", "");
      }
    });
    BuyNewPlan().then((res) => {
      console.log("allPlans", res.data.data);
      this.setState({ allPlans: res.data.data });
      this.props.storeElitePlan(res.data.data["UnFiltered Elite"][0]);
    });
  }
  render() {
    const { classes, value } = this.props;

    return (
      <div className="MFooter">
        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={this.handleChange}
              className="Maininnertabs"
              indicatorColor="primary"
              textColor="primary"
              scrollButtons="off"
              classes={{
                indicator: classes.tabIndicator,
              }}
            >
              <Tab
                label={Discover}
                classes={{
                  label: value === 0 ? classes.labelActive : classes.labelNotActive,
                }}
                icon={
                  <img src={value === 0 ? Icons.TabDiscoverActive : Icons.TabDiscoverNotActive} height={20} width={20} alt="Discover" />
                }
              />
              <Tab
                label={Prospects}
                classes={{
                  label: value === 1 ? classes.labelActive : classes.labelNotActive,
                }}
                icon={
                  <img src={value === 1 ? Icons.TabProspectsActive : Icons.TabProspectsNotActive} height={20} width={22} alt="Prospects" />
                }
              />
              <Tab
                label={Dates}
                classes={{
                  label: value === 2 ? classes.labelActive : classes.labelNotActive,
                }}
                icon={<img src={value === 2 ? Icons.TabDateActive : Icons.TabDateNotActive} height={20} width={22} alt="Dates" />}
              />
              <Tab
                label={Matches}
                classes={{
                  label: value === 3 ? classes.labelActive : classes.labelNotActive,
                }}
                icon={
                  <>
                    <img src={value === 3 ? Icons.TabMatchActive : Icons.TabMatchNotActive} height={20} width={22} alt="Matches" />
                    <span className="m_tab_unreadCount">{this.props && this.props.unreadChatCount}</span>
                  </>
                }
              />
              <Tab
                label={Friends}
                classes={{
                  label: value === 4 ? classes.labelActive : classes.labelNotActive,
                }}
                icon={<img src={value === 4 ? Icons.TabFriendActive : Icons.TabFriendNotActive} height={20} width={23} alt="Friends" />}
              />
            </Tabs>
          </AppBar>
          {/* <Mqtt />; */}
        </div>
        <MatUiModal width={"90%"} toggle={this.toggleUpdateToPremiumUserModal} isOpen={this.state.premiumUserModal}>
          <PremiumUserContent
            storePlanDetails={this.storePlanDetails}
            allPlans={this.state.allPlans}
            togglePaymentPage={this.togglePaymentPage}
            toggle={this.toggleUpdateToPremiumUserModal}
          />
        </MatUiModal>
        <MainDrawer width={400} onClose={this.togglePaymentPage} onOpen={this.togglePaymentPage} open={this.state.paymentPage}>
          <MobilePayPalPayment
            allPlans={this.state.allPlans}
            purchaseSubscriptionPlan={this.state.purchaseSubscriptionPlan}
            planName={this.state.plan}
            index={this.state.index}
            onClose={this.togglePaymentPage}
            isCoinPlan={false}
          />
        </MainDrawer>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    UserSelectedToChat: state.UserSelectedToChat.UserSelectedToChat,
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    friendReqCount: state.Friends.FriendRequest,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addNewChatToExistingObject: (msg) => dispatch(actionChat.addNewChatToExistingObject(msg)),
    leftSideChatLogsFN: (allChatLogs) => dispatch(leftSideChatLogs(allChatLogs)),
    _storeUnreadChatCount: (count) => dispatch(actionChat.storeUnreadChatCount(count)),
    boostActivatedSessionIncomingData: (data) => dispatch(boostActivatedSessionIncomingData(data)),
    __updateMqttMessage: (data) => dispatch(__updateMqttMessage(data)),
    __mqttData: (data) => dispatch(__mqttData(data)),
    setCoinBalance: (coins) => dispatch(storeCoins(coins)),
    saveCoinConfig: (data) => dispatch(coinCfg.getCoinConfig(data)),
    UPDATE_ACKNOWLEDGEMENT: (msgId, status) => dispatch(updateAcknowledgement(msgId, status)),
    storeFriendRequestCount: (count) => dispatch(FRIEND_REQUEST_COUNT_FUNC(count)),
    storeElitePlan: (data) => dispatch(MOBILE_ACTION_FUNC("UnFilteredPlus", data)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Footer)));
