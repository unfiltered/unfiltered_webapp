import React from "react";
import { connect } from "react-redux";
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";
import MobileNewsFeed from "../NewsFeed/MobileNewsFeed";
import Button from "../../../../Components/Button/Button";
import MobileUploadPost from "../NewsFeed/MobileUploadPost";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import { Images } from "../../../../Components/Images/Images";
import AppSettings from "../Settings/Setings";
import CoinBalances from "../Coins/CoinBalance";
import * as actionsUser from "../../../../actions/UserProfile";
import { CircularProgress } from "@material-ui/core";
import * as actions from "../../../../actions/UserSelectedToChat";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import * as PostActions from "../../../../actions/Posts";
import { GetAllChats, ActivateBoost, ReasonblockUser, ReportUser, getLikesOnBoost } from "../../../../controller/auth/verification";
import FriendChat from "./FriendChat";
import * as actionChat from "../../../../actions/chat";
import { Icons } from "../../../../Components/Icons/Icons";
import NoChats from "../../../../asset/WebUserCard/m_NoChats.png";
import { getCookie, setCookie } from "../../../../lib/session";
// import { ackOfMessage, subscribeToUser, unsubscribeToUser } from "../../../../lib/Mqtt/Mqtt";
import { boostActivated } from "../../../../actions/boost";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import doubleTick from "../../../../asset/svg/double-tick-indicator.svg";
import blueTick from "../../../../asset/svg/double-tick-indicator-blue.svg";
import singleTick from "../../../../asset/svg/check-symbol.svg";
import moment from "moment";
import GotLikesDrawer from "./GotLikesDrawer";
import { subscribeToUserTopic } from "../../../../lib/MqttHOC/utils";
import { withRouter } from "react-router-dom";
import Loader from "../../../../Components/Loader/Loader";

class ChatsAndNewsFeed extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      value: 0,
      openImageCropperModalForImageUpload: false,
      croppedImageObjectToUploadAfterClickingPinkBox: {},
      uploadPostDrawer: false,
      imageArray: [],
      isPostUploaded: false,
      settingsDrawer: false,
      coinDrawer: false,
      /** chat related */
      pageNo: 0,
      MatchFoundData: [],
      loadingChats: true,
      selectedIdOfUserToMessage: "",
      intervalId: "",
      countDown: "",
      boostModal: false,
      reportReasons: [],
      reportReasonModal: false,
      isBoostActive: false,
      getLikesDrawer: false,
      likeDuringBoost: [],
      open: false,
      variant: "success",
      usermessage: "",
      loader: false,
      showCoinAnimation: false,
      coinPopUpSuccessfull: false,
    };
  }

  toggleGotLikesDrawer = () => {
    this.setState({ getLikesDrawer: !this.state.getLikesDrawer });
  };

  toggleBoostModal = () => {
    this.setState({ boostModal: !this.state.boostModal });
  };

  toggleCoinDrawer = () => {
    this.setState({ coinDrawer: !this.state.coinDrawer });
  };

  toggleSettingsDrawer = () => {
    this.setState({ settingsDrawer: !this.state.settingsDrawer });
  };

  toggleReportReasonsModal = () => {
    this.setState({ reportReasonModal: !this.state.reportReasonModal });
  };

  timer = () => {
    var newCount = this.state.countDown - 1;
    if (newCount >= 0) {
      this.setState({ countDown: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  ActivateBoostFunc = () => {
    if (this.props.CoinBalance > 0) {
      ActivateBoost()
        .then((res) => {
          this.props.coinReduce(5);
          this.setState({
            open: true,
            variant: "success",
            usermessage: "Boost Activated for 5 minutes",
            isBoostActive: true,
          });
          this.props.boostActivated(res.data);
          var intervalId = setInterval(this.timer, 1000);

          this.setState({ intervalId: intervalId, countDown: 299 });
        })
        .catch((err) => {
          console.log("something went wrong", err);
        });
    } else {
      this.setState({
        open: true,
        variant: "success",
        usermessage: "Oops ! You don't have any coins to use boost",
      });
    }
  };

  componentDidMount() {
    this.setState({ loadingChats: true, loader: true });
    this.props.handlevalue(3);
    let selectedUserData = this.props.selectedProfile.selectedProfile;
    GetAllChats(this.state.pageNo)
      .then((res) => {
        this.setState({ loader: false });
        let allMessages = res.data.data;

        res.data.data.map((k) => {
          if (k.payload === "3embed test") {
            let arr = this.state.MatchFoundData.concat([k]);
            let result = arr.map(function (el) {
              var o = Object.assign({}, el);
              o.isInMatches = true;
              return o;
            });
            this.setState({ MatchFoundData: result });
          }
        });
        if (selectedUserData) {
          let foundUser = allMessages.find(
            (k) =>
              k.recipientId === selectedUserData._id || k.senderId === selectedUserData._id || k.receiverId === selectedUserData.opponentId
          );
          if (foundUser) {
            let allUsersList = [...res.data.data];
            let result = allUsersList.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            this.props.leftSideChatLogsFN(result);
          } else {
            let allUsersList = [selectedUserData, ...res.data.data];
            let result = allUsersList.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            this.props.leftSideChatLogsFN(result);
          }
          if (allMessages.length === 0) {
            let result = selectedUserData.map(function (el) {
              var o = Object.assign({}, el);
              o.checked = false;
              return o;
            });
            this.props.leftSideChatLogsFN([result]);
          }
        } else {
          let result = allMessages.map(function (el) {
            var o = Object.assign({}, el);
            o.checked = false;
            return o;
          });
          this.props.leftSideChatLogsFN(result);
        }
      })
      .catch((err) => this.setState({ loadingChats: false, loader: false }));
    ReasonblockUser().then((data) => {
      this.setState({ reportReasons: data.data.data });
    });
    if ((this.props && this.props.boostActiveDetails.expire) || (this.props && this.props.boostProfileExpire.expire)) {
      this.setState({ isBoostActive: true });
      getLikesOnBoost(getCookie("token"))
        .then((res) => {
          if (res.status === 200) {
            this.setState({ likeDuringBoost: res.data.data });
          }
        })
        .catch((err) => console.log(err));
    }

    window.addEventListener("popstate", this.onBackButtonEvent);

    // let row = document.getElementById("m_chatView_userMatches").clientWidth;
    // let x = document.getElementsByClassName("m_matched_profile");
    // let sum = 0;
    // for (let i = 0; i < 4; i++) {
    //   sum += document.getElementById(`m_matched_profile${i}`).clientWidth;
    //   // console.log(x[i].clientHeight);
    // }
    // console.log("sum", sum);
    // console.log("width", row);
  }

  onBackButtonEvent = (e) => {
    e.preventDefault();
  };

  componentWillUnmount() {
    this.props.selectedProfileForChat("");
    window.addEventListener("popstate", this.onBackButtonEvent);
    clearInterval(this.state.intervalId);
  }

  checkIfPostUploaded = (bool) => {
    this.setState({ isPostUploaded: bool });
    if (bool) {
      setTimeout(() => {
        this.setState({ isPostUploaded: false });
      }, 800);
    }
  };

  _openImageCropperModalForImageUploadModal = (data) => {
    if (data) {
      this.setState({ croppedImageObjectToUploadAfterClickingPinkBox: data }); // // this one store the image data from ref
    } else {
      this.setState({
        openImageCropperModalForImageUpload: !this.state.openImageCropperModalForImageUpload,
      });
    }

    if (data) {
      setTimeout(() => {
        this.toggleUploadPostDrawer();
      }, 500);
    }
  };

  addNewPostPreviewForCustomModal = (blobUrl, blobObj) => {
    let arr = [...this.state.imageArray];
    if (arr.length === 0) {
      arr.push({
        fileDetails: blobObj,
        blobUrl: blobUrl,
      });
    } else {
      arr[0] = {
        fileDetails: blobObj,
        blobUrl: blobUrl,
      };
    }
    this.setState({
      imageArray: arr,
    });
  };

  closeImageCropModal = () => {
    this.setState({
      openImageCropperModalForImageUpload: !this.state.openImageCropperModalForImageUpload,
    });
  };

  setValue = (value) => {
    this.setState({ value });
  };

  toggleUploadPostDrawer = () => {
    this.setState({ uploadPostDrawer: !this.state.uploadPostDrawer });
  };

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  /***************************************************** chat related ***********************************************/

  GetUserInfoAndNavigateToChatScreen = (obj) => {
    let id = obj._id || obj.recipientId || obj.receiverId || obj.opponentId || obj.receiverIdentifier;
    // console.log("GetUserInfoAndNavigateToChatScreen", obj);
    this.setState({ selectedIdOfUserToMessage: id });
    subscribeToUserTopic(id);
    this.props.USERSELECTEDTOCHAT(obj);
    setCookie("userselectedtochat", id);
    this.props.history.push("/app/friendChat");
  };

  renderByStatus = (status) => {
    if (status === 1 || status === "1") {
      return <img src={singleTick} alt="single-tick" width={18} />;
    } else if (status === 2 || status === "2") {
      return <img src={doubleTick} alt="double-tick" width={18} />;
    } else if (status === 3 || status === "3") {
      return <img src={blueTick} alt="double-blue-tick" width={18} />;
    }
  };

  acknowledgementAllToBlueTick = (appendedChatData) => {
    let arr = appendedChatData.filter((k) => k.status !== 3 && k.senderId !== getCookie("uid"));
    let ackArr = [...arr];

    for (let i = 0; i < ackArr.length; i++) {
      ackArr[i].status = 3;
    }
    this.props.allMessagesForSelectedUser(appendedChatData);
  };

  messageFilterRenderer = (obj) => {
    if (obj.messageType === "3" || obj.type === "3") {
      return "Sent Location";
    } else if (obj.messageType === "2" || obj.type === "2") {
      return "Sent Video";
    } else if (obj.messageType === "1" || obj.type === "1") {
      return "Sent Photo";
    } else if (obj.messageType === "0" || obj.type === "0") {
      if (obj.payload === "3embed test") {
        return "";
      } else {
        let msg = atob(obj.payload);
        if (msg.includes("https://fetch")) {
          return "Sent Photo";
        }
        return atob(obj.payload);
      }
    } else if (obj.messageType === "8" || obj.type === "8") {
      return "Sent GIF";
    } else if (obj.messageType === "15" || obj.type === "15") {
      return "Coins Transfer";
    }
  };

  postReportReason = (reason) => {
    let UserReportPayload = {
      targetUserId: this.props.UserSelectedToChat.recipientId,
      reason: reason,
      message: "Report",
    };
    ReportUser(UserReportPayload)
      .then((data) => {
        this.setState({
          true: false,
          usermessage: "Report Submitted Successfully",
        });
        this.toggleReportReasonsModal();
        setTimeout(() => {
          this.setState({ open: false });
        }, 2500);
      })
      .catch((error) => {
        console.log("report error");
        this.toggleReportReasonsModal();
      });
  };

  ifOtherDeviceOrWebkitDevice = () => {
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
      return (
        <div className="m_nf_createPost_ios">
          <Button
            handler={this.postNewStory}
            text={
              <div>
                <label className="mb-0">
                  <input
                    type="file"
                    ref={this.myRef}
                    onChange={() => this._openImageCropperModalForImageUploadModal(this.myRef)}
                    id="my_file"
                    accept="image/png, image/jpeg, video/mp4"
                    hidden
                  />
                  Create Post
                </label>
              </div>
            }
          />
        </div>
      );
    } else {
      return (
        <div className="m_nf_createPost">
          <Button
            handler={this.postNewStory}
            text={
              <div>
                <label className="mb-0">
                  <input
                    type="file"
                    ref={this.myRef}
                    onChange={() => this._openImageCropperModalForImageUploadModal(this.myRef)}
                    id="my_file"
                    accept="image/png, image/jpeg, video/mp4"
                    hidden
                  />
                  Create Post
                </label>
              </div>
            }
          />
        </div>
      );
    }
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  render() {
    let chatView = (
      <div className="col-12 m_chatView_screen px-0">
        <div className="row mx-0">
          <div className="col-12 py-2 m_chatView_header">Matches</div>
        </div>
        <div className="row mx-0 pb-2 border-bottom">
          <div className="col-12 m_chatView_userMatches">
            <div className="row mx-0" id="m_chatView_userMatches">
              <div onClick={this.state.isBoostActive ? this.toggleGotLikesDrawer : this.toggleBoostModal}>
                <img src={Icons.BigHeart2} alt="boostLikes" height={60} width={55} />
                <div className="m_chat_matched">Likes</div>
              </div>
              {this.state.MatchFoundData.length > 0 ? (
                this.state.MatchFoundData.map((k, i) => (
                  <div
                    id={`m_matched_profile${i}`}
                    className="m_matched_profile pl-2"
                    onClick={() => this.GetUserInfoAndNavigateToChatScreen(k)}
                  >
                    <img src={k.profilePic} alt={k.firstName || k.name} height={60} width={60} />
                    {k.isFriend ? (
                      <div className="m_common_friends">
                        <img src={Icons.twoFriends} alt="two-users" />
                      </div>
                    ) : (
                      <></>
                    )}
                    <div className="m_common_matched">
                      {k.isMatchedUser === 1 || k.isMatched === 1 ? (
                        <img src={Icons.friendsChat} alt="friends-chat" className="d_matched_heart" />
                      ) : (
                        <span />
                      )}
                    </div>
                    <div>{k.firstName}</div>
                  </div>
                ))
              ) : (
                <span />
              )}
            </div>
          </div>
        </div>
        <div className="row mx-0">
          <div className="col-12 py-2 m_chatView_header">Active Chats</div>
        </div>
        <div className="col-12 px-0 m_chatScrollView">
          <div className="row mx-0">
            {this.props.leftSideChatLogs && this.props.leftSideChatLogs.length > 0 ? (
              this.props.leftSideChatLogs.map((k, i) => (
                <div className="m_single_chat" key={i} onClick={() => this.GetUserInfoAndNavigateToChatScreen(k)}>
                  <div className={k.onlineStatus === 1 ? "m_onlineDot" : ""}></div>
                  <div className="m_matchedUserIcon">
                    {k.isMatchedUser === 1 ? (
                      <img height={16} width={16} src={Icons.friendsChat} alt="friends-chat" className="m_matched_heart" />
                    ) : (
                      <span />
                    )}
                  </div>
                  {k.isFriend ? (
                    <div className="m_friendUserIcon">
                      <img src={Icons.twoFriends} height={10} width={10} alt="two-users" />
                    </div>
                  ) : (
                    <span />
                  )}
                  {(k.isMatchedUser === 0 && k.isFriend === 0) || (k.isMatched === 0 && k.isFriend === 0) ? (
                    <img src={Icons.goldCoin} height={28} width={28} alt="gold-coin" className="m_notMatched_coin" />
                  ) : (
                    <span />
                  )}
                  <div className="m_single_chat_image">
                    <img src={k.profilePic || k.userImage} onError={this.onError} alt={k.firstName} />
                  </div>
                  <div className="m_single_chat_content">
                    <div>{k.firstName || k.name}</div>
                    <div>
                      <span>{this.renderByStatus(k.status)}</span>
                      {this.messageFilterRenderer(k)}
                    </div>
                  </div>
                  <div className="m_single_chat_time">
                    {k.totalUnread > 0 ? <span>{k.totalUnread}</span> : <span />}
                    <div>{moment(parseInt(k.timestamp)).fromNow()}</div>
                    <div>{moment(parseInt(k.timestamp)).format("LT")}</div>
                  </div>
                </div>
              ))
            ) : (
              <div className="text-center m_chatView_NoMessages pt-5">
                <img src={NoChats} alt="no chat" />
                <div>No active Chat found, tap on match list to start a chat.</div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
    return (
      <div className="col-12 chatAndNewsFeed">
        <MainDrawer
          width={400}
          onClose={this.toggleUploadPostDrawer}
          onOpen={this.toggleUploadPostDrawer}
          open={this.state.uploadPostDrawer}
        >
          <MobileUploadPost
            closeImageCropModal={() => {
              this.closeImageCropModal();
              this.toggleUploadPostDrawer();
            }}
            checkIfPostUploaded={this.checkIfPostUploaded}
            imageArray={this.state.imageArray}
            UserFinalData={this.props.UserFinalData}
            addNewPostPreviewForCustomModal={this.addNewPostPreviewForCustomModal}
            croppedImageObjectToUploadAfterClickingPinkBox={this.state.croppedImageObjectToUploadAfterClickingPinkBox}
          />
        </MainDrawer>
        <MainDrawer onClose={this.toggleSettingsDrawer} onOpen={this.toggleSettingsDrawer} open={this.state.settingsDrawer}>
          <AppSettings onClose={this.toggleSettingsDrawer} />
        </MainDrawer>
        <MainDrawer onClose={this.toggleGotLikesDrawer} onOpen={this.toggleGotLikesDrawer} open={this.state.getLikesDrawer}>
          <GotLikesDrawer onClose={this.toggleGotLikesDrawer} likeDuringBoost={this.state.likeDuringBoost} />
        </MainDrawer>
        <MainDrawer onClose={this.toggleCoinDrawer} onOpen={this.toggleCoinDrawer} open={this.state.coinDrawer}>
          <CoinBalances onClose={this.toggleCoinDrawer} UserFinalData={this.props.UserFinalData} />
        </MainDrawer>
        <div className="row">
          <div className="col-12 py-3 m_chatNewsHeader">
            <div className="row">
              <div className="col-4"></div>
              <div className="col-4 friends_header">Matches</div>
              <div className="col-4">
                <div className="row header_row">
                  <div className="global_coinbalance col-8" onClick={this.toggleCoinDrawer}>
                    <img src={Images.dollarIcon} className="img-fluid" alt="dollarimg" title="dollarimg" />
                    <p className="m-0">{this.renderCoins(this.props.CoinBalance)}</p>
                  </div>
                  <div className="col-4" onClick={this.toggleSettingsDrawer}>
                    <img
                      style={{ position: "absolute", bottom: "-2px" }}
                      src={this.props && this.props.userProfilePicture}
                      alt={this.props && this.props.UserFinalData && this.props.UserFinalData.firstName}
                      height={30}
                      width={30}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="chatsTabs w-100">
            <SimpleTabs
              isMobile={true}
              mobile={true}
              setValue={this.setValue}
              tabs={[{ label: "Chats" }, { label: "NewsFeed" }]}
              tabContent={[
                {
                  content: this.state.loader ? (
                    <div className="m_chat_Loader">
                      <Loader text={"Loading..."} />
                    </div>
                  ) : (
                    chatView
                  ),
                },
                {
                  content: <MobileNewsFeed isPostUploaded={this.state.isPostUploaded} UserFinalData={this.props.UserFinalData} />,
                },
              ]}
            />
          </div>
          {this.state.value === 1 ? this.ifOtherDeviceOrWebkitDevice() : ""}
        </div>
        <MaterialModal toggle={this.toggleReportReasonsModal} isOpen={this.state.reportReasonModal} width={"90%"}>
          <div className="col-12 m_reportReasonsModal">
            <div>Report User</div>
            {this.state.reportReasons.map((k) => (
              <div key={k} onClick={() => this.postReportReason(k)}>
                {k}
              </div>
            ))}
            <div onClick={this.toggleReportReasonsModal}>Cancel</div>
          </div>
        </MaterialModal>
        <MaterialModal toggle={this.toggleBoostModal} isOpen={this.state.boostModal} width={"90%"}>
          <div className="col-12 m_boostModal">
            <div onClick={this.toggleBoostModal}>
              <img src={Icons.PinkCloseBtn} alt="close-btn" height={18} width={18} />
            </div>
            <div>
              <img src={Icons.SpendCoins} alt="Credits" height={120} width={100} />
            </div>
            <div>Spend 5 coins to boost your profile.</div>
            <Button
              handler={() => {
                this.ActivateBoostFunc();
                this.toggleBoostModal();
              }}
              text="I am Ok to spend 5 Credits"
            />
          </div>
        </MaterialModal>
        {/* <MaterialModal isOpen={this.props.coinDialogShow} toggle={this.props.coinToastDismiss} width={"90%"}>
          <div className="col-12 p-3 text-center m_spendCoinsOnChatModal" style={{ position: "relative" }}>
            <div onClick={this.props.coinToastDismiss}>
              <img src={Icons.PinkCloseBtn} height={15} width={15} alt="close-btn" />
            </div>
            <div className="col-12">
              <img src={Icons.SpendCoins} height={120} width={100} alt="gold-coins" />
            </div>
            <div className="text-center">Give away coins to connect.</div>
            <div className="text-center">Get a chance to connect without being matched</div>
            <Button
              text={`I am OK to spend ${
                this.props.CoinConfig && this.props.CoinConfig.perMsgWithoutMatch && this.props.CoinConfig.perMsgWithoutMatch.Coin
              } Coins`}
              handler={() => {
                this.setState({ coinPopUpSuccessfull: true });
                // subscribeToUser(this.state.selectedIdOfUserToMessage);
                this.props.coinToastDismiss();
                this.props.chatToggleDrawer(true);
                this.setState({ coinPopUpSuccessfull: false });
              }}
            />
          </div>
        </MaterialModal> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedProfile: state.selectedProfile.selectedProfile,
    SingleUserAllMessages: state.UserSelectedToChat.SingleUserAllMessages,
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    UserSelectedToChat: state.UserSelectedToChat.UserSelectedToChat,
    getLatestTimeStamp: state.UserSelectedToChat.getLatestTimeStamp,
    CoinBalance: state.UserProfile.CoinBalance,
    CoinConfig: state.CoinConfig.CoinConfig,
    serverImageFolderLocation: state.UserSelectedToChat.serverImageFolderLocation,
    boostProfileExpire: state.Boost.BoostProfile,
    boostActiveDetails: state.Boost.BoostDetailsOnRefresh,
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    userProfilePicture: state.UserProfile && state.UserProfile.UserProfile && state.UserProfile.UserProfile.UserProfilePic,
    coinDialogShow: state.UserSelectedToChat.showCoinToast,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    coinToastShow: () => dispatch(actionChat.showUpCoinChatToast()),
    coinToastDismiss: () => dispatch(actionChat.dismissCoinChatToast()),
    boostActivated: (data) => dispatch(boostActivated(data)),
    leftSideChatLogsFN: (allChatLogs) => dispatch(actionsUser.leftSideChatLogs(allChatLogs)),
    allMessagesForSelectedUser: (msgs) => dispatch(actions.allMessagesForSelectedUser(msgs)),
    chatSessionStarted: () => dispatch(actions.chatSessionStarted()),
    UserSelectedToChatFn: (user) => dispatch(actions.userSelectedToChat(user)),
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    addNewChatToExistingObject: (obj) => dispatch(actionChat.addNewChatToExistingObject(obj)),
    imageUploadFn: (img) => dispatch(actions.uploadImage(img)),
    updateUnreadChatCountFunc: (count) => dispatch(actionChat.storeUnreadChatCount(count)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
    m_updatePostStatus: (text, bool) => dispatch(PostActions.uploadingPostStatus(text, bool)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChatsAndNewsFeed));
