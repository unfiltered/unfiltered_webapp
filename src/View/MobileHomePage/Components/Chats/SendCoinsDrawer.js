import React from "react";
import "./Chats.scss";
import { Icons } from "../../../../Components/Icons/Icons";
import Input from "../../../../Components/Input/Input";
import { sendCoins } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";

class SendCoinsDrawer extends React.Component {
  state = {
    coinsText: "",
    note: "",
  };
  setText = (e) => {
    this.setState({ coinsText: e.target.value });
  };

  //   amount: 1
  // chatId: "5f991010e8caf9034dc27d53"
  // from: "5f1967f7000b5f39fbc8de47"
  // id: "1604510874242"
  // messageId: "1604510888376"
  // name: "Ninad"
  // payload: "MTA=↵"
  // reason: "MTA=↵"
  // receiverIdentifier: "5f3fd1fede39d81809110906"
  // receiverImage: "https://res.cloudinary.com/niko94/image/upload/v1602933336/blob_qvuw8v.jpg"
  // receiverName: "Ninad"
  // targetUserId: "5f3fd1fede39d81809110906"
  // timestamp: 1604510888376
  // to: "5f3fd1fede39d81809110906"
  // toDocId: "92e0c1be-b88b-497d-95d9-2c868cf66711"
  // type: 15
  // userImage: "https://res.cloudinary.com/niko94/image/upload/v1602933336/

  sendCoinsFn = () => {
    sendCoins(getCookie("token"), {
      reason: btoa(this.state.note),
      payload: btoa(this.state.note),
      amount: parseInt(this.state.coinsText),
      chatId: this.props.userInfo.chatId ? this.props.userInfo.chatId : "",
      receiverName: this.props.userInfo.firstName,
      toDocId: "xxx",
      receiverImage: this.props.userInfo.profilePic || this.props.userInfo.userImage,
      targetUserId: this.props.userInfo.recipientId || this.props.userInfo._id,
      id: new Date().getTime().toString(),
      type: "15",
      from: getCookie("uid"),
      to: this.props.userInfo.recipientId || this.props.userInfo._id,
      name: this.props.sendersName,
      userImage: this.props.myPic,
      receiverIdentifier: this.props.userInfo.recipientId || this.props.userInfo._id,
      timestamp: new Date().getTime(),
      messageId: new Date().getTime().toString(),
    })
      .then((res) => {
        this.props && this.props.coinReduce(parseInt(this.state.coinsText));
        this.props.onClose();
        console.log("res", res);
      })
      .catch((err) => console.log("res", err));
  };

  setNode = (e) => {
    this.setState({ note: e.target.value });
  };
  render() {
    return (
      <div className="col-12 m_sendCoinsView">
        <div className="row py-3 border-bottom">
          <div className="col-3" onClick={this.props.onClose}>
            <img src={Icons.PinkBack} alt="back" height={20} width={25} />
          </div>
          <div className="col-6 m_refferal_header">Gift Credits</div>
          <div className="col-3"></div>
        </div>
        <div className="row m_sendCoins_info">
          <div className="col-12">
            <img src={this.props && this.props.userInfo.profilePic} alt={this.props && this.props.userInfo.firstName} />
          </div>
          <div className="col-12">{this.props && this.props.userInfo.firstName}</div>
          <div className="col-12">Please enter the number of Credits you want to gift {this.props && this.props.userInfo.firstName}</div>
          <div className="col-12">
            <div className="row">
              <div className="col-4"></div>
              <div className="col-4">
                <Input placeholder="0 Coins" onChange={this.setText} />
              </div>
              <div className="col-4"></div>
            </div>
          </div>
          <div className="col-12">
            <div className="row">
              <div className="col-1"></div>
              <div className="col-10">
                <Input placeholder="Add Note" onChange={this.setNode} />
              </div>
              <div className="col-1"></div>
            </div>
          </div>
        </div>
        <div className="Mprefer">
          <i onClick={this.sendCoinsFn} className="fas fa-check" />
        </div>
      </div>
    );
  }
}

export default SendCoinsDrawer;
