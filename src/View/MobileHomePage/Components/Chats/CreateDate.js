import React from "react";
import "../Friends/Friends.scss";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import CustomButton from "../../../../Components/Button/Button";
import moment from "moment";
import TextField from "@material-ui/core/TextField";
import { getCookie } from "../../../../lib/session";
import * as actionChat from "../../../../actions/chat";
import { connect } from "react-redux";
import MenuItem from "@material-ui/core/MenuItem";
import { keys } from "../../../../lib/keys";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { createDate } from "../../../../controller/auth/verification";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import * as PostActions from "../../../../actions/Posts";
import "./createDate.scss";

var foursquare = require("react-foursquare")({
  clientID: keys.foursquareClientID,
  clientSecret: keys.foursquareClientSecret,
});

class CreateDateScreen extends React.Component {
  state = {
    modal: false,
    date: "",
    time: "",
    open: false,
    usermessage: "",
    variant: "warning",
    locationSelected: "",
    items: [],
    savedParams: "",
  };
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };

  getCurrentTime = () => {
    let time = `${moment(new Date().getTime()).format("HH")}:${moment(new Date().getTime()).format("mm")}`.toString();
    this.setState({ time: time });
    return time;
  };

  getCurrentDate = () => {
    let d = moment(new Date().getTime()).format("DD");
    let m = moment(new Date().getTime()).format("MM");
    let y = moment(new Date().getTime()).format("YYYY");
    let date = `${y}-${m}-${d}`.toString();
    this.setState({ date: date });
    return date;
  };

  componentDidMount() {
    let params;
    if (getCookie("selectedLocation") != null) {
      let obj = JSON.parse(getCookie("selectedLocation"));
      console.log("111111");
      params = {
        ll: `${obj.lat}, ${obj.lng}`,
        query: "",
      };
    } else if (getCookie("lat") != null && getCookie("lng") != null) {
      console.log("222222");
      params = {
        ll: `${getCookie("lat")}, ${getCookie("lng")}`,
        query: "",
      };
    } else {
      console.log("333333");
      if (getCookie("baseAddress") != null) {
        params = {
          ll: `${getCookie("baseAddress").lat}, ${getCookie("baseAddress").lng}`,
          query: "",
        };
      }
    }
    this.setState({ time: this.getCurrentTime(), date: this.getCurrentDate() });

    let str = params.ll;
    var myarray = str.split(",");
    for (var i = 0; i < myarray.length; i++) {
      this.setState({ savedParams: { lat: myarray[0].trim(), lng: myarray[1].trim() } });
    }

    if (this.props._createDateType === 2) {
      foursquare.venues.getVenues(params).then((res) => {
        this.setState({
          items: res && res.response && res.response.venues,
          locationSelected: res && res.response && res.response.venues && res.response.venues[0].name,
        });
      });
    }
  }

  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  renderCoinCost = (type) => {
    let { physicalDate, audioDate, videoDate } = this.props.CoinConfig;
    if (type === 1) {
      return `Confirm Call Date for ${physicalDate.Coin} Credits`;
    } else if (type === 2) {
      return `Confirm Physical Date for ${videoDate.Coin} Credits`;
    } else {
      return `Confirm Audio Call Date for ${audioDate.Coin} Credits`;
    }
  };

  renderRequestedFor = (type) => {
    if (type === 1) {
      return "Video Call Date";
    } else if (type === 2) {
      return "Physical Date";
    } else {
      return "Audio Call Date";
    }
  };

  createDateFn = (targetUserId, time, dateType, lat, lng, place) => {
    let { physicalDate, audioDate, videoDate } = this.props.CoinConfig;
    let PLAN =
      this.props.checkIfUserIsProUser &&
      this.props.checkIfUserIsProUser.ProUserDetails &&
      this.props.checkIfUserIsProUser.ProUserDetails.tag;
    if (this.props.CoinBalance > 4) {
      let proposedOn = `${this.state.date}T${this.state.time}`;
      if (dateType === 3 || dateType === 1) {
        createDate(getCookie("token"), targetUserId, new Date(proposedOn).getTime(), dateType, lat, lng, place, PLAN)
          .then((res) => {
            if (res.status === 200) {
              this.props.m_updatePostStatus("Creating new date...", true);
              if (dateType === 1) {
                this.props.coinReduce(parseInt(videoDate.Coin));
                setTimeout(() => {
                  this.props.m_updatePostStatus("", false);
                  this.props.onClose();
                }, 2000);
              } else if (dateType === 3) {
                this.props.coinReduce(parseInt(audioDate.Coin));
                setTimeout(() => {
                  this.props.m_updatePostStatus("", false);
                  this.props.onClose();
                }, 2000);
              }
              this.setState({
                usermessage: "Date created successffully.",
                open: true,
                variant: "success",
              });
            } else if (res.status === 422) {
              this.props.m_updatePostStatus("", false);
              this.setState({
                usermessage: "You already have a date active with this user.",
                open: true,
                variant: "error",
              });
            } else {
              this.props.m_updatePostStatus("", false);
              this.setState({
                usermessage: "Something went wrong while creating date.",
                open: true,
                variant: "error",
              });
            }
          })
          .catch((err) => this.props.m_updatePostStatus("", false));
      } else if (dateType === 2) {
        if (place === "" || place === undefined) {
          this.setState({
            usermessage: "Please select place.",
            open: true,
            variant: "warning",
          });
        } else {
          createDate(getCookie("token"), targetUserId, time, dateType, lat, lng, place)
            .then((res) => {
              if (res.status === 200) {
                this.props.m_updatePostStatus("Creating new date...", true);
                this.props.coinReduce(parseInt(physicalDate.Coin));
                setTimeout(() => {
                  this.props.m_updatePostStatus("", false);
                  this.props.onClose();
                }, 2000);
                this.setState({
                  usermessage: "Date created successffully.",
                  open: true,
                  variant: "success",
                });
              } else if (res.status === 422) {
                this.props.m_updatePostStatus("", false);
                this.setState({
                  usermessage: "You already have a date active with this user.",
                  open: true,
                  variant: "error",
                });
              } else {
                this.props.m_updatePostStatus("", false);
                this.setState({
                  usermessage: "Something went wrong while creating date.",
                  open: true,
                  variant: "error",
                });
              }
            })
            .catch((err) => this.props.m_updatePostStatus("", false));
        }
      }
    } else {
      this.setState({
        usermessage: "You don't have enough Credits to create date.",
        open: true,
        variant: "warning",
      });
    }
  };

  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    console.log("CLG time", this.state.time);
    console.log("CLG date", this.state.date);
    return (
      <div className="col-12 m-createDate-mainView">
        <div className="row">
          <section className="col-12 py-3 m-createDate-header">
            <div className="row">
              <div className="col-10 label-common2">Plan {this.renderRequestedFor(this.props._createDateType)}</div>
              <div className="col-2 friends_header" onClick={this.props.onClose}>
                <img src={Icons.PinkCloseBtn} alt="close" height={20} width={20} />
              </div>
            </div>
          </section>

          <MaterialModal toggle={this.toggleModal} isOpen={this.state.modal} width={"90%"}>
            <div className="col-12 m_boostModal">
              <div onClick={this.toggleModal}>
                <img src={Icons.PinkCloseBtn} alt="close-btn" height={18} width={18} />
              </div>
              <div>
                <img src={Icons.SpendCoins} alt="Credits" height={120} width={100} />
              </div>
              <div>Are you sure, you want to confirm the date ?</div>
              <CustomButton
                handler={() => {
                  this.toggleModal();
                  this.createDateFn(
                    this.props && this.props.opponentProfileInfo && this.props.opponentProfileInfo.recipientId,
                    new Date(`${this.state.date} ${this.state.time}`.toString()).getTime(),
                    this.props._createDateType,
                    this.state.savedParams.lat,
                    this.state.savedParams.lng,
                    this.state.locationSelected
                  );
                }}
                text={this.renderCoinCost(this.props._createDateType)}
              />
            </div>
          </MaterialModal>
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
            type={this.state.variant}
            message={this.state.usermessage}
            open={this.state.open}
            timeout={2500}
            onClose={this.handleClosesnakbar}
          />
          <section className="m-createDate-ScrollView">
            <div className="col-12 py-3 px-5 text-center rescheduleDateText">
              Suggest a new place and time to meet. <br />
              We'll notify you if {this.props.opponentProfileInfo && this.props.opponentProfileInfo.firstName} confirms.
            </div>
            <div className="col-12 d-flex py-3 d_physical_couples_img rescheduleDatehearts">
              <div>
                <img
                  src={this.props.opponentProfileInfo && this.props.opponentProfileInfo.profilePic}
                  className="d_physical_date_img"
                  alt={this.props.opponentProfileInfo && this.props.opponentProfileInfo.profilePic}
                />
              </div>
              <div>
                <img
                  src={this.props && this.props.UserFinalData && this.props.UserFinalData.profilePic}
                  className="d_physical_date_img"
                  alt={this.props.UserFinalData.profilePic}
                />
              </div>
              <span className="heart1">
                <img src={Icons.Heart1} alt="heart" />
              </span>
              <span className="heart2">
                <img src={Icons.Heart2} alt="heart" />
              </span>
              <span className="heart3">
                <img src={Icons.Heart3} alt="heart" />
              </span>
              <span className="heart4">
                <img src={Icons.Heart4} alt="heart" />
              </span>
            </div>
            {this.props._createDateType === 2 ? (
              <div className="col-10 pt-5">
                <FormControl className="m_create_date_formControl">
                  <Select
                    className="m_create_date_selectedLocation"
                    value={this.state.locationSelected && this.state.locationSelected}
                    onChange={this.handleChange("locationSelected")}
                  >
                    {this.state.items &&
                      this.state.items.map((k, i) => (
                        <MenuItem key={k.name} value={k.name} className="m_create_date_single_location">
                          {k.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </div>
            ) : (
              <span />
            )}
            <div className={this.props._createDateType === 2 ? "col-10 pt-3" : "col-10 pt-5"}>
              <TextField
                id="date"
                label="Date"
                type="date"
                onChange={this.handleChange("date")}
                value={this.state && this.state.date}
                placeholder={this.state && this.state.date}
                defaultValue={this.state && this.state.date}
                className="datesTextField"
              />
            </div>
            <div className="col-10 pt-3">
              <TextField
                id="time"
                label="Time"
                type="time"
                onChange={this.handleChange("time")}
                value={this.state && this.state.time}
                placeholder={this.state && this.state.time}
                defaultValue={this.state && this.state.time}
                className="datesTextField"
              />
            </div>
            <CustomButton
              text={<img src={Icons.DateConfirmBtn} alt="confirm-btn" width={75} height={75} />}
              className="confirmDateBtn"
              handler={this.toggleModal}
            />
          </section>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    CoinConfig: state.CoinConfig.CoinConfig,
    checkIfUserIsProUser: state.ProUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    m_updatePostStatus: (text, bool) => dispatch(PostActions.uploadingPostStatus(text, bool)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateDateScreen);
