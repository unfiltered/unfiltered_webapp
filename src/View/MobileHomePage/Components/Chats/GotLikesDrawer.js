import React from "react";
import "./Chats.scss";
import { Media } from "reactstrap";
import { Icons } from "../../../../Components/Icons/Icons";

class GotLikesDrawer extends React.Component {
  render() {
    return (
      <div className="col-12 m_gotLikesView py-2">
        <div className="row py-3">
          <div className="col-10">Likes</div>
          <div className="col-2">
            <img src={Icons.PinkCloseBtn} alt="goBack" onClick={this.props.onClose} height={17} width={17} />
          </div>
        </div>
        <div className="col-12">
          <div className="row">
            {this.props && this.props.likeDuringBoost.length > 0 ? (
              this.props.likeDuringBoost.map((k, i) => (
                <Media className="py-2 col-12 Mcmn_prospect h-100 d-flex align-items-center flex-row px-0" key={i}>
                  <Media left className="u_img">
                    <Media object src={k.profilePic} alt={k.firstName} title={k.firstName} className="img-fluid" />
                  </Media>
                  <Media body className="col-12 Mcardmain">
                    <Media heading>{k.firstName}</Media>
                  </Media>
                </Media>
              ))
            ) : (
              <div className="col-12">
                <div className="nomorelike text-center">
                  <img className="img-fluid" width={130} height={130} src={Icons.likesMe} alt="nolike" title="nolike" />
                  <div className="m_prospects_label">Likes Me</div>
                  <div className="m_prospects_subText">Sorry no user has liked your profile yet. Keep your profile fully updated to improve your chances to get liked.</div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default GotLikesDrawer;
