import React, { useState, useEffect } from "react";
import "../../../WebHomePage/Components/Chat/Chat.scss";
import { getGifs, getRandomGifs } from "../../../../controller/auth/verification";

function GifPage({ toggleGifDrawer, sendGifHandler, isGif }) {
  const [randomGifs, setRandomGifs] = useState("");

  const [searchTerm, setSearchTerm] = useState("");
  useEffect(() => {
    getRandomGifs().then((res) => {
      setRandomGifs(res.data.data);
    });
  }, []);

  const getStickers = (searchTerm) => {
    getGifs(searchTerm)
      .then((res) => {
        setRandomGifs(res.data.data);
      })
      .catch((err) => {});
  };
  const onChangeHandler = (e) => {
    setSearchTerm(e.target.value);
  };
  const getDetailsAndSendGif = (gif, bool) => {
    const img = bool ? gif.images.downsized_large.url : gif.images.downsized_still.url;
    sendGifHandler(img, bool);
    toggleGifDrawer();
  };
  return (
    <div>
      <div className="col-12">
        <div className="py-2 row">
          <div className="pt-2 col-2 MCommonwidth">
            <i className="fa fa-arrow-left pt-1 pl-1 MCommonbackbutton" onClick={toggleGifDrawer} />
          </div>
          <div className="col-8 d-flex justify-content-center align-items-center px-0">{isGif ? "Gifs" : "Stickers"}</div>
          <div className="col-2"></div>
        </div>
      </div>
      <div className="col-12 py-3 border-bottom border-top">
        <div className="row">
          <div className="gifSearchBar">
            <input type="text" onChange={onChangeHandler} placeholder="Search Sticker" />
          </div>
          <div className="gifSeachButton" onClick={() => getStickers(searchTerm)}>
            Search
          </div>
        </div>
      </div>
      <div className="col-12 m_gif_scroll_screen">
        <div className="row">
          {randomGifs &&
            randomGifs.map((k, i) => (
              <img
                key={i}
                src={isGif ? k.images.fixed_width_small.url : k.images.downsized_still.url}
                width={80}
                height={80}
                className="selectedImage"
                onClick={() => getDetailsAndSendGif(k, isGif)}
              />
            ))}
        </div>
      </div>
    </div>
  );
}

export default GifPage;
