import React from "react";
import "./Chats.scss";
import MatUiModal from "../../../../Components/Model/MAT_UI_Modal";
import { Images } from "../../../../Components/Images/Images";
import { FormattedMessage } from "react-intl";
import SendIcon from "../../../../asset/mobile_img/send.svg";
import Attachment from "../../../../asset/mobile_img/paperclip.svg";
import ThreeDots from "../../../../asset/mobile_img/ellipsis.svg";
import { Media } from "reactstrap";
import { withStyles } from "@material-ui/core/styles";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import TextField from "@material-ui/core/TextField";
import Messages from "../../../WebHomePage/Components/Chat/Message";
import { getCookie } from "../../../../lib/session";
import Button from "@material-ui/core/Button";
import CustomButton from "../../../../Components/Button/Button";
import Popover from "@material-ui/core/Popover";
import toRenderProps from "recompose/toRenderProps";
import withState from "recompose/withState";
import Pin from "../../../../asset/svg/pin.svg";
import Video from "../../../../asset/svg/cinema.svg";
import Gallery from "../../../../asset/svg/picture.svg";
import { demoThumbNail } from "../../../WebHomePage/Components/Chat/DemoThumbnail";
import GifPage from "./GIF";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import _MAT_UI_Modal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import { GoogleApiWrapper, Map } from "google-maps-react";
import { geolocated } from "react-geolocated";
import { keys } from "../../../../lib/keys";
import { withRouter } from "react-router-dom";
import LocationSearchInput from "./LocationSearchInput";
import SendCoinsDrawer from "./SendCoinsDrawer";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { connect } from "react-redux";
import {
  BlockUser,
  CoinChat,
  GetAllChatsForSingleUser,
  ReasonblockUser,
  ReportUser,
  unblockuser,
  UnfriendUser,
  unmatchuser,
  deletechatTotally,
  GetAllChats,
  OtherUserProfile,
  getCoinConfig,
  uploadMediaOverLocalServer,
} from "../../../../controller/auth/verification";
import * as coinCfg from "../../../../actions/coinConfig";
import * as actionsUser from "../../../../actions/UserProfile";
import * as actionChat from "../../../../actions/chat";
import * as actions from "../../../../actions/UserSelectedToChat";
// import { ackOfMessage } from "../../../../lib/Mqtt/Mqtt";
import CreateDateScreen from "./CreateDate";
import { CircularProgress } from "@material-ui/core";
import { __callInit } from "../../../../actions/webrtc";
import { sendMessageOverMqtt, unSbscribeToUserTopic, sendAcknowledgementsOverMqtt } from "../../../../lib/MqttHOC/utils";
import * as PostActions from "../../../../actions/Posts";
import ReactDOM from "react-dom";
import OtherUserProfileFromChatView from "../otherUserMobileProfile/otherUserProfileFromChatView";
import Loader from "../../../../Components/Loader/Loader";

const styles = (theme) => ({
  underline: {
    "&&&:before": {
      borderBottom: "none",
    },
    "&&:after": {
      borderBottom: "none",
    },
  },
  textField: {
    width: "inherit",
    padding: "5px",
    border: "1px solid grey",
    borderRadius: "12px",
  },
  attachmentButton: {
    padding: "0",
    maxWidth: "0",
    minWidth: "30px",
    background: "#161616",
    boxShadow: "none",
  },
  openAttachment: {
    width: "95%",
  },
  options: {
    display: "flex",
    flexDirection: "column",
    paddingTop: "15px",
    paddingBottom: "15px",
    background: "#161616",
  },
  eachOptions: {
    fontFamily: "Circular Air Book",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    paddingLeft: "15px",
    paddingRight: "15px",
    fontSize: "14px",
    color: "#808080",
    background: "#161616",
  },
  optionSub: {
    display: "flex",
  },
  otherOptionsOptions: {
    padding: "7px 0px",
    fontFamily: "Circular Air Book",
    display: "flex",
    background: "#161616",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    paddingLeft: "15px",
    paddingRight: "15px",
    fontSize: "14px",
    color: "#2E72D5",
    borderBottom: "1px solid #f3f3f3",
  },
  onClick: {
    backgroundColor: "#161616",
    "&:hover": {
      backgroundColor: "#161616",
    },
  },
  root: {
    width: "95%",
  },
  optionsRoot: {
    width: "220px",
  },
  otherOptions: {
    width: "180px",
  },
  optionsSub: {
    display: "flex",
    flexDirection: "column",
  },
});

const WithState = toRenderProps(withState("anchorEl", "updateAnchorEl", null));
const OptionsState = toRenderProps(withState("Op_anchorEl", "Op_updateAnchorEl", null));

class FriendChat extends React.Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.state = {
      multiline: "",
      noCoinModal: false,
      sendCoinsDrawer: false,
      openLocationDrawer: false,
      gifDrawer: false,
      variant: "success",
      loader: true,
      isMobile: "",
      datesModal: false,
      createDateScreen: false,
      dateType: "",
      createNewDate: false,
      otherUserProfileDrawer: false,
      coinPopUpSuccessfull: false,
      reportReasonModal: false,
      showCoinAnimation: false,
      reportReasons: [],
      type: 0,
      storeGif: "", // string to send coin based msg,
      lat: "",
      lng: "",
      city: "",
      locationSelected: "",
      realFileToUpload: "", // to upload file { blob }
    };
  }

  CoinChatFunc = (obj) => {
    obj["isMatchedUser"] = 0;
    let { perMsgWithoutMatch } = this.props.CoinConfig;
    let updateUserSelectedChat = { ...this.props.userInfo };
    if (obj["chatId"] === undefined) {
      obj["chatId"] = "";
    } else {
      obj["chatId"] = obj["chatId"];
    }

    CoinChat(obj)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            isNewUserChatFocused: true,
            showCoinAnimation: true,
          });
        }

        if (
          this.props.selectedProfile.selectedProfile._id === updateUserSelectedChat._id ||
          this.props.selectedProfile.selectedProfile._id === updateUserSelectedChat.userId ||
          this.props.selectedProfile.selectedProfile._id === updateUserSelectedChat.opponentId
        ) {
          obj["timestamp"] = obj.timestamp;
          obj["chatId"] = res.data.chatId;
          this.setState({
            isNewUserChatFocused: true,
            showCoinAnimation: true,
          });
          updateUserSelectedChat["timestamp"] = obj.timestamp;
          updateUserSelectedChat["chatId"] = res.data.chatId;
          updateUserSelectedChat["isMatchedUser"] = 0;
          this.props.USERSELECTEDTOCHAT(updateUserSelectedChat);
          this.addNewMessage(obj);
        }
        if (
          updateUserSelectedChat.initiated ||
          !("initiated" in updateUserSelectedChat) ||
          !("chatInitiatedBy" in updateUserSelectedChat)
        ) {
          this.setState({
            isNewUserChatFocused: true,
            showCoinAnimation: true,
          });
          this.props.coinReduce(perMsgWithoutMatch.Coin);
        }
        setTimeout(() => {
          this.setState({ showCoinAnimation: false });
        }, 1000);
      })
      .catch((err) => {
        if (updateUserSelectedChat.initiated) {
          this.setState({ showCoinAnimation: true });
          this.props.coinReduce(perMsgWithoutMatch.Coin);
          this.setState({
            isNewUserChatFocused: true,
            showCoinAnimation: true,
          });
        }
        setTimeout(() => {
          this.setState({ showCoinAnimation: false });
        }, 1000);
        console.log(err);
      });
  };

  unblockUserFunc = (targetUserId) => {
    this.props.m_updatePostStatus(`Unblocking ${this.props.userInfo.firstName}...`, true);
    let leftSideData = [...this.props.leftSideChatLogs];
    let index = leftSideData.findIndex((k) => k.receiverId === targetUserId);
    if (index > -1) {
      unblockuser(getCookie("token"), targetUserId).then((res) => {
        leftSideData[index].isBlockedByMe = 0;
        this.props.USERSELECTEDTOCHAT(leftSideData[index]);
        this.props.leftSideChatLogsFN(leftSideData);
        this.props.m_updatePostStatus("", false);
      });
    }
  };

  unfriendUserFunc = (targetUserId, chatId) => {
    let leftSideData = [...this.props.leftSideChatLogs];
    this.props.m_updatePostStatus(`Unfriending ${this.props.userInfo.firstName}...`, true);
    let index = leftSideData.findIndex((k) => k.chatId === chatId || k.recipientId === targetUserId);
    if (index > -1) {
      UnfriendUser(getCookie("token"), targetUserId)
        .then((res) => {
          leftSideData.splice(index, 1);
          this.props.leftSideChatLogsFN(leftSideData);
          this.props.m_updatePostStatus("", false);
        })
        .catch((err) => {
          console.log("error unfriending user");
        });
    }
  };

  BlockUserFn = (targetUserId) => {
    this.props.m_updatePostStatus(`Blocking ${this.props.userInfo.firstName}...`, true);
    let leftSideData = [...this.props.leftSideChatLogs];
    let index = leftSideData.findIndex((k) => k.receiverId === targetUserId);
    if (index > -1) {
      BlockUser(targetUserId)
        .then((res) => {
          leftSideData[index].isBlockedByMe = 1;
          this.props.USERSELECTEDTOCHAT(leftSideData[index]);
          this.props.leftSideChatLogsFN(leftSideData);
          this.props.m_updatePostStatus("", false);
        })
        .catch((err) => {
          console.log("blocked user err", err);
        });
    }
  };

  /** function to add message in existing object  */
  addNewMessage = (obj) => {
    if (this.props.SingleUserAllMessages && this.props.SingleUserAllMessages.length >= 0) {
      this.props.addNewChatToExistingObject(obj);
    }
    // let index = this.state.MatchFoundData.findIndex((k) => k.recipientId === obj.to);
    // if (index > -1 && this.state.MatchFoundData[index].isInMatches) {
    //   let cloneMatchFoundData = [...this.state.MatchFoundData];
    //   let objToAdd = cloneMatchFoundData[index];
    //   objToAdd.payload = obj.payload;
    //   this.props.leftSideChatLogsFN([objToAdd, ...this.props.leftSideChatLogs]);
    //   cloneMatchFoundData.splice(index, 1);
    //   this.setState({ MatchFoundData: cloneMatchFoundData });
    // } else {
    //   this.updateLeftSideChat(obj);
    // }
  };

  updateLeftSideChat = (obj) => {
    let leftSideAllChats = [...this.props.leftSideChatLogs];
    let index = leftSideAllChats.findIndex((k) => k.recipientId === obj.to || k._id === obj.to);
    if (index > -1) {
      leftSideAllChats[index].payload = obj.payload;
      leftSideAllChats[index].timestamp = obj.timestamp;
      if (index !== 0) {
        this.props.leftSideChatLogsFN(
          leftSideAllChats.sort(function (a, b) {
            return b.timestamp - a.timestamp;
          })
        );
      } else {
        this.props.leftSideChatLogsFN(leftSideAllChats);
      }
    }
  };

  DeleteChatTotally = (chatId) => {
    this.props.m_updatePostStatus("Deleting Chat...", true);
    let leftSideData = [...this.props.leftSideChatLogs];
    let index = leftSideData.findIndex((k) => k.chatId === chatId);
    deletechatTotally(chatId)
      .then((res) => {
        if (res.status === 200) {
          leftSideData.splice(index, 1);
          this.props.m_updatePostStatus("", false);
          this.props.leftSideChatLogsFN([...leftSideData]);
          this.props.history.goBack();
        } else {
          this.props.m_updatePostStatus(res.message || "Something went wrong...", true);
          setTimeout(() => {
            this.props.m_updatePostStatus("", false);
          }, 2000);
        }
      })
      .catch((err) => {
        this.props.m_updatePostStatus("Something went wrong...", true);
        setTimeout(() => {
          this.props.m_updatePostStatus("", false);
        }, 2000);
      });
  };

  unMatchUserFunc = (userId) => {
    this.props.m_updatePostStatus(`UnMatching ${this.props.userInfo.firstName}...`, true);
    unmatchuser(userId)
      .then((res) => {
        if (res.status === 200) {
          this.props.m_updatePostStatus("", false);
          this.props.history.goBack();
        }
      })
      .catch((err) => {
        console.log("error occured while unmatching user");
      });
  };

  toggleReportReasonsModal = () => {
    this.setState({ reportReasonModal: !this.state.reportReasonModal });
  };

  postReportReason = (reason) => {
    let UserReportPayload = {
      targetUserId: this.props.userInfo.recipientId,
      reason: reason,
      message: "Report",
    };
    ReportUser(UserReportPayload)
      .then((data) => {
        this.setState({
          _snackbar: true,
          usermessage: "Report Submitted Successfully",
        });
        this.toggleReportReasonsModal();
        setTimeout(() => {
          this.setState({ _snackbar: false });
        }, 2500);
      })
      .catch((error) => {
        console.log("report error");
        this.toggleReportReasonsModal();
      });
  };

  /******************************************************************************************************** */

  storeDateType = (type) => {
    if (type) {
      this.setState({
        dateType: type,
        createDateScreen: !this.state.createDateScreen,
        datesModal: false,
      });
    } else {
      this.setState({
        createDateScreen: !this.state.createDateScreen,
        datesModal: false,
      });
    }
  };

  setTypeOfMessage = (gif, type) => {
    this.setState({ storeGif: gif, type: type });
  };

  toggleDatesModal = () => {
    this.setState({ datesModal: !this.state.datesModal });
  };

  handleChange = (event) => {
    this.setState({ multiline: event.target.value });
  };

  toggleNoCoinModal = () => {
    this.setState({ noCoinModal: !this.state.noCoinModal });
  };

  toggleSendCoinsDrawer = () => {
    this.setState({ sendCoinsDrawer: !this.state.sendCoinsDrawer });
  };

  toggleLocationDrawer = () => {
    this.setState({ openLocationDrawer: !this.state.openLocationDrawer });
  };

  _setIsGif = () => {
    this.setState({ isGif: false });
  };

  toggleGifDrawer = (cb) => {
    this.setState({ gifDrawer: !this.state.gifDrawer });
  };

  setHandleChangeEmpty = () => {
    this.setState({ multiline: "" });
  };

  openUserProfilePage = (firstName, opponentId) => {
    this.props.history.push(`/app/mobile/user/${firstName}/${opponentId}`);
  };

  sendMessage = () => {
    let newMsg = this.state.multiline;
    if (newMsg.trim() === "" || newMsg.trim().length === 0) {
      return;
    }
    this.setTypeOfMessage("", 0);
    let uid = getCookie("uid");
    let objToSendIfOnline = {
      chatId: this.props.userInfo.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props && this.props.UserFinalData && this.props.UserFinalData.firstName,
      payload: this.textencode(newMsg),
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to: this.props.userInfo.recipientId || this.props.userInfo._id || this.props.userInfo.opponentId || this.props.userInfo.from,
      toDocId: "",
      type: "0",
      status: 1,
      userImage: this.props.userInfo.profilePic || this.props.userInfo.userImage,
    };

    if (
      (parseInt(this.props.coins) === 0 &&
        "chatInitiatedBy" in this.props.userInfo &&
        this.props.userInfo.chatInitiatedBy !== getCookie("uid")) ||
      (parseInt(this.props.coins) === 0 && this.props.userInfo.initiated === false)
    ) {
      objToSendIfOnline["isMatchedUser"] = 0;
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
      this.setHandleChangeEmpty();
    } else if (
      "chatInitiatedBy" in this.props.userInfo &&
      (this.props.userInfo.isMatched || this.props.userInfo.isMatchedUser || this.props.userInfo.isFriend)
    ) {
      this.props.userInfo.isFriend ? (objToSendIfOnline["isMatchedUser"] = 0) : (objToSendIfOnline["isMatchedUser"] = 1);
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
      this.setHandleChangeEmpty();
    } else if (parseInt(this.props.coins) > 0) {
      if (
        ((this.props.userInfo.isMatched === 0 || this.props.userInfo.isMatchedUser === 0 || this.props.userInfo.isFriend === 0) &&
          !("chatInitiatedBy" in this.props.userInfo)) ||
        !("initiated" in this.props.userInfo)
      ) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
          this.setHandleChangeEmpty();
        }
      } else if ("initiated" in this.props.userInfo && this.props.userInfo.initiated === true) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
          this.setHandleChangeEmpty();
        }
      } else {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
        this.setHandleChangeEmpty();
      }
    } else if (
      parseInt(this.props.coins) === 0 &&
      (!("chatInitiatedBy" in this.props.userInfo) || "chatInitiatedBy" in this.props.userInfo)
    ) {
      this.toggleNoCoinModal();
    }
  };

  sendGifHandler = (gif, bool) => {
    this.setTypeOfMessage(gif, 1);
    let encodedString = new Buffer(gif).toString("base64");
    let uid = getCookie("uid");
    let objToSendIfOnline = {
      chatId: this.props.userInfo.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props && this.props.UserFinalData && this.props.UserFinalData.firstName,
      payload: encodedString,
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to: this.props.userInfo.recipientId || this.props.userInfo._id || this.props.userInfo.opponentId || this.props.userInfo.from,
      toDocId: "",
      type: bool == true ? "8" : "6",
      userImage: this.props.userInfo.profilePic || this.props.userInfo.userImage,
    };
    if (
      (parseInt(this.props.coins) === 0 &&
        "chatInitiatedBy" in this.props.userInfo &&
        this.props.userInfo.chatInitiatedBy !== getCookie("uid")) ||
      (parseInt(this.props.coins) === 0 && this.props.userInfo.initiated === false)
    ) {
      objToSendIfOnline["isMatchedUser"] = 0;
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (
      "chatInitiatedBy" in this.props.userInfo &&
      (this.props.userInfo.isMatched || this.props.userInfo.isMatchedUser || this.props.userInfo.isFriend)
    ) {
      this.props.userInfo.isFriend ? (objToSendIfOnline["isMatchedUser"] = 0) : (objToSendIfOnline["isMatchedUser"] = 1);
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (parseInt(this.props.coins) > 0) {
      if (
        ((this.props.userInfo.isMatched === 0 || this.props.userInfo.isMatchedUser === 0 || this.props.userInfo.isFriend === 0) &&
          !("chatInitiatedBy" in this.props.userInfo)) ||
        !("initiated" in this.props.userInfo)
      ) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else if ("initiated" in this.props.userInfo && this.props.userInfo.initiated === true) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
      }
    } else if (
      parseInt(this.props.coins) === 0 &&
      (!("chatInitiatedBy" in this.props.userInfo) || "chatInitiatedBy" in this.props.userInfo)
    ) {
      this.toggleNoCoinModal();
    }
  };

  sendLocation = (lat, lng, placeName, onClickedAddress) => {
    this.setTypeOfMessage("", 3);
    let buff = new Buffer(this.createLocationToSend(lat, lng, placeName, onClickedAddress));
    let base64data = buff.toString("base64");
    let uid = getCookie("uid");
    let objToSendIfOnline = {
      chatId: this.props.userInfo.chatId,
      from: uid,
      id: new Date().getTime().toString(),
      isMatchedUser: this.isUserMatchedOrFriend(),
      name: this.props && this.props.UserFinalData && this.props.UserFinalData.firstName,
      payload: base64data,
      receiverIdentifier: uid,
      timestamp: new Date().getTime(),
      to: this.props.userInfo.recipientId || this.props.userInfo._id || this.props.userInfo.opponentId || this.props.userInfo.from,
      toDocId: "",
      type: "3",
      userImage: this.props.userInfo.profilePic || this.props.userInfo.userImage,
    };
    if (
      (parseInt(this.props.coins) === 0 &&
        "chatInitiatedBy" in this.props.userInfo &&
        this.props.userInfo.chatInitiatedBy !== getCookie("uid")) ||
      (parseInt(this.props.coins) === 0 && this.props.userInfo.initiated === false)
    ) {
      objToSendIfOnline["isMatchedUser"] = 0;
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (
      "chatInitiatedBy" in this.props.userInfo &&
      (this.props.userInfo.isMatched === 1 || this.props.userInfo.isMatchedUser === 1 || this.props.userInfo.isFriend === 1)
    ) {
      this.props.userInfo.isFriend ? (objToSendIfOnline["isMatchedUser"] = 0) : (objToSendIfOnline["isMatchedUser"] = 1);
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (parseInt(this.props.coins) > 0) {
      if (
        ((this.props.userInfo.isMatched === 0 || this.props.userInfo.isMatchedUser === 0 || this.props.userInfo.isFriend === 0) &&
          !("chatInitiatedBy" in this.props.userInfo)) ||
        !("initiated" in this.props.userInfo)
      ) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else if ("initiated" in this.props.userInfo && this.props.userInfo.initiated === true) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
      }
    } else if (
      parseInt(this.props.coins) === 0 &&
      (!("chatInitiatedBy" in this.props.userInfo) || "chatInitiatedBy" in this.props.userInfo)
    ) {
      this.toggleNoCoinModal();
    }
  };

  handleClosesnakbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ _snackbar: false });
  };

  navigateBackToChatScreen = () => {
    this.props.allMessagesForSelectedUserFunc([]);
    let id = this.props.userInfo.recipientId || this.props.userInfo._id || this.props.userInfo.opponentId || this.props.userInfo.from;
    unSbscribeToUserTopic(id);
    let ChatLogs = [...this.props.leftSideChatLogs];
    let index = ChatLogs.findIndex((k) => k.chatId === this.props.userInfo.chatId);
    if (ChatLogs[index] && index > -1) {
      ChatLogs[index]["checked"] = false;
      ChatLogs[index]["totalUnread"] = 0;
      this.props.USERSELECTEDTOCHAT(ChatLogs[index]);
    }
    this.props.history.goBack();
  };

  generateSubstring = (url) => {
    let res = url.substr(23, 41);
    return res;
  };

  isUserMatchedOrFriend = () => {
    let { isFriend, isMatchedUser, isMatched } = this.props.userInfo;
    if (isMatchedUser == 1 || isMatched == 1 || isFriend == 1) {
      return 1;
    }
    return 0;
  };

  uploadImage = async (files) => {
    this.setState({ realFileToUpload: files });
    let size = "";
    let uid = getCookie("uid");
    let typeOfData = files.type;
    this.setTypeOfMessage("", 2);
    let objToSendIfOnline = "";
    size = files.size;

    let formData = new FormData();
    formData.append("photo", files, new Date().getTime().toString());
    let responseFileUrl = await uploadMediaOverLocalServer(formData);
    let url = responseFileUrl.data.url.substr(23, responseFileUrl.data.url.length - 1);

    let url2 = "https://fetch.unfiltered.love/" + url;

    objToSendIfOnline = {
      receiverIdentifier: uid,
      from: uid,
      userImage: this.props.userInfo.profilePic || this.props.userInfo.userImage,
      chatId: this.props.userInfo.chatId,
      to: this.props.userInfo.recipientId || this.props.userInfo._id || this.props.userInfo.opponentId || this.props.userInfo.from,
      toDocId: "xxx",
      timestamp: new Date().getTime().toString(),
      id: new Date().getTime().toString(),
      name: this.props && this.props.UserFinalData && this.props.UserFinalData.firstName,
      isMatchedUser: this.isUserMatchedOrFriend(),
      type: typeOfData.includes("mp4") ? "2" : "1",
      payload: btoa(url2),
      dataSize: size,
      thumbnail: demoThumbNail,
    };
    console.log("objToSendIfOnline", objToSendIfOnline);
    if (
      (parseInt(this.props.coins) === 0 &&
        "chatInitiatedBy" in this.props.userInfo &&
        this.props.userInfo.chatInitiatedBy !== getCookie("uid")) ||
      (parseInt(this.props.coins) === 0 && this.props.userInfo.initiated === false)
    ) {
      objToSendIfOnline["isMatchedUser"] = 0;
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (
      "chatInitiatedBy" in this.props.userInfo &&
      (this.props.userInfo.isMatched || this.props.userInfo.isMatchedUser || this.props.userInfo.isFriend)
    ) {
      this.props.userInfo.isFriend ? (objToSendIfOnline["isMatchedUser"] = 0) : (objToSendIfOnline["isMatchedUser"] = 1);
      this.addNewMessage(objToSendIfOnline);
      sendMessageOverMqtt(objToSendIfOnline);
    } else if (parseInt(this.props.coins) > 0) {
      if (
        ((this.props.userInfo.isMatched === 0 || this.props.userInfo.isMatchedUser === 0 || this.props.userInfo.isFriend === 0) &&
          !("chatInitiatedBy" in this.props.userInfo)) ||
        !("initiated" in this.props.userInfo)
      ) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else if ("initiated" in this.props.userInfo && this.props.userInfo.initiated === true) {
        if (this.state.coinPopUpSuccessfull === false) {
          this.props.coinToastShow();
        } else if (this.state.coinPopUpSuccessfull === true) {
          objToSendIfOnline["isMatchedUser"] = 0;
          this.CoinChatFunc(objToSendIfOnline);
          this.addNewMessage(objToSendIfOnline);
        }
      } else {
        objToSendIfOnline["isMatchedUser"] = 0;
        this.addNewMessage(objToSendIfOnline);
        sendMessageOverMqtt(objToSendIfOnline);
      }
    } else if (
      parseInt(this.props.coins) === 0 &&
      (!("chatInitiatedBy" in this.props.userInfo) || "chatInitiatedBy" in this.props.userInfo)
    ) {
      this.toggleNoCoinModal();
    }
  };

  setLocationForSendingMessage = (lat, lng, city, locationSelected) => {
    this.setState({ lat: lat, lng: lng, city: city, locationSelected: locationSelected });
  };

  createLocationToSend = (lat, lng, placeName, completeAddress) => {
    return `(${lat},${lng})@@${placeName}@@${completeAddress}`;
  };

  textencode = (str) => {
    try {
      return btoa(unescape(encodeURIComponent(str)));
    } catch (e) {}
  };

  textdecode = (str) => {
    // console.log("text decode ", str);
    try {
      return decodeURIComponent(escape(atob(str)));
    } catch (e) {}
  };

  handleScroll = (event) => {
    var node = event.target;
    if (node.scrollTop === 0 && node.scrollHeight > node.clientHeight) {
      // console.log("am i top", this.props.SingleUserAllMessages);
      if (this.props.SingleUserAllMessages && this.props.SingleUserAllMessages.length % 10 === 0) {
        setTimeout(() => {
          this.GetAllChatsForSingleUserFn(
            this.props.userInfo.chatId,
            this.props.SingleUserAllMessages[this.props.SingleUserAllMessages.length - 1].timestamp,
            10
          );
        }, 300);
      }
    }
  };

  acknowledgementAllToBlueTick = (appendedChatData, index) => {
    try {
      let arr = appendedChatData.filter((k) => k.status !== 3 && k.senderId !== getCookie("uid"));
      let ackArr = [...arr];
      for (let i = 0; i < ackArr.length; i++) {
        sendAcknowledgementsOverMqtt({ obj: ackArr[i], newStatus: "3" });
      }
      // let ChatLogs = [...this.props.leftSideChatLogs];
      // ChatLogs[index]["totalUnread"] = 0;
      // this.props.leftSideChatLogsFN(ChatLogs);
      this.props.allMessagesForSelectedUserFunc(appendedChatData);
    } catch (ee) {
      console.log("something fucked up", ee);
    }
  };

  GetAllChatsForSingleUserFn = (chatId, timeStamp, pageSize) => {
    this.setState({ loader: true });
    GetAllChatsForSingleUser(chatId, timeStamp, pageSize)
      .then((data) => {
        if (data.status === 200) {
          this.setState({ loader: false });
          // let chatData = data.data.data;
          let chatData = this.props.SingleUserAllMessages;
          let ChatLogs = [...this.props.leftSideChatLogs];
          let index = ChatLogs.findIndex((k) => k.chatId === chatId);
          if (index > -1 && ChatLogs[index].totalUnread !== 0) {
            this.props.updateUnreadChatCountFunc(this.props.unreadChatCount - 1);
            ChatLogs[index]["totalUnread"] = 0;
          }
          this.props.leftSideChatLogsFN(ChatLogs);
          let appendedChatData = chatData.concat(data.data.data);
          console.log("appendedChatData", appendedChatData);
          this.acknowledgementAllToBlueTick(appendedChatData, index);
        }
      })
      .catch((err) => {
        this.setState({ loader: false });
        let emptyData = [];
        this.props.allMessagesForSelectedUserFunc(emptyData);
      });
  };

  componentDidMount() {
    try {
      if (this.props.userInfo && !("chatId" in this.props.userInfo)) {
        if (Object.keys(this.props.userInfo).length > 0) {
          this.props.USERSELECTEDTOCHAT(this.props.userInfo);
          this.setState({ loader: false });
        } else {
          GetAllChats(0).then((res) => {
            let exLogs = [...res.data.data];
            let index = exLogs.findIndex(
              (k) => k.receiverId === getCookie("userselectedtochat") || k.recipientId === getCookie("userselectedtochat")
            );
            if (index > -1) {
              console.log("oh i seeee 3333333333");
              this.GetAllChatsForSingleUserFn(exLogs[index].chatId, exLogs[index].timestamp + 4000, 20);
              // GetAllChatsForSingleUser(exLogs[index].chatId, exLogs[index].timestamp + 4000, 20).then((res) => {
              //   this.props.allMessagesForSelectedUserFunc(res.data.data);
              //   this.setState({ loader: false });
              // });
              this.props.USERSELECTEDTOCHAT(exLogs[index]);
            } else {
              OtherUserProfile({ targetUserId: this.props.userInfo._id || getCookie("userselectedtochat") }).then((res) => {
                this.props.USERSELECTEDTOCHAT(res.data.data);
                this.setState({ loader: false });
              });
            }
          });
        }
      } else if (this.props.userInfo && "chatId" in this.props.userInfo) {
        console.log("oh i seeee 444444444");
        this.GetAllChatsForSingleUserFn(this.props.userInfo.chatId, new Date().getTime() + 4000, 20);
      }
    } catch (e) {
      console.log("errorrrrrrrrrrrrrrrrrrrrrrr", e);
    }

    this.scrollRef.current.addEventListener("scroll", this.handleScroll, true);
    this.scrollToBot();
    ReasonblockUser().then((data) => {
      this.setState({ reportReasons: data.data.data });
    });
    if ((this.props && Object.keys(this.props.CoinConfig).length == 0) || (this.props && this.props.CoinConfig == "")) {
      getCoinConfig(getCookie("token")).then((res) => {
        if (res.status === 200) {
          this.props.saveCoinConfig(res.data.data[0]);
        }
      });
    }
  }

  scrollToBot() {
    ReactDOM.findDOMNode(this.scrollRef.current).scrollTop = ReactDOM.findDOMNode(this.scrollRef.current).scrollHeight;
  }

  componentDidUpdate() {
    this.scrollToBot();
  }

  componentWillUnmount() {
    this.scrollRef.current.removeEventListener("scroll", this.handleScroll, true);
    this.setState({ isMobile: window.innerWidth });
    this.props.allMessagesForSelectedUserFunc([]);
  }

  renderTextInputPlaceholder = (userInfo) => {
    if (userInfo.isBlockedByMe === 1) {
      return `You have blocked ${userInfo.firstName}`;
    } else if (userInfo.isBlocked === 1) {
      return `${userInfo.firstName} has blocked you.`;
    } else {
      return "Type a message";
    }
  };

  openOtherUserProfileDrawer = () => {
    this.setState({
      otherUserProfileDrawer: !this.state.otherUserProfileDrawer,
    });
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  render() {
    // console.log("showCoinAnimation", this.state.showCoinAnimation);
    let { classes, onClose, userInfo, SingleUserAllMessages, coins, coinReduce } = this.props;
    return (
      <div className="m_chat_view">
        <MainDrawer width={"100%"} onClose={this.toggleGifDrawer} onOpen={this.toggleGifDrawer} open={this.state.gifDrawer}>
          <GifPage toggleGifDrawer={this.toggleGifDrawer} sendGifHandler={this.sendGifHandler} isGif={this.state.isGif} />
        </MainDrawer>
        <MainDrawer width={"100%"} onClose={this.toggleGifDrawer} onOpen={this.toggleGifDrawer} open={this.state.gifDrawer}>
          <GifPage toggleGifDrawer={this.toggleGifDrawer} sendGifHandler={this.sendGifHandler} isGif={this.state.isGif} />
        </MainDrawer>
        <MainDrawer width={"100%"} onClose={this.storeDateType} onOpen={this.storeDateType} open={this.state.createDateScreen}>
          <CreateDateScreen
            onClose={this.storeDateType}
            CoinBalance={this.props.coins}
            opponentProfileInfo={this.props.userInfo}
            UserFinalData={this.props.UserFinalData}
            _createDateType={this.state.dateType}
          />
        </MainDrawer>

        <MainDrawer
          width={"100%"}
          onClose={this.openOtherUserProfileDrawer}
          onOpen={this.openOtherUserProfileDrawer}
          open={this.state.otherUserProfileDrawer}
        >
          <OtherUserProfileFromChatView
            coinReduce={coinReduce}
            balance={coins}
            userInfo={userInfo}
            onClose={this.openOtherUserProfileDrawer}
          />
        </MainDrawer>

        <MainDrawer
          width={"100%"}
          onClose={this.toggleSendCoinsDrawer}
          onOpen={this.toggleSendCoinsDrawer}
          open={this.state.sendCoinsDrawer}
        >
          <SendCoinsDrawer
            myPic={this.props && this.props.UserFinalData && this.props.UserFinalData.profilePic}
            sendersName={this.props && this.props.UserFinalData && this.props.UserFinalData.firstName}
            coinReduce={coinReduce}
            balance={coins}
            userInfo={userInfo}
            onClose={this.toggleSendCoinsDrawer}
          />
        </MainDrawer>

        <MainDrawer
          width={"100%"}
          onClose={this.toggleLocationDrawer}
          onOpen={this.toggleLocationDrawer}
          open={this.state.openLocationDrawer}
        >
          <LocationSearchInput
            setLocationForSendingMessage={this.setLocationForSendingMessage}
            sendLocation={this.sendLocation}
            onClose={this.toggleLocationDrawer}
          />
        </MainDrawer>

        <div className="col-12 py-2 Mfriend_chat" id="Mfriend_chat">
          <div className="row align-items-center m_chat_header">
            <div className="p col-2 MCommonwidth">
              <img src={Icons.PinkBack} width={25} height={20} alt="back" onClick={this.navigateBackToChatScreen} />
            </div>
            <div
              className="col-6 px-0"
              onClick={() => {
                this.openOtherUserProfileDrawer();
                // this.openUserProfilePage(userInfo.firstName, userInfo.recipientId);
              }}
            >
              <Media className="d-flex align-items-center">
                <Media left>
                  <Media
                    object
                    style={{
                      borderRadius: "50%",
                      width: "50px",
                      height: "50px",
                      objectFit: "cover",
                    }}
                    src={userInfo.profilePic || userInfo.userImage}
                    alt={userInfo.firstName}
                    title={userInfo.firstName}
                    onError={this.onError}
                  />
                </Media>
                <Media body className="col-12 m_ChatView_userName">
                  <Media heading className="m-0" style={{ textTransform: "capitalize" }}>
                    {userInfo.firstName || userInfo.name}
                    <div>{userInfo.onlineStatus === 1 ? "Online" : "Offline"}</div>
                  </Media>
                </Media>
              </Media>
            </div>
            <div className="col-4 MChat_icon">
              <div className="row">
                <div>
                  {(userInfo.isMatched === 1 || userInfo.isMatchedUser === 1) && userInfo.isFriend === 1 ? (
                    // <img src={Icons.videoCameraEnabled} alt="dates" height={22} width={22} onClick={() => this.props.__callInit(true)} />
                    ""
                  ) : (userInfo.isMatched === 1 || userInfo.isMatchedUser === 1) && userInfo.isFriend === 0 ? (
                    <img src={Icons.TabDateActive} alt="dates" height={22} width={22} onClick={this.toggleDatesModal} />
                  ) : (userInfo.isMatched === 0 || userInfo.isMatchedUser === 0) && userInfo.isFriend === 1 ? (
                    // <img src={Icons.videoCameraEnabled} alt="dates" height={22} width={22} onClick={() => this.props.__callInit(true)} />
                    ""
                  ) : (
                    ""
                  )}
                </div>

                {/* <div className="col">
                  <img
                    src={Icons.videoCameraEnabled}
                    alt="dates"
                    height={22}
                    width={22}
                    onClick={this.toggleDatesModal}
                  />
                </div> */}

                <div>
                  <WithState>
                    {({ anchorEl, updateAnchorEl }) => {
                      const open = Boolean(anchorEl);
                      return (
                        <React.Fragment>
                          <Button
                            aria-owns={open ? "render-props-popover" : undefined}
                            aria-haspopup="true"
                            className={classes.attachmentButton}
                            classes={{
                              contained: classes.onClick,
                              root: classes.onClick,
                              raised: classes.onClick,
                            }}
                            variant="raised"
                            onClick={(event) => {
                              updateAnchorEl(event.currentTarget);
                            }}
                          >
                            <img src={Attachment} alt="attachment" height={22} width={22} />
                          </Button>
                          <Popover
                            id="render-props-popover"
                            open={open}
                            anchorEl={anchorEl}
                            onClose={() => {
                              updateAnchorEl(null);
                            }}
                            anchorOrigin={{
                              vertical: "bottom",
                              horizontal: "center",
                            }}
                            transformOrigin={{
                              vertical: "top",
                              horizontal: "center",
                            }}
                            className={classes.root}
                          >
                            <div className={classes.options}>
                              <div className={classes.optionSub}>
                                <div className={classes.eachOptions}>
                                  <div className="a_gallery">
                                    <label>
                                      <img src={Gallery} alt="Gallery" width={25} height={25}></img>
                                      <input
                                        style={{
                                          visibility: "hidden",
                                          width: "0px",
                                        }}
                                        type="file"
                                        id="add_image"
                                        accept="image/png, image/jpeg, video/mp4"
                                        onChange={(e) => {
                                          this.uploadImage(e.target.files[0]);
                                          updateAnchorEl(null);
                                        }}
                                      />
                                    </label>
                                  </div>
                                  <div className="text-center pt-1">Gallery</div>
                                </div>
                                <div
                                  className={classes.eachOptions}
                                  onClick={() => {
                                    this.toggleGifDrawer();
                                    updateAnchorEl(null);
                                    this.setState({ isGif: false });
                                  }}
                                >
                                  <div className="a_sticker">
                                    <img src={Icons.MobileSticker} height={22} width={22} alt="sticker" />
                                  </div>
                                  <div className="text-center pt-1">Sticker</div>
                                </div>
                                <div
                                  className={classes.eachOptions}
                                  onClick={() => {
                                    this.toggleGifDrawer();
                                    updateAnchorEl(null);
                                  }}
                                >
                                  <div className="a_gif">GIF</div>
                                  <div className="text-center pt-1">Gifs</div>
                                </div>
                                <div className={classes.eachOptions}>
                                  <div className="a_video">
                                    <label>
                                      <img src={Video} alt="Video" width={25} height={25} />
                                      <input
                                        style={{
                                          visibility: "hidden",
                                          width: "0px",
                                        }}
                                        type="file"
                                        id="add_image"
                                        accept="image/png, image/jpeg, video/mp4"
                                        onChange={(e) => {
                                          this.uploadImage(e.target.files[0]);
                                          updateAnchorEl(null);
                                        }}
                                      />
                                    </label>
                                  </div>
                                  <div className="text-center pt-1">Video</div>
                                </div>
                              </div>
                              <div className={classes.optionSub}>
                                <div
                                  className={classes.eachOptions}
                                  onClick={() => {
                                    updateAnchorEl(null);
                                    this.toggleLocationDrawer();
                                  }}
                                >
                                  <div className="a_location">
                                    <img src={Pin} alt="Pin" width={25} height={25} />
                                  </div>
                                  <div className="text-center pt-1">Location</div>
                                </div>
                                {/* <div
                                  className={`${classes.eachOptions} px-0`}
                                  onClick={() => {
                                    updateAnchorEl(null);
                                    this.toggleSendCoinsDrawer();
                                  }}
                                >
                                  <div className="a_send_coins">
                                    <img src={Icons.MoneyTransfer} alt="MoneyTransfer" width={25} height={25} />
                                  </div>
                                  <div className="text-center pt-1">Send Credits</div>
                                </div> */}
                              </div>
                            </div>
                          </Popover>
                        </React.Fragment>
                      );
                    }}
                  </WithState>
                </div>
                <div>
                  <OptionsState>
                    {({ Op_anchorEl, Op_updateAnchorEl }) => {
                      const open = Boolean(Op_anchorEl);
                      return (
                        <React.Fragment>
                          <Button
                            aria-owns={open ? "render-props-popover" : undefined}
                            aria-haspopup="true"
                            className={classes.attachmentButton}
                            classes={{
                              contained: classes.onClick,
                              root: classes.onClick,
                              raised: classes.onClick,
                            }}
                            variant="contained"
                            onClick={(event) => {
                              Op_updateAnchorEl(event.currentTarget);
                            }}
                          >
                            <img src={ThreeDots} alt="ellipsis" height={22} width={22} className="mr-2" />
                          </Button>
                          <Popover
                            id="render-props-popover"
                            open={open}
                            anchorEl={Op_anchorEl}
                            onClose={() => {
                              Op_updateAnchorEl(null);
                            }}
                            anchorOrigin={{
                              vertical: "bottom",
                              horizontal: "center",
                            }}
                            transformOrigin={{
                              vertical: "top",
                              horizontal: "center",
                            }}
                            className={classes.optionsRoot}
                          >
                            <div className={classes.otherOptions}>
                              <div className={classes.optionsSub}>
                                {(userInfo.isMatchedUser === 1 || userInfo.isMatched === 1) && userInfo.isFriend === 1 ? (
                                  userInfo.isFriend ? (
                                    <div
                                      className={classes.otherOptionsOptions}
                                      onClick={() => {
                                        this.unfriendUserFunc(userInfo.recipientId, userInfo.chatId);
                                        Op_updateAnchorEl(null);
                                        onClose();
                                      }}
                                    >
                                      <div className="text-center pt-1">UnFriend {userInfo.firstName}</div>
                                    </div>
                                  ) : (
                                    <></>
                                  )
                                ) : userInfo.isMatchedUser === 1 || userInfo.isMatched === 1 ? (
                                  <div
                                    className={classes.otherOptionsOptions}
                                    onClick={() => {
                                      this.unMatchUserFunc(userInfo.recipientId);
                                      Op_updateAnchorEl(null);
                                    }}
                                  >
                                    <div className="text-center pt-1">UnMatch {userInfo.firstName}</div>
                                  </div>
                                ) : userInfo.isFriend === 1 ? (
                                  <div
                                    className={classes.otherOptionsOptions}
                                    onClick={() => {
                                      this.unfriendUserFunc(userInfo.recipientId, userInfo.chatId);
                                      Op_updateAnchorEl(null);
                                      onClose();
                                    }}
                                  >
                                    <div className="text-center pt-1">UnFriend {userInfo.firstName}</div>
                                  </div>
                                ) : (
                                  <></>
                                )}
                                <div
                                  className={classes.otherOptionsOptions}
                                  onClick={() => {
                                    this.DeleteChatTotally(userInfo.chatId, onClose);
                                  }}
                                >
                                  <div className="text-center pt-1">Delete Chat</div>
                                </div>
                                {userInfo.isBlockedByMe ? (
                                  <div
                                    className={classes.otherOptionsOptions}
                                    onClick={() => {
                                      this.unblockUserFunc(userInfo.recipientId);
                                      Op_updateAnchorEl(null);
                                    }}
                                  >
                                    <div className="text-center pt-1">Unblock {userInfo.firstName}</div>
                                  </div>
                                ) : (
                                  <div
                                    className={classes.otherOptionsOptions}
                                    onClick={() => {
                                      this.BlockUserFn(userInfo.recipientId);
                                      Op_updateAnchorEl(null);
                                    }}
                                  >
                                    <div className="text-center pt-1">Block {userInfo.firstName}</div>
                                  </div>
                                )}
                                <div
                                  className={classes.otherOptionsOptions}
                                  onClick={() => {
                                    Op_updateAnchorEl(null);
                                    setTimeout(() => {
                                      this.toggleReportReasonsModal();
                                    }, 300);
                                  }}
                                >
                                  <div className="text-center pt-1">Report {userInfo.firstName}</div>
                                </div>
                              </div>
                            </div>
                          </Popover>
                        </React.Fragment>
                      );
                    }}
                  </OptionsState>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ul className="m_chat_scrollView px-2 pb-3" ref={this.scrollRef}>
          {this.state.loader ? (
            <div className="m_Fchat_Loader">
              <Loader text="Loading..." />
            </div>
          ) : (
            ""
          )}
          {SingleUserAllMessages.map((chat, index) => (
            <Messages
              key={index}
              chat={chat}
              sendersName={this.props && this.props.UserFinalData && this.props.UserFinalData.firstName}
              mobile={this.state.isMobile <= 576 ? true : false}
              textdecode={this.textdecode}
            />
          )).reverse()}
        </ul>
        {/* Chat Enter Text Module */}
        {this.state.showCoinAnimation ? (
          <lottie-player
            src="https://assets6.lottiefiles.com/packages/lf20_smGEjL.json"
            background="transparent"
            speed="1"
            repeat
            style={{
              width: "200px",
              height: "200px",
              position: "absolute",
              zIndex: 999,
              left: "30px",
              bottom: "0px",
              // right: "-20px",
            }}
            autoplay
          ></lottie-player>
        ) : (
          <span />
        )}
        <div className="m_chat_text_edit_view py-1">
          <div className="mx-1">
            <TextField
              InputProps={{
                disableUnderline: true,
              }}
              inputProps={classes.underline}
              id="standard-multiline-flexible"
              multiline={true}
              rowsMax={2}
              rows={2}
              placeholder={this.renderTextInputPlaceholder(this.props.userInfo)}
              value={this.state.multiline}
              onChange={(e) => this.handleChange(e)}
              className={classes.textField}
              disabled={this.props.userInfo.isBlockedByMe === 1 || this.props.userInfo.isBlocked === 1}
            />
          </div>
          <div>
            <div onClick={this.sendMessage}>
              <img src={SendIcon} alt="send-icon" />
            </div>
          </div>
        </div>
        <MatUiModal isOpen={this.state.datesModal} onClose={this.toggleDatesModal} toggle={this.toggleDatesModal} width={"90%"}>
          <div className="col-12 p-3">
            <div className="row">
              <div className="col-4 border-right" onClick={() => (this.props.coins >= 5 ? this.storeDateType(3) : this.purchaseCoinModal)}>
                <div className="text-center">
                  <img src={Icons.m_call_date} height={80} alt="call-date" />
                </div>
                <div className="pt-3 m_date_options">
                  <FormattedMessage id="message.audioDate" />
                </div>
              </div>
              <div className="col-4" onClick={() => (this.props.coins >= 500 ? this.storeDateType(2) : this.purchaseCoinModal)}>
                <div className="text-center">
                  <img src={Icons.m_physical_date} height={80} alt="physical-date" />
                </div>
                <div className="pt-3 m_date_options">
                  <FormattedMessage id="message.inPersonDate" />
                </div>
              </div>
              <div className="col-4 border-left" onClick={() => (this.props.coins >= 500 ? this.storeDateType(1) : this.purchaseCoinModal)}>
                <div className="text-center">
                  <img src={Icons.m_video_call_date} height={80} alt="video-call" />
                </div>
                <div className="pt-3 m_date_options">
                  <FormattedMessage id="message.videoDate" />
                </div>
              </div>
            </div>
          </div>
        </MatUiModal>
        <MaterialModal toggle={this.toggleNoCoinModal} isOpen={this.state.noCoinModal} width={"90%"}>
          <div className="col-12 m_boostModal">
            <div onClick={this.toggleNoCoinModal}>
              <img src={Icons.PinkCloseBtn} alt="close-btn" height={18} width={18} />
            </div>
            <div>
              <img src={Icons.SpendCoins} alt="coins" height={120} width={100} />
            </div>
            <div>You don't have enough coins to start chat.</div>
            <CustomButton
              handler={() => {
                this.toggleNoCoinModal();
              }}
              text="Ok"
            />
          </div>
        </MaterialModal>
        <MaterialModal isOpen={this.props.coinDialogShow} toggle={this.props.coinToastDismiss} width={"90%"}>
          <div className="col-12 p-3 text-center m_spendCoinsOnChatModal" style={{ position: "relative" }}>
            <div onClick={this.props.coinToastDismiss}>
              <img src={Icons.PinkCloseBtn} height={15} width={15} alt="close-btn" />
            </div>
            <div className="col-12">
              <img src={Icons.SpendCoins} height={120} width={100} alt="gold-coins" />
            </div>
            <div className="text-center">Give away coins to connect.</div>
            <div className="text-center">Get a chance to connect without being matched</div>
            <CustomButton
              text={`I am OK to spend ${
                this.props.CoinConfig && this.props.CoinConfig.perMsgWithoutMatch && this.props.CoinConfig.perMsgWithoutMatch.Coin
              } Credits`}
              handler={() => {
                console.log("%c handler 11111", "background: #FFA500; color: #FFFFFF; font-size: 18px;");
                this.setState({ coinPopUpSuccessfull: true }, () => {
                  console.log("%c handler 22222", "background: #FFA500; color: #FFFFFF; font-size: 18px;");
                  if (this.state.type === 0) {
                    console.log("%c handler 33333", "background: #FFA500; color: #FFFFFF; font-size: 18px;");
                    this.setState({ showCoinAnimation: true });
                    this.sendMessage();
                  } else if (this.state.type === 1) {
                    this.setState({ showCoinAnimation: true });
                    this.sendGifHandler(this.state.storeGif, this.state.isGif);
                  } else if (this.state.type === 2) {
                    this.setState({ showCoinAnimation: true });
                    this.uploadImage(this.state.realFileToUpload);
                  } else if (this.state.type === 3) {
                    this.setState({ showCoinAnimation: true });
                    this.sendLocation(this.state.lat, this.state.lng, this.state.city, this.state.locationSelected);
                  }
                  console.log("%c handler 44444", "background: #FFA500; color: #FFFFFF; font-size: 18px;");
                  this.props.coinToastDismiss();
                });
              }}
            />
          </div>
        </MaterialModal>
        <MaterialModal toggle={this.toggleReportReasonsModal} isOpen={this.state.reportReasonModal} width={"90%"}>
          <div className="col-12 m_reportReasonsModal">
            <div>Report User</div>
            {this.state.reportReasons.map((k) => (
              <div key={k} onClick={() => this.postReportReason(k)}>
                {k}
              </div>
            ))}
            <div onClick={this.toggleReportReasonsModal}>Cancel</div>
          </div>
        </MaterialModal>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state._snackbar}
          timeout={2500}
          onClose={this.handleClosesnakbar}
        />
      </div>
    );
  }
}

const MapWrapper = (props) => (
  <Map google={props.google} visible={false}>
    <FriendChat {...props} />
  </Map>
);

const mapDispatchToProps = (dispatch) => {
  return {
    leftSideChatLogsFN: (allChatLogs) => dispatch(actionsUser.leftSideChatLogs(allChatLogs)),
    updateUnreadChatCountFunc: (count) => dispatch(actionChat.storeUnreadChatCount(count)),
    allMessagesForSelectedUserFunc: (msgs) => dispatch(actions.allMessagesForSelectedUser(msgs)),
    m_updatePostStatus: (text, bool) => dispatch(PostActions.uploadingPostStatus(text, bool)),
    addNewChatToExistingObject: (obj) => dispatch(actionChat.addNewChatToExistingObject(obj)),
    USERSELECTEDTOCHAT: (user) => dispatch(actions.userSelectedToChat(user)),
    coinToastDismiss: () => dispatch(actionChat.dismissCoinChatToast()),
    coinToastShow: () => dispatch(actionChat.showUpCoinChatToast()),
    saveCoinConfig: (data) => dispatch(coinCfg.getCoinConfig(data)),
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    imageUploadFn: (img) => dispatch(actions.uploadImage(img)),
    __callInit: (data) => dispatch(__callInit(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    CoinConfig: state.CoinConfig.CoinConfig,
    coins: state.UserProfile.CoinBalance, // wallet coins
    selectedProfile: state.selectedProfile.selectedProfile,
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    lastIndexTimeStamp: state.UserSelectedToChat.lastIndexTimeStamp,
    SingleUserAllMessages: state.UserSelectedToChat.SingleUserAllMessages, // each and every single message of selected user for chat
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    coinDialogShow: state.UserSelectedToChat.showCoinToast,
    userInfo: state.UserSelectedToChat.UserSelectedToChat, // user selected to chat
    serverImageFolderLocation: state.UserSelectedToChat.serverImageFolderLocation,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(MapWrapper)));
