// Main React Components
import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import "./Discover.scss";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MainModel from "../../../../Components/Model/model";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { withStyles } from "@material-ui/core/styles";
import Preferences from "../Preferences/Preferences";
import CoinBalances from "../Coins/CoinBalance";
import SwiplebleCards from "./SwiplebleCard";
import MobileSettings from "../Settings/Setings";
import { connect } from "react-redux";
import { NearbyPeople, MyDisLikeUser, LikeNearPeople, SuperLikeNearPeople, SuperLikeUser } from "../../../../controller/auth/verification";
import { searchUserByPagination } from "../../../../services/auth";
import { discoverPeople } from "../../../../actions/DiscoverPeople";
import { Images } from "../../../../Components/Images/Images";
import * as actionChat from "../../../../actions/chat";
import { getCookie } from "../../../../lib/session";
import { Icons } from "../../../../Components/Icons/Icons";
import { MOBILE_ACTION_FUNC } from "../../../../actions/User";
import PremiumUserContent from "../ModalContent/PremiumUser";
import MobilePayPalPayment from "../Coins/WalletScreens/MobilePayPalPayment";

const styles = (theme) => ({
  slider: {
    padding: "22px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  rootrtabs: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  root: {
    flexGrow: 1,
    maxWidth: 500,
  },
});

class HeaderBody extends Component {
  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
    this.state = {
      Preferences: false,
      CoinBalances: false,
      setting: false,
      start: 0,
      end: 10,
      checkedA: true,
      checkedB: true,
      age: "",
      labelWidth: 0,
      activeTab: "1",
      open: false,
      value: 0,
      modalsuperlike: false,
      shown: true,
      settingDrawer: false,
      count: 0,
      intervalId: "",
      countDown: "",
      boostModal: false,
      lastDirection: "",
      variant: "success",
      _eliteDrawer: false,
      superLikeStatusCode: "",
      links: [
        {
          id: 1,
          icon: "fas fa-square-full",
          className: "MSwitch_view",
        },
        {
          id: 2,
          icon: "fas fa-th-large",
          className: "MSwitch_view",
        },
      ],
      activeLink: 1,
    };
    this.toggle = this.toggle.bind(this);
  }

  _toggleElitePopup = (bool) => {
    if (bool) {
      setTimeout(() => {}, 300);
      this._toggleEliteDrawer();
    } else {
      this.props.storeElitePlan(null);
    }
  };

  _storeElitePlanFunc = () => this.props.storeElitePlan(null);

  timer = () => {
    var newCount = this.state.countDown - 1;
    if (newCount >= 0) {
      this.setState({ countDown: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  componentDidMount() {
    this.scrollRef.current.addEventListener("scroll", this.handleScroll, true);
    const { match } = this.props;
    // console.log("match is ->", match);
    if (match.url === "/app") {
      window.history.pushState(null, document.title, window.location.href);
      window.addEventListener("popstate", function (event) {
        window.history.pushState(null, document.title, window.location.href);
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
    this.scrollRef.current.removeEventListener("scroll", this.handleScroll, true);
  }

  remove = () =>
    this.setState(({ cards }) => ({
      cards: cards.slice(1, cards.length),
    }));

  toggleSettingsDrawer = () => {
    this.setState({ settingDrawer: !this.state.settingDrawer });
  };

  toggleBoostModal = () => {
    this.setState({ boostModal: !this.state.boostModal });
  };

  _toggleEliteDrawer = () => this.setState({ _eliteDrawer: !this.state._eliteDrawer });

  /** this is preferences drawer */
  openProfileDrawer = () => {
    try {
      let selectedLocation = getCookie("selectedLocation");
      let homeAddr = getCookie("baseAddress");
      if (
        JSON.parse(selectedLocation) == null ||
        (JSON.parse(selectedLocation) == undefined && JSON.parse(homeAddr) == null) ||
        JSON.parse(homeAddr) == undefined
      ) {
        this.setState({ variant: "error", usermessage: "Please clear your cookies, and login and back / refresh the page", open: true });
      } else {
        this.setState({ Preferences: true });
      }
    } catch (e) {
      this.setState({ variant: "error", usermessage: "Please clear your cookies, and log back in / refresh the page", open: true });
      console.log("prevented app from crashing");
    }
  };

  closeProfileDrawer = () => {
    this.setState({ Preferences: false });
  };

  openCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: true });
  };

  closeCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: false });
  };

  handleChangeslider = (event, value) => {
    this.setState({ value });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Used to get switch tabs in homepage
  handleChange = (event, value) => {
    this.setState({ value });
  };

  // Function for the Model Toggle
  toggle() {
    this.setState((prevState) => ({
      modalsuperlike: !prevState.modalsuperlike,
    }));
  }

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  getDataWithAndWithoutScroll = (start, end) => {
    let usersArr = [...this.props.discoverPeopleList];
    searchUserByPagination(start, end)
      .then((res) => {
        console.log("calling from discover");
        let newData = [...usersArr, ...res.data.data];
        this.props.setAllDiscoverPeople(newData);
      })
      .catch((err) => {
        this.setState({ loader: true, loaderStatus: "Show More" });
      });
  };

  handleScroll = (event) => {
    var node = event.target;
    const bottom = node.scrollHeight - Math.round(node.scrollTop) === node.clientHeight;
    console.log(node.scrollHeight, Math.round(node.scrollTop), node.clientHeight);
    if (bottom && !this.state.viewMore) {
      if (!this.state.viewMore) {
        if (this.props.discoverPeopleList.length % 10 === 0) {
          this.setState(
            {
              start: this.state.start + 10,
              end: this.state.end,
              viewMore: false,
            },
            () => {
              this.getDataWithAndWithoutScroll(this.state.start, this.state.end);
            }
          );
        }
      }
    }
  };

  // Get Nearby People by this Fucntion
  handlenearbypeople = () => {
    // console.log("discover /View/MobileHomePage/Components/Discover/Discover.js");
    NearbyPeople().then((data) => {
      this.props.setAllDiscoverPeople(data.data.data);
      // this.setState({ UserSearchresult: data.data.data });
    });
  };

  // API Call to UnLike the User
  handledunlikeuser = (opponentId) => {
    let Unlikepayload = {
      targetUserId: opponentId,
    };
    let exData = [...this.props.discoverPeopleList];
    let index = exData.findIndex((k) => k.opponentId === opponentId || k._id === opponentId);
    exData.splice(index, 1);

    MyDisLikeUser(Unlikepayload)
      .then((data) => {
        // this.handlenearbypeople();
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User unlike successfully.",
        });
        this.props.setAllDiscoverPeople(exData);
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = (opponentId) => {
    let likepayload = {
      targetUserId: opponentId,
    };
    let exData = [...this.props.discoverPeopleList];
    let index = exData.findIndex((k) => k.opponentId === opponentId || k._id === opponentId);
    exData.splice(index, 1);
    LikeNearPeople(likepayload)
      .then((data) => {
        // this.handlenearbypeople();
        // console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "Profile has been liked successfully",
        });
        this.props.setAllDiscoverPeople(exData);
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to SuperLike the User
  handlesuperlikeuser = (opponentId) => {
    let superlikepayload = {
      targetUserId: opponentId,
    };
    let exData = [...this.props.discoverPeopleList];
    let index = exData.findIndex((k) => k.opponentId === opponentId || k._id === opponentId);
    console.log("clicked");
    SuperLikeUser(superlikepayload, getCookie("token"))
      .then((data) => {
        this.setState({ superLikeStatusCode: data.status });
        if (data.status === 200) {
          if (
            this.props.checkIfUserIsProUser &&
            this.props.checkIfUserIsProUser.ProUserDetails &&
            this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
            this.props.UserCoin > 0
          ) {
            this.props.coinReduce(parseInt(this.props.superLikeCost.Coin));
          }
          this.setState({
            open: true,
            variant: "success",
            usermessage: "User superlike successfully.",
          });
          exData.splice(index, 1);
          this.props.setAllDiscoverPeople(exData);
        } else {
          this.setState({ open: true, usermessage: data.data.message, variant: "error" });
        }
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to SuperLike the User (NO Coin Availble)
  handlesuperlikeusernoCoin = () => {
    this.toggle();
  };

  handleClick = (id) => {
    if (id !== this.state.activeLink) {
      this.setState({
        activeLink: id,
        shown: !this.state.shown,
      });
    }
  };

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  render() {
    const { links, activeLink } = this.state;
    var shown = {
      display: this.state.shown ? "block" : "none",
    };

    var hidden = {
      display: this.state.shown ? "none" : "flex",
    };

    const Preferencesdrawer = <Preferences onClose={this.closeProfileDrawer} handlenearbypeople={this.handlenearbypeople} />;
    const CoinBalancesdrawer = <CoinBalances onClose={this.closeCoinBalancesDrawer} />;

    return (
      <div className="px-0 col-12" style={{ overflowY: "hidden", overflowX: "hidden" }}>
        <MainDrawer width={400} onClose={this.closeProfileDrawer} onOpen={this.openProfileDrawer} open={this.state.Preferences}>
          {Preferencesdrawer}
        </MainDrawer>
        <MainDrawer width={400} onClose={this.toggleSettingsDrawer} onOpen={this.toggleSettingsDrawer} open={this.state.settingDrawer}>
          <MobileSettings onClose={this.toggleSettingsDrawer} />
        </MainDrawer>
        <MainDrawer width={400} onClose={this.closeCoinBalancesDrawer} onOpen={this.openCoinBalancesDrawer} open={this.state.CoinBalances}>
          {CoinBalancesdrawer}
        </MainDrawer>
        {/* Header Module */}
        <div className="m-0 p-3 row">
          {/* Left Side Prefernce Module */}
          <div className="p-0 col-4">
            <i
              className="fas fa-bars"
              onClick={this.openProfileDrawer}
              style={{
                fontSize: "25px",
                color: "#f80402",
              }}
            />
          </div>

          {/* Tabs that Switch Components */}
          <div className="p-0 col-4 text-center Mmain_tabs">
            <div className="d-inline-flex row">
              {links.map((link) => {
                return (
                  <div key={link.id} className="p-0 col-auto MSwitch_viewcontainer">
                    <ul>
                      <li
                        onClick={() => this.handleClick(link.id)}
                        className={link.className + (link.id === activeLink ? " activetab" : "")}
                      >
                        <i className={link.icon} />
                      </li>
                    </ul>
                  </div>
                );
              })}
            </div>
          </div>

          {/* Right Side Coin Module */}
          <div className="p-0 col-4 text-right">
            <div className="row">
              <div className="col-8" onClick={this.openCoinBalancesDrawer}>
                <div className="global_coinbalance">
                  <img src={Icons.BigHeart} className="img-fluid" alt="dollarimg" title="dollarimg" />
                  <p className="m-0">{this.renderCoins(this.props.CoinBalance)}</p>
                </div>
              </div>
              <div className="col-4 discover_profile_pic" onClick={this.toggleSettingsDrawer}>
                <img
                  style={{ position: "absolute", bottom: "-2px" }}
                  src={this.props && this.props.userProfilePicture}
                  alt={this.props.UserFinalData.firstName}
                  height={30}
                  width={30}
                />
              </div>
            </div>
          </div>
        </div>

        <div style={shown}>
          {/* toggle switch to left  view  */}
          <div className="pt-3 MSwipycard">
            {this.props && this.props.discoverPeopleList && this.props.discoverPeopleList.length > 0 ? (
              <SwiplebleCards
                handlesuperlikeuser={this.handlesuperlikeuser}
                handlelikeuser={this.handlelikeuser}
                handledunlikeuser={this.handledunlikeuser}
                toggleBoostModal={this.toggleBoostModal}
                UserSearchresult={this.props.discoverPeopleList}
                handlenearbypeople={this.handlenearbypeople}
                USERSELECTEDTOCHAT={this.props.USERSELECTEDTOCHAT}
                handlevalue={this.props.handlevalue}
                superLikeStatusCode={this.state.superLikeStatusCode}
              />
            ) : (
              <div className="nomorelike text-center">
                <img className="img-fluid" width={200} src={Icons.U_Visitor} alt="nolike" title="nolike" />
                <p>No Nearby People</p>
                <p>Sorry no user has nearby you as yet.</p>
              </div>
            )}
          </div>
        </div>

        {/* toggle switch to right  view -> Like Nearby People Module */}

        <div
          className={
            this.state.shown
              ? "m-0 MNearpeople_Scroll"
              : this.props.discoverPeopleList && this.props.discoverPeopleList.length < 5
              ? "m-0 MNearpeople_ScrollV2-grid"
              : "m-0 MNearpeople_ScrollV2-row"
          }
          style={hidden}
          ref={this.scrollRef}
        >
          {this.props.discoverPeopleList && this.props.discoverPeopleList.length > 0 ? (
            this.props.discoverPeopleList.map((data, index) => (
              <div key={index} className="m-2">
                <div className="Mnearpeople_card">
                  <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId}`}>
                    <div className="Muser_img">
                      <img src={data.profilePic} alt={data.firstName} title={data.firstName} className="img-fluid" onError={this.onError} />
                    </div>
                    <div className="Wuser_name">
                      <p className="m-0">
                        {data.firstName}, {data.age ? data.age.value : ""}
                      </p>
                    </div>
                  </Link>
                  <div className="Muser_action">
                    <img
                      src={Icons.NewDislike}
                      alt="dislike"
                      width={22}
                      height={22}
                      onClick={this.handledunlikeuser.bind(this, data.opponentId)}
                    />
                    <img
                      src={Icons.NewLike}
                      alt="dislike"
                      width={22}
                      height={22}
                      onClick={this.handlelikeuser.bind(this, data.opponentId)}
                    />
                    {this.props.CoinBalance ? (
                      <img src={Icons.NewSuperLike} width={22} height={22} onClick={this.handlesuperlikeuser.bind(this, data.opponentId)} />
                    ) : (
                      <img src={Icons.NewSuperLike} width={22} height={22} onClick={this.handlesuperlikeusernoCoin} />
                    )}
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="nomorelike text-center">
              <img className="img-fluid" width={200} src={Icons.U_Visitor} alt="nolike" title="nolike" />
              <p>No Nearby People</p>
              <p>Sorry no user has nearby you as yet.</p>
            </div>
          )}
        </div>

        {/* Snakbar Components */}
        <Snackbar
          timeout={2500}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleClose}
        />

        <MaterialModal
          isOpen={this.props.UnFilteredPlus && this.props.UnFilteredPlus.tag == "UnFiltered Elite" && this.props.UnFilteredPlus != null}
          toggle={() => this._toggleElitePopup(false)}
          width={"100%"}
        >
          <section className="_elite_modal pt-4 position-relative">
            <div className="text-right pr-4">
              <img src={Icons.WhiteCancel} alt="cancel" onClick={() => this._toggleElitePopup(false)} height={24} />
            </div>
            <div className="exclusive_tour">
              <img src={Icons.Exclusive_tour} alt="exclusive_tour" />
            </div>
            <div>UnFiltered Elite</div>
            <div className="text-center">
              <div>
                <img src={Icons.TabProspectsActive} alt="coins" height={20} />
              </div>
            </div>
            <div>
              <p className="mb-1">Today $ {this.props.UnFilteredPlus && this.props.UnFilteredPlus.cost} for</p>
              <p>first Year</p>
            </div>
            <div>
              <div>You get:</div>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Unlimited Likes</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>See Who Likes You</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Passport to Any US City</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Additional Matches</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Rewind Features</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Date Setup Features</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Video Chat</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Boost Profile Features</span>
              </p>
              <p>
                <img src={Icons.CheckMark} alt="check-mark" height={16} /> <span>Extra Preferences</span>
              </p>

              <button className="m_elite_buyNow" onClick={() => this._toggleElitePopup(true)}>
                CONTINUE
              </button>
            </div>
          </section>
        </MaterialModal>

        <MainDrawer width={400} onClose={this._toggleEliteDrawer} onOpen={this._toggleEliteDrawer} open={this.state._eliteDrawer}>
          <MobilePayPalPayment _storeElitePlanFunc={this._storeElitePlanFunc} isSubscription={true} onClose={this._toggleEliteDrawer} />
        </MainDrawer>

        {/* Model for the SuprLike the nearb People */}
        <MainModel isOpen={this.state.modalsuperlike} type="ActivedPlanModal">
          {/* Header Module */}
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="pt-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.toggle} />
              <div className="WCharge_coins">
                <i className="fas fa-coins" />
              </div>
              <h5>Oops coin get over.</h5>
              <p className="m-0">
                You have no more Credits left to Superlike the Profile. Please recharge your wallet to continue further.
              </p>
              <div className="Wcharge_wallet" onClick={this.openCoinBalancesDrawer}>
                <button>Buy Credits</button>
              </div>
            </div>
          </div>
        </MainModel>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    setAllDiscoverPeople: (data) => dispatch(discoverPeople(data)),
    storeElitePlan: (data) => dispatch(MOBILE_ACTION_FUNC("UnFilteredPlus", data)),
  };
};

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    CoinConfig: state.CoinConfig.CoinConfig,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
    discoverPeopleList: state.discoverPeople.discoverPeopleList,
    UnFilteredPlus: state.Main.UserData.UnFilteredPlus,
    checkIfUserIsProUser: state.ProUser,
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HeaderBody)));
