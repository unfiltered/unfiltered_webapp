// Main React Components
import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";

// Scss
import "./Discover.scss";

// Reusable Components
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MainModel from "../../../../Components/Model/model";

// Reactstrap Components
import { Card, Button } from "reactstrap";

// Material-Ui Components
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonPinIcon from "@material-ui/icons/PersonPin";

// React Swiple Card
import Swipeable from "react-swipy";

// Imported Components
import Preferences from "../Preferences/Preferences";
import CoinBalances from "../Coins/CoinBalance";
import UserProfile from "../../../../Components/Profile/Profile";

// Redux Components
import { connect } from "react-redux";

// imported Images
import dislike from "../../../../asset/images/error.png";
import repeat from "../../../../asset/images/repeat.png";
import star from "../../../../asset/images/star.png";
import like from "../../../../asset/images/like.png";
import thunder from "../../../../asset/images/bolt.png";
import DollarIcon from "../../../../asset/images/dollar.png";
import Novisitor from "../../../../asset/images/boy.png";

// Redux Components
import { NearbyPeople, MyDisLikeUser, LikeNearPeople, SuperLikeNearPeople, BuyNewCoins, CurrentCoinBalance } from "../../../../controller/auth/verification";
import { USER_PROFILE_ACTION_FUN, storeCoins } from "../../../../actions/UserProfile";
import { getCookie } from "../../../../lib/session";
const data = ["Alexandre", "Thomas", "Lucien"];

const styles = (theme) => ({
  slider: {
    padding: "22px 0px",
  },
  thumbIcon: {
    borderRadius: "50%",
  },
  thumbIconWrapper: {
    backgroundColor: "#fff",
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  rootrtabs: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  root: {
    flexGrow: 1,
    maxWidth: 500,
  },
});

class HeaderBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Preferences: false,
      CoinBalances: false,
      setting: false,
      UserUpdatedCoinBalance: "",
      value: 80,
      checkedA: true,
      checkedB: true,
      age: "",
      labelWidth: 0,
      activeTab: "1",
      UserSearchresult: "",
      shown: true,
      open: false,
      value: 0,
      modalsuperlike: false,
      shown: true,
      cards: [
        "First",
        "Second",
        "Third",
        "Fourth",
        "Fifth",
        "Six",
        "Seven",
        "Eight",
        "Ninght",
        "Ten",
        "Demo",
        "Demo1",
        "Demo2",
        "Demo3",
        "Demo4",
        "Demo5",
        "First",
        "Second",
        "Third",
        "Fourth",
        "Fifth",
        "Six",
        "Seven",
        "Eight",
        "Ninght",
        "Ten",
        "Demo",
        "Demo1",
        "Demo2",
        "Demo3",
        "Demo4",
        "Demo5",
      ],
      peopleimg: "https://cdn.kapwing.com/Group_565-Kgyd0cXCv.png",
      links: [
        {
          id: 1,
          icon: "fas fa-square-full",
          className: "MSwitch_view",
        },
        {
          id: 2,
          icon: "fas fa-th-large",
          className: "MSwitch_view",
        },
      ],
      activeLink: 1,
    };
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this.handlenearbypeople();
    this.HandleCoinBalance();
  }

  // Current COin Balance
  HandleCoinBalance = () => {
    let uid = getCookie("uid");
    CurrentCoinBalance(uid, getCookie("token")).then((data) => {
      this.setState(
        {
          UserUpdatedCoinBalance: data.data.walletEarningData[0].balance,
        },
        () => {
          console.log("coin", this.state.UserUpdatedCoinBalance);
          // this.props.dispatch(USER_PROFILE_ACTION_FUN("CoinBalance", this.state.UserUpdatedCoinBalance));
        }
      );
    });
  };

  remove = () =>
    this.setState(({ cards }) => ({
      cards: cards.slice(1, cards.length),
    }));

  openProfileDrawer = () => {
    this.setState({ Preferences: true });
  };

  closeProfileDrawer = () => {
    this.setState({ Preferences: false });
  };

  openCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: true });
  };

  closeCoinBalancesDrawer = () => {
    this.setState({ CoinBalances: false });
  };

  handleChangeslider = (event, value) => {
    this.setState({ value });
  };

  // Show & Hide Component Toggle
  toggleshowhide = () => {
    this.setState({
      shown: !this.state.shown,
    });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Used to get switch tabs in homepage
  handleChange = (event, value) => {
    this.setState({ value });
  };

  // Function for the Model Toggle
  toggle() {
    this.setState((prevState) => ({
      modalsuperlike: !prevState.modalsuperlike,
    }));
  }

  // Get Nearby People by this Fucntion
  handlenearbypeople = () => {
    NearbyPeople().then((data) => {
      this.setState({ UserSearchresult: data.data.data }, () => {
        console.log("userdata", this.state.UserSearchresult);
      });
    });
  };

  // API Call to UnLike the User
  handledunlikeuser = (data) => {
    let Unlikepayload = {
      targetUserId: data.opponentId,
    };
    MyDisLikeUser(Unlikepayload)
      .then((data) => {
        this.handlenearbypeople();
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User unlike successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = (data) => {
    let likepayload = {
      targetUserId: data.opponentId,
    };

    LikeNearPeople(likepayload)
      .then((data) => {
        this.handlenearbypeople();
        console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User like successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to SuperLike the User
  handlesuperlikeuser = (data) => {
    let superlikepayload = {
      targetUserId: data.opponentId,
    };

    SuperLikeNearPeople(superlikepayload)
      .then((data) => {
        this.handlenearbypeople();
        console.log("finaldata", data);
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User superlike successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  handleClick = (id) => {
    if (id != this.state.activeLink) {
      this.setState({
        activeLink: id,
        shown: !this.state.shown,
      });
    }
  };

  render() {
    console.log("asdadsssssssssssssss", this.props.UserProfile.CoinBalance);
    const { cards, peopleimg, links, activeLink } = this.state;

    var shown = {
      display: this.state.shown ? "block" : "none",
    };

    var hidden = {
      display: this.state.shown ? "none" : "flex",
    };

    const Preferencesdrawer = <Preferences onClose={this.closeProfileDrawer} />;
    const CoinBalancesdrawer = <CoinBalances onClose={this.closeCoinBalancesDrawer} />;
    console.log("bk.js /View/MobileHomePage/Components/Discover/bk.js");
    return (
      <div className="py-3 col-12">
        <MainDrawer width={400} onClose={this.closeProfileDrawer} onOpen={this.openProfileDrawer} open={this.state.Preferences}>
          {Preferencesdrawer}
        </MainDrawer>
        <MainDrawer width={400} onClose={this.closeCoinBalancesDrawer} onOpen={this.openCoinBalancesDrawer} open={this.state.CoinBalances}>
          {CoinBalancesdrawer}
        </MainDrawer>
        {/* Header Module */}
        <div className="m-0 pb-2 row">
          {/* Left Side Prefernce Module */}
          <div className="p-0 col-4">
            <i
              className="fas fa-bars"
              onClick={this.openProfileDrawer}
              style={{
                fontSize: "25px",
                color: "#F74167",
              }}
            />
          </div>

          {/* Tabs that Switch Components */}
          <div className="p-0 col-4 text-center Mmain_tabs">
            <div className="d-inline-flex row">
              {links.map((link) => {
                return (
                  <div key={link.id} className="p-0 col-auto MSwitch_viewcontainer">
                    <ul>
                      <li onClick={() => this.handleClick(link.id)} className={link.className + (link.id === activeLink ? " activetab" : "")}>
                        <i className={link.icon} />
                      </li>
                    </ul>
                  </div>
                );
              })}
            </div>
          </div>

          {/* Right Side Coin Module */}
          <div className="p-0 col-4 text-right">
            <div className="coinbalances" onClick={this.openCoinBalancesDrawer}>
              <img src={DollarIcon} className="img-fluid" alt="dollarimg" title="dollarimg" />
              <p className="m-0">{this.props.CoinBalance}</p>
            </div>
          </div>
        </div>

        <div style={shown}>
          {/* Main Card Module */}
          <div className="py-3 MSwipycard">
            {cards.length > 0 ? (
              <div>
                <Swipeable
                  buttons={({ left, right, top }) => (
                    <div className="swipe-buttons">
                      <Button onClick={left}>
                        <img src={repeat} alt="repeat" title="repeat" />
                      </Button>
                      <Button onClick={left}>
                        <img src={dislike} alt="dislike" title="dislike" />
                      </Button>
                      <Button onClick={top}>
                        <img src={star} alt="star" title="star" />
                      </Button>
                      <Button onClick={right}>
                        <img src={like} alt="like" title="like" />
                      </Button>
                      <Button onClick={right}>
                        <img src={thunder} alt="thunder" title="thunder" />
                      </Button>
                    </div>
                  )}
                  onAfterSwipe={this.remove}
                >
                  <Card>
                    <img src={peopleimg} className="mainpeopelimg" alt="peopleimgs" title="peopleimgs" />
                  </Card>
                </Swipeable>
              </div>
            ) : (
              <Card zIndex={-2}>No More Cards</Card>
            )}
          </div>
        </div>

        {/* Like Nearby People Module */}
        <div className="row MNearpeople_Scroll" style={hidden}>
          {this.state.UserSearchresult && this.state.UserSearchresult.length > 0 ? (
            this.state.UserSearchresult.map((data, index) => (
              <div key={index} className="py-2 col-6">
                <div className="Mnearpeople_card">
                  <Link to={`/app/mobile/user/${data.firstName}/${data.opponentId}`}>
                    <div className="py-3 Muser_img">
                      <img
                        src={
                          // data.otherImages.length > 0
                          //   ? data.otherImages[0]
                          //   : data.profilePic
                          data.profilePic
                        }
                        alt={data.firstName}
                        title={data.firstName}
                        className="img-fluid"
                      />
                    </div>
                    <div className="pb-2 Wuser_name">
                      <p className="m-0">
                        {data.firstName}, {data.age ? data.age.value : ""}
                      </p>
                    </div>
                  </Link>
                  <div className="Muser_action">
                    <i className={"fas fa-times " + data.opponentId} onClick={this.handledunlikeuser.bind(this, data)} />
                    <i className={"fas fa-heart " + data.opponentId} onClick={this.handlelikeuser.bind(this, data)} />
                    <i className={"fas fa-star " + data.opponentId} onClick={this.handlesuperlikeuser.bind(this, data)} />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="nomorelike text-center">
              <img className="img-fluid" width="15%" src={Novisitor} alt="nolike" title="nolike" />
              <p>No Nearby People</p>
              <p>Sorry no user has nearby you as yet.</p>
            </div>
          )}
        </div>

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />

        {/* Model for the SuprLike the nearb People */}
        <MainModel isOpen={this.state.modalsuperlike}>
          {/* Header Module */}
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="pt-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.toggle} />
              <div className="WCharge_coins">
                <i className="fas fa-coins" />
              </div>
              <h5>Oops coin get over.</h5>
              <p className="m-0">You have no more Credits left to Superlike the Profile. Please recharge your wallet to continue further.</p>
              <div className="Wcharge_wallet">
                <button>Buy Credits</button>
              </div>
            </div>
          </div>
        </MainModel>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
  };
};

// const mapDispatchToProps = dispatch => {
//   return {
//     storeCoins: (coins) => dispatch(storeCoins(coins))
//   }
// }

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(HeaderBody)));
