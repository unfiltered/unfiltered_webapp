import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import "./Cards.css";
import { ActivateBoost, GetRewindDetails } from "../../../../controller/auth/verification";
import Dislike from "../../../../asset/images/Cards/dislike.png";
import Boost from "../../../../asset/images/Cards/boost.png";
import Like from "../../../../asset/images/Cards/like.png";
import SuperLike from "../../../../asset/images/Cards/super_like.png";
import Undo from "../../../../asset/images/Cards/undo.png";
import { InitDoc } from "../../../../lib/CardsLib";
import Novisitor from "../../../../asset/images/boy.png";
import { connect } from "react-redux";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import * as actionChat from "../../../../actions/chat";
import { boostActivated } from "../../../../actions/boost";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";
import Snackbar from "../../../../Components/Snackbar/Snackbar";

const convertSecondsToMinutesTwoPads = (totalSeconds) => {
  totalSeconds %= 3600;
  let minutes = Math.floor(totalSeconds / 60);
  let seconds = totalSeconds % 60;
  minutes = String(minutes).padStart(2, "0");
  seconds = String(seconds).padStart(2, "0");
  return minutes + ":" + seconds;
};

class SwiplebleCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      friendchat: false,
      intervalId: "",
      countDown: "",
      boostModal: false,
      isActive: false,
      likeDuringBoost: [],
      usermessage: "",
      open: false,
      variant: "success",
      currentIdIndex: 0,
    };
  }

  timer = () => {
    var newCount = this.state.countDown - 1;
    if (newCount >= 0) {
      this.setState({ countDown: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  ActivateBoostFunc = () => {
    if (this.props.CoinBalance > 0) {
      let { boostProfileForADay } = this.props.CoinConfig;
      ActivateBoost()
        .then((res) => {
          console.log("boost activate");
          this.props.coinReduce(parseInt(boostProfileForADay.Coin));
          this.props.boostActivated(res.data);
          var intervalId = setInterval(this.timer, 1000);
          this.setState({ intervalId: intervalId, countDown: 299, isActive: true });
        })
        .catch((err) => {
          console.log("boost activate fail");
        });
    }
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  GetRewindDetailsFunc = async () => {
    let res = await GetRewindDetails();
    console.log("res", res);
    if (res && res.status === 200) {
      // this.props.handlenearbypeople();
      this.setState({ open: true, usermessage: "Rewind Successfull", variant: "success" });
      InitDoc(this.props.handlelikeuser, this.props.handledunlikeuser, this.props.handlesuperlikeuser, this.props.superLikeStatusCode);
    } else if ((res && res.status === 412) || res === undefined) {
      this.setState({ open: true, usermessage: "There are no previous rewinds...", variant: "error" });
    }
  };

  // On Component Mount this Call
  componentDidMount() {
    setTimeout(() => {
      // handlelikeuser={this.handlelikeuser} handledunlikeuser={this.handledunlikeuser}
      InitDoc(this.props.handlelikeuser, this.props.handledunlikeuser, this.props.handlesuperlikeuser, this.props.superLikeStatusCode);
    }, 500);
    var intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId });
  }

  static getDerivedStateFromProps(props, state) {
    if (props && props.boostActiveDetails && props.boostActiveDetails.expire) {
      let time = (props && props.boostActiveDetails && props.boostActiveDetails.expire - new Date().getTime()) / 1000;
      return {
        countDown: parseInt(time),
        isActive: true,
      };
    }
  }

  componentWillUnmount() {
    this.setState({ countDown: 0 });
    clearInterval(this.state.intervalId);
  }

  // Function to Open a Chat Drawer (User has Coin)
  openchatdrawer = () => {
    this.setState({ friendchat: true });
  };

  // Function to Open a Model (User has No Coin)
  openchatdrawernoCoin = () => {
    this.togglemodel();
  };

  // Function to Close a Chat Drawer
  toggleChatDrawer = () => {
    this.setState({ friendchat: !this.state.friendchat });
  };

  toggleBoostModal = () => {
    this.setState({ boostModal: !this.state.boostModal });
  };

  /** take user object and navigate to chat screen */
  selectedProfileForChatFunc = (item) => {
    this.props.selectedProfileForChat(item);
    this.props.history.push("/app/chat");
    this.props.handlevalue(3);
  };

  render() {
    return (
      <section>
        <div className="stage">
          <div id="stacked-cards-block" className="stackedcards stackedcards--animatable init">
            <div className="stackedcards-container">
              {this.props.UserSearchresult ? (
                this.props.UserSearchresult.map((item, index) => (
                  <div key={index} className="card" id={item.opponentId}>
                    <div className="card-content" id={item.opponentId}>
                      <div className="card-image" id={item.opponentId}>
                        <img alt={item.firstName} src={item.profilePic} width="100%" height="100%" />
                        {/* Action For User Profile */}
                        <Link to={`/app/mobile/user/${item.firstName}/${item.opponentId}`} style={{ zIndex: 999 }}>
                          <i className="fas fa-exclamation MOtherUser_cardinfo" />
                        </Link>
                        {this.props.CoinBalance ? (
                          <i className="fas fa-comment-alt MOtherUser_cardchat" onClick={() => this.selectedProfileForChatFunc(item)} />
                        ) : (
                          <i onClick={this.openchatdrawernoCoin} className="fas fa-comment-alt MOtherUser_cardchat" />
                        )}
                        <p className="m-0 MOtherUser_cardname">
                          {item.firstName}, {item.age ? item.age.value : ""}{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="nomorelike text-center">
                  <img className="img-fluid" width="15%" src={Novisitor} alt="nolike" title="nolike" />
                  <p>No Nearby People</p>
                  <p>Sorry no user has nearby you as yet.</p>
                </div>
              )}
            </div>

            <div className="stackedcards--animatable stackedcards-overlay top">
              <img src="https://image.ibb.co/m1ykYS/rank_army_star_2_3x.png" width="auto" height="auto" alt="star" />
            </div>
            <div className="stackedcards--animatable stackedcards-overlay right">
              <img src="https://image.ibb.co/dCuESn/Path_3x.png" width="auto" height="auto" alt="love" />
            </div>
            <div className="stackedcards--animatable stackedcards-overlay left">
              <img src="https://image.ibb.co/heTxf7/20_status_close_3x.png" width="auto" height="auto" alt="cancel" />
            </div>
          </div>

          {/* Action Button's */}
          <div className="global-actions pt-2">
            <div
              className="revert-action"
              onClick={() => {
                this.GetRewindDetailsFunc();
              }}
            >
              <img src={Undo} width={22} height={22} alt="undo" />
            </div>

            <div className="left-action" onClick={this.handledunlikeuser}>
              <img src={Icons.NewDislike} width={65} height={65} alt="cancel" />
            </div>
            {this.state.countDown > 0 && this.state.countDown != NaN && this.state.isActive ? (
              <div className="m_timer">
                {this.props && this.props.boostViews && this.props.boostViews.views >= 0
                  ? this.props.boostViews.views
                  : this.props.boostActiveDetails && this.props.boostActiveDetails.views}{" "}
                views
              </div>
            ) : (
              ""
            )}

            <div className="revert-action" onClick={!this.state.isActive ? this.toggleBoostModal : ""}>
              <img src={Boost} width={22} height={22} alt="boost" />
            </div>

            <div className="right-action">
              <img src={Icons.NewLike} width={65} height={65} alt="like" />
            </div>

            <div
              className="top-action"
              onClick={() => this.props.handlesuperlikeuser(this.props.UserSearchresult[this.state.currentIdIndex].opponentId)}
            >
              <img src={Icons.NewSuperLike} width={50} height={50} alt="superlike" />
            </div>
          </div>
        </div>

        <div className="final-state text-center hidden">
          <h2>No NearBy People.</h2>
        </div>
        <MaterialModal toggle={this.toggleBoostModal} isOpen={this.state.boostModal} width={"90%"}>
          <div className="col-12 m_boostModal">
            <div onClick={this.toggleBoostModal}>
              <img src={Icons.PinkCloseBtn} alt="close-btn" height={18} width={18} />
            </div>
            <div>
              <img src={Icons.SpendCoins} alt="Credits" height={120} width={100} />
            </div>
            <div>Spend 5 Credits to boost your profile.</div>
            <Button
              handler={() => {
                this.ActivateBoostFunc();
                this.toggleBoostModal();
              }}
              text="I am Ok to spend 5 Credits"
            />
          </div>
        </MaterialModal>
        <Snackbar
          type={this.state.variant}
          message={this.state.usermessage}
          timeout={2500}
          open={this.state.open}
          onClose={this.handleClose}
        />
      </section>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    boostActiveDetails: state.Boost.BoostDetailsOnRefresh,
    boostViews: state.Boost.BoostActivatedSessionIncomingData,
    CoinConfig: state.CoinConfig.CoinConfig,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    boostActivated: (data) => dispatch(boostActivated(data)),
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SwiplebleCards));
