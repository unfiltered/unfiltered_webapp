import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import _1 from "../../../../asset/mobile_img/1.png";
import _2 from "../../../../asset/mobile_img/2.png";
import _3 from "../../../../asset/mobile_img/3.png";
import _4 from "../../../../asset/mobile_img/4.png";
import _5 from "../../../../asset/mobile_img/5.png";
import _6 from "../../../../asset/mobile_img/6.png";
import "../Settings/Setings.scss";
// import MobilePayPalPayment from "../Coins/WalletScreens/MobilePayPalPayment";

function PremiumUserContent({ toggle, storePlanDetails, togglePaymentPage, allPlans }) {
  const [selectedPlan, setSelectedPlan] = useState("");

  const selectedPlanFn = (k, i) => {
    setSelectedPlan(k);
    storePlanDetails(k, i);
  };
  var settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 1500,
    autoplaySpeed: 1500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div className="py-4 px-2">
      <Slider {...settings}>
        <div>
          <img src={_1} className="img-circle m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>
        <div>
          <img src={_2} className="img-fluid m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>
        <div>
          <img src={_3} className="img-fluid m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>
        <div>
          <img src={_4} className="img-fluid m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>
        <div>
          <img src={_5} className="img-fluid m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>
        <div>
          <img src={_6} className="img-fluid m-auto" alt="Group4" title="Group4" width="150px" />
          <div className="py-3 text-center sl_header">
            <p className="m-0">
              <b>Swipe Around the World</b>
            </p>
            <p className="m-0">Swipe right as much you want!</p>
          </div>
        </div>{" "}
      </Slider>
      <div className="pt-5 col-12 MDatumupgrade">
        <div className="row align-items-center m_premium_plan_row">
          {/* {selectedPlan === "Weekly" ? <div className="weeklyPlan">Most Popular</div> : <div className="weeklyPlan_hidden" />} */}
          {allPlans &&
            Object.keys(allPlans).map((k, i) => (
              <div
                className={`${k === selectedPlan ? "popularPlan" : "notPopularPlan"}`}
                key={i}
                onClick={() => {
                  selectedPlanFn(k, i);
                }}
              >
                <p>{k}</p>
                <p>Subscription</p>
              </div>
            ))}
        </div>
      </div>
      <div className={selectedPlan ? "py-1 col-12 Mupgradebtn" : "py-1 col-12 Mupgradebtn__disabled"}>
        <button
          disabled={!selectedPlan}
          onClick={() => {
            setTimeout(() => {
              toggle();
            }, 300);
            togglePaymentPage();
          }}
        >
          Subscribe
        </button>
        <p className="m_prospects_no_thanks" onClick={toggle}>
          No Thanks
        </p>
      </div>
    </div>
  );
}

export default PremiumUserContent;
