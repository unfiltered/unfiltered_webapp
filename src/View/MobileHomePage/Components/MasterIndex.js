// Main React Components
import React from "react";

function MasterIndex({ children }) {
  return <div>{children}</div>;
}

export default MasterIndex;
