import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  progress: {
    color: "#e31b1b",
    marginTop: "45vh",
    textAlign: "center",
  },
};

class SafetyTips extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
    };
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 50000);
  }

  render() {
    return (
      <div>
        <div className="pt-3 px-3 MCommonwidth">
          <i
            className="fa fa-arrow-left"
            onClick={this.props.onClose}
            style={{
              fontSize: "24px",
              color: "#F74167",
            }}
          />

          <div className="py-3 MCommonheader">
            <p className="m-0">Safety</p>
            <p className="m-0">Tips</p>
          </div>
          <div>
            <h5>Dating Safely.</h5>
            <p>
              DatesnDime brings people together. With more than 20 million matches made to date and millions of new matches made daily, our
              community is constantly growing. With so many people on DatesnDime, user safety is a priority. We understand that meeting
              someone for the ﬁrst time whether online, through an acquaintance or on an outing is intriguing and exciting
            </p>
            <p>
              However, your safety is very important, and because you are in control of your DatesnDime experience, there are certain safety
              steps that you should follow while dating – both online and oﬄine.
            </p>
            <p>
              We ask you to read the tips and information below, and strongly urge you to follow these guidelines in the interest of your
              personal safety and well-being. However, you are always the best judge of your own safety, and these guidelines are not
              intended to be a substitute for your own judgment and Online Behavior.
            </p>
            <h5>Protect Your Finances & Never Send Money or Financial Information</h5>
            <p>
              Never respond to any request to send money, especially overseas or via wire transfer, and report it to us immediately – even
              if the person claims to be in an emergency. Wiring money is like sending cash: the sender has no protections against loss and
              it’s nearly impossible to reverse the transaction or trace the money. For more information, click on the video below to the
              U.S. Federal Trade Commission's advice to avoid online dating scams, also available here. Protect
            </p>
            <h5>Your Personal Information</h5>
            <p>
              Never give personal information, such as: your social security number, credit card number or bank information, or your work or
              home address to people you don’t know or haven’t met in person. Note: DatesnDime will never send you an email asking for your
              username and password information. Any such communication should be reported immediately.
            </p>
            <p>
              Be Web Wise Block and report suspicious users. You can block and report concerns about any suspicious user anonymously at any
              time on DatesnDime – while swiping or after you’ve matched. Keep conversations on the platform. Bad actors will try to move
              the conversation to text, personal email or phone conversations.
            </p>
            <p>
              Report All Suspicious Behavior Additionally, please report anyone who violates our terms of use. Examples of terms of use
              violations include: Asks you for money or donations Requesting photographs. Minors using the platform Users sending harassing
              or offensive messages. Users behaving inappropriately after meeting in person fraudulent registration or proﬁles
            </p>
            <p>Spam or solicitation, such as invitations to call 1-900 numbers or attempts to sell products or services.</p>
            <p>
              Oﬄine Behavior First meetings are exciting, but always take precautions and follow these guidelines to help you stay safe: Get
              to Know the Other Person Keep your communications limited to the platform and really get to know users online/ using the app
              before meeting them in person. Bad actors often push people to communicate off the platform immediately. It’s up to you to
              research and do your due diligence.
            </p>
            <p>
              Always Meet and Stay in Public Meet for the ﬁrst time in a populated, public place – never in a private or remote location,
              and never at your home or apartment. If your date pressures you, end the date and leave at once. Tell Your Friends and Family
              Members of Your Plans Inform a friend or family member of your plans and when and where you’re going. Make sure you have your
              cell phone charged and with you at all times.
            </p>
            <p>
              Transport yourself to and from the Meeting You need to be independent and in control of your own transportation, especially in
              case things don’t work out.
            </p>
            <p>
              Stay Sober Consumption of alcohol and/or other drugs can impair your judgment and potentially put you in danger. It’s
              important to keep a clear mind and avoid anything that might place you at risk. Be aware that bad actors might try to take
              advantage of you by altering your beverage(s) with synthetic substances.
            </p>
            <p>
              DatesnDime welcomes everyone and empowers our community of users to create and cultivate relationships. An important aspect of
              any healthy relationship though – whether formed on DatesnDime or otherwise – is ensuring proper sexual health and safety. And
              as a member of the DatesnDime community it is your responsibility to make sure you do the following, if you choose to engage
              in sexual activity. Protect Yourself. You and your partner should use proper protection. Condoms and other mechanisms can
              signiﬁcantly reduce the risk of contracting or passing on an STI, such as HIV. To be effective, however, protective measures
              must be used consistently. Please keep in mind; you can still get certain STIs, like herpes or HPV from contact with your
              partner’s skin even when using a condom. Be Open and Honest It is completely reasonable to have a conversation with your
              partner regarding sex and sexual contact before actually having it. All issues ranging from the number of partners each of you
              has had, to the last time each of you was tested for STIs are fair game. Many STIs are curable or treatable. If either you or
              your partner has an STI that is curable, you both need to start treatment to avoid becoming re-infected. It is important to be
              completely honest in these conversations. Vaccinate the risk of contracting some STIs can be reduced through vaccination. Talk
              to your doctor or a professional at a sexual health clinic to learn more.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SafetyTips);
