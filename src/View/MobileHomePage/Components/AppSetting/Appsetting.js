// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import * as func from "../../../../init-fcm";
import "./Appsettings.scss";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Slide from "@material-ui/core/Slide";
import CommunityGuide from "./CommunityGuide";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { removeCookie, getCookie } from "../../../../lib/session";
import SafetyTips from "./SafetyTips";
import { logoutFromApp, goOfflineSubscribeToTopic } from "../../../../lib/MqttHOC/utils";
import { Icons } from "../../../../Components/Icons/Icons";
import CoinBalances from "../Coins/CoinBalance";

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Appsetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Community: false,
      open: false,
      safetyTips: false,
    };
    this.HandleSubmit = this.HandleSubmit.bind(this);
  }

  // Function for the Open Toggle
  handleClickOpen = () => {
    this.setState({ open: true });
  };

  // Function for the Close Toggle
  handleClose = () => {
    this.setState({ open: false });
  };

  // Function to open Community Drawer
  opencommunityDrawer = () => {
    this.setState({ Community: true });
  };

  // Function to Close Community Drawer
  closecommunityDrawer = () => {
    this.setState({ Community: false });
  };

  toggleSafetyTips = () => {
    this.setState({ safetyTips: !this.state.safetyTips });
  };

  HandleSubmit() {
    // func.logoutFirebase();
    removeCookie("token");
    removeCookie("SignUpseemeid");
    removeCookie("findMateId");
    // removeCookie("location");
    // removeCookie("citylocation");
    // removeCookie("lat");
    // removeCookie("long");
    goOfflineSubscribeToTopic(this.state.userID);
    logoutFromApp(getCookie("uid"));

    this.props.history.push("/");
  }

  navigateToPrivacyPolicy = () => {
    window.open("/privacy-policy", "_blank");
  };

  navigateToTermsAndConditions = () => {
    window.open("/terms-and-conditions", "_blank");
  };

  navigateToCookiePolicy = () => {
    window.open("/cookie-policy", "_blank");
  };

  render() {
    console.log("asdad", this.props);
    const Preferencesdrawer = <CommunityGuide onClose={this.closecommunityDrawer} />;
    const SafetyTipsDrawer = <SafetyTips onClose={this.toggleSafetyTips} />;
    return (
      <div style={{ height: "100vh" }}>
        <MainDrawer width={400} onClose={this.closecommunityDrawer} onOpen={this.opencommunityDrawer} open={this.state.Community}>
          {Preferencesdrawer}
        </MainDrawer>
        <MainDrawer width={400} onClose={this.toggleSafetyTips} onOpen={this.toggleSafetyTips} open={this.state.safetyTips}>
          {SafetyTipsDrawer}
        </MainDrawer>

        <div className="pt-3 px-3 MCommonwidth">
          <img src={Icons.PinkBack} height={20} width={25} onClick={this.props.onClose} />
          <div className="py-3 MCommonheader">
            <p />
            <FormattedMessage tagName="p" id="message.appsettingtitle" />
          </div>
        </div>

        <div className="col-12">
          <div className="Mcommunity">
            <h4 className="py-2 border-bottom">
              <FormattedMessage tagName="h4" id="message.appsettingtitle1" />
            </h4>

            <div className="row py-2">
              <div className="col-9 text-left" onClick={this.opencommunityDrawer}>
                <FormattedMessage tagName="p" id="message.appsettingtitle1firsttitle" />
              </div>
              <div className="col-3 text-right">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
            <div className="row py-2">
              <div className="col-9 text-left" onClick={this.toggleSafetyTips}>
                <FormattedMessage tagName="p" id="message.appsettingtitle1secoundttitle" />
              </div>
              <div className="col-3 text-right">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
          </div>
        </div>

        <div className="col-12">
          <div className="Mcommunity py-3">
            <h4 className="py-2 border-bottom">
              <FormattedMessage tagName="h4" id="message.appsettingtitle2" />
            </h4>

            <div className="row py-2">
              <div className="col-9 text-left" onClick={this.navigateToPrivacyPolicy}>
                <FormattedMessage tagName="p" id="Privacy Policy" />
              </div>
              <div className="col-3 text-right">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
            <div className="row py-2">
              <div className="col-9 text-left" onClick={this.navigateToTermsAndConditions}>
                <FormattedMessage tagName="p" id="Terms of Services" />
              </div>
              <div className="col-3 text-right">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
            <div className="row py-2">
              <div className="col-9 text-left" onClick={this.navigateToCookiePolicy}>
                <FormattedMessage tagName="p" id="Licenses" />
              </div>
              <div className="col-3 text-right">
                <i class="fas fa-chevron-right"></i>
              </div>
            </div>
          </div>
        </div>

        {/** Logout and Delete Account Buttons */}
        <div className="col-12">
          <p className="py-2 px-2 m-0 text-center border border-secondary exit-buttons rounded">
            <div className="m_s_exitButtons" onClick={this.handleClickOpen}>
              Logout
            </div>
          </p>
          <div className="py-2 text-center">
            <img src={Icons.cmeLogo} alt="Mainlogo" title="Mainlogo" width="120px" />
          </div>
          <p className="py-2 px-2 m-0 text-center border border-secondary exit-buttons rounded">
            <div className="m_s_exitButtons">Delete Account</div>
          </p>
        </div>

        {/* Report User Module (POP-UP) */}
        <Dialog
          open={this.state.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent className="p-3">
            <DialogContentText id="alert-dialog-slide-description" style={{ fontFamily: "Product Sans" }}>
              Are you sure want to log out? you will continue to be seen by compatible users in your last known location
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.HandleSubmit} color="primary">
              <p style={{ fontFamily: "Product Sans" }} className="m-0 text-danger">
                Logout
              </p>
            </Button>
            <Button onClick={this.handleClose} color="primary">
              <p style={{ fontFamily: "Product Sans" }} className="m-0 text-secondary">
                Cancel
              </p>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapstateToProps = function (state) {
  return {};
};

export default withRouter(connect(mapstateToProps, null)(Appsetting));
