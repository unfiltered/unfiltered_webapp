import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  progress: {
    color: "#e31b1b",
    marginTop: "45vh",
    textAlign: "center",
  },
};

class Community extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
    };
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 50000);
  }

  render() {
    return (
      <div>
        <div className="pt-3 px-3 MCommonwidth">
          <i
            className="fa fa-arrow-left"
            onClick={this.props.onClose}
            style={{
              fontSize: "24px",
              color: "#F74167",
            }}
          />

          <div className="py-3 MCommonheader">
            <p className="m-0">Community</p>
            <p className="m-0">Guidelines</p>
          </div>
          <div>
            <h5> Welcome to the DatesnDime community.</h5>
            <p>
              If you’re honest, kind and respectful to others, you’ll always be welcome here. If you choose not to be, you may not last. Our
              goal is to allow users to express themselves freely as long as it doesn’t offend others. Everyone is held to the same standard
              on DatesnDime. We’re asking you to be considerate, think before you act, and abide by our community guidelines both on and
              oﬄine. Seriously, don’t make us Swipe Left on you—because there will be no do-overs once we do because you’re blocked forever.
            </p>
            <h5>Nudity or Sexual Content</h5>
            <p>
              DatesnDime wasn’t invented to show off your six most provocative photos. Seriously We’re not asking you to comb your hair to
              one side and put on your Sunday best, just try to keep it classy and appropriate for public consumption. No nudity, no
              sexually explicit content, no sex toys. Also, if you choose to sport a thong, please note: suggestive poses are not allowed on
              DatesnDime. (If it’s in the Kama sutra, it probably shouldn’t be in your proﬁle pic.) Photos that violate these guidelines may
              be deleted, and the most severe cases may result in account removal. Inappropriateness If you wouldn’t say it to someone’s
              face, don’t say it on DatesnDime. Keep in mind, there is a human being on the other end of your screen. Be respectful with
              anyone you interact with in the DatesnDime community. If you are disrespectful, offensive, threatening or rude to another
              user, we reserve the right to ban you from DatesnDime.
            </p>
            <h5>Violent or Graphic Content</h5>
            <p>
              We do not tolerate violent, graphic or obscene photos or proﬁles containing hate speech on DatesnDime, and will remove any
              such content that we identify. Any content that we deem disrespectful to others in the community may be removed, and the user
              banned, depending on its severity. With this in mind, please exercise restraint and respect for others in the community when
              posting hunting photos, as many users ﬁnd images of dead animals disturbing.
            </p>
            <h5>Hate</h5>
            <p>
              We have a zero-tolerance policy on hate speech. Any content that promotes or condones violence against individuals or groups
              based on race or ethnicity, religion, disability, gender, age, nationality, sexual orientation or gender identity is strictly
              forbidden and may result in you being permanently banned. Children Unattended Standard supermarket rules apply. No children
              unaccompanied by an adult — in this case, we’re talking about your proﬁle photos. Even if it’s you at age seven and it’s the
              cutest you ever. This is to protect kids. Their safety is more important than your adorableness of years past. We reserve the
              right to remove these photos.
            </p>
            <h5>Harassment</h5>
            <p>
              Any reports of stalking, threats, bullying, intimidation, invading privacy, or revealing other people's personal information
              are taken very seriously. That means no taking screenshots of other people’s proﬁles or conversations and posting or
              distributing them publicly. DatesnDime reserves the right to ban any user that has exhibited a behavior that is threatening,
              intentionally malicious, or harmful to other users. Children Unattended Standard supermarket rules apply. No children
              unaccompanied by an adult — in this case, we’re talking about your proﬁle photos. Even if it’s you at age seven and it’s the
              cutest you ever had. This is to protect kids. Their safety is more important than your adorableness of years past. We reserve
              the right to remove these photos.
            </p>
            <h5>Scamming</h5>
            <p>
              DatesnDime has a zero-tolerance policy on predatory behavior of any kind. Anyone attempting to get other users’ private
              information for fraudulent or illegal activity may be banned. Also, any user caught sharing their own ﬁnancial account
              information (PayPal, Venmo, etc.) For the purpose of receiving money from other users may be banned from DatesnDime. All gifts
              should be shared through DatesnDime.
            </p>
            <h5>Copyright and Infringement</h5>
            <p>
              If it doesn’t belong to you, it doesn’t belong on your DatesnDime proﬁle. Any of your proﬁle content that someone else owns
              the copyright to (and has not granted you authorization to use) may be deleted. Phone Numbers or Private Information No, we
              don’t need your digits. Please do not publish a phone number on your proﬁle. DatesnDime invented the double opt-in, (two
              people have to swipe right on each other to connect), so that personal information can be shared privately, if desired, and
              not broadcast publicly. Private information of any sort, such as phone numbers, social security numbers, addresses, passwords
              or ﬁnancial information is subject to removal and may result in you being banned from DatesnDime
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Community);
