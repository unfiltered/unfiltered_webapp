import React from "react";
import "../Friends/Friends.scss";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import CustomButton from "../../../../Components/Button/Button";
import moment from "moment";
import TextField from "@material-ui/core/TextField";
import { PendingDates, PastDates, RescheduleDate } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import * as actionChat from "../../../../actions/chat";
import { connect } from "react-redux";
import Snackbar from "../../../../Components/Snackbar/Snackbar";

class RescheduleDateScreen extends React.Component {
  state = {
    upcomingdates: [],
    pendingdates: [],
    pastdates: [],
    modal: false,
    date: "",
    time: "",
    open: false,
    usermessage: "",
    variant: "warning",
  };
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };
  toggleAddFriendDrawer = () => {
    this.setState({ addFriendDrawer: !this.state.addFriendDrawer });
  };
  componentDidMount() {
    PendingDates()
      .then((data) => {
        this.setState({
          upcomingdates: data.data.data.upcomingDates,
        });

        this.setState({
          pendingdates: data.data.data.pendingDates,
        });
      })
      .catch((err) => this.setState({ pendingdates: [], upcomingDates: [] }));

    PastDates()
      .then((data) => {
        this.setState({ pastdates: data.data.data });
      })
      .catch((err) => this.setState({ pastdates: [] }));
    let time = `${moment(this.props.dateData.proposedOn).format("HH")}:${moment(this.props.dateData.proposedOn).format("mm")}`.toString();
    let d = moment(this.props.dateData.proposedOn).format("DD");
    let m = moment(this.props.dateData.proposedOn).format("MM");
    let y = moment(this.props.dateData.proposedOn).format("YYYY");
    let date = `${y}-${m}-${d}`.toString();
    this.setState({ time, date });
  }

  getCurrentTime = () => {
    let time = `${moment(new Date().getTime()).format("HH")}:${moment(new Date().getTime()).format("mm")}`.toString();
    return time;
  };

  getCurrentDate = () => {
    let d = moment(new Date().getTime()).format("DD");
    let m = moment(new Date().getTime()).format("MM");
    let y = moment(new Date().getTime()).format("YYYY");
    let date = `${y}-${m}-${d}`.toString();
    return date;
  };

  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  againRescheduleDate = (input) => {
    let PLAN =
      this.props.checkIfUserIsProUser &&
      this.props.checkIfUserIsProUser.ProUserDetails &&
      this.props.checkIfUserIsProUser.ProUserDetails.tag;
    let { resheduleDate } = this.props.CoinConfig;
    this.props.setDateType(3, this.props.dateData);
    let dateTime = `${this.state.date}T${this.state.time}`.toString();
    RescheduleDate(
      getCookie("token"),
      input,
      this.props && this.props.dateData && this.props.dateData.data_id,
      moment(dateTime).valueOf(),
      this.props && this.props.dateData && this.props.dateData.dateType,
      this.props && this.props.dateData && this.props.dateData.latitude,
      this.props && this.props.dateData && this.props.dateData.longitude,
      this.props && this.props.dateData && this.props.dateData.placeName,
      PLAN
    )
      .then((res) => {
        if (res.status === 200) {
          this.props.setIsCallbackAfterAPISuccess(true);
          this.props.coinReduce(parseInt(resheduleDate.Coin));
          this.props.onClose();
          setTimeout(() => {
            this.props.setIsCallbackAfterAPISuccess(false);
          }, 800);
        } else {
          this.setState({ open: true, usermessage: "Please check time or Date", variant: "warning" });
          console.log("something went wrong", res);
        }
      })
      .catch((err) => {
        this.setState({ open: true, usermessage: "Something went wrong", variant: "error" });
      });
    setTimeout(() => {
      this.setState({ open: false });
    }, 2500);
  };

  render() {
    return (
      <div className="col-12 rescheduleDateScreen">
        <div className="row">
          <div className="col-12 py-3">
            <div className="row">
              <div className="col-10 label-common2">Plan {this.props.dateData.requestedFor}</div>
              <div className="col-2 friends_header" onClick={this.props.onClose}>
                <img src={Icons.PinkCloseBtn} alt="close" height={20} width={20} />
              </div>
            </div>
          </div>
          <div className="col-12 py-3 px-5 text-center rescheduleDateText">
            Suggest a new place and time to meet. <br />
            We'll notify you if {this.props.dateData.opponentName} confirms.
          </div>
          <div className="col-12 d-flex py-3 d_physical_couples_img rescheduleDatehearts">
            <div>
              <img
                src={this.props && this.props.dateData && this.props.dateData.opponentProfilePic}
                className="d_physical_date_img"
                alt={this.props.dateData.opponentProfilePic}
              />
            </div>
            <div>
              <img
                src={this.props && this.props.UserFinalData && this.props.UserFinalData.profilePic}
                className="d_physical_date_img"
                alt={this.props.UserFinalData.profilePic}
              />
            </div>
            <span className="heart1">
              <img src={Icons.Heart1} alt="heart" />
            </span>
            <span className="heart2">
              <img src={Icons.Heart2} alt="heart" />
            </span>
            <span className="heart3">
              <img src={Icons.Heart3} alt="heart" />
            </span>
            <span className="heart4">
              <img src={Icons.Heart4} alt="heart" />
            </span>
          </div>
          <div className="col-10 pt-5">
            <TextField
              id="date"
              label="Date"
              type="date"
              onChange={this.handleChange("date")}
              value={this.state && this.state.date}
              placeholder={this.state && this.state.date}
              defaultValue={this.state && this.state.date}
              className="datesTextField"
            />
          </div>
          <div className="col-10 pt-3">
            <TextField
              id="time"
              label="Time"
              type="time"
              onChange={this.handleChange("time")}
              value={this.state && this.state.time}
              defaultValue={this.state && this.state.time}
              className="datesTextField"
            />
          </div>
        </div>
        <CustomButton
          text={<img src={Icons.DateConfirmBtn} alt="confirm-btn" width={75} height={75} />}
          className="confirmDateBtn"
          handler={this.toggleModal}
        />
        <MaterialModal
          width={"70%"}
          isOpen={this.state.modal}
          toggle={() => {
            this.toggleModal();
            this.setState({ time: this.getCurrentTime(), date: this.getCurrentDate() });
          }}
        >
          <div className="col-12 p-3" style={{ position: "relative" }}>
            <div
              className="rescheduleConfirmPopupModalCloseBtn"
              onClick={() => {
                this.toggleModal();
                this.setState({ time: this.getCurrentTime(), date: this.getCurrentDate() });
              }}
            >
              <img src={Icons.PinkCloseBtn} height={20} width={20} alt="close-btn" />
            </div>
            <div className="row justify-content-center">
              <img src={Icons.SpendCoins} alt="gold-coins" height={160} width={130} />
            </div>
            <div className="row">
              <div className="col-12 rescheduleDateHeader">Give away Credits to reschedule date.</div>
            </div>
            <div className="row">
              <div className="col-12 rescheduleDateSubText">Spend some coin to reschedule existing Date</div>
            </div>
            <div className="row justify-content-center py-3">
              <CustomButton
                text={"I am OK to spend 1 coin"}
                className="rescheduleDateConfirmBtn"
                handler={() => this.againRescheduleDate(3)}
              />
            </div>
          </div>
        </MaterialModal>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
  };
};

const mapStateToProps = function (state) {
  return {
    CoinConfig: state.CoinConfig.CoinConfig,
    checkIfUserIsProUser: state.ProUser,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RescheduleDateScreen);
