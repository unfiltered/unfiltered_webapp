import React from "react";
import "../Friends/Friends.scss";
import moment from "moment";
import SimpleTabs from "../../../../Components/ScrollbleTabs/Scrollble_tabs";
import MaterialModal from "../../../../Components/Model/MAT_UI_Modal";
import { Icons } from "../../../../Components/Icons/Icons";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import MobileSettings from "../Settings/Setings";
import { PendingDates, PastDates, RescheduleDate } from "../../../../controller/auth/verification";
import MobileRescheduleDate from "./RescheduleDate";
import { getCookie } from "../../../../lib/session";
import { connect } from "react-redux";
import CoinBalances from "../Coins/CoinBalance";
import { Images } from "../../../../Components/Images/Images";
import Button from "../../../../Components/Button/Button";
import { __callInit } from "../../../../actions/webrtc";
import * as actions from "../../../../actions/UserSelectedToChat";
import { SELECTED_PROFILE_ACTION_FUN } from "../../../../actions/selectedProfile";

class MobileDates extends React.Component {
  state = {
    upcomingdates: [],
    pendingdates: [],
    pastdates: [],
    modal: false,
    dateData: {},
    rescheduleDateScreen: false,
    isCallbackAfterAPISuccess: false,
    setDateAction: "",
    acceptRejectModal: false,
    input: "",
    coinDrawer: false,
    value: 0,
  };

  setValue = (val) => this.setState({ value: val });

  toggleCoinDrawer = () => {
    this.setState({ coinDrawer: !this.state.coinDrawer });
  };

  toggleAcceptRejectModal = () => {
    this.setState({ acceptRejectModal: !this.state.acceptRejectModal });
  };

  setDateType = (input, data) => {
    console.log("input, data", input, data);
    if (input === 1) {
      this.setState({ setDateAction: "Accept", input: 1, dateData: data });
      this.toggleAcceptRejectModal();
    } else if (input === 2) {
      this.setState({ setDateAction: "Reject", input: 2, dateData: data });
      this.toggleAcceptRejectModal();
    } else if (input === 3) {
      this.setState({ setDateAction: "Reschedule" });
    }
  };

  setIsCallbackAfterAPISuccess = (boolean) => {
    this.setState({ isCallbackAfterAPISuccess: boolean });
  };

  toggleRescheduleDateScreen = () => {
    this.setState({ rescheduleDateScreen: !this.state.rescheduleDateScreen });
  };

  toggleAddFriendDrawer = () => {
    this.setState({ addFriendDrawer: !this.state.addFriendDrawer });
  };

  renderCoins = (inputCoins) => {
    let calc = inputCoins / 1000;
    if (inputCoins > 999) {
      return `${parseFloat(calc).toFixed(1)}k`;
    } else {
      return inputCoins;
    }
  };

  initiateVideoCall = (data) => {
    let obj = {};
    obj["dateId"] = data.data_id;
    obj["recipientId"] = data.opponentId;
    obj["UserProfileName"] = this.props.UserProfile.UserProfileName;
    obj["userId"] = getCookie("uid");
    obj["callerImage"] = data.opponentProfilePic;
    obj["callerIdentifier"] = data.opponentId;
    obj["requestedFor"] = data && data.requestedFor.includes("audio") ? "0" : "1";
    console.log("data is", data);
    this.props.userSelectedToChat(obj);
    this.props.selectedProfileForChat(obj);
    this.props.__callInit(true);
  };

  acceptOrCancelDate = (input) => {
    let PLAN =
      this.props.checkIfUserIsProUser &&
      this.props.checkIfUserIsProUser.ProUserDetails &&
      this.props.checkIfUserIsProUser.ProUserDetails.tag;
    let setType = "";
    if (this.state.dateData.dateType === 1 || this.state.dateData.requestedFor.includes("video")) {
      setType = 1;
    } else if (this.state.dateData.dateType === 2 || this.state.dateData.requestedFor.includes("phy")) {
      setType = 2;
    } else if (this.state.dateData.dateType === 3 || this.state.dateData.requestedFor.includes("audio")) {
      setType = 3;
    }
    this.setState({ acceptRejectModal: false });
    console.log("acceptOrCancelDate");
    RescheduleDate(
      getCookie("token"),
      input,
      this.state.dateData.data_id,
      this.state.dateData.proposedOn,
      setType,
      this.state.dateData.latitude,
      this.state.dateData.longitude,
      this.state.dateData.placeName,
      PLAN
    )
      .then((res) => {
        if (res.status === 200) {
          this.setState({ dateData: {}, input: "", setDateAction: "" });
          this.setIsCallbackAfterAPISuccess(true);

          setTimeout(() => {
            this.setIsCallbackAfterAPISuccess(false);
          }, 800);
        }
      })
      .catch((err) => {});
  };

  componentDidMount() {
    this.props.handlevalue(2);
    PendingDates()
      .then((data) => {
        this.setState({
          upcomingdates: data.data.data.upcomingDates,
        });

        this.setState({
          pendingdates: data.data.data.pendingDates,
        });
      })
      .catch((err) => this.setState({ pendingdates: [], upcomingDates: [] }));

    PastDates()
      .then((data) => {
        this.setState({ pastdates: data.data.data });
      })
      .catch((err) => this.setState({ pastdates: [] }));
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.isCallbackAfterAPISuccess !== this.state.isCallbackAfterAPISuccess) {
      PendingDates()
        .then((data) => {
          this.setState({
            upcomingdates: data.data.data.upcomingDates,
          });

          this.setState({
            pendingdates: data.data.data.pendingDates,
          });
        })
        .catch((err) => this.setState({ pendingdates: [], upcomingDates: [] }));

      PastDates()
        .then((data) => {
          this.setState({ pastdates: data.data.data });
        })
        .catch((err) => this.setState({ pastdates: [] }));
    }
  }

  toggleModal = (data) => {
    if (data) {
      this.setState({ modal: !this.state.modal, dateData: data });
    } else {
      this.setState({ modal: !this.state.modal });
    }
  };

  navigateToRescheduleScreen = () => {
    this.setState({ modal: false, rescheduleDateScreen: true });
  };

  render() {
    console.log("acceptRejectModal", this.state.acceptRejectModal, this.state.modal);
    const pendingdates =
      this.state.pendingdates && this.state.pendingdates.length > 0 ? (
        this.state.pendingdates.map((k) => (
          <div className="col-12 px-0" key={k._id}>
            <div className="row mx-0 datesPendingInfo align-items-center">
              <div className="col-1 px-0">
                <img src={k.opponentProfilePic} alt={k.opponentName} />
              </div>
              <div className="col-7">{k.opponentName}</div>
              <div className="col-4 text-right px-0">{moment(k.createdTimestamp).fromNow()}</div>
            </div>
            <div className="row mx-0 datesPendingInfoImage pt-2">
              <img src={k.opponentProfilePic} alt={k.opponentName} />
            </div>
            <div className="row mx-0 pt-2 datesPendingInfoText text-right">
              {k.requestedFor} request received from {k.opponentName}
            </div>
            <div className="row mx-0 datesPendingTime align-items-center">
              <div className="col-1 px-0">
                <img src={Icons.DateCalendar} alt="calendar" height={10} width={10} />
              </div>
              <div className="col-10 px-0">{moment(k.proposedOn).format("ddd MMM DD @ hh:mm A")}</div>
            </div>
            {k.isInitiatedByMe ? (
              <div className="row mx-0 datesPendingActivity pt-2">
                <div className="col-10 py-1 text-center" onClick={() => this.toggleModal(k)}>
                  Reschedule
                </div>
                <div className="col-2 py-1 text-center" onClick={() => this.setDateType(2, k)}>
                  <img src={Icons.deleteDate} alt="thrash" width={27} height={27} />
                </div>
              </div>
            ) : (
              <div className="row mx-0 datesPendingActivity pt-2">
                <div className="col-9 py-1 text-center" onClick={() => this.toggleModal(k)}>
                  Reschedule
                </div>
                <div className="col-3">
                  <div className="row">
                    <div onClick={() => this.setDateType(1, k)}>
                      <img src={Icons.acceptDate} alt="aceept" width={27} height={27} />
                    </div>
                    <div onClick={() => this.setDateType(2, k)}>
                      <img src={Icons.deleteDate} alt="thrash" width={27} height={27} />
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        ))
      ) : (
        <div className="col-12 text-center h-75 align-items-center flex-column d-flex justify-content-center">
          <div>
            <img src={Icons.U_Alert} alt="no-friends" width={200} />
          </div>
          <div className="friendsTabNoData pt-2">You have no pending dates</div>
          <div className="friendsTabNoData-s pt-2">Please schedule a date with one of your matched</div>
        </div>
      );

    const upcomingdates =
      this.state.upcomingdates && this.state.upcomingdates.length > 0 ? (
        this.state.upcomingdates.map((k) => (
          <div className="col-12 px-0" key={k._id}>
            <div className="row mx-0 datesPendingInfo align-items-center">
              <div className="col-1 px-0">
                <img src={k.opponentProfilePic} alt={k.opponentName} />
              </div>
              <div className="col-7">{k.opponentName}</div>
              <div className="col-4 text-right px-0">{moment(k.createdTimestamp).fromNow()}</div>
            </div>
            <div className="row mx-0 datesPendingInfoImage pt-2">
              <img src={k.opponentProfilePic} alt={k.opponentName} />
            </div>
            <div className="row mx-0 pt-2 datesPendingInfoText text-right">{k.requestedFor}</div>
            <div className="row mx-0 datesPendingTime align-items-center">
              <div className="col-1 px-0">
                <img src={Icons.DateCalendar} alt="calendar" height={10} width={10} />
              </div>
              <div className="col-10 px-0">{moment(k.proposedOn).format("ddd MMM DD @ hh:mm A")}</div>
            </div>
            <div className="row mx-0 datesUpcomingActivity pt-2">
              <Button
                className={
                  new Date().getTime() < parseInt(k.proposedOn)
                    ? "col-9 py-1 text-center date-activityButton-disabled"
                    : "col-9 py-1 text-center date-activityButton-enabled"
                }
                handler={() => this.initiateVideoCall(k)}
                disabled={new Date().getTime() < parseInt(k.proposedOn)}
                text={k.requestedFor}
              />
              <div className="col-3">
                <div className="row">
                  <div className="m_date_reschedule" onClick={() => this.toggleModal(k)}>
                    <img src={Icons.MReschedule} alt="calendar" width={15} height={15} />
                  </div>
                  <div onClick={() => this.setDateType(1, k)}>
                    <img src={Icons.deleteDate} alt="thrash" width={27} height={27} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="col-12 text-center h-75 align-items-center flex-column d-flex justify-content-center">
          <div>
            <img src={Icons.U_Alert} alt="no-friends" width={200} />
          </div>
          <div className="friendsTabNoData pt-2">You have no upcoming dates</div>
          <div className="friendsTabNoData-s pt-2">No upcoming dates</div>
        </div>
      );

    const pastdates =
      this.state.pastdates && this.state.pastdates.length > 0 ? (
        this.state.pastdates.map((k) => (
          <div className="col-12 py-2" id={k._id}>
            <div className="row pastDateStatus">
              <div className="col-2">
                <img src={k.opponentProfilePic} alt={k.opponentName} />
              </div>
              <div className="col-6">
                <div>{k.opponentName}</div>
                <div>{k.status}</div>
              </div>
              <div className="col-4 text-right">{moment(k.proposedOn).fromNow()}</div>
            </div>
          </div>
        ))
      ) : (
        <div className="col-12 text-center h-75 align-items-center flex-column d-flex justify-content-center">
          <div>
            <img src={Icons.U_Alert} alt="no-friends" width={200} />
          </div>
          <div className="friendsTabNoData pt-2">Past Date</div>
          <div className="friendsTabNoData-s pt-2">You have no past dates</div>
        </div>
      );
    return (
      <div className="col-12 friendsView">
        <MainDrawer onClose={this.toggleAddFriendDrawer} onOpen={this.toggleAddFriendDrawer} open={this.state.addFriendDrawer}>
          <MobileSettings onClose={this.toggleAddFriendDrawer} UserFinalData={this.props.UserFinalData} />
        </MainDrawer>
        <MainDrawer onClose={this.toggleCoinDrawer} onOpen={this.toggleCoinDrawer} open={this.state.coinDrawer}>
          <CoinBalances onClose={this.toggleCoinDrawer} UserFinalData={this.props.UserFinalData} />
        </MainDrawer>
        <MainDrawer
          onClose={this.toggleRescheduleDateScreen}
          onOpen={this.toggleRescheduleDateScreen}
          open={this.state.rescheduleDateScreen}
        >
          <MobileRescheduleDate
            setIsCallbackAfterAPISuccess={this.setIsCallbackAfterAPISuccess}
            onClose={this.toggleRescheduleDateScreen}
            UserFinalData={this.props.UserFinalData}
            dateData={this.state.dateData}
            setDateType={this.setDateType}
          />
        </MainDrawer>

        <div className="row">
          <div className="col-12 py-3 m_dates_stickyHeader">
            <div className="row">
              <div className="col-4"></div>
              <div className="col-4 friends_header">Dates</div>
              <div className="col-4">
                <div className="row header_row">
                  <div className="global_coinbalance col-8" onClick={this.toggleCoinDrawer}>
                    <img src={Images.dollarIcon} className="img-fluid" alt="dollarimg" title="dollarimg" />
                    <p className="m-0">{this.renderCoins(this.props.CoinBalance)}</p>
                  </div>
                  <div className="col-4" onClick={this.toggleAddFriendDrawer}>
                    <img
                      style={{ position: "absolute", bottom: "-2px" }}
                      src={this.props && this.props.userProfilePicture}
                      alt={this.props.UserFinalData.firstName}
                      height={30}
                      width={30}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="datesTab w-100">
            <SimpleTabs
              mobile={true}
              setValue={this.setValue}
              tabs={[{ label: "Pending" }, { label: "Upcoming" }, { label: "Past Dates" }]}
              tabContent={[{ content: pendingdates }, { content: upcomingdates }, { content: pastdates }]}
            />
          </div>
        </div>
        <MaterialModal width={"70%"} isOpen={this.state.acceptRejectModal} toggle={this.toggleAcceptRejectModal}>
          <div className="col-12 confirmDateModal">
            <div className="row p-3">
              Are you sure you want to {this.state.setDateAction} {this.state.dateData.requestedFor} date with{" "}
              {this.state.dateData.opponentName}
            </div>
            <div className="row border-top py-2">
              <div className="col-6" onClick={() => this.setState({ acceptRejectModal: false })}>
                Cancel
              </div>
              <div className="col-6" onClick={() => this.acceptOrCancelDate(this.state.input)}>
                Ok
              </div>
            </div>
          </div>
        </MaterialModal>
        <MaterialModal width={"70%"} isOpen={this.state.modal} toggle={this.toggleModal}>
          <div className="col-12 confirmDateModal">
            <div className="row p-3">
              Are you sure you want to Reschedule {this.state.dateData.requestedFor} date with {this.state.dateData.opponentName}
            </div>
            <div className="row border-top py-2">
              <div className="col-6" onClick={() => this.setState({ modal: false })}>
                Cancel
              </div>
              <div className="col-6" onClick={this.navigateToRescheduleScreen}>
                Ok
              </div>
            </div>
          </div>
        </MaterialModal>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    CoinBalance: state.UserProfile.CoinBalance,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
    UserProfile: state.UserProfile.UserProfile,
    checkIfUserIsProUser: state.ProUser,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    userSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
    __callInit: (data) => dispatch(__callInit(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MobileDates);
