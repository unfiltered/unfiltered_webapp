import React from "react";
import Ref from "./ref_big.png";
import "./Referral.scss";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { Icons } from "../../../../Components/Icons/Icons";

class Referral extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      variant: "success",
      usermessage: "Link Copied, you can share it with your friends.",
      shareCodeValue:
        "Hey! Join me on Candy, this cool new dating app. Use the code and some Credits for free. (Your code is: " +
        this.props.refCode +
        ")",
    };
  }
  shareCode = () => {
    navigator.clipboard.writeText(
      `Invite your friends into our new dating app, https://app.unfiltered.love, and refer them with code ${this.props.refCode} to get extra Credits on signup`
    );
    this.setState({
      open: true,
    });
    setTimeout(() => {
      this.setState({
        open: false,
        usermessage: "Link Copied, you can share it with your friends.",
      });
    }, 1500);
  };
  refCodeCopied = () => {
    navigator.clipboard.writeText(
      `Invite your friends into our new dating app, https://app.unfiltered.love, and refer them with code ${this.props.refCode} to get extra Credits on signup`
    );
    this.setState({
      open: true,
      usermessage: "Referral Code Copied",
    });
    setTimeout(() => {
      this.setState({
        open: false,
      });
    }, 1500);
  };
  render() {
    let { variant, usermessage, shareCodeValue, open } = this.state;
    let { onClose, refCode } = this.props;
    return (
      <div className="col-12" style={{ height: "100vh" }}>
        <div className="row py-3 border-bottom">
          <div className="col-3" onClick={onClose}>
            <img src={Icons.PinkBack} alt="back" height={20} width={25} />
          </div>
          <div className="col-6 m_refferal_header">Referral Code</div>
          <div className="col-3"></div>
        </div>
        <div className="text-center">
          <div className="py-3">
            <img src={Ref} height={100} alt="referral" />
          </div>
          <div className="referYourFriends">Refer your friends</div>
          <div className="getReward">Get a reward if your contacts sign up using your invite code</div>
          <div className="col-12 d-flex justify-content-center pt-3">
            <div className="copy" onClick={() => this.refCodeCopied()}>
              <span className="pr-3">{refCode}</span> <span> Copy</span>
            </div>
          </div>
          <div className="shareCode mt-3" onClick={() => this.shareCode()}>
            <textarea hidden ref={(textarea1) => (this.textArea1 = textarea1)} value={shareCodeValue} />
            <span>Share Code</span>
          </div>
        </div>
        <Snackbar type={variant} message={usermessage} open={open} />
      </div>
    );
  }
}

export default Referral;
