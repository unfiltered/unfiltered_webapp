import React, { Component } from "react";
import "./Location.scss";
import { geolocated } from "react-geolocated";
import { GoogleApiWrapper, Map } from "google-maps-react";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Selectlocation from "./SelectLocation";
import { getCookie, setCookie } from "../../../../lib/session";
import deleteSvg from "./svg/close.svg";
import { keys } from "../../../../lib/keys";
import tickSvg from "./svg/tick.svg";
import circleSvg from "./svg/circle.svg";
import { Icons } from "../../../../Components/Icons/Icons";

class ChooseLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Getlocation: false,
      multiselectchecbox: [],
      selectedAddressToShow: "",
      selectedLocation: "",
      locationToShow: "",
      baseAddress: "",
      currentPlaceToShow: "",
      heightOfFixedSection: "",
    };
  }

  componentDidMount() {
    if (getCookie("locations") != null) {
      let loc = getCookie("locations");
      let _loc = JSON.parse(loc);
      if (_loc.length > 0) {
        this.setState({
          multiselectchecbox: _loc,
          selectedAddressToShow: _loc[0].address,
        });
      }
      if (getCookie("selectedLocation") != null) {
        this.setState({
          selectedLocation: JSON.parse(getCookie("selectedLocation")),
          currentPlaceToShow: JSON.parse(getCookie("selectedLocation")).city,
        });
      }
    } else if (getCookie("selectedLocation") != null) {
      let selectedLocation = JSON.parse(getCookie("selectedLocation"));
      this.setState({
        selectedLocation: selectedLocation,
        currentPlaceToShow: selectedLocation.city,
      });
    } else if (getCookie("location") != null) {
      this.setState({ currentPlaceToShow: getCookie("location") });
    }
    this.setState({ baseAddress: JSON.parse(getCookie("baseAddress")) });
    let h = document.getElementById("m_multiple_location_static_section").clientHeight;
    this.setState({ heightOfFixedSection: h });
  }

  openlocationdrawer = () => {
    this.setState({ Getlocation: true });
  };

  closelocationdrawer = () => {
    this.setState({ Getlocation: false });
  };

  delete = (index) => {
    let { multiselectchecbox } = this.state;
    multiselectchecbox.splice(index + 1, 1);
    this.setState({ multiselectchecbox });
    setCookie("locations", JSON.stringify(multiselectchecbox));
  };

  saveLocation = (address, city, lat, lng) => {
    let { multiselectchecbox } = this.state;

    if (multiselectchecbox.includes(address)) {
      let index = multiselectchecbox.indexOf(address);
      multiselectchecbox.splice(index, 1);
    } else if (multiselectchecbox.length >= 5) {
      multiselectchecbox.pop();
    }
    multiselectchecbox.push({ address: address, city: city, lat: lat, lng: lng });

    this.setState({ multiselectchecbox });
    setCookie("locations", JSON.stringify(multiselectchecbox));
  };

  selectLocationFn = (obj) => {
    this.setState({ selectedLocation: obj });
  };

  saveLocaionAndNavigateBack = () => {
    // console.log("selected location", this.state.selectedLocation);
    this.props.setLocationOfCookie(this.state.selectedLocation);
    this.props.onClose();
  };

  render() {
    console.log("baseAddress", this.state.baseAddress);
    const getdrawer = <Selectlocation onClose={this.closelocationdrawer} saveLocation={this.saveLocation} />;
    return (
      <div className="MCommonwidth m_multiple_locations_view">
        <MainDrawer width={400} onClose={this.closelocationdrawer} onOpen={this.openlocationdrawer} open={this.state.Getlocation}>
          {getdrawer}
        </MainDrawer>
        <div
          className="m_multiple_location_static_section"
          id="m_multiple_location_static_section"
          style={{ height: this.state.heightOfFixedSection, position: "fixed", top: 0 }}
        >
          <div className="col-12 py-3">
            <div className="row">
              <div className="col-3">
                {this.props.iseditable ? (
                  <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.props.onClose} />
                ) : (
                  <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.props.onClose} />
                )}
              </div>
              <div className="col-6 text-center">
                <p className="m-0 m_location_header">
                  <strong>Location</strong>
                </p>
              </div>
              <div className="col-3"></div>
            </div>
          </div>

          <div className="Mcurrentlocation">
            <p className="m-0">CURRENT LOCATION</p>
          </div>

          <div className="col-12 py-2" style={{ height: "83px" }}>
            <div className="h-100 row align-items-center">
              <div className="col-2 pr-0 d-flex align-items-center">
                <i className="fas fa-map-marker-alt locationIcon" />
              </div>
              <div className="col-8 pl-0 Muserlocation">
                <p className="m-0">My Current Location :</p>
                <p className="m-0" style={{ whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>
                  {this.state.baseAddress.address}
                </p>
              </div>
              <div className="col-2 pr-0 d-flex align-items-center">
                {this.state.selectedLocation.address === this.state.baseAddress.address ? (
                  <img src={tickSvg} height={25} width={25} alt="tick" />
                ) : (
                  <img src={circleSvg} height={25} width={25} onClick={() => this.selectLocationFn(this.state.baseAddress)} alt="circle" />
                )}
              </div>
            </div>
          </div>
          <div className="Mcurrentlocation">
            <p className="m-0">RECENT PASSPORT LOCATIONS</p>
          </div>
        </div>
        <div
          style={{
            // marginTop: this.state.heightOfFixedSection + 44,
            marginTop: "230px",
            // height: `calc(100vh - ${this.state.heightOfFixedSection + 84 + 59}px)`,
            height: `calc(100vh - 327px)`,
            marginBottom: "40px",
            overflowY: "scroll",
          }}
        >
          {this.state.multiselectchecbox.length > 0 &&
            this.state.multiselectchecbox.map((k, i) => (
              <div className="col-12 border-bottom py-2" key={i}>
                <div className="row">
                  <div className="col-9">
                    <div className="m_loc_city">{k.city}</div>
                    <div className="m_loc_addr">{k.address}</div>
                  </div>
                  <div className="col-3">
                    <div className="row mx-0 h-100 align-items-center">
                      <div className={this.state.selectedLocation.address === k.address ? "col-6 px-0" : "col-6 px-0"}>
                        {this.state.selectedLocation.address === k.address ? (
                          <img src={tickSvg} height={25} width={25} alt="tick" />
                        ) : (
                          <img src={circleSvg} height={25} width={25} onClick={() => this.selectLocationFn(k)} alt="circle" />
                        )}
                      </div>
                      <div className="col-6 px-0" onClick={() => this.delete(i)}>
                        {this.state.selectedLocation.address === k.address ? (
                          ""
                        ) : (
                          <img src={deleteSvg} alt="delete" height={25} width={25} />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
        <div className="Mnewlocation" onClick={this.openlocationdrawer}>
          <div className="col-12 text-center">
            <p className="m-0">
              <i className="fas fa-plane pr-2" />
              Add a New Location
            </p>
          </div>
        </div>
        <button className="m_saveLocation_circle" onClick={this.saveLocaionAndNavigateBack}>
          <img src={Icons.checked} height={25} width={25} alt="checked" />
        </button>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: keys.googleApiKey,
})(
  geolocated({
    positionOptions: { enableHighAccuracy: false },
    userDecisionTimeout: 5000,
  })(ChooseLocation)
);
