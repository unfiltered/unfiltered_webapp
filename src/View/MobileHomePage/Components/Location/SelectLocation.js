// Main React Components
import React, { Component } from "react";
import { GoogleApiWrapper, Marker, Map } from "google-maps-react";
import { geolocated } from "react-geolocated";
import { getCookie } from "../../../../lib/session";
import "./SelectLocation.scss";
import "./Location.scss";
import { keys } from "../../../../lib/keys";
import { Icons } from "../../../../Components/Icons/Icons";

let lat = getCookie("lat");
let long = getCookie("long");

class Contents extends Component {
  state = {
    position: null,
    city: "",
    multiselectchecbox: [],
    markers: [
      {
        name: "Current position",
        position: {
          lat: lat,
          lng: long,
        },
      },
    ],
    lat: "",
    lng: "",
  };

  componentDidMount() {
    let { multiselectchecbox } = this.state;
    if (getCookie("locations") != null) {
      let d = getCookie("locations");
      multiselectchecbox = JSON.parse(d);
      this.setState({ multiselectchecbox });
    }
    this.renderAutoComplete();
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps.map) this.renderAutoComplete();
  }

  onSubmit(e) {
    e.preventDefault();
  }

  renderAutoComplete() {
    const { google, map } = this.props;
    if (!google || !map) return;
    const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
    autocomplete.bindTo("bounds", map);
    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) return;
      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(9);
      }
      this.setState({ position: place.geometry.location }, () => {
        this.getAddressHelper(place.geometry.location.lat(), place.geometry.location.lng());
      });
    });
  }

  getAddressHelper = (lat, lng) => {
    let google = window.google;
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ latLng: latlng }, (results, status) => {
      let resLength = results && results[0].address_components.length;
      if (status === google.maps.GeocoderStatus.OK) {
        // console.log("city", results[0].address_components[resLength - 5].long_name);
        // console.log("addr", results[0].formatted_address);
        this.setState({
          lat,
          lng,
          locationSelected: results[0].formatted_address,
          city: results[0].address_components[resLength - 5].long_name,
        });
      }
    });
  };

  onMarkerDragEnd = (coord, index) => {
    const { latLng } = coord;
    let lat = latLng.lat();
    let lng = latLng.lng();
    this.getAddressHelper(lat, lng);
    this.setState((prevState) => {
      const markers = [...this.state.markers];
      markers[index] = { ...markers[index], position: { lat, lng } };
      return { markers };
    });
  };

  confirmLocation = () => {
    this.props.saveLocation(this.state.locationSelected, this.state.city, this.state.lat, this.state.lng);
    this.props.onClose();
  };

  render() {
    const { position } = this.state;
    return (
      <div className="MCommonwidth">
        <div className="col-12">
          <div className="row py-3">
            <div className="col-2">
              <img src={Icons.PinkBack} alt="back" height={20} width={25} onClick={this.props.onClose} />
            </div>
            <div className="col-8 text-center">
              <p className="m-0">
                <strong className="m_choose_location">Choose Location</strong>
              </p>
            </div>
            <div className="col-2"></div>
          </div>
        </div>
        <div style={{ height: "84vh", width: "100%", position: "relative" }}>
          <form onSubmit={this.onSubmit} className="m_location_searchBox">
            <input placeholder="Enter a location" ref={(ref) => (this.autocomplete = ref)} type="text" />
          </form>
          <div>
            <Map
              {...this.props}
              center={position}
              initialCenter={{
                lat: 12,
                lng: 77,
              }}
              centerAroundCurrentLocation={false}
              containerStyle={{
                height: "100vh",
                position: "relative",
                width: "100%",
              }}
            >
              <Marker position={position} draggable={true} onDragend={(t, map, coord) => this.onMarkerDragEnd(coord)} />
            </Map>
          </div>
        </div>
        <div className="col-12">
          <div className="col-12 MConfromlocation">
            <div className="row">
              <button onClick={this.confirmLocation}>Confirm Location</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const MapWrapper = (props) => (
  <Map className="map" google={props.google} visible={false}>
    <Contents {...props} />
  </Map>
);

export default GoogleApiWrapper({
  apiKey: keys.googleApiKey,
})(
  geolocated({
    positionOptions: { enableHighAccuracy: false },
    userDecisionTimeout: 5000,
  })(MapWrapper)
);
