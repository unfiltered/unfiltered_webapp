// Main React Components
import React from "react";
import "./UserProfile.scss";
import CircularProgress from "@material-ui/core/CircularProgress";
import { connect } from "react-redux";
import { UserProfileData, UserProfileNewData } from "../../../../controller/auth/verification";
import Button from "../../../../Components/Button/Button";
import { Icons } from "../../../../Components/Icons/Icons";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { __BlobUpload, uploadVideoToCloudinaryPromise } from "../../../../lib/cloudinary-image-upload";
import { USER_PROFILE_ACTION_FUN } from "../../../../actions/UserProfile";
import ReactPlayer from "react-player";
import MaterialUiModal from '../../../../Components/Model/MAT_UI_Modal'
import "react-html5-camera-photo/build/css/index.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";

class ProfileImage extends React.Component {
  constructor(props) {
    super(props);
    this.files = [];
    this.state = {
      _otherImages: [],
      userData: {},
      loader: false,
      profilePic: "",
      wh: "",
      usermessage: "",
      variant: "success",
      open: false,
      uploadingImageState: false,
      doYouHaveCamera: false,
      cameraModal: false,
      isTakenPhoto: false,
      realImage: "",
      textToRender: "Please wait while we upload image...",
    };
  }

  handleTakePhotoAnimationDone = (dataUri) => {
    this.setState({ isTakenPhoto: true });
    console.log("takePhoto");
  };

  startImageCapture = (reset) => {
    console.log("clicked");
    this.setState({
      imageCaptureView: !this.state.imageCaptureView,
      realImage: reset ? "" : this.state.realImage,
    });
  };


  handleCameraError = (error) => {
    console.log("handleCameraError", error);
  };

  handleCameraStart = (stream) => {
    console.log("handleCameraStart", stream);
  };

  handleCameraStop = () => {
    console.log("handleCameraStop");
  };

  handleTakePhoto = (dataUri) => {
    this.files[0] = dataUri;
    this.startImageCapture(false);
    if (dataUri) {
      this.setState({ realImage: dataUri, valid: true });
    }
  };

  toggleCameraModal = () => this.setState({ cameraModal: !this.state.cameraModal });

  componentDidMount() {
    UserProfileData()
      .then((res) => {
        this.setState({
          userData: res.data.data,
          _otherImages: [{url: res.data.data.profilePic, timestamp: new Date().getTime()}, ...res.data.data.otherImages],
          loader: false,
        });
        let wh = document.getElementById("single_image").clientWidth;
        this.setState({ wh: wh });
        if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
          navigator.mediaDevices
            .enumerateDevices()
            .then((res) => {
              let index = res.filter((k) => k.kind == "videoinput");
              if (index.length > 0) {
                this.setState({ doYouHaveCamera: true });
              }
            })
            .catch((err) => {});
        }
      })
      .catch((err) => {
        this.setState({ loader: false });
      });
  }

  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  deleteImageFromOtherImages = (file) => {
    this.setState({ textToRender: "Deleting picture...", uploadingImageState: true });
    let arrayImages = [...this.state._otherImages];
    let uploadImages = [...this.state._otherImages];
    let index = uploadImages.findIndex((k) => k.url === file);
    let index1 = arrayImages.findIndex((k) => k.url  === file);
    let index2 = uploadImages.findIndex((k) => k.url  === this.props.UserProfile.UserProfilePic);
    uploadImages.splice(index, 1);
    uploadImages.splice(index2, 1);
    arrayImages.splice(index1, 1);
    UserProfileNewData({
      profilePic: this.props.UserProfile.UserProfilePic,
      otherImages: uploadImages,
    }).then((result) => {
      this.setState({ textToRender: "", uploadingImageState: false, _otherImages: arrayImages });
    });
  };

  uploadImage = (file, type) => {
    /** type 1 for other images */
    /** type 2 for updating profile picture */
    if (type === 1) {
      let arrayImages = [...this.state._otherImages];
      let imagesToUpload = [...this.state._otherImages];
      let index = imagesToUpload.findIndex((k) => k.url === this.props.UserProfile.UserProfilePic);
      imagesToUpload.splice(index, 1);
      this.setState({ textToRender: "Please wait while we add new image", uploadingImageState: true });
        arrayImages.push({url: file, timestamp: new Date().getTime()});
        UserProfileNewData({
          profilePic: this.props.UserProfile.UserProfilePic,
          otherImages: [file],
        }).then((result) => {
          imagesToUpload = [];
          this.setState({ textToRender: "", uploadingImageState: false, _otherImages: arrayImages });
        });
    }  else if (type === 2) {
      this.setState({ textToRender: "Updating profile picture...", uploadingImageState: true });
      let imagesToUpload = [...this.state._otherImages];
      let index = imagesToUpload.findIndex((k) => k.url === file);
      console.log("image before updating", imagesToUpload);
      imagesToUpload.splice(index, 1);
      console.log("image after updating", imagesToUpload);
      UserProfileNewData({
        profilePic: file,
        otherImages: imagesToUpload.map(k => k.url),
      }).then((result) => {
        imagesToUpload = [];
        this.props.updateProfilePhoto(file);
        this.setState({ textToRender: "", uploadingImageState: false, textToRender: "" });
      });
    }
  };

  removeVideo = () => {
    let existingObj = { ...this.state.userData };
    existingObj["profileVideo"] = "";
    this.setState({ userData: existingObj });
    UserProfileNewData({ profileVideo: "" })
      .then((data) => {
        existingObj["profileVideo"] = "";
        this.toggleGallery();
        this.setState({
          userData: existingObj,
          open: true,
          variant: "success",
          usermessage: "Please wait while video being deleted",
        });
      })
      .catch((error) => {
        this.setState({
          userData: existingObj,
          open: true,
          variant: "success",
          usermessage: "Please wait while video being deleted",
        });
      });
  };

  cloudinaryVideoStateHandler = (file) => {
    this.setState({ uploadingImageState: true, textToRender: "Please wait while video is being uploaded..." });
    let existingObj = { ...this.state.userData };
    // if (file[0].size <= this.state.size) {
    this.files[0] = file;
    uploadVideoToCloudinaryPromise(this.files)
      .then((res) => {
        existingObj["profileVideo"] = res.body.secure_url;
        UserProfileNewData({ profileVideo: res.body.secure_url })
          .then((res) => { 
            this.setState({
              userData: existingObj,
              uploadingImageState: false,
              textToRender: "",
              open: true,
              usermessage: "Video Successfully Uploaded...",
            });
          })
          .catch((err) => {
            this.setState({ uploadingImageState: false, textToRender: "" });
          });
      })
      .catch((err) => {
        this.setState({ uploadingImageState: false, textToRender: "" });
      });
  };

  uploadImagePhotoImage = async () => {
    if (this.state.userData.otherImages.length >= 5) {
      this.setState({ open: true, usermessage: "Cannot upload more than 5 photos", variant: "error" });
    } else {
      let upload = await __BlobUpload(this.state.realImage);
      this.toggleCameraModal();
      this.setState({ realImage: "", isTakenPhoto: false });
      this.uploadImage(upload.body.secure_url, 1);
    }
  };

  render() {
    return (
      <div className="col-12 px-0">
        {this.state.uploadingImageState ? (
          <div className="m_chat_customLoader">
            <div>
              <CircularProgress /> <span>{this.state.textToRender}</span>
            </div>
          </div>
        ) : (
          <span />
        )}
        <div className="row mx-0">
          <div className="col-12 p-3 px-0">
            <div className="row mx-0">
              <img src={Icons.PinkBack} alt="back" width={25} height={20} onClick={this.props.onClose} />
            </div>
          </div>
          <div className="col-12 p-3 px-0">
            <div className="row mx-0">
              <div className="MCommonheader">
                <p>Edit</p>
                <p>Profile</p>
              </div>
            </div>
          </div>
          <div className="col-12 p-3 px-0 m_update_profile_video">
            <div className="row mx-0 border-bottom">
              {this.state.userData && this.state.userData.profileVideo ? (
                <div
                  className="col-4 px-0 m_update_profile_video d-flex justify-content-center align-items-center"
                  id="single_image"
                  style={{ width: this.state.wh + 20, height: this.state.wh + 20 }}
                >
                  <span onClick={this.removeVideo}>
                    <img src={Icons.M_DeletePicture} alt="delete_picutre" height={21} width={21} />
                  </span>
                  <ReactPlayer url={this.state.userData.profileVideo} playing={true} controls={true} />
                </div>
              ) : (
                <span />
              )}

              <div>Video</div>
              <Button
                text={
                  <label className="col-auto m-0" htmlFor="VideoFile">
                    Add Video
                    <input
                      style={{ visibility: "hidden" }}
                      accept="video/mp4,video/x-m4v,video/*"
                      id="VideoFile"
                      type="file"
                      className="Wuser_input d-none"
                      ref={(videoInputEl) => (this.videoInputEl = videoInputEl)}
                      onChange={() => this.cloudinaryVideoStateHandler(this.videoInputEl.files)}
                    />
                  </label>
                }
              />
            </div>
          </div>
          <div className="col-12 p-3 px-0 m_update_profile_photo_header">
            <div className="row mx-0">
              <div>Photo</div>
            </div>
          </div>
          <div className="col-12 pt-3 px-0">
            <div className="row mx-0 justify-content-start">
              {this.state._otherImages.length > 0 ? (
                this.state._otherImages.map((k, i) => (
                  <div
                    className="col-4 px-0 m_update_profile_otherImages d-flex justify-content-center align-items-center"
                    id="single_image"
                    key={i}
                    style={{ width: this.state.wh }}
                  >
                    {this.props.UserProfile.UserProfilePic !== k ? (
                      <span className="m_update_profile_deletePicture" onClick={() => this.deleteImageFromOtherImages(k.url)}>
                        <img src={Icons.M_DeletePicture} alt="delete_picutre" height={21} width={21} />
                      </span>
                    ) : (
                      ""
                    )}
                    <div>
                      <div style={{ width: this.state.wh - 15, height: this.state.wh - 15 }}>
                        <img src={k.url} alt={k.url} style={{ width: this.state.wh - 15, height: this.state.wh - 15, objectFit: "cover" }} />
                      </div>
                      <div className="set_profile_picture">
                        {this.props.UserProfile.UserProfilePic === k.url ? (
                          <div>
                            <div>
                              <img src={Icons.profilePicActive} alt="pink-tick" />
                            </div>
                            <div> Set as Profile Picture</div>
                          </div>
                        ) : (
                          <div onClick={() => this.uploadImage(k.url, 2)}>
                            <div>
                              <img src={Icons.activateProfilePic} alt="pink-tick" />
                            </div>
                            <div> Set as Profile Picture</div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <span />
              )}
              <div
                className="col-4 px-0 m_update_profile_upload_image d-flex justify-content-center align-items-center"
                style={{ width: this.state.wh, height: this.state.wh }}
                onClick={this.toggleCameraModal}
              >
                <label className="col-auto" htmlFor="File">
                  <img src={Icons.camera} alt="camera" height={40} width={40} />
                </label>
              </div>
            </div>
          </div>
        </div>
        <Snackbar
          timeout={2500}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleClose}
        />
        <MaterialUiModal isOpen={this.state.cameraModal} toggle={this.toggleCameraModal} width={'90%'}>
          <div className="py-4">
            {this.state.realImage ? (
              <div className="text-center">
                <img
                  src={this.state.realImage}
                  alt="image"
                  height={300}
                  width={200}
                  style={{ objectFit: "cover", borderRadius: "5px", marginBottom: "20px" }}
                />
                <div className="uploadButton">
                  <button
                    onClick={() => {
                      this.setState({ isTakenPhoto: false });
                      this.startImageCapture(true);
                    }}
                  >
                    Try Again
                  </button>
                  <button onClick={this.uploadImagePhotoImage}>Upload</button>
                </div>
              </div>
            ) : (
              <section>
                <div className="moments_cancel_button" onClick={this.toggleCameraModal}>
                  <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
                </div>
                {this.state.doYouHaveCamera ? (
                  <div className="m-camera-module">
                    <Camera
                      onTakePhoto={(dataUri) => {
                        this.handleTakePhoto(dataUri);
                      }}
                      onTakePhotoAnimationDone={(dataUri) => {
                        this.handleTakePhotoAnimationDone(dataUri);
                      }}
                      onCameraError={(error) => {
                        this.handleCameraError(error);
                      }}
                      idealFacingMode={FACING_MODES.ENVIRONMENT}
                      idealResolution={{ width: 768, height: 1366 }}
                      imageType={IMAGE_TYPES.JPG}
                      imageCompression={0.97}
                      isMaxResolution={true}
                      isImageMirror={false}
                      isSilentMode={false}
                      isDisplayStartCameraError={true}
                      isFullscreen={false}
                      sizeFactor={1}
                      onCameraStart={(stream) => {
                        this.handleCameraStart(stream);
                      }}
                      onCameraStop={() => {
                        this.handleCameraStop();
                      }}
                    />
                  </div>
                ) : (
                  <div className="text-center py-4">
                    <h3>Please Enable Webcam if you want to continue clicking photos</h3>
                  </div>
                )}
              </section>
            )}
          </div>
        </MaterialUiModal>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateProfilePhoto: (image) => dispatch(USER_PROFILE_ACTION_FUN("UserProfilePic", image)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileImage);
