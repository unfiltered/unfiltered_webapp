import React, { Component } from "react";
import BaseProfile from "./BaseProfile";
import { connect } from "react-redux";

class FinalUserProfile extends Component {
  constructor() {
    super();
    this.state = {
      width: window.innerWidth,
    };
  }

  componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  handleScreen = (screen) => {
    this.setState({ currentScreen: screen });
  };

  render() {
    const { width } = this.state;
    const isMobile = width <= 570;

    return (
      <div>
        {!this.state.currentScreen ? (
          <BaseProfile
            handleScreen={this.handleScreen}
            currentScreen={this.state.currentScreen}
            isMobile={isMobile}
            onclose={this.props.onClose}
          />
        ) : (
          this.state.currentScreen
        )}
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
  };
}

export default connect(mapstateToProps, null)(FinalUserProfile);
