import React, { Component } from "react";
import "./UserProfile.scss";
import MainDrawer from "../../../../Components/Drawer/Drawer";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddProfileImage from "./AddProfile";
import ChooseLocation from "../Location/Location";
import GenderInput from "../../../LandingPage/LoginModuleMobile/UserSignup/Gender/GenderInput";
import MobileInput from "../../../LandingPage/LoginModuleMobile/UserSignup/MobileInput/MobileInput";
import EmailInput from "../../../LandingPage/LoginModuleMobile/UserSignup/EmailInput/EmailInput";
import ArrowNext from "../../../../Components/MobileNextButton/NextButton";
import { connect } from "react-redux";
import { UserProfileData, UserProfileNewData, getPreference } from "../../../../controller/auth/verification";
import { getCookie } from "../../../../lib/session";
import MobileEditPreferences from "./MobileEditPreferences";
import Carousel from "./MUI-carousel";
import MobileMoments from "../otherUserMobileProfile/MobileMoments";
import { Icons } from "../../../../Components/Icons/Icons";

const styles = (theme) => ({
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    paddingLeft: theme.spacing.unit * 4,
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 255,
    maxWidth: 400,
    overflow: "hidden",
    display: "block",
    width: "100%",
  },
  arrow: {
    color: "#fff",
    backgroundColor: "#f74167",
    padding: 15,
    borderRadius: "50%",
  },
  disabledarrow: {
    color: "#fff",
    backgroundColor: "#b7b7b7",
    padding: 15,
    borderRadius: "50%",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

class BaseUserProfile extends Component {
  state = {
    valid: false,
    open: false,
    aboutPayload: "",
    prevAboutPayload: "",
    checkedA: false,
    checkedB: false,
    profileimages: false,
    phone: false,
    Gender: false,
    EmailID: false,
    location: false,
    Prefrence: false,
    value: "",
    activeStep: 0,
    UserFinalData: "",
    newfindmateid: "",
    UserPrefSelectedData: "",
    MobileSelectedindex: "",
    selectedScreenIndex: 0,
    loader: false,
    squareWH: "",
    momentsScreen: false,
    postInfo: {},
  };

  componentDidMount() {
    this.getuserprofile();
    this.handleuserpre();
  }

  // Function to get User Prefernces
  handleuserpre = () => {
    getPreference()
      .then((data) => {
        console.log("baseProfile.js", data);
      })
      .catch((err) => {
        console.log("BaseProfile.js err", err);
      });
  };

  // Fucntion to get User Profile
  getuserprofile = () => {
    this.setState({ loader: true });
    UserProfileData().then((data) => {
      this.setState(
        {
          loader: false,
          UserFinalData: data.data.data,
          aboutPayload: data.data.data.about,
          prevAboutPayload: data.data.data.about,
          checkedA: this.returnTrueOrFalseBasedOnToggle(data.data.data.age.isHidden),
          checkedB: this.returnTrueOrFalseBasedOnToggle(data.data.data.distance.isHidden),
        },
        () => {
          if (data.data.data && data.data.data.moments && data.data.data.moments.length > 0) {
            let w = document.getElementById("m_moments_boxes").offsetWidth;
            this.setState({ squareWH: w });
          }
        }
      );
    });
  };

  // Function for the Show Age Button On/Off
  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  // Function for the Show Distance On/Off
  handleChangeother = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  // Function for the Profileimages Open Drawer
  openProfileimageDrawer = () => {
    this.setState({ profileimages: true });
  };

  // Function for the Profileimages Close Drawer
  closeProfileimageDrawer = () => {
    this.setState({ profileimages: false }, () => {
      this.getuserprofile();
    });
  };

  // Function for the Edit Gender Open Drawer
  OpenEditablegenderDrawer = () => {
    this.setState({ Gender: true });
  };

  // Function for the Edit Gender Close Drawer
  CloseEditablegenderDrawer = () => {
    this.setState({ Gender: false });
  };

  // Function for the Edit Phone Number Open Drawer
  OpenEditablephoneDrawer = () => {
    this.setState({ phone: true });
  };

  // Function for the Edit Phone Number Close Drawer
  CloseEditablephoneDrawer = () => {
    this.setState({ phone: false });
  };

  // Function for the Edit EmailID Open Drawer
  OpenEditableEmailIDDrawer = () => {
    this.setState({ EmailID: true });
  };

  // Function for the Edit EmailID Close Drawer
  CloseEditableEmailIDDrawer = () => {
    this.setState({ EmailID: false });
  };

  // Function for the Edit Location Open Drawer
  OpenEditablelocationDDrawer = () => {
    this.setState({ location: true });
  };

  // Function for the Edit Location Close Drawer
  CloseEditablelocationDrawer = () => {
    this.setState({ location: false });
  };

  // Function for the Edit Prefrence Open Drawer
  OpenEditablePrefrenceDDrawer = (prefItem, index) => {
    console.log("[prefItem]", prefItem);
    this.setState({
      Prefrence: true,
      UserPrefSelectedData: prefItem,
      MobileSelectedindex: index,
    });
  };

  // Function for the Edit Prefrence Close Drawer
  CloseEditablePrefrenceDrawer = () => {
    this.setState({ Prefrence: false });
  };

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the Input Module
  handlename = (event) => {
    this.setState({ aboutPayload: event.target.value, valid: true });
  };

  // API Call the UserAbout Module
  updateAbout = () => {
    UserProfileNewData({ about: this.state.aboutPayload })
      .then((data) => {
        this.getuserprofile();
        this.setState({
          open: true,
          variant: "success",
          prevAboutPayload: this.state.aboutPayload,
          usermessage: "User Preference Update successfully.!!",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
      });
  };

  returnTrueOrFalseBasedOnToggle = (val) => {
    if (val === 1) {
      return true;
    } else if (val === 0) {
      return false;
    }
  };

  handleChangeAge = (event) => {
    this.setState({ checkedA: event.target.checked });
    let checked = event.target.checked === true ? 1 : 0;
    UserProfileNewData({ dontShowMyAge: checked })
      .then((data) => {
        this.setState({ variant: "success", open: true, usermessage: "Preferences updated successfully." });
      })
      .catch((error) => {
        this.setState({ variant: "error", open: true, usermessage: "Something went wrong." });
      });
  };

  handleChangeDist = (event) => {
    this.setState({ checkedB: event.target.checked });
    let checked = event.target.checked === true ? 1 : 0;
    UserProfileNewData({ dontShowMyDist: checked })
      .then((data) => {
        this.setState({ variant: "success", open: true, usermessage: "Preferences updated successfully." });
      })
      .catch((error) => {
        this.setState({ variant: "error", open: true, usermessage: "Something went wrong." });
      });
  };

  toggleMoments = (data) => {
    if (data) {
      this.setState({ postInfo: data });
    }
    this.setState({ momentsScreen: !this.state.momentsScreen });
  };

  render() {
    const { classes } = this.props;

    const profileimagesdrawer = (
      <AddProfileImage
        onClose={this.closeProfileimageDrawer}
        UserFinalData={this.state.UserFinalData}
        getuserprofile={this.getuserprofile}
      />
    );

    return (
      <div>
        {/* Add Profile && Video Module Drawer */}
        <MainDrawer width={400} onClose={this.closeProfileimageDrawer} onOpen={this.openProfileimageDrawer} open={this.state.profileimages}>
          {profileimagesdrawer}
        </MainDrawer>

        {/* Edit User Gender Module Drawer */}
        <MainDrawer onClose={this.CloseEditablegenderDrawer} onOpen={this.OpenEditablegenderDrawer} open={this.state.Gender}>
          <GenderInput
            iseditable={true}
            isMobile={true}
            onClose={this.CloseEditablegenderDrawer}
            UserFinalData={this.state.UserFinalData}
          />
        </MainDrawer>

        {/* Edit User Phone Module Drawer */}
        <MainDrawer onClose={this.CloseEditablephoneDrawer} onOpen={this.OpenEditablephoneDrawer} open={this.state.phone}>
          <MobileInput iseditable={true} isMobile={true} onClose={this.CloseEditablephoneDrawer} />
        </MainDrawer>

        {/* Edit User EmailID Drawer */}
        <MainDrawer onClose={this.CloseEditableEmailIDDrawer} onOpen={this.OpenEditableEmailIDDrawer} open={this.state.EmailID}>
          <EmailInput
            iseditable={true}
            isMobile={true}
            onClose={this.CloseEditableEmailIDDrawer}
            getuserprofile={this.getuserprofile}
            UserFinalData={this.state.UserFinalData}
          />
        </MainDrawer>

        {/* Edit User Location Drawer */}
        <MainDrawer onClose={this.CloseEditablelocationDrawer} onOpen={this.OpenEditablelocationDDrawer} open={this.state.location}>
          <ChooseLocation iseditable={true} isMobile={true} onClose={this.CloseEditablelocationDrawer} />
        </MainDrawer>

        {/* Edit User Prefrence Data Drawer */}
        <MainDrawer onClose={this.CloseEditablePrefrenceDrawer} onOpen={this.OpenEditablePrefrenceDDrawer} open={this.state.Prefrence}>
          <MobileEditPreferences
            getuserprofile={this.getuserprofile}
            onClose={this.CloseEditablePrefrenceDrawer}
            dataToEdit={this.state.UserPrefSelectedData}
          />
        </MainDrawer>
        <MainDrawer width={400} onClose={this.toggleMoments} onOpen={this.toggleMoments} open={this.state.momentsScreen}>
          <MobileMoments onClose={this.toggleMoments} UserFinalData={this.state.UserFinalData} selectedPost={this.postInfo} />
        </MainDrawer>
        {this.state.loader ? (
          <div className="text-center">
            <CircularProgress className={classes.progress} />
          </div>
        ) : (
          <div>
            {/* Back && More Picture Module */}
            <section className="col-12 py-2 selfProfile-header" style={{ zIndex: "9999", background: "rgba(0,0,0,0.4)" }}>
              <div className="row">
                <div className="col-6 text-left">
                  <i
                    className="fa fa-arrow-left"
                    onClick={this.props.onclose}
                    style={{
                      fontSize: "24px",
                      color: "#fff",
                    }}
                  />
                </div>
                <div className="col-6 text-right" onClick={this.openProfileimageDrawer}>
                  <i
                    className="far fa-images"
                    style={{
                      fontSize: "24px",
                      color: "#fff",
                    }}
                  />
                </div>
              </div>
            </section>
            <section className="selfProfile-scrollView">
              {/* User Uploaded Images Module */}
              <div className="p-0 col-12 text-center MUserProfileimg">
                <Carousel userProfileInfo={this.state.UserFinalData} />
                {/* <img src={this.props && this.props.userProfilePicture} alt={this.state.UserFinalData.firstName} width="100%" height="350px" /> */}
              </div>

              {/* Main User Data Module */}
              <div className="col-12 pt-3 pb-4">
                {/* User Name Module */}
                <div className="MUserProfiledetails">
                  <h4 className="m-0">
                    <strong>{this.state.UserFinalData.firstName}</strong>
                    {this.state.UserFinalData.onlineStatus === 1 ? <i className="fas fa-circle" /> : ""}
                  </h4>
                  <div className="cmeId">
                    <span>ID</span>
                    <span>{this.state.UserFinalData.findMateId}</span>
                  </div>
                </div>

                {/* User Gender Module*/}
                <div className="row MProfile" onClick={this.OpenEditablegenderDrawer}>
                  <div className="col-5 text-left Mbasic">
                    <p>Gender</p>
                  </div>
                  <div className="col-7 text-right Mbasicanswer">
                    <p className="Mrightarrow">
                      {this.state.UserFinalData.gender} <i class="fas fa-chevron-right pl-1"></i>
                    </p>
                  </div>
                </div>

                <div className="row MProfile" onClick={this.OpenEditableEmailIDDrawer}>
                  <div className="col-4 text-left Mbasic">
                    <p>Age</p>
                  </div>
                  <div className="col-8 text-right Mbasicanswer">
                    <p className="Mrightarrow">
                      {this.state.UserFinalData && this.state.UserFinalData.age && this.state.UserFinalData.age.value}
                      {/* <i class="fas fa-chevron-right pl-1"></i> */}
                    </p>
                  </div>
                </div>

                {/* User Mobile Number Module*/}
                <div className="row MProfile" onClick={this.OpenEditablephoneDrawer}>
                  <div className="col-5 text-left Mbasic">
                    <p>Phone</p>
                  </div>
                  <div className="col-7 text-right Mbasicanswer">
                    <p className="Mrightarrow">
                      {this.state.UserFinalData.mobileNumber}
                      <i class="fas fa-chevron-right pl-1"></i>
                    </p>
                  </div>
                </div>

                {/* User EmailID Module*/}
                <div className="row MProfile" onClick={this.OpenEditableEmailIDDrawer}>
                  <div className="col-4 text-left Mbasic">
                    <p>Email</p>
                  </div>
                  <div className="col-8 text-right Mbasicanswer">
                    <p className="Mrightarrow">
                      {this.state.UserFinalData.emailId}
                      <i class="fas fa-chevron-right pl-1"></i>
                    </p>
                  </div>
                </div>

                {/* User Location Module*/}
                {/* <div className="row MProfile" onClick={this.OpenEditablelocationDDrawer}> */}
                <div className="row MProfile">
                  <div className="col-5 text-left Mbasic">
                    <p>Location</p>
                  </div>
                  <div className="col-7 text-right Mbasicanswer">
                    <p className="Mrightarrow">
                      {getCookie("selectedLocation") != null
                        ? JSON.parse(getCookie("selectedLocation")).address
                        : getCookie("location") != null
                        ? getCookie("location")
                        : JSON.parse(getCookie("baseAddress")).address}
                      <i class="fas fa-chevron-right pl-1"></i>
                    </p>
                  </div>
                </div>

                {/* User About Us Start */}
                <div className="MUserAbout">
                  <p>
                    <strong>About You</strong>
                  </p>
                  <textarea
                    value={this.state.aboutPayload}
                    onChange={this.handlename}
                    placeholder="Tell something about you."
                    style={{
                      border: "none",
                      width: "100%",
                      borderBottom: "1px solid",
                      marginBottom: "10px",
                    }}
                  />
                </div>

                {/* OtherUser Prefence Module */}
                {this.state.UserFinalData &&
                  this.state.UserFinalData.myPreferences.map((data, index) => (
                    <div key={index} className="m-0 py-3 border-bottom">
                      <div className="m_userpro_header">
                        <h5>{data.title}</h5>
                      </div>
                      {data.data.map((prefItem, childIndex) => (
                        <div
                          key={"myPref--" + childIndex}
                          className="MProfile"
                          onClick={this.OpenEditablePrefrenceDDrawer.bind(this, prefItem, index)}
                        >
                          <div className="row">
                            <div className="col-4 text-left Mbasic">
                              <p>{prefItem.label}</p>
                            </div>
                            <div className="col-8 text-right MLoopbasicanswer">
                              <p className="Mrightarrow">
                                {prefItem.selectedValues && prefItem.selectedValues.length > 0 ? prefItem.selectedValues.join(",") : "N/A"}{" "}
                                <i class="fas fa-chevron-right pl-1"></i>
                              </p>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  ))}

                <div className="row mx-0 py-3">
                  <div className="col-6 px-0">
                    <div className="m_userpro_header">
                      <h5>Moments</h5>
                    </div>
                  </div>
                  <div
                    className="col-6 text-right px-0"
                    style={{
                      fontFamily: "Circular Air Bold",
                      color: "#F1F1F1",
                    }}
                    onClick={() => this.toggleMoments()}
                  >
                    View All
                  </div>
                  {this.state.UserFinalData && this.state.UserFinalData.moments.length > 0
                    ? this.state.UserFinalData.moments.slice(0, 6).map((k, i) => (
                        <div
                          className="col-4 m_moments_boxes"
                          id="m_moments_boxes"
                          key={i}
                          style={{ height: this.state.squareWH }}
                          onClick={() => this.toggleMoments(k)}
                        >
                          {k.url[0].includes("mp4") ? (
                            <video src={k.url[0]} alt={k.description}></video>
                          ) : (
                            <img src={k.url[0]} alt={k.description} />
                          )}
                        </div>
                      ))
                    : ""}
                </div>

                {/* User Other Information Module */}
                <div className="pt-2 MUserextra">
                  <p>
                    <strong>Other</strong>
                  </p>
                  {/* User Age Module */}
                  <div className="row MProfileshow">
                    <div className="col-7 text-left">
                      <p>Don't show my age</p>
                    </div>
                    <div className="col-5 pr-0 text-right">
                      <Switch checked={this.state.checkedA} onChange={this.handleChangeAge} value="checkedA" />
                    </div>
                  </div>

                  {/* User Distance Module */}
                  <div className="row MProfileshow border-bottom">
                    <div className="col-7 text-left">
                      <p>Don't show my distance</p>
                    </div>
                    <div className="col-5 pr-0 text-right">
                      <Switch checked={this.state.checkedB} onChange={this.handleChangeDist} value="checkedB" />
                    </div>
                  </div>
                </div>
              </div>

              {/* Snakbar Components */}
            </section>
          </div>
        )}
        <Snackbar
          timeout={2500}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          onClose={this.handleClose}
        />
        {this.state.prevAboutPayload !== this.state.aboutPayload ? (
          <button className="m_saveLocation_circle" onClick={this.updateAbout}>
            <img src={Icons.checked} height={25} width={25} alt="checked" />
          </button>
        ) : (
          // <div className="m_upload-about">
          //   <ArrowNext onClick={this.updateAbout} />
          // </div>
          ""
        )}
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
    UserProfile: state.UserProfile.UserProfile,
    userProfilePicture: state.UserProfile.UserProfile.UserProfilePic,
  };
}

export default connect(mapstateToProps, null)(withStyles(styles, { withTheme: true })(BaseUserProfile));
