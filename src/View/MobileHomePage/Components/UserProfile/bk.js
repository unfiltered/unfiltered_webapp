// Main React Components
import React, { Component } from "react";

// Scss
import "./UserProfile.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

// Redux Components
import { connect } from "react-redux";
import { UserProfileNewData } from "../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../actions/User";

const styles = () => ({
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px"
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px"
  },
  progress: {
    color: "#e31b1b",
    marginTop: "25vh",
    textAlign: "center"
  },
  arrow: {
    color: "#fff",
    backgroundColor: "#f74167",
    padding: 15,
    borderRadius: "50%"
  },
  disabledarrow: {
    color: "#fff",
    backgroundColor: "#b7b7b7",
    padding: 15,
    borderRadius: "50%"
  }
});

let addmorephoto = [];
let deletedarr = [];

class ProfileImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      value: "",
      files: [],
      newFile: [],
      loader: false,
      ShowPhotoFormat: false
    };
    this.handlephotochange = this.handlephotochange.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }

  // handlephotochange(event) {
  handlephotochange(event) {
    let wholeFileInput = this.state.files;
    let reader = new FileReader();

    let obj = {
      fileKey: Date.now(),
      fileData: event.target.files
    };

    let flInput;
    let filename;
    let fileExtension;

    reader.onload = function(e) {
      flInput = document.getElementById("ProfileFiles").value;
      filename = flInput.replace(/^.*[\\\/]/, "");
      fileExtension = filename.split(".")[1];
      obj["filePreview"] = e.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    setTimeout(() => {
      if (fileExtension === "jpg" || fileExtension === "jpeg" || fileExtension === "png") {
        wholeFileInput.push(obj);

        this.setState({
          files: wholeFileInput,
          ShowPhotoFormat: false
        });

        if (this.state.files !== "") {
          this.setState({ valid: true });
        }

        return;
      } else {
        this.setState({ ShowPhotoFormat: true });
      }
    }, 100);
  }

  // Function to Map the Multiple File Upload
  uploadFiles = () => {
    return new Promise((resolve, reject) => {
      let x = this.state.files && this.state.files.length > 0 ? this.state.files.map(async (file, index) => await this.fileHandler(file)) : "";
      return resolve(1);
    });
  };

  // Function for the to Upload the Userimges on the Amazon S3 Server o create Images Online links
  fileHandler = files => {
    return new Promise((resolve, reject) => {
      window.AWS.config.update({
        accessKeyId: "AKIATONSSUTMLIIE534E",
        secretAccessKey: "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",
        region: "us-east-1"
      });

      var s3 = new window.AWS.S3();

      var params = {
        Bucket: "datum-clone",
        Key: "users/Pro_Pic/" + "ProPic_" + Date.now(),
        ContentType: files.fileData[0].type,
        Body: files.fileData[0],
        ACL: "public-read"
      };
      s3.putObject(params, (err, res) => {
        if (err) {
          return reject(false);
        } else {
          let data = {
            identityCard: "https://datum-clone.s3.amazonaws.com/" + params.Key
          };

          addmorephoto.push(data.identityCard);

          // Action Dispatch for User Uploaded UserPorfile Pic
          this.props.dispatch(MOBILE_ACTION_FUNC("otherImages", addmorephoto));

          return resolve(addmorephoto);
        }
      });
    });
  };

  // To Delete a Photo (Jugad)
  handleDeleteProfile = item => {
    this.setState({});
    deletedarr.push(item);
    return deletedarr;
  };

  // UserProfile API Call Module
  profileapi = () => {
    this.setState({ loader: true });
    let promises = this.state.files.map(file => this.fileHandler(file));

    Promise.all(promises)
      .then(data => {
        // Merging the new uploaded images with old images
        this.setState(
          {
            otherImagesArr: [...this.state.otherImagesArr, ...data]
          },
          () => {
            // calling the profile pic changes manually, to manage setup new profile picture, from new uploaded images
            data &&
              data.map((newImage, index) => {
                this.handleProfilePicChanges(this.state.currentType, newImage);
              });
          }
        );

        let profilePayload = {
          ...this.props.UserData,
          profilePic: this.state.profilePicArr[0].toString(),
          otherImages: this.state.otherImagesArr
        };

        UserProfileNewData(profilePayload).then(data => {
          this.setState({ loader: false }, () => {
            addmorephoto = [];
          });
        });
      })
      .catch(function(err) {});
  };

  handleProfilePicChanges = (type, event) => {
    // this.setState({ valid: true });
    let tempProfPicArray = [...this.state.profilePicArr];
    let tempOtherImgArray = [...this.state.otherImagesArr];

    let currentUrl;
    let isDirectUrl = false;
    if (event && event.target) {
      currentUrl = event.target.value;
      isDirectUrl = false;
    } else {
      currentUrl = event;
      isDirectUrl = true;
    }

    switch (type) {
      case 1:
        tempOtherImgArray.push(tempProfPicArray[0]); // pushing the old profile pic into other images
        let indexProf =
          tempOtherImgArray.findIndex(url => url === currentUrl) > -1
            ? tempOtherImgArray.splice(
                tempOtherImgArray.findIndex(url => url === currentUrl),
                1
              )
            : ""; // removing the picture from other images array which pushed as a profile picture

        tempProfPicArray[0] = currentUrl;
        break;

      case 2:
        // if url is from function (not from UI), just add the current url to otherImages array, and return from here itself.
        if (isDirectUrl) {
          tempOtherImgArray.push(currentUrl);
          return;
        }

        tempOtherImgArray.push(tempProfPicArray[0]); // pushing the old profile pic into other images

        tempProfPicArray[0] = currentUrl;
        let index =
          tempOtherImgArray.findIndex(url => url === currentUrl) > -1
            ? tempOtherImgArray.splice(
                tempOtherImgArray.findIndex(url => url === currentUrl),
                1
              )
            : ""; // removing the picture from other images array which pushed as a profile picture

        break;
      case 3:
        break;
    }
    this.setState(
      {
        profilePicArr: tempProfPicArray,
        otherImagesArr: tempOtherImgArray,
        valid: true,
        currentType: type
      },
      () => {
        console.log("sadasd", type, this.state.profilePicArr, this.state.otherImagesArr);
      }
    );
  };

  componentDidMount = () => {
    setTimeout(() => {
      let userProfilePic = document.getElementById("userProfilePic") ? (document.getElementById("userProfilePic").checked = true) : "";
    }, 500);

    this.setState({
      profilePicArr: [this.props.UserFinalData.profilePic],
      otherImagesArr: this.props.UserFinalData.otherImages
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className="py-3 MCommonwidth" style={{ height: "100vh" }}>
        {/* Header Module */}
        <div className="col-12">
          <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
          <div className="MCommonheader pt-3">
            <p>Edit</p>
            <p>Profile</p>
          </div>

          {/* Add New Video Module */}
          <div className="py-3 border-bottom">
            <p>
              <strong>Video</strong>
            </p>
            <button className="Mdatumplus">Add Profile Video</button>
          </div>

          <div>
            {this.state.loader ? (
              <div className="text-center">
                <CircularProgress className={classes.progress} />
              </div>
            ) : (
              <div>
                {/* Add New Photo Module */}
                <div className="pt-3 MUpload">
                  <p>
                    <strong>Photo's</strong>
                  </p>

                  <div className="row">
                    <div className="col-4">
                      {/* Current User Profile Picture */}
                      <div className="text-center MUser_setprofile">
                        <img
                          src={this.props.UserFinalData.profilePic}
                          className="p-2"
                          alt={this.props.UserFinalDatafirstName}
                          title={this.props.UserFinalDatafirstName}
                          width="120px"
                        />
                        <input
                          type="radio"
                          id="userProfilePic"
                          name="radio-group"
                          onChange={this.handleProfilePicChanges.bind(this, 1)}
                          value={this.props.UserFinalData.profilePic}
                        />
                        <label className="Muserphoto_currentpic" htmlFor="userProfilePic">
                          <span>Set as profile pic.</span>
                        </label>
                      </div>

                      {/* other profie pictures from API */}
                      {this.props.UserFinalData &&
                        this.props.UserFinalData.otherImages &&
                        this.props.UserFinalData.otherImages.map((item, imgIndex) =>
                          item === deletedarr[imgIndex] ? (
                            ""
                          ) : (
                            <div className="text-center MUser_setprofile" key={"img-db-" + imgIndex}>
                              <img src={item} className="p-2" width="120px" alt="deom" title="demo" />
                              <input
                                type="radio"
                                id={"img-db-top-" + imgIndex}
                                name="radio-group"
                                onChange={this.handleProfilePicChanges.bind(this, 2)}
                                value={item}
                              />
                              <label className="Muserphoto_currentpic" htmlFor={"img-db-top-" + imgIndex}>
                                <span>Set as profile pic.</span>
                              </label>

                              {/* Close Button Module */}
                              <i className="fas fa-times" onClick={this.handleDeleteProfile.bind(this, item)} />
                            </div>
                          )
                        )}
                    </div>

                    <div className="col-4 MUser_uploadphoto">
                      {/* Image Upload Module */}
                      {this.state.files.length <= 3 ? (
                        <form encType="multipart/form-data">
                          <label className="col-auto" htmlFor="ProfileFiles">
                            <i className="fas fa-camera" />
                            <input type="file" id="ProfileFiles" className="p-2 d-none Wuser_input" onChange={this.handlephotochange} />
                          </label>
                        </form>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>

                  {/*  Uploaded Img Show Module  - New uploaded images */}
                  <div className="row Muser_profile">
                    {this.state.files
                      ? this.state.files.map((data, index) => (
                          <div className="col-4 MUser_setprofile">
                            <img key={index} src={data.filePreview} className="p-2" alt="User" title="User" width="120px" />
                            <input
                              type="radio"
                              id={"Newimg1-mobi-" + index}
                              name="radio-group"
                              onChange={this.handleProfilePicChanges.bind(this, 3)}
                            />
                            <label className="Muserphoto_currentpic" htmlFor={"Newimg1-mobi-" + index}>
                              <span>Set as profile pic.</span>
                            </label>
                            {/* Close Button Module */}
                            <i className="fas fa-times" onClick={this.handleDeleteProfile.bind(this)} />
                          </div>
                        ))
                      : ""}
                  </div>
                </div>

                {/* Checking Photo Upload Photo Format */}
                {this.state.ShowPhotoFormat ? (
                  <p
                    style={{
                      fontSize: "14px",
                      color: "red",
                      fontWeight: "600",
                      margin: "0"
                    }}
                  >
                    Please choose the valid photo format.
                  </p>
                ) : (
                  ""
                )}

                {/* Save Butoon Module */}
                <button className="MProfile_addsave" onClick={this.profileapi} disabled={!this.state.valid}>
                  <i className={"fas fa-check " + (this.state.valid ? classes.arrow : classes.disabledarrow)} />
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
    UserProfile: state.UserProfile.UserProfileData
  };
}

export default connect(mapstateToProps, null)(withStyles(styles)(ProfileImage));
