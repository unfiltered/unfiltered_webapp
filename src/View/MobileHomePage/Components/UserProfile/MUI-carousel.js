import React from "react";
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators } from "reactstrap";
import { Images } from "../../../../Components/Images/Images";

class CustomCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
    this.state = {
      activeIndex: 0,
      animating: false,
      items: [],
      indexOfCarousel: 0,
    };
  }

  componentDidMount() {
    let profilePic = this.props && this.props.userProfileInfo && this.props.userProfileInfo.profilePic;
    let otherImages = this.props && this.props.userProfileInfo && this.props.userProfileInfo.otherImages;
    let video = this.props && this.props.userProfileInfo && this.props.userProfileInfo.profileVideo;
    // console.log('UserFinalData', [...otherImages])
    if (video.length === 0) {
      this.setState({ items: [{url: profilePic, timestamp: new Date().getTime()}, ...otherImages] });
    } else {
      this.setState({ items: [{url:video, timestamp: new Date().getTime()}, {url: profilePic, timestamp: new Date().getTime()}, ...otherImages] });
    }
  }

  setActiveIndex = (index) => {
    this.setState({ activeIndex: index });
  };

  setAnimating = (bool) => {
    this.setState({ animating: bool });
  };

  next = () => {
    if (this.state.animating) return;
    let video = this.videoRef && this.videoRef.current;
    const nextIndex = this.state.activeIndex === this.state.items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setActiveIndex(nextIndex);
    try {
      if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
        video.pause();
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  previous = () => {
    if (this.state.animating) return;
    let video = this.videoRef && this.videoRef.current;
    const nextIndex = this.state.activeIndex === 0 ? this.state.items.length - 1 : this.state.activeIndex - 1;
    this.setActiveIndex(nextIndex);
    try {
      if (video.currentTime > 0 && !video.paused && !video.ended && video.readyState > 2) {
        video.pause();
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  goToIndex = (newIndex) => {
    if (this.state.animating) return;
    this.setActiveIndex(newIndex);
  };

  onError = (e) => {
    e.target.src = Images.placeholder;
  };

  slides = () => {
    // let slides = [
    //     this.props && this.props.userProfileInfo && this.props.userProfileInfo.profilePic,
    //   ...(this.props && this.props.userProfileInfo && this.props.userProfileInfo.otherImages),
    // ].map((item, key) => {
    let slides = this.state.items.map((item, key) => {
      return (
        <CarouselItem onExiting={() => this.setAnimating(true)} onExited={() => this.setAnimating(false)} key={key}>
          {item && item.url.includes(".mp4") ? (
            <video src={item.url} controls alt={item.url} height={400} onError={this.onError} width={"100%"} ref={this.videoRef} />
          ) : (
            <img src={item.url} style={{ objectFit: "cover" }} alt={item} height={400} onError={this.onError} width={"100%"} />
          )}
        </CarouselItem>
      );
    });
    return slides;
  };

  render() {
    return (
      <Carousel activeIndex={this.state.activeIndex} next={this.next} previous={this.previous} className="UserImage">
        <CarouselIndicators items={this.state.items} activeIndex={this.state.activeIndex} onClickHandler={this.goToIndex} />
        {this.slides()}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}

export default CustomCarousel;
