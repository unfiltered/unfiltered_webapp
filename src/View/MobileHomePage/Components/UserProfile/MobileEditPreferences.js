import React from "react";
import { Icons } from "../../../../Components/Icons/Icons";
import { UserdataPrefence } from "../../../../controller/auth/verification";
import ArrowNext from "../../../../Components/MobileNextButton/NextButton";
import Input from "../../../../Components/Input/Input";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";
import "./UserProfile.scss";

const styles = (theme) => ({
  root: {
    color: "#FFF",
    "&$checked": {
      color: "#FFF",
    },
  },
  checked: {
    color: "#FFF",
    "&$checked": {
      color: "#FFF",
    },
  },
});

class MobileEditPreferences extends React.Component {
  state = {
    selectedData: "",
  };

  componentDidMount() {
    if ((this.props.dataToEdit && this.props.dataToEdit.type === 1) || (this.props.dataToEdit && this.props.dataToEdit.type === 5)) {
      this.setState({ selectedData: this.props.dataToEdit && this.props.dataToEdit.selectedValues[0] });
    } else if (this.props.dataToEdit && this.props.dataToEdit.type === 2) {
      this.setState({ selectedData: this.props.dataToEdit && this.props.dataToEdit.selectedValues });
    } else {
      if (this.props.dataToEdit && this.props.dataToEdit.selectedValues[0] === "NA") this.setState({ selectedData: "" });
    }
  }

  storeData = (data) => {
    this.setState({ selectedData: data });
  };

  textHandler = (e) => {
    this.setState({ selectedData: e.target.value });
  };

  updateData = () => {
    UserdataPrefence({
      pref_id: this.props.dataToEdit.pref_id,
      values: this.checkArrayOfArrays([this.state.selectedData])
        ? this.state.selectedData
        : this.state.selectedData === "Prefer Not To Say"
        ? []
        : [this.state.selectedData],
    })
      .then((res) => {
        this.props.onClose();
        this.props.getuserprofile(); // call API to update data
        console.log("successfully updated");
      })
      .catch((err) => console.log("failed to update"));
  };

  checkArrayOfArrays = (a) => {
    return a.every(function (x) {
      return Array.isArray(x);
    });
  };

  edit = (event) => {
    let sel = [...this.state.selectedData];
    if (sel.includes(event)) {
      let index = sel.indexOf(event);
      sel.splice(index, 1);
    } else {
      sel.push(event);
    }
    this.setState({
      selectedData: sel,
    });
  };

  handleChange = (event) => {
    this.setState({ selectedData: event.target.value });
  };

  render() {
    let { classes } = this.props;
    return (
      <div className="col-12 m_editView">
        <div className="row">
          <div className="col-12 py-3">
            <img src={Icons.PinkBack} alt="back-icon" height={20} width={25} onClick={this.props.onClose} />
          </div>
          <div className="col-12">
            <div className="m-label-common1">{this.props.dataToEdit && this.props.dataToEdit.title}</div>
            <div className="label-common2">{this.props.dataToEdit && this.props.dataToEdit.label}</div>
          </div>

          {this.props.dataToEdit.type === 1 &&
            this.props.dataToEdit &&
            this.props.dataToEdit.options.map((k) => (
              <div
                key={k}
                className={this.state.selectedData === k ? "col-12 m_edit_selected py-2" : "col-12 m_edit_notSelected py-2"}
                onClick={() => this.storeData(k)}
              >
                <div className="row">
                  <div className="col-10">{k}</div>
                  <div className="col-2"> {this.state.selectedData === k ? <i className="m_ep_pinkCheck fas fa-check" /> : <span />}</div>
                </div>
              </div>
            ))}
          {this.props.dataToEdit.type === 5 &&
            this.props.dataToEdit &&
            this.props.dataToEdit.options.map((k) => (
              <div className="col-12 m_edit_selected py-2" key={k}>
                <div className="row">
                  <div className="col-12 m_ep_editText">
                    <Input
                      onChange={this.textHandler}
                      value={this.state.selectedData}
                      placeholder={this.props.dataToEdit && this.props.dataToEdit.options[0]}
                    />
                  </div>
                </div>
              </div>
            ))}
          {this.props.dataToEdit.type === 2 &&
            this.props.dataToEdit &&
            this.props.dataToEdit.options.map((k) => (
              <div
                key={k}
                className={`${this.state.selectedData.includes(k) ? "col-12 m_edit_selected py-2" : "col-12 m_edit_notSelected py-2"}`}
                onClick={() => this.edit(k)}
              >
                <div className="row">
                  <div className="col-10">{k}</div>
                  <div className="col-2">
                    {" "}
                    {this.state.selectedData.includes(k) ? <i className="m_ep_pinkCheck fas fa-check" /> : <span />}
                  </div>
                </div>
              </div>
            ))}
        </div>

        <div className="MCommonbottomnextarrow">
          <ArrowNext disabled={false} onClick={this.updateData} />
        </div>
        <div className="m_ep_preferNotToSay">
          <RadioGroup aria-label="Pref" name="pref" value={this.state.selectedData} onChange={this.handleChange}>
            <FormControlLabel
              value="Prefer Not To Say"
              control={<Radio color="default" classes={{ root: classes.root }} />}
              label="Prefer Not To Say"
            />
          </RadioGroup>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(MobileEditPreferences);
