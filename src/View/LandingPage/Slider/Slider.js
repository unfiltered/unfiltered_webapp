// Main React Components
import React, { Component } from "react";

// Scss
import "./style.scss";

// Reusuable Components for Slider
import Slider from "../../../Components/Slider/index";

// Reactstrap Components
import { CarouselItem, CarouselCaption } from "reactstrap";

// Images
// import SliderOne from '../../../asset/images/Slider-1.jpg';
// import Slider2 from '../../../asset/images/Slider-2.jpg';
import Slider3 from "../../../asset/images/Slider-3.jpg";

const items = [
  {
    src: Slider3,
    altText: "UnFiltered App",
    caption: " Creative Consulting Agency For Smart Solutions. ",
    captiondec:
      " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.",
  },
  {
    src: Slider3,
    altText: "UnFiltered App",
    caption: " Creative Consulting Agency For Smart Solutions. ",
    captiondec:
      " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.",
  },
  {
    src: Slider3,
    altText: "UnFiltered App",
    caption: " Creative Consulting Agency For Smart Solutions. ",
    captiondec:
      " Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.",
  },
];

class LandingSlider extends Component {
  render() {
    const slides = items.map((item) => {
      return (
        <CarouselItem className="MainSlider" onExiting={this.onExiting} onExited={this.onExited} key={item.src}>
          <img src={item.src} alt={item.altText} />
          <CarouselCaption captionText={item.captiondec} captionHeader={item.caption} />
          {/* <div className="signupbuttonslider" >
            <PrimaryButton name="Log In" />
          </div> */}
        </CarouselItem>
      );
    });

    return (
      <div>
        <Slider item={items}>{slides}</Slider>
      </div>
    );
  }
}

export default LandingSlider;
