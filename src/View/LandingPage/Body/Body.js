// Main React Components
import React, { Component } from "react";

// Scss
import "./style.scss";

// Reusable Components for Button
import PrimaryButton from "../../../Components/PrimaryButton/PrimaryButton";

// Reactstrap Components
import { Col, Row } from "reactstrap";

// Images
import Services1 from "../../../asset/images/service1.jpg";

export default class LandingBody extends Component {
  render() {
    return (
      <div className="Section-BgColor py-5">
        <div className="container">
          <div className="MainHeading">
            <Col md="12" lg="12">
              <span>How We Work</span>
              <h2>Make Successful Business</h2>
              <div className="Section-Headingline" />
              <p className="mt-3 mb-0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <br /> incididunt ut labore et dolore magna aliqua.
              </p>
            </Col>
          </div>

          <div className="OurServices my-5">
            <Row>
              <Col md="12" lg="6">
                <div className="Section-Heading">
                  <h4>Welcome To UnFiltered App</h4>
                </div>
                <div className="Section-HeadingLeftline" />
                <div className="Section-Tectcontent">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div className="MainFeature">
                  <ul>
                    <li>Immigration consultant, Information technology consulting.</li>
                    <li>Consultant pharmacist Creative consultant</li>
                    <li>Employment consultant Environmental consultant</li>
                  </ul>
                </div>
                <div className="Section-Button">
                  <PrimaryButton name="Get Started" />
                </div>
              </Col>
              <Col md="12" lg="6">
                <img src={Services1} className="img-fluid Servicesimg" alt="Services1" title="Services1" />
              </Col>
            </Row>
          </div>

          {/* Desktop View for the Services */}
          <div className="DesktopView">
            <div className="OurServices my-5">
              <Row>
                <Col md="12" lg="6">
                  <img src={Services1} className="img-fluid Servicesimg" alt="Services2" title="Services2" />
                </Col>
                <Col md="12" lg="6">
                  <div className="Section-Heading">
                    <h4>Welcome To Unfiltered App</h4>
                  </div>
                  <div className="Section-HeadingLeftline" />
                  <div className="Section-Tectcontent">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                  <div className="MainFeature">
                    <ul>
                      <li>Immigration consultant, Information technology consulting.</li>
                      <li>Consultant pharmacist Creative consultant</li>
                      <li>Employment consultant Environmental consultant</li>
                    </ul>
                  </div>
                  <div className="Section-Button">
                    <PrimaryButton name="Get Started" />
                  </div>
                </Col>
              </Row>
            </div>
          </div>

          {/* Mobile View for the Services */}
          <div className="MobileView">
            <div className="OurServices my-5">
              <Row>
                <Col md="12" lg="6">
                  <div className="Section-Heading">
                    <h4>Welcome To Unfiltered App</h4>
                  </div>
                  <div className="Section-HeadingLeftline" />
                  <div className="Section-Tectcontent">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                  <div className="MainFeature">
                    <ul>
                      <li>Immigration consultant, Information technology consulting.</li>
                      <li>Consultant pharmacist Creative consultant</li>
                      <li>Employment consultant Environmental consultant</li>
                    </ul>
                  </div>
                  <div className="Section-Button">
                    <PrimaryButton name="Get Started" />
                  </div>
                </Col>
                <Col md="12" lg="6">
                  <img src={Services1} className="img-fluid Servicesimg" alt="Services2" title="Services2" />
                </Col>
              </Row>
            </div>
          </div>

          <div className="OurServices my-5">
            <Row>
              <Col md="12" lg="6">
                <div className="Section-Heading">
                  <h4>Welcome To Unfiltered App</h4>
                </div>
                <div className="Section-HeadingLeftline" />
                <div className="Section-Tectcontent">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
                <div className="MainFeature">
                  <ul>
                    <li>Immigration consultant, Information technology consulting.</li>
                    <li>Consultant pharmacist Creative consultant</li>
                    <li>Employment consultant Environmental consultant</li>
                  </ul>
                </div>
                <div className="Section-Button">
                  <PrimaryButton name="Get Started" />
                </div>
              </Col>
              <Col md="12" lg="6">
                <img src={Services1} className="img-fluid Servicesimg" alt="Services3" title="Services3" />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
