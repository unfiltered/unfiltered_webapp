// Main React Components
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

// Enzyme TestCase Module Components
import { shallow, configure, mount, render } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

// PropType Component
import checkPropTypes from "check-prop-types";

// Imported Components
import MyComponent from "../SeemeID";

// Redux Components
import { Provider } from "react-redux";
import { IntlProvider } from "react-intl";
import { store } from "../../../../../../store";

configure({ adapter: new Adapter() });

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<MyComponent {...props} />);
  if (state) wrapper.setstate(state);
  return wrapper;
};

describe("<SeemeID />", () => {
  it("should be Defined", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper).toBeDefined();
  });

  it("should render correctly", () => {
    const wrapper = setup()
      .first()
      .shallow();
  });

  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.unmountComponentAtNode(div);
  });

  it("renders component with main one '<div>' tag", () => {
    const wrapper = setup()
      .first()
      .shallow();
    const SeemeButton = wrapper.find('[data-test="component-Seeme"]');
    expect(SeemeButton.length).toBe(1);
  });

  it("Checking 'input' tag", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper.find("input").length).toEqual(1);
  });

  it("Checking 'form' tag", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper.find("form").length).toEqual(1);
  });


  it("at the time of render newValue State Should be empty", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper.state("newValue")).toEqual([]);
  });

  it("at the time of render newradiovalue State Should be empty", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper.state("newradiovalue")).toEqual("");
  });

  it("at the time of render newselectedOption State Should be empty", () => {
    const wrapper = setup()
      .first()
      .shallow();
    expect(wrapper.state("newselectedOption")).toEqual("");
  });

  it("does not throw warning with expected props", () => {
    const expectedProps = { success: false };
    const propError = checkPropTypes(
      MyComponent.propTypes,
      expectedProps,
      "prop"
    );
    expect(propError).toBeUndefined();
  });

  it("componentDidMount Calls onload", () => {
    const wrapper = jest.spyOn(MyComponent.prototype, "componentDidMount");
    expect(wrapper);
  });
});
