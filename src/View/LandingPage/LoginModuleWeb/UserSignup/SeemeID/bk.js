// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";

// Scss
import "./SeemeID.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// Redux Components
import { connect } from "react-redux";
import { getPreference, UserdataPrefence, UserProfileNewData, UpdateSeemeID } from "../../../../../controller/auth/verification";
import { getCookie } from "../../../../../lib/session";

// Material-Ui Components
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Checkbox from "@material-ui/core/Checkbox";
import Done from "@material-ui/icons/Done";

// MultiSelect Dropdown Components for UserHeight Input
import Select from "react-select";

// Validation Components
import { ValidateName } from "../../../../../Validation/Validation";

// Material Inline Color Components Styles
const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  root: {
    width: "99%",
    backgroundColor: theme.palette.background.paper,
    paddingBottom: "100px",
  },
  rootRadio: {
    color: "#e31b1b",
    "&$checked": {
      color: "#e31b1b",
    },
    height: "25px",
    width: "40px",
    fontSize: "20px",
    float: "left",
    left: "0px !important",
  },
  label: {
    display: "unset",
    margin: "7px 0px",
  },
  labelRadio: {
    padding: "7px 0px",
    margin: "0px",
    display: "flex",
    fontSize: "16px",
  },
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
});

// Globally Constant Variables
let currentPrefId = "";
let currentlabel = "";
let currentheight = "";
let multiselecthoobies = "";
let multiselectchecbox = [];
let UserSignupSeemeId = "";

class SeemeID extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {},
      errors: {},
      valid: false,
      value: false,
      selectedScreenIndex: 0,
      selectedPrefIndex: 0,
      Preitem: "",
      newValue: [],
      newradiovalue: "",
      newselectedOption: "",
      seemenamePayload: {},
    };
    this.radioChange = this.radioChange.bind(this);
  }

  componentDidMount() {
    UserSignupSeemeId = getCookie("findmateid");
    console.log("UserSignupSeemeid", UserSignupSeemeId);
    setTimeout(() => {
      document.getElementById("activeinput").focus();
    }, 100);
  }

  // Function for Scrren Change onClick of Skip Button
  handleskipscreen = () => {
    let currentScreenLength = this.state.PrefernceData.myPreferences[this.state.selectedScreenIndex].data.length;

    if (this.state.selectedScreenIndex === this.state.PrefernceData.myPreferences.length - 1 && this.state.selectedPrefIndex === currentScreenLength - 1) {
      this.postPreferences();
      console.log("all api....");

      this.props.history.push("/app/");
    } else {
      if (this.state.selectedPrefIndex === currentScreenLength - 1) {
        this.setState({
          selectedScreenIndex: parseInt(this.state.selectedScreenIndex + 1),
          selectedPrefIndex: 0,
        });
        return;
      }

      // To Skip a Screen from API Data
      this.setState({
        selectedPrefIndex: parseInt(this.state.selectedPrefIndex) + 1,
        value: false,
      });
    }
  };

  // Function for Scrren Change onClick of Skip Button
  handlebackscreen = () => {
    this.setState({
      selectedPrefIndex: parseInt(this.state.selectedPrefIndex) - 1,
    });
  };

  // Function for the Radio Button
  radioChange(status, value) {
    console.log("asdasd", status.target.value);
    this.setState(
      {
        value: status.target.value,
        // value: status
      },
      () => {
        console.log("Selectedradiobtnvalue", this.state.value);
      }
    );
  }

  // Function for the prefer Not to say Button
  handlenotsayradioChange = (status) => {
    this.setState({
      value: status.target.value,
    });
  };

  // Function for the CheckBox Select
  handleChange = (event, value, currentlabel) => {
    this.setState({ value: event.target.value });
    console.log("Selected-Checkbox-Value--->>> ", event.target.value);
  };

  // Function for the Checking Multiple Input (Hobbies) List
  handlemultiselect = (event, value) => {
    if (event.target.checked) {
      multiselectchecbox.push(event.target.value);
    } else {
      multiselectchecbox.slice(event.target.value);
    }
    this.setState({ value: multiselectchecbox });
    console.log("multiselect--value", this.state.value);
  };

  // Function for the User Input Value
  handleUser = (event, value, currentlabel) => {
    ValidateName(event)
      ? this.setState({ nameErr: "", valid: true })
      : this.setState({
          nameErr: "Please Enter valid Details",
          valid: false,
        });
    console.log("Selected-Text-Value--->>>", event.target.value);
    this.setState({ value: event.target.value });
  };

  // Function to Chnage the Seeme ID
  handleseemeid = (event) => {
    this.setState({ seemenamePayload: { seemeid: event.target.value } }, () => {
      console.log("NewValuefirstName", this.state.seemenamePayload.seemeid);
    });
  };

  getInput = (type, prefItem) => {
    const { classes } = this.props;

    // Transfering Map(Array) Data to Constant Value
    currentPrefId = prefItem.pref_id;
    currentlabel = prefItem.label;
    currentheight = prefItem.priority;
    multiselecthoobies = prefItem.priority;

    // Checking Type from the API Data & Rendring the Components
    switch (type) {
      // For the Input
      case 5:
        let inputBox = (
          <div>
            <input
              refs="name"
              name="name"
              type="text"
              size="30"
              id="activeinput"
              onChange={this.handleUser}
              placeholder={prefItem.options ? prefItem.options[0] : prefItem.label}
              // id="seemeqa"
            />
            <div className="WNotificationmessage">
              <span>This is how it will appear in SeeMe.</span>
            </div>
          </div>
        );
        return inputBox;

      // For the Checkbox
      case 1:
        let multiCheckInput = (
          <div className="col-12">
            <div className="row justify-content-center">
              {prefItem.options && prefItem.options.length > 0
                ? prefItem.options.map((option) => (
                    <div className="col-auto Wsignupapiradiochecked_btn">
                      <input type="radio" id={option} name="radio-group" value={option} checked={this.state.value === option} onChange={this.radioChange} />
                      <label htmlFor={option}>{option}</label>
                    </div>
                  ))
                : ""}
            </div>
          </div>
        );
        return multiCheckInput;

      // For the Multi-SelectCheckbox || Single-SelectCheckbox
      case 2:
        var Multiselectpriority =
          multiselecthoobies === 1 ? (
            <FormControl className="col-12" component="fieldset">
              <RadioGroup name="sortFilter" value={this.state.value} onChange={this.handlemultiselect} className={classes.radioGroup}>
                <div className="row justify-content-center">
                  {prefItem.options && prefItem.options.length > 0
                    ? prefItem.options.map((option) => (
                        <div className="col-auto">
                          <FormControlLabel
                            value={option}
                            control={
                              <Checkbox
                                classes={{
                                  root: classes.rootRadio,
                                  checked: classes.checked,
                                }}
                                checkedIcon={<Done className={classes.sizeIcon} />}
                              />
                            }
                            classes={{
                              root: classes.labelRadio,
                              label: classes.labelRadio,
                            }}
                            label={option}
                          />
                        </div>
                      ))
                    : ""}
                </div>
              </RadioGroup>
            </FormControl>
          ) : (
            <div className="col-12">
              <div className="row justify-content-center">
                {prefItem.options && prefItem.options.length > 0
                  ? prefItem.options.map((option) => (
                      <div className="col-auto Wsignupapiradiochecked_btn">
                        <input type="radio" id={option} name="radio-group" value={option} checked={this.state.value === option} onChange={this.radioChange} />
                        <label htmlFor={option}>{option}</label>
                      </div>
                    ))
                  : ""}
              </div>
            </div>
          );
        return Multiselectpriority;

      // For the Checkbox
      case 10:
        let options = [];
        this.state.PrefernceData.myPreferences[2].data[3].options.map((data) =>
          options.push({
            label: data,
            value: data,
          })
        );

        var SingleMultiselect =
          // Checking Array data according to the Proprity
          currentheight === 11 ? (
            <div className="col-12">
              <div className="row justify-content-center">
                <div className="col-md-6 col-lg-6 col-xl-6 WUser_heightsel">
                  <Select onChange={this.handleUserSelects} options={options} />
                </div>
              </div>
            </div>
          ) : (
            // For the Radiobox
            <div className="col-12">
              <div className="row justify-content-center">
                {prefItem.options && prefItem.options.length > 0
                  ? prefItem.options.map((option) => (
                      <div className="col-auto Wsignupapiradiochecked_btn">
                        <input type="radio" id={option} name="radio-group" value={option} checked={this.state.value === option} onChange={this.radioChange} />
                        <label htmlFor={option}>{option}</label>
                      </div>
                    ))
                  : ""}
              </div>
            </div>
          );
        return SingleMultiselect;

      // For the radiobutton
      case 10:
        multiCheckInput = (
          <div className="col-12">
            <div className="row justify-content-center">
              {prefItem.options && prefItem.options.length > 0
                ? prefItem.options.map((option) => (
                    <div className="col-auto Wsignupapiradiochecked_btn">
                      <input type="radio" id={option} name="radio-group" value={option} checked={this.state.value === option} onChange={this.radioChange} />
                      <label htmlFor={option}>{option}</label>
                    </div>
                  ))
                : ""}
            </div>
          </div>
        );
        return multiCheckInput;
    }
  };

  // Function for the Height
  handleUserSelects = (event) => {
    console.log("event...", event);
    this.setState({ value: event.value });
    console.log("event...", this.state.value);
  };

  handlePrefernce = () => {
    // API Call for to Update the Seeme Id
    let UpdateidPayload = {
      findMateId: this.state.seemenamePayload.seemeid,
    };

    UpdateSeemeID(UpdateidPayload).then((data) => {
      console.log("updatedSeemeID", data);
    });

    console.log("newToekn", getCookie("token"));
    if (!this.state.PrefernceData) {
      getPreference().then((data) => {
        console.log("APIDATA", data);
        let finalPrefData = data.data.data.myPreferences.map((pref) => {
          console.log("one by one", pref);
          pref.data.sort((a, b) => a.priority - b.priority);
          console.log("after sort", pref.data);
        });
        this.setState({
          PrefernceData: data.data.data,
        });
      });
    } else {
      let currentScreenLength = this.state.PrefernceData.myPreferences[this.state.selectedScreenIndex].data.length;

      if (this.state.selectedScreenIndex === this.state.PrefernceData.myPreferences.length - 1 && this.state.selectedPrefIndex === currentScreenLength - 1) {
        this.postPreferences();
        console.log("all api....");

        this.props.history.push("/app/");
      } else {
        console.log("ahed");
        this.postPreferences();

        if (this.state.selectedPrefIndex === currentScreenLength - 1) {
          this.setState({
            selectedScreenIndex: parseInt(this.state.selectedScreenIndex + 1),
            selectedPrefIndex: 0,
          });
          return;
        }

        // To Skip a Screen from API Data
        this.setState({
          selectedPrefIndex: parseInt(this.state.selectedPrefIndex) + 1,
          value: false,
        });
      }
    }
  };

  // API Call Payload Data
  postPreferences = () => {
    let PrefUser = {
      pref_id: currentPrefId,
      values: [this.state.value],
    };

    // Function for the API Call
    UserdataPrefence(PrefUser).then((data) => {
      console.log("deeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", data);
    });
  };

  render() {
    console.log("userdaddta", this.props);

    const { classes } = this.props;
    const { selectedScreenIndex } = this.state;

    return (
      // this Module Used for the SeemeID Screen render
      <div className="py-3">
        <div className="col-12">
          <div className="row justify-content-center">
            {!this.state.PrefernceData ? (
              <div className="text-center col-12 col-sm-10 col-md-10 col-lg-8 col-xl-6 signupinput_module">
                <div className="py-1">
                  <h3>
                    Your <br />
                    <span style={{ color: "#e31b1b", fontWeight: "600" }}>SeeMe ID</span>
                  </h3>
                  <div className="right_skipbtn" onClick={this.handlePrefernce}>
                    <p className="m-0">Skip</p>
                  </div>
                  <div className="pt-4 W_userdatainput">
                    <input
                      refs="name"
                      name="name"
                      type="text"
                      size="30"
                      id="activeinput"
                      value={
                        this.state.seemenamePayload.seemeid && this.state.seemenamePayload.seemeid.length > 0
                          ? this.state.seemenamePayload.seemeid
                          : getCookie("findmateid")
                      }
                      onChange={this.handleseemeid}
                    />
                  </div>
                  <div className="WNotificationmessage">
                    <span>Your desired SeeMe ID.</span>
                  </div>
                  <div className="pt-3 text-center">
                    <button className={classes.btn} onClick={this.handlePrefernce}>
                      Next
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>

          {/* this Module Used for the All API Screen Data render */}
          {/* mapping loop on the api data for rendering ui screens */}
          {this.state.PrefernceData && this.state.PrefernceData.myPreferences && this.state.PrefernceData.myPreferences
            ? this.state.PrefernceData.myPreferences.map((pref, index) =>
                // check if the current selected screen is same as current index
                selectedScreenIndex === index
                  ? pref.data.map((prefItem, key) =>
                      // check if the current selected screen is same as current index
                      this.state.selectedPrefIndex === key ? (
                        <div className="py-3">
                          <div className="col-12">
                            <div className="row justify-content-center">
                              <div className="text-center col-12 col-sm-10 col-md-10 col-lg-8 col-xl-6 signupinput_module">
                                <div className="py-1">
                                  <h3>
                                    {prefItem.title} <br />
                                    <span
                                      style={{
                                        color: "#e31b1b",
                                        fontWeight: "600",
                                      }}
                                    >
                                      {prefItem.label}
                                    </span>
                                  </h3>
                                  {/* to skip a current screen */}
                                  <div className="right_skipbtn" onClick={this.handleskipscreen}>
                                    <p className="m-0">Skip</p>
                                  </div>
                                  {/* Pefer not to say Button */}
                                  <div className="Wuser_notsay">
                                    <input
                                      type="radio"
                                      id="Pefer not to say"
                                      name="radio-group"
                                      value={"Pefer not to say"}
                                      onChange={this.handlenotsayradioChange}
                                    />
                                    <label htmlFor="Pefer not to say">Prefer not to say</label>
                                  </div>
                                </div>
                                <div className="pt-4 W_userdatainput">
                                  {/* get the user input dynamically based on type */}
                                  {this.getInput(prefItem.type, prefItem, prefItem.priority, this.state.PrefernceData.myPreferences[2].data[3].options)}
                                </div>
                                <div className="pt-3 text-center">
                                  <button
                                    onClick={this.handlePrefernce}
                                    className={!this.state.value ? classes.disablebtn : classes.btn}
                                    disabled={!this.state.value}
                                  >
                                    Next
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )
                    )
                  : ""
              )
            : ""}
        </div>
      </div>
    );
  }
}

export default withRouter(withStyles(styles)(SeemeID));
