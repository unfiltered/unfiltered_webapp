import React from "react";
import "../OtpInput/OtpInput.scss";
import { connect } from "react-redux";
import "../ProfileInput/ProfileInput.scss";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import StartWithDatum from "../StartWithDatum/StartWithDatum";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { setCookie, getCookie } from "../../../../../lib/session";
import Message from "../../../../../message";
import { profileVerification, referralCodeExistVerification } from "../../../../../controller/auth/verification";

const styles = () => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    width: "94%",
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  btn: {
    color: "#fff",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",

    borderRadius: "5px",
    width: "95%",
    fontFamily: "Product Sans",
    marginTop: "120px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#C8CFE0",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",

    borderRadius: "5px",
    width: "95%",
    fontFamily: "Product Sans",
    marginTop: "120px",
  },
  myBestVideoCaption: {
    fontSize: "24px",
    fontFamily: "Product Sans",
    color: "#292929",
    textAlign: "center",
  },
  myBestVideo: {
    fontSize: "24px",
    fontFamily: "Product Sans Bold",
    color: "#e31b1b",
  },
});

class ReferralCode extends React.Component {
  state = {
    valid: "",
    refCode: "",
    variant: "warning",
    usermessage: "Referral code does not match.",
    alert: false,
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.value });
  };
  profileapi = (token) => {
    delete this.props.UserData["carouselIndex"];
    Promise.all(this.props.UserData.profilePic)
      .then((data) => {
        this.props.dispatch(MOBILE_ACTION_FUNC("referelCode", token));
        setTimeout(() => {}, 300);
        profileVerification(this.props.UserData).then((data) => {
          setCookie("token", data.data.data.token);
          this.props.updateScreen(<StartWithDatum updateScreen={this.props.updateScreen} {...this.props} />);
        });
      })
      .catch(function (err) {});
  };

  handleskipscreen = () => {
    this.props.dispatch(MOBILE_ACTION_FUNC("latitude", getCookie("lat")));
    this.props.dispatch(MOBILE_ACTION_FUNC("longitude", getCookie("long")));
    delete this.props.UserData["carouselIndex"];
    profileVerification(this.props.UserData).then((data) => {
      setCookie("token", data.data.data.token);
      this.props.updateScreen(<StartWithDatum updateScreen={this.props.updateScreen} {...this.props} />);
    });
  };

  dismiss = () => {
    this.setState({ alert: false });
  };

  checkIfCodeExists = (token) => {
    referralCodeExistVerification(token, getCookie("token"))
      .then((res) => {
        if (res.status === 200) {
          this.setState({ alert: true, usermessage: "Referral Code successfully redeemed", variant: "success" });
          this.profileapi(token);
        } else {
          this.setState({ alert: true, usermessage: res.message || "Something went wrong...", variant: "error" });
          setTimeout(() => {
            this.setState({ alert: false });
          }, 1300);
        }
      })
      .catch((err) => {
        this.setState({ alert: true, usermessage: err.message || "Something went wrong...", variant: "error" });
        setTimeout(() => {
          this.setState({ alert: false });
        }, 1300);
      });
  };

  render() {
    const { classes } = this.props;
    const lang = localStorage.getItem("lang") || "en";
    return (
      <div data-test="component-video" className="col-12">
        <div className="row justify-content-center">
          <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
            <div className="p-5">
              {/* header Module */}
              <div className="py-4">
                <h3 className={classes.myBestVideoCaption}>
                  Enter <span className={classes.myBestVideo}>ReferralCode</span>
                </h3>
                <div className="right_skipbtn" onClick={this.handleskipscreen}>
                  <p className="m-0">Skip</p>
                </div>
              </div>
              <TextField
                id="standard-name"
                className={classes.textField}
                value={this.state.name}
                autoFocus
                // onBlur={this.checkIfCodeExists(this.state.refCode)}
                onChange={this.handleChange("refCode")}
                InputProps={{ classes: { focused: { fontFamily: "Product Sans" }, formControl: { fontFamily: "Product Sans" } } }}
                margin="normal"
              />
              <div className="WProfile_next">
                <button
                  onClick={() => this.checkIfCodeExists(this.state.refCode)}
                  className={this.state.refCode.length > 5 ? classes.btn : classes.disablebtn}
                  disabled={this.state.refCode.length < 5}
                >
                  Continue
                </button>
              </div>
            </div>
          </div>
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.alert} onClose={this.dismiss} />
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
  };
}

export default connect(mapstateToProps, null)(withStyles(styles)(ReferralCode));
