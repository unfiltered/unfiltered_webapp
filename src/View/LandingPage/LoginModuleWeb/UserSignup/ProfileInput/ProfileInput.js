import React, { Component } from "react";
import "./ProfileInput.scss";
import "../OtpInput/OtpInput.scss";
import { withStyles } from "@material-ui/core/styles";
// import CircularProgress from "@material-ui/core/CircularProgress";
import { getCookie } from "../../../../../lib/session";
import VideoInput from "../VideoInput/VideoInput";
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { __BlobUpload } from "../../../../../lib/cloudinary-image-upload";
import "react-html5-camera-photo/build/css/index.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import MobileNumber from "../../../TinderPage/Components/MobileNumber";

const styles = () => ({
  btn: {
    color: "#fff",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    marginTop: "50px",
    width: "95%",
    fontFamily: "Product Sans",
  },
  disablebtn: {
    color: "#fff",
    fontFamily: "Product Sans",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    marginTop: "50px",
    width: "95%",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "18vh",
    textAlign: "center",
  },
  myBestPicture: {
    fontSize: "24px",
    fontFamily: "Product Sans",
    color: "#e31b1b",
  },
  myBestPictureCaption: {
    fontSize: "24px",
    textAlign: "center",
    fontFamily: "Product Sans",
    color: "#292929",
  },
  progressV1: {
    color: "#e31b1b",
    marginTop: "15vh",
    marginBottom: "15vh",
    textAlign: "center",
  },
});

class ProfileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      // files: [],
      loader: false,
      index: 0,
      ShowVideoFormat: false,
      cropModal: false,
      urlToRender: "",
      imageCaptureView: false,
      realImage: "",
      doYouHaveCamera: false,
    };
    this.files = [];
    this.child = React.createRef();
  }

  componentDidMount() {
    if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
      navigator.mediaDevices
        .enumerateDevices()
        .then((res) => {
          console.log("res", res);
          let index = res.filter((k) => k.kind == "videoinput");
          console.log("index", index);
          if (index.length > 0) {
            this.setState({ doYouHaveCamera: true, imageCaptureView: true });
          }
        })
        .catch((err) => {});
    }
  }
 
  // UserProfile API Call Module
  profileapi = async () => {
    // calling child function to uplaod cropped image
    if (this.state.doYouHaveCamera) {
      let upload = await __BlobUpload(this.state.realImage);
      this.props.dispatch(MOBILE_ACTION_FUNC("latitude", parseFloat(getCookie("lat"))));
      this.props.dispatch(MOBILE_ACTION_FUNC("longitude", parseFloat(getCookie("lng"))));
      this.props.dispatch(MOBILE_ACTION_FUNC("countryNameCode", this.props.UserData.countryNameCode));
      this.props.dispatch(MOBILE_ACTION_FUNC("profilePic", upload.body.secure_url));
      this.props.updateScreen(<VideoInput profilePic={upload.body.secure_url} updateScreen={this.props.updateScreen} {...this.props} />);
    } else {
      // let upload = await __BlobUpload(this.state.realImage[0].fileObj);
      // this.props.dispatch(MOBILE_ACTION_FUNC("latitude", parseFloat(getCookie("lat"))));
      // this.props.dispatch(MOBILE_ACTION_FUNC("longitude", parseFloat(getCookie("lng"))));
      // this.props.dispatch(MOBILE_ACTION_FUNC("countryNameCode", this.props.UserData.countryNameCode));
      // this.props.updateScreen(<VideoInput profilePic={upload.body.secure_url} updateScreen={this.props.updateScreen} {...this.props} />);
    }
  };

  startImageCapture = () => {
    this.setState({ imageCaptureView: !this.state.imageCaptureView });
  };

  handleTakePhoto = (dataUri) => {
    // this.files[0] = dataUri;
    this.startImageCapture();
    if (dataUri) {
      this.setState({ realImage: dataUri, valid: true });
    }
  };

  handleTakePhotoAnimationDone = (dataUri) => {
    console.log("takePhoto");
  };

  handleCameraError = (error) => {
    console.log("handleCameraError", error);
  };

  handleCameraStart = (stream) => {
    console.log("handleCameraStart");
  };

  handleCameraStop = () => {
    console.log("handleCameraStop");
  };

  render() {
    // console.log("doYouHaveCamera", this.state.doYouHaveCamera);
    const { classes } = this.props;
    return (
      <div className="col-12 p-5">
        <div className="row justify-content-center">
          {this.state.imageCaptureView && this.state.doYouHaveCamera ? (
            <section>
              <div className="camera-module">
                <Camera
                  onTakePhoto={(dataUri) => {
                    this.handleTakePhoto(dataUri);
                  }}
                  onTakePhotoAnimationDone={(dataUri) => {
                    this.handleTakePhotoAnimationDone(dataUri);
                  }}
                  onCameraError={(error) => {
                    this.handleCameraError(error);
                  }}
                  idealFacingMode={FACING_MODES.ENVIRONMENT}
                  idealResolution={{ width: 768, height: 1366 }}
                  imageType={IMAGE_TYPES.JPG}
                  imageCompression={0.97}
                  isMaxResolution={true}
                  isImageMirror={false}
                  isSilentMode={false}
                  isDisplayStartCameraError={true}
                  isFullscreen={false}
                  sizeFactor={1}
                  onCameraStart={(stream) => {
                    this.handleCameraStart(stream);
                  }}
                  onCameraStop={() => {
                    this.handleCameraStop();
                  }}
                />
              </div>
            </section>
          ) : this.state.doYouHaveCamera ? (
            <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12">
              <div className="pb-3">
                <h3 className={classes.myBestPictureCaption}>
                  We want to see the{" "}
                  <span className={classes.myBestPicture}>UnFiltered You. No Filters, No Uploads, Just the Real You</span>
                </h3>
                {/* <p className="modal_smallText">Adding pictures is a great way to show off your personality.</p> */}
              </div>
              {this.state.realImage != "" ? (
                this.state.doYouHaveCamera ? (
                  <div style={{ height: "0px" }} className="w-100" onClick={this.startImageCapture}>
                    <label className="col-auto w-100" htmlFor="ProfileFile">
                      <h5 className="Wchange_photo" style={{ width: "93%" }}>
                        Change Photo
                      </h5>
                    </label>
                  </div>
                ) : (
                  <div style={{ height: "0px" }} className="w-100" onClick={this.startImageCapture}>
                    <label className="col-auto w-100" htmlFor="ProfileFile">
                      <h5 className="Wchange_photo" style={{ width: "93%" }}>
                        Change Photo
                      </h5>
                    </label>
                  </div>
                )
              ) : (
                ""
              )}

              <div className="py-3 row Wuser_profile justify-content-center">
                <div className="Wuserimg_preview ">
                  {this.state.realImage != "" ? <img className="px-1" src={this.state.realImage} /> : ""}
                </div>
              </div>
              <button
                onClick={this.profileapi}
                className={!this.state.valid ? classes.disablebtn : classes.btn}
                disabled={!this.state.valid}
              >
                Continue
              </button>
            </div>
          ) : (
            <div className="text-center">
              <h3>Please Enable Webcam if you want to continue signup</h3>
              <button
                className={classes.btn}
                onClick={() => this.props.updateScreen(<MobileNumber updateScreen={this.props.updateScreen} />)}
              >
                Start From Login
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    UserData: state.Main.UserData,
    afterCroppingUrl: state.UserProfile.urlAfterCropping,
    profileObject: state.profileObjectAfterCropped,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(ProfileInput));
