// Main React Components
import React, { Component } from "react";

// Scss
import "./ProfileInput.scss";
import "../OtpInput/OtpInput.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// Imported Components
import VideoInput from "../VideoInput/VideoInput";

// Redux Components
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { profileVerification } from "../../../../../controller/auth/verification";
import { setCookie } from "../../../../../lib/session";

// import DummyImg from "../../../../../asset/WebUserCard/boy.png";

const styles = () => ({
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
    marginTop: "15px"
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
    marginTop: "15px"
  }
});

class ProfileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      files: [],
      newFile: [],
      loader: true
    };
    this.handlephotochange = this.handlephotochange.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
  }

  // Function for the User Images
  handlephotochange(event) {
    let wholeFileInput = this.state.files;
    let reader = new FileReader();

    // reader.onload = function (e) {
    // let flInput = document.getElementById(fileID).value;
    // let filename = flInput.replace(/^.*[\\\/]/, '');

    let obj = {
      fileKey: Date.now(),
      fileData: event.target.files
    };

    reader.onload = function(e) {
      // let flInput = document.getElementById(fileID).value;
      // let filename = flInput.replace(/^.*[\\\/]/, '');
      // fileExtension = filename.split(".")[1];
      obj["filePreview"] = e.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    setTimeout(() => {
      // if (fileExtension == "jpg" || fileExtension == "jpeg" || fileExtension == "png") {
      wholeFileInput.push(obj);
      // this.setState({ totalFileArray: this.state.totalFileArray.concat(obj) })
      // } else {
      // console.log("invalid file format...");
      // this.setState({ showFileFormatError: true })
      // setTimeout(() => {
      //   this.setState({ showFileFormatError: false })
      // }, 2000);
      // }
      this.setState({ files: wholeFileInput });
      console.log("userimages", this.state.files);

      if (this.state.files !== "") {
        this.setState({ valid: true });
      }
    }, 100);
    // }
  }

  // Function to Map the Multiple File
  uploadFiles = () => {
    return new Promise((resolve, reject) => {
      let x =
        this.state.files && this.state.files.length > 0
          ? this.state.files.map(async (file, index) => (index <= 2 ? await this.fileHandler(file) : console.log("morethen3file")))
          : "";

      console.log("final repsone...");
      return resolve(1);
    });
  };

  // Function for the to Upload the Userimges on the Amazon S3 Server o create Images Online links
  fileHandler = files => {
    return new Promise((resolve, reject) => {
      window.AWS.config.update({
        accessKeyId: "AKIATONSSUTMCYKI2D7F",
        secretAccessKey: "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",
        region: "us-east-1"
      });

      var s3 = new window.AWS.S3();

      var params = {
        Bucket: "datum-clone",
        Key: "users/Pro_Pic/" + "ProPic_" + Date.now(),
        ContentType: files.fileData[0].type,
        Body: files.fileData[0],
        ACL: "public-read"
      };
      // this.setState({ loading: true, idLoading: true })
      s3.putObject(params, (err, res) => {
        if (err) {
          console.log("Error uploading data: ", err);
          // this.setState({ loading: false });
          return reject(false);
        } else {
          let data = {
            identityCard: "https://datum-clone.s3.amazonaws.com/" + params.Key
          };

          // Action Dispatch for User Uploaded Video
          this.props.dispatch(MOBILE_ACTION_FUNC("profilePic", data.identityCard));

          // this.setState({ idSuccess: true, showPreviewforImg: false, loading: false })
          // xyz.push("https://datum-clone.s3.amazonaws.com/" + params.Key);
          // console.log("final uploaded links...", xyz);

          // // Action Dispatch for User Uploaded Images Array
          // this.props.dispatch(MOBILE_ACTION_FUNC("otherImages", xyz));

          return resolve(data.identityCard);
        }
      });
    });
  };

  profileapi = () => {
    let promises = this.state.files.map(file => this.fileHandler(file));

    // this.state.loader ? (
    //   <CircularProgress />
    // ) : (
    Promise.all(promises)
      .then(data => {
        console.log("main succes", data);
        profileVerification(this.props.UserData).then(data => {
          console.log("-sdadasdasd-----------------", data.data.data);
          setCookie("token", data.data.data.token);
          setCookie("findmateid", data.data.data.findMateId);
          this.props.handleScreen(<VideoInput handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />);
        });
      })
      .catch(function(err) {
        console.log("main err", err);
      });
    // );
  };

  render() {
    console.log("UserMobileNumber", this.props.UserData);

    const { classes } = this.props;

    return (
      // Img Upload Module
      <div className="col-12">
        <div className="row justify-content-center">
          <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
            {/* header Module */}
            <div className="py-4">
              <h3>Upload your best Profile</h3>
              <p>Adding Pictures is a Great way to Show off your Personality.!</p>
            </div>

            <div className="row Wuser_profile justify-content-center">
              {/*  Uploaded Img Show Module */}
              <div className="Wuserimg_preview">
                {this.state.files
                  ? this.state.files.map((data, index) => <img className="px-1" key={index} src={data.filePreview} alt="User1" title="User1" />)
                  : ""}
              </div>

              {/* Image Upload Module */}
              {this.state.files.length <= 0 ? (
                <form encType="multipart/form-data">
                  <label className="col-auto" htmlFor="File">
                    Upload Your Photo
                    <input type="file" id="File" className="Wuser_input" onChange={this.handlephotochange} />
                  </label>
                </form>
              ) : (
                ""
              )}
            </div>

            {/* Next Button Module */}
            <div className="WProfile_next">
              <button onClick={this.profileapi} className={!this.state.valid ? classes.disablebtn : classes.btn} disabled={!this.state.valid}>
                Continue
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData
  };
}

export default connect(mapstateToProps, null)(withStyles(styles)(ProfileInput));
