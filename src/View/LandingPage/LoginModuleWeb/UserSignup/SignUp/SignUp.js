import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./SignUp.scss";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import OtpInput from "../OtpInput/OtpInput";
import Message from "../../../../../message";
import { ValidateName, ValidateEmail } from "../../../../../Validation/Validation";
import FacebookLogin from "react-facebook-login";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { connect } from "react-redux";
import MobileNumber from "../../../TinderPage/Components/MobileNumber";
import { mobileVerification, emailVerfication, Userfacebooklogin, PhoneExistVerification } from "../../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { setCookie } from "../../../../../lib/session";
import { keys } from "../../../../../lib/keys";
import BirthdayValidator from "./BirthdayValidator";

const styles = (theme) => ({
  root: {
    border: "2px solid #ccc",
    padding: "5px 12px",
    borderRadius: "11px",
    borderBottom: 0,
    "&:hover": {
      borderBottom: 0,
    },
  },
  focused: {
    border: "2px solid #e31b1b",
    borderRadius: "11px",
    "&:focus": {
      border: "none",
      outline: 0,
    },
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px",
  },
});

let globalnumber = "";
// let numberstatus = "";
let globalemail = "";
// let multiselectchecbox = [];

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valuenumber: "",
      phone: "",
      dropdownOpen: false,
      date: {},
      errors: {},
      valid: null,
      selectedDate: "",
      isDateSelected: false,
      age: "",
      firstName: "",
      fbId: "",
      pushToken: "",
      facebooktoken: "",
      email: "",
      picture: "",
      height: "",
      open: false,
      isLoggedIn: false,
      labelWidth: 0,
      selectedOption: null,
      Finalvalue: "",
      nameValid: null,
      emailValid: null,
      emailAlreadyExists: false,
      mobileValid: null,
      FianlDOB: null,
      newValue: [],
      latititudelocal: "",
      longitudelocal: "",
      mobileNum: "",
      password: "",
      cCode: "",
      countryName: "",
      isDateError: false,
      data: {
        phoneNumber: "",
        mobileNum: "",
      },
    };
    this.toggle = this.toggle.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    // this.handleDate = this.handleDate.bind(this);
    this.radioChange = this.radioChange.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      document.getElementById("signupfocus").focus();
    }, 100);
  }

  // Used for the Get User Current latititudelocal && longitudelocal with Full Address
  static getDerivedStateFromProps(nextProps) {
    const userlocation = {
      latititudelocal: nextProps.coords ? nextProps.coords.latitude : "",
      longitudelocal: nextProps.coords ? nextProps.coords.longitude : "",
    };
    let google = window.google;
    if (google) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(userlocation.latititudelocal, userlocation.longitudelocal);
      geocoder.geocode({ latLng: latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          setCookie("location", results[0].formatted_address);
          setCookie("UserCurrentCity", results[6].formatted_address);
          setCookie("citylocation", results[7].formatted_address);
        }
      });
    }
    return userlocation;
  }

  // Used to get User Current Long && Lot
  dispatchLatitude(lat, long) {
    this.props.dispatch(MOBILE_ACTION_FUNC("latitude", lat));
    setCookie("lat", lat);
    this.props.dispatch(MOBILE_ACTION_FUNC("longitude", long));
    setCookie("long", long);
  }

  // the screen sent as argument to this function will be rendered, or make it false to render default screen
  // updateScreen = screen => this.setState({ currentScreen: screen });

  // Function for the Toggle lang box
  toggle() {
    this.setState((prevState) => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  // Function for the MouseEnter Hover
  onMouseEnter() {
    this.setState({ dropdownOpen: true });
  }

  // Function for the MouseLeave Hover
  onMouseLeave() {
    this.setState({ dropdownOpen: false });
  }

  // Function for the Date of Birth Choose
  // handleDate(date) {
  //   let isValid = this.checkPlus(moment(date).get("date"), moment(date).get("month"), moment(date).get("year"));
  //   this.setState({
  //     valid: isValid,
  //     isDateSelected: true,
  //   });
  //   let FianlDOB = moment(date).unix();
  //   this.setState({ selectedDate: date });

  //   let UserDOB = FianlDOB * 1000;

  //   this.props.dispatch(MOBILE_ACTION_FUNC("dob", UserDOB));
  // }

  // Checking User 18 Year old or not
  checkPlus = (day, month, year) => {
    return new Date(year + 18, month - 1, day) <= new Date();
  };

  // Function for the Radio Button
  radioChange(status) {
    this.setState(
      {
        selectedOption: status.currentTarget.value,
        value: status,
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("gender", this.state.selectedOption));
      }
    );
  }

  maleButtonClicked = () => {
    this.setState(
      {
        selectedOption: 1,
        value: 1,
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("gender", 1));
      }
    );
  };

  femaleButtonClicked = () => {
    this.setState(
      {
        selectedOption: 2,
        value: 2,
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("gender", 2));
      }
    );
  };

  // Function for the Input Value
  handleName = (event) => {
    ValidateName(event);
    event.target.value.length > 0 && !ValidateName(event)
      ? this.setState({
          nameErr: "Please enter valid name.",
          nameValid: false,
        })
      : this.setState({ nameErr: "", nameValid: true });
    this.props.dispatch(MOBILE_ACTION_FUNC("firstName", event.target.value));
  };

  handlePassword = (event) => {
    this.setState({ password: event.target.value });
    this.props.dispatch(MOBILE_ACTION_FUNC("password", event.target.value));
  };

  // Function for the Email Value
  handleEmail = (event) => {
    ValidateEmail(event);
    event.target.value.length > 0 && !ValidateEmail(event)
      ? this.setState({
          emailErr: "Please enter valid email.",
          emailValid: false,
          emailAlreadyExists: false,
        })
      : this.setState({
          emailErr: "",
          emailValid: false,
          emailAlreadyExists: false,
        });
    globalemail = event.target.value;
    this.props.dispatch(MOBILE_ACTION_FUNC("email", event.target.value));
  };

  handleOnChange = (email) => {
    // don't remember from where i copied this code, but this works.
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email.target.value)) {
      this.setState({ emailValid: true });
    } else {
      this.setState({ emailValid: false });
    }
    this.props.dispatch(MOBILE_ACTION_FUNC("email", email.target.value));
  };

  // Function for the Phone Number Flag
  handleflag = (num) => {
    this.setState({ cCode: num.dialCode });
  };

  // Function for the Phone Number Value
  handlephone = (status, valuenumber, number, phoneNumber) => {
    this.setState({
      valuenumber: status,
      cCode: number.dialCode,
    });

    globalnumber = phoneNumber.replace(/ /g, "");

    this.props.dispatch(MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode));
    this.props.dispatch(MOBILE_ACTION_FUNC("contactNumber", globalnumber));
    this.props.dispatch(MOBILE_ACTION_FUNC("countryNameCode", number.iso2.toUpperCase()));
  };

  showToast = (open, type, msg) => {
    this.setState({
      open: open,
      variant: type,
      usermessage: msg,
    });
  };

  // Checking All Validation for the Form && Showing the error
  checkValidation = (e) => {
    e.preventDefault();

    if (!this.state.nameValid) {
      this.showToast(true, "error", "Please enter valid Name.");
      return;
    }

    if (!this.state.valid) {
      this.showToast(true, "error", "Please select valid date of birth.");
      return;
    }

    if (!this.state.selectedOption) {
      this.showToast(true, "error", "Please select your gender.");
      return;
    }

    if (this.state.emailAlreadyExists) {
      this.showToast(true, "error", "Email address already exists.");
      return;
    }

    if (!this.state.emailValid) {
      this.showToast(true, "error", "Please enter valid email address.");
      return;
    }

    if (this.state.password.length < 6) {
      this.showToast(true, "error", "Password should be more than 6 characters");
      return;
    }

    if (!this.state.mobileNum) {
      this.showToast(true, "error", "Please enter valid mobile number.");
      return;
    }
    this.handlesignup();
  };

  // Function for the Notification (Snakbar)
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification (Snakbar)
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the SignUP Button
  handlesignup = () => {
    this.verifyEmail();
    this.sendOtp();
  };

  // Function for Send OTP API
  sendOtp = () => {
    let mobileverfiyPayload = {
      phoneNumber: this.state.mobileNum,
    };

    let mobilePayload = {
      // email: globalemail,
      phoneNumber: this.state.mobileNum,
      type: 1,
      deviceId: "0909",
    };
    this.props.dispatch(MOBILE_ACTION_FUNC("dob", new Date(this.state.selectedDate).getTime()));
    this.props.dispatch(MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode));
    this.props.dispatch(MOBILE_ACTION_FUNC("contactNumber", this.state.mobileNum));
    this.props.dispatch(MOBILE_ACTION_FUNC("countryNameCode", this.state.countryName.toUpperCase()));
    PhoneExistVerification(mobileverfiyPayload)
      .then((data) => {
        if (data === undefined) {
          // this code is crap, firstly when entering correct number it will prompt for
          // phone number already registered, if u change number the same error will prompt
          // to remove this error, i have written the code to go forward i.e it is telling 412 status
          // which means number is not registered. RN the function does not sends response 412. just go through
          this.props.updateScreen(
            <OtpInput
              mobilenumber={this.state.mobileNum}
              globalnumber={this.state.mobileNum}
              cCode={this.state.cCode}
              updateScreen={this.props.updateScreen}
              globalemail={globalemail}
              {...this.props}
            />
          );
        }
        console.log("[PhoneExistVerification success]", data);
        mobileVerification(mobilePayload)
          .then((data) => {
            console.log("[mobileVerification]", data);
            this.setState({
              open: true,
              variant: "error",
              usermessage: "Mobile number already exists.",
            });
          })
          .catch((err) => console.log("[mobileVerification]", err));
      })
      .catch((err) => {
        console.log("[PhoneExistVerification error]", err);
        mobileVerification(mobilePayload).then((data) => {
          this.setState({
            open: true,
            variant: "success",
            usermessage: "OTP sent successfully",
          });
        });

        this.props.updateScreen(
          <OtpInput
            mobilenumber={"+" + this.state.mobileNum}
            globalnumber={"+" + this.state.mobileNum}
            updateScreen={this.props.updateScreen}
            globalemail={globalemail}
            {...this.props}
          />
        );
      });
    setTimeout(() => {
      this.setState({ open: false });
    }, 2000);
  };

  navigateToLopinInModal = () => {
    this.props.updateScreen(<MobileNumber updateScreen={this.props.updateScreen} toggleModal={this.props.toggleModal} />);
  };

  // Function to Check EmailID Already Exits or Not
  verifyEmail = () => {
    if (globalemail && globalemail.length > 1) {
      let emailPayload = {
        email: globalemail,
      };

      emailVerfication(emailPayload)
        .then((data) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: data.data.message || "Email address already exists.",
            emailValid: false,
            emailAlreadyExists: true,
          });
          return false;
        })
        .catch(() => {
          this.setState({ emailValid: true });
          return true;
        });
    }
  };

  handlePhoneNumberChange = (value, country, e, formattedValue) => {
    this.setState({ mobileNum: value, cCode: country.dialCode, countryName: country.countryCode });
  };

  // Function for the Facebook SignUP
  responseFacebook = (response) => {
    this.setState(
      {
        firstName: response.name,
        fbId: response.id,
        pushToken: response.accessToken,
        email: response.email,
        picture: response.picture.data.url,
        height: response.picture.data.height,
      },
      () => {
        let facebookpayload = {
          firstName: this.state.firstName,
          fbId: this.state.fbId,
          pushToken: this.state.pushToken,
          email: this.state.email,
          deviceId: "15645646564989",
          deviceMake: "Apple",
          deviceModel: "Md-LG",
          deviceType: "1",
          profilePic: this.state.picture,
          height: this.state.height,
        };
        localStorage.setItem("firstName", response.name);
        localStorage.setItem("email", response.email);
        localStorage.setItem("picture", response.picture.data.url);
        localStorage.setItem("height", response.picture.data.height);
        localStorage.setItem("facebookLogin", true);
        Userfacebooklogin(facebookpayload)
          .then((data) => {
            setCookie("uid", data.data.data._id);
            setCookie("token", data.data.data.token);
            this.setState({
              isLoggedIn: true,
            });
          })
          .catch((err) => {
            console.log("err", err);
          });
      }
    );
  };

  openPolicy = () => {
    const url = "https://appconfig.unfiltered.love/policy.html";
    window.open(url, "_blank");
  };

  openTermsAndPolicy = () => {
    const url = "https://appconfig.unfiltered.love/TermsAndConditions.html";
    window.open(url, "_blank");
  };

  openCommunityGuidelines = () => {
    const url = "https://appconfig.unfiltered.love/CommunityGuidelines.html";
    window.open(url, "_blank");
  };

  setIsDateError = (bool) => this.setState({ isDateError: bool, valid: bool });

  setDateInput = (e) => this.setState({ selectedDate: e });

  render() {
    let lang = localStorage.getItem("lang") || "en";

    let fbContent;
    if (this.state.isLoggedIn) {
      fbContent = this.props.history.push("/app/");
    } else {
      fbContent = (
        <FacebookLogin
          accessToken={keys.facebookToken}
          textButton="f"
          cssClass="signup_facebook_small_btn"
          appId={keys.facebookAppId}
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
        />
      );
    }
    return (
      <div data-test="component-app">
        {/* to Get Latt && Long of Location */}
        {!this.props.isGeolocationAvailable ? (
          <div />
        ) : !this.props.isGeolocationEnabled ? (
          <div />
        ) : this.props.coords ? (
          this.dispatchLatitude(this.props.coords.latitude, this.props.coords.longitude)
        ) : (
          <div />
        )}
        <div>
          {/* Navbar Module */}
          <div>
            {/* MainBody Module */}
            {!this.state.currentScreen ? (
              <div className="col-12">
                <div className="row justify-content-center">
                  <div className="signup_model_updated">
                    {/* Title Module */}
                    <div className="signup__title">
                      <h1 className="m-0 text-center create_new_account">Create New Account</h1>
                    </div>
                    {/* Main Form Module */}
                    <form onSubmit={this.checkValidation}>
                      <div className="signup__form">
                        {/* Name Module */}
                        <div className="mr-0 pb-2">
                          <div className="title pb-1">
                            <p className="text-left mb-0 signup_label">Name*</p>
                          </div>
                          <div>
                            <form id="signup_common" className="signup_firstname" onChange={this.handleName}>
                              <input
                                refs="name"
                                name="name"
                                type="text"
                                size="30"
                                className="signup_name"
                                placeholder={(Message && Message[lang] && Message[lang]["message.signupName"]) || "Enter Your Name"}
                                id="signupfocus"
                              />
                            </form>
                            <p className="signup_error">{this.state.nameErr}</p>
                          </div>
                        </div>

                        {/* Email-ID Module */}
                        <div className="mr-0 pb-2">
                          <div className="title pb-1">
                            <p className="text-left mb-0 signup_label">Email*</p>
                          </div>
                          <div>
                            <form id="signup_common">
                              <input
                                refs="name"
                                name="name"
                                type="text"
                                size="30"
                                className="signup_name"
                                placeholder={(Message && Message[lang] && Message[lang]["message.signupEmail"]) || "Enter Your Email Address"}
                                onChange={this.handleOnChange}
                                onBlur={this.verifyEmail}
                              />
                            </form>
                            <p className="signup_error">{this.state.emailErr}</p>
                          </div>
                        </div>

                        <div className="mr-0 pb-2">
                          <div className="title pb-1">
                            <p className="text-left mb-0 signup_label">Password*</p>
                          </div>
                          <div>
                            <form id="signup_common">
                              <input
                                refs="password"
                                name="password"
                                type="password"
                                size="30"
                                className="signup_name"
                                placeholder={""}
                                onChange={(e) => this.handlePassword(e)}
                              />
                            </form>
                            <p className="signup_error">
                              {this.state.password.length > 1 && this.state.password.length < 8 ? "Password should be 8 characters long" : ""}
                            </p>
                          </div>
                        </div>

                        {/* Birthday Module */}
                        <div className="col-12">
                          <div className="row">
                            <div className="col-5 px-0 mr-0 pb-2">
                              <div className="title pb-1">
                                <p className="text-left mb-0 signup_label">Birthday*</p>
                              </div>
                              <div className="bDate">
                                <BirthdayValidator setIsDateError={this.setIsDateError} setDateInput={this.setDateInput} />
                              </div>
                            </div>
                            <div className="col-2 px-0"></div>
                            {/* Gender Module */}
                            <div className="col-5 px-0 mr-0 pb-2">
                              <div className="title pb-1">
                                <p className="text-left mb-0 signup_label">Gender*</p>
                              </div>
                              <div className="row">
                                <div className="col-5">
                                  <button
                                    type="button"
                                    onClick={() => this.maleButtonClicked()}
                                    className={this.state.selectedOption === 1 ? "btn btn-danger male_button_actived" : "btn btn-outline-danger male_button"}
                                  >
                                    Male
                                  </button>
                                </div>
                                <div className="col-7">
                                  <button
                                    type="button"
                                    onClick={() => this.femaleButtonClicked()}
                                    className={
                                      this.state.selectedOption === 2 ? "btn btn btn-danger female_button_activated" : "btn btn-outline-danger female_button"
                                    }
                                  >
                                    Female
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        {/* Mobile-Number Module */}
                        <div className="mr-0 pb-4">
                          <div className="title pb-1">
                            <p className="text-left mb-0 signup_label">Mobile Number*</p>
                          </div>
                          <div>
                            <form className="d_signup">
                              <PhoneInput
                                type="number"
                                country={"us"}
                                preferredCountries={["us", "in"]}
                                value={this.state.mobileNum}
                                onChange={this.handlePhoneNumberChange}
                                placeholder={"8698997798"}
                                autoFormat={false}
                                inputProps={{
                                  name: "phone",
                                  required: true,
                                  autoFocus: true,
                                  placeholder: "8698997798",
                                  autoFormat: false,
                                }}
                              />
                            </form>
                          </div>
                        </div>

                        <div className="col-12 px-0">
                          <div className="row mx-0">
                            <div className="col-9 px-0">
                              <button
                                type="submit"
                                data-test="component-signup"
                                className={
                                  this.state.password.length > 7 &&
                                  this.state.selectedOption &&
                                  this.state.isDateError &&
                                  this.state.emailValid &&
                                  this.state.mobileNum.length > 7
                                    ? "w-100 continue_btn_signup"
                                    : "w-100 continue_btn_signup_disabled"
                                }
                              >
                                Continue
                              </button>
                            </div>
                            <div className="col-2 px-0 justify-content-center align-items-center d-flex">OR</div>
                            <div className="facebook_btn col-1 px-0">{fbContent}</div>
                            {/* <div className="facebook_btn col-2">{fbContent}</div> */}
                          </div>
                        </div>
                        {/* </div> */}

                        {/* Condition Content Module */}
                        <div className="py-2 row">
                          {/* <div className="col-auto col-sm-2 col-md-2 col-lg-2 col-xl-2" /> */}
                          <div className="col-12 signup_notification">
                            <p>
                              By Sign Up, You’re Confirming that You’ve read and agree to our
                              <span onClick={this.openTermsAndPolicy} style={{ textDecoration: "underline", cursor: "pointer" }}>
                                Terms and Conditions.
                              </span>
                              Learn how we process your data in our
                              <span onClick={this.openPolicy} style={{ textDecoration: "underline", cursor: "pointer", paddingLeft: "5px" }}>
                                Privacy Policy
                              </span>
                              and{" "}
                              <span onClick={this.openCommunityGuidelines} style={{ textDecoration: "underline", cursor: "pointer", paddingLeft: "5px" }}>
                                Community Guidelines
                              </span>
                            </p>
                          </div>
                          <div className="col-12 already_member_sign_in">
                            <span id="helper-txt">Already a member?</span>
                            <span onClick={() => this.navigateToLopinInModal()} className="login">
                              {" "}
                              Log In
                            </span>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            ) : (
              this.state.currentScreen
            )}
          </div>
          {/* Snakbar Components */}
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      </div>
    );
  }
}

export default connect()(withStyles(styles)(withRouter(SignUp)));
