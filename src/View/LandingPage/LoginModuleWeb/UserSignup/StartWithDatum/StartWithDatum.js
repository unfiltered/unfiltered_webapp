// Main React Components
import React, { Component } from "react";
// import { connect } from "react-redux";
// Scss
import "./StartWithDatum.scss";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withRouter } from "react-router-dom";
// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
// import { discoverPeople } from "../../../../../actions/DiscoverPeople";
// Material-Ui Components
// import RadioGroup from "@material-ui/core/RadioGroup";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
// import FormControl from "@material-ui/core/FormControl";
// import Checkbox from "@material-ui/core/Checkbox";
// import Done from "@material-ui/icons/Done";
// Img Module
import Getstarted from "../../../../../asset/images/Tinder-Slider-1.jpg";

// MultiSelect Dropdown Components for UserHeight Input
import Select from "react-select";

// Validation Components
import { ValidateName } from "../../../../../Validation/Validation";

// Redux Components
import { getPreference, UserdataPrefence } from "../../../../../controller/auth/verification";
import { Icons } from "../../../../../Components/Icons/Icons";

const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  root: {
    width: "99%",
    backgroundColor: theme.palette.background.paper,
    paddingBottom: "100px",
  },
  rootRadio: {
    color: "#e31b1b",
    "&$checked": {
      color: "#e31b1b",
    },
    height: "25px",
    width: "40px",
    fontSize: "20px",
    float: "left",
    left: "0px !important",
  },
  // label: {
  //   display: "unset",
  //   margin: "7px 0px",
  // },
  labelRadio: {
    padding: "7px 0px",
    margin: "0px",
    display: "flex",
    fontSize: "16px",
  },
  btn: {
    color: "#fff",
    cursor: "pointer",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    width: "100%",
    marginTop: "10px",
    position: "absolute",
    bottom: "0px",

    left: "0",
    fontSize: "18px",
    fontFamily: "Product Sans",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    cursor: "pointer",
    borderRadius: "5px",
    width: "100%",
    marginTop: "10px",
    position: "absolute",
    bottom: "0px",

    left: "0",
    fontSize: "18px",
    fontFamily: "Product Sans",
  },
  skipButton: {
    position: "absolute",
    top: "-20px",
    right: "-25px",
    cursor: "pointer",
    color: "#C8CFE0",
    fontFamily: "Product Sans",
    fontSize: "16px",
  },
  relationshipStartsWith: {
    color: "#585A60",
    fontSize: "21px",
  },
  __radioToButtons: {
    cursor: "pointer",
    padding: "5px 8px",
    margin: "5px 5px",
    borderRadius: "5px",
    fontFamily: "Product Sans",
    fontSize: "18px",
    border: "1px solid #e31b1b",
    color: "#e31b1b",
  },

  // color: #989898;
  //   cursor: pointer;
  //   margin: 5px 5px;
  //   border: 1px solid #989898;
  //   padding: 5px 8px;
  //   font-size: 18px;
  //   font-family: Product Sans;
  //   border-radius: 5px;

  __falseRadioToButtons: {
    cursor: "pointer",
    padding: "5px 8px",
    borderRadius: "5px",
    margin: "5px 5px",
    fontFamily: "Product Sans",
    fontSize: "18px",
    border: "1px solid #989898",
    color: "#989898",
  },
  title: {
    fontSize: "24px",
    color: "#292929",
    textAlign: "center",
    fontFamily: "Product Sans",
  },
  label: {
    fontSize: "24px",
    color: "#e31b1b",
    fontFamily: "Product Sans",
    fontWeight: 900,
  },
  Prefernottosay: {
    fontSize: "16px",
    color: "#484848",
    fontFamily: "Product Sans",
  },
  startWithDatumLogo: {
    position: "absolute",
    top: "40%",
    textAlign: "center",
    width: "100%",
  },
});

// Globally Constant Variables
let currentPrefId = "";
// let currentlabel = "";
// let currentheight = "";
// let multiselecthoobies = "";
let multiselectchecbox = [];
// let UserSignupSeemeId = "";

class StartWithDatum extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {},
      errors: {},
      valid: false,
      value: false,
      selectedScreenIndex: 0,
      selectedPrefIndex: 0,
      Preitem: "",
      newValue: [],
      newradiovalue: "",
      newselectedOption: "",
      seemenamePayload: {},
      preferTrue: false,
      open: false,
      usermessage: "",
      selectedRadioButton: false,
      variant: "warning",
    };
    this.radioChange = this.radioChange.bind(this);
    this.inputRef = React.createRef();
    // this.handlebackscreen = this.handlebackscreen.bind(this);
  }

  // Set Focus in Input box that Comes from the API data
  setControlFocus = (id) => {
    // setTimeout(() => {
    //   let focusData = document.getElementById("activeinput-" + currentPrefId) ? document.getElementById("activeinput-" + currentPrefId).focus() : "";
    // }, 100);
  };

  // Function for Scrren Change onClick of Skip Button
  handleskipscreen = () => {
    this.props.history.push("/app/");
  };

  // Function for Scrren Change onClick of Skip Button
  handlebackscreen = () => {
    let { selectedScreenIndex, selectedPrefIndex } = this.state;
    if (selectedScreenIndex === 0 && selectedPrefIndex === 0) {
      return;
    } else if (selectedScreenIndex > 0 && selectedPrefIndex === 0) {
      this.setState({
        selectedScreenIndex: selectedScreenIndex - 1,
        selectedPrefIndex: this.state.PrefernceData.myPreferences[selectedScreenIndex - 1].data.length - 1,
        value: false,
      });
    } else {
      this.setState({ selectedPrefIndex: selectedPrefIndex - 1, value: false });
    }
  };

  // Function for the Radio Button
  radioChange = (status) => {
    console.log("radioChange(status)", status);
    if (status) {
      this.setState({
        value: status,
        selectedRadioButton: false,
      });
    } else {
      this.setState({
        open: true,
        variant: "warning",
        usermessage: "Please select the option",
      });
      setTimeout(() => {
        this.setState({ open: false });
      }, 1500);
    }
  };

  // Function for the prefer Not to say Button
  handlenotsayradioChange = (status) => {
    console.log("prefer not to say cliked", status);
    this.setState({ selectedRadioButton: true, value: false });

    multiselectchecbox = [];
    let toRender = "";
    try {
      this.inputRef.current.value = "";
    } catch (e) {}
    if (Array.isArray(status)) {
      toRender = [];
    } else if (typeof this.state.value === "string" || this.state.value instanceof String) {
      try {
        this.inputRef.current.value = "";
        toRender = "";
      } catch (e) {}
    }

    this.setState({
      value: toRender,
    });
  };

  // Function for the Checking Multiple Input (Hobbies) List
  handlemultiselect = (event) => {
    if (multiselectchecbox.includes(event)) {
      let index = multiselectchecbox.indexOf(event);
      multiselectchecbox.splice(index, 1);
    } else {
      multiselectchecbox.push(event);
    }
    this.setState({ selectedRadioButton: false, value: multiselectchecbox });
  };

  // Function for the User Input Value
  handleUser = (event) => {
    ValidateName(event)
      ? this.setState({ nameErr: "", valid: true })
      : this.setState({
          nameErr: "Please Enter valid Details",
          valid: false,
        });
    this.setState({ value: event.target.value, selectedRadioButton: false });
  };

  // Function to Chnage the Seeme ID
  handleseemeid = (event) => {
    this.setState({ seemenamePayload: { seemeid: event.target.value } });
  };

  getInput = (type, prefItem) => {
    const { classes } = this.props;
    currentPrefId = prefItem.pref_id;
    switch (type) {
      // For the Input
      case 5:
        let inputBox = (
          <div>
            <form onSubmit={this.handlePrefernce}>
              <input
                name="name"
                type="text"
                size="30"
                ref={this.inputRef}
                onChange={this.handleUser}
                placeholder={prefItem.options ? prefItem.options[0] : prefItem.label}
                id={"activeinput-" + currentPrefId}
              />
              <div className="WNotificationmessage">
                <span>This is how it will appear in UnFiltered.</span>
              </div>
            </form>
          </div>
        );
        return inputBox;

      // For the RadioButton
      case 1:
        let RadioButton = (
          <div className="col-12">
            <div className="row justify-content-center">
              {prefItem.options && prefItem.options.length > 0
                ? prefItem.options.map((option, index) => (
                    <div
                      key={index}
                      id={option}
                      className={this.state.value === option ? classes.__radioToButtons : classes.__falseRadioToButtons}
                      onClick={(e) => this.radioChange(option)}
                    >
                      {option}
                    </div>
                  ))
                : ""}
            </div>
          </div>
        );
        return RadioButton;

      // For the Multi-SelectCheckbox
      case 2:
        let active = {
          padding: "8px 16px",
          margin: "5px 5px",
          borderRadius: "5px",
          border: "2px solid #e31b1b",
          color: "#e31b1b",
          cursor: "pointer",
        };
        let deactive = {
          padding: "8px 16px",
          borderRadius: "5px",
          margin: "5px 5px",
          border: "2px solid #DBDBDB",
          color: "#484848",
          cursor: "pointer",
        };
        let MultiSelectCheckbox =
          prefItem.options &&
          prefItem.options.map((k, i) => (
            <div key={i} style={multiselectchecbox.includes(k) ? active : deactive} onClick={() => this.handlemultiselect(k)}>
              {k}
            </div>
          ));

        return MultiSelectCheckbox;

      // For the Select DropDown
      case 10:
        let options = [];
        this.state.PrefernceData.myPreferences[0].data[0].options.map((data) =>
          options.push({
            label: data,
            value: data,
          })
        );

        let SelectDropDown = (
          <div className="col-12">
            <div className="row justify-content-center">
              <div className="col-md-12 col-lg-12 col-xl-12 WUser_heightsel">
                <Select placeholder={"Select your height"} onChange={this.handleUserSelects} options={options} />
              </div>
            </div>
          </div>
        );
        return SelectDropDown;

      // For the radiobutton
      // case 10:
      //   let multiCheckInput = (
      //     <div className="col-12">
      //       <div className="row justify-content-center">
      //         {prefItem.options && prefItem.options.length > 0
      //           ? prefItem.options.map((option) => (
      //               <div className="col-auto Wsignupapiradiochecked_btn">
      //                 <input type="radio" id={option} name="radio-group" value={option} checked={this.state.value === option} onChange={this.radioChange} />
      //                 <label htmlFor={option}>{option}</label>
      //               </div>
      //             ))
      //           : ""}
      //       </div>
      //     </div>
      //   );
      //   return multiCheckInput;
      default:
        console.log("none");
    }
  };

  // Function for the Height
  handleUserSelects = (event) => {
    console.log("dropdown for height", event.value);
    this.setState({ value: event.value, selectedRadioButton: false });
  };

  showWarning = () => {
    this.setState({ variant: "warning", open: true, usermessage: "Please select one option" });
    setTimeout(() => {
      this.setState({ open: false });
    }, 1500);
  };

  checkForEmptyData = (obj) => {
    if (Array.isArray(obj)) {
      if (obj.length > 0) return true;
    } else if ((typeof obj === "string" || obj instanceof String) && obj !== "") {
      return true;
    } else if (this.state.selectedRadioButton) {
      return true;
    } else if (typeof obj === "boolean") {
    }

    return false;
  };

  handleNextStep = (e) => {
    e.preventDefault();
    if (this.state.value === false) {
      this.handlePrefernce(e);
    } else if (this.checkForEmptyData(this.state.value)) {
      this.handlePrefernce(e);
    }
    // this.setState({value: ""})
  };

  handlePrefernce = (e) => {
    e.preventDefault();

    // Main API Call for the User prefrences
    if (!this.state.PrefernceData) {
      getPreference().then((data) => {
        console.log("start with datum", data);
        // let finalPrefData = data.data.data.myPreferences.map((pref) => {
        //   pref.data.sort((a, b) => a.priority - b.priority);
        // });
        this.setState({
          PrefernceData: data.data.data,
        });
      });
    } else {
      let currentScreenLength = this.state.PrefernceData.myPreferences[this.state.selectedScreenIndex].data.length;
      if (
        this.state.selectedScreenIndex === this.state.PrefernceData.myPreferences.length - 1 &&
        this.state.selectedPrefIndex === currentScreenLength - 1
      ) {
        console.log("A executed");
        this.postPreferences();

        this.props.history.push("/app");
      } else {
        console.log("B executed");
        this.postPreferences();
        if (this.state.selectedPrefIndex === currentScreenLength - 1) {
          this.setState(
            {
              selectedScreenIndex: parseInt(this.state.selectedScreenIndex + 1),
              selectedPrefIndex: 0,
            },
            () => {
              this.setControlFocus(currentPrefId);
            }
          );
          return;
        }

        // To Skip a Screen from API Data
        this.setState(
          {
            selectedPrefIndex: parseInt(this.state.selectedPrefIndex) + 1,
            value: false,
          },
          () => {
            this.setControlFocus(currentPrefId);
          }
        );
      }
    }

    // setTimeout(() => {
    //   let resetradiobtn = document.getElementById("Prefernottosay") ? (document.getElementById("Prefernottosay").checked = false) : "";

    //   let focusData = document.getElementById("activeinput-" + currentPrefId) ? (document.getElementById("activeinput-" + currentPrefId).value = "") : "";

    //   let focusData1 = document.getElementById("inputArray") ? (document.getElementById("inputArray").values = "") : "";
    // }, 100);
  };

  checkArrayOfArrays = (a) => {
    return a.every(function (x) {
      return Array.isArray(x);
    });
  };

  // Post request with payload data
  postPreferences = () => {
    let PrefUser = {
      pref_id: currentPrefId,
      values: this.checkArrayOfArrays([this.state.value]) ? this.state.value : [this.state.value],
    };

    // Function for the API Call
    UserdataPrefence(PrefUser)
      .then((data) => {
        console.log("UserdataPrefence", data);
        this.setState({ value: "", selectedRadioButton: false });
        multiselectchecbox = [];
        console.log("post preferences", data);
      })
      .catch((err) => {
        console.log("UserdataPrefence error", err);
      });
  };

  render() {
    const { classes } = this.props;
    const { selectedScreenIndex } = this.state;

    return (
      // this Module Used for the SeemeID Screen render
      <div data-test="component-Seeme" key={"Seemeid"} className="p-5">
        {this.state.selectedScreenIndex === 0 && this.state.selectedPrefIndex === 0 ? (
          <></>
        ) : (
          <div className="modal_close_btn" onClick={this.handlebackscreen} style={{ zIndex: 9999 }}>
            <i className="fa fa-arrow-left MCommonbackbutton" />
          </div>
        )}
        <div className="col-12">
          <div className="row justify-content-center">
            {!this.state.PrefernceData ? (
              <div data-test="component-StartWithDatum" className="col-12">
                <div className="row justify-content-center">
                  <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
                    {/* Img Module */}
                    <div className={classes.startWithDatumLogo}>
                      <img
                        src={require("../../../../../asset/new_assets/unfiltered.png")}
                        title="Getstarted"
                        alt="Getstarted"
                        width={400}
                      />
                    </div>
                    {/* Body Module */}
                    <div className="Wget_started">
                      <p className={`m-0 pb-3` + classes.relationshipStartsWith}>Relationships starts with</p>
                      <h4 className="m-0 pt-1 pb-3">
                        <b>
                          A community of people who are ready for something real. <span>Join us.</span>
                        </b>
                      </h4>
                      <button data-test="component-StartWithDatumbtn" onClick={this.handlePrefernce} className={classes.btn}>
                        Get Started
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>

          {/* this Module Used for the All API Screen Data render */}
          {/* mapping loop on the api data for rendering ui screens */}
          {this.state.PrefernceData && this.state.PrefernceData.myPreferences && this.state.PrefernceData.myPreferences
            ? this.state.PrefernceData.myPreferences.map((pref, index) =>
                // check if the current selected screen is same as current index
                selectedScreenIndex === index
                  ? pref.data.map((prefItem, key) =>
                      // check if the current selected screen is same as current index
                      this.state.selectedPrefIndex === key ? (
                        <div key={index}>
                          <div className="col-12">
                            <div className="row justify-content-center">
                              <div className="text-center col-12 col-sm-10 col-md-10 col-lg-12 col-xl-12 signupinput_module">
                                <div className="py-1">
                                  <h3 className={classes.title}>
                                    {prefItem.title} <br />
                                    <span className={classes.label}>{prefItem.label}</span>
                                  </h3>
                                  {/* to skip a current screen */}
                                  <div className={classes.skipButton} onClick={this.handleskipscreen}>
                                    <p className="m-0">Skip</p>
                                  </div>
                                  {/* Pefer not to say Button */}
                                  <div className="Wuser_notsay">
                                    <input
                                      type="radio"
                                      id="Prefernottosay"
                                      name="radio-group"
                                      checked={this.state.selectedRadioButton}
                                      // value="Prefer not to say"
                                      onChange={this.handlenotsayradioChange}
                                    />
                                    <label htmlFor="Prefernottosay" className={classes.Prefernottosay}>
                                      Prefer not to say
                                    </label>
                                  </div>
                                </div>
                                <div className="py-2 row justify-content-center W_userdatainput">
                                  {/* get the user input dynamically based on type */}
                                  {this.getInput(
                                    prefItem.type,
                                    prefItem,
                                    prefItem.priority,
                                    this.state.PrefernceData.myPreferences[0].data[0].options
                                  )}
                                </div>
                                <div className="py-2 text-center">
                                  <button
                                    data-test="component-Seemebtn"
                                    type="submit"
                                    onClick={(e) =>
                                      this.state.selectedRadioButton || this.checkForEmptyData(this.state.value)
                                        ? this.handleNextStep(e)
                                        : this.showWarning()
                                    }
                                    className={
                                      this.state.selectedRadioButton || this.checkForEmptyData(this.state.value)
                                        ? classes.btn
                                        : classes.disablebtn
                                    }
                                    // className={!this.state.value ? classes.disablebtn : classes.btn}
                                    // disabled={this.state.value}
                                  >
                                    Next
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )
                    )
                  : ""
              )
            : ""}
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     setAllDiscoverPeople: data => dispatch(discoverPeople(data))
//   };
// };

export default withStyles(styles)(withRouter(StartWithDatum));
// export default connect(null, mapDispatchToProps)(withStyles(styles)(withRouter(StartWithDatum)));
