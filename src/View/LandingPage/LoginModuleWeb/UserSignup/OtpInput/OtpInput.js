import React, { Component } from "react";
import "./OtpInput.scss";
import "../SignUp/SignUp.scss";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import SignUp from "../SignUp/SignUp";
import ProfileInput from "../ProfileInput/ProfileInput";
import OTPInput from "otp-input-react";
import { otpVerification } from "../../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { Link } from "react-router-dom";
// import ResendOTP from "../../../LoginModuleMobile/UserSignup/OtpInput/ResendOtp";
import { mobileVerification } from "../../../../../controller/auth/verification";

const styles = () => ({
  arrow: {
    color: "#fff",
    backgroundColor: "#e31b1b",
    padding: 15,
    borderRadius: "50%",
  },
  disabledarrow: {
    color: "#fff",
    backgroundColor: "#b7b7b7",
    padding: 15,
    borderRadius: "50%",
  },
  btn: {
    color: "#fff",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    width: "95%",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#C8CFE0",
    fontWeight: "600",
    padding: "10px 2rem",
    borderRadius: "5px",
    width: "95%",
  },
  progress: {
    color: "#e31b1b",
    padding: "15vh",
    textAlign: "center",
  },
  resend: {
    color: "#fc3476",
    fontSize: "16px",
    fontFamily: "Product Sans Bold",
    fontWeight: 900,
    cursor: "pointer",
  },
  dummyText: {
    color: "#484848",
    fontSize: "14px",
    fontFamily: "Product Sans",
  },
  links: {
    cursor: "pointer",
    textDecoration: "underline",
  },
  sent: {
    color: "#21242A",
    fontSize: 20,
  },
  verifyYourMobileNumber: {
    fontSize: "24px",
    color: "#21242A",
    textAlign: "center",
    fontFamily: "Product Sans",
  },
  enterCodeSentTo: {
    color: "#585A60",
    fontSize: "13px",
    fontFamily: "Product Sans",
  },
});

class OtpMainInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      enterotp: "",
      valid: true,
      // valid: false, // actual state
      phoneNumber: "",
      open: false,
      loader: true,
      checkicon: false,
      isVisible: true,
      usermessage: "",
      currentCount: 59,
    };
  }

  componentDidMount() {
    var intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId: intervalId });
    // setTimeout(() => {
    //   this.autoRedirect();
    // }, 1500);
  }

  tryAgain = () => {
    var intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId: intervalId, currentCount: 59 });
  };

  timer = () => {
    var newCount = this.state.currentCount - 1;
    if (newCount >= 0) {
      this.setState({ currentCount: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  // Successfull Send Loader Animation
  componentWillMount() {
    clearInterval(this.state.intervalId);
    setTimeout(() => {
      this.setState({ loader: false, checkicon: true });
    }, 1000);
  }

  // Function for the Notification ( Snakbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snakbar )
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the OTP Input && Automatically redirect to the next Page
  handleclickOTP = (otp) => {
    this.setState({
      valid: otp && otp.length === 6 ? true : false,
      enterotp: otp,
    });
  };

  autoRedirect = () => {
    let otpPayload = {
      phoneNumber: this.props.globalnumber,
      type: 1,
      otp: this.state.enterotp,
    };

    otpVerification(otpPayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "OTP verify sucessfully.",
        });

        this.props.updateScreen(<ProfileInput updateScreen={this.props.updateScreen} {...this.props} />);
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Enter valid OTP.",
        });
      });
    this.props.dispatch(MOBILE_ACTION_FUNC("otp", this.state.enterotp));
  };

  // Function for the Button Click For Verify the OTP
  handleotp = (e) => {
    e.preventDefault();
    let otpPayload = {
      phoneNumber: this.props.globalnumber,
      type: 1,
      otp: this.state.enterotp,
      // email: this.props.globalemail
      // phoneNumber: this.props.globalnumber,
      // type: 1,
      // otp: this.state.enterotp,
      // email: this.props.email
    };

    otpVerification(otpPayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "OTP verify sucessfully.",
        });

        this.props.updateScreen(<ProfileInput updateScreen={this.props.updateScreen} {...this.props} />);
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Enter valid OTP.",
        });
      });
    this.props.dispatch(MOBILE_ACTION_FUNC("otp", this.state.enterotp));
  };

  // Function for the to back to the SignUp Page
  hanldePhoneNumber = () => {
    this.props.updateScreen(<SignUp updateScreen={this.props.updateScreen} />);
  };

  ResendOtp = () => {
    let mobilePayload = {
      phoneNumber: this.props.globalnumber,
      // email: globalnumber,
      type: "1",
      deviceId: "0101",
    };
    this.tryAgain();
    if (mobilePayload) {
      mobileVerification(mobilePayload)
        .then((data) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Otp is resend to the number",
            isVisible: false,
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 1300);
          setTimeout(() => {
            this.setState({ isVisible: true });
          }, 60000);
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Enter Correct OTP",
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 1300);
        });
    }
  };

  openPolicy = () => {
    const url = "https://appconfig.unfiltered.love/policy.html";
    window.open(url, "_blank");
  };

  openTermsAndPolicy = () => {
    const url = "https://appconfig.unfiltered.love/TermsAndConditions.html";
    window.open(url, "_blank");
  };

  render() {
    const { classes } = this.props;
    return (
      <div data-test="component-otp">
        {this.state.loader ? (
          <div className="text-center">
            <div className={classes.progress}>
              <div className="success-checkmark">
                <div className="check-icon">
                  <span className="icon-line line-tip" />
                  <span className="icon-line line-long" />
                  <div className="icon-circle" />
                  <div className="icon-fix" />
                </div>
                <p className="pt-2 m-0">
                  <b className={classes.sent}>Sent</b>
                </p>
              </div>
            </div>
          </div>
        ) : (
          // Verify Mobile Number Module
          <div className="col-12 p-5">
            <div className="row justify-content-center">
              <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
                <div className="Wopt">
                  <h3 className={`pb-4 ${classes.verifyYourMobileNumber}`}>Verify your mobile number</h3>
                  <div className={`pb-5 ${classes.enterCodeSentTo}`}>
                    Please enter the code sent to <b>{this.props.globalnumber}</b>
                  </div>
                  <div className="row justify-content-center">
                    <div className="col-12 verify_main">
                      <OTPInput
                        inputClassName="otp-input-class"
                        value={this.state.enterotp}
                        onChange={(otp) => this.handleclickOTP(otp)}
                        autoFocus
                        OTPLength={6}
                        otpType="number"
                        disabled={false}
                        // secure
                      />
                      {/* <OtpInput
                        onChange={(otp) => this.handleclickOTP(otp)}
                        numInputs={6}
                        value={this.state.enterotp}
                        separator={<span> - </span>}
                        defaultValue="will focus"
                        shouldAutoFocus={true}
                      /> */}
                    </div>
                  </div>
                  <div className={`col-12 text-center pt-4 pb-4 ` + classes.resend} onClick={() => this.ResendOtp()}>
                    {this.state.currentCount === 0 ? <div>Resend</div> : <div>00:{this.state.currentCount}</div>}
                  </div>
                  <div className="py-3 text-center">
                    <button
                      data-test="component-verifybtn"
                      onClick={this.handleotp}
                      className={!this.state.valid ? classes.disablebtn : classes.btn}
                      disabled={!this.state.valid}
                    >
                      Continue
                    </button>
                  </div>
                  <div className={`col-12 text-center pt-4 ` + classes.dummyText}>
                    By Sign In, You’re Confirming that You’ve read and agree to our
                    <strong
                      className={classes.links}
                      onClick={this.openTermsAndPolicy}
                      style={{ textDecoration: "underline", cursor: "pointer" }}
                    >
                      Terms & Conditions
                    </strong>
                    and
                    <span className={classes.links} onClick={this.openPolicy} style={{ textDecoration: "underline", cursor: "pointer" }}>
                      Privacy Policy
                    </span>
                    and
                    <Link to="/cookie-policy" target="_blank">
                      <span style={{ textDecoration: "underline", cursor: "pointer" }} className={classes.links}>
                        Cookie Policy.{" "}
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    email: state.Main.UserData.email,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(OtpMainInput));
