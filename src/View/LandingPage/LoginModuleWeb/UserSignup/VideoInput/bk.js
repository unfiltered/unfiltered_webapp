// Main React Components
import React, { Component } from "react";

// Scss
import "../OtpInput/OtpInput.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// imported Components
import SeemeID from "../SeemeID/SeemeID";
import { Message } from "paho-mqtt";
import LocalMessage from '../../../../../message';

const styles = theme => ({
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px"
  }
});

class VideoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preview: null,
      file: "",
      imagePreviewUrl: "",
      valid: false,
      totalFiles: []
    };
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    let oldFilesArray = this.state.totalFiles;
    reader.onloadend = () => {
      oldFilesArray.push(reader.result);
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    };
    reader.readAsDataURL(file);
    console.log("Chnage--Clicked", file);
  }

  handleskipscreen = () => {
    this.props.handleScreen(
      <SeemeID
        updateScreen={this.updateScreen}
        mobilenumber={this.state.phone}
        cCode={this.state.cCode}
      />
    );
  };

  render() {
    const { classes } = this.props;
    let lang = localStorage.getItem('lang') || 'en';
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;

    if (imagePreviewUrl) {
      $imagePreview = (
        <img src={imagePreviewUrl} alt="UserProfile" title="UserProfile" />
      );
      // this.state.valid = true;
      console.log("Image-Upload-data", imagePreviewUrl);
    } else {
      $imagePreview = <div />;
    }

    return (
      <div>
        {/* Upload Picture Module */}
        <div className="m-0 row">
          <div className="col-lg-3" />
          <div className="text-center col-lg-6 signupinput_module">
            <div className="upload_header">
              <h3>
                Upload your Video
                <div className="right_skipbtn" onClick={this.handleskipscreen}>
                  <p className="m-0">Skip</p>
                </div>
              </h3>
              <p>
                Adding Your Video is a great way to show off your personality
              </p>
            </div>
            <div className="button-wrapper">
              {this.state.totalFiles && this.state.totalFiles.length < 1 ? (
                <div>
                  <label htmlFor="upload" className="label">
                    Upload Your File
                  </label>
                  <input
                    type="file"
                    name="upload"
                    id="upload"
                    className="upload-box"
                    placeholder={LocalMessage && LocalMessage[lang] && LocalMessage[lang]["message.videoInput"] ||  'Upload Your Video'}
                    accept="video/mp4,video/x-m4v,video/*"
                    onChange={e => this._handleImageChange(e)}
                  />
                  <button
                    className={classes.btn}
                    onClick={this.props.handleScreen.bind(
                      this,
                      <SeemeID handleScreen={this.props.handleScreen} />
                    )}
                  >
                    Next
                  </button>
                </div>
              ) : (
                ""
              )}
            </div>

            {this.state.totalFiles && this.state.totalFiles.length >= 1
              ? this.state.totalFiles.map(url => (
                  <div className="upload_preview">
                    <img src={url} alt="UserProfile" title="UserProfile" />
                    <div className="mt-4 upload_btn" />
                  </div>
                ))
              : ""}
          </div>
        </div>
        <div className="col-lg-3" />
      </div>
    );
  }
}

export default withStyles(styles)(VideoInput);
