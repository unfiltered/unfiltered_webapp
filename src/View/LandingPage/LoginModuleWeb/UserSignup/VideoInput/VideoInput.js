import "../OtpInput/OtpInput.scss";
import { connect } from "react-redux";
import ReactPlayer from "react-player";
import React, { Component } from "react";
import "../ProfileInput/ProfileInput.scss";
import { withStyles } from "@material-ui/core/styles";
import ReferralCode from "../ReferralCode/ReferralCode";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import CircularProgress from "@material-ui/core/CircularProgress";
import { setCookie } from "../../../../../lib/session";
import { profileVerification } from "../../../../../controller/auth/verification";
import { uploadVideoToCloudinaryPromise } from "../../../../../lib/cloudinary-image-upload";

const styles = () => ({
  btn: {
    color: "#fff",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    marginTop: "20px",
    borderRadius: "5px",
    width: "95%",
    fontFamily: "Product Sans",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#C8CFE0",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    marginTop: "30px",
    borderRadius: "5px",
    width: "95%",
    fontFamily: "Product Sans",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "18vh",
    textAlign: "center",
  },
  resend: {
    color: "#fc3476",
  },
  dummText: {
    color: "#585A60",
    fontSize: "14px",
    fontFamily: "Product Sans",
  },
  myBestVideoCaption: {
    fontSize: "24px",
    fontFamily: "Product Sans",
    color: "#292929",
    textAlign: "center",
  },
  myBestVideo: {
    fontSize: "24px",
    fontFamily: "Product Sans",
    color: "#e31b1b",
  },
  videoSizeWarning: {
    color: "#484848",
    fontSize: "16px",
    fontFamily: "Product Sans",
  },
  progressV1: {
    color: "#e31b1b",
    textAlign: "center",
  },
  uploadingVideo: {
    fontFamily: "Circular Air Book",
    fontSize: "16px",
    color: "#484848",
    paddingLeft: "10px",
  },
});

class ProfileVideoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      loader: false,
      open: false,
      usermessage: "Video size should be less than 50MB",
      variant: "success",
      ShowVideoFormat: false,
      size: 46447850,
      sizeAlertToShow: false,
      videoUploadLoader: false,
      loadUI: true,
    };
    this.files = [];
  }

  // UserVideo API Call Module
  profileapi = () => {
    this.setState({ loader: true });

    Promise.all(this.props.UserData.profileVideo)
      .then((data) => {
        delete this.props.UserData["carouselIndex"];

        profileVerification(this.props.UserData).then((data) => {
          this.setState({ loader: false });
          setCookie("token", data.data.data.token);
          this.props.updateScreen(<ReferralCode updateScreen={this.props.updateScreen} {...this.props} />);
          // this.props.updateScreen(<StartWithDatum updateScreen={this.props.updateScreen} {...this.props} />);
        });
      })
      .catch(function (err) {});
  };

  async componentDidMount() {
    setTimeout(() => {
      this.setState({ loadUI: false });
    }, 1000);
  }

  cloudinaryVideoStateHandler = (file) => {
    this.setState({ videoUploadLoader: true });
    if (file[0].size <= this.state.size) {
      this.files[0] = file;
      this.setState({ valid: true });
      uploadVideoToCloudinaryPromise(this.files)
        .then((res) => {
          this.setState({ videoUploadLoader: false });
          this.props.dispatch(MOBILE_ACTION_FUNC("profileVideo", res.body.secure_url));
        })
        .catch((err) => {
          this.setState({ videoUploadLoader: false });
        });
    } else {
      this.setState({
        sizeAlertToShow: true,
        videoUploadLoader: false,
        usermessage: "Video size should be less than 50MB",
      });
    }
  };

  handleskipscreen = () => {
    delete this.props.UserData["carouselIndex"];
    profileVerification(this.props.UserData).then((data) => {
      setCookie("token", data.data.data.token);
      return this.props.updateScreen(<ReferralCode updateScreen={this.props.updateScreen} {...this.props} />);
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <div data-test="component-video" className="col-12">
        <div className="row justify-content-center">
          <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
            {this.state.loader ? (
              <div className="text-center">
                <CircularProgress className={classes.progress} />
              </div>
            ) : (
              <div className="p-5">
                {/* header Module */}
                <div className="py-4">
                  <h3 className={classes.myBestVideoCaption}>
                    My Best <span className={classes.myBestVideo}>Video</span>
                  </h3>
                  <div className="right_skipbtn" onClick={this.handleskipscreen}>
                    <p className="m-0">Skip</p>
                  </div>
                  <p className={classes.dummText}>Take a short clip of an introduction or something you love!</p>
                </div>
                {this.state.videoUploadLoader ? (
                  <div className="flex-column">
                    <div className="d-flex justify-content-center align-items-center">
                      <CircularProgress className={classes.progressV1} />
                      <span className={classes.uploadingVideo}>Please wait while video is uploading...</span>
                    </div>
                  </div>
                ) : (
                  <div className="row Wuser_profile justify-content-center">
                    <div className="Wuserimg_preview Wuser_video">
                      {this.files
                        ? this.files.map((data, index) => (
                            <ReactPlayer key={index} url={this.props.UserData.profileVideo} playing={true} controls={true} />
                          ))
                        : ""}
                    </div>

                    {/* Video Upload Module */}
                    {this.files.length <= 0 ? (
                      <form encType="multipart/form-data">
                        <div className="ppppppp">
                          <label className="col-auto" htmlFor="VideoFile">
                            <div className="Waddprofile">
                              <i className="fas fa-plus" />
                              <p className="m-0">Add your video</p>
                            </div>
                            <input
                              style={{ visibility: "hidden" }}
                              type="file"
                              id="VideoFile"
                              className="Wuser_input"
                              ref={(fileInputEl) => (this.fileInputEl = fileInputEl)}
                              onChange={() => this.cloudinaryVideoStateHandler(this.fileInputEl.files)}
                              accept="mp4,video/mp4,video/x-m4v,video/*"
                            />
                          </label>
                        </div>
                      </form>
                    ) : (
                      ""
                    )}
                  </div>
                )}

                {/* Next Button Module */}
                <div className="WProfile_next">
                  {this.state.ShowVideoFormat ? (
                    <p
                      style={{
                        paddingTop: "15px",
                        color: "red",
                        fontWeight: "600",
                        margin: "0",
                      }}
                    >
                      Please Upload mp4 Video.
                    </p>
                  ) : (
                    ""
                  )}
                  <div className={`col-12 text-center pt-5 ` + classes.videoSizeWarning}>Video size should be less than 50MB</div>
                  <button
                    onClick={this.profileapi}
                    className={!this.state.valid ? classes.disablebtn : classes.btn}
                    disabled={!this.state.valid}
                  >
                    Continue
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>
        {/* <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.sizeAlertToShow} /> */}
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
    UserLocationDetails: state.UserProfile.UserData,
  };
}

export default connect(mapstateToProps, null)(withStyles(styles)(ProfileVideoInput));

// export default withStyles(styles)(ProfileVideoInput);
