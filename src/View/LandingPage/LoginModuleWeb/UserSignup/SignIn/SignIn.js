// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "../SignUp/SignUp.scss";

// Re-Usuable Components
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import MainModal from "../../../../../Components/Model/model";

// Reactstrap Components
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Container } from "reactstrap";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// imported Components
import OtpSignIn from "../OtpSignIn/OtpSignIn";

// Facebook Login Components
import FacebookLogin from "react-facebook-login";

// import React Node-Module for Mobile-Number Components
import "react-intl-tel-input/dist/main.css";
import IntlTelInput from "react-intl-tel-input";

// Images
import DatumLogo from "../../../../../asset/images/Main-Whitelogo.svg";
import Sorry from "../../../../../asset/images/Sorry.png";

// Redux Components
import { mobileVerification, Userfacebooklogin, PhoneExistVerification } from "../../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { keys } from "../../../../../lib/keys";

const styles = (theme) => ({
  root: {
    border: "2px solid #ccc",
    padding: "5px 12px",
    borderRadius: "11px",
    borderBottom: 0,
    "&:hover": {
      borderBottom: 0,
    },
  },
  focused: {
    border: "2px solid #e31b1b",
    borderRadius: "11px",
    "&:focus": {
      border: "none",
      outline: 0,
    },
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 20px",
    borderRadius: "12px",
  },
});

let globalnumber = "";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valuenumber: "",
      phone: "",
      dropdownOpen: false,
      date: {},
      errors: {},
      valid: null,
      selectedDate: new Date(),
      isDateSelected: false,
      age: "",
      labelWidth: 0,
      selectedOption: null,
      Finalvalue: "",
      nameValid: null,
      emailValid: null,
      mobileValid: null,
      FianlDOB: null,
      isLoggedIn: false,
      firstName: "",
      fbId: "",
      pushToken: "",
      email: "",
      picture: "",
      height: "",
      open: false,
      newValue: [],
      latititudelocal: "",
      longitudelocal: "",
      togglemodel: "",
      Usernumber: "",
      data: {
        phoneNumber: "",
      },
    };
    this.toggle = this.toggle.bind(this);
    this.togglemodel = this.togglemodel.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      document.getElementById("intelinputField").focus();
    }, 100);
  }

  // Function for the Toggle Error Model
  togglemodel() {
    this.setState((prevState) => ({
      togglemodel: !prevState.togglemodel,
    }));
  }

  // Function for the Focus Input Onload
  handleFocus = (event) => {
    event.target.select();
  };

  // the screen sent as argument to this function will be rendered, or make it false to render default screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  toggle() {
    this.setState((prevState) => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  onMouseEnter() {
    this.setState({ dropdownOpen: true });
  }
  onMouseLeave() {
    this.setState({ dropdownOpen: false });
  }

  // Function for changing the Screen
  handleScreen = (screen) => {
    this.setState({ currentScreen: screen });
  };

  // Function for the Phone Number Flag
  handleflag = (num) => {
    this.setState({ cCode: num.dialCode });
  };

  // Function for the Phone Number Value
  handlephone = (status, valuenumber, number, phoneNumber) => {
    this.setState(
      {
        valuenumber: status,
        cCode: number.dialCode,
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode));
      }
    );

    globalnumber = phoneNumber.replace(/ /g, "");

    this.props.dispatch(MOBILE_ACTION_FUNC("contactNumber", globalnumber));
  };

  // Function for the Notification
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification
  handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Dynemic Toster
  showToast = (open, type, msg) => {
    this.setState({
      open: open,
      variant: type,
      usermessage: msg,
    });
  };

  // Checking All Validation
  checkValidation = (e) => {
    e.preventDefault();
    if (!globalnumber) {
      this.showToast(true, "error", "Please Enter Valid Mobile Number");
      return;
    }
    this.handlesignup();
  };

  // Function for the SignUP Button
  handlesignup = () => {
    if (globalnumber) {
      this.sendOtp();
    }
  };

  // Function for Send OTP API
  sendOtp = () => {
    let mobileverfiyPayload = {
      phoneNumber: globalnumber,
    };

    let mobilePayload = {
      phoneNumber: globalnumber,
      type: 1,
      deviceId: "0909",
    };

    PhoneExistVerification(mobileverfiyPayload)
      .then((data) => {
        mobileVerification(mobilePayload).then((data) => {
          {
            this.handleScreen(
              <OtpSignIn
                handleScreen={this.handleScreen}
                mobilenumber={this.state.phone}
                cCode={this.state.cCode}
                updateScreen={this.updateScreen}
                globalnumber={globalnumber}
              />
            );
          }
        });
        this.setState({
          open: true,
          variant: "success",
          usermessage: "OTP Send Sucessfully.!!",
        });
      })
      .catch((err) => this.togglemodel());
  };

  responseFacebook = (response) => {
    if (response.status === "unknown") {
      this.setState({
        firstName: "",
        fbId: "",
        pushToken: "",
        email: "",
        picture: "",
        height: "",
        isLoggedIn: false,
      });
      localStorage.setItem("facebookLogin", false);
      localStorage.setItem("firstName", "");
      localStorage.setItem("email", "");
      localStorage.setItem("picture", "");
      localStorage.setItem("height", "");
    } else {
      this.setState(
        {
          isLoggedIn: true,
          firstName: response.name,
          fbId: response.id,
          pushToken: response.accessToken,
          email: response.email,
          picture: response.picture.data.url,
          height: response.picture.data.height,
        },
        () => {
          let facebookpayload = {
            firstName: this.state.firstName,
            fbId: this.state.fbId,
            pushToken: this.state.pushToken,
            email: this.state.email,
            deviceId: "15645646564989",
            deviceMake: "Apple",
            deviceModel: "Md-LG",
            deviceType: "1",
          };
          localStorage.setItem("firstName", response.name);
          localStorage.setItem("email", response.email);
          localStorage.setItem("picture", response.picture.data.url);
          localStorage.setItem("height", response.picture.data.height);
          localStorage.setItem("facebookLogin", true);
          Userfacebooklogin(facebookpayload).then((data) => {
            console.log("Userfacebooklogin => data =>", data);
          });
        }
      );
    }
  };

  render() {
    const { classes } = this.props;

    let fbContent;
    if (this.state.isLoggedIn) {
      fbContent = this.props.history.push("/app/");
    } else {
      fbContent = (
        <FacebookLogin
          accessToken={keys.facebookToken}
          cssClass="facebook_btn"
          appId={keys.facebookAppId}
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
        />
      );
    }

    return (
      <div data-test="component-app">
        {/* Navbar Module */}
        <div className="Wsignup">
          <Container>
            <div className="row">
              <div className="pt-1 col-4 col-lg-5 col-xl-5 text-left header_lang">
                <Dropdown
                  className="d-inline-block"
                  onMouseOver={this.onMouseEnter}
                  onMouseLeave={this.onMouseLeave}
                  isOpen={this.state.dropdownOpen}
                  toggle={this.toggle}
                >
                  <DropdownToggle caret>English</DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem>Russian</DropdownItem>
                    <DropdownItem>Chinese</DropdownItem>
                    <DropdownItem>Hindi</DropdownItem>
                    <DropdownItem>Spanish</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
              </div>
              <div className="col-4 col-md-3 col-lg-2 col-xl-2 text-center header_logo">
                <Link to="/">
                  <img src={DatumLogo} alt="UnFiltered" title="UnFiltered" className="img-fluid" width="46px" />
                </Link>
              </div>
              <div className="col-4 col-md-5 col-lg-5 col-xl-5 text-right">
                <div className="header_singin">
                  <p className="m-0"> Already a Member?</p>
                  <span>
                    <Link to="/signup">Sign Up</Link>
                  </span>
                </div>
              </div>
            </div>
          </Container>

          {/* Main Body Module */}
          {!this.state.currentScreen ? (
            <div className="col-12">
              <div className="row justify-content-center">
                <div className="col-12 col-sm-10 col-md-10 col-lg-8 col-xl-6 signup_model">
                  {/* Title Module */}
                  <div className="signup__title">
                    <p className="m-0 text-left">Sign In to {keys.AppName}</p>
                    <p className="text-left">
                      Please Enter Your Register <b>Mobile Number</b>
                    </p>
                  </div>
                  {/* Main Form Module */}
                  <form onSubmit={this.checkValidation}>
                    <div className="signup__form">
                      <div className="mr-0 pt-2 row">
                        <div className="pr-0 col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                          <p className="text-left">Mobile Number*</p>
                        </div>
                        <div className="pl-0 col-9 col-sm-10 col-md-10 col-lg-9 col-xl-9">
                          <form className="signup_mobile">
                            <IntlTelInput
                              preferredCountries={["TZ"]}
                              containerClassName="intl-tel-input"
                              inputClassName="form-control"
                              onSelectFlag={this.handleflag}
                              onPhoneNumberChange={this.handlephone}
                              formatOnInit={true}
                              separateDialCode
                              fieldId="intelinputField"
                              autofocus
                            />
                          </form>
                        </div>
                      </div>

                      {/* SignUp || Facebook button Module */}
                      <div className="row">
                        <div className="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2" />
                        <div className="p-0 d-sm-flex d-md-flex d-lg-flex d--xl-flex text-left col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 signup_btn">
                          <button data-test="component-signup" type="submit" className={classes.btn}>
                            Sign In
                          </button>
                          <div className="facebook_btn">{fbContent}</div>
                          {/* <div className="facebook_btn">{fbContent}</div> */}
                        </div>
                      </div>

                      {/* Condition Content Module */}
                      <div className="py-2 row">
                        <div className="col-auto col-sm-2 col-md-2 col-lg-2 col-xl-2" />
                        <div className="p-0 pt-2 col-12 col-sm-10 col-md-8 col-lg-9 col-xl-9 signup_notification">
                          <p className="m-0">
                            By Sign In, You're Confirming that You've read and agree to our
                            <Link to="/terms-and-conditions" target="_blank">
                              <u className="text-dark">Terms & Conditions</u>
                            </Link>
                            and{" "}
                            <Link to="/privacy-policy" target="_blank">
                              <u className="text-dark">Privacy Policy</u>
                            </Link>{" "}
                            and
                            <Link to="/cookie-policy" target="_blank">
                              <u className="text-dark">Cookie Policy.</u>
                            </Link>
                          </p>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          ) : (
            this.state.currentScreen
          )}
        </div>
        {/* Error Message For not Resgister Number Model */}
        <MainModal isOpen={this.state.togglemodel} ref={this.erroeRef}>
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="py-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.togglemodel} />
              <img src={Sorry} alt="NoData" title="NoData" />
              <div className="py-2 Werror_heading">
                <h2 className="m-0">HEY WAIT !!</h2>
              </div>
              <h5 className="m-0">Your Mobile Number is not Register with us.!!</h5>
              <h5>Plz, go to SignUp Page & Register.</h5>
              <button>
                <Link to="/signup">SignUp</Link>
              </button>
            </div>
          </div>
        </MainModal>

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default withStyles(styles)(SignIn);
