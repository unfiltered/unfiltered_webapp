// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";

// Scss
import "../OtpInput/OtpInput.scss";
import "../SignUp/SignUp.scss";

// Re-Usuable Components
import Snackbar from "../../../../../Components/Snackbar/Snackbar";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// User GeoLocation Components
import { geolocated } from "react-geolocated";

// For Validating OTP
import OtpInput from "react-otp-input";

// Images
import VerifyUser from "../../../../../asset/images/Verify.png";

// Redux COmponents
import { connect } from "react-redux";
import { otpVerification } from "../../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { setCookie, getCookie } from "../../../../../lib/session";
import * as userActions from "../../../../../actions/User";

const styles = () => ({
  arrow: {
    color: "#fff",
    backgroundColor: "#e31b1b",
    padding: 15,
    borderRadius: "50%",
  },
  disabledarrow: {
    color: "#fff",
    backgroundColor: "#b7b7b7",
    padding: 15,
    borderRadius: "50%",
  },
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "30vh",
    textAlign: "center",
  },
});

class OtpMainInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      valid: false,
      phoneNumber: "",
      open: false,
      loader: true,
      checkicon: false,
      latititudelocal: "",
      longitudelocal: "",
      useraddress: "",
      userotpvalue: "",
    };
  }

  // Used for the Get User Current latititudelocal && longitudelocal with Full Address
  static getDerivedStateFromProps(nextProps) {
    console.log("OtpSignIn.js", nextProps, getCookie("lat"), getCookie("long"));
    const userlocation = {
      latititudelocal: nextProps.coords ? nextProps.coords.latitude : "",
      longitudelocal: nextProps.coords ? nextProps.coords.longitude : "",
    };
    let google = window.google;
    if (google) {
      let geocoder = new google.maps.Geocoder();
      let latlng = new google.maps.LatLng(userlocation.latititudelocal, userlocation.longitudelocal);
      geocoder.geocode({ latLng: latlng }, (results, status) => {
        let resLength = results.length;
        if (status === google.maps.GeocoderStatus.OK) {
          setCookie("location", results[0].formatted_address);
          setCookie("UserCurrentCity", results[resLength - 1].formatted_address);
          setCookie("citylocation", results[resLength - 1].formatted_address);
        }
      });
    }
    return userlocation;
  }

  // Used to get User Current Long && Lot
  dispatchLatitude(lat, long) {
    this.props.setLatitude(lat);
    setCookie("lat", lat);
    this.props.setLongiitutde(long);
    setCookie("long", long);
  }

  componentDidUpdate(nextProps, prevState) {
    console.log("prevState.pathname", nextProps);
    console.log("this.props.pathname", prevState);
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false, checkicon: true });
    }, 1000);
  }

  handleclickOTP = (otp) => {
    this.setState({ value: otp });
  };

  // Function for the Notification
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // After otp Enter it will automatically redirect to next Page
  handleOTPChange = (otp) => {
    let otpPayload = {
      phoneNumber: this.props.globalnumber,
      type: 1,
      otp: Number(otp),
      // longitude: this.state.latititudelocal,
      // latitude: this.state.longitudelocal
    };

    if (otp.length >= 6) {
      otpVerification(otpPayload)
        .then((data) => {
          // setCookie("token", data.data.token);
          this.setState({
            open: true,
            variant: "success",
            usermessage: "OTP Verify Sucessfully.!!",
            valid: true,
          });
          this.props.history.push("/app/");
        })
        .catch((error) => {
          console.log("eeeeeeeeeeeeee", error);
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Enter Valid OTP.!!",
            valid: false,
          });
        });
      this.props.setOTP(otp);
    }
  };

  render() {
    console.log("UserMobileNumber", this.props);
    const { classes } = this.props;

    return (
      <div>
        {/* to Get Latt && Long of Location */}
        {!this.props.isGeolocationAvailable ? (
          <div />
        ) : !this.props.isGeolocationEnabled ? (
          <div />
        ) : this.props.coords ? (
          this.dispatchLatitude(this.props.coords.latitude, this.props.coords.longitude)
        ) : (
          <div />
        )}
        {/* // Verify Mobile Number Module */}
        <div className="col-12">
          <div className="row justify-content-center">
            <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12 signupinput_module">
              <div className="py-4">
                <img src={VerifyUser} className="img-fluid" alt="Verify" title="Verify" />
              </div>
              <div className="Wopt">
                <h3>Verify Your Mobile Number</h3>
                <p>
                  We Will Send you a Message on <b>{this.props.globalnumber}</b>, Which Contain OTP.
                </p>
                <div className="row justify-content-center">
                  <div className="col-6 verify_main">
                    <OtpInput
                      value={this.handleclickOTP}
                      numInputs={6}
                      value={this.state.enterotp}
                      separator={<span> - </span>}
                      onChange={this.handleOTPChange}
                      defaultValue="will focus"
                      shouldAutoFocus={true}
                    />
                  </div>
                </div>
                <div className="py-3 text-center">
                  <button onClick={this.handleotp} className={!this.state.valid ? classes.disablebtn : classes.btn} disabled={!this.state.valid}>
                    Verify Number
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* // )} */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLatitude: (lan) => dispatch(userActions.setLatitude(lan)),
    setLongiitutde: (lan) => dispatch(userActions.setLongiitutde(lan)),
    setOTP: (otp) => dispatch(userActions.OTP(otp)),
  };
};

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(
    withStyles(styles)(
      geolocated({
        positionOptions: { enableHighAccuracy: false },
        userDecisionTimeout: 5000,
      })(OtpMainInput)
    )
  )
);
