// Main React Components
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

// Scss
import "../OtpInput/OtpInput.scss";
import "../SignUp/SignUp.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// Imported Components
import ProfileInput from "../ProfileInput/ProfileInput";

// User GeoLocation Components
import { geolocated } from "react-geolocated";

// For Validating OTP
import OtpInput from "react-otp-input";

// Images
import VerifyUser from "../../../../../asset/images/Verify.png";

// Redux COmponents
import { connect } from "react-redux";
import { otpVerification } from "../../../../../controller/auth/verification";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { setCookie } from "../../../../../lib/session";

const styles = () => ({
  arrow: {
    color: "#fff",
    backgroundColor: "#e31b1b",
    padding: 15,
    borderRadius: "50%",
  },
  disabledarrow: {
    color: "#fff",
    backgroundColor: "#b7b7b7",
    padding: 15,
    borderRadius: "50%",
  },
  btn: {
    color: "#fff",
    backgroundColor: "#F6376D",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
  disablebtn: {
    color: "#fff",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "12px",
  },
});

class OtpSignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      valid: false,
      latititudelocal: "",
      longitudelocal: "",
      useraddress: "",
      loader: true,
      userotpvalue: "",
    };
  }

  // Used for the Get User Current latititudelocal && longitudelocal with Full Address
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   const userlocation = {
  //     latititudelocal: nextProps.coords ? nextProps.coords.latitude : "",
  //     longitudelocal: nextProps.coords ? nextProps.coords.longitude : ""
  //   };

  //   let google = window.google;
  //   let geocoder = new google.maps.Geocoder();
  //   let latlng = new google.maps.LatLng(userlocation.latititudelocal, userlocation.longitudelocal);

  //   geocoder.geocode({ latLng: latlng }, (results, status) => {
  //     if (status == google.maps.GeocoderStatus.OK) {
  //       console.log("result is..", results[0].formatted_address);
  //       setCookie("location", results[0].formatted_address);
  //     }
  //   });
  //   return userlocation;
  // }

  componentDidUpdate(nextProps, prevState) {
    console.log("prevState.pathname", nextProps);
    console.log("this.props.pathname", prevState);
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false, checkicon: true });
    }, 1000);
  }

  // After otp Enter it Enable the button
  handleOTPChange = (otp) => {
    if (otp.length >= 6) {
      this.setState({ userotpvalue: otp, valid: otp });
      console.log("otpotpotpotpotp", this.state.userotpvalue);
    }
  };

  // Function for the Verify OTO at the time of SIgnin
  handlehomepage = (otp) => {
    let otpPayload = {
      phoneNumber: "+" + this.props.cCode + this.props.mobilenumber,
      type: 1,
      otp: this.state.userotpvalue,
      longitude: this.state.latititudelocal,
      latitude: this.state.longitudelocal,
    };

    otpVerification(otpPayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "OTP Verify Sucessfully.!!",
          valid: true,
        });
        this.props.history.push("/app/");
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Enter Valid OTP.!!",
          valid: false,
        });
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        {/* {this.state.loader ? (
          <div className="text-center">
            <div className={classes.progress}>
              <div className="success-checkmark">
                <div className="check-icon">
                  <span className="icon-line line-tip" />
                  <span className="icon-line line-long" />
                  <div className="icon-circle" />
                  <div className="icon-fix" />
                </div>
                <p className="pt-2 m-0">
                  <b>OTP Send Successfully.!!</b>
                </p>
              </div>
            </div>
          </div>
        ) : ( */}
        {/* Verify Mobile Number Module */}
        <div className="col-lg-12">
          <div className="row">
            <div className="col-lg-3 col-xl-3" />
            <div className="text-center col-lg-6 col-xl-6 signupinput_module">
              <div className="my-2">
                <img src={VerifyUser} className="img-fluid" alt="Verify" title="Verify" />
              </div>
              <div className="my-1">
                <h3>Verify Your Mobile Number</h3>
                <p>We Will Send you a Meddage, Which Contain 4 Digit Number</p>
                <div className="row justify-content-center">
                  <div className="col-6 verify_main">
                    <OtpInput numInputs={6} separator={<span> - </span>} onChange={this.handleOTPChange} defaultValue="will focus" autoFocus />
                  </div>
                </div>
                <div className="pt-3 text-center">
                  <button onClick={this.handlehomepage} className={!this.state.valid ? classes.disablebtn : classes.btn} disabled={!this.state.valid}>
                    Verify !!
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-xl-3" />
          </div>
        </div>
        {/* )} */}
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {};
};

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(
    withStyles(styles)(
      geolocated({
        positionOptions: { enableHighAccuracy: false },
        userDecisionTimeout: 5000,
      })(OtpSignIn)
    )
  )
);
