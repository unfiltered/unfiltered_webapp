// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "./style.scss";

// JQuery
import $ from "jquery";

// Main Login-Model Components
import MainLogin from "../LoginModuleWeb/UserSignup/SignUp/SignUp";

// Main Reactstrap Components
import { Container, Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Row, Modal } from "reactstrap";

// Imported Images
import Datumlogo from "../../../asset/images/Main-Whitelogo.svg";

export default class LandingHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      modal: false,
      menuOpen: false,
    };
    this.toggle = this.toggle.bind(this);
    this.togglemodel = this.togglemodel.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  openMenu() {
    this.setState({ menuOpen: true });
  }

  closeMenu() {
    this.setState({ menuOpen: false });
  }

  togglemodel() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  handleScreen = (screen) => {
    this.setState({ currentScreen: screen });
  };

  componentDidMount() {
    $(document).ready(function () {
      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 300) {
          $(".MainDesktopHeader").css("background", "linear-gradient(to bottom right, #e31b1b 0%, #ff7b43 100%)");
          $(".MainDesktopHeader").css("margin", "0 -50px");
          $(".MainDesktopHeader").css("box-shadow", "rgba(68, 68, 68, 0.6) 5px 5px 5px");
          $(".navbar-toggler").css("color", "#fff");
          $(".navbar-toggler").css("border-color", "#fff");
        }
        if ($(window).width() < 767) {
          $(".MainDesktopHeader").css("margin", "0");
        }
        if (scroll < 300) {
          $(".MainDesktopHeader").css("background", "transparent");
          $(".MainDesktopHeader").css("transition", "all 0.3s ease");
          $(".MainDesktopHeader").css("margin", "0");
          $(".MainDesktopHeader").css("box-shadow", "none");
          $(".navbar-toggler").css("color", "rgba(0,0,0,.5)");
          $(".navbar-toggler").css("border-color", "rgba(0,0,0,.1)");
        }
      });
    });
  }

  render() {
    return (
      <div>
        {/* Desktop View for the Header Start*/}
        <div className="MainDesktopHeader">
          <Container fluid>
            <Navbar light expand="md">
              <NavbarBrand href="/">
                <img src={Datumlogo} alt="UnFiltered" title="UnFiltered" width="130px" />
              </NavbarBrand>

              <NavbarToggler onClick={this.toggle} />

              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <NavLink href="/">Home</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/">About</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/">Service</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink className="MenuModel">
                      <Link to="/signup">Create Account</Link>
                    </NavLink>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
          </Container>
        </div>

        {/* Model (POP-UP) Open For Log-In */}
        <Row>
          <Modal isOpen={this.state.modal} togglemodel={this.togglemodel} className={this.props.className}>
            {/* Login Module Model Call */}
            <MainLogin handleScreen={this.handleScreen} togglemodel={this.togglemodel} currentScreen={this.state.currentScreen} />
          </Modal>
        </Row>
      </div>
    );
  }
}
