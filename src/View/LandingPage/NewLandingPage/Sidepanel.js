// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Reactstrap Components
import { Label, Input } from "reactstrap";

// Imported Images
import ios from "../../../asset/images/ios.png";
import android from "../../../asset/images/android.png";

class Sidepanel extends Component {
  render() {
    return (
      <div className="WSidepanel">
        <div>
          {/* Menubar Module */}
          <ul>
            <li>
              <Link to="/signup">
                <p>Create Account</p>
              </Link>
            </li>
            <li>
              <Link to="/signin">
                <p>Sign In</p>
              </Link>
            </li>
            <li>
              <Link to="/">
                <p>Privacy Policy</p>
              </Link>
            </li>
            <li>
              <Link to="/">
                <p>Terms Of Service</p>
              </Link>
            </li>
          </ul>

          {/* Change languges Module */}
          <div className="WMenu_languages">
            <Label for="Select_Language">Language</Label>
            <Input type="select" name="select" id="Select_Language">
              <option>English</option>
              <option>French</option>
              <option>Russian</option>
              <option>Italian</option>
              <option>Polish</option>
              <option>Thai</option>
              <option>Spanish</option>
            </Input>
          </div>

          {/* Dowanload Module */}
          <ul className="Wdownload">
            <li>
              <a
                href="https://play.google.com/store/apps/details?id=com.datum.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={android} alt="android" title="android" />
              </a>
            </li>
            <li className="pt-3">
              <a
                href="https://itunes.apple.com/in/app/datum-dating-and-chatting-app/id931927850?mt=8"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={ios} alt="ios" title="ios" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Sidepanel;
