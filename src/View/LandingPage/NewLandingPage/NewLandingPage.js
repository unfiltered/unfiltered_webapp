// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "./NewLandingPage.scss";

// Re-usuable Components
import MainDrawer from "../../../Components/Drawer/LeftDrawer";

// Material-Ui Components
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";

// JQuery
import $ from "jquery";

// Imported Components
import Sidepanel from "./Sidepanel";

// Imported Images
import WebLogo from "../../../asset/images/Main-Whitelogo.svg";
import iphone from "../../../asset/images/iphone.png";
import One from "../../../asset/images/first.png";
import two from "../../../asset/images/secound.png";
import three from "../../../asset/images/third.png";
import fourth from "../../../asset/images/fourth.png";
import fifth from "../../../asset/images/fifth.png";
import six from "../../../asset/images/six.png";
import ios from "../../../asset/images/ios.png";
import android from "../../../asset/images/android.png";

const drawerWidth = 240;

const styles = (theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    // marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: "0",
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing.unit * 3,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    // marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: parseInt(+drawerWidth + 60),
  },
});

class NewLandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Sidepanel: false,
    };
  }

  componentDidMount() {
    $(document).ready(function () {
      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 300) {
          $(".WNavbar").css("background", "#e31b1b");
          $(".WNavbar").css("z-index", "9999");
          // $(".WNavbar").css("width", "calc(100% - 300px)");
        }

        if (scroll < 300) {
          $(".WNavbar").css("background", "transparent");
          $(".WNavbar").css("transition", "all 0.2s ease");
          // $(".WNavbar").css("width", "100%");
        }

        if ($(window).width() < 767) {
          $(".WNavbar").css("margin", "0");
        }

        if (scroll > 700) {
          $(".WMobile_video").css("position", "sticky");
          $(".WMobile_video").css("top", "10rem");
          $(".WMobile_video").css("width", "100%");
        }

        if (scroll < 700) {
          $(".WMobile_video").css("position", "static");
          $(".WMobile_video").css("top", "0");
        }
      });
    });
  }

  // Toggle Function for Drawer
  toggleSidepaneldrawer = () => {
    this.setState({ Sidepanel: !this.state.Sidepanel });
  };

  render() {
    const { classes } = this.props;

    const SidePanel = <Sidepanel onClose={this.closeSidepaneldrawer} />;

    return (
      <div>
        <MainDrawer width={400} variant="persistent" onClose={this.closeSidepaneldrawer} onOpen={this.openSidepaneldrawer} open={this.state.Sidepanel}>
          {SidePanel}
        </MainDrawer>

        <main
          className={classNames(classes.content, {
            [classes.contentShift]: this.state.Sidepanel,
          })}
        >
          {/* Navbar Module */}
          <div className="py-3 WNavbar">
            <i className="fas fa-bars" onClick={this.toggleSidepaneldrawer} />
            <div className="text-center">
              <img src={WebLogo} alt="UnFiltered" title="UnFiltered" width="70px" />
            </div>
          </div>

          {/* Main Babnner Module */}
          <div className="WMbanner">
            <div className="WMainbg_title">
              <h1>
                Find the people <br /> near you.
              </h1>
            </div>
            <div className="Wdownload_app">
              <a href="https://play.google.com/store/apps/details?id=com.datum.com" target="_blank" rel="noopener noreferrer">
                <img src={android} alt="android" title="android" />
              </a>
              <a href="https://itunes.apple.com/in/app/datum-dating-and-chatting-app/id931927850?mt=8" target="_blank" rel="noopener noreferrer">
                <img src={ios} alt="ios" title="ios" />
              </a>
            </div>
          </div>

          {/* Main Body Party Module */}
          <div className="container">
            <div className="WMain_body">
              <div className="col-12">
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div className="Whome_step1">
                      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
                      <p className="m-0">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </p>
                    </div>

                    <div className="Whome_stepcmn">
                      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
                      <p className="m-0">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </p>
                    </div>

                    <div className="Whome_stepcmn">
                      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
                      <p className="m-0">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </p>
                    </div>

                    <div className="Whome_stepcmn">
                      <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
                      <p className="m-0">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      </p>
                    </div>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div className="text-center WMobile_video">
                      <img src={iphone} alt="Video" title="Video" width="330px" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* commitments Module */}
          <div className="py-5 WCommits">
            <div className="container">
              <div className="col-12">
                <h5> Lorem ipsum dolor</h5>
              </div>
              <div className="row WCommits_type">
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={One} alt="location" title="location" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </b>
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={two} alt="report-block-notInterested" title="report-block-notInterested" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</b>
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={three} alt="mutual-friends" title="mutual-friends" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</b>
                  </p>
                </div>
              </div>
              <div className="row WCommits_type">
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={fourth} alt="notification" title="notification" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</b>
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={fifth} alt="write-about-you" title="write-about-you" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</b>
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                  <img src={six} alt="chat-Now-or-later" title="chat-Now-or-later" />
                  <p className="m-0">
                    <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</b>
                  </p>
                </div>
              </div>
            </div>
          </div>

          {/* Footer Module */}
          <div className="Wfooter">
            <div className="Wfooter_title">
              <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
            </div>
            <p>Demo User</p>
            <div className="WFooter_App">
              <a href="https://play.google.com/store/apps/details?id=com.datum.com" target="_blank" rel="noopener noreferrer">
                <img src={android} alt="android" title="android" />
              </a>
              <a href="https://itunes.apple.com/in/app/datum-dating-and-chatting-app/id931927850?mt=8" target="_blank" rel="noopener noreferrer">
                <img src={ios} alt="ios" title="ios" />
              </a>
            </div>
          </div>

          {/* Footer Menu Module */}
          <div className="text-center Wfooter_menu">
            <div className="py-2 col-12 ">
              <ul>
                <li>
                  <Link to="/">
                    <p>Sign In</p>
                  </Link>
                </li>
                <li>
                  <Link to="/">
                    <p>Create Account</p>
                  </Link>
                </li>
                <li>
                  <Link to="/">
                    <p>Privacy Policy</p>
                  </Link>
                </li>
                <li>
                  <Link to="/">
                    <p>Terms Of Service</p>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(NewLandingPage);
