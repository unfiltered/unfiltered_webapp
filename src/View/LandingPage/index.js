import React, { Component } from "react";
import MainLogin from "./LoginModuleMobile/UserSignup/MainLogin/MainLogin";
import TinderPage from "./TinderPage/Index";
import { mobileVerification, emailVerfication } from "../../controller/auth/verification";

class Index extends Component {
  state = {
    width: window.innerWidth,
  };

  componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  // the screen sent as argument to this function will be rendered if api call gets successfully
  handleScreen = (screen, screenData) => {
    switch (screenData.type) {
      case "loginWithMobile":
        mobileVerification(screenData.data).then((data) => {
          this.updateScreen(screen);
        });
        break;

      case "verifyEmail":
        emailVerfication(screenData.data).then((data) => {
          this.updateScreen(screen);
        });
        break;
    }
  };

  // the screen sent as argument to this function will be rendered, or make it false to render default screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  render() {
    const { width } = this.state;
    const isMobile = width <= 576;

    // Mobile View Module
    if (isMobile) {
      return (
        <MainLogin
          handleScreen={this.handleScreen}
          currentScreen={this.state.currentScreen}
          isMobile={isMobile}
          updateScreen={this.updateScreen}
        />
      );
    } else {
      return <TinderPage />;
    }
  }
}

export default Index;
