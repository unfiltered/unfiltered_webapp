import React from "react";
import { Icons } from "../../../Components/Icons/Icons";
import { withRouter } from "react-router-dom";

const Footer = ({ history }) => {
  const openYoutube = () => {
    window.open("https://www.youtube.com/channel/UCY2Lv67lUTeWAe1OEk7jetA", "_blank");
  };

  const openInstagram = () => {
    window.open("https://www.instagram.com/unfiltered.love/", "_blank");
  };

  const openTwitter = () => {
    window.open("https://twitter.com/UnFiltered_App", "_blank");
  };

  const openFacebook = () => {
    window.open("https://www.facebook.com/UnFilteredApp", "_blank");
  };

  const openPrivacyPolicy = () => {
    window.open("https://unfiltered.love/privacy/", "_blank");
  };

  const openTermsAndConditions = () => {
    window.open("https://unfiltered.love/terms/", "_blank");
  };

  const openFAQ = () => {
    window.open("http://unfiltered.love/help/", "_blank");
  };

  return (
    <footer className="realFooter col-12">
      <div className="row">
        <div className="col-5">
          <div>
            <img src={Icons.Youtube} height={26} width={26} onClick={openYoutube} />
            <img src={Icons.Instagram} height={20} width={20} onClick={openInstagram} />
            <img src={Icons.TwitterLogo} height={20} width={20} onClick={openTwitter} />
            <img src={Icons.FacebookApp} height={20} width={20} onClick={openFacebook} />
          </div>
        </div>
        <div className="col-7">
          <div>
            Copyright &#169; 2021 | UnFiltered |<span onClick={openPrivacyPolicy}>Privacy Policy</span> |{" "}
            <span onClick={openTermsAndConditions}>Terms And Conditions</span> |<span onClick={openFAQ}>Help</span>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default withRouter(Footer);
