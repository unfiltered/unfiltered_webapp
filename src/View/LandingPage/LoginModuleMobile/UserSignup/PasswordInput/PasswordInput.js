import React, { Component } from "react";
import "../EmailLogin/EmailLogin.scss";
import { connect } from "react-redux";
import "react-phone-input-2/lib/style.css";
import { Icons } from "../../../../../Components/Icons/Icons";
import Input from "../../../../../Components/Input/MatUI_Input";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withRouter } from "react-router-dom";
import FirstName from "../FirstName/FirstnameInput";
import EmailInput from "../EmailInput/EmailInput";

class PasswordInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      variant: "success",
      usermessage: "",
      open: false,
      screenData: {
        type: "verifyEmail",
        data: {
          password: "",
        },
      },
    };
    this.goBack = this.goBack.bind(this);
  }

  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  goBack() {
    this.props.updateScreen(
      <EmailInput handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />
    );
  }

  handlePassword = (event) => {
    let tmpState = this.state;
    tmpState["screenData"]["data"]["password"] = event.target.value || this.state.password;
    this.setState({
      password: event.target.value,
    });
    this.props.dispatch(MOBILE_ACTION_FUNC("password", this.state.password));
  };

  goNextScreen = () => {
    if (this.state.password.length > 5) {
      this.props.updateScreen(
        <FirstName handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />
      );
    } else if (this.state.password.length === 0) {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Password cannot be empty...",
      });
    } else {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Password should be more than 6 characters.",
      });
    }
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.goNextScreen();
  };

  render() {
    return (
      <div className="col-12 m_addFriendWOptions" style={{ height: "100vh" }}>
        <div className="row">
          <div className="col-12 py-3 m_addFriendWOptionsHeader">
            <div className="row">
              <div className="col-4" onClick={this.goBack}>
                <img src={Icons.PinkBack} alt="Back" height={20} width={25} />
              </div>
            </div>
          </div>
          <div className="m_addFriendScreen">
            <div className="col-12 m-label-common1">Enter Your</div>
            <div className="col-12 label-common2">Password</div>
          </div>
          <section className="w-100">
            <form className="col-12 pt-2 addFriendInput w-100 d-flex flex-column" onSubmit={this.onSubmit}>
              <div className="m_custom_email-input">
                <Input
                  required={true}
                  multiline={false}
                  type="password"
                  label="Password"
                  placeholder="******"
                  onChange={this.handlePassword}
                />
              </div>
            </form>
          </section>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          timeout={1500}
          onClose={this.handleClose}
        />
        <div className="MCommonbottomnextarrow">
          <ArrowNext onClick={this.goNextScreen} disabled={this.state.password.length < 6} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {};
};

export default withRouter(connect(mapStateToProps, null)(PasswordInput));
