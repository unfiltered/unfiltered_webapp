import React, { Component } from "react";
import "./Mobileinput.scss";
import { withStyles } from "@material-ui/core/styles";
import OTPVerification from "../OtpInput/OtpInput";
import "react-intl-tel-input/dist/main.css";
// import IntlTelInput from "react-intl-tel-input";
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import { PhoneExistVerification } from "../../../../../controller/auth/verification";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { FormattedMessage } from "react-intl";

const styles = (theme) => ({
  progress: {
    margin: theme.spacing.unit * 2,
    color: "#e31b1b",
  },
  btn: {
    width: "95%",
    backgroundColor: "#e31b1b",
    border: "1px solid #e31b1b",
    padding: "8px 0",
    color: "#fff",
    fontFamily: "Circular Air Book",
  },
  disablebtn: {
    width: "95%",
    fontFamily: "Circular Air Book",
    backgroundColor: "#eef7fe",
    border: "1px solid #eef7fe",
    padding: "8px 0",
  },
  disablebtnwp: {
    width: "95%",
    backgroundColor: "#f4f8fb",
    border: "1px solid #dde1e4",
    padding: "8px 0",
    margin: "10px 0",
    color: "#888",
  },
  btnwp: {
    width: "95%",
    backgroundColor: "#fff",
    border: "1px solid #000",
    padding: "8px 0",
    margin: "10px 0",
    color: "#000",
  },
});

let placeholder = "";
let globalnumber = "";

class Mobileinput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valuenumber: "",
      phone: "",
      cCode: "",
      screenData: {
        type: "loginWithMobile",
        data: {
          phoneNumber: "",
          type: "1",
          deviceId: "0909",
        },
      },
    };
    this.handlephone = this.handlephone.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  // static getDerivedStateFromProps(props, state) {
  //   console.log("LLLLL", props, state);
  //   let pNo = props && props.UserData && props.UserData.contactNumber;
  //   let cc = props && props.UserData && props.UserData.countryCode;
  //   let phNo = pNo && pNo.substr(cc.length, pNo.length - 1);
  //   return {
  //     phone: phNo,
  //     cCode: cc,
  //   };
  // }

  // Function for the Phone Number Value
  handlephone = (value, country, e, formattedValue) => {
    this.setState({
      phone: value,
      cCode: country.dialCode,
    });
    let tmpState = this.state;
    tmpState["screenData"]["data"]["phoneNumber"] = value.trim();
    this.props.dispatch(MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode));
    this.props.dispatch(MOBILE_ACTION_FUNC("contactNumber", value.trim()));
  };

  // Function for the Notification (Snakbar)
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification (Snakbar)
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  goBack() {
    this.props.updateScreen(false);
  }

  handlemobilenumber = () => {
    if (this.state.phone) this.sendOtp();
  };

  // Function for Send OTP API
  sendOtp = () => {
    let mobileverfiyPayload = {
      phoneNumber: this.state.phone,
    };

    PhoneExistVerification(mobileverfiyPayload)
      .then((data) => {
        this.props.handleScreen(
          <OTPVerification
            value={this.state.value}
            handleScreen={this.props.handleScreen}
            isMobile={this.props.isMobile}
            mobilenumber={this.state.phone}
            cCode={this.state.cCode}
            updateScreen={this.props.updateScreen}
            ChangeScreen={this.ChangeScreen}
          />,
          this.state.screenData
        );
      })
      .catch((err) => {
        this.props.handleScreen(
          <OTPVerification
            value={this.state.value}
            handleScreen={this.props.handleScreen}
            isMobile={this.props.isMobile}
            mobilenumber={this.state.phone}
            cCode={this.state.cCode}
            updateScreen={this.props.updateScreen}
            ChangeScreen={this.ChangeScreen}
          />,
          this.state.screenData
        );
      });
  };

  handlemobilenumbereditable = () => {
    if (globalnumber) {
      this.sendOtpupdate();
    } else {
    }
  };

  // Function for Send OTP Editable Phone NUmber API
  sendOtpupdate = () => {
    let mobileverfiyPayload = {
      phoneNumber: globalnumber,
      type: 1,
      deviceId: "0909",
    };

    PhoneExistVerification(mobileverfiyPayload)
      .then((data) => {
        this.props.handleScreen(
          <OTPVerification
            handleScreen={this.props.handleScreen}
            isMobile={this.props.isMobile}
            mobilenumber={this.state.phone}
            cCode={this.state.cCode}
            updateScreen={this.props.updateScreen}
            ChangeScreen={this.ChangeScreen}
            iseditable={this.props.iseditable}
          />,
          this.state.screenData
        );
      })
      .catch((err) => {
        this.props.handleScreen(
          <OTPVerification
            handleScreen={this.props.handleScreen}
            isMobile={this.props.isMobile}
            mobilenumber={this.state.phone}
            cCode={this.state.cCode}
            updateScreen={this.props.updateScreen}
            ChangeScreen={this.ChangeScreen}
            iseditable={this.props.iseditable}
          />,
          this.state.screenData
        );
      });
  };

  render() {
    let { classes } = this.props;

    return (
      <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
        <div className="col-12 text-center Mlogin_header">
          {this.props.iseditable ? (
            <div className="p-0 text-left col-12">
              <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
            </div>
          ) : (
            <div className="p-0 text-left col-12">
              <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
            </div>
          )}

          {/* Header Module */}
          {/* <h6 className="py-3 m-0">Enter your mobile number</h6>  */}
          <div className="MCommonheader text-left px-2 py-3">
            <FormattedMessage tagName="p" id="Enter Your" />
            <FormattedMessage tagName="p" id="Number" />
          </div>

          {/* Mobile Number Input Module */}
          <div className="py-2 col-12 px-0 Minttellinput d-flex justify-content-center align-items-center">
            <PhoneInput
              inputProps={{
                name: "phone",
                required: true,
                autoFocus: true,
                placeholder: "8698997798",
                autoFormat: false,
              }}
              country={"us"}
              autoFormat={false}
              preferredCountries={["us"]}
              type="number"
              autoFocus={true}
              value={this.state.phone}
              placeholder={"8698997798"}
              onChange={this.handlephone}
            />
          </div>

          {/* Mobile Send Send Otp Button Module  */}
          <div className="MCommonbottomnextarrow">
            <ArrowNext disabled={this.state.phone.length < 7} onClick={this.handlemobilenumber} />
          </div>
        </div>

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserData: state.Main.UserData,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(Mobileinput));
