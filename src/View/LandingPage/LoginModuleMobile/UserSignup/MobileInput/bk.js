// Main React Components
import React, { Component } from "react";

// Scss
import "./Mobileinput.scss";

// Reusable Components
import PrimaryButton from "../../../../../Components/PrimaryButton/PrimaryButton";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

// Reactstrap Components
import { Row, Col } from "reactstrap";

// Material-UI Loader Components
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

// imported Components
import OTPVerification from "../OtpInput/OtpInput";

// import React Node-Module for Mobile-Number Components
import "react-intl-tel-input/dist/main.css";
import IntlTelInput from "react-intl-tel-input";

// Languges Components
import { FormattedMessage } from "react-intl";

// Redux Components
import { connect } from "react-redux";

// Action Components
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import {
  PhoneExistVerification,
  mobileVerification
} from "../../../../../controller/auth/verification";
import OtpSignIn from "../../../LoginModuleWeb/UserSignup/OtpSignIn/OtpSignIn";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
    color: "#e31b1b"
  }
});

let placeholder = "";
let numberstatus = "";
let globalnumber = "";

class Mobileinput extends Component {
  constructor(props) {
    // console.log("Mobile Screen Data------------->>>>>>>>>>>>>>>", props);
    super(props);
    this.state = {
      valuenumber: "",
      phone: "",
      screenData: {
        type: "loginWithMobile",
        data: {
          phoneNumber: "",
          type: "1",
          deviceId: "0909"
        }
      }
    };
    this.handlephone = this.handlephone.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  // Function for the Phone Number Flag
  handleflag = num => {
    this.setState({ cCode: num.dialCode });
  };

  // Function for the Phone Number Value
  handlephone = (status, valuenumber, number, phoneNumber) => {
    // Taking PlaceHolder With JS Module
    placeholder = document
      .getElementById("intelinputField")
      .getAttribute("placeholder");
    placeholder = placeholder.replace(/ /g, "");

    console.log("placeholder", valuenumber.length, placeholder.length);
    if (valuenumber.length <= placeholder.length) {
      valuenumber = valuenumber.substring(0, placeholder);
      this.setState({
        valuenumber: status,
        phone: valuenumber,
        cCode: number.dialCode
      });

      globalnumber = phoneNumber.replace(/ /g, "");
      numberstatus = status;

      let tmpState = this.state;
      tmpState["screenData"]["data"]["phoneNumber"] = globalnumber;
    } else {
    }
    this.props.dispatch(
      MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode)
    );
    this.props.dispatch(MOBILE_ACTION_FUNC("contactNumber", globalnumber));
  };

  // Function for the Notification (Snakbar)
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification (Snakbar)
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  goBack() {
    this.props.handleScreen(false);
  }

  handlemobilenumber = () => {
    if (globalnumber) {
      this.sendOtp();
    } else {
    }
  };

  // Function for Send OTP API
  sendOtp = () => {
    let mobileverfiyPayload = {
      phoneNumber: globalnumber
    };

    let mobilePayload = {
      phoneNumber: globalnumber,
      type: 1,
      deviceId: "0909"
    };

    PhoneExistVerification(mobileverfiyPayload)
      .then(data => {
        mobileVerification(mobilePayload).then(data => {
          this.setState({
            open: true,
            variant: "success",
            usermessage: "OTP Send Sucessfully.!!"
          });
          {
            this.handleScreen(
              <OTPVerification
                value={this.state.value}
                handleScreen={this.props.handleScreen}
                isMobile={this.props.isMobile}
                mobilenumber={this.state.phone}
                cCode={this.state.cCode}
                updateScreen={this.props.updateScreen}
              />,
              this.state.screenData
            );
          }
        });
      })
      .catch(err => {
        console.log("error");
      });
  };

  render() {
    const { classes } = this.props;

    // Mobile View Module
    if (this.props.isMobile) {
      return (
        <div className="my-3" style={{ width: "100vw" }}>
          {this.state.loader ? (
            <CircularProgress className={classes.progress} />
          ) : (
            <div className="col-12">
              {this.props.iseditable ? (
                <i
                  className="fa fa-arrow-left MCommonbackbutton"
                  onClick={this.props.onClose}
                />
              ) : (
                <i
                  className="fa fa-arrow-left MCommonbackbutton"
                  onClick={this.goBack}
                />
              )}
              <div className="MCommonheader my-3">
                <FormattedMessage tagName="p" id="message.mobilemessage1" />
                <FormattedMessage tagName="p" id="message.mobilemessage2" />
                <div className="my-3 Minttellinput">
                  <IntlTelInput
                    preferredCountries={["in"]}
                    containerClassName="intl-tel-input"
                    inputClassName="form-control"
                    onSelectFlag={this.handleflag}
                    onPhoneNumberChange={this.handlephone}
                    formatOnInit={true}
                    separateDialCode
                    fieldId="intelinputField"
                    autofocus
                    value={this.state.phone}
                  />
                </div>
                <div className="MCommonbottomnextarrow">
                  {this.props.iseditable ? (
                    <h1>demo</h1>
                  ) : (
                    <ArrowNext
                      disabled={!this.state.valuenumber}
                      onClick={this.handlemobilenumber}
                      // onClick={this.props.handleScreen.bind(
                      //   this,
                      // <OTPVerification
                      //   value={this.state.value}
                      //   handleScreen={this.props.handleScreen}
                      //   isMobile={this.props.isMobile}
                      //   mobilenumber={this.state.phone}
                      //   cCode={this.state.cCode}
                      //   updateScreen={this.props.updateScreen}
                      // />,
                      //   this.state.screenData
                      // )}
                    />
                  )}
                </div>
              </div>
            </div>
          )}
          {/* Snakbar Components */}
          <Snackbar
            type={this.state.variant}
            message={this.state.usermessage}
            open={this.state.open}
            onClose={this.handleClose}
          />
        </div>
      );
    }
    // Desktop View Module
    else {
      return <div />;
    }
  }
}

const mapStateToProps = function(state) {
  return {};
};

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(Mobileinput));
