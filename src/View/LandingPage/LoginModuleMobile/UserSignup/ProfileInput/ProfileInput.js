import React, { Component } from "react";
import "../ProfileInput/ProfileInput.scss";
import "../../../LoginModuleWeb/UserSignup/OtpInput/OtpInput.scss";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { __BlobUpload } from "../../../../../lib/cloudinary-image-upload";
import "react-html5-camera-photo/build/css/index.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import MobileNumber from "../../../TinderPage/Components/MobileNumber";
// import { UploadPosePhoto, checkIfPosePhotoUploaded } from "../../../../../controller/auth/verification";
import { getCookie } from "../../../../../lib/session";
import VideoInput from "../VideoInput/VideoInput";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";

const styles = () => ({
  btn: {
    color: "#fff",
    background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    marginTop: "50px",
    width: "95%",
    fontFamily: "Circular Air Book",
  },
  disablebtn: {
    color: "#fff",
    fontFamily: "Circular Air Book",
    backgroundColor: "#c3c3c3",
    fontWeight: "600",
    border: "none",
    padding: "10px 2rem",
    borderRadius: "5px",
    marginTop: "50px",
    width: "95%",
  },
  progress: {
    color: "#e31b1b",
    marginTop: "15vh",
    marginBottom: "15vh",
    textAlign: "center",
  },
  myBestPicture: {
    fontSize: "20px",
    fontFamily: "Circular Air Bold",
    color: "#808080",

  },
  myBestPictureCaption: {
    fontSize: "24px",
    textAlign: "left",
    fontFamily: "Circular Air Book",
    color: "#292929",
  },
  myProfilePicUploadCaption: {
    fontFamily: "Circular Air Book",
    color: "#484848",
    textAlign: "left"
  },
  progressV1: {
    color: "#e31b1b",
    marginTop: "15vh",
    marginBottom: "15vh",
    textAlign: "center",
  },
});

class ProfileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      // files: [],
      loader: false,
      index: 0,
      ShowVideoFormat: false,
      cropModal: false,
      urlToRender: "",
      imageCaptureView: false,
      realImage: "",
      doYouHaveCamera: false,
      isPosePhotoVerified: true,
      isUploading: false,
    };
    this.files = [];
    this.child = React.createRef();
  }

  componentDidMount() {
    if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
      navigator.mediaDevices
        .enumerateDevices()
        .then((res) => {
          console.log("res", res);
          let index = res.filter((k) => k.kind == "videoinput");
          console.log("index", index);
          if (index.length > 0) {
            this.setState({ doYouHaveCamera: true, imageCaptureView: true });
          }
        })
        .catch((err) => {});
    }
  }

  // UserProfile API Call Module
  profileapi = async () => {
    this.setState({ isUploading: true });
    let upload = await __BlobUpload(this.state.realImage);
    this.setState({ isUploading: false });
    this.props.dispatch(MOBILE_ACTION_FUNC("latitude", parseFloat(getCookie("lat"))));
    this.props.dispatch(MOBILE_ACTION_FUNC("longitude", parseFloat(getCookie("lng"))));
    this.props.dispatch(MOBILE_ACTION_FUNC("countryNameCode", this.props.UserData.countryNameCode));
    this.props.dispatch(MOBILE_ACTION_FUNC("profilePic", upload.body.secure_url));
    this.props.updateScreen(<VideoInput profilePic={upload.body.secure_url} updateScreen={this.props.updateScreen} {...this.props} />);
  };

  startImageCapture = () => {
    this.setState({ imageCaptureView: !this.state.imageCaptureView });
  };

  handleTakePhoto = (dataUri) => {
    // this.files[0] = dataUri;
    this.startImageCapture();
    if (dataUri) {
      this.setState({ realImage: dataUri, valid: true });
    }
  };

  handleTakePhotoAnimationDone = (dataUri) => {
    console.log("takePhoto");
  };

  handleCameraError = (error) => {
    console.log("handleCameraError", error);
  };

  handleCameraStart = (stream) => {
    console.log("handleCameraStart");
  };

  handleCameraStop = () => {
    console.log("handleCameraStop");
  };

  render() {
    // console.log("doYouHaveCamera", this.state.doYouHaveCamera);
    const { classes } = this.props;
    return (
      <div
        className={this.state.imageCaptureView && this.state.doYouHaveCamera ? "col-12" : "col-12 py-5"}
        style={
          this.state.imageCaptureView && this.state.doYouHaveCamera ? { height: "100vh", overflowX: "hidden", overflowY: "hidden" } : {height: "100vh", overflowX: "hidden", overflowY: "hidden" }
        }
      >
        <div className="row">
          {this.state.imageCaptureView && this.state.doYouHaveCamera ? (
            <section>
              <div className="m-camera-module h-100">
                <Camera
                  onTakePhoto={(dataUri) => {
                    this.handleTakePhoto(dataUri);
                  }}
                  onTakePhotoAnimationDone={(dataUri) => {
                    this.handleTakePhotoAnimationDone(dataUri);
                  }}
                  onCameraError={(error) => {
                    this.handleCameraError(error);
                  }}
                  idealFacingMode={FACING_MODES.ENVIRONMENT}
                  idealResolution={{ width: 768, height: 1366 }}
                  imageType={IMAGE_TYPES.JPG}
                  imageCompression={0.97}
                  isMaxResolution={true}
                  isImageMirror={false}
                  isSilentMode={false}
                  isDisplayStartCameraError={true}
                  isFullscreen={false}
                  sizeFactor={1}
                  onCameraStart={(stream) => {
                    this.handleCameraStart(stream);
                  }}
                  onCameraStop={() => {
                    this.handleCameraStop();
                  }}
                />
              </div>
            </section>
          ) : this.state.doYouHaveCamera ? (
            <div className="text-center p-sm-0 col-12 col-sm-10 col-md-12 col-lg-12 col-xl-12">
              <div className="pb-3">
                <h3 className={classes.myBestPictureCaption}>
                  <span className={classes.myBestPicture}>We want to see the UnFiltered You. No Filters, No Uploads, Just the Real You.</span>
                </h3>
                {/* <p className="modal_smallText">Adding pictures is a great way to show off your personality.</p> */}
              </div>
              {this.state.realImage != "" ? (
                this.state.doYouHaveCamera ? (
                  <div style={{ height: "0px" }} className="w-100" onClick={this.startImageCapture}>
                    <label className="col-auto w-100" htmlFor="ProfileFile">
                      <h5 className="Wchange_photo" style={{ width: "93%" }}>
                        Change Photo
                      </h5>
                    </label>
                  </div>
                ) : (
                  <div style={{ height: "0px" }} className="w-100" onClick={this.startImageCapture}>
                    <label className="col-auto w-100" htmlFor="ProfileFile">
                      <h5 className="Wchange_photo" style={{ width: "93%" }}>
                        Change Photo
                      </h5>
                    </label>
                  </div>
                )
              ) : (
                ""
              )}

              <div className="py-3 row Wuser_profile justify-content-center">
                <div className="Wuserimg_preview ">
                  {this.state.realImage != "" ? <img className="px-1" src={this.state.realImage} /> : ""}
                </div>
              </div>

              <div className="MCommonbottomnextarrow">
                <ArrowNext disabled={!this.state.valid} onClick={this.profileapi} />
              </div>

<div style={{
  margin: "13px",
  borderRadius: "50%",
  position: "fixed",
  left: "5px",
  fontFamily: "Circular Air Bold",
  color: "#808080",
  bottom: "25px"
}}>Change</div>
              {this.state.isUploading ? <div className="text-center pt-3">Please wait, we are updating your profile picture...</div> : ""}
            </div>
          ) : (
            <div className="text-center">
              <h3>Please Enable Webcam if you want to continue signup</h3>
              <button
                className={classes.btn}
                // onClick={() => this.props.updateScreen(<MobileNumber updateScreen={this.props.updateScreen} />)}
              >
                Start From Login
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    UserData: state.Main.UserData,
    afterCroppingUrl: state.UserProfile.urlAfterCropping,
    profileObject: state.profileObjectAfterCropped,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(ProfileInput));
