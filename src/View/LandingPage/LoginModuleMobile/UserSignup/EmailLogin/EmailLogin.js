import React, { Component } from "react";
import "./EmailLogin.scss";
import { connect } from "react-redux";
import "react-phone-input-2/lib/style.css";
import { Icons } from "../../../../../Components/Icons/Icons";
import Input from "../../../../../Components/Input/MatUI_Input";
import { EmailLogin } from "../../../../../controller/auth/verification";
import { getCookie, setCookie } from "../../../../../lib/session";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withRouter } from "react-router-dom";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

class Mobileinput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      variant: "success",
      usermessage: "",
      open: false,
      screenData: {
        type: "loginWithMobile",
        data: {
          phoneNumber: "",
          type: "1",
          deviceId: "0909",
        },
      },
    };
    this.goBack = this.goBack.bind(this);
  }

  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  goBack() {
    this.props.updateScreen(false);
  }

  onEmailListener = (e) => {
    this.setState({ email: e.target.value });
  };

  onPasswordListener = (e) => {
    this.setState({ password: e.target.value });
  };

  emailLogin = () => {
    if (this.state.email.length > 0 && this.state.password.length > 0) {
      let data = {
        emailId: this.state.email,
        password: this.state.password,
        pushToken: getCookie("fcmToken"),
        deviceId: "PC",
        deviceMake: "PC",
        deviceModel: "PC",
        deviceType: "PC",
        deviceOs: "PC",
        appVersion: "0",
      };
      EmailLogin(data)
        .then((res) => {
          console.log("res", res);
          if (res.status === 200) {
            setCookie("token", res.data.data.token);
            setCookie("uid", res.data.data._id);
            console.log("why not push");
            this.props.history.push("/app");
          } else {
            this.setState({
              open: true,
              variant: "error",
              usermessage: res.data.message || "Check your email and password and try again...",
            });
          }
        })
        .catch((err) => {
          console.log("err", err);
          this.setState({
            open: true,
            variant: "error",
            usermessage: err.message || "Something went wrong...",
          });
        });
    } else {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Email and Password cannot be empty...",
      });
    }
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.emailLogin();
  };

  render() {
    return (
      <div className="col-12 m_addFriendWOptions" style={{ height: "100vh" }}>
        <div className="row">
          <div className="col-12 py-3 m_addFriendWOptionsHeader">
            <div className="row">
              <div className="col-4" onClick={this.goBack}>
                <img src={Icons.PinkBack} alt="Back" height={20} width={25} />
              </div>
            </div>
          </div>
          <div className="m_addFriendScreen">
            <div className="col-12 m-label-common1">Login with</div>
            <div className="col-12 label-common2">Email & Password</div>
          </div>
          <section className="w-100">
            <form className="col-12 pt-4 addFriendInput w-100 d-flex flex-column" onSubmit={this.onSubmit}>
              <div className="m_custom_email-input">
                <Input
                  type="email"
                  required={true}
                  multiline={false}
                  label="Email"
                  placeholder="john@doe.com"
                  onChange={this.onEmailListener}
                />
              </div>
              <div className="m_custom_email-input">
                <Input
                  required={true}
                  id="standard-password-input"
                  label="Password"
                  type="password"
                  placeholder="******"
                  multiline={false}
                  onChange={this.onPasswordListener}
                />
              </div>
            </form>
          </section>
        </div>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          type={this.state.variant}
          message={this.state.usermessage}
          open={this.state.open}
          timeout={1500}
          onClose={this.handleClose}
        />
        <div className="MCommonbottomnextarrow">
          <ArrowNext onClick={this.emailLogin} disabled={this.state.password.length < 3} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {};
};

export default withRouter(connect(mapStateToProps, null)(Mobileinput));
