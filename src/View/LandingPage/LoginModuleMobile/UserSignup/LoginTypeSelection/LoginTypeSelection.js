// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./LoginType.scss";
import PrimaryButton from "../../../../../Components/PrimaryButton/PrimaryButton";
import MainDrawer from "../../../../../Components/Drawer/Drawer";
import Mobileinput from "../MobileInput/MobileInput";
import CMSPage from "../../CmsPages/CMSPage";
import Slider from "react-slick";
import FacebookLogin from "react-facebook-login";
import { localeset } from "../../../../../actions/locale";
import { Userfacebooklogin } from "../../../../../controller/auth/verification";
import { setCookie } from "../../../../../lib/session";
import { keys } from "../../../../../lib/keys";
import EmailLogin from "../../../LoginModuleMobile/UserSignup/EmailLogin/EmailLogin";
import OtpInput from "../OtpInput/OtpInput";
// import WelcomePage from "../WelcomePage/WelcomePage";
// import VideoInput from "../VideoInput/VideoInput";
// import ProfileInput from "../ProfileInput/ProfileInput";

class LoginTypeSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cmspage: false,
      selectedFooter: 1,
      latititudelocal: "",
      longitudelocal: "",
      pNo: "",
      cc: "",
      isLoggedIn: false,
    };
  }

  responseFacebook = (response) => {
    console.log("fb btn clicked", response);
    if (response.status === "unknown") {
      this.setState({
        firstName: "",
        fbId: "",
        pushToken: "",
        email: "",
        picture: "",
        height: "",
        isLoggedIn: false,
      });
      localStorage.setItem("facebookLogin", false);
      localStorage.setItem("firstName", "");
      localStorage.setItem("email", "");
      localStorage.setItem("picture", "");
      localStorage.setItem("height", "");
    } else {
      this.setState(
        {
          isLoggedIn: true,
          firstName: response.name,
          fbId: response.id,
          pushToken: response.accessToken,
          email: response.email,
          picture: response.picture.data.url,
          height: response.picture.data.height,
        },
        () => {
          let facebookpayload = {
            firstName: this.state.firstName,
            fbId: this.state.fbId,
            pushToken: this.state.pushToken,
            email: this.state.email,
            deviceId: "15645646564989",
            deviceMake: "Apple",
            deviceModel: "Md-LG",
            deviceType: "1",
          };
          localStorage.setItem("firstName", response.name);
          localStorage.setItem("email", response.email);
          localStorage.setItem("picture", response.picture.data.url);
          localStorage.setItem("height", response.picture.data.height);
          localStorage.setItem("facebookLogin", true);
          Userfacebooklogin(facebookpayload)
            .then((data) => {
              console.log("OP from facebook");
              setCookie("uid", data.data.data._id);
              setCookie("token", data.data.data.token);
            })
            .catch((err) => {
              console.log("facebook login failed", err);
            });
        }
      );
    }
  };

  // Function for the Change Languges
  handleLang = (selectedLang) => {
    this.props.dispatch(localeset(selectedLang));
  };

  opencmspageDrawer = () => {
    this.setState({ cmspage: true });
  };

  closecmspageDrawer = () => {
    this.setState({ cmspage: false });
  };

  openPrivacyPolicy = () => {
    window.open("https://unfiltered.love/privacy/", "_blank");
  };

  openTermsAndConditions = () => {
    window.open("https://unfiltered.love/terms/", "_blank");
  };

  render() {
    const cmspagedrawer = <CMSPage onClose={this.closecmspageDrawer} />;
    let fbContent;
    if (this.state.isLoggedIn) {
      setTimeout(() => {
        fbContent = this.props.history.push("/app");
      }, 1800);
    } else {
      fbContent = (
        <FacebookLogin
          cssClass="m_facebook_login"
          accessToken={keys.facebookToken}
          appId={keys.facebookAppId}
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
        />
      );
    }

    var settings = {
      dots: true,
      infinite: true,
      speed: 200,
      autoplay: true,
      // autoplaySpeed: 900,
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 0,
      beforeChange: (prevIndex, nextIndex) => {
        this.setState({
          selectedFooter: [0, 1, 2, 3].indexOf(nextIndex) !== -1 ? 1 : 2,
        });
      },
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <div>
        <video autoPlay={true} loop id="myVideo">
          <source src={require("../../../../../asset/ui_update/landingVideo.mp4")} type="video/mp4" />
        </video>
        <div className="MobileSlider">
          <MainDrawer width={400} onClose={this.closecmspageDrawer} onOpen={this.opencmspageDrawer} open={this.state.cmspage}>
            {cmspagedrawer}
          </MainDrawer>
          <div className="px-4 ">
            <Slider {...settings} className="Mslider"></Slider>
          </div>

          <div className="Mlogin text-center">
            {/* <div className="facebook_btn col-1 px-0">{fbContent}</div> */}
            <div className="Mfacebooklogin" onClick={() => this.responseFacebook()}>
              {fbContent}
            </div>
            <div className="Mmobilelogin">
              <PrimaryButton
                name="Log In With Phone Number"
                onClick={this.props.updateScreen.bind(
                  this,
                  //  <OtpInput
                  <Mobileinput
                    pNo={this.state.pNo}
                    handleScreen={this.props.handleScreen}
                    isMobile={this.props.isMobile}
                    updateScreen={this.props.updateScreen}
                  />
                )}
              />
            </div>
            <div className="MEmaillogin">
              <PrimaryButton
                name="Login with email"
                onClick={this.props.updateScreen.bind(
                  this,
                  <EmailLogin updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} />
                )}
              />
            </div>

            <p className="m-0 info-text">We don't post anything to Facebook.</p>
            <p className="m-0 info-text">
              By Signing up, you’re confirming that you’ve read and agree to our{" "}
              <u className="links" onClick={this.openTermsAndConditions}>
                Terms of Services
              </u>{" "}
              and{" "}
              <u className="links" onClick={this.openPrivacyPolicy}>
                Privacy Policy{" "}
              </u>
            </p>

            <div className="MSigninissue mx-3 my-3 text-center"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(LoginTypeSelection);
