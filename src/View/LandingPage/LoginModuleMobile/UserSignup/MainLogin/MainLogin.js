import React from "react";
import LoginTypeSelection from "../LoginTypeSelection/LoginTypeSelection";

const MainLogin = ({ currentScreen, isMobile, updateScreen, handleScreen }) => {
  return !currentScreen ? (
    <LoginTypeSelection handleScreen={handleScreen} isMobile={isMobile} updateScreen={updateScreen} />
  ) : (
    currentScreen
  );
};

export default MainLogin;
