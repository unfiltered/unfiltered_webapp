// Main React Components
import React, { Component } from "react";

// Scss
import "./EmailInput.scss";

// Reusable Components
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";

// Material-UI Components
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";

// imported Components
// import FirstName from "../FirstName/FirstnameInput";
import OtpInput from "../OtpInput/OtpInput";

// imported Validation Components
import { ValidateEmail } from "../../../../../Validation/Validation";

// Languges Components
import { FormattedMessage } from "react-intl";

// Redux Components
import { connect } from "react-redux";

// Redux Components
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { emailVerfication, UserProfileNewData } from "../../../../../controller/auth/verification";
import PasswordInput from "../PasswordInput/PasswordInput";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

class EmailInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueemail: "",
      errors: {},
      valid: false,
      emailValid: false,
      loader: true,
      open: false,
      screenData: {
        type: "verifyEmail",
        data: {
          email: "",
        },
      },
    };
    this.goBack = this.goBack.bind(this);
  }

  // componentDidMount() {
  //   setTimeout(() => {
  //     document.getElementById("activeinput").focus();
  //   }, 100);

  // Function for the Notification
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for the Email Value
  handleEmail = (event) => {
    ValidateEmail(event);
    event.target.value.length > 0 && !ValidateEmail(event)
      ? this.setState({
          emailErr: "Please enter valid Email Address.!!",
          emailValid: false,
        })
      : this.setState({ emailErr: "", valid: true, emailValid: true });

    let tmpState = this.state;
    tmpState["screenData"]["data"]["email"] = event.target.value || this.state.valueemail || this.props.stateEmail;
    this.setState({
      valueemail: event.target.value,
    });

    this.props.dispatch(MOBILE_ACTION_FUNC("email", this.state.valueemail || this.props.stateEmail));
  };

  // Function for the SignUP Button
  handlesignupemail = () => {
    this.verifyEmail();
  };

  verifyEmail = () => {
    let emailPayload = {
      email: this.state.valueemail,
    };

    emailVerfication(emailPayload)
      .then((data) => {
        if (data === undefined || data.status === 412) {
          this.props.updateScreen(
            <PasswordInput handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />,
            // <FirstName handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />,
            this.state.screenData
          );
        }
        console.log("[emailVerfication]", data);
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Entered email is already registered",
          emailValid: false,
        });
        return false;
      })
      .catch(() => {
        this.setState({ emailValid: true });
        this.props.updateScreen(
          <PasswordInput handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />,
          // <FirstName handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />,
          this.state.screenData
        );

        return true;
      });
  };

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 1000);
  }

  goBack() {
    this.props.updateScreen(
      <OtpInput updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />
    );
  }

  // API Call the Update the User Email-ID Details Module
  updateEmail = () => {
    let Useremailupdatedpayload = {
      email: this.state.valueemail,
    };

    UserProfileNewData(Useremailupdatedpayload)
      .then((data) => {
        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Data Update successfully.!!",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.!!",
        });
      });
    this.props.onClose();
  };

  render() {
    const { classes } = this.props;
    // Mobile View Module
    if (this.props.isMobile) {
      return (
        <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
          {this.state.loader ? (
            <div className="text-center">
              <CircularProgress className={classes.progress} />
            </div>
          ) : (
            <div className="col-12">
              {this.props.iseditable ? (
                <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
              ) : (
                <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
              )}
              <div className="MCommonheader py-3">
                <FormattedMessage tagName="p" id="message.emailmessage1" />
                <FormattedMessage tagName="p" id="message.emailmessage2" />
                <div className="my-3 MInputCommon">
                  <input
                    refs="email"
                    name="email"
                    type="text"
                    value={this.state.valueemail ? this.state.valueemail : this.props.stateEmail}
                    onChange={this.handleEmail}
                    size="30"
                    autoComplete="off"
                    id="activeinput"
                    placeholder="Enter your email address"
                  />
                  <div className="MNotificationmessage pt-2">
                    <FormattedMessage id="message.emailnotification" />
                  </div>
                  <span className="text-danger d-block">{this.state.emailErr}</span>
                </div>
                <div className="MCommonbottomnextarrow">
                  {this.props.iseditable ? (
                    <ArrowNext onClick={this.updateEmail} disabled={!this.state.valid} />
                  ) : (
                    <ArrowNext onClick={this.handlesignupemail} disabled={!this.state.valid} />
                  )}
                </div>
              </div>
            </div>
          )}
          {/* Snakbar Components */}
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    stateEmail: state && state.UserData && state.UserData.email,
  };
}

export default connect(mapStateToProps, null)(withStyles(styles)(EmailInput));
