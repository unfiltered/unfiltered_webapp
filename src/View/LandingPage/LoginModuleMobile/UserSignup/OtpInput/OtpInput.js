import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./OTPInput.scss";
import MainDrawer from "../../../../../Components/Drawer/Drawer";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core/styles";
import EmailInput from "../EmailInput/EmailInput";
import MobileInput from "../MobileInput/MobileInput";
import ResendOtp from "../OtpInput/ResendOtp";
import OTPInput from "otp-input-react";
// import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { otpVerification, mobileVerification } from "../../../../../controller/auth/verification";
import { keys } from "../../../../../lib/keys";
import { Icons } from "../../../../../Components/Icons/Icons";
import Loader from "../../../../../Components/Loader/Loader";

const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  btn: {
    width: "95%",
    backgroundColor: "#e31b1b",
    border: "1px solid #e31b1b",
    padding: "8px 0",
    color: "#fff",
    fontFamily: "Circular Air Book",
  },
  disablebtn: {
    width: "95%",
    backgroundColor: "#eef7fe",
    border: "1px solid #eef7fe",
    padding: "8px 0",
    fontFamily: "Circular Air Book",
  },
});

class _OTPInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: {},
      seconds: "",
      otp: "",
      loader: true,
      valid: false,
      resendvalue: "",
      Preferences: false,
      isButtonDisabled: false,
      open: false,
      screenData: {
        type: "loginWithMobile",
        data: {
          phoneNumber: "",
          type: "1",
          otp: "0909",
        },
      },
    };
    this.timer = 0;
    this.countDown = this.countDown.bind(this);
    this.handleclickOTP = this.handleclickOTP.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  // Resend OTP Drawer Open
  openProfileDrawer = () => {
    this.setState({ Preferences: true });
  };

  // Resend OTP Drawer Close
  closeProfileDrawer = () => {
    this.setState({ Preferences: false });
  };

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 1000);
  }

  secondsToTime(secs) {
    let divisor_for_minutes = secs % (60 * 60);
    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);
    let obj = {
      s: seconds,
    };
    return obj;
  }

  componentDidMount() {
    this.startTimer();
  }

  startTimer() {
    //set the seconds for timer
    this.setState({ seconds: 60 }, () => {
      let timeLeftVar = this.secondsToTime(this.state.seconds);
      this.setState({ time: timeLeftVar });

      if (this.timer === 0 && this.state.seconds >= 0) {
        this.timer = setInterval(this.countDown, 1000); // start the timer
      }
    });
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    // Check if we're at zero.
    if (seconds === 0) {
      clearInterval(this.timer);
      this.timer = 0;
    }
  }

  // Function for the Notification
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleclickOTP = (otp) => {
    this.setState({ otp: otp, valid: otp && otp.length == 6 ? true : false });
    if (this.state.otp.length === 6) {
      this.handleOTPChange();
    }
  };

  // After otp Enter it will automatically redirect to next Page
  handleOTPChange = () => {
    console.log("handleOTPChange");

    let otpPayload = {
      phoneNumber: this.props.mobilenumber,
      type: 1,
      otp: this.state.otp,
    };

    if (this.state.otp) {
      console.log("hererererer");
      otpVerification(otpPayload)
        .then((data) => {
          this.setState({
            open: true,
            variant: "success",
            usermessage: "OTP Verify Sucessfully.!!",
            valid: true,
          });
          if (data.data && data.data.data && data.data.data.isNewUser) {
            this.props.updateScreen(
              <EmailInput
                updateScreen={this.props.updateScreen}
                handleScreen={this.props.handleScreen}
                isMobile={this.props.isMobile}
                updateScreen={this.props.updateScreen}
                goBack={this.goBack}
              />
            );
          } else {
            this.props.history.push("/app");
          }
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Please enter valid OTP.!!",
            valid: false,
          });
        });
      this.props.dispatch(MOBILE_ACTION_FUNC("otp", this.state.otp));
    }
  };

  // Function for the Goback
  goBack() {
    this.props.updateScreen(
      <MobileInput updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />
    );
  }

  resendOTP = () => {
    let mobilePayload = {
      phoneNumber: this.props.mobilenumber,
      type: "1",
      deviceId: "0101",
    };
    if (mobilePayload) {
      mobileVerification(mobilePayload)
        .then((data) => {
          this.setState({
            open: true,
            variant: "success",
            usermessage: "Otp is resend to the number",
          });
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Enter Correct OTP",
          });
        });
    }
  };

  render() {
    const { classes } = this.props;

    const Preferencesdrawer = (
      <ResendOtp
        updateScreen={this.props.updateScreen}
        onClose={this.closeProfileDrawer}
        time={this.state.time}
        newtime={this.startTimer}
        mobileno={this.props.mobilenumber}
        cCode={this.props.cCode}
      />
    );

    return this.state.loader ? (
      <div style={{ width: "100vw", height: "100vh" }}>
        <Loader text={"Verifying your number"} />
      </div>
    ) : (
      <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
        <MainDrawer width={400} onClose={this.closeProfileDrawer} onOpen={this.openProfileDrawer} open={this.state.Preferences}>
          {Preferencesdrawer}
        </MainDrawer>
        <div className="col-12">
          <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
          <div className="text-center MOtp_main">
            {/* Header Module */}
            <h6 className="pt-3 m-0">Enter the code that was sent to</h6>
            <p className="m-0">{this.props.mobilenumber || this.props.userData.contactNumber}</p>

            {/* OTP input Module */}
            <div className="MVerify_otp d-flex justify-content-center">
              <OTPInput
                otpType="number"
                value={this.state.otp}
                autoFocus
                OTPLength={6}
                otpType="number"
                separator={<span />}
                onChange={(otp) => this.handleclickOTP(otp)}
              />
            </div>

            <div className="MCommonbottomnextarrow">
              <ArrowNext disabled={!this.state.valid} onClick={this.handleOTPChange} />
            </div>
            <div className="Motp_fail">
              <p className="py-3 m-0" onClick={this.resendOTP}>
                I didn't get a code
              </p>
              <p className="m-0 M_login_info">
                {keys.AppName} Dating uses OTP Login, a technology, to help you signin. You don't need a account. Message and data rates may
                apply.
              </p>
            </div>
          </div>

          {/* Snakbar Components */}
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userData: state.Main.UserData,
  };
}

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(_OTPInput)));
