// Main React Components
import React, { Component } from "react";

class ResendOTP extends Component {
  render() {
    return (
      <div className="my-3 MCommonwidth">
        <div className="col-12">
          <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />

          {/* Header Module */}
          <div className="MOtp_main">
            <h6 className="pt-3 m-0">If you haven't gotten a code :</h6>
          </div>

          <div className="Mresend_otp">
            {/* Resend Otp Module */}
            <div className="py-3 row">
              <div className="col-1 pr-0">
                <i className="fas fa-mobile-alt" />
              </div>
              <div className="col-10">
                <p className="m-0">Verify your mobile number</p>
                <p className="m-0">
                  {"+" + this.props.cCode + this.props.mobileno}, <span style={{ color: "#e31b1b" }}>Change Number</span>
                </p>
              </div>
            </div>

            {/* Check SMS Module */}
            <div className="py-2 row">
              <div className="col-1 pr-0">
                <i className="far fa-comment" />
              </div>
              <div className="col-10">
                <p className="m-0">Check your SMS inbox</p>
                <p className="m-0" style={{ color: "#e31b1b" }}>
                  Enter Code
                </p>
              </div>
            </div>

            {/* Resend SMS Code Module */}
            <div className="py-2 text-center">
              <button onClick={this.props.newtime}>Resend SMS Code (00:{this.props.time})</button>
            </div>

            {/* Facebook SMS Module */}
            <div className="py-2 row">
              <div className="col-1 pr-0">
                <i className="fab fa-facebook-square" />
              </div>
              <div className="col-10">
                <p className="m-0" style={{ color: "#e31b1b" }}>
                  Get Code on Facebook
                </p>
                <p className="m-0">it will arrivrs as a Facebook notification</p>
              </div>
            </div>

            {/* Get a Call Module */}
            <div className="py-2 row">
              <div className="col-1 pr-0">
                <i className="fas fa-phone-volume" />
              </div>
              <div className="col-10">
                <p className="m-0" style={{ color: "#e31b1b" }}>
                  Get a Call
                </p>
                <p className="m-0">Get your code in a voice call</p>
              </div>
            </div>

            {/* Get a Whatsapp Code Module */}
            <div className="py-2 row">
              <div className="col-1 pr-0">
                <i className="fab fa-whatsapp" />
              </div>
              <div className="col-10">
                <p className="m-0" style={{ color: "#e31b1b" }}>
                  Get a Whatsapp code
                </p>
                <p className="m-0">Get a message sent to WhatApp with your code.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ResendOTP;
