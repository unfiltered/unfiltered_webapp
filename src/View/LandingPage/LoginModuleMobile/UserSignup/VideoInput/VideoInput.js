// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";

// Scss
import "../ProfileInput/ProfileInput.scss";

// Reusable Components
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

// imported Components
import ProfileInput from "../ProfileInput/ProfileInput";
import WelcomePage from "../WelcomePage/WelcomePage";

// Video Components
import ReactPlayer from "react-player";

// Redux Components
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { profileVerification } from "../../../../../controller/auth/verification";
import { setCookie, getCookie } from "../../../../../lib/session";
import { uploadVideoToCloudinaryPromise } from "../../../../../lib/cloudinary-image-upload";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "15vh",
    marginBottom: "15vh",
    textAlign: "center",
  },
  progressV1: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  mSkip: {
    fontFamily: "Circular Air Bold",
    fontSize: "16px",
  },
  mAddNewView: {
    fontFamily: "Circular Air Book",
    fontSize: "16px",
    color: "#161616",
  },
  uploadingVideo: {
    fontFamily: "Circular Air Book",
    fontSize: "16px",
    color: "#161616",
    paddingLeft: "10px",
  },
});

class VideoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: false,
      loader: false,
      ShowVideoFormat: false,
      size: 46447850,
      videoFromUrl: "",
      videoUploadLoader: false,
    };
    this.files = [];
    this.goBack = this.goBack.bind(this);
  }

  profileapi = () => {
    this.setState({ loader: true });
    this.props.dispatch(MOBILE_ACTION_FUNC("latitude", getCookie("lat")));
    this.props.dispatch(MOBILE_ACTION_FUNC("longitude", getCookie("long")));
    Promise.all(this.props.UserData.profileVideo)
      .then((data) => {
        profileVerification(this.props.UserData).then((data) => {
          this.setState({ loader: false });
          setCookie("uid", data.data.data._id);
          setCookie("token", data.data.data.token);
          this.props.updateScreen(
            <WelcomePage handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />
          );
        });
      })
      .catch(function (err) {});
  };

  // Function for to Goback Screen
  goBack() {
    this.props.updateScreen(
      <ProfileInput updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />
    );
  }

  // Function for to Skip Video Scrren
  handleskipscreen = () => {
    this.props.dispatch(MOBILE_ACTION_FUNC("latitude", getCookie("lat")));
    this.props.dispatch(MOBILE_ACTION_FUNC("longitude", getCookie("long")));
    profileVerification(this.props.UserData).then((data) => {
      setCookie("token", data.data.data.token);
      setCookie("uid", data.data.data._id);
      return this.props.updateScreen(
        <WelcomePage handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />
      );
    });
  };

  cloudinaryVideoStateHandler = (file) => {
    this.setState({ videoUploadLoader: true });
    if (file[0].size <= this.state.size) {
      this.files[0] = file;
      this.setState({ valid: true });
      uploadVideoToCloudinaryPromise(this.files)
        .then((res) => {
          this.setState({ videoFromUrl: res.body.secure_url, videoUploadLoader: false });
          this.props.dispatch(MOBILE_ACTION_FUNC("profileVideo", res.body.secure_url));
        })
        .catch((err) => {});
    } else {
      this.setState({ usermessage: "Video size should be less than 50MB" });
    }
  };

  render() {
    const { classes } = this.props;

    // Mobile View Module
    if (this.props.isMobile) {
      return (
        <div style={{ width: "100vw", height: "100vh", overflow: "hidden" }}>
          {this.state.loader ? (
            <div className="text-center">
              <CircularProgress className={classes.progressV1} />
            </div>
          ) : (
            <div className="py-3" style={{ height: "100vh" }}>
              <div className="col-12">
                <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
                {/* used to skip a screen */}
                <div
                  onClick={this.handleskipscreen}
                  style={{
                    position: "absolute",
                    top: "0px",
                    right: "15px",
                    zIndex: 999,
                  }}
                >
                  <p className="m-0">
                    <b className={classes.mSkip}>Skip</b>
                  </p>
                </div>
              </div>
              <div className="col-12 MCommonheader py-3">
                <p>Take a short clip of an introduction or something you love!</p>

                {this.state.videoUploadLoader ? (
                  <div className="flex-column">
                    <div className="d-flex justify-content-center align-items-center">
                      <CircularProgress className={classes.progress} />
                      <span className={classes.uploadingVideo}>Please wait while video is uploading...</span>
                    </div>
                  </div>
                ) : (
                  <div className="row Muser_inputprofile justify-content-center">
                    <div className="Wuserimg_preview MUser_video">
                      {this.files
                        ? this.files.map((data, index) => (
                            <ReactPlayer key={index} url={this.state.videoFromUrl} playing={true} controls={true} />
                          ))
                        : ""}
                    </div>

                    {/* Video Upload Module */}
                    {this.files.length <= 0 ? (
                      <form encType="multipart/form-data">
                        <div className="ppppppp">
                          <label className="col-auto" htmlFor="VideoFile">
                            <div className="Waddprofile">
                              <i className="fas fa-plus" />
                              <span className={classes.mAddNewView}>Add your video</span>
                            </div>
                            <input
                              style={{ visibility: "hidden" }}
                              type="file"
                              id="VideoFile"
                              className="Wuser_input"
                              ref={(fileInputEl) => (this.fileInputEl = fileInputEl)}
                              onChange={() => this.cloudinaryVideoStateHandler(this.fileInputEl.files)}
                              accept="mp4,video/mp4,video/x-m4v,video/*"
                            />
                          </label>
                        </div>
                      </form>
                    ) : (
                      ""
                    )}
                  </div>
                )}
              </div>

              {/* Next Button Module */}
              <div className="MCommonbottomnextarrow">
                <ArrowNext disabled={!this.state.valid} onClick={this.profileapi} />
              </div>

              {/* Showing the Error For Invalid Photo Format */}
              <div className="WInvalid_filr">{this.state.ShowVideoFormat ? <p>Please choose the valid format video.</p> : ""}</div>
            </div>
          )}
        </div>
      );
    }

    // Desktop View Module
    else {
      return <div />;
    }
  }
}

function mapstateToProps(state) {
  return {
    UserData: state.Main.UserData,
  };
}

export default withRouter(connect(mapstateToProps, null)(withStyles(styles)(VideoInput)));
