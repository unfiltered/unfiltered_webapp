import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./WelcomePage.scss";
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";
import { withStyles } from "@material-ui/core/styles";
import Select from "react-select";
import { connect } from "react-redux";
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { getPreference, UserdataPrefence } from "../../../../../controller/auth/verification";
import "../../../../MobileHomePage/Components/UserProfile/UserProfile.scss";
import Snackbar from "../../../../../Components/Snackbar/Snackbar";
import { keys } from "../../../../../lib/keys";
import { Icons } from "../../../../../Components/Icons/Icons";

const styles = (theme) => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
  root: {
    width: "99%",
    backgroundColor: theme.palette.background.paper,
    paddingBottom: "100px",
  },
  rootRadio: {
    color: "#e31b1b",
    "&$checked": {
      color: "#e31b1b",
    },
    height: "25px",
    width: "40px",
    fontSize: "20px",
    float: "left",
    left: "0px !important",
  },
  label: {
    display: "unset",
    margin: "7px 0px",
  },
  labelRadio: {
    padding: "7px 0px",
    margin: "0px",
    display: "flex",
    fontSize: "16px",
  },
  relationShipStartsWith: {
    fontSize: "20px",
    color: "#585A60",
    fontFamily: "Circular Air Bold",
  },
  aCommunityOfPeople: {
    fontSize: "24px",
    color: "#292929",
    marginLeft: "0px",
    fontFamily: "Circular Air Bold",
  },
  joinUs: {
    fontSize: "24px",
    color: "#e31b1b",
    fontFamily: "Circular Air Bold",
  },
  notSelectedCircle: {
    padding: "2px",
    border: "2px solid #d1d1d1",
    borderRadius: "50%",
    height: "20px",
    width: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  innerCicrle: {
    padding: "2px",
    border: "2px solid #e31b1b",
    borderRadius: "50%",
    height: "20px",
    width: "20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  outerCircle: {
    borderRadius: "50%",
    background: "#e31b1b",
    width: "10px",
    height: "10px",
  },
});

// Globally Constant Variables
let currentPrefId = "";
let multiselectchecbox = [];

class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {},
      errors: {},
      valid: false,
      selectedScreenIndex: 0,
      selectedPrefIndex: 0,
      Preitem: "",
      value: false,
      selectedRadioButton: false,
      seemenamePayload: {},
      demodata: "",
      usermessage: "",
      variant: "",
      open: false,
    };
    this.inputRef = React.createRef();
    this.goBack = this.goBack.bind(this);
    this.radioChange = this.radioChange.bind(this);
  }

  // On Component Mount this Call's
  componentDidMount() {
    setTimeout(() => {
      let selectedPreferenceIndex = -1;
      if (selectedPreferenceIndex > -1) {
        this.setState({
          selectedScreenIndex: this.props.MobileSelectedindex,
          selectedPrefIndex: selectedPreferenceIndex,
          PrefernceData: this.props.PrefernceData,
        });
      }
    }, 100);
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // Function for Scrren Change onClick of Skip Button
  handleskipscreen = () => {
    this.props.history.push("/app/");
  };

  // Function for the OnChnage Input in UserPrefernces
  handleChange = (event, value) => {
    this.setState({ value: event.target.value });
  };

  // Function for the Radio Button
  radioChange = (status) => {
    if (status) {
      this.setState({
        value: status,
        selectedRadioButton: false,
      });
      // multiselectchecbox = []
    }
  };

  // Function for the prefer Not to say Button
  handlenotsayradioChange = (status) => {
    this.setState({ selectedRadioButton: true, value: [] });
    multiselectchecbox = [];
  };

  // Function for the User Input Value
  handleUser = (event, value) => {
    this.setState({ value: event.target.value, selectedRadioButton: false });
    multiselectchecbox = [];
  };

  // Function for the Checking Multiple Input (Hobbies) List
  handlemultiselect = (event, value) => {
    if (multiselectchecbox.includes(event)) {
      let index = multiselectchecbox.indexOf(event);
      multiselectchecbox.splice(index, 1);
    } else {
      multiselectchecbox.push(event);
    }
    this.setState({ selectedRadioButton: false, value: multiselectchecbox });
  };

  // Accroding the Map it render the Screen Dta
  getInput = (type, prefItem) => {
    const { classes } = this.props;

    // Transfering Map(Array) Data to Constant Value
    currentPrefId = prefItem.pref_id;

    // Checking Type from the API Data & Rendring the Components
    switch (type) {
      // For the InputType
      case 5:
        let InputText = (
          <div>
            <input
              ref={this.inputRef}
              name="name"
              type="text"
              size="30"
              autoFocus={true}
              value={this.state.valuel}
              onChange={this.handleUser}
              placeholder={prefItem.options ? prefItem.options[0] : prefItem.label}
              id={"activeinput-" + currentPrefId}
            />
            <div className="MNotificationmessage">
              <span>This is how it will appear in {keys.AppName}.</span>
            </div>
          </div>
        );
        return InputText;

      // For the RadioButton
      case 1:
        let RadioButton = (
          <div>
            {prefItem.options && prefItem.options.length > 0
              ? prefItem.options.map((k, i) =>
                  k === this.state.value ? (
                    <div className="d-flex pt-2 align-items-center" key={i} onClick={(e) => this.radioChange(k)}>
                      <div className={classes.innerCicrle} key={i}>
                        <div className={classes.outerCircle}></div>
                      </div>
                      <div className="ml-2 m_radioSelect">{k}</div>
                    </div>
                  ) : (
                    <div className="d-flex pt-2 align-items-center" key={i} onClick={(e) => this.radioChange(k)}>
                      <div className={classes.notSelectedCircle}></div>
                      <div className="ml-2 m_radioSelect">{k}</div>
                    </div>
                  )
                )
              : ""}
          </div>
        );
        return RadioButton;

      // For the Multi-SelectCheckbox
      case 2:
        let MultiSelectCheckbox =
          prefItem.options &&
          prefItem.options.map((k, i) => (
            <div
              key={i}
              className={`${multiselectchecbox.includes(k) ? "col-12 m_edit_selected py-2" : "col-12 m_edit_notSelected py-2"}`}
              onClick={() => this.handlemultiselect(k)}
            >
              <div className="row">
                <div className="col-10">{k}</div>
                <div className="col-2"> {multiselectchecbox.includes(k) ? <i className="m_ep_pinkCheck fas fa-check" /> : <span />}</div>
              </div>
            </div>
          ));
        return MultiSelectCheckbox;

      // For the Select DropDown
      case 10:
        let options = [];
        this.state.PrefernceData.myPreferences[0].data[0].options.map((data) =>
          options.push({
            label: data,
            value: data,
          })
        );

        let SelectDropDown = (
          <div className="WUser_heightsel">
            <Select onChange={this.handleUserSelects} options={options} placeholder={"Select your height"} />
          </div>
        );
        return SelectDropDown;
      default:
        console.log("none");
    }
  };

  checkForEmptyData = (obj) => {
    if (Array.isArray(obj)) {
      if (obj.length > 0) return true;
    } else if ((typeof obj === "string" || obj instanceof String) && obj !== "") {
      return true;
    } else if (this.state.selectedRadioButton) {
      return true;
    } else if (typeof obj === "boolean") {
    }

    return false;
  };

  // Function for the UserHeight Select
  handleUserSelects = (event) => {
    this.setState({ value: event.value, selectedRadioButton: false });
  };

  // Main User Prefernce Data that Comes form the API
  handlePrefernce = () => {
    if (!this.state.PrefernceData) {
      getPreference().then((data) => {
        console.log("welcome page #2", data);
        this.setState({ PrefernceData: data.data.data });
        this.props.dispatch(MOBILE_ACTION_FUNC("prefernces", data.data.Prefernce));
      });
    } else {
      let currentScreenLength = this.state.PrefernceData.myPreferences[this.state.selectedScreenIndex].data.length;

      if (
        this.state.selectedScreenIndex === this.state.PrefernceData.myPreferences.length - 1 &&
        this.state.selectedPrefIndex === currentScreenLength - 1
      ) {
        this.postPreferences();

        this.props.history.push("/app/");
      } else {
        this.postPreferences();

        if (this.state.selectedPrefIndex === currentScreenLength - 1) {
          this.setState({
            selectedScreenIndex: parseInt(this.state.selectedScreenIndex + 1),
            selectedPrefIndex: 0,
          });
          return;
        }

        this.setState({
          selectedPrefIndex: parseInt(this.state.selectedPrefIndex) + 1,
          value: "",
        });
      }
    }
  };

  // Function for Scrren Change onClick of Goback Button
  goBack() {
    let { selectedScreenIndex, selectedPrefIndex } = this.state;
    if (selectedScreenIndex === 0 && selectedPrefIndex === 0) {
      return;
    } else if (selectedScreenIndex > 0 && selectedPrefIndex === 0) {
      this.setState({
        selectedScreenIndex: selectedScreenIndex - 1,
        selectedPrefIndex: this.state.PrefernceData.myPreferences[selectedScreenIndex - 1].data.length - 1,
        value: "",
      });
    } else {
      this.setState({ selectedPrefIndex: selectedPrefIndex - 1, value: "" });
    }
  }

  checkArrayOfArrays = (a) => {
    return a.every(function (x) {
      return Array.isArray(x);
    });
  };

  // API Call Payload Data
  postPreferences = () => {
    let PrefUser = {
      pref_id: currentPrefId,
      values: this.state.value,
    };

    UserdataPrefence(PrefUser).then((data) => {
      this.setState({ value: "", selectedRadioButton: false });
      multiselectchecbox = [];
    });
  };

  handleNextStep = (e) => {
    let { PrefernceData, selectedScreenIndex, selectedPrefIndex } = this.state;
    if (this.state.value === "" || this.state.value === []) {
      if (PrefernceData.myPreferences[selectedScreenIndex].data[selectedPrefIndex].type === 5) {
        console.log("Input cannot be empty");
      } else if (PrefernceData.myPreferences[selectedScreenIndex].data[selectedPrefIndex].type === 1) {
        console.log("Select at least one option");
      } else if (PrefernceData.myPreferences[selectedScreenIndex].data[selectedPrefIndex].type === 2) {
        console.log("Select at least one option");
      }
    } else {
      e.preventDefault();
      this.handlePrefernce(e);
    }
  };

  render() {
    const { classes } = this.props;
    const { selectedScreenIndex } = this.state;

    // Mobile View Module
    if (this.props.isMobile) {
      return (
        <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
          <div className="col-12">
            {this.props.iseditable ? (
              <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
            ) : this.state.selectedScreenIndex === 0 && this.state.selectedPrefIndex === 0 ? (
              ""
            ) : (
              <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
            )}

            {!this.state.PrefernceData ? (
              <div className="MCommonheader py-3">
                {this.props.iseditable ? (
                  <div className="Mskipbtn" onClick={this.handlePrefernce}>
                    <p className="m-0">Skip</p>
                  </div>
                ) : (
                  ""
                )}
                <div style={{ height: "100vh" }}>
                  <div className="pt-5 text-center">
                    <img
                      src={Icons.Spinner}
                      alt="spinner"
                      style={{
                        position: "absolute",
                        height: "40vh",
                        top: "15vw",
                      }}
                    />
                    <img
                      style={{ height: "40vh" }}
                      src={require("../../../../../asset/mobile_img/signup_placeholder.png")}
                      className="img-fluid"
                      alt="Welcomeimg"
                      title="Welcomeimg"
                    />
                  </div>
                  <div className="col-12 pt-5 MWelcome">
                    <p className={classes.relationShipStartsWith}>Relationship start with</p>
                    <h3 className={classes.aCommunityOfPeople}>
                      A community of people who are ready for something real.
                      <span className={classes.joinUs}> Join us.</span>
                    </h3>
                  </div>
                </div>
                <div className="MCommonbottomnextarrow">
                  <ArrowNext onClick={this.handlePrefernce} />
                </div>
              </div>
            ) : (
              ""
            )}
            {/* mapping loop on the api data for rendering ui screens */}
            {this.state.PrefernceData && this.state.PrefernceData.myPreferences && this.state.PrefernceData.myPreferences
              ? this.state.PrefernceData.myPreferences.map((pref, index) =>
                  // check if the current selected screen is same as current index
                  selectedScreenIndex === index
                    ? pref.data.map((prefItem, key) =>
                        // check if the current selected screen is same as current index
                        this.state.selectedPrefIndex === key ? (
                          <div key={index} className="MCommonheader py-3">
                            <p>{prefItem.title}</p>
                            <p>{prefItem.label}</p>
                            {this.props.iseditable ? (
                              " "
                            ) : (
                              <div className="Mskipbtn" onClick={this.handleskipscreen}>
                                <p className="m-0">Skip</p>
                              </div>
                            )}
                            {/* Pefer not to say Button */}
                            <div className="Muser_notsay">
                              <input
                                type="radio"
                                id="Prefernottosay"
                                name="radio-group"
                                value="Prefer not to say"
                                checked={this.state.selectedRadioButton}
                                onChange={this.handlenotsayradioChange}
                              />
                              <label htmlFor="Prefernottosay">Prefer not to say</label>
                            </div>
                            <div className="py-3 MInputCommon">
                              {/* get the user input dynamically based on type */}
                              {this.getInput(prefItem.type, prefItem)}
                            </div>
                            <div className="MCommonbottomnextarrow">
                              {this.props.iseditable ? (
                                <ArrowNext
                                  onClick={(e) => this.handleNextStep(e)}
                                  className={this.state.value.length != 0 ? classes.btn : classes.disablebtn}
                                />
                              ) : (
                                <ArrowNext
                                  onClick={(e) => this.handleNextStep(e)}
                                  className={this.state.value.length != 0 ? classes.btn : classes.disablebtn}
                                />
                              )}
                            </div>
                          </div>
                        ) : (
                          ""
                        )
                      )
                    : ""
                )
              : ""}
          </div>
          <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
        </div>
      );
    }
    // Desktop View Module
    else {
      return <div />;
    }
  }
}

function mapStateToProps(state) {
  return {};
}

export default withRouter(connect(mapStateToProps, null)(withStyles(styles)(WelcomePage)));
