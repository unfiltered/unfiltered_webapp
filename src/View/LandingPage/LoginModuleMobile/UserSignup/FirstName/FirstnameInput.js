// Main React Components
import React, { Component } from "react";

// Scss
import "./FirstName.scss";

// Reusable Components
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// imported Components
import DobInput from "../DobInput/DobInput";
import EmailInput from "../EmailInput/EmailInput";

// imported Validation Components
import { ValidateName } from "../../../../../Validation/Validation";

// Languges Components
import { FormattedMessage } from "react-intl";

// Redux Components
import { connect } from "react-redux";

// Redux Components
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";

const styles = () => ({
  progress: {
    color: "#e31b1b",
    marginTop: "50vh",
    textAlign: "center",
  },
});

class FirstnameInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {},
      errors: {},
      valid: false,
    };
    this.goBack = this.goBack.bind(this);
  }

  // componentDidMount() {
  //   setTimeout(() => {
  //     document.getElementById("activeinput").focus();
  //   }, 100);

  //   var elem = document.getElementById("activeinput");
  //   if (typeof elem !== null && elem !== "undefined") {
  //     document.getElementById("activeinput").focus();
  //   }
  // }

  // Function for the Input Value
  handleName = (event) => {
    ValidateName(event);
    event.target.value.length > 0 && !ValidateName(event)
      ? this.setState({
          nameErr: "Please Enter valid Name.!!",
          nameValid: false,
        })
      : this.setState({ nameErr: "", valid: true, nameValid: true });
    this.props.dispatch(MOBILE_ACTION_FUNC("firstName", event.target.value));
  };

  goBack() {
    this.props.updateScreen(
      <EmailInput handleScreen={this.props.handleScreen} updateScreen={this.props.updateScreen} isMobile={this.props.isMobile} />
    );
  }

  render() {
    // Mobile View Module

    return (
      <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
        <div className="col-12">
          <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
          <div className="MCommonheader my-3">
            <FormattedMessage tagName="p" id="message.Namemessage1" />
            <FormattedMessage tagName="p" id="message.Namemessage2" />
            <div className="py-3 MInputCommon signup_firstname">
              <input
                refs="name"
                name="name"
                type="text"
                onChange={this.handleName}
                size="30"
                id="activeinput"
                placeholder="Enter your name"
              />
              <div className="MNotificationmessage pt-2">
                <FormattedMessage id="message.Namenotification" />
              </div>
              <span className="text-danger d-block">{this.state.nameErr}</span>
            </div>
            {/* <div className="MCommonbottomnextarrow">
              <div className={this.state.nameValid ? "Mprefer" : "Mprefer-disabled"}>
                <i
                  onClick={this.props.updateScreen.bind(
                    this,
                    <DobInput
                      handleScreen={this.props.handleScreen}
                      isMobile={this.props.isMobile}
                      updateScreen={this.props.updateScreen}
                    />
                  )}
                  className="fas fa-check"
                />
              </div>
            </div> */}
            <div className="MCommonbottomnextarrow">
              <ArrowNext
                onClick={this.props.updateScreen.bind(
                  this,
                  <DobInput handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} updateScreen={this.props.updateScreen} />
                )}
                disabled={!this.state.valid}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, null)(withStyles(styles)(FirstnameInput));
