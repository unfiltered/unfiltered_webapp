// Main React Componentsz
import React, { Component } from "react";

// Scss
import "./DobInput.scss";

// Reusable Components
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// imported Components
import GenderInput from "../Gender/GenderInput";
import FirstnameInput from "../FirstName/FirstnameInput";

// Date Picker Material UI Components
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";

// Moment Js Components
import moment from "moment";

// Languges Components
import { FormattedMessage } from "react-intl";

// Redux Components
import { connect } from "react-redux";

// Action Components
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";

const styles = (theme) => ({
  progress: {
    margin: theme.spacing.unit * 2,
    color: "#f80402",
  },
  grid: {
    width: "60%",
  },
  toolBar: {
    background: "#f80402",
  },
});

class DobInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: {},
      errors: {},
      valid: false,
      selectedDate: null,
      isDateSelected: false,
    };
    this.goBack = this.goBack.bind(this);
    this.handleDate = this.handleDate.bind(this);
  }

  // Function for the Date of Birth
  handleDate(date) {
    let isValid = this.checkPlus(moment(date).get("date"), moment(date).get("month"), moment(date).get("year"));
    this.setState({
      valid: isValid,
      isDateSelected: true,
    });
    let FianlDOB = moment(date).unix(); // Converting Dob into the Timestamp Secound
    this.setState({ selectedDate: date });

    let UserDOB = FianlDOB * 1000; // Converting Dob into the Timestamp Milisecound

    console.log("UserDOB", UserDOB, this.state.selectedDate);
    this.props.dispatch(MOBILE_ACTION_FUNC("dob", UserDOB));
  }

  // Checking User 18 Year old or not
  checkPlus = (day, month, year) => {
    return new Date(year + 18, month - 1, day) <= new Date();
  };

  handledob = (event) => {
    event ? this.setState({ valid: true }) : this.setState({ valid: false });
  };

  // Function to render Placeholder for the DOB
  renderLabel = (date) => {
    console.log("ahsdhaskd", date);
    if (moment(date).isValid()) {
      return moment(date).format("DD/MM/YYYY");
    } else {
      return "DD/MM/YYYY";
    }
  };

  goBack() {
    this.props.updateScreen(
      <FirstnameInput updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />
    );
  }

  render() {
    let { classes } = this.props;
    let { selectedDate } = this.state;
    return (
      <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
        <div className="col-12">
          <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
          <div className="py-3 MCommonheader">
            <FormattedMessage tagName="p" id="message.dobmessage1" />
            <FormattedMessage tagName="p" id="Birthday" />

            <div className="py-3 MInputCommon Mdob_calander">
              <MuiPickersUtilsProvider utils={DateFnsUtils} InputLabelProps={{ shrink: true }}>
                <i className="fas fa-calendar-week" />
                <DatePicker value={selectedDate} onChange={this.handleDate} labelFunc={this.renderLabel} />
              </MuiPickersUtilsProvider>

              <div className="MNotificationmessage">
                <FormattedMessage id="message.dobnotification" />
              </div>

              {this.state.isDateSelected && !this.state.valid ? (
                <span className="dob_error">You must be 18 years or above to create an account with us.!!</span>
              ) : (
                ""
              )}
            </div>

            <div className="MCommonbottomnextarrow">
              <ArrowNext
                disabled={!this.state.valid}
                onClick={this.props.updateScreen.bind(
                  this,
                  <GenderInput
                    handleScreen={this.props.handleScreen}
                    isMobile={this.props.isMobile}
                    updateScreen={this.props.updateScreen}
                  />
                )}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {};
};

export default connect(mapStateToProps, null)(withStyles(styles)(DobInput));
