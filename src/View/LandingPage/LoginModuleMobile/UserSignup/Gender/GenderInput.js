// Main React Components
import React, { Component } from "react";

// Scss
import "./GenderInput.scss";

// Reusable Components
import ArrowNext from "../../../../../Components/MobileNextButton/NextButton";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

// imported Components
import ProfileInput from "../ProfileInput/ProfileInput";
import DobInput from "../DobInput/DobInput";

// Languges Components
import { FormattedMessage } from "react-intl";

// Redux Components
import { connect } from "react-redux";

// Action Components
import { MOBILE_ACTION_FUNC } from "../../../../../actions/User";
import { UserProfileNewData } from "../../../../../controller/auth/verification";

const styles = (theme) => ({
  progress: {
    margin: theme.spacing.unit * 2,
    color: "#e31b1b",
  },
});

class GenderInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: "",
      UserFinalData: "",
    };
    this.radioChange = this.radioChange.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  // Radio Button Check or not
  radioChange(status) {
    this.setState(
      {
        selectedOption: status.currentTarget.value,
        value: status,
      },
      () => {
        this.props.dispatch(MOBILE_ACTION_FUNC("gender", this.state.selectedOption));
      }
    );
  }

  // Goback Button
  goBack() {
    this.props.updateScreen(
      <DobInput updateScreen={this.props.updateScreen} handleScreen={this.props.handleScreen} isMobile={this.props.isMobile} />
    );
  }

  // API Call the Update the User Gender Details Module
  updateGender = () => {
    let Userprofileupdatedpayload = {
      gender: this.state.selectedOption,
    };

    UserProfileNewData(Userprofileupdatedpayload)
      .then((data) => {
        this.props.getuserprofile();

        this.setState({
          open: true,
          variant: "success",
          usermessage: "User Data Update successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some Error Occured.",
        });
      });
    this.props.onClose();
  };

  // Function for the Show & Change Gender
  static getDerivedStateFromProps(nextProps, prevState) {
    setTimeout(() => {
      if (!prevState.selectedOption) {
        if (nextProps.UserFinalData && nextProps.UserFinalData.gender) {
          let radioCheck = nextProps.UserFinalData.gender === "Male" ? document.getElementById("Male") : document.getElementById("Female");
          radioCheck.checked = true;
        }
        return {
          // selectedOption: nextProps.UserFinalData.gender
        };
      }
    }, 300);
    return {};
  }

  render() {
    return (
      <div className="py-3" style={{ width: "100vw", height: "100vh" }}>
        <div className="col-12">
          {this.props.iseditable ? (
            <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.props.onClose} />
          ) : (
            <i className="fa fa-arrow-left MCommonbackbutton" onClick={this.goBack} />
          )}
          <div className="MCommonheader py-3">
            <FormattedMessage tagName="p" id="message.gendermessage1" />
            <FormattedMessage tagName="p" id="message.gendermessage2" />
            <div className="py-3 px-0 col-12 MInputgender">
              {this.props.iseditable ? (
                <div className="row">
                  <div className="col-4 Wsignupradiochecked_btn">
                    <input
                      type="radio"
                      id="Male"
                      value="Male"
                      name="radio-group"
                      checked={this.state.selectedOption === "Male"}
                      onChange={this.radioChange}
                    />
                    <label htmlFor="Male">Male</label>
                  </div>
                  <div className="col-4 Wsignupradiochecked_btn">
                    <input
                      type="radio"
                      id="Female"
                      value="Female"
                      name="radio-group"
                      checked={this.state.selectedOption === "Female"}
                      onChange={this.radioChange}
                    />
                    <label htmlFor="Female">Female</label>
                  </div>
                </div>
              ) : (
                <div className="row">
                  <div className="col-4 Wsignupradiochecked_btn">
                    <input
                      type="radio"
                      id="Male"
                      value="1"
                      name="radio-group"
                      checked={this.state.selectedOption === "1"}
                      onChange={this.radioChange}
                    />
                    <label htmlFor="Male">Male</label>
                  </div>
                  <div className="col-4 Wsignupradiochecked_btn">
                    <input
                      type="radio"
                      id="Female"
                      value="2"
                      name="radio-group"
                      checked={this.state.selectedOption === "2"}
                      onChange={this.radioChange}
                    />
                    <label htmlFor="Female">Female</label>
                  </div>
                </div>
              )}
            </div>
            <div className="MCommonbottomnextarrow">
              {this.props.iseditable ? (
                <ArrowNext onClick={this.updateGender} disabled={!this.state.value} />
              ) : (
                <ArrowNext
                  disabled={!this.state.value}
                  onClick={this.props.updateScreen.bind(
                    this,
                    <ProfileInput
                      handleScreen={this.props.handleScreen}
                      isMobile={this.props.isMobile}
                      updateScreen={this.props.updateScreen}
                    />
                  )}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    UserData: state.Main.UserData,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(GenderInput));
