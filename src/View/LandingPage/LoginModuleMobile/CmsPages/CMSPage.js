// Main React Components
import React, { Component } from "react";

// Material-UI Loader Components
import CircularProgress from "@material-ui/core/CircularProgress";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";

const styles = {
  progress: {
    color: "#e31b1b",
    marginTop: "45vh",
    textAlign: "center"
  }
};

class CMSPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true
    };
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 50000);
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <div className="pt-3 px-3 MCommonwidth">
          <i
            className="fa fa-arrow-left"
            onClick={this.props.onClose}
            style={{
              fontSize: "24px",
              color: "#F74167"
            }}
          />
          {this.state.loader ? (
            <div className="text-center">
              <CircularProgress className={classes.progress} />
            </div>
          ) : (
            <div className="py-3 MCommonheader">
              <p className="m-0">Community</p>
              <p className="m-0">Guidelines</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CMSPage);
