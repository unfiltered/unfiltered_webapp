import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import { otpVerification, mobileVerification } from "../../../../controller/auth/verification";
import { setCookie } from "../../../../lib/session";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import MobileNumber from "./MobileNumber";
import OTPInput from "otp-input-react";
import { Icons } from "../../../../Components/Icons/Icons";

class OtpVerify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterotp: "",
      open: false,
      variant: "warning",
      usermessage: "",
      isVisible: true,
      currentCount: 59,
    };
  }

  componentDidMount() {
    var intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId: intervalId });
    // console.log('direct login')
  }
  tryAgain = () => {
    var intervalId = setInterval(this.timer, 1000);
    this.setState({ intervalId: intervalId, currentCount: 59 });
  };
  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }
  timer = () => {
    var newCount = this.state.currentCount - 1;
    if (newCount >= 0) {
      this.setState({ currentCount: newCount });
    } else {
      clearInterval(this.state.intervalId);
    }
  };

  // Function for the OTP Input && Automatically redirect to the next Page
  handleclickOTP = (otp) => {
    this.setState({
      enterotp: otp,
      valid: otp && otp.length == 6 ? true : false,
    });
  };

  HandleNavigate = (e) => {
    if (e.key === "Enter" || e.charCode === 13) {
      let otppayload = {
        // email: this.props.globalnumber, dev
        phoneNumber: this.props.globalnumber,
        type: "1",
        otp: this.state.enterotp,
      };

      otpVerification(otppayload)
        .then((data) => {
          setCookie("token", data.data.data.token);
          setCookie("ref", data.data.data.refreshTokens);
          this.props.history.push("/app/");
        })
        .catch((err) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Enter Correct OTP",
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 700);
        });
    } else {
      let otppayload = {
        phoneNumber: this.props.globalnumber,
        type: "1",
        otp: this.state.enterotp,
      };

      otpVerification(otppayload)
        .then((data) => {
          setCookie("token", data.data.data.token);
          setCookie("ref", data.data.data.refreshTokens);
          this.props.history.push("/app/");
        })
        .catch((err) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Enter Correct OTP",
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 700);
        });
    }
  };

  ResendOtp = () => {
    let mobilePayload = {
      phoneNumber: this.props.globalnumber,
      // email: globalnumber,
      type: "1",
      deviceId: "0101",
    };
    if (mobilePayload) {
      mobileVerification(mobilePayload)
        .then((data) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Otp is resend to the number",
            isVisible: false,
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 1300);
          setTimeout(() => {
            this.setState({ isVisible: true });
          }, 60000);
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "warning",
            usermessage: "Enter Correct OTP",
          });
          setTimeout(() => {
            this.setState({ open: false });
          }, 1300);
        });
    }
  };

  HandleNavigateToLoginScreen = () => {
    this.props.updateScreen(<MobileNumber updateScreen={this.props.updateScreen} />);
    this.props.toggleModal();
  };


  openPolicy = () => {
    const url = "https://appconfig.unfiltered.love/policy.html";
    window.open(url, "_blank");
  };

  openTermsAndPolicy = () => {
    const url = "https://appconfig.unfiltered.love/TermsAndConditions.html";
    window.open(url, "_blank");
  };

  render() {
    return (
      <div className="OtpModal">
        <div className="modal_close_btn" onClick={() => this.HandleNavigateToLoginScreen()}>
          <img src={Icons.closeBtn} alt="close-btn" style={{ width: "18px", height: "18px" }} />
        </div>
        <div className="text-center p-4">
          <h4 className="VerifyNumber_Header">Verify Your Mobile Number</h4>
          <div>
            <p className="VerifyNumber_EnterOTP_Text">
              {/* Enter the Code Sent To {this.props.globalnumber} <a href="">RESEND</a>{" "} */}
              Please Enter the Code Sent To <span>{this.props.globalnumber}</span>
            </p>
          </div>
          <div className="d-flex justify-content-center my-3">
            <div className="verify_main">
              <OTPInput
                value={this.state.enterotp}
                onChange={(otp) => this.handleclickOTP(otp)}
                autoFocus
                OTPLength={6}
                otpType="number"
                disabled={false}
                // secure
              />
              {/* <OtpInput
                onChange={(otp) => this.handleclickOTP(otp)}
                onKeyPress={(e) => this.HandleNavigate(e)}
                numInputs={6}
                value={this.state.enterotp}
                separator={<span> - </span>}
                defaultValue="will focus"
                shouldAutoFocus={true}
              /> */}
            </div>
          </div>
          <div>
            <p className="text-right px-4 ml-3 mr-3" onClick={() => this.ResendOtp()}>
              <div className="VerifyNumber_Resend">
                {this.state.currentCount === 0 ? <div onClick={this.tryAgain}>Resend</div> : <div>00:{this.state.currentCount}</div>}
              </div>
            </p>
          </div>
          <div className="my-3">
            <Button
              size="lg VerifyNumber_Continue"
              id="Continue"
              onClick={(e) => this.HandleNavigate(e)}
              disabled={!this.state.valid}
              // disabled={this.state.enterotp.length >= 6 ? true : false}
            >
              Continue
            </Button>
          </div>
          <div className="MobNumContent">
            <p>
              By Sign In, You’re Confirming that You’ve read and agree to our{" "}
              <span onClick={this.openTermsAndPolicy} style={{ textDecoration: "underline", cursor: "pointer" }}>
                Terms and Conditions.
              </span>
              Learn how we process your data in our{" "}
              <span onClick={this.openPolicy} style={{ textDecoration: "underline", cursor: "pointer" }}>
                Privacy Policy
              </span>{" "}
              and{" "}
              <Link to="/cookie-policy" target="_blank">
                Cookie Policy.
              </Link>
            </p>
          </div>
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

export default withRouter(OtpVerify);
