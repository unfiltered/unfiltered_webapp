import React, { Component } from "react";
import { Link } from "react-router-dom";
import FacebookLogin from "react-facebook-login";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Form, FormGroup, Col } from "reactstrap";
import OtpVerify from "./OtpVerify";
import { MOBILE_ACTION_FUNC } from "../../../../actions/User";
import Snackbar from "../../../../Components/Snackbar/Snackbar";
import { mobileVerification, PhoneExistVerification, Userfacebooklogin, EmailLogin } from "../../../../controller/auth/verification";
import SignUp from "../../LoginModuleWeb/UserSignup/SignUp/SignUp";
import { getCookie, setCookie } from "../../../../lib/session";
import { keys } from "../../../../lib/keys";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import "../../LoginModuleWeb/UserSignup/SignUp/SignUp.scss";
import { Icons } from "../../../../Components/Icons/Icons";

let globalnumber = "";

class MobileNumber extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cCode: "",
      mobileNum: "",
      variant: "error",
      open: false,
      isNumberPerfect: false,
      formattedValue: "",
      loginWithEmail: false,
      email: "",
      password: "",
    };
  }

  // on click of this button -> it will navigate to next screen
  HandleOtpVerification = (e) => {
    e.preventDefault();
    if (this.state.loginWithEmail) {
      let data = {
        emailId: this.state.email,
        password: this.state.password,
        pushToken: getCookie("fcmToken"),
        deviceId: "PC",
        deviceMake: "PC",
        deviceModel: "PC",
        deviceType: "PC",
        deviceOs: "PC",
        appVersion: "0",
      };
      EmailLogin(data)
        .then((res) => {
          console.log("res", res);
          if (res.status === 200) {
            setCookie("token", res.data.data.token);
            setCookie("uid", res.data.data._id);
            console.log("why not push");
            this.props.history.push("/app");
          } else {
            this.setState({
              open: true,
              variant: "error",
              usermessage: res.data.message || "Check your email and password and try again...",
            });
          }
        })
        .catch((err) => console.log("err", err));
    } else {
      if (this.state.mobileNum) {
        let mobilePayload = {
          phoneNumber: this.state.mobileNum.trim(),
          // email: globalnumber,
          type: "1",
          deviceId: "0101",
        };
        let mobileverfiyPayload = {
          phoneNumber: this.state.mobileNum.trim(),
        };

        PhoneExistVerification(mobileverfiyPayload)
          .then((data) => {
            if (data.status === 200) {
              this.props.dispatch(MOBILE_ACTION_FUNC("countryCode", "+" + this.state.cCode));
              mobileVerification(mobilePayload).then((data) => {
                console.log("i am here", data);
                this.props.updateScreen(
                  <OtpVerify
                    mobileNum={this.state.mobileNum}
                    toggleModal={this.props.toggleModal}
                    updateScreen={this.props.updateScreen}
                    globalnumber={this.state.mobileNum}
                  />
                );
              });
            } else {
              this.setState(
                {
                  open: true,
                  variant: "error",
                  usermessage: "No Phone Number Found",
                },
                () => {
                  setTimeout(() => {
                    this.setState({ open: false });
                  }, 1500);
                }
              );
            }
          })
          .catch((err) => {
            this.setState(
              {
                open: true,
                variant: "error",
                usermessage: "No Phone Number Found, Please Sign Up.",
              },
              () => {
                setTimeout(() => {
                  this.setState({ open: false });
                }, 1500);
              }
            );
          });
      } else {
        this.setState(
          {
            open: true,
            variant: "error",
            usermessage: "Please Enter Correct Number",
          },
          () => {
            setTimeout(() => {
              this.setState({ open: false });
            }, 1500);
          }
        );
      }
    }
  };

  // Used to Chnage SignUP Screen
  handleSugnUpScreen = () => {
    this.props.updateScreen(<SignUp updateScreen={this.props.updateScreen} toggleModal={this.props.toggleModal} />);
  };

  responseFacebook = (response) => {
    if (response.status === "unknown") {
      this.setState({
        firstName: "",
        fbId: "",
        pushToken: "",
        email: "",
        picture: "",
        height: "",
        isLoggedIn: false,
      });
      localStorage.setItem("facebookLogin", false);
      localStorage.setItem("firstName", "");
      localStorage.setItem("email", "");
      localStorage.setItem("picture", "");
      localStorage.setItem("height", "");
    } else {
      this.setState(
        {
          isLoggedIn: true,
          firstName: response.name,
          fbId: response.id,
          pushToken: response.accessToken,
          email: response.email,
          picture: response.picture.data.url,
          height: response.picture.data.height,
        },
        () => {
          let facebookpayload = {
            firstName: this.state.firstName,
            fbId: this.state.fbId,
            pushToken: this.state.pushToken,
            email: this.state.email,
            deviceId: "15645646564989",
            deviceMake: "Apple",
            deviceModel: "Md-LG",
            deviceType: "1",
          };
          localStorage.setItem("firstName", response.name);
          localStorage.setItem("email", response.email);
          localStorage.setItem("picture", response.picture.data.url);
          localStorage.setItem("height", response.picture.data.height);
          localStorage.setItem("facebookLogin", true);
          Userfacebooklogin(facebookpayload)
            .then((data) => {
              setCookie("uid", data.data.data._id);
              setCookie("token", data.data.data.token);
            })
            .catch((err) => {
              console.log("facebook login failed", err);
            });
        }
      );
    }
  };

  openPolicy = () => {
    const url = "https://appconfig.unfiltered.love/policy.html";
    window.open(url, "_blank");
  };

  openTermsAndPolicy = () => {
    const url = "https://appconfig.unfiltered.love/TermsAndConditions.html";
    window.open(url, "_blank");
  };

  openCommunityGuidelines = () => {
    const url = "https://appconfig.unfiltered.love/CommunityGuidelines.html";
    window.open(url, "_blank");
  };

  handlePhoneNumberChange = (value, country, e, formattedValue) => {
    console.log("value", value, country, e, formattedValue);
    this.setState({ mobileNum: value, cCode: country.dialCode });
  };

  handleEmailLogin = () => {
    this.setState({ loginWithEmail: !this.state.loginWithEmail });
  };

  handleEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  handlePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  render() {
    let { toggleModal } = this.props;
    let fbContent;
    if (this.state.isLoggedIn) {
      setTimeout(() => {
        fbContent = this.props.history.push("/app/");
      }, 1800);
    } else {
      fbContent = (
        <FacebookLogin
          cssClass="facebook_btn"
          accessToken={keys.facebookToken}
          // containerStyle={{ width: "350px", height: "38px" }}
          // buttonStyle={{ width: "350px", height: "38px" }}
          appId={keys.facebookAppId}
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
        />
      );
    }
    return (
      <div className="MobileNumberModal" onKeyDown={(event) => (event.key === "Escape" ? toggleModal() : null)} onClose={() => toggleModal()}>
        <div className="modal_close_btn" onClick={() => toggleModal()}>
          <img src={require("../../../../asset/images/close.png")} alt="close-btn" style={{ width: "18px", height: "18px" }} />
        </div>
        <div className="text-center p-4">
          <div className="py-3 d-flex justify-content-center align-items-center">
            <div style={{ color: "#292929", fontWeight: "100", fontSize: "26px", fontFamily: "Product Sans" }}> Sign In To</div>
            <div>
              <img src={Icons.cmeLogo} alt="app-logo" width={162} className="ml-1 mb-1" />
            </div>
          </div>
          <div>
            {this.state.loginWithEmail ? (
              <section>
                <div className="mr-0 pb-2">
                  <div className="emailTitle pb-1">
                    <p className="text-left mb-0 signup_label">Email*</p>
                  </div>
                  <div>
                    <input type="text" size="30" className="emailForm" placeholder={"john@doe.com"} onChange={this.handleEmail} />
                    <p className="signup_error">{this.state.emailErr}</p>
                  </div>
                </div>
                <div className="mr-0 pb-2">
                  <div className="emailTitle pb-1">
                    <p className="text-left mb-0 signup_label">Password*</p>
                  </div>
                  <div>
                    <input type="password" size="30" placeholder={"******"} className="emailForm" onChange={this.handlePassword} />
                    <p className="signup_error">{this.state.emailErr}</p>
                  </div>
                </div>
              </section>
            ) : (
              <form className="MobNumInput" onSubmit={this.HandleOtpVerification}>
                {/* <Form > */}
                <div className="MobileLoginScreen_MobileNumberText">Please Enter Your Mobile Number:</div>
                {/* <FormGroup> */}
                <div className="d_loginMobileNumberInput">
                  <PhoneInput
                    inputProps={{
                      name: "phone",
                      required: true,
                      autoFocus: true,
                      placeholder: "8698997798",
                      autoFormat: false,
                    }}
                    country={"us"}
                    autoFormat={false}
                    preferredCountries={["us"]}
                    type="number"
                    autoFocus={true}
                    value={this.state.mobileNum}
                    placeholder={"8698997798"}
                    onChange={this.handlePhoneNumberChange}
                  />
                </div>
              </form>
            )}
          </div>
          <div className="my-3">
            <Button size="lg MobNumBtn" onClick={this.HandleOtpVerification}>
              Continue
            </Button>
          </div>

          <div className="mb-3 d-flex justify-content-center" onClick={() => this.responseFacebook}>
            {/* <Button size="lg FaceBookBtn" onClick={this.responseFacebook}> */}
            {fbContent}
            {/* </Button> */}
          </div>

          <div className="my-3">
            <Button size="lg EmailLoginBtn" onClick={this.handleEmailLogin}>
              {!this.state.loginWithEmail ? "Signin with email" : "Signin with phone number"}
            </Button>
          </div>

          <div className="MobNumContent">
            <p>
              By Sign In, You’re Confirming that You’ve read and agree to our
              <span onClick={this.openTermsAndPolicy} style={{ textDecoration: "underline", cursor: "pointer" }}>
                Terms and Conditions.
              </span>
              Learn how we process your data in our
              <span onClick={this.openPolicy} style={{ textDecoration: "underline", cursor: "pointer", paddingLeft: "5px" }}>
                Privacy Policy
              </span>
              and{" "}
              <span onClick={this.openCommunityGuidelines} style={{ textDecoration: "underline", cursor: "pointer", paddingLeft: "5px" }}>
                Community Guidelines
              </span>
            </p>
          </div>
          {/* SignUp Modula */}
          <div className="alreadyMemeber">
            <span>Not a Member?</span>
            <Link>
              <span onClick={this.handleSugnUpScreen} style={{ fontWeight: "600" }}>
                {" "}
                Sign Up?
              </span>
            </Link>
          </div>
        </div>
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />
      </div>
    );
  }
}

function mapstateToProps(state) {
  return {
    // mobileNumber: state.userData.conta
  };
}

export default connect(mapstateToProps, null)(withRouter(MobileNumber));
