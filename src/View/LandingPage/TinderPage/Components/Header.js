import React, { Component } from "react";
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, Button } from "reactstrap";
import "./Index.scss";
import MainModal from "../../../../Components/Model/model";
import MobileNumber from "./MobileNumber";
import { connect } from "react-redux";
import IOSIcon from "../../../../asset/CME/png/iosicon.png";
import AndroidIcon from "../../../../asset/CME/png/androidicon.png";
import { keys } from "../../../../lib/keys";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      modal: false,
    };
    this.toggle = this.toggle.bind(this);
    this.toggleLoginModal = this.toggleLoginModal.bind(this);
  }

  // toggle Hamburger menu
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  // toggle login modal
  toggleModal = (event) => {
    // if(event !== undefined && event.key === 'Escape') {

    // }
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  };

  // close login modal on escape button

  // toggle login modal
  toggleLoginModal() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  toggleSignUpModal = () => {
    this.setState((prevState) => ({
      modal: prevState.modal,
    }));
  };

  // the screen sent as argument to this function will be rendered, or make it false to render default screen
  updateScreen = (screen) => this.setState({ currentScreen: screen });

  HandleMobileNumber = () => {
    this.state.updateScreen(<MobileNumber updateScreen={this.updateScreen} toggleModal={this.toggleModal} />);
  };

  // renderButtonBasedOnIndex = () => {
  //   if (this.props.currentIndex === 0) {
  //     return (
  //       <button className="loginBtn1" onClick={this.toggleModal}>
  //         LOG IN
  //       </button>
  //     );
  //   } else if (this.props.currentIndex !== 0 && this.props.currentIndex !== 5) {
  //     return (
  //       <div className="d-flex">
  //         <div>
  //           <img src={IOSIcon} alt="ios-icon" />
  //         </div>
  //         <div>
  //           <img src={AndroidIcon} alt="android-icon" />
  //         </div>
  //         <button className="loginBtn2" onClick={this.toggleModal}>
  //           LOG IN
  //         </button>
  //       </div>
  //     );
  //   } else if (this.props.currentIndex === 5) {
  //     return (
  //       <button className="loginBtn2" onClick={this.toggleModal}>
  //         LOG IN
  //       </button>
  //     );
  //   }
  // };

  openAndroidTab = () => {
    const url = `https://play.google.com/store/apps/details?id=com.cMe.com&hl=en_IN`;
    window.open(url, "_blank");
  };

  renderButtonBasedOnIndex = () => {
    return (
      <div className="d-flex">
        <div>
          <img src={IOSIcon} alt="ios-icon" />
        </div>
        <button onClick={this.openAndroidTab} className="androidBtn">
          <img src={AndroidIcon} alt="android-icon" />
        </button>
        {/* <div >
    
        </div> */}
        <button className="loginBtn2" onClick={this.toggleModal}>
          LOG IN
        </button>
      </div>
    );
  };

  render() {
    return (
      <div className="Header" onClose={this.toggleModal}>
        {/* Navbar Module */}
        <Navbar className="Navbar WNavbar" expand="lg">
          <NavbarBrand className="Navbrand">
            <img className="img-responsive" src={keys.AppLogo} width={75} height={75} alt="c-me-logo" />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem className="mx-4 d-flex justify-content-center align-items-center">
                <div className="d-flex">
                  <div>
                    <img src={IOSIcon} alt="ios-icon" />
                  </div>
                  <div onClick={this.openAndroidTab} className="androidBtn">
                    <img src={AndroidIcon} alt="android-icon" />
                  </div>
                  <button className="loginBtn2" onClick={this.toggleModal}>
                    LOG IN
                  </button>
                </div>
                {/* <Button className="btn-md py-2 px-4 " onClick={this.toggleModal}>
                  Log In/Sign Up
                </Button> */}
              </NavItem>
              <NavItem className="mx-4 d-flex justify-content-center align-items-center">
                <Button className="HamburgerBtn">
                  <i className="fas fa-bars fa-1x" />
                </Button>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <MainModal className="Modal" isOpen={this.state.modal} toggle={this.toggleSignUpModal} type="LoginModal">
          {!this.state.currentScreen ? (
            <MobileNumber
              toggleSignUpModal={this.toggleSignUpModal}
              toggleModal={this.toggleModal}
              onClose={this.toggleModal}
              updateScreen={this.updateScreen}
            />
          ) : (
            this.state.currentScreen
          )}
        </MainModal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentIndex: state.Main.UserData.carouselIndex,
  };
};

export default connect(mapStateToProps, null)(Header);
