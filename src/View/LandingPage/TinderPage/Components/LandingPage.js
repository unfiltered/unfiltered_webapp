import React, { Component } from "react";
import "./Index.scss";
import { Link } from "react-router-dom";

class LandingPage extends Component {
  render() {
    return (
      <div id="BelowSection">
        <div className="col-12 bg_links">
          <ul className="d-flex col-12 list-inline">
            <li>
              <Link to="/privacy-policy">
                <span>Privacy Policy</span>
              </Link>
            </li>
            <li>
              <Link to="/terms-and-conditions">
                <span>Terms Of Conditions</span>
              </Link>
            </li>
            <li>
              <Link to="/cookie-policy">
                <span>Cookie Policy</span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default LandingPage;
