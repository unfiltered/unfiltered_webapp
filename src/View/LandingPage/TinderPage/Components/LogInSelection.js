import React, { Component } from "react";
// import Button from reactstrap
import { Button } from "reactstrap";

// import images
import TinderLogo from "../../../../asset/images/tinder_logo.jpg";
import AppleLogo from "../../../../asset/images/apple.png";
import PlayStore from "../../../../asset/images/playstore.png";

// Child Component
import MobileNumber from "./MobileNumber";

class LogInSelection extends Component {
  HandleMobileNumber = () => {
    this.props.updateScreen(<MobileNumber updateScreen={this.props.updateScreen} />);
  };

  render() {
    return (
      <div>
        <div className="LogInmodal text-center p-4">
          <i class="fas fa-times closeBtn" onClick={this.props.toggleModal} />
          <div className="">
            <img src={TinderLogo} width="75" />
          </div>
          <h3>GET STARTED</h3>
          <p>
            By clicking Log In, you agree to our <a href="#">Terms</a>. Learn How we process your data in our <a href="#">Privacy policy</a> and{" "}
            <a href="#">Cookie Policy</a>
          </p>
          <div>
            <Button className="btn m-2 LoginToggleBtn" onClick={this.HandleMobileNumber}>
              LOG IN WITH PHONE NUMBER
            </Button>
          </div>
          <div>
            <Button className="btn m-2 LoginToggleBtn">LOG IN WITH FACEBOOK</Button>
          </div>
          <div className="mt-3">
            <a href="#">Troble Logging In?</a>
          </div>
          <div className="mt-3">
            <span className="">GET THE APP!</span>
          </div>
          <div className="mt-3">
            <Button className="btn m-2 DownloadBtn">
              <img src={AppleLogo} width="25" />
            </Button>
            <Button className="btn m-2 DownloadBtn">
              <img src={PlayStore} width="25" />
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default LogInSelection;
