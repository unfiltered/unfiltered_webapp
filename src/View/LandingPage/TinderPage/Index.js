import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./Components/Index.scss";
import { Images } from "../../../Components/Images/Images";
import "./NewIndex.scss";
import InternetChecker from "../../../hoc/InternetChecker/InternetChecker";
import { Icons } from "../../../Components/Icons/Icons";
import MobileNumber from "./Components/MobileNumber";
import MaterialModal from "../../../Components/Model/MAT_UI_Modal";
import Footer from "../Footer/Footer";

class TinderPage extends Component {
  constructor(props) {
    super(props);
    this.scroll1 = this.scroll1.bind(this);
    this.scroll2 = this.scroll2.bind(this);
    this.box = React.createRef();
    this.state = {
      value: 0,
      currentScreen: "",
      modal: false,
      buttons: [
        // { name: "Real Change", index: 1 },
        // { name: "Real Life", index: 2 },
        { name: "Download", index: 3 },
      ],
    };
  }

  updateScreen = (screen) => this.setState({ currentScreen: screen });

  toggleModal = () => this.setState({ modal: !this.state.modal });

  HandleMobileNumber = () => this.state.updateScreen(<MobileNumber updateScreen={this.updateScreen} toggleModal={this.toggleModal} />);

  changeValue = (data) => this.setState({ value: data });

  openPrivacyPolicy = () => {
    window.open("https://unfiltered.love/privacy/", "_blank");
  };

  openTermsAndConditions = () => {
    window.open("https://unfiltered.love/terms/", "_blank");
  };

  openYoutube = () => {
    window.open("https://www.youtube.com/channel/UCY2Lv67lUTeWAe1OEk7jetA", "_blank");
  };

  openInstagram = () => {
    window.open("https://www.instagram.com/unfiltered.love/", "_blank");
  };

  openTwitter = () => {
    window.open("https://twitter.com/UnFiltered_App", "_blank");
  };

  openFacebook = () => {
    window.open("https://www.facebook.com/UnFilteredApp", "_blank");
  };

  scroll1() {
    this.box.current.scrollBy({ top: 160, behavior: "smooth" });
  }

  openFAQ = () => {
    this.props.history.push("faq");
  };

  scroll2() {
    console.log("clicked");
    this.box.current.scrollBy({ top: -160, behavior: "smooth" });
  }

  openAndroidLink = () => {
    window.open("https://play.google.com/store/apps/details?id=com.unfiltered.com", "_blank");
  };

  openIOSLink = () => {
    window.open("https://apps.apple.com/us/app/unfiltered-dating/id1547926342", "_blank");
  };

  // ye hai landing page.
  render() {
    return (
      <div className="MainPage col-12 p-0" style={{ backgroundImage: "url(" + Images.BackgroundImage + ")", height: "100vh" }}>
        <div className="row m-0">
          <div className="text-right col-12 pr-4">
            {this.state.buttons.map((k) => (
              <button
                className={k.index === this.state.value ? "lp_buttons_active" : "lp_buttons"}
                key={k.index}
                onClick={() => this.changeValue(k.index)}
              >
                {k.name}
              </button>
            ))}
          </div>
          <div className="text-center col-12 p-0">
            <img src={"https://unfiltered.love/wp-content/uploads/2020/11/unfiltered.png"} height={180} />
          </div>
          <div className="subHeader text-center col-12 p-0 mb-5">Real People. Real Love</div>
        </div>
        {this.state.value === 0 ? (
          <div>
            {/* <section className="mx-auto d-flex mb-5" style={{ justifyContent: "space-evenly", width: "40%" }}>
              <div className="d-flex flex-column text-center">
                <div className="timer-count">005</div>
                <div className="bold-2-sub-text">Days</div>
              </div>
              <div className="d-flex flex-column text-center">
                <div className="timer-count">14</div>
                <div className="bold-2-sub-text">Hours</div>
              </div>
              <div className="d-flex flex-column text-center">
                <div className="timer-count">14</div>
                <div className="bold-2-sub-text">Minutes</div>
              </div>
              <div className="d-flex flex-column text-center">
                <div className="timer-count">23</div>
                <div className="bold-2-sub-text">Seconds</div>
              </div>
            </section> */}
            <div className="text-center col-12 p-0">
              <div className="main-box position-relative">
                <div className="w-100 position-absolute bold-text-data">
                  <p className="subHeader-insideBox m-0">Be Real, Get Real and Meet Real</p>
                  <p className="subText-insideBox">Start Meeting new real people</p>
                  <div>
                    <button className="red-button mr-1" onClick={this.toggleModal}>
                      Join
                    </button>
                    <button className="white-button ml-1" onClick={this.toggleModal}>
                      Login
                    </button>
                  </div>
                </div>
                <div className="black-box"></div>
              </div>
            </div>
          </div>
        ) : this.state.value === 1 ? (
          <div className="text-center col-12 p-0">
            <div className="main-box position-relative">
              <div className="w-100 position-absolute bold-text-data1">
                <p className="subHeader-insideBox1 m-0">UnFiltered is passionate about</p>
                <p className="subHeader-insideBox1 m-0">
                  <b>BEING THE REAL CHANGE</b> to a
                </p>
                <p className="subHeader-insideBox1 m-0">
                  world that needs <b>CHANGING</b>
                </p>
                <button className="red-button mt-3" onClick={() => this.changeValue(2)}>
                  See how we do it
                </button>
              </div>
              <div className="black-box1"></div>
            </div>
          </div>
        ) : this.state.value === 2 ? (
          <div className="text-center col-12 p-0">
            <div className="main-box position-relative">
              <div className="w-100 position-absolute bold-text-data2">
                <section className="w-50 mx-auto scrollable-box-2 px-4" ref={this.box}>
                  <article>
                    <div className="pb-3">
                      <b className="bold-2-header">1</b>
                    </div>
                    <div className="bold-2-sub-header">Patented Security Features</div>
                    <div className="bold-2-sub-text">
                      We use patented security technology to keep your account information safe and secure for compromise
                    </div>
                  </article>
                  <article>
                    <div className="py-3">
                      <b className="bold-2-header">2</b>
                    </div>
                    <div className="bold-2-sub-header">Transparent Image Identiy</div>
                    <div className="bold-2-sub-text">
                      All profiles are UnFiltered. Photos and vidoes are taken from within the app and time stamped. No saved uploads, Real
                      People.
                    </div>
                  </article>
                  <article className="py-3">
                    <div>
                      <b className="bold-2-header">3</b>
                    </div>
                    <div className="bold-2-sub-header">Verified Profiles</div>
                    <div className="bold-2-sub-text">
                      We use algorithms to verify email, name and phone number across our channels to verify <b>REAL PEOPLE</b> and{" "}
                      <b>REMOVE FAKE PROFILES</b>
                    </div>
                  </article>
                  <article>
                    <div className="py-3">
                      <b className="bold-2-header">4</b>
                    </div>
                    <div className="bold-2-sub-header">In-App Communications</div>
                    <div className="bold-2-sub-text">
                      <b>ENHANCED</b> and <b>ENCRYPTED</b> communications to protect you. Talk, text, video share attachments all while
                      protecting your personal space.
                    </div>
                  </article>
                  <article>
                    <div className="py-3">
                      <b className="bold-2-header">5</b>
                    </div>
                    <div className="bold-2-sub-header">Limited Matches</div>
                    <div className="bold-2-sub-text">
                      Allows for <b>GREATER INTERACTION</b> and <b>MORE MEANINGFUL CONNECTIONS.</b> Make match selection deliberate and
                      intentional space.
                    </div>
                  </article>
                  <article>
                    <div className="py-3">
                      <b className="bold-2-header">6</b>
                    </div>
                    <div className="bold-2-sub-header">In-App Date Concierge</div>
                    <div className="bold-2-sub-text">
                      Where to go? What to do? Our Intellegent in-app concierge will provide options, send invites, updates schedules and
                      provide GPS
                    </div>
                  </article>
                </section>
              </div>
              <div className="position-absolute scroll-chevron w-100">
                <img src={Icons.ChevronDown} width={30} alt="chevron-up" onClick={this.scroll1} />
              </div>
              <div className="position-absolute scroll-chevron-up w-100">
                <img src={Icons.ChevronDown} style={{ transform: "rotate(180deg)" }} width={30} alt="chevron-down" onClick={this.scroll2} />
              </div>
              <div className="black-box2"></div>
            </div>
          </div>
        ) : this.state.value === 3 ? (
          <div className="text-center col-12 p-0">
            <div className="main-box position-relative">
              <div className="w-100 position-absolute bold-text-data1">
                <p className="subHeader-insideBox m-0">Get the UnFiltered App Now!</p>
                <p className="bold-2-sub-text">Enter your number and get a text with a download link</p>
                <div className="position-relative input-box">
                  <img src={Icons.callDate} className="phone-icon" alt="phone-icon" width={20} />
                  <input placeholder="913-939-6939" type="number" className="phone-text" />
                  <button className="red-button mt-3" onClick={() => this.changeValue(2)}>
                    Send
                  </button>
                </div>
              </div>
              <div className="black-box3"></div>
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="position-absolute app-links">
          <div onClick={this.openIOSLink}>
            <img src={require("../../../asset/images/Image_14.png")} alt="android" height={70} />
          </div>
          <div onClick={this.openAndroidLink}>
            <img src={require("../../../asset/images/Image_15.png")} alt="android" height={73} />
          </div>
        </div>
        <MaterialModal
          // className="Modal"
          isOpen={this.state.modal}
          toggle={this.toggleModal}
          width={"480px"}
          // type="LoginModal"
        >
          {!this.state.currentScreen ? (
            <MobileNumber
              toggleSignUpModal={this.toggleModal}
              toggleModal={this.toggleModal}
              onClose={this.toggleModal}
              updateScreen={this.updateScreen}
            />
          ) : (
            this.state.currentScreen
          )}
        </MaterialModal>
        <Footer />
      </div>
    );
  }
}

export default withRouter(InternetChecker(TinderPage));
