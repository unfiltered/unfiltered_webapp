// Phone-Number Validation
export const ValidatePhone = (input) => {
  let inputVal = input.target.value;
  let PhonePattern = /[0-9]{3}-[0-9]{3}-[0-9]{4}/;
  return PhonePattern.test(inputVal);
};

// Email-ID Validation
export const ValidateEmail = (input) => {
  // let pattern = /^\w+@\w+\..{2,3}(.{2,3})?$/;
  let pattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

  return pattern.test(input.target.value) ? true : false;
  // return pattern.test(input) ? true : false;
};

// Common Validation for All Name Input
export const ValidateName = (input) => {
  let inputVal = input.target.value;
  let NamePattern = /^[a-zA-Z\s]*$/;
  return NamePattern.test(inputVal);
};

// Password Validation
export const ValidatePassword = (input) => {
  let inputVal = input.target.value;
  let PasswordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;
  return PasswordPattern.test(inputVal);
};
