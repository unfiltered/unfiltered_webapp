import request from "superagent";
var cloudinary = require("cloudinary/lib/cloudinary");
const unsginedGuy = "qqt6ygpd";
const urlToUpload = "https://api.cloudinary.com/v1_1/niko94/image/upload";
const videoUrlToUpload = "https://api.cloudinary.com/v1_1/niko94/video/upload";

cloudinary.config({
  cloud_name: "niko94",
  api_key: "349222331418379",
  api_secret: "jnjaBONFvmocwus9DW-CfpaumfQ",
});

let uploadImageToCloudinaryPromise = (files) => {
  console.log("main fn", files);
  return new Promise((resolve, reject) => {
    for (let file of files) {
      console.log("file", file);
      request
        .post(urlToUpload)
        .field("upload_preset", unsginedGuy)
        .field("file", file[0])
        .field("multiple", false)
        .end((error, response) => {
          return resolve(response);
        });
    }
  });
};

let singleImageToCloudinaryPromise = (file) => {
  console.log("main fn", file);
  return new Promise((resolve, reject) => {
    request
      .post(urlToUpload)
      .field("upload_preset", unsginedGuy)
      .field("file", file[0])
      .field("multiple", false)
      .end((error, response) => {
        return resolve(response);
      });
  });
};

let __BlobUpload = (file) => {
  // var file = new File([file], "profile");
  return new Promise((resolve, reject) => {
    request
      .post(urlToUpload)
      .field("upload_preset", unsginedGuy)
      .field("file", file)
      .field("multiple", false)
      .end((error, response) => {
        return resolve(response);
      });
  });
  // data.append("file", file);
  // data.append("upload_preset", "tpczegbo");
  // const res = await fetch("https://api.cloudinary.com/v1_1/datesndime/image/upload/", {
  //   method: "POST",
  //   body: data
  // });
  // const op = await res;
  // return op
};

let uploadVideoToCloudinaryPromise = (files) => {
  // console.log("main fn", files);
  return new Promise((resolve, reject) => {
    for (let file of files) {
      console.log("file", file);
      request
        .post(videoUrlToUpload)
        .field("upload_preset", unsginedGuy)
        .field("file", file[0])
        .field("multiple", false)
        .end((error, response) => {
          return resolve(response);
        });
    }
  });
};

let singleVideoUpload = (file) => {
  // console.log("main fn", files);
  return new Promise((resolve, reject) => {
    console.log("file", file);
    request
      .post(videoUrlToUpload)
      .field("upload_preset", unsginedGuy)
      .field("file", file)
      .field("multiple", false)
      .end((error, response) => {
        return resolve(response);
      });
  });
};

export { __BlobUpload, uploadImageToCloudinaryPromise, uploadVideoToCloudinaryPromise, singleVideoUpload, singleImageToCloudinaryPromise };
