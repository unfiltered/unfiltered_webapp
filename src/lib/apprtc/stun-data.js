const stunServers = {
    "_id": "5e57bde19bd027e35aa03a0d",
    "iceServers": [
        {
            "urls": [
                "stun:bturn2.xirsys.com"
            ]
        },
        {
            "username": "AjxdMUu_pSHgQBzBgc4jTOMcV1Z-_I1ozjQIoggBpNBBToJdD8ERSrluHqLuVR8hAAAAAF5DnDlkdWJseQ==",
            "credential": "95331758-4d61-11ea-bd26-9646de0e6ccd",
            "urls": [
                "turn:bturn2.xirsys.com:80?transport=udp",
                "turn:bturn2.xirsys.com:3478?transport=udp",
                "turn:bturn2.xirsys.com:80?transport=tcp",
                "turn:bturn2.xirsys.com:3478?transport=tcp",
                "turns:bturn2.xirsys.com:443?transport=tcp",
                "turns:bturn2.xirsys.com:5349?transport=tcp"
            ]
        }
    ]
};

export const finalServerCreds = {
    ...stunServers,
    "lifetimeDuration": "86400s",
    "blockStatus": "NOT_BLOCKED",
    "iceTransportPolicy": "all"
}