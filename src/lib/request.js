import axios from "axios";
import { getCookie, setCookie, removeCookie } from "./session";
// import { setCookie, getCookie, removeCookie } from "./session";
import { API_HOST } from "./config";
import { refreshToken } from "../services/auth";

export const getUrl = (endpoint) => API_HOST + endpoint;

// export const getMediaUrl = (endpoint) => "https://live-upload.unfiltered.love" + endpoint;

export const getMediaUrl = (endpoint) => "https://upload.unfiltered.love" + endpoint;

const globalLogout = () => {
  if (typeof window != "undefined") {
    removeCookie("token");
    removeCookie("SignUpseemeid");
    removeCookie("findMateId");
    removeCookie("location");
    removeCookie("citylocation");
    removeCookie("uid");
    localStorage.removeItem("firstName");
    localStorage.removeItem("height");
    localStorage.removeItem("email");
    localStorage.removeItem("picture");
    window.location.href = "/";
  }
};

export const mediaPost = async (endpoint, data) => {
  delete axios.defaults.headers.common["Authorization"];
  delete axios.defaults.headers.common["lang"];
  delete axios.defaults.headers.common["accept"];
  return axios.post(getMediaUrl(endpoint), data, {
    headers: { "Content-type": "multipart/form-data" },
  });
};

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response) {
      if (error.response.status === 401) {
        refreshToken()
          .then((data) => {
            // if the refresh token api is not 200, clear session and logout user.
            if (data.status !== 200) {
              globalLogout();
              return;
            }

            // update new session token in cookie.
            let newToken = data && data.data && data.data.data && data.data.data.accessToken;
            setCookie(newToken);
            window.location.reload();
          })
          .catch((err) => {
            globalLogout(); // clear session
          });
      }
    } else {
      return Promise.reject(error);
    }
  }
);

export const post = async (endpoint, data) => {
  return axios.post(getUrl(endpoint), data, {
    headers: { "Content-Type": "application/json", lang: "en" },
  });
};

export const patch = async (endpoint, data) => {
  return axios.patch(getUrl(endpoint), data, {
    headers: { "Content-Type": "application/json", lang: "en" },
  });
};

//
export const get = async (endpoint, mytoken) => {
  axios.defaults.headers.common["Authorization"] = await getCookie("token", "");
  return axios.get(getUrl(endpoint), {
    headers: { "Content-Type": "application/json", lang: "en" },
  });
};

export const postWithToken = async (endpoint, data) => {
  let a = await endpoint;
  if (a === "/faceBooklogin" || a === "/verificaitonCode" || a === "/requestOtp" || a === "/profile" || a === "/login") {
    axios.defaults.headers.common["Authorization"] = "Basic M2VtYmVkMDA3OjNlbWJlZDAwNw==";
    return axios.post(getUrl(endpoint), data, {
      headers: { "Content-Type": "application/json", lang: "en" },
    });
  } else {
    axios.defaults.headers.common["Authorization"] = await getCookie("token", "");
    return axios.post(getUrl(endpoint), data, {
      headers: { "Content-Type": "application/json", lang: "en" },
    });
  }
};

export const patchWithToken = async (endpoint, data) => {
  let a = await endpoint;
  if (a === "/phoneNumberExistsVerificaton" || a === "/emailIdExistsVerificaiton") {
    axios.defaults.headers.common["Authorization"] = "Basic M2VtYmVkMDA3OjNlbWJlZDAwNw==";
    return axios.patch(getUrl(endpoint), data, {
      headers: { "Content-Type": "application/json", lang: "en" },
    });
  } else {
    axios.defaults.headers.common["Authorization"] = await getCookie("token", "");
    return axios.patch(getUrl(endpoint), data, {
      headers: { "Content-Type": "application/json", lang: "en" },
    });
  }
};

export const putWithToken = async (endpoint, data) => {
  axios.defaults.headers.common["Authorization"] = await getCookie("token", "");
  return axios.put(getUrl(endpoint), data, {
    headers: { "Content-Type": "application/json", lang: "en" },
  });
};

export const deleteReq = async (endpoint, data) => {
  axios.defaults.headers.common["Authorization"] = await getCookie("token", "");
  axios.defaults.headers.common["lang"] = "en";
  return axios.delete(getUrl(endpoint), data, {
    headers: { "Content-Type": "application/json" },
  });
};
