import React, { Component } from "react";

class PaginationHOC extends Component {
  state = {
    scrolling: false,
  };
  async componentDidMount() {
    console.log("didMount paginationHOC");
    document.addEventListener(
      "scroll",
      (event) => {
        console.log("handleScroll attached");
      },
      true
    );
  }

  detectBottomTouch = (bottomDifferenceInPixel = 0) => {
    return document.body.offsetHeight + window.scrollY >= document.body.scrollHeight - bottomDifferenceInPixel;
  };

  handleScroll = () => {
    console.log("handleScroll attached");
    // if (
    //   this.detectBottomTouch(400) &&
    //   !this.state.scrolling &&
    //   this.props.products &&
    //   this.props.products.length > 0 &&
    //   this.props.products.length !== 200
    // ) {
    //   this.loadMoreData();
    // }
  };

  loadMoreData = () => {
    this.setState(
      {
        scrolling: true,
      },
      async () => {
        try {
          let data = await this.props.apiCall(this.props.offset).then((res) => {
            return res;
          });
          console.log("incoming data", data);
          this.props.updateOffset(20);
          this.props.updatePosts(data);
          this.setState({
            scrolling: false,
          });
        } catch (e) {
          this.setState({
            scrolling: true,
          });
        }
      }
    );
  };
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
    // console.log("innerHight ",window.innerHeight);
  }
  render() {
    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

export default PaginationHOC;
