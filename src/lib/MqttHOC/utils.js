import {
  messageSubject,
  acknowledgementSubject,
  subscribeToUserSubject,
  unSubscribeToUserSubject,
  goOnlineSubject,
  goOOfflineSubject,
  logoutSubject,
  videoCallSubject,
} from "./subjects";

export const sendMessageOverMqtt = async (payload) => {
  messageSubject.next(payload);
};

export const initVideoCallOverMqtt = async (payload, key) => {
  console.log(
    'CALLEDDDDDDDDDDDDDDDDDDDDDDDD',payload, key
  )
  videoCallSubject.next({ payload: payload, key: key });
};

export const sendAcknowledgementsOverMqtt = async (payload) => {
  acknowledgementSubject.next(payload);
};

export const subscribeToUserTopic = async (payload) => {
  subscribeToUserSubject.next(payload);
};

export const unSbscribeToUserTopic = async (payload) => {
  unSubscribeToUserSubject.next(payload);
};

export const goOnlineSubscribeToTopic = async (payload) => {
  console.log("goOnlineSubscribeToTopic", payload);
  goOnlineSubject.next(payload);
};

export const goOfflineSubscribeToTopic = async (payload) => {
  goOOfflineSubject.next(payload);
};

export const logoutFromApp = async (payload) => {
  logoutSubject.next(payload);
};
