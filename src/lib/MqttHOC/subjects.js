import { Subject } from "rxjs";

export const messageSubject = new Subject();
export const videoCallSubject = new Subject();
export const acknowledgementSubject = new Subject();
export const notification = new Subject();
export const subscribeToUserSubject = new Subject();
export const unSubscribeToUserSubject = new Subject();
export const goOnlineSubject = new Subject();
export const goOOfflineSubject = new Subject();
export const logoutSubject = new Subject();
