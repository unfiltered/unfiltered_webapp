import Paho from "paho-mqtt";
import { getCookie } from "../session";

const hasUndefined = (target) => {
  for (var member in target) {
    if (target[member] === undefined || target[member] === null) return true;
  }
  return false;
};

/** function to publish message when logged in user send message to target user */
export const PublishMessage = (payload, client, store) => {
  if (hasUndefined(payload)) {
    console.log("some fields are missing message cannot be sent over mqtt");
  } else {
    let message = new Paho.Message(JSON.stringify(payload));
    let topic = "Message/" + payload.to;
    message.qos = 2;
    message.destinationName = topic;
    client.send(message);
    payload.status = 1;
  }
};

export const publishTopic = (publishObj = {}, client, key) => {
  try {
    if (client) {
      client.publish(`Calls/${key}`, JSON.stringify(publishObj), 0, false);
    }
  } catch (ex) {
    console.log("Error In Call Publish  Message:: ", ex);
  }
};

/** function to publish acknowledgements when clicked on user chat */
export const PublishAcknowledgement = (payload, client, store) => {
  console.log("PublishAcknowledgement", payload);
  let topic;
  if ("senderId" in payload.obj) {
    topic = "Acknowledgement/" + payload.obj.senderId;
  } else {
    topic = "Acknowledgement/" + payload.obj.from;
  }
  let obj = {};
  obj["doc_id"] = "xxx";
  obj["from"] = payload.obj.receiverIdentifier || payload.obj.to || payload.obj.receiverId;
  obj["msgIds"] = [payload.obj.messageId || payload.obj.id];
  obj["status"] = payload.newStatus;
  obj["to"] = payload.obj.senderId || payload.obj.from;
  if (payload.newStatus === 2 || payload.newStatus === "2") {
    obj["deliveryTime"] = new Date().getTime();
  } else if (payload.newStatus === 3 || payload.newStatus === "3") {
    obj["readTime"] = new Date().getTime();
  }
  let message = new Paho.Message(JSON.stringify(obj));
  message.qos = 2;
  message.destinationName = topic;
  message.retain = false;
  client.send(message);
};

export const subscribeToUserUtils = (payload, client, store) => {
  if (hasUndefined(payload) || payload === "" || payload.length === 0) {
    console.log("some fields are missing message cannot be sent over mqtt");
  } else {
    setTimeout(() => {
      try {
        client.subscribe("OnlineStatus/" + payload);
      } catch (e) {
        // setTimeout(() => {
        //   client.subscribe("OnlineStatus/" + payload);
        // }, 2500)
      }
    }, 2500);
  }
};

export const unSubscribeToUserUtils = (payload, client, store) => {
  if (hasUndefined(payload)) {
    console.log("some fields are missing message cannot be sent over mqtt");
  } else {
    client.unsubscribe("OnlineStatus/" + payload);
  }
};

export const goOnlineUtils = (payload, client, store) => {
  console.log("go online util", client);
  let obj = {
    status: 1,
    onlineStatus: 1,
    userId: payload,
    _id: payload,
    lastSeenEnabled: true,
    lastOnline: new Date().getTime(),
  };
  if (hasUndefined(obj)) {
    console.log("cannot publish onlineStatus on undefined");
  } else {
    let message = new Paho.Message(JSON.stringify(obj));
    message.qos = 0;
    message.destinationName = `OnlineStatus/${payload}`;
    message.retain = true;
    client.publish(message);
  }
};

export const goOfflineUtils = (payload, client, store) => {
  let obj = {
    status: 0,
    onlineStatus: 0,
    userId: payload,
    _id: payload,
    lastSeenEnabled: false,
    lastOnline: new Date().getTime(),
  };
  if (hasUndefined(obj)) {
    console.log("cannot publish onlineStatus on undefined");
  } else {
    let message = new Paho.Message(JSON.stringify(obj));
    message.qos = 0;
    message.destinationName = `OnlineStatus/${payload}`;
    message.retain = true;
    client.publish(message);
  }
};

export const logoutUtils = (payload, client, store, func) => {
  let publishObj = {};
  publishObj["status"] = 0;
  publishObj["onlineStatus"] = 0;
  publishObj["_id"] = payload;
  publishObj["timestamp"] = new Date().getTime();
  publishObj["userId"] = payload;
  publishObj["lastSeenEnabled"] = true;
  publishObj["lastOnline"] = new Date().getTime();
  let message = new Paho.Message(JSON.stringify(publishObj));
  message.qos = 0;
  message.destinationName = `OnlineStatus/${payload}`;
  message.retain = true;
  if (hasUndefined(publishObj)) {
    console.log("cannot publish onlineStatus on undefined");
  } else {
    client.publish(message);
    client.unsubscribe("Message/" + payload);
    client.unsubscribe("Acknowledgement/" + payload);
    client.unsubscribe("Calls/" + payload);
    client.unsubscribe("UserUpdates/" + payload);
    client.unsubscribe("FetchMessages/" + payload);
    client.unsubscribe("Friend/" + payload);
    client.unsubscribe("OnlineStatus/" + payload);
    client.unsubscribe(payload);
    func();
  }
};
