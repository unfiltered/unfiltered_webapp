import React from "react";
import Paho from "paho-mqtt";
import { setCookie, getCookie } from "../../lib/session";
import { keys } from "../keys";
// import {
//   storeAllChatData,
//   storeAllChatsOfSelectedUser,
//   addNewChatToSelectChat,
//   updateAcknowledgement,
//   userSelectedToChat,
// } from "../../redux/actions/chat/chat";
// import { storeWebrtcData } from "../../redux/actions/webrtc/webrtc";
import {
  messageSubject,
  acknowledgementSubject,
  subscribeToUserSubject,
  unSubscribeToUserSubject,
  goOnlineSubject,
  goOOfflineSubject,
  logoutSubject,
  videoCallSubject,
} from "./subjects";
import { connect } from "react-redux";
import {
  PublishMessage,
  PublishAcknowledgement,
  subscribeToUserUtils,
  unSubscribeToUserUtils,
  goOfflineUtils,
  goOnlineUtils,
  logoutUtils,
  publishTopic,
} from "./mqttPublish";
import * as func from "../../init-fcm";
import { sendAcknowledgementsOverMqtt, goOnlineSubscribeToTopic, initVideoCallOverMqtt } from "./utils";
// import { getChatsWithPageNo } from "../../services/chat";
// import MatUIModal from "../../components/model/centerModal";
// import VideoComponent from "../../components/video-call/video-call";
import { USER_PROFILE_ACTION_FUN, storeCoins, matchFound, leftSideChatLogs, updateAcknowledgement } from "../../actions/UserProfile";
import * as actions from "../../actions/UserSelectedToChat";
import * as actionChat from "../../actions/chat";
import { boostActivatedSessionIncomingData } from "../../actions/boost";
import Snackbar from "../../Components/Snackbar/Snackbar";
import { __updateMqttMessage } from "../../actions/webrtc";
import { GetAllChats, GetAllFriendsRequest } from "../../controller/auth/verification";
import { FRIEND_REQUEST_COUNT_FUNC } from "../../actions/Friends";
import { SELECTED_PROFILE_ACTION_FUN } from "../../actions/selectedProfile";

let client = "";

class MQTTHOC extends React.Component {
  state = {
    open: false,
    variant: "success",
    usermessage: "",
    page: 0,
  };

  getAllMessages = (page) => {
    GetAllChats(page).then((res) => {
      let allData = res.data.data.filter((k) => k.totalUnread !== 0 && k.payload !== "3embed test");
      let result = res.data.data.map(function (el) {
        var o = Object.assign({}, el);
        o.checked = false;
        return o;
      });
      this.props.leftSideChatLogsFN(result);
      this.props._storeUnreadChatCount(allData.length);
    });
  };

  onMessageArrived = (message) => {
    // console.log("incoming message", message);
    try {
      let data = JSON.parse(message.payloadString);
      console.log(data, message.topic);

      this.props.__updateMqttMessage({ payload: data, topic: message.topic });

      /**
       * when user is subscribed to userId
       */
      if (message.topic === getCookie("uid")) {
        if (data.messageType === "block") {
          let allExChats = [...this.props.leftSideChatLogs];
          let index = allExChats.findIndex((k) => k.receiverId === data.userId);
          allExChats[index].isBlocked = 1;
          this.props.leftSideChatLogsFN(allExChats);
        }

        if (data.messageType === "unBlock") {
          let allExChats = [...this.props.leftSideChatLogs];
          let index = allExChats.findIndex((k) => k.receiverId === data.userId);
          allExChats[index].isBlocked = 0;
          this.props.leftSideChatLogsFN(allExChats);
        }

        if ("SecondLikedByName" in data && "firstLikedByName" in data) {
          this.props.matchFoundFunc(data);
          this.setState({
            open: true,
            variant: "success",
            usermessage: "Match Found",
          });
        }
      }

      /**
       * when user is subscribed to boost
       */

      if (data.messageType === "boost") {
        this.props.boostActivatedSessionIncomingData(data.boost);
      }

      /**
       * when user is subscribed to target user messages
       */

      let allExisitingChat = [...this.props.leftSideChatLogs];
      if (message.topic.includes("Message/" + getCookie("uid"))) {
        if (this.props.UserSelectedToChat.receiverIdentifier === data.from || this.props.UserSelectedToChat.recipientId === data.from) {
          console.log("*** chatting with him ***");
          sendAcknowledgementsOverMqtt({ obj: data, newStatus: "2" });
          setTimeout(() => {
            sendAcknowledgementsOverMqtt({ obj: data, newStatus: "3" });
          }, 300);
        } else {
          console.log("*** chat not focused ***");
          sendAcknowledgementsOverMqtt({ obj: data, newStatus: "2" });
        }

        let checkIfUserExistsInChatIndex = allExisitingChat.findIndex((k) => k.chatId === data.chatId);

        if (checkIfUserExistsInChatIndex > -1) {
          if (data.chatId === this.props.UserSelectedToChat.chatId) {
            func.displayMessages(data);
            // if user chat is active with target user
            console.log("--- YYYY");
            allExisitingChat[checkIfUserExistsInChatIndex].payload = data.payload;
            allExisitingChat[checkIfUserExistsInChatIndex].timestamp = data.timestamp;
            if (
              allExisitingChat[checkIfUserExistsInChatIndex]["checked"] === false &&
              allExisitingChat[checkIfUserExistsInChatIndex].totalUnread === 0
            ) {
              allExisitingChat[checkIfUserExistsInChatIndex]["checked"] = true;
              // allExisitingChat[checkIfUserExistsInChatIndex]["onlineStatus"] = 1;
              this.props._storeUnreadChatCount(this.props.unreadChatCount + 1);
            }
            this.props.addNewChatToExistingObject(data);
          } else {
            func.displayMessages(data);
            // user is on other page
            console.log("--- ZZZZ");
            allExisitingChat[checkIfUserExistsInChatIndex].payload = data.payload;
            allExisitingChat[checkIfUserExistsInChatIndex].timestamp = data.timestamp;
            if (
              allExisitingChat[checkIfUserExistsInChatIndex]["checked"] === false &&
              allExisitingChat[checkIfUserExistsInChatIndex].totalUnread === 0
            ) {
              allExisitingChat[checkIfUserExistsInChatIndex]["checked"] = true;
              // allExisitingChat[checkIfUserExistsInChatIndex]["onlineStatus"] = 1;
              this.props._storeUnreadChatCount(this.props.unreadChatCount + 1);
            }

            allExisitingChat[checkIfUserExistsInChatIndex].totalUnread = ++allExisitingChat[checkIfUserExistsInChatIndex].totalUnread;
          }

          this.props.leftSideChatLogsFN(allExisitingChat);
        } else {
          if (this.props.UserSelectedToChat.receiverIdentifier === data.from || this.props.UserSelectedToChat.recipientId === data.from) {
            console.log("---- here");
            this.props.addNewChatToExistingObject(data);
          } else {
            /** this works when a message from new user is comming */
            GetAllChats(this.state.page).then((res) => {
              this.props._storeUnreadChatCount(this.props.unreadChatCount + 1);
              this.props.leftSideChatLogsFN(res.data.data);
            });
          }
        }
      }

      /** this function is to get acknowledgements */
      if (message.topic.includes(`Acknowledgement/${data.to}`)) {
        let index = allExisitingChat.findIndex((k) => k.senderId === data.to || k.chatInitiatedBy === data.to);
        if (index > -1) {
          if (this.props.UserSelectedToChat.receiverIdentifier === data.from || this.props.UserSelectedToChat.recipientId === data.from) {
            this.props.UPDATE_ACKNOWLEDGEMENT(data.msgIds[0], parseInt(data.status));
          }
        }
      }
      // if (message.topic.includes(`Acknowledgement/${getCookie("uid")}`)) {
      //   console.log("received acknowledgement", data);
      //   if (checkIfUserExistsInChatIndex > -1) {
      //     if (this.props.userSelectedToChat.senderId === data.from) {
      //       console.log("received acknowledgement");
      //       this.props.UPDATE_ACKNOWLEDGEMENT(data.msgIds[0], parseInt(data.status));
      //     }
      //     console.log("incoming message", data);
      //   }
      // }

      /**
       * get user current status of offline/online
       */

      if (message.topic.includes(`OnlineStatus/${this.props.UserSelectedToChat.recipientId}`)) {
        let checkIfUserExistsInChatIndex = allExisitingChat.findIndex((k) => k.recipientId === data.userId || k._id === data.userId);
        allExisitingChat[checkIfUserExistsInChatIndex]["onlineStatus"] = data.onlineStatus;
        this.props.leftSideChatLogsFN(allExisitingChat);
        this.props.userSelectedToChat(allExisitingChat[checkIfUserExistsInChatIndex]);
      }

      if (message.topic.includes(`Calls/${getCookie("uid")}`)) {
        if (data.type === 2) {
          initVideoCallOverMqtt(
            { type: 2, callType: "1", roomId: data.callId, dateId: data.dateId && data.dateId.length > 0 ? data.dateId : "" },
            data.userId
          );
          this.props.userSelectedToChat("");
          this.props.selectedProfileForChat("");
        }
      }
      /**
       * store coins when added from admin
       */
      if (data.messageType === "AdminCoin") {
        let updatedCoins = this.props.CoinBalance + data.coinAmount;
        this.props.storeCoinBalance(updatedCoins);
      }
    } catch (e) {
      console.log("message derived error", e);
    }
  };
  connection = (userId) => {
    console.log("userId", userId);

    if (userId) {
      let clientId = userId;
      client = new Paho.Client(keys.mqtt, clientId);
      console.log("mqtt connected [chat]", client.isConnected());
      client.connect({
        userName: keys.mqttUserName,
        password: keys.mqttPassword,
        useSSL: true,
        mqttVersion: 3,
        keepAliveInterval: 3,
        // willMessage: offlineStatus(),
        cleanSession: false,
        reconnect: true,
        onFailure: (e) => {
          console.log("mqtt connection fail", client.isConnected());
          this.connection();
        },

        onSuccess: (res) => {
          console.log("onSuccess", res);
          console.log("mqtt connected [chat]", client.isConnected());
          goOnlineSubscribeToTopic(userId);
          try {
            console.log("inside try block");
            client.subscribe("Message/" + userId);
            client.subscribe("Acknowledgement/" + userId);
            client.subscribe("Calls/" + userId);
            client.subscribe("UserUpdates/" + userId);
            client.subscribe("FetchMessages/" + userId);
            client.subscribe("Friend/" + userId);
            client.subscribe("OnlineStatus/" + userId);
            client.subscribe(userId);
            client.onMessageArrived = this.onMessageArrived;
          } catch (e) {
            console.log("mqtt connection error", e);
          }
        },
      });

      client.onConnectionLost = () => {
        setCookie("connected", "false");
      };
    }
  };

  subscribeToAllSubjects = () => {
    try {
      this.messageSubjectRxObject = messageSubject.subscribe((payload) => {
        PublishMessage(payload, client, this.props.store);
      });
      this.videoInitObject = videoCallSubject.subscribe(({ payload, key }) => {
        publishTopic(payload, client, key);
      });
      this.acknowledgementSubjectRxObject = acknowledgementSubject.subscribe((payload) => {
        PublishAcknowledgement(payload, client, this.props.store);
      });
      this.subscribeToUserRxObject = subscribeToUserSubject.subscribe((payload) => {
        subscribeToUserUtils(payload, client, this.props.store);
      });
      this.unSubscribeToUserRxObject = unSubscribeToUserSubject.subscribe((payload) => {
        unSubscribeToUserUtils(payload, client, this.props.store);
      });
      this.onlineRxObject = goOnlineSubject.subscribe((payload) => {
        goOnlineUtils(payload, client, this.props.store);
      });
      this.offlineRxObject = goOOfflineSubject.subscribe((payload) => {
        goOfflineUtils(payload, client, this.props.store);
      });
      this.logoutSubject = logoutSubject.subscribe((payload) => {
        logoutUtils(payload, client, this.props.store, this.unsubscribeAllSubjects);
      });
    } catch (e) {
      console.log("[subscribeToAllSubjects] err", e);
    }
  };

  componentDidMount() {
    console.log("[HOC] mqtt mount");
    setTimeout(() => {
      let userId = this.props._userProfile && this.props._userProfile.UserProfile && this.props._userProfile.UserProfile.userId;
      if (userId === undefined || userId === "" || userId === null) {
        this.subscribeToAllSubjects();
        this.connection(getCookie("uid"));
        this.getAllMessages(this.state.page);
        func.subscribeToNotificationsOnCookie();
        GetAllFriendsRequest().then((res) => {
          this.props.storeFriendRequestCount(res.data.data.length);
        });
      } else {
        this.subscribeToAllSubjects();
        this.connection(userId);
        this.getAllMessages(this.state.page);
        GetAllFriendsRequest().then((res) => {
          this.props.storeFriendRequestCount(res.data.data.length);
        });
        func.subscribeToNotificationsOnCookie();
      }
    }, 2000);
  }

  unsubscribeAllSubjects = () => {
    this.unSubscribeToUserRxObject.unsubscribe();
    this.acknowledgementSubjectRxObject.unsubscribe();
    this.subscribeToUserRxObject.unsubscribe();
    this.unSubscribeToUserRxObject.unsubscribe();
    this.onlineRxObject.unsubscribe();
    this.offlineRxObject.unsubscribe();
    this.logoutSubject.unsubscribe();
  };

  componentWillUnmount() {
    this.unSubscribeToUserRxObject.unsubscribe();
    this.acknowledgementSubjectRxObject.unsubscribe();
    this.subscribeToUserRxObject.unsubscribe();
    this.unSubscribeToUserRxObject.unsubscribe();
    this.onlineRxObject.unsubscribe();
    this.offlineRxObject.unsubscribe();
    this.logoutSubject.unsubscribe();
  }

  //   wipeWebrtcData = () => {
  //     this.props.removeWebrtcData({});
  //   };

  render() {
    // console.log("mqtt webrtc data", this.props.webrtc);
    return (
      <div>
        {this.props.children}
        {/* <MatUIModal isOpen={this.props.webrtc && this.props.webrtc.callId} toggle={this.wipeWebrtcData} width={"80%"}> */}
        {/* <VideoComponent
            UserSelectedToChat={this.props.userSelectedToChat}
            wipeWebrtcData={this.wipeWebrtcData}
            callId={this.props.webrtc && this.props.webrtc.callId}
            roomName={this.props.webrtc && this.props.webrtc.room}
            token={this.props.webrtc && this.props.webrtc.twilioToken}
            isIncomingCall={Object.keys(this.props.webrtc).length}
            webrtc={this.props.webrtc}
          /> */}
        {/* </MatUIModal> */}
        {/* <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} /> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    _userProfile: state.UserProfile,
    leftSideChatLogs: state.UserProfile.leftSideChatLogs,
    UserSelectedToChat: state.UserSelectedToChat.UserSelectedToChat,
    CoinBalance: state.UserProfile.CoinBalance,
    unreadChatCount: state.UserSelectedToChat.unreadChatCount,
    friendReqCount: state.Friends.FriendRequest,
    // allChats: state.allChats,
    // userSelectedToChat: state.userSelectedToChat,
    // allMessagesForSelectedUser: state.allMessagesForSelectedUser,
    // webrtc: state.webrtc,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    storeCoinBalance: (coins) => dispatch(storeCoins(coins)),
    leftSideChatLogsFN: (allChatLogs) => dispatch(leftSideChatLogs(allChatLogs)),
    boostActivatedSessionIncomingData: (data) => dispatch(boostActivatedSessionIncomingData(data)),
    __updateMqttMessage: (data) => dispatch(__updateMqttMessage(data)),
    _storeUnreadChatCount: (count) => dispatch(actionChat.storeUnreadChatCount(count)),
    addNewChatToExistingObject: (msg) => dispatch(actionChat.addNewChatToExistingObject(msg)),
    storeFriendRequestCount: (count) => dispatch(FRIEND_REQUEST_COUNT_FUNC(count)),
    UPDATE_ACKNOWLEDGEMENT: (msgId, status) => dispatch(updateAcknowledgement(msgId, status)),
    matchFoundFunc: (obj) => dispatch(matchFound(obj)),
    userSelectedToChat: (user) => dispatch(actions.userSelectedToChat(user)),
    selectedProfileForChat: (obj) => dispatch(SELECTED_PROFILE_ACTION_FUN("selectedProfile", obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MQTTHOC);
