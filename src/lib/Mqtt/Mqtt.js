import Paho from "paho-mqtt";
import MQTT from "paho-mqtt";

// Redux Components
import { getCookie } from "../session";
import { keys } from "../keys";

window.Paho = { MQTT };

let client = new Paho.Client(keys.mqtt, "clientID-" + new Date().getTime().toString());

let mqttStatusConnected = true;
let x = 0;

const hasUndefined = (target) => {
  for (var member in target) {
    if (target[member] === undefined || target[member] === null) return true;
  }
  return false;
};

const options = {
  userName: keys.mqttUserName,
  password: keys.mqttPassword,
  useSSL: true,
  keepAliveInterval: 10,
  mqttVersion: 3,
  reconnect: true,
  cleanSession: true,
  onSuccess: (message) => {
    console.log("mqtt connected");
    setTimeout(() => {
      client.subscribe("Message/" + getCookie("uid"));
      client.subscribe("Acknowledgement/" + getCookie("uid"));
      client.subscribe("Calls/" + getCookie("uid"));
      client.subscribe("UserUpdates/" + getCookie("uid"));
      client.subscribe("FetchMessages/" + getCookie("uid"));
      client.subscribe("Friend/" + getCookie("uid"));
      client.subscribe("OnlineStatus/" + getCookie("uid"));
      client.subscribe(getCookie("uid"));
      publish("Login");
    }, 1500);
  },
  onFailure: (message) => {
    try {
      console.log("[LOGIN TRY]");
      setTimeout(() => {
        client.subscribe("Message/" + getCookie("uid"));
        client.subscribe("Acknowledgement/" + getCookie("uid"));
        client.subscribe("Calls/" + getCookie("uid"));
        client.subscribe("UserUpdates/" + getCookie("uid"));
        client.subscribe("FetchMessages/" + getCookie("uid"));
        client.subscribe("Friend/" + getCookie("uid"));
        client.subscribe("OnlineStatus/" + getCookie("uid"));
        client.subscribe(getCookie("uid"));
        publish("Login");
      }, 1500);
    } catch (e) {
      console.log("[LOGIN CATCH]", e);
      console.log("Connection failed:.", message);
    }
  },
};

const logs = {
  invocationContext: {},
  timeout: 10000000,
  onSuccess: (message) => {
    console.log("success", message);
  },
  onFailure: (message) => {
    console.log("error", message);
  },
};

export const Mqtt = (callbackOnMessage) => {
  console.log("value of x: " + x);
  if (mqttStatusConnected === true) {
    // to ensure it fires only once

    if (!client.isConnected()) {
      client.connect(options);
      mqttStatusConnected = false;
    }
  }

  client.onMessageArrived = (message) => {
    let mqttData = JSON.parse(message.payloadString);
    return callbackOnMessage ? callbackOnMessage({ payload: mqttData, topic: message.topic }) : "";
  };
};

export const publishTopic = (key, publishObj = {}) => {
  try {
    console.log("Publishing On:: ", `Calls/${key}`, publishObj);
    client.publish(`Calls/${key}`, JSON.stringify(publishObj), 0, false);
  } catch (ex) {
    console.log("Error In Call Publish  Message:: ", ex);
  }
};

export const subscribeToUser = (targetUserId) => {
  console.log("subscribe to user", targetUserId);
  client.subscribe("OnlineStatus/" + targetUserId);
};

export const unsubscribeToUser = (targetUserId) => {
  console.log("unsubscribeToUser to user", targetUserId);
  client.subscribe("OnlineStatus/" + targetUserId);
};

const publish = (type) => {
  let publishObj = {};
  let uid = getCookie("uid");
  if (type === "Login") {
    publishObj = {
      status: 1,
      onlineStatus: 1,
      userId: uid,
      _id: uid,
      lastSeenEnabled: true,
      lastOnline: new Date().getTime(),
    };
    if (hasUndefined(publishObj)) {
      console.log("cannot publish onlineStatus on undefined");
    } else {
      client.publish(`OnlineStatus/${uid}`, JSON.stringify(publishObj), 0, true);
    }
  }
};

// export const sendMessageUsingMQTT = (senderId, userImage, chatId, isMatchedUser, payload, timestamp, name) => {
export const sendMessageUsingMQTT = (obj) => {
  console.log("obj ->", obj);
  if (
    obj.from !== undefined &&
    obj.from !== undefined &&
    obj.receiverIdentifier !== undefined &&
    obj.to !== undefined &&
    obj.from !== null &&
    obj.from !== null &&
    obj.receiverIdentifier !== null &&
    obj.to !== null
  ) {
    client.publish(`Message/${obj.to}`, JSON.stringify(obj), 1, false);
  } else {
    console.log("cannot publish message over MQTT");
  }
};

export const ackOfMessage = (receiverId, senderId, msgId, status) => {
  console.log("sending ack from my side");
  let arr = [];
  arr.push(msgId);
  if (receiverId !== undefined || senderId !== undefined || msgId !== null || status !== null) {
    console.log("receiverId", receiverId, "senderId", senderId);
    let obj2 = {};
    obj2["from"] = senderId;
    obj2["msgIds"] = arr;
    obj2["to"] = receiverId;
    obj2["status"] = parseInt(status);
    if (status === 2 || status === "2") {
      obj2["deliveryTime"] = new Date().getTime();
      console.log("###-----1");
    } else if (status === 3 || status === "3") {
      console.log("###-----2");
      obj2["readTime"] = new Date().getTime();
    }
    if (hasUndefined(obj2)) {
      console.log("cannot publish ackOfMessage");
    } else {
      console.log("ack published ->", JSON.stringify(obj2));
      client.publish("Acknowledgement/" + receiverId, JSON.stringify(obj2), 2, false);
    }
  }
};

export const LogoutMQTTCalled = (userID) => {
  if (client.isConnected()) {
    let publishObj = {};
    publishObj["status"] = 0;
    publishObj["onlineStatus"] = 0;
    publishObj["_id"] = userID;
    publishObj["timestamp"] = new Date().getTime();
    publishObj["userId"] = userID;
    publishObj["lastSeenEnabled"] = true;
    publishObj["lastOnline"] = new Date().getTime();
    if (hasUndefined(publishObj)) {
      console.log("cannot logout on undefined object LogoutMQTTCalled");
    } else {
      try {
        console.log("[TRY]");
        client.unsubscribe("Message/" + userID, logs);
        client.unsubscribe("Acknowledgement/" + userID, logs);
        client.unsubscribe("Calls/" + userID, logs);
        client.unsubscribe("UserUpdates/" + userID, logs);
        client.unsubscribe("FetchMessages/" + userID, logs);
        client.unsubscribe(userID, logs);
        client.publish(`OnlineStatus/${userID}`, JSON.stringify(publishObj), 0, true);
        console.log("LogoutMQTTCalled(id)", userID);
        setTimeout(() => {
          client.disconnect();
        }, 300);
      } catch (e) {
        console.log("[CATCH]", e);
        client.unsubscribe("Message/" + userID, logs);
        client.unsubscribe("Acknowledgement/" + userID, logs);
        client.unsubscribe("Calls/" + userID, logs);
        client.unsubscribe("UserUpdates/" + userID, logs);
        client.unsubscribe("FetchMessages/" + userID, logs);
        client.unsubscribe(userID, logs);
        client.publish(`OnlineStatus/${userID}`, JSON.stringify(publishObj), 0, true);
        console.log("LogoutMQTTCalled(id)", userID);
        setTimeout(() => {
          client.disconnect();
        }, 300);
      }
    }
  }
};
