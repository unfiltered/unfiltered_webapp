import axios from "axios";
import { getUrl, postWithToken, patchWithToken, get, putWithToken, deleteReq, mediaPost } from "../lib/request";
import { setCookie, getCookie } from "../lib/session";

// Reqest OTP Module
export const requestOtp = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/requestOtp", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const uploadMedia = (data) => {
  return new Promise((resolve, reject) => {
    mediaPost("/upload", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Verify OTP Module
export const verifyOtp = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/verificaitonCode", data)
      .then((data) => {
        setCookie("token", data.data.data.token);
        setCookie("uid", data.data.data._id);
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Email-ID Verification Module
export const verifyemail = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/emailIdExistsVerificaiton", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Post User Profile Module
export const verifyprofile = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/profile", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get User Preferences Module
export const preference = (data) => {
  return new Promise((resolve, reject) => {
    get("/preferences", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Update User Preferences Module
export const UserdataPrefences = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/preferences", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get User Profile Module
export const UserProfile = (data) => {
  return new Promise((resolve, reject) => {
    get("/profile", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// User Profile Module
export const UserNewData = (data) => {
  return new Promise((resolve, reject) => {
    putWithToken("/profile", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Phone Exits Checking Module
export const PhoneExistVerify = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/phoneNumberExistsVerificaton", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Facebook login Module
export const facebooklogin = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/faceBooklogin", data)
      .then((data) => {
        setCookie("token", data.data.data.token);
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Delete User Profile Module
export const Profiledelete = (data) => {
  return new Promise((resolve, reject) => {
    deleteReq("/profile", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// SearchResult ( Nearby People ) Module
export const SearchUser = (data) => {
  return new Promise((resolve, reject) => {
    get("/searchResult", data)
      .then((data) => {
        // console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// FriendRequest Module
export const friendreq = (data) => {
  return new Promise((resolve, reject) => {
    get("/friendRequest", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// FriendList Module
export const friendlist = (data) => {
  return new Promise((resolve, reject) => {
    get("/friends", data)
      .then((data) => {
        // console.log("success", data);÷
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Past Dates Module
export const pastdatelist = (data) => {
  return new Promise((resolve, reject) => {
    get("/pastDates", data)
      .then((data) => {
        // console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

export const getAllFriends = (data) => {
  return new Promise((resolve, reject) => {
    get("/friends", data)
      .then((data) => {
        // console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

export const getAllFriendsReq = (data) => {
  return new Promise((resolve, reject) => {
    get("/friendRequest", data)
      .then((data) => {
        // console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Pending Dates Module
export const pendingdatelist = (data) => {
  return new Promise((resolve, reject) => {
    get("/currentDates", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// friendRequestResponse Module
export const friendreqres = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/friendRequestResponse", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Report User Module
export const reportuser = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/reportUser", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// To Get OtherUser Profile Module
export const Otheruserprofile = (data) => {
  return new Promise((resolve, reject) => {
    get("/profileById?targetUserId=" + data.targetUserId)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// My UserProfile Liked by OtherUser Module
export const likeby = (data) => {
  return new Promise((resolve, reject) => {
    get("/likesBy", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Online User Module
export const onlineuser = (data) => {
  return new Promise((resolve, reject) => {
    get("/onlineUsers", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// SuperLiked By Module
export const superlikeby = (data) => {
  return new Promise((resolve, reject) => {
    get("/supperLikesBy", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Recent Visitor Module
export const recentvisitor = (data) => {
  return new Promise((resolve, reject) => {
    get("/recentVisitors", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// My Likes Module
export const mylikes = (data) => {
  return new Promise((resolve, reject) => {
    get("/myLikes", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// My SuperLiked Module
export const mysuperlike = (data) => {
  return new Promise((resolve, reject) => {
    get("/mySupperLikes", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Passed by User Module
export const passedby = (data) => {
  return new Promise((resolve, reject) => {
    get("/myUnLikes", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

export const thumbsUpMe = (data) => {
  return new Promise((resolve, reject) => {
    get("/ProfileLikeBy", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

export const iThumbsUpped = (data) => {
  return new Promise((resolve, reject) => {
    get("/MyProfileLike", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

export const history = (data) => {
  return new Promise((resolve, reject) => {
    get("/history", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// Like a Nearby People Module
export const likepeople = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/like ", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// SuperLike a Nearby People Module
export const superlikepeople = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/supperLike", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err", err);
        return reject(err);
      });
  });
};

// Buy New Plan Module
export const newplan = (data) => {
  return new Promise((resolve, reject) => {
    get("/plan", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Buy New Coins Module
export const newcoins = (data) => {
  return new Promise((resolve, reject) => {
    get("/coinPlans ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Coin Spent Histroy Module
export const coinhistory = (data) => {
  return new Promise((resolve, reject) => {
    get("/coinHistory ", data)
      .then((data) => {
        // console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// My UnLike User Module
export const myunlikeuser = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/unLike ", data)
      .then((data) => {
        console.log("success", data);
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// To Search a Friend by Seeme ID Module
export const searchseemefriend = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/searchFriend ", data)
      .then((data) => {
        console.log("Searchresult this");
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// To Add new Friend Module
export const addfriend = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/friendRequest ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        console.log("err==", err);
        return reject(err);
      });
  });
};

// To update a Findmate ID of User
export const updateseemeid = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/findMateId ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get a Findmate ID of User
export const getseemeid = (data) => {
  return new Promise((resolve, reject) => {
    get("/findMateId ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Search Prefernce // Filter User
export const searchprefence = (data) => {
  return new Promise((resolve, reject) => {
    get("/searchPreferences ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Search Prefernce Post // Filter User
export const searchprefencepost = (data) => {
  return new Promise((resolve, reject) => {
    postWithToken("/searchPreferences ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get Reason to Block a User
export const Blockuserreason = (data) => {
  return new Promise((resolve, reject) => {
    get("/reportUserReasons ", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const MatchFound = (data) => {
  return new Promise((resolve, reject) => {
    get("/match", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getAllChats = (pgNo) => {
  return new Promise((resolve, reject) => {
    get(`/Chats?pageNo=${pgNo}`)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const getAllChatsForSingleUser = (chatId, timeStamp, pageSize) => {
  return new Promise((resolve, reject) => {
    // get(`/Messages/${chatId}/${timeStamp}/${pageSize}`)
    get(`/Messages?chatId=${chatId}&timestamp=${timeStamp}&pageSize=${pageSize}`)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const messageWithoutMatch = (senderId, payload, type, id, userImage, name) => {
  let data = {
    senderId: senderId,
    payload: payload,
    type: type,
    id: id,
    userImage: userImage,
    name: name,
  };
  return new Promise((resolve, reject) => {
    postWithToken("/messageWithoutMatch", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const searchUserByPagination = (start, end) => {
  return new Promise((resolve, reject) => {
    get(`/searchResult?offset=${start}&limit=${end}`)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const coinChat = (obj) => {
  return new Promise((resolve, reject) => {
    // postWithToken("/messageWithoutMatch", obj)
    postWithToken("/messageWithoutMatch", obj)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const blockUser = (userId) => {
  let data = { targetUserId: userId };
  return new Promise((resolve, reject) => {
    patchWithToken("/block", data)
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const deleteSelectedMessages = (messageIds) => {
  let msg = encodeURIComponent(JSON.stringify(messageIds));
  return new Promise((resolve, reject) => {
    deleteReq(`/Messages/selected/${msg}`)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const unMatchUser = (targetUserId) => {
  let data = {
    targetUserId: targetUserId,
  };
  return new Promise((resolve, reject) => {
    postWithToken("/unMatch", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

export const deleteChatTotally = (chatId) => {
  let cid = chatId.toString();
  let data = {
    data: {
      chatId: cid,
    },
  };
  return new Promise((resolve, reject) => {
    deleteReq("/Chats", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Get Active Plan Of User
// http://178.62.59.128:1008/v1/mySubscription
export const getActivePlan = (data) => {
  return new Promise((resolve, reject) => {
    get("/mySubscription", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// To Activate Boost for 5 min
export const activateBoost = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/boost", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Get Boost Details
export const getBoostDetails = (data) => {
  return new Promise((resolve, reject) => {
    get("/boost", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Get ReWind Back User Details
export const ReWindUser = (data) => {
  return new Promise((resolve, reject) => {
    patchWithToken("/rewind", data)
      .then((data) => {
        return resolve(data);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

// Get Refresh Token Details
export const refreshToken = (data) => {
  return axios.get(getUrl("/refreshToken"), {
    headers: {
      authorization: getCookie("token", ""),
      refreshToken: getCookie("ref"),
      "Content-Type": "application/json",
      lang: "en",
    },
  });
};
