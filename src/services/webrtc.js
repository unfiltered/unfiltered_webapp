import { get } from "../lib/request";

// To Get User Profile Module
export const getIceServers = (data) => {
    return new Promise((resolve, reject) => {
        get("/iservers", data)
            .then((data) => {
                return resolve(data);
            })
            .catch((err) => {
                return reject(err);
            });
    });
};