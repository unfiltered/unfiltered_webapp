import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import rootReducer from "./reducer/index";
// import rootReducer, { initialState } from "./reducer/index";
// import { rootSaga } from './saga/index'

// import { persistReducer } from "redux-persist";
// import { persistStore, persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage";

const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV === "production" || process.env.NODE_ENV === "development") {
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

// const persistConfig = {
//   key: "root",
//   storage
// };

// const persistedReducer = persistReducer(persistConfig, rootReducer);

function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, bindMiddleware([sagaMiddleware]));

  // persistStore(store);

  // store.runSagaTask = () => {
  // store.sagaTask = sagaMiddleware.run(rootSaga)
  // }

  // store.runSagaTask()
  return store;
}

export default configureStore;
