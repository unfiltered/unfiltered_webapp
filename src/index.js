// Main React Components
import React from "react";
import ReactDOM from "react-dom";
import "./App.scss";
import Index from "./Routes/index";
import "bootstrap/dist/css/bootstrap.min.css";

// MultiLang Support Module
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import * as serviceWorker from "./serviceWorker";
import ru from "react-intl/locale-data/ru";

// Store Cmponents Module
import { Provider } from "react-redux";
import createStore from "./store";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

const theme = createMuiTheme({
  typography: {
    fontFamily: ["Product Sans"].join(","),
  },
});

addLocaleData(en);
addLocaleData(ru);

const store = createStore();

ReactDOM.hydrate(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Index store={store} />
    </Provider>
  </ThemeProvider>,

  document.getElementById("root")
);

serviceWorker.register();
