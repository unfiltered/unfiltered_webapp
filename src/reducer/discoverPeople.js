import * as actionTypes from "../actions";

export const initialState = {
  discoverPeopleList: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.DISCOVER_PEOPLE_LIST:
      return {
        ...state,
        discoverPeopleList: action.data
      };

    default:
      return state;
  }
};

export default reducer;
