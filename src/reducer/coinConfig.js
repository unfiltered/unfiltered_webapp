import * as actionTypes from "../actions";

export const initialState = {
  CoinConfig: "",
  ProUserPlans: "",
  indirectRouting: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_COIN_CONFIG:
      return {
        ...state,
        CoinConfig: action.data
      };
    case actionTypes.GET_PRO_USER_PLAN:
      return {
        ...state,
        ProUserPlans: action.data
      };
      case actionTypes.ROUTE_AFTER_PAYMENT:
        return {
          ...state,
          indirectRouting: action.data
      };
    default:
      return state;
  }
};

export default reducer;
