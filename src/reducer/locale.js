import { LOCALE_SET } from "../actions/index";

const localeState = {
  lang: localStorage.getItem("lang") || "en"
};

export default function locale(state = localeState, action = {}) {
  switch (action.type) {
    case LOCALE_SET:
      localStorage.setItem("lang", action.lang);
      return { lang: action.lang };
    default:
      return state;
  }
}
