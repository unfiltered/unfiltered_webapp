import * as actionTypes from "../actions";

export const mqttData = {
  mqttData: ""
};

const reducer = (state = mqttData, action) => {
  switch (action.type) {
    case actionTypes.MQTT_DATA:
      return {
        ...state,
        mqttData: action.data
      };

    default:
      return state;
  }
};

export default reducer;
