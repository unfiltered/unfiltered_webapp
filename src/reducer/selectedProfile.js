import * as actionTypes from "../actions";
import { updateObject } from "../shared/utility";

export const initialState = {
  selectedProfile: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SELECTED_PROFILE_ACTION:
      return updateObject(state, {
        selectedProfile: {
          ...state.selectedProfile,
          [action.datatype]: action.data
        }
      });
    default:
      return state;
  }
};

export default reducer;
