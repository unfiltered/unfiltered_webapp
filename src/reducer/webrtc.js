import * as actionTypes from "../actions";

export const webrtcData = {
};

const reducer = (state = webrtcData, action) => {
    switch (action.type) {
        case actionTypes.MQTT_MSG_UPDATE:
            return {
                ...state,
                lastMqttMsg: action.data
            }
        case actionTypes.WEBRTC_CALL_INIT:
            console.log("Inside ", action);
            return {
                ...state,
                callInit: action.data,
                lastMqttMsg: {
                    payload: {

                    }
                }
            }

        default:
            return state;
    }
};

export default reducer;
