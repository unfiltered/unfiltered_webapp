import * as actionTypes from "../actions";

export const initialState = {
  UserSelectedToChat: {},
  SingleUserAllMessages: [],
  chatStarted: false,
  imageUploaded: "",
  getLatestTimeStamp: "",
  serverImageFolderLocation: "https://fetch.cmeedating.com/",
  doubleTickAck: "",
  onSearchSendLocation: "",
  showCoinToast: false,
  readOnlyUser: false,
  unreadChatCount: 0,
  chatDrawer: false,
  lastIndexTimeStamp: "",
  // "/var/www/html/chatdocs/" ||
};

const removeDefaultMsg = (messages) => {
  return messages.filter((k, i) => {
    return k.payload !== "3embed test";
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_ACKNOWLEDGEMENT: {
      let allMessagesOfSingleUserUpdated = [...state.SingleUserAllMessages];
      let filterMessagesWithStatusNotEqualToThree = allMessagesOfSingleUserUpdated.filter((k) => k.status !== 3);
      for (let i = 0; i < filterMessagesWithStatusNotEqualToThree.length; i++) {
        filterMessagesWithStatusNotEqualToThree[i].status = action.status;
      }
      return {
        ...state,
        SingleUserAllMessages: allMessagesOfSingleUserUpdated,
      };
    }

    case actionTypes.STORE_UNREAD_CHAT_COUNT: {
      return {
        ...state,
        unreadChatCount: action.data,
      };
    }

    case actionTypes.CHAT_DRAWER: {
      return {
        ...state,
        chatDrawer: action.data,
      };
    }

    case actionTypes.JUST_VIEW_PROFILE_ACTIVE: {
      return {
        ...state,
        readOnlyUser: true,
      };
    }

    case actionTypes.JUST_VIEW_PROFILE_REMOVE: {
      return {
        ...state,
        readOnlyUser: false,
      };
    }

    case actionTypes.DISMISS_COIN_CHAT_TOAST: {
      return {
        ...state,
        showCoinToast: false,
      };
    }
    case actionTypes.SHOW_COIN_CHAT_TOAST: {
      return {
        ...state,
        showCoinToast: true,
      };
    }
    case actionTypes.USER_SELECTED_TO_CHAT:
      return {
        ...state,
        UserSelectedToChat: action.data,
      };
    case actionTypes.SELECTED_USER_ALL_MESSAGES:
      let allMessages = [...action.data];
      return {
        ...state,
        SingleUserAllMessages: removeDefaultMsg(allMessages),
      };
    case actionTypes.APPEND_MESSAGE_EXISTING_CHAT:
      // let prevData = [...state.SingleUserAllMessages, action.data];
      let prevData = [action.data, ...state.SingleUserAllMessages];
      // prevents mqtt data being added on refresh of second client(user)
      const uniqueArray = prevData.filter((thing, index) => {
        return (
          index ===
          prevData.findIndex((obj) => {
            return JSON.stringify(obj) === JSON.stringify(thing);
          })
        );
      });
      return {
        ...state,
        SingleUserAllMessages: uniqueArray,
        getLatestTimeStamp: action.data.timestamp, //action.data
      };
    case actionTypes.CHAT_STARTED: {
      return {
        ...state,
        chatStarted: true,
      };
    }
    case actionTypes.CHAT_ENDED: {
      return {
        ...state,
        chatStarted: false,
      };
    }
    case actionTypes.UPLOAD_IMAGE: {
      return {
        ...state,
        imageUploaded: action.data,
      };
    }

    case actionTypes.DELETE_ALL_MESSAGES_FOR_SINGLE_USER: {
      return {
        ...state,
        SingleUserAllMessages: [],
      };
    }
    case actionTypes.DOUBLE_TICK: {
      return {
        ...state,
        doubleTickAck: action.data,
      };
    }
    case actionTypes.ON_SEARCH_SEND_LOCATION: {
      return {
        ...state,
        onSearchSendLocation: action.data,
      };
    }
    default:
      return state;
  }
};

export default reducer;
