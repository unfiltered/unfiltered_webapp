import * as actionTypes from "../actions";
import { updateObject } from "../shared/utility";
import { setCookie, getCookie } from "../lib/session";

export const initialState = {
  UserProfile: {
    AgeDetails: "",
    DistanceDetails: "",
  },
  leftSideChatLogs: [],
  filterByAgeValue: { min: "", max: "" },
  filterByAgeArr: ["", ""],
  filterByHeightValue: { min: "", max: "" },
  filterByHeightArr: ["", ""],
  filterByDistanceValue: { min: "", max: "" },
  filterByDistanceArr: ["", ""],
  CoinBalance: "",
  matchFound: undefined,
  urlAfterCropping: "",
  profileObjectAfterCropped: "",
  currencySymbol: "",
  UserData: {
    locations: [],
    currentLocation: "",
    lat: "",
    lan: "",
    otp: "",
  },
};

let fn = (arr, mn, mx) => {
  arr[0] = mn;
  arr[1] = mx;
  return arr;
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_PROFILE_ACTION:
      return updateObject(state, {
        UserProfile: {
          ...state.UserProfile,
          userId: getCookie("uid"),
          [action.datatype]: action.data,
        },
      });
    case actionTypes.AGE_DETAILS: {
      return {
        ...state,
        UserProfile: { ...state.UserProfile, AgeDetails: action.data },
      };
    }
    case actionTypes.DETAILS_DETAILS: {
      return {
        ...state,
        UserProfile: { ...state.UserProfile, DistanceDetails: action.data },
      };
    }
    case actionTypes.IMAGE_CHANGE_ON_PROFILE_IMG_UPDATE: {
      return {
        ...state,
        UserProfile: {
          ...state.UserProfile,
          UserProfilePic: action.data,
        },
      };
    }
    case actionTypes.LEFT_SIDE_MESSAGE_LOGS: {
      let allChatsLogs = [...action.data].filter((k) => k.payload !== "3embed test");
      return {
        ...state,
        leftSideChatLogs: allChatsLogs,
      };
    }
    case actionTypes.DELETE_ALL_MESSAGES_FOR_SINGLE_USER: {
      let prevData = [...state.leftSideChatLogs].reverse();
      let index = prevData.findIndex((k) => k.chatId === action.data);
      prevData.splice(index, 1);
      return {
        ...state,
        leftSideChatLogs: prevData,
      };
    }
    case actionTypes.FILTER_BY_AGE_OBJ: {
      let filteredAges = fn(state.filterByAgeArr, action.min, action.max);
      return {
        ...state,
        filterByAgeValue: {
          ...state.filterByAgeValue,
          min: action.min,
          max: action.max,
        },
        filterByAgeArr: filteredAges,
      };
    }
    case actionTypes.FILTER_BY_HEIGHT_OBJ: {
      let filteredHeight = fn(state.filterByHeightArr, action.min, action.max);
      return {
        ...state,
        filterByHeightValue: {
          ...state.filterByHeightValue,
          min: action.min,
          max: action.max,
        },
        filterByHeightArr: filteredHeight,
      };
    }
    case actionTypes.FILTER_BY_DISTANCE_OBJ: {
      let filteredDistance = fn(state.filterByDistanceArr, action.min, action.max);
      return {
        ...state,
        filterByDistanceValue: {
          ...state.filterByDistanceValue,
          min: action.min,
          max: action.max,
        },
        filterByDistanceArr: filteredDistance,
      };
    }
    case actionTypes.STORE_COINS: {
      return {
        ...state,
        CoinBalance: action.data,
      };
    }
    case actionTypes.COIN_CHAT: {
      return {
        ...state,
        CoinBalance: state.CoinBalance - action.data,
      };
    }
    case actionTypes.SET_CURRENCY_SYMBOL: {
      return {
        ...state,
        currencySymbol: action.data,
      };
    }
    case actionTypes.ADD_COINS_TO_EXISTING_COINS: {
      return {
        ...state,
        CoinBalance: parseInt(state.CoinBalance) + parseInt(action.data),
      };
    }

    /** map thing */
    case actionTypes.SET_DEFAULT_LOCATION: {
      return {
        ...state,
        UserData: {
          ...state.UserData,
          currentLocation: action.data,
          // locations: [...state.UserData.locations, action.data]
        },
      };
    }

    case actionTypes.SET_LATITUDE: {
      return {
        ...state,
        UserData: {
          ...state.UserData,
          lat: action.data,
        },
      };
    }

    case actionTypes.SET_URL_AFTER_CROPPING: {
      return {
        ...state,
        urlAfterCropping: action.data,
      };
    }
    case actionTypes.SET_PROFILE_OBJECT: {
      return {
        ...state,
        profileObjectAfterCropped: action.data,
      };
    }
    case actionTypes.SET_LONGITUDE: {
      return {
        ...state,
        UserData: {
          ...state.UserData,
          lan: action.data,
        },
      };
    }

    case actionTypes.SET_OTP: {
      return {
        ...state,
        UserData: {
          ...state.UserData,
          otp: action.data,
        },
      };
    }

    case actionTypes.MATCH_FOUND: {
      return {
        ...state,
        matchFound: action.data,
      };
    }

    case actionTypes.ADD_NEW_LOCATION_TO_SEARCH: {
      let addNewLocations = state.UserData.locations;

      if (addNewLocations.length > 4) {
        addNewLocations.pop();
        addNewLocations.push(...action.data);
      } else {
        addNewLocations.push(...action.data);
      }
      const uniqueArray = addNewLocations.filter((thing, index) => {
        return (
          index ===
          addNewLocations.findIndex((obj) => {
            return JSON.stringify(obj) === JSON.stringify(thing);
          })
        );
      });

      setCookie("locations", JSON.stringify(uniqueArray));
      return {
        ...state,
        UserData: {
          ...state.UserData,
          locations: [...uniqueArray],
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
