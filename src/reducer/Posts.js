import * as actionTypes from "../actions";

export const initialState = {
  Posts: [],
  currentText: "",
  display: false,
};

// export const M_STORE_ALL_POSTS = "M_STORE_ALL_POSTS";
// export const M_ADD_NEW_POST = "M_ADD_NEW_POST";
// export const M_DELETE_POST = "M_DELETE_POST";

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.M_STORE_ALL_POSTS:
      return {
        ...state,
        Posts: action.data,
      };
    case actionTypes.M_POST_UPLOADING_STATUS:
      return {
        ...state,
        currentText: action.text,
        display: action.boolean,
      };
    case actionTypes.M_ADD_NEW_POST:
      let allPosts = [...state.Posts];
      allPosts.unshift(action.data);
      return {
        ...state,
        Posts: allPosts,
      };
    case actionTypes.M_DELETE_POST:
      let newPosts = [...state.Posts];
      let index = newPosts.findIndex((k) => k.postId === action.data);
      newPosts.splice(index, 1);
      return {
        ...state,
        Posts: newPosts,
      };
    case actionTypes.M_LIKE_DISLIKE:
      let _allPosts = [...state.Posts];
      let _index = _allPosts.findIndex((k) => k.postId === action.data);
      let _prevLiked = _allPosts[_index].liked;
      _allPosts[_index].liked = !_prevLiked;
      if (_allPosts[_index].liked) {
        _allPosts[_index].likeCount = ++_allPosts[_index].likeCount;
      } else {
        _allPosts[_index].likeCount = --_allPosts[_index].likeCount;
      }
      return {
        ...state,
        Posts: _allPosts,
      };
    case actionTypes.M_ADD_COMMENT:
      let oldData = [...state.Posts];
      let __index = oldData.findIndex((k) => k.postId === action.data);
      oldData[__index].commentCount += 1;
      return {
        ...state,
        Posts: oldData,
      };
    default:
      return state;
  }
};

export default reducer;
