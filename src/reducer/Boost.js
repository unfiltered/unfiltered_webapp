import * as actionTypes from "../actions";

export const initialState = {
  BoostProfile: "",
  BoostDetailsOnRefresh: {},
  BoostActivatedSessionIncomingData: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.BOOST_ACTIVED:
      return {
        ...state,
        BoostProfile: action.data,
      };
    case actionTypes.BOOST_DETAILS_ON_REFRESH:
      return {
        ...state,
        BoostDetailsOnRefresh: action.data,
      };
    case actionTypes.BOOST_ACTIVED_SESSION_INCOMING_DATA:
      return {
        ...state,
        BoostActivatedSessionIncomingData: action.data,
      };

    default:
      return state;
  }
};

export default reducer;
