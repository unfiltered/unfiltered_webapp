import * as actionTypes from "../actions";

export const initialState = {
  FriendRequest: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FRIEND_REQUEST_COUNT:
      return {
        ...state,
        FriendRequest: action.data,
      };
    default:
      return state;
  }
};

export default reducer;
