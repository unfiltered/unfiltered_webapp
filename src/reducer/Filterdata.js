import * as actionTypes from "../actions";
import { updateObject } from "../shared/utility";

export const initialState = {
  selectedFilter: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SELETED_FILTER:
      return updateObject(state, {
        selectedFilter: {
          ...state.selectedFilter,
          [action.datatype]: action.data
        }
      });
    default:
      return state;
  }
};

export default reducer;
