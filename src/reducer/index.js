import { combineReducers } from "redux";

import Main from "./Main";
import locale from "./locale";
import UserProfile from "./UserProfile";
import selectedProfile from "./selectedProfile";
import ProUser from "./ProUser";
import UserSelectedToChat from "./UserSelectedToChat";
import discoverPeople from "./discoverPeople";
import Boost from "./Boost";
import UserFilter from "./Filterdata";
import CoinConfig from "./coinConfig";
import mqttData from "./mqtt-data";
import webrtc from "./webrtc";
import Posts from "./Posts";
import Friends from "./Friends";

const rootReducer = combineReducers({
  Main,
  locale,
  UserProfile,
  selectedProfile,
  UserSelectedToChat,
  ProUser,
  discoverPeople,
  Boost,
  UserFilter,
  CoinConfig,
  mqttData,
  Posts,
  webrtc,
  Friends,
});

export default rootReducer;
