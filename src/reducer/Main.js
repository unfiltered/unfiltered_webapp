import * as actionTypes from "../actions";
import { updateObject } from "../shared/utility";

export const initialState = {
  UserData: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.MOBILE_LOGIN_ACTION:
      return updateObject(state, {
        UserData: {
          ...state.UserData,
          [action.datatype]: action.data,
        },
      });
    case actionTypes.CAROUSEL_INDEX:
      return updateObject(state, {
        UserData: {
          ...state.UserData,
          carouselIndex: action.data,
        },
      });
    default:
      return state;
  }
};

export default reducer;
