import * as actionTypes from "../actions";

export const initialState = {
  ProUserDetails: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHECK_IF_PRO_USER:
      return {
        ...state,
        ProUserDetails: action.data
      };
    default:
      return state;
  }
};

export default reducer;
