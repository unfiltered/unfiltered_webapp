// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// materail-UI Components
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import SwipeableViews from "react-swipeable-views";

function TabContainer(props) {
  return (
    <Typography
      component="div"
      style={{
        padding: "24px",
        minHeight: "670px",
        overflowY: "scroll",
        height: "500px"
      }}
    >
      {props.children}
    </Typography>
  );
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  }
});

class SimpleTabs extends Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs, tabContent } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            className="inner-tabs"
            value={value}
            onChange={this.handleChange}
          >
            {tabs.map((tab, key) => (
              <Tab key={key} style={{ color: "#000" }} label={tab.label} />
            ))}
          </Tabs>
        </AppBar>

        {tabContent.map((tabContent, key) =>
          key === value ? (
            <TabContainer key={key}>{tabContent.content}</TabContainer>
          ) : (
            ""
          )
        )}
      </div>
    );
  }
}

SimpleTabs.propTypes = {
  classes: PropTypes.object.isRequired
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

export default withStyles(styles)(SimpleTabs);
