import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = (theme) => ({
  root: {
    backgroundColor: "white",
    width: "inherit",
  },
  tabsRoot: {
    borderBottom: "none",
  },
  tabsIndicator: {
    backgroundColor: "#f50057",
  },
  tabRoot: {
    textTransform: "initial",
    fontWeight: 100,
    color: "#C8CFE0",
    fontSize: "16px",
    fontFamily: "Product Sans",
    "&:hover": {
      color: "#f50057",
    },
    "&$tabSelected": {
      color: "#484848",
      fontFamily: "Product Sans Bold",
      fontWeight: 900,
      fontSize: "16px",
      borderBottom: "1px solid #f50057",
    },
    "&:focus": {
      color: "#f50057",
      fontSize: "16px",
      fontFamily: "Product Sans",
    },
  },
});

class FullWidthTabs extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.props.handleChangeIndex(value);
    this.setState({ value });
  };

  handleChangeIndex = (index) => {
    this.setState({ value: index });
    this.props.handleChangeIndex(index);
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <div className={classes.root}>
        <AppBar elevation={0} position="static" color="white">
          <Tabs
            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="secondary"
            textColor="secondary"
            variant="fullWidth"
          >
            <Tab classes={{ root: classes.tabRoot, selected: classes.tabSelected }} label={this.props.label1 ? this.props.label1 : "Active Chats"} />
            <Tab classes={{ root: classes.tabRoot, selected: classes.tabSelected }} label={this.props.label2 ? this.props.label2 : "Matches"} />
          </Tabs>
        </AppBar>
        <SwipeableViews axis={theme.direction === "rtl" ? "x-reverse" : "x"} index={this.state.value} onChangeIndex={this.handleChangeIndex}>
          <TabContainer dir={theme.direction}>{this.props.chats}</TabContainer>
          <TabContainer dir={theme.direction}>{this.props.match}</TabContainer>
        </SwipeableViews>
      </div>
    );
  }
}

FullWidthTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(FullWidthTabs);
