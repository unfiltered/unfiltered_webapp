// Main React Components
import React from "react";

// Scss
import "./WebTabs.scss";

// Material UI Components
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

function TabContainer(props) {
  return <Typography component="div">{props.children}</Typography>;
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  tabsRoot: {
    borderBottom: "2px solid #d1d1d1",
    borderRight: "2px solid #d1d1d1",
    borderLeft: "2px solid #d1d1d1",
    backgroundColor: "#fff",
  },
  webTabsRoot: {
    backgroundColor: "#fff",
    width: "100% !important",
  },
  tabsIndicator: {
    backgroundColor: "#e31b1b",
  },
  webTabsIndicator: {
    backgroundColor: "#e31b1b",
    width: "100% !important",
  },
  tabRoot: {
    textTransform: "initial",
    minWidth: 72,
    fontWeight: "bold",
    // marginRight: theme.spacing.unit * 4,
    fontSize: "15px",
    color: "#787878",
    width: "100% !important",
    "&$tabSelected": {
      color: "#e31b1b",
      fontWeight: "bold",
      outline: "none",
      fontSize: "15px",
      width: "100% !important",
    },
    "&:focus": {
      color: "#e31b1b ",
    },
  },
  webTabRoot: {
    textTransform: "initial",
    width: "100% !important",
    fontWeight: "bold",
    fontFamily: "Product Sans",
    marginRight: theme.spacing.unit * 4,
    fontSize: "15px",
    color: "#787878",
    "&$tabSelected": {
      width: "100% !important",
      color: "#e31b1b",
      fontWeight: "bold",
      outline: "none",
      fontSize: "15px",
      fontFamily: "Product Sans",
    },
    "&:focus": {
      color: "#e31b1b",
    },
  },
  webTabSelected: {
    width: "100% !important",
  },
  tabSelected: {},
  typography: {
    padding: "10px",
  },
  webTabsTypography: {
    padding: "0px",
  },
});

class WebTabs extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs, tabContent, type } = this.props;
    const { value } = this.state;
    return (
      <div className={classes.root}>
        <Tabs
          value={value}
          onChange={this.handleChange}
          classes={{
            root: type === "webTabs" ? classes.webTabsRoot + " classes_webTabRoot" : classes.tabsRoot,
            indicator: type === "webTabs" ? classes.webTabsIndicator : classes.tabsIndicator,
          }}
        >
          {tabs.map((tab, index) => (
            <Tab
              key={index}
              fullWidth={true}
              classes={{
                root: type === "webTabs" ? classes.webTabRoot : classes.tabRoot,
                selected: type === "webTabs" ? classes.webTabSelected : classes.tabSelected,
              }}
              label={tab.label}
            />
          ))}
        </Tabs>
        <Typography className={type === "webTabs" ? classes.type : classes.webTabsTypography}>
          {tabContent.map((tabContent, key) => (key === value ? <TabContainer key={key}>{tabContent.content}</TabContainer> : ""))}
        </Typography>
      </div>
    );
  }
}

WebTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WebTabs);
