import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { __BlobUpload } from "../../lib/cloudinary-image-upload";

const styles = {
  appBar: {
    position: "relative"
  },
  flex: {
    flex: 1
  },
  imgContainer: {
    position: "relative",
    flex: 1,
    padding: 16,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  img: {
    maxWidth: "100%",
    maxHeight: "100%"
  },
  rightIcon: {
    marginLeft: "10px"
  }
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class ImgDialog extends React.Component {
  state = {
    open: false
  };

  uploadImage = file => {
    __BlobUpload(file)
      .then(res => {
        // this.props.__setUrlAfterCropping(res.body.secure_url);
        this.props.updateImageAfterCropping(res.body.secure_url);
        this.props.onClose();
        setTimeout(() => {
          this.props.toggle();
        }, 500);
      })
      .catch(err => console.log("err", err));
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog open={!!this.props.img} onClose={this.props.onClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={this.props.onClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              Cropped image
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.imgContainer}>
          <img src={this.props.img && this.props.img.blobObject} alt="Cropped" className={classes.img} />
        </div>
        <div className="text-center pb-2" onClick={() => this.uploadImage(this.props.img && this.props.img.fileObj)}>
          <Button variant="contained" color="default" className={classes.button}>
            Upload
            <CloudUploadIcon className={classes.rightIcon} />
          </Button>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(styles)(ImgDialog);
