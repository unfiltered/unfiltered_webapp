// Main React Components
import React, { useState, useEffect } from "react";
// Scss
import "./style.scss";
import { FormattedHTMLMessage } from "react-intl";
import { Icons } from "../Icons/Icons";
import { getLikesOnBoost } from "../../controller/auth/verification";
import MaterialUiModal from "../../Components/Model/MAT_UI_Modal";
import { withRouter } from "react-router-dom";
import { getCookie } from "../../lib/session";

const data = [
  {
    contactNumber: "+913887101587",
    countryCode: "+91",
    email: "brian.moraes@example.com",
    firstName: "Brian Moraes",
    profilePic: "https://randomuser.me/api/portraits/men/56.jpg",
    _id: "5ef074a4db1ccd79fb77c9d9",
  },
  {
    contactNumber: "+913887101587",
    countryCode: "+91",
    email: "brian.moraes@example.com",
    firstName: "Brian Moraes",
    profilePic: "https://randomuser.me/api/portraits/men/56.jpg",
    _id: "5ef074a4db1ccd79fb77c9d9",
  },
  {
    contactNumber: "+913887101587",
    countryCode: "+91",
    email: "brian.moraes@example.com",
    firstName: "Brian Moraes",
    profilePic: "https://randomuser.me/api/portraits/men/56.jpg",
    _id: "5ef074a4db1ccd79fb77c9d9",
  },
];

function Webheader({ headername, endTimer, BoostActivatedSessionIncomingData, history }) {
  const [t, setTimer] = useState((endTimer && endTimer.expire - new Date().getTime()) / 1000);
  const [modal, openModal] = useState(false);
  const [likeData, setLikeData] = useState([]);

  const convertSecondsToMinutesTwoPads = (totalSeconds) => {
    totalSeconds %= 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    minutes = String(minutes).padStart(2, "0");
    seconds = String(seconds).padStart(2, "0");
    return minutes + ":" + seconds;
  };

  const toggleModal = () => {
    openModal(!modal);
  };

  useEffect(() => {
    getLikesOnBoost(getCookie("token"))
      .then((res) => {
        if (res.status === 200) {
          setLikeData(res.data.data);
          console.log("res", res.data.data);
        }
      })
      .catch((err) => console.log(""));
  }, [modal]);

  useEffect(() => {
    const interval = setInterval(() => {
      setTimer((t) => t - 1);
      if (t < 1) {
        setTimer(0);
        clearInterval(interval);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const __NotificationText = { fontSize: "13px", color: "#484848", fontFamily: "Product Sans", padding: "0 15px 0 15px" };
  return (
    <div className="col-12 border-bottom">
      <div className="row">
        <div className="col-md-5 col-lg-5 col-xl-5 col-5 d-flex align-items-center pt-2 pl-3">
          <div className="row">
            <div className="col-12">
              <h4 className="headerName">UnFiltered People</h4>
            </div>
            <div className="col-12">
              <h5 className="subHeaderName">See Who Is Close and Around You</h5>
            </div>
          </div>
        </div>
        <div className="col-md-1 col-lg-1 col-xl-1 col-1 d-flex justify-content-center align-items-center">
          {endTimer && endTimer.expire && t > 1 ? (
            <div onClick={toggleModal}>
              <img src={Icons.boost} alt="boost" height={50} width={50} />
              <span className="boost_views">
                {BoostActivatedSessionIncomingData && BoostActivatedSessionIncomingData.views
                  ? BoostActivatedSessionIncomingData.views
                  : endTimer.views}{" "}
                Views
              </span>
            </div>
          ) : (
            <span />
          )}
        </div>
        <div className="col-md-6 col-lg-6 col-xl-6 col-6 w_header py-2">
          <div className="row h-100" onClick={() => history.push("/app/coinbalance")}>
            <div className="col-6"></div>
            <div className="col-6 d-flex justify-content-end align-items-center">
              <div>
                <img src={Icons.BigHeart} alt="User" title="User" width={50} />
              </div>
              <div style={__NotificationText}>
                <FormattedHTMLMessage id="message.nearbyPeople_buyCoinsToTalk" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <MaterialUiModal toggle={toggleModal} isOpen={modal} width={850}>
        <div className="col-12 p-4">
          <div className="d_newsFeed_modal_close_btn" onClick={toggleModal}>
            <img src={Icons.closeBtn} alt="close-btn" height={13} width={13} />
          </div>
          <div className="boostText">Likes you got on boost</div>

          <div className="boostText">
            {t > 0 ? (
              <div className="d-flex pt-1 justify-content-center align-items-center">
                <div className="text-center">Boost time Left: {convertSecondsToMinutesTwoPads(parseInt(t))}</div>
              </div>
            ) : (
              <span />
            )}
          </div>
          <div className="row mt-4 boostModal">
            {likeData.length > 0 ? (
              likeData.map((k) => (
                <div className="col-4 boostLikeCard">
                  <img src={k.profilePic} alt={k.firstName} />
                </div>
              ))
            ) : (
              <div className="d-flex justify-content-center align-items-center">
                <h3>No Likes Yet</h3>
              </div>
            )}
          </div>
        </div>
      </MaterialUiModal>
    </div>
  );
}

export default withRouter(Webheader);
