import NoPremiumSidebar from "../../asset/svg/no_premium_sidebar.svg";
import PremiumSidebar from "../../asset/new_assets/diamond.png";
import SVG_NearbyPeople from "../../asset/mobile_img/nearby_people.svg";
import SVG_Chats from "../../asset/mobile_img/chats.svg";
import SVG_ChatsActive from "../../asset/mobile_img/chats_active.svg";
import SVG_Prospects from "../../asset/mobile_img/prospects.svg";
import prospectsActive from "../../asset/mobile_img/prospects_active.svg";
import ReferralBG from "../../asset/svg/referral_bg.svg";
import Referral from "../../asset/svg/referral.svg";
import Whatsapp from "../../asset/svg/whatsapp.svg";
import Twitter from "../../asset/svg/twitter.svg";
import Facebook from "../../asset/svg/facebook.svg";
import GooglePlus from "../../asset/svg/google.svg";
import SVG_Friends from "../../asset/svg/friends.svg";
import SVG_FriendsActive from "../../asset/svg/friendsActive.svg";
import SVG_Dates from "../../asset/mobile_img/plus_dates.svg";
import SVG_DatesActive from "../../asset/mobile_img/plus_dates_active.svg";
import SVG_Referrals from "../../asset/svg/Referrals.svg";
import SVG_ReferralsActive from "../../asset/svg/ReferralsActive.svg";
import SVG_Earnings from "../../asset/svg/Earnings.svg";
import EarningsActive from "../../asset/svg/EarningsActive.svg";
import SVG_NewsFeed from "../../asset/svg/Newsfeed.svg";
import SVG_NewsFeed_Active from "../../asset/svg/SVG_NewsFeed_Active.svg";
import SVG_Settings from "../../asset/svg/Settings.svg";
import SVG_SettingsActive from "../../asset/svg/SVG_SettingsActive.svg";
import SVG_Discover from "../../asset/mobile_img/discover.svg";
import SVG_DiscoverActive from "../../asset/mobile_img/discover_active.svg";
import SVG_Logout from "../../asset/svg/Logout.svg";
import SVG_LogoutActive from "../../asset/svg/SVG_LogoutActive.svg";
import SVG_NearbyPeopleActive from "../../asset/mobile_img/nearby_people_active.svg";
import CoinsSidebar from "../../asset/new_assets/coins.png";
import NoCoinsSidebar from "../../asset/svg/no_coins_sidebar.svg";
import Wallet from "../../View/WebHomePage/Components/Coin/Assets/wallet.svg";
import SendChatIcon from "../../View/WebHomePage/Components/Chat/svg/send_chat.svg";
import TwoFriends from "../../View/WebHomePage/Components/Chat/svg/two_users.svg";
import TransferCoins from "../../View/WebHomePage/Components/Chat/svg/transfer.svg";
import FixDate from "../../View/WebHomePage/Components/Chat/svg/fixDate.svg";
import Sticker from "../../asset/svg/sticker.svg";
import CallDate from "../../View/WebHomePage/Components/Chat/svg/_call.svg";
import PhysicalDate from "../../View/WebHomePage/Components/Chat/svg/_place.svg";
import CloseBtn from "../../asset/svg/close.svg";
import PinkCloseBtn from "../../asset/svg/pink-close.svg";
import NoMatches from "../../asset/new_assets/no_matches.svg";
import GoldCoin from "../../asset/images/gold-coin.svg";
import DisabledAttachment from "../../asset/mobile_img/disabled_attachment.svg";
import DisabledPlus from "../../asset/mobile_img/disabled_plus.svg";
import DisabledThreeDots from "../../asset/mobile_img/disabled_three_dots.svg";
// import HeartIconChat from "../../View/WebHomePage/ComponentsNewsFeed/assets/heart.svg";
import HeartIconChat from "../../View/WebHomePage/Components/NewsFeed/assets/heart.svg";
import ThreeDots from "../../asset/new_assets/n1.svg";
import Attachment from "../../asset/new_assets/n2.svg";
import Plus from "../../asset/new_assets/n3.svg";
import BackArrow from "../../asset/images/black-left-arrow.svg";
import Love from "../../View/WebHomePage/Components/Coin/Assets/love.svg";
import Star from "../../View/WebHomePage/Components/Coin/Assets/Group-649.svg";
import UpArrow from "../../View/WebHomePage/Components/Coin/Assets/up-arrow.svg";
import DatumLove from "../../View/WebHomePage/Components/Coin/Assets/Group-650.svg";
import DiamondWithStars from "../../View/WebHomePage/Components/Coin/Assets/diamond_icon.svg";
import Camera from "../../asset/images/rev_camera.svg";
import DeleteImage from "../../View/WebHomePage/Components/Coin/Assets/grey_close.svg";
import PinkTick from "../../asset/svg/pink_tick.svg";
import GreyTick from "../../asset/svg/greyout_tick.svg";
import MoveSVG from "../../asset/svg/move.svg";
import NoUsersFound from "../../asset/new_assets/no_users.svg";
import ThumbsUp from "../../asset/svg/gestures.svg";
import AlreadyThumbsUpped from "../../asset/svg/disabled-gestires.svg";
import AlreadyLiked from "../../asset/ActivePlanModal_Assets/like.svg";
import AlreadySuperLiked from "../../asset/ActivePlanModal_Assets/super_like.svg";
import PostComment from "../../asset/svg/post_comment.svg";
import DisabledComment from "../../asset/svg/disabled_comment.svg";
import VideoCamera from "../../asset/svg/video-camera.svg";
import PassportIcon from "../../asset/svg/maps-and-flags.svg";
import Preferences from "../../asset/svg/preferences.svg";
import Thrash from "../../asset/svg/trash.svg";
import Check from "../../asset/svg/check-mark.svg";
import Money from "../../asset/images/money.svg";
import Paypal from "../../asset/images/paypal.svg";
import GIFIcon from "../../asset/images/gif.svg";
import HappySmiley from "../../asset/images/happiness.svg";
import RevDiamond from "../../asset/new_assets/rev_diamon.svg";
import Checked from "../../asset/svg/checked.svg";
import Path from "../../asset/svg/Shape.svg";
import Path1 from "../../asset/svg/Shape-1.svg";
import CommentLogo from "../../asset/svg/Path-1.svg";
import CMeLoader from "../../asset/svg/c.gif";
import NewsFeedPlaceholder from "../../asset/svg/newsfeedplaceholder.svg";
import TickSvg from "../../View/MobileHomePage/Components/Location/svg/tick.svg";
import CircleSvg from "../../View/MobileHomePage/Components/Location/svg/circle.svg";
import NewsFeedThreeDots from "../../View/WebHomePage/Components/NewsFeed/assets/menu.svg";
import SendSticker from "../../asset/new_assets/sticker.svg";
import SendLocation from "../../asset/new_assets/map.svg";
import SendMoney from "../../asset/new_assets/money-bag.svg";
import SendGIF from "../../asset/new_assets/gif.svg";
import SendAttachment from "../../asset/new_assets/digital-campaign.svg";
import VideoPlay from "../../asset/svg/youtube.svg";
import Boost from "../../asset/svg/boost.svg";
import TabDateActive from "../../asset/mobile_img/tabDateActive.svg";
import TabDateNotActive from "../../asset/mobile_img/tabDateNotActive.svg";
import TabDiscoverActive from "../../asset/mobile_img/tabDiscoverActive.svg";
import TabDiscoverNotActive from "../../asset/mobile_img/tabDiscoverNotActive.svg";
import TabFriendActive from "../../asset/mobile_img/tabFriendActive.svg";
import TabFriendNotActive from "../../asset/mobile_img/tabFriendNotActive.svg";
import TabMatchActive from "../../asset/mobile_img/tabMatchActive.svg";
import TabMatchNotActive from "../../asset/mobile_img/tabMatchNotActive.svg";
import TabProspectsActive from "../../asset/mobile_img/tabProspectsActive.svg";
import TabProspectsNotActive from "../../asset/mobile_img/tabProspectsNotActive.svg";
import SettingCMEPlus from "../../asset/mobile_img/settings-cmeplus.svg";
import SettingHelp from "../../asset/mobile_img/settings-help.svg";
import SettingInvite from "../../asset/mobile_img/settings-invite.svg";
import SettingSettings from "../../asset/mobile_img/settings-settings.svg";
import SettingMyPref from "../../asset/mobile_img/settings-mypref.svg";
import MobileAddFriend from "../../asset/mobile_img/addFriend.png";
import MobileNoFriend from "../../asset/mobile_img/nofriend.png";
import PinkBack from "../../asset/mobile_img/pinkBack.png";
import MobileNoRequest from "../../asset/mobile_img/norequest.png";
import RightArrow from "../../asset/mobile_img/rightArrow.png";
import DateCalendar from "../../asset/mobile_img/dateCalendar.png";
import MCallDate from "../../asset/mobile_img/callDate.png";
import MReschedule from "../../asset/mobile_img/calendar.svg";
import Heart1 from "../../asset/mobile_img/heart1.png";
import Heart2 from "../../asset/mobile_img/heart2.png";
import Heart3 from "../../asset/mobile_img/heart3.png";
import Heart4 from "../../asset/mobile_img/heart4.png";
import DateConfirmBtn from "../../asset/mobile_img/dateConfirmBtn.png";
import SpendCoins from "../../asset/mobile_img/spendCoins.png";
import UploadPost from "../../asset/mobile_img/uploadPost.png";
import NoNewsSad from "../../asset/mobile_img/noNews.png";
import WalletTransactionInfo from "../../asset/mobile_img/walletTransactionInfo.png";
import recentVisitor from "../../asset/mobile_img/recentVisitor.png";

import likesMe from "../../asset/mobile_img/likesMe.png";
import superLike from "../../asset/mobile_img/superLike.png";
import boostLikes from "../../asset/mobile_img/boostLikes.png";
import MoneyTransfer from "../../asset/mobile_img/money-transfer.svg";
import M_Chat from "../../asset/mobile_img/chat.svg";
import M_DeletePicture from "../../asset/new_assets/cancel.svg";
import MatchedPlus from "../../asset/svg/plus.svg";
import M_Updated_Referrals from "../../asset/mobile_img/voucher.svg";
import FaceTime from "../../asset/new_assets/facetime-button.svg";
import MobileSticker from "../../asset/svg/m_sticker.svg";
import cmeLogoSVG from "../../asset/svg/cme-logo.svg";
import BigHeart from "../../asset/WebUserCard/big-heart.png";
import BigHeart2 from "../../asset/WebUserCard/big-heart-2.png";
import SmallHeart from "../../asset/WebUserCard/big-heart.png";
import NoUsers from "../../asset/new_assets/common_placeholder.svg";
import UpdateNoChats from "../../asset/svg/u_no_chats.svg";
import ReplacementForCoins from "../../asset/new_assets/replacement_for_coins.png";
import NewReferIcon from "../../asset/new_assets/refer-icon.svg";
import ChevronDown from "../../asset/new_assets/down-chevron.svg";
import NewDollarIcon from "../../asset/svg/dollar@3x.png";
import BlueHeart from "../../asset/new_assets/blueHeart.png";
import GreenHeart from "../../asset/new_assets/greenHeart.png";
import RedHeart from "../../asset/new_assets/redHeart.png";
import VipIcon from "../../asset/svg/heart.svg";
import NewNewsFeedIcon from "../../asset/svg/newsfeed.jpeg";
import NewBell from "../../asset/ui_update/bell.svg";
import NewUser from "../../asset/ui_update/User.svg";
import NewMessenger from "../../asset/ui_update/messenger.svg";
import GrinSmiley from "../../asset/svg/grinning.svg";
import FacebookApp from "../../asset/footer-logos/facebook-app-symbol.svg";
import TwitterLogo from "../../asset/footer-logos/twitter.svg";
import Youtube from "../../asset/footer-logos/youtube.svg";
import Instagram from "../../asset/footer-logos/instagram-logo.svg";
import U_LocationMap from "../../asset/ui_update/Location-map.svg";
import U_Globe from "../../asset/ui_update/Globe.svg";
import U_Chat from "../../asset/ui_update/Comment.svg";
import U_Prospects from "../../asset/ui_update/Flame.svg";
import U_Friends from "../../asset/ui_update/User1.svg";
import U_Dates from "../../asset/ui_update/Love.svg";
import U_NewsFeed from "../../asset/ui_update/View.svg";
import U_Referral from "../../asset/ui_update/Link.svg";
import U_Settings from "../../asset/ui_update/Settings.svg";
import U_SignOut from "../../asset/ui_update/Sign-out.svg";
import NewChat from "../../asset/WebUserCard/chat.png";
import NewSuperLike from "../../asset/WebUserCard/super-like.png";
import NewDislike from "../../asset/WebUserCard/dislike.png";
import NewLike from "../../asset/WebUserCard/like.png";
import U_Alert from "../../asset/ui_update/WarningIcon.svg";
import U_Visitor from "../../asset/ui_update/Visitor.svg";

import unlimitedLikes from "../../asset/mobile_img/unlimitedLikes.png";
import swipeAround from "../../asset/mobile_img/swipeAround.svg";
import unlimitedSuperlikes from "../../asset/mobile_img/unlimitedSuperlikes.png";
import unlimited_rewinds from "../../asset/mobile_img/unlimited_rewinds.png";

import m_video_call_date from "../../asset/mobile_img/video_call.png";
import m_physical_date from "../../asset/mobile_img/physical_date.png";
import m_call_date from "../../asset/mobile_img/call_date.png";
import danger from "../../asset/mobile_img/danger.png";
import Spinner from "../../asset/svg/eclipseLoader.svg";
import DotsSpinner from "../../asset/ui_update/dotsSpinner.svg";
import M_Preferences from "../../asset/new_assets/m_preference.png";
import Exclusive_tour from "../../asset/new_assets/exclusive_tour.png";
import WhiteCancel from "../../asset/svg/cancel.png";
import RedBgWhiteLoveIcon from "../../asset/new_assets/coin1.png";
import CheckMark from "../../asset/mobile_img/check-mark.png";
import { keys } from "../../lib/keys";

export const Icons = {
  CheckMark: CheckMark,
  RedBgWhiteLoveIcon: RedBgWhiteLoveIcon,
  WhiteCancel: WhiteCancel,
  Exclusive_tour: Exclusive_tour,
  M_Preferences: M_Preferences,
  DotsSpinner: DotsSpinner,
  Spinner: Spinner,
  danger: danger,
  m_call_date: m_call_date,
  m_physical_date: m_physical_date,
  m_video_call_date: m_video_call_date,
  unlimited_rewinds: unlimited_rewinds,
  swipeAround: swipeAround,
  unlimitedLikes: unlimitedLikes,
  unlimitedSuperlikes: unlimitedSuperlikes,
  BigHeart2: BigHeart2,
  U_Visitor: U_Visitor,
  U_Alert: U_Alert,
  NewChat: NewChat,
  NewSuperLike: NewSuperLike,
  NewDislike: NewDislike,
  NewLike: NewLike,
  U_SignOut: U_SignOut,
  U_Settings: U_Settings,
  U_Referral: U_Referral,
  U_NewsFeed: U_NewsFeed,
  U_Dates: U_Dates,
  U_Friends: U_Friends,
  U_Chat: U_Chat,
  U_Globe: U_Globe,
  U_Prospects: U_Prospects,
  U_LocationMap: U_LocationMap,
  FacebookApp: FacebookApp,
  TwitterLogo: TwitterLogo,
  Youtube: Youtube,
  Instagram: Instagram,
  GrinSmiley: GrinSmiley,
  NewBell: NewBell,
  NewUser: NewUser,
  NewMessenger: NewMessenger,
  NewNewsFeedIcon: NewNewsFeedIcon,
  VipIcon: VipIcon,
  BlueHeart: BlueHeart,
  GreenHeart: GreenHeart,
  RedHeart: RedHeart,
  NewDollarIcon: NewDollarIcon,
  ChevronDown: ChevronDown,
  NewReferIcon: NewReferIcon,
  UpdateNoChats: UpdateNoChats,
  ReplacementForCoins: ReplacementForCoins,
  NoUsers: NoUsers,
  BigHeart: BigHeart,
  SmallHeart: SmallHeart,
  MobileSticker: MobileSticker,
  FaceTime: FaceTime,
  MatchedPlus: MatchedPlus,
  SVG_ReferralsActive: SVG_ReferralsActive,
  prospectsActive: prospectsActive,
  settingLogo: SVG_Settings,
  settingLogoActive: SVG_SettingsActive,
  newsFeed_active: SVG_NewsFeed_Active,
  newsFeed: SVG_NewsFeed,
  earnings: SVG_Earnings,
  earningsActive: EarningsActive,
  referrals: SVG_Referrals,
  dates: SVG_Dates,
  SVG_DatesActive: SVG_DatesActive,
  friends: SVG_Friends,
  friendsActive: SVG_FriendsActive,
  prospects: SVG_Prospects,
  googlePlus: GooglePlus,
  whatsapp: Whatsapp,
  facebook: Facebook,
  twitter: Twitter,
  referralSquare: Referral,
  referralBg: ReferralBG,
  chats: SVG_Chats,
  discover: SVG_Discover,
  nearbyPeople: SVG_NearbyPeople,
  nearbyPeopleActive: SVG_NearbyPeopleActive,
  logout: SVG_Logout,
  logoutActive: SVG_LogoutActive,
  cmeLogo: keys.AppLogo,
  hasMoney: CoinsSidebar,
  noMoney: NoCoinsSidebar,
  premiumPlan: PremiumSidebar,
  noPremiumPlan: NoPremiumSidebar,
  sticker: Sticker,
  wallet: Wallet,
  sendChat: SendChatIcon,
  transferCoins: TransferCoins,

  fixDate: FixDate,
  callDate: CallDate,
  physicalDate: PhysicalDate,

  closeBtn: CloseBtn,
  noMatches: NoMatches,
  disabledAttachment: DisabledAttachment,
  disabledPlus: DisabledPlus,
  disabledThreeDots: DisabledThreeDots,
  enabledThreeDots: ThreeDots,
  enabledAttachment: Attachment,
  enabledPlus: Plus,
  twoFriends: TwoFriends,
  goldCoin: GoldCoin,
  friendsChat: HeartIconChat,
  backArrow: BackArrow,
  actPlanLove: Love,
  actPlanStar: Star,
  actPlanUpArrow: UpArrow,
  datumIcon: DatumLove,
  actPlanDiamond: DiamondWithStars,
  camera: Camera,
  deleteImage: DeleteImage,
  profilePicActive: PinkTick,
  activateProfilePic: GreyTick,
  reorderImages: MoveSVG,
  noUsersFound: NoUsersFound,
  otherUsersThumbsUp: ThumbsUp,
  otherUsersAlreadyLiked: AlreadyLiked,
  alreadySuperLiked: AlreadySuperLiked,
  postComment: PostComment,
  disabledComment: DisabledComment,
  videoCameraEnabled: VideoCamera,
  preferences: Preferences,
  passportIcon: PassportIcon,
  deleteDate: Thrash,
  acceptDate: Check,
  money: Money,
  paypal: Paypal,
  gifIcon: GIFIcon,
  happySmiley: HappySmiley,
  revDiamond: RevDiamond,
  checked: Checked,
  likedLogo: Path,
  likedLogo1: Path1,
  commentLogo: CommentLogo,
  CMeLoader: CMeLoader,
  tickSvg: TickSvg,
  circleSvg: CircleSvg,
  NewsFeedPlaceholder: NewsFeedPlaceholder,
  newsFeedThreeDots: NewsFeedThreeDots,
  SendSticker: SendSticker,
  SendGIF: SendGIF,
  SendMoney: SendMoney,
  SendAttachment: SendAttachment,
  SendLocation: SendLocation,
  alreadyThumbsUpped: AlreadyThumbsUpped,
  videoPlay: VideoPlay,
  boost: Boost,
  TabDateActive: TabDateActive,
  TabDateNotActive: TabDateNotActive,
  TabProspectsActive: TabProspectsActive,
  TabProspectsNotActive: TabProspectsNotActive,
  TabMatchActive: TabMatchActive,
  TabMatchNotActive: TabMatchNotActive,
  TabDiscoverActive: TabDiscoverActive,
  TabDiscoverNotActive: TabDiscoverNotActive,
  TabFriendActive: TabFriendActive,
  TabFriendNotActive: TabFriendNotActive,
  SettingCMEPlus: SettingCMEPlus,
  SettingHelp: SettingHelp,
  SettingInvite: SettingInvite,
  SettingSettings: SettingSettings,
  SettingMyPref: SettingMyPref,
  MobileAddFriend: MobileAddFriend,
  MobileNoFriend: MobileNoFriend,
  MobileNoRequest: MobileNoRequest,
  PinkBack: PinkBack,
  RightArrow: RightArrow,
  DateCalendar: DateCalendar,
  MCallDate: MCallDate,
  MReschedule: MReschedule,
  Heart1: Heart1,
  Heart2: Heart2,
  Heart3: Heart3,
  Heart4: Heart4,
  PinkCloseBtn: PinkCloseBtn,
  DateConfirmBtn: DateConfirmBtn,
  SpendCoins: SpendCoins,
  UploadPost: UploadPost,
  NoNewsSad: NoNewsSad,
  WalletTransactionInfo: WalletTransactionInfo,
  recentVisitor: recentVisitor,
  likesMe: likesMe,
  superLike: superLike,
  boostLikes: boostLikes,
  MoneyTransfer: MoneyTransfer,
  M_Chat: M_Chat,
  M_DeletePicture: M_DeletePicture,
  SVG_DiscoverActive: SVG_DiscoverActive,
  SVG_ChatsActive: SVG_ChatsActive,
  M_Updated_Referrals: M_Updated_Referrals,
  cmeLogoSVG: cmeLogoSVG,
};
