import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import cloudinary from "cloudinary-core";

const styles = {
  appBar: {
    position: "relative",
  },
  flex: {
    flex: 1,
  },
  imgContainer: {
    position: "relative",
    flex: 1,
    padding: 16,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  img: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
  rightIcon: {
    marginLeft: "10px",
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class ImgDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  uploadImage = async (file) => {
    var file = new File([file], "profile");
    const data = new FormData();
    data.append("file", file);
    data.append("upload_preset", "tpczegbo");
    const res = await fetch("https://api.cloudinary.com/v1_1/datesndime/image/upload/", {
      method: "POST",
      body: data,
    });
    console.log("upload image 1", res.json());
    const x = await res.json();
    console.log("upload image 2", x);
    console.log("file---->>", x);
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog fullScreen open={!!this.props.img} onClose={this.props.onClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={this.props.onClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              Cropped image
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.imgContainer}>
          <img src={this.props.img && this.props.img.blobObject} alt="Cropped" className={classes.img} />
        </div>
        <div onClick={() => this.uploadImage(this.props.img && this.props.img.fileObj)}>
          <Button variant="contained" color="default" className={classes.button}>
            Upload
            <CloudUploadIcon className={classes.rightIcon} />
          </Button>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(styles)(ImgDialog);
