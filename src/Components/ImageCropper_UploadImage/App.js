import React, { useState, useCallback, useEffect } from "react";
import Cropper from "react-easy-crop";
import Slider from "@material-ui/lab/Slider";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import getCroppedImg from "./cropImage";
import { styles } from "./styles";
import { __BlobUpload } from "../../lib/cloudinary-image-upload";
import { connect } from "react-redux";
import { __setUrlAfterCropping, __setFileObjectAfterCropped } from "../../actions/User";
import { Icons } from "../Icons/Icons";

const ImageCropper_UploadImage = ({
  onlyGenerateBlob,
  classes,
  propImage,
  toggle,
  cloudinaryImageStateHandlerAfterCropping,
  cloudinaryImageStateHandler,
  signup,
  addNewPostPreviewForCustomModal,
  __setUrlAfterCroppingFn,
  __setFileObjectAfterCropped,
  closeImageCropModal,
  setGotImageData,
  uploadImage,
  isMobile,
  gotData,
}) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [rotation, setRotation] = useState(0);
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [croppedImage, setCroppedImage] = useState(null);

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);

  const showCroppedImage = useCallback(async () => {
    try {
      const croppedImageRes = await getCroppedImg(openedFile, croppedAreaPixels, rotation);
      setCroppedImage(croppedImageRes);

      if (onlyGenerateBlob) {
        addNewPostPreviewForCustomModal(croppedImageRes.blobObject, croppedImageRes.fileObj);
        setOpenedfile(croppedImageRes.blobObject);
        // setOpenedfile(URL.createObjectURL(propImage && propImage.current.files[0]))
      } else {
        if (signup === false) {
          __BlobUpload(croppedImageRes.fileObj).then((res) => {
            cloudinaryImageStateHandlerAfterCropping(res.body.secure_url);
            __setUrlAfterCroppingFn(res.body.secure_url);
          });
        } else {
          cloudinaryImageStateHandler(croppedImageRes.blobObject);
          __setUrlAfterCroppingFn(croppedImageRes.blobObject);
          __setFileObjectAfterCropped(croppedImageRes);
        }
      }
      // sending the cropped result to parent
      uploadImage && uploadImage({ cloudinaryUpload: uploadCroppedImageOnCloudinary, croppedImage: croppedImageRes });
    } catch (e) {
      console.error(e);
    }

    if (isMobile) {
      console.log();
    } else {
      toggle();
    }
  }, [croppedAreaPixels, rotation]);

  const uploadCroppedImageOnCloudinary = (profileCroppedImage) => {
    __BlobUpload((croppedImage && croppedImage.fileObj) || (profileCroppedImage && profileCroppedImage.fileObj)).then((res) => {
      cloudinaryImageStateHandler(res.body.secure_url);
      __setUrlAfterCroppingFn(res.body.secure_url);
    });
  };

  // const onClose = useCallback(() => {
  //   setCroppedImage(null);
  // }, []);

  const [openedFile, setOpenedfile] = useState("");

  useEffect(() => {
    if (signup) {
      setOpenedfile(URL.createObjectURL(propImage && propImage));
    } else {
      setOpenedfile(URL.createObjectURL(propImage && propImage.current.files[0]));
    }
    // console.log('UploadImage', uploadImage)
    uploadImage && uploadImage({ cloudinaryUpload: uploadCroppedImageOnCloudinary, croppedImage: croppedImage });
  }, []);
  return (
    <div style={{ position: "relative" }} uploadCroppedImageOnCloudinary={uploadCroppedImageOnCloudinary}>
      {onlyGenerateBlob && !isMobile ? (
        <div className="d_newsFeed_modal_close_btn_2" onClick={closeImageCropModal}>
          <img src={Icons.closeBtn} height={13} width={13} alt="close-btn" />
        </div>
      ) : (
        ""
      )}
      <div>
        <div className={classes.cropContainer}>
          <Cropper
            image={openedFile}
            crop={crop}
            rotation={rotation}
            zoom={zoom}
            aspect={isMobile ? 16 / 9 : 4 / 3}
            onCropChange={setCrop}
            onRotationChange={setRotation}
            onCropComplete={onCropComplete}
            onZoomChange={setZoom}
          />
        </div>
        <div className={classes.controls}>
          <div className={classes.sliderContainer}>
            <Typography variant="overline" classes={{ root: classes.sliderLabel }}>
              Zoom
            </Typography>
            <Slider
              value={zoom}
              min={1}
              max={3}
              step={0.1}
              aria-labelledby="Zoom"
              classes={{ container: classes.slider, thumb: classes.thumb, track: classes.track }}
              onChange={(e, zoom) => setZoom(zoom)}
            />
          </div>
          <div className={classes.sliderContainer}>
            <Typography variant="overline" classes={{ root: classes.sliderLabel }}>
              Rotation
            </Typography>
            <Slider
              value={rotation}
              min={0}
              max={360}
              step={1}
              aria-labelledby="Rotation"
              classes={{ container: classes.slider, thumb: classes.thumb, track: classes.track }}
              onChange={(e, rotation) => setRotation(rotation)}
            />
          </div>
          {isMobile ? (
            !gotData ? (
              <Button
                onClick={() => {
                  setGotImageData(true);
                  showCroppedImage();
                }}
                classes={{
                  root: classes.uploadButton,
                }}
                variant="contained"
                color="default"
                className="m-0"
              >
                Crop & Proceed
              </Button>
            ) : (
              <span />
            )
          ) : (
            <Button
              onClick={showCroppedImage}
              variant="contained"
              className="m-0"
              style={{ color: "#FFF", background: "linear-gradient(to bottom right,#f80402 0,#a50f0d)" }}
            >
              Crop & Upload
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    __setUrlAfterCroppingFn: (url) => dispatch(__setUrlAfterCropping(url)),
    __setFileObjectAfterCropped: (object) => dispatch(__setFileObjectAfterCropped(object)),
    // __setUrlAfterCropping: (object) => dispatch(__setUrlAfterCropping(object))
  };
};

const mapStateToProps = (state) => {
  return {
    SET_URL_AFTER_CROPPING: state.urlAfterCropping,
  };
};

const _App = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ImageCropper_UploadImage));

export default _App;
