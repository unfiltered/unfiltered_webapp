import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";

class MaterialDropdown extends React.Component {
  render() {
    return (
      <>
        <IconButton aria-label="More" aria-owns={false ? "long-menu" : undefined} aria-haspopup="true" onClick={this.props.onClick} style={{ color: "#f74167" }}>
          <img src={this.props.icon} height={25} alt="menu" />
        </IconButton>
        <Menu
          className={this.props.menuClass}
          id="long-menu"
          anchorEl={this.props.anchorEl}
          open={this.props.anchorEl}
          onClose={this.props.onClose}
          PaperProps={this.props.PaperProps}
        >
          {this.props.children}
        </Menu>
      </>
    );
  }
}

export default MaterialDropdown;
