import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  arrow: {
    // color: "#fff",
    // background: "linear-gradient(to bottom right, #f80402 0, #a50f0d)",
    // height: "50px",
    // width: "50px",
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    // borderRadius: "12px",
    // outline: "none",
    background: "transparent",
    outline: "none",
    border: "none",
  },
  disabledarrow: {
    // color: "#fff",
    // background: "#b1b1b1",
    // height: "50px",
    // width: "50px",
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    // borderRadius: "12px",
    background: "transparent",
    outline: "none",
    border: "none",
  },
});

class ArrowNext extends Component {
  render() {
    const { classes, disabled } = this.props;
    return (
      <button className={disabled ? classes.disabledarrow : classes.arrow} onClick={this.props.onClick} disabled={this.props.disabled}>
        {disabled ? (
          <img src={require("../../asset/new_assets/disabled-arrow.svg")} width={60} height={60} alt="double-arrow" />
        ) : (
          <img src={require("../../asset/new_assets/arrow.svg")} width={60} height={60} alt="double-arrow" />
        )}
      </button>
    );
  }
}

ArrowNext.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ArrowNext);
