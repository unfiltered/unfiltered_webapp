// Main React Components
import React, { Component } from "react";
import MainModal from "../../Components/Model/model";
import UpdatedPlansProUser from "../../View/WebHomePage/Components/UpdatePlansProUser/UpdatePlansProUser";
import { connect } from "react-redux";
import SmallDialog from "../Dialogs/SmallDialog";
// Scss
import "./style.scss";

class WebHeader extends Component {
  state = {
    modelplan: false,
    boostModal: false,
  };
  tooglecoinmodelplan = () => {
    this.setState((prevState) => ({
      modelplan: !prevState.modelplan,
    }));
  };
  toggleBoostModal = () => {
    this.setState((prevState) => ({
      boostModal: !prevState.boostModal,
    }));
  };
  render() {
    let { boostStatus } = this.props;
    let headerName = { color: "#21242A" };
    let { views, likes, supperLikes } = this.props.boostStats;
    return (
      <div className="Revised_WCommonbox">
        {/* Meet New People Navbar Start */}
        <div className="py-sm-2">
          {boostStatus ? (
            <div className="m-0 row">
              <div className="pt-sm-2 pt-md-2 pt-lg-2 col-sm-7 col-md-8 col-lg-9 text-left Wcommonheading">
                <div className="col-auto">
                  <p style={headerName} className="h4 m-0">
                    {this.props.headername}
                  </p>
                </div>
              </div>
              <div className="pt-sm-0 pt-md-0 pt-lg-0 col-sm-4 col-md-3 col-lg-2 text-center"></div>
            </div>
          ) : (
            <div className="m-0 row">
              <div className="pt-sm-2 pt-md-2 pt-lg-2 col-sm-8 col-md-9 col-lg-10 text-left Wcommonheading">
                <div className="pl-2">
                  <p className="m-0" style={headerName}>
                    {this.props.headername}
                  </p>
                </div>
              </div>
              <div className="pt-sm-0 pt-md-0 pt-lg-0 col-sm-4 col-md-3 col-lg-2 text-center"></div>
            </div>
          )}
        </div>
        <MainModal isOpen={this.state.modelplan} toggle={this.tooglecoinmodelplan}>
          <UpdatedPlansProUser openModal={!this.state.modelplan} toggleModal={this.tooglecoinmodelplan} />
        </MainModal>
        <SmallDialog
          open={this.state.boostModal}
          handleClose={this.toggleBoostModal}
          views={views > 0 ? views : 0}
          supperLikes={supperLikes > 0 ? supperLikes : 0}
          likes={likes > 0 ? likes : 0}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    boostStatus: state.Boost.BoostProfile || state.Boost.BoostDetailsOnRefresh,
    boostStats: state.Boost.BoostActivatedSessionIncomingData,
  };
};

export default connect(mapStateToProps, null)(WebHeader);
