import React from "react";
// import { Carousel, CarouselItem, CarouselControl, CarouselIndicators } from "reactstrap";
import { connect } from "react-redux";
import FirstPageBGImage from "../../asset/images/banner_cme.webp";
import SecondPage from "../../asset/images/BG.webp";
import AndroidBtn from "../../asset/CME/png/androidbutton.png";
import IOSBtn from "../../asset/CME/png/iosbutton.png";
import Random from "../../asset/CME/png/random.png";
import OwnDating from "../../asset/CME/png/own_dating_app.png";
// import Love from "../../asset/CME/png/love.png";
import Earn from "../../asset/CME/png/earn.png";
import SecondImg from "../../asset/mobile_img/2nd.png";
import ThirdImg from "../../asset/mobile_img/3rd.png";
import FourthImg from "../../asset/mobile_img/4th.png";
import FifthImg from "../../asset/mobile_img/5th.png";
import SixthImg from "../../asset/mobile_img/6th.png";
import SeventhImg from "../../asset/mobile_img/7th.png";
import DynamicPref from "../../asset/mobile_img/dp.svg";
import LastImg from "../../asset/mobile_img/last-img.png";
import WhatsappLikeChat from "../../asset/mobile_img/wa.svg";
import SharedRev from "../../asset/mobile_img/sh.svg";
import FooterMockUp from "../../asset/mobile_img/footer_Mockup.png";
import LandingPage from "../../View/LandingPage/TinderPage/Components/LandingPage";
import "./customCarousel.scss";
// import Swiper from "react-id-swiper";

// const params = {
//   direction: "vertical",
//   pagination: {
//     el: ".swiper-pagination",
//     clickable: true,
//   },
//   autoplay: true,
// };

const items = [
  {
    src: FirstPageBGImage,
    androidBtn: AndroidBtn,
    iosBtn: IOSBtn,
    caption: "Start Something Epic",
  },
  {
    src: SecondPage,
    mobileImg: SecondImg,
    svgImg: DynamicPref,
    header: "DYNAMIC SEARCH PREFERENCES",
    alt: "random-video-date",
    subHeader: "Set your preferences for your perfect match. Gender, age, height, distance and more.",
  },
  {
    src: SecondPage,
    mobileImg: ThirdImg,
    svgImg: Random,
    header: "RANDOM VIDEO CALL",
    alt: "random-video-date",
    subHeader: (
      <div>
        <div>Swipe on the screen to connect with random users who match your preferences.When connected over a live video call, the user can:</div>
        <div>1) Send messages in real time to the other user</div>
        <div>2) Send a friend request for talking more later</div>
      </div>
    ),
  },
  {
    src: SecondPage,
    mobileImg: FourthImg,
    svgImg: Earn,
    header: "HOW DO YOU EARN MONEY FROM UNFILTERED?",
    alt: "withdraw-money-modal",
    subHeader: (
      <div>
        <div>1. In-app purchases</div>
        <div>2. Earn money via gifting Credits</div>
        <div>3. Earn for every video / audio call date setup</div>
        <div>4. Chat before getting matched</div>
      </div>
    ),
  },
  {
    src: SecondPage,
    mobileImg: FifthImg,
    svgImg: OwnDating,
    header: "SETUP DATES OVER AUDIO / VIDEO CALL",
    alt: "its-a-match",
    subHeader:
      "Users need to spend Credits to set up a date with other users on the app. App makes a % commission whenever Credits are spent to set up a date.",
  },
  {
    src: SecondPage,
    mobileImg: SixthImg,
    svgImg: Earn,
    header: "SHARED REVENUE MODEL",
    alt: "its-a-match",
    subHeader:
      "Users can spend Credits to connect with prospects they are not matched with. A % of the amount goes to the person they are spending the amount on, and the rest of it comes to the admin. A platform for mutual benefits.",
  },
  {
    src: SecondPage,
    mobileImg: SeventhImg,
    svgImg: WhatsappLikeChat,
    header: "CHAT",
    alt: "whatsapp-like-chat",
    subHeader: "Chat with other users in real-time. Gift Credits, Send Images, gifs, videos, location, and contacts in chats.",
  },
  {
    src: SecondPage,
    midHeader: "Check out Our App",
    midSubHeader: "The best way to find matches nearby is to go to the newest version of the mobile app!",
    androidBtn: AndroidBtn,
    iosBtn: IOSBtn,
    rightSideImg: LastImg,
    btmContent: <LandingPage />,
  },
];

const openAndroidTab = () => {
  const url = `https://play.google.com/store/apps/details?id=com.cMe.com&hl=en_IN`;
  window.open(url, "_blank");
};

const openIOSTab = () => {
  const url = "https://apps.apple.com/us/app/c-me-random-video-date/id1511510705";
  window.open(url, "_blank");
};

const CustomCarousel = (props) => {
  const slides = items.map((item, index) => {
    return (
      <div className="col-12 px-0 customCarousel" style={{ backgroundImage: "url(" + item.src + ")", height: "100vh" }} key={index}>
        <div className="row mx-0 h-100 ">
          <div className={"col-6 d-flex justify-content-center align-items-center flex-column"}>
            {item.midHeader ? (
              <div className="row flex-column justify-content-center align-items-center h-100">
                <div className="c_midHeader">{item.midHeader}</div>
                <div className="c_midSubheader">{item.midSubHeader}</div>
                <div className="d-flex">
                  <div onClick={openAndroidTab} style={{ cursor: "pointer" }}>
                    <img src={item.androidBtn} alt="android-btn" />
                  </div>
                  <div onClick={openIOSTab} style={{ cursor: "pointer" }}>
                    <img src={item.iosBtn} alt="ios-btn" />
                  </div>
                </div>
              </div>
            ) : item.mobileImg ? (
              <div className="c_mainHeader w-100 h-100">
                <div className="h-100 d-flex align-items-center">
                  <img src={item.mobileImg} alt={item.alt} height={600} />
                </div>
              </div>
            ) : (
              <div>
                <div className="c_mainHeader w-100">{item.caption}</div>
                <div className="c_users w-100">
                  <div onClick={openAndroidTab} style={{ cursor: "pointer" }}>
                    <img src={item.androidBtn} alt="android-btn" />
                  </div>
                  <div onClick={openIOSTab} style={{ cursor: "pointer" }}>
                    <img src={item.iosBtn} alt="ios-btn" />
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className={"col-6 d-flex justify-content-center align-items-center flex-column"}>
            {item.rightSideImg ? (
              <div>
                <img src={item.rightSideImg} alt={item.rightSideImg} width={300} />
              </div>
            ) : item.mobileImg ? (
              <div className="row">
                <div className="col-12">
                  <div className="row">
                    {item.svgImg_1 ? (
                      <span />
                    ) : (
                      <div className="col-8">
                        <div>
                          <img src={item.svgImg} alt={item.alt} />
                        </div>
                        <div className="c_header">{item.header}</div>
                        <div className="c_subHeader">{item.subHeader}</div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="col-4"></div>
              </div>
            ) : (
              <span />
            )}
            {item.svgImg_1 ? (
              <div className="row">
                <div className="col-12">
                  <div className="row">
                    <div className="col-12">
                      <div className="row">
                        <div className="col-6">
                          <div>
                            <img src={item.svgImg_1} alt={item.alt} />
                          </div>
                          <div className="cm_header">{item.svgCaption_1}</div>
                          <div className="c_subHeader">{item.svgSubCaption_1}</div>
                        </div>
                        <div className="col-6">
                          <div>
                            <img src={item.svgImg_2} alt={item.alt} />
                          </div>
                          <div className="cm_header">{item.svgCaption_2}</div>
                          <div className="c_subHeader">{item.svgSubCaption_2}</div>
                        </div>
                      </div>
                      <div className="row justify-content-center mt-5">
                        <div>
                          <div>
                            <img src={item.svgImg_2} alt={item.alt} />
                          </div>
                          <div className="cm_header">{item.svgCaption_2}</div>
                          <div className="c_subHeader">{item.svgSubCaption_2}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <span />
            )}
          </div>
        </div>
        {item.rightSideImg ? item.btmContent : <span />}
      </div>
    );
  });

  return slides;
};

export default CustomCarousel;
