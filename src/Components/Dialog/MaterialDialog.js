import React from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";

class MaterialDialog extends React.Component {
  render() {
    return (
      <Dialog open={this.props.open} onClose={this.closeModal} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
        <DialogTitle className="text-center" color="primary" id="alert-dialog-title">
          {this.props.title}
        </DialogTitle>
        {this.props.children}
      </Dialog>
    );
  }
}

export default MaterialDialog;
