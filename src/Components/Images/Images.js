import HeartIcon from "../../asset/ActivePlanModal_Assets/like.png";
import PinkToggleIcon from "../../asset/ActivePlanModal_Assets/control_your_profile.png";
import RewindIcon from "../../asset/ActivePlanModal_Assets/unlimited_rewinds.png";
import LocationIcon from "../../asset/ActivePlanModal_Assets/swipe_around_the_world.png";
import SearchMagnifyingGlass from "../../asset/images/_search.png";
import VideoCall from "../../View/WebHomePage/Components/Chat/svg/_video.svg";
import NoChat from "../../asset/images/no_chat.png";
import User1 from "../../View/WebHomePage/Components/Coin/Assets/user1.png";
import User2 from "../../View/WebHomePage/Components/Coin/Assets/user2.png";
import User3 from "../../View/WebHomePage/Components/Coin/Assets/user3.png";
import User4 from "../../View/WebHomePage/Components/Coin/Assets/user4.png";
import User5 from "../../View/WebHomePage/Components/Coin/Assets/user5.png";
import User6 from "../../View/WebHomePage/Components/Coin/Assets/user6.png";
import EditPencil from "../../asset/images/edit.png";
import Search from "../../asset/images/_search.png";
import ReferralLogo from "../../asset/mobile_img/ref.png";
import Like from "../../asset/images/Cards/like.png";
import SuperLike from "../../asset/images/Cards/super_like.png";
import Dislike from "../../asset/images/Cards/dislike.png";
import NoTransactionsFound from "../../View/WebHomePage/Components/Coin/Assets/coin_history_empty.png";
import SearchChatPage from "../../asset/images/_search.png";
import Placeholder from "../../asset/images/placeholder.png";
import FriendsSearch from "../../asset/images/_search.png";
import BackButton from "../../asset/images/black-left-arrow.svg";
import PayPal from "../../View/WebHomePage/Components/CoinBalance/assets/paypal.svg";
import SentCheck from "../../View/WebHomePage/Components/CoinBalance/assets/sent_check.svg";
import JPaypal from "../../View/WebHomePage/Components/Coin/Assets/j_paypal.png";
import P_Dislike from "../../asset/images/Cards/dislike.png";
import P_Boost from "../../asset/images/Cards/boost.png";
import P_Like from "../../asset/images/Cards/like.png";
import P_SuperLike from "../../asset/images/Cards/super_like.png";
import P_Undo from "../../asset/images/Cards/undo.png";
import P_AlertBox from "../../asset/images/alert-box-outline.svg";
import P_Verified from "../../asset/images/green-verified.svg";
import G1 from "../../asset/images/g1.png";
import G2 from "../../asset/images/g2.png";
import G3 from "../../asset/images/g3.png";
import brokenImage from "../../asset/mobile_img/broken-1.png";
import BackgroundImage from "../../asset/images/wallpaper.png";
import BigHeart from "../../asset/WebUserCard/big-heart.png";

export const Images = {
  BackgroundImage: BackgroundImage,
  brokenImage: brokenImage,
  dollarIcon: BigHeart,
  heartIcon: HeartIcon,
  pinkToggleIcon: PinkToggleIcon,
  rewindIcon: RewindIcon,
  locationIcon: LocationIcon,
  search: SearchMagnifyingGlass,
  videoCall: VideoCall,
  noChats: NoChat,
  User1: User1,
  User2: User2,
  User3: User3,
  User4: User4,
  User5: User5,
  User6: User6,
  editData: EditPencil,
  searchIcon: Search,
  otherProfileLike: Like,
  otherProfileDislike: Dislike,
  otherProfileSuperlike: SuperLike,
  ReferralLogo: ReferralLogo,
  NoTransactionsFound: NoTransactionsFound,
  Search: SearchChatPage,
  placeholder: Placeholder,
  friendsSearch: FriendsSearch,
  backButton: BackButton,
  paypal: PayPal,
  sentCheck: SentCheck,
  JPaypal: JPaypal,
  P_Dislike: P_Dislike,
  P_Boost: P_Boost,
  P_Like: P_Like,
  P_SuperLike: P_SuperLike,
  P_Undo: P_Undo,
  P_AlertBox: P_AlertBox,
  P_Verified: P_Verified,
  G1: G1,
  G2: G2,
  G3: G3,
};
