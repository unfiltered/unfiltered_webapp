// Main React Components
import React from "react";
// Reactstrap Components
import { Modal } from "reactstrap";

function MainModal(props) {
  let toRenderClass;
  let { type } = props;
  if (type === "LoginModal") {
    toRenderClass = "LoginMdialog";
  } else if (type === "Mdialog") {
    toRenderClass = "Mdialog";
  } else if (type === "ActivedPlanModal") {
    toRenderClass = "ActivedPlanModal";
  } else if (type === "coinHistoryModalPosition") {
    toRenderClass = "coinHistoryModalPosition";
  } else if (type === "FilterUserModal") {
    toRenderClass = "FilterUserModal";
  } else if (type === "ProspectsModal") {
    toRenderClass = "ProspectsModal";
  } else if (type === "InChatLocationModal") {
    toRenderClass = "InChatLocationModal";
  } else if (type === "MapModal") {
    toRenderClass = "MapModal";
  } else if (type === "CoinTransactionModal") {
    toRenderClass = "CoinTransactionModal";
  } else if (type === "PayPalModal") {
    toRenderClass = "PayPalModal";
  } else if (type === "_EditImageModal") {
    toRenderClass = "_EditImageModal";
  } else if (type === "m_activedplan") {
    toRenderClass = "m_activedplan";
  } else if (type === "m_textInputModal") {
    toRenderClass = "m_textInputModal";
  } else if (type === "m_settings_report") {
    toRenderClass = "m_settings_report";
  } else if (type === "m_settings_input_text") {
    toRenderClass = "m_settings_input_text";
  } else if (type === "_m_EditImageModal") {
    toRenderClass = "_m_EditImageModal";
  } else if (type === "d_sendLocationFromChat") {
    toRenderClass = "d_sendLocationFromChat";
  } else {
    toRenderClass = "LoginMdialog"; // if modal class not provided
  }
  return (
    <Modal className={toRenderClass} isOpen={props.isOpen} toggle={props.toggle}>
      {props.children}
    </Modal>
  );
}

export default MainModal;
