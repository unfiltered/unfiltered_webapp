import React from "react";
import { Modal } from "reactstrap";

const RS_Modal = (props) => {
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle} className={props.className}>
      {props.children}
    </Modal>
  );
};

export default RS_Modal;
