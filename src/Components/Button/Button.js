import React from "react";

const Button = ({ className, handler, text, disabled }, props) => {
  return (
    <button {...props} className={className} onClick={handler} disabled={disabled ? disabled : false}>
      {text}
    </button>
  );
};

export default Button;
