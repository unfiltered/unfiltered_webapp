// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "./style.scss";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

function TabContainer(props) {
  return (
    <Typography component="div" className="p-0">
      {props.children}
    </Typography>
  );
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: "#fff"
  }
});

class webTabHeader extends Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs, tabContent } = this.props;
    const { value } = this.state;

    return (
      <div className="Web_dates">
        <div className="m-0 row">
          <div className="pt-2 col-sm-6 col-md-9 col-lg-10 text-left date_tabs">
            <div className={classes.root}>
              <AppBar position="static" color="default">
                <Tabs
                  value={value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="scrollable"
                  scrollButtons="auto"
                >
                  {tabs.map((tab, key) => (
                    <Tab key={key} style={{ color: "#000" }} label={tab.label} />
                  ))}
                </Tabs>
              </AppBar>

              {tabContent.map((tabContent, key) => (key === value ? <TabContainer key={key}>{tabContent.content}</TabContainer> : ""))}
            </div>
          </div>
          <div className="py-2 col-sm-6 col-md-3 col-lg-2 text-center">
            <div className="Webdatesboost">
              <Link to="/app/plans">
                <i className="fas fa-rocket">
                  <span style={{ fontFamily: "Avenir" }}>{this.props.boost}</span>
                </i>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(webTabHeader);
