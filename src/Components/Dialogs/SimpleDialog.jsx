import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";

class SimpleDialog extends React.Component {
  render() {
    const { open, handleClose, children, dialogTitle } = this.props;
    return (
      <div>
        <Dialog
          className={this.props.isMobile ? "mobile_dialog" : "desktop_dialog"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          {children}
        </Dialog>
      </div>
    );
  }
}

export default SimpleDialog;
