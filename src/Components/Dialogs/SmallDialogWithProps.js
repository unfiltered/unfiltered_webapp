import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const DialogContent = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit,
  },
}))(MuiDialogActions);

class SmallDialogWithProps extends React.Component {
  render() {
    return (
      <div>
        <Dialog onClose={this.props.handleClose} aria-labelledby="customized-dialog-title" open={this.props.open}>
          <DialogContent>
            <Typography gutterBottom>{this.props.children}</Typography>
          </DialogContent>
          <DialogActions>
            <Link to="/app/chat" onClick={this.props.closeModal}>
              <Button color="primary">{this.props.text}</Button>
            </Link>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default SmallDialogWithProps;
