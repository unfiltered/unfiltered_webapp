// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// Material-UI Components
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  list: {
    width: 374
  },
  fullList: {
    width: "auto"
  }
});

class BottomDrawer extends Component {
  state = {
    left: false
  };

  render() {
    return (
      <div>
        <Drawer
          anchor="bottom"
          width={this.props.width}
          open={this.props.open}
          onOpen={this.props.onOpen}
        >
          <div tabIndex={0} role="button" onClose={this.props.onClose}>
            {this.props.children}
          </div>
        </Drawer>
      </div>
    );
  }
}

BottomDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(BottomDrawer);
