// Main React Components
import React, { Component } from "react";

// Prop-Type Checker
import PropTypes from "prop-types";

// Material-UI Components
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  list: {
    width: 374,
  },
  fullList: {
    width: "auto",
  },
  mobileView: {
    height: "100vh",
    width: "100vw",
    background: "#161616",
  },
  mobileWidth: { width: "100vw" },
});

class MainDrawer extends Component {
  state = {
    left: false,
  };

  render() {
    const { classes } = this.props;
    return (
      <Drawer anchor="right" width={this.props.width} open={this.props.open} onOpen={this.props.onOpen} className={classes.mobileWidth}>
        <div tabIndex={0} role="button" onClose={this.props.onClose} className={classes.mobileView}>
          {this.props.children}
        </div>
      </Drawer>
    );
  }
}

MainDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainDrawer);
