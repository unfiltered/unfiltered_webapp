// Main React Components
import React, { Component } from "react";

// Reusable Components
import MainDrawer from "../Drawer/Drawer";

// imported Components
import Setting from "../../View/MobileHomePage/Components/Settings/Setings";
// Redux Components
import { UserProfileData } from "../../controller/auth/verification";

class Profilepic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profiledrawer: false,
      UserFinalData: ""
    };
  }

  componentDidMount() {
    UserProfileData().then(data => {
      this.setState({ UserFinalData: data.data.data });
    });
  }

  openprofiledrawerDrawer = () => {
    this.setState({ profiledrawer: true });
  };

  closeprofiledrawerDrawer = () => {
    this.setState({ profiledrawer: false });
  };

  render() {
    const ProfileDrawer = <Setting onClose={this.closeprofiledrawerDrawer} />;

    return (
      <div className="d-inline-block">
        <MainDrawer width={400} onClose={this.closeprofiledrawerDrawer} onOpen={this.openprofiledrawerDrawer} open={this.state.profiledrawer}>
          {ProfileDrawer}
        </MainDrawer>
        <div className="p-0 Msettings" onClick={this.openprofiledrawerDrawer}>
          <img src={this.state.UserFinalData.profilePic} alt={this.state.UserFinalData.firstName} title={this.state.UserFinalData.firstName} />
        </div>
      </div>
    );
  }
}

export default Profilepic;
