// Main React Components
import React, { Component } from "react";

// React Props-Type Checker
import PropTypes from "prop-types";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "#e31b1b"
  },
  disabledButton: {
    backgroundColor: "#f8f8f8"
  }
});

class PrimaryButton extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button
          variant="contained"
          onClick={this.props.onClick}
          disabled={this.props.disabled}
          className={
            this.props.disabled ? classes.disabledButton : classes.button
          }
        >
          {this.props.name}
        </Button>
      </div>
    );
  }
}

PrimaryButton.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PrimaryButton);
