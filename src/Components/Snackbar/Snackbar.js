// Main React Components
import React, { Component } from "react";

// Material-UI Components
import { withStyles } from "@material-ui/core/styles";
import MySnackbarContentWrapper from "./index";
import Snackbar from "@material-ui/core/Snackbar";

const styles2 = (theme) => ({
  margin: {
    margin: theme.spacing.unit,
  },
});

class CustomizedSnackbars extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Snackbar
          anchorOrigin={
            this.props.anchorOrigin
              ? this.props.anchorOrigin
              : {
                  vertical: "bottom",
                  horizontal: "left",
                }
          }
          open={this.props.open}
          autoHideDuration={this.props.timeout ? this.props.timeout : 1000}
          onClose={this.props.onClose}
        >
          <MySnackbarContentWrapper variant={this.props.type} className={classes.margin} message={this.props.message} />
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(styles2)(CustomizedSnackbars);
