import React from "react";
import { Icons } from "../Icons/Icons";

const Loader = ({ text }) => {
  return (
    <div className="m_loading_loader">
      <div className="text-center">
        <img src={Icons.DotsSpinner} alt="spinner" height={60} />
      </div>
      <div>{text}</div>
    </div>
  );
};

export default Loader;
