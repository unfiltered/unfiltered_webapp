import React from "react";

class Input extends React.Component {
  render() {
    return (
      <>
        <input
          className={this.props.className}
          {...this.props}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}
        />
      </>
    );
  }
}

export default Input;
