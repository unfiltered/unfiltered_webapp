import React from "react";
import TextField from "@material-ui/core/TextField";

class MATTextField extends React.Component {
  render() {
    return (
      <>
        <TextField {...this.props} type={this.props.type} onChange={this.props.onChange} defaultValue={this.props.defaultValue} className={this.props.className} />
      </>
    );
  }
}

export default MATTextField;
