import React from "react";
import TextField from "@material-ui/core/TextField";
import Input from "@material-ui/core/Input";

class MaterialInput extends React.Component {
  render() {
    return (
      <TextField
        fullWidth={this.props.fullWidth ? this.props.fullWidth : true}
        id="standard-multiline-flexible"
        label={this.props.label}
        multiline
        rowsMax={this.props.maxRows}
        value={this.props.value}
        onChange={this.props.onChange}
        InputProps={this.props.InputProps}
        InputLabelProps={this.props.InputLabelProps}
        className={this.props.className}
        margin="normal"
        type={this.props.type}
        {...this.props}
      />
    );
  }
}

export default MaterialInput;
