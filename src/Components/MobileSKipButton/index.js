// Main React Components
import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class SkipButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.HandleSubmit = this.HandleSubmit.bind(this);
  }

  HandleSubmit() {
    this.props.history.push("/app/");
  }

  render() {
    return (
      <div>
        <div style={{ position: "fixed", right: "20px", top: "13px" }}>
          <span onClick={this.HandleSubmit}>{this.props.name}</span>
        </div>
      </div>
    );
  }
}

export default withRouter(SkipButton);
