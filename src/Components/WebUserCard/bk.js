// Main React Components
import React, { Component } from "react";
import { Link } from "react-router-dom";

// Scss
import "./style.scss";

// Redux Components
import { getCookie } from "../../lib/session";

class UserCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tooltipOpenfans: false,
      tooltipOpenverfired: false,
      UserMainData: ""
    };
    this.toggleFans = this.toggleFans.bind(this);
    this.toggleVerified = this.toggleVerified.bind(this);
  }

  toggleFans() {
    this.setState({
      tooltipOpenfans: !this.state.tooltipOpenfans
    });
  }

  toggleVerified() {
    this.setState({
      tooltipOpenverfired: !this.state.tooltipOpenverfired
    });
  }

  render() {
    return (
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div className="ml-2 row">
          {this.props.Userresult && this.props.Userresult
            ? this.props.Userresult.map((data, index) => (
                <div
                  key={index}
                  className="pl-md-0 px-3 mt-3 col-6 col-sm-6 col-md-6 col-lg-3 col-xl-3 Wmain_card"
                >
                  <Link to={`/app/user/${data.firstName}/${data.opponentId}`}>
                    <div className="WUsercard">
                      <div className="WPhotocount">
                        <i className="fas fa-camera">
                          <span>2</span>
                        </i>
                      </div>
                      <div className="WPhoto">
                        <img
                          src={
                            data.otherImages.length > 0
                              ? data.otherImages[0]
                              : data.profilePic
                          }
                          alt={data.firstName}
                          title={data.firstName}
                          className="img-fluid"
                        />
                      </div>
                      <div className="WUserdata">
                        <p className="m-0">
                          {data.firstName}, {data.age ? data.age.value : ""}
                        </p>
                        <i className="fas fa-flag-usa">
                          <span>{getCookie("UserCurrentCity")}</span>
                        </i>
                      </div>
                    </div>
                  </Link>
                </div>
              ))
            : " "}
        </div>
      </div>
    );
  }
}

export default UserCard;
