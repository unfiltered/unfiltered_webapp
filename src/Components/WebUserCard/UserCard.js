import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./style.scss";
import * as actionChat from "../../actions/chat";
import MainModel from "../Model/model";
import Snackbar from "../Snackbar/Snackbar";
import { connect } from "react-redux";
import { SuperLikeNearPeople, LikeNearPeople, MyDisLikeUser } from "../../controller/auth/verification";
import { Icons } from "../../Components/Icons/Icons";
import Card from "./Card";
import { FormattedMessage } from "react-intl";
import Button from "../Button/Button";

class UserCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserSearchresult: "",
      modal: false,
    };

    this.refs = React.createRef(null);
    this.toggle = this.toggle.bind(this);
  }

  // Function for the Model Toggle
  toggle() {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  }

  // Function for the Notification ( Snackbar )
  handleClicksnakbar = () => {
    this.setState({ open: true });
  };

  // Function for the Notification ( Snackbar )
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  // API Call to UnLike the User
  handledunlikeuser = (data) => {
    let Unlikepayload = {
      targetUserId: data.opponentId,
    };

    MyDisLikeUser(Unlikepayload)
      .then((data) => {
        this.props.handleNearPeopleUpdated();
        this.setState({
          open: true,
          variant: "success",
          usermessage: "UserName Passed successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Some error occured.",
        });
      });
  };

  // API Call to Like the User
  handlelikeuser = (data) => {
    if (this.props.remainsLikesInString > 0 || this.props.remainsLikesInString === "unlimited") {
      LikeNearPeople({ targetUserId: data.opponentId })
        .then((data) => {
          this.props.handleNearPeopleUpdated();
          this.setState({
            open: true,
            variant: "success",
            usermessage: "UserName Liked successfully.",
          });
        })
        .catch((error) => {
          this.setState({
            open: true,
            variant: "error",
            usermessage: "Some error occured.",
          });
        });
    } else {
      this.setState({
        open: true,
        variant: "error",
        usermessage: "Your Likes have exhausted.",
      });
    }
  };

  // API Call to SuperLike the User (Coin Availble)
  handlesuperlikeuser = (data) => {
    SuperLikeNearPeople({ targetUserId: data.opponentId })
      .then((data) => {
        this.props.handleNearPeopleUpdated();
        if (
          this.props.checkIfUserIsProUser &&
          this.props.checkIfUserIsProUser.ProUserDetails &&
          this.props.checkIfUserIsProUser.ProUserDetails.rewindCount !== "unlimited" &&
          this.props.CoinBalance > 0
        ) {
          this.props.coinReduce(this.props.CoinConfig.superLike.Coin);
        }
        this.setState({
          open: true,
          variant: "success",
          usermessage: "UserName Superliked successfully.",
        });
      })
      .catch((error) => {
        this.setState({
          open: true,
          variant: "error",
          usermessage: "Something went wrong...",
        });
      });
  };

  // API Call to SuperLike the User (NO Coin Availble)
  handlesuperlikeusernoCoin = () => {
    this.toggle();
  };

  conditionToRenderCardsBasedOnArraySize = (array) => {
    if (array.length >= 5) {
      return "mainGrid";
    } else if (array.length <= 4 && array.length > 0) {
      return "cardsLessThan4";
    } else if (array.length === 0) {
      return "noCards";
    }
  };

  render() {
    let { UserSearchresult, CoinBalance } = this.props;
    /** Card.js => a single card file which shows the people related data in form of card, includes animation 
         and click listeners ,
         if no data found, it will show no data found image with text 
      */

    return (
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Main_Content h-100" id="user-list">
        <div className={this.conditionToRenderCardsBasedOnArraySize(UserSearchresult)}>
          {UserSearchresult && UserSearchresult.length > 0 ? (
            UserSearchresult.map((data, index) => (
              <div key={index}>
                <Card
                  data={data}
                  index={index}
                  handlesuperlikeusernoCoin={this.handlesuperlikeusernoCoin}
                  justViewProfileActive={this.props.justViewProfileActive}
                  handledunlikeuser={this.handledunlikeuser}
                  handlelikeuser={this.handlelikeuser}
                  handlesuperlikeuser={this.handlesuperlikeuser}
                  CoinBalance={CoinBalance}
                />
              </div>
            ))
          ) : (
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
              <div className="Wcommon_nodata">
                <img src={Icons.NoUsers} alt="NoData" title="NoData" height={300} />
                <h5 className="py-3 text-muted">
                  <FormattedMessage id="message.noNearbyPeopleV2" />.
                </h5>
              </div>
            </div>
          )}
        </div>
        {UserSearchresult && UserSearchresult.length > 19 ? (
          <div className="loadMore">
            <Button text={this.props.loaderStatus} handler={this.props.loadMore} />
          </div>
        ) : (
          ""
        )}

        {/* Snakbar Components */}
        <Snackbar type={this.state.variant} message={this.state.usermessage} open={this.state.open} onClose={this.handleClose} />

        {/* Model for the SuprLike the nearb People */}
        <MainModel isOpen={this.state.modal} type="ActivedPlanModal">
          {/* Header Module */}
          <div className="py-4 text-center col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div className="pt-3 Werror_signin">
              <i className="far fa-times-circle" onClick={this.toggle} />
              <div className="WCharge_coins">
                <i className="fas fa-coins" />
              </div>
              <h4>Oops, Not Enough Coin.</h4>
              <p className="m-0">
                You have no more Credits left to Superlike the Profile. Please recharge your wallet to continue further.
              </p>
              <div className="Wcharge_wallet">
                <button>
                  <Link to="/app/coinbalance">Buy Credits</Link>
                </button>
              </div>
            </div>
          </div>
        </MainModel>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    coinReduce: (amount) => dispatch(actionChat.coinChat(amount)),
  };
};

const mapstateToProps = (state) => {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
    selectedProfile: state.selectedProfile.selectedProfile.selectedProfile,
    CoinConfig: state.CoinConfig.CoinConfig,
    checkIfUserIsProUser: state.ProUser,
  };
};

export default connect(mapstateToProps, mapDispatchToProps)(UserCard);
