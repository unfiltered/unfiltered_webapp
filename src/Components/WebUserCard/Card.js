import React from "react";
import { Link } from "react-router-dom";
import { getCookie } from "../../lib/session";
import { Icons } from "../Icons/Icons";
import { Images } from "../Images/Images";

// adds animation on the card when mouse enters the card(div)
function onMouseEnterAnimation(id) {
  var user_data = document.getElementById("WUserdata_" + id);
  user_data.style.bottom = "50px";
  user_data.style.transition = "0.3s";
  let user_action = document.getElementById("Wuser_action_" + id);
  user_action.style.bottom = "0px";
  user_action.style.transition = "0.3s";
  user_action.style.background = "white";
  user_action.style.opacity = 0.9;
}

// removes animation on the card when mouse enters the card(div)
function onMouseLeaveAnimation(id) {
  var user_data = document.getElementById("WUserdata_" + id);
  user_data.style.bottom = "10px";
  let user_action = document.getElementById("Wuser_action_" + id);
  user_action.style.bottom = "-20px";
  user_action.style.opacity = 0;
}

function Card(props) {
  let { firstName, opponentId, otherImages, profilePic, city } = props && props.data;
  let { value, isHidden } = props && props.data.age;
  let { index, handlesuperlikeusernoCoin, handledunlikeuser, handlelikeuser, handlesuperlikeuser, CoinBalance } = props && props;

  const onError = (e) => {
    e.target.src = Images.placeholder;
  };

  return (
    <div className="WUsercard" onMouseEnter={() => onMouseEnterAnimation(index)} onMouseLeave={() => onMouseLeaveAnimation(index)}>
      <Link to={`/app/user/${firstName}/${opponentId}`}>
        <div className="WPhotocount">
          <i className="fas fa-camera">
            <span>{otherImages ? otherImages.length + 1 : 1}</span>
          </i>
        </div>
        <div className="WPhoto">
          <img
            src={profilePic.includes("blob:http") ? Images.placeholder : profilePic}
            alt={firstName}
            onError={onError}
            title={firstName}
            className="img-fluid"
          />
        </div>
        <div className="WUserdata" id={"WUserdata_" + index}>
          <p className="m-0">{isHidden ? firstName : firstName + ", " + value}</p>
          <span>{city}</span>
        </div>
      </Link>
      <div className="Wuser_action" id={"Wuser_action_" + index}>
        <img src={Icons.NewDislike} width={28} height={28} onClick={() => handledunlikeuser(props.data)} />
        <img src={Icons.NewLike} width={28} height={28} onClick={() => handlelikeuser(props.data)} />
        {CoinBalance ? (
          <img src={Icons.NewSuperLike} width={28} height={28} onClick={() => handlesuperlikeuser(props.data)} />
        ) : (
          <img src={Icons.NewSuperLike} width={28} height={28} onClick={handlesuperlikeusernoCoin} />
        )}
      </div>
    </div>
  );
}

export default Card;
