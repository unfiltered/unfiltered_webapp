import React from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";

function TabContainer(props) {
  return (
    <Typography component="div" style={props.isMobile ? { padding: 0 } : { padding: 10 }}>
      {props.children}
    </Typography>
  );
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  tabIndicator: {
    borderBottom: "2px solid #f80402",
    color: "#f80402",
  },
  labelActive: {
    color: "#f80402",
    fontSize: "13px",
    textTransform: "capitalize",
  },
  labelNotActive: {
    color: "#fff",
    fontSize: "13px",
    textTransform: "capitalize",
  },
  none: {},
});

class ScrollableTabs extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
    if (this.props.mobile) {
      this.props.setValue(value);
    }
  };

  renderLabel = (tab) => {
    if (tab === "Requests") {
      return (
        <div>
          <span>Requests</span>{" "}
          {this.props.friendRequests > 0 ? (
            <span
              style={{ height: "20px", width: "20px", color: "#FFF", display: "inline-block", borderRadius: "50%", background: "#e31b1b" }}
            >
              {this.props.friendRequests}
            </span>
          ) : (
            ""
          )}
        </div>
      );
    } else if (tab === "Friends") {
      return (
        <div>
          <span>Friends</span>{" "}
          {this.props.allFriends > 0 ? (
            <span
              style={{ height: "20px", width: "20px", color: "#FFF", display: "inline-block", borderRadius: "50%", background: "#e31b1b" }}
            >
              {this.props.allFriends}
            </span>
          ) : (
            ""
          )}
        </div>
      );
    } else {
      return tab;
    }
  };

  render() {
    const { classes, tabs, tabContent } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="sticky" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            // variant="fullWidth"
            scrollButtons="auto"
            centered={this.props.isCentered}
            classes={{
              indicator: classes.tabIndicator,
            }}
          >
            {tabs.map((tab, key) => (
              <Tab
                classes={{
                  label: this.props.mobile ? (value === key ? classes.labelActive : classes.labelNotActive) : classes.none,
                }}
                key={key}
                style={{ color: "#000", fontFamily: "Product Sans" }}
                label={this.renderLabel(tab.label)}
              />
            ))}
          </Tabs>
        </AppBar>

        {tabContent.map((tabContent, key) => (key === value ? <TabContainer key={key}>{tabContent.content}</TabContainer> : ""))}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    friendRequests: state.Friends.FriendRequest,
  };
};

export default connect(mapStateToProps, null)(withStyles(styles, { withTheme: true })(ScrollableTabs));
