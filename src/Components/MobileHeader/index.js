// Main React Components
import React from "react";
import DollarIcon from "../../asset/images/dollar.png";
import { connect } from "react-redux";

function MobileHeader({ header, openCoinBalancesDrawer, CoinBalance }) {
  return (
    <div className="py-3 col-12">
      <div className="m-0 row">
        <div className="p-0 col-4" />
        <div className="p-0 col-4 text-center">
          <span>
            <strong>{header}</strong>
          </span>
        </div>
        <div className="p-0 col-4 text-right">
          <div className="coinbalances" onClick={openCoinBalancesDrawer}>
            <img src={DollarIcon} className="img-fluid" alt="dollarimg" title="dollarimg" />
            <p className="m-0">{CoinBalance}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

function mapstateToProps(state) {
  return {
    UserProfile: state.UserProfile.UserProfile,
    CoinBalance: state.UserProfile.CoinBalance,
  };
}

export default connect(mapstateToProps, null)(MobileHeader);
