import React from "react";
import { FormattedMessage } from "react-intl";
import { Icons } from "../Icons/Icons";

export const webRoutes = [
  {
    linkName: <FormattedMessage id="message.sidepanel_nearbyPeople" />,
    url: true,
    urlName: "/app",
    src: Icons.U_LocationMap,
    srcActive: Icons.U_LocationMap,
    hw: 22,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_discover" />,
    url: true,
    urlName: "/app/discover",
    src: Icons.U_Globe,
    srcActive: Icons.U_Globe,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_chat" />,
    url: true,
    urlName: "/app/chat",
    src: Icons.U_Chat,
    srcActive: Icons.U_Chat,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_prospects" />,
    url: true,
    urlName: "/app/prospects",
    src: Icons.U_Prospects,
    srcActive: Icons.U_Prospects,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_friends" />,
    url: true,
    urlName: "/app/friends",
    src: Icons.U_Friends,
    srcActive: Icons.U_Friends,
    hw: 18,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_dates" />,
    url: true,
    urlName: "/app/dates",
    src: Icons.U_Dates,
    srcActive: Icons.U_Dates,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_newsFeed" />,
    url: true,
    urlName: "/app/newsFeed",
    src: Icons.U_NewsFeed,
    srcActive: Icons.U_NewsFeed,
    hw: 20,
  },

  {
    linkName: <FormattedMessage id="message.sidepanel_referral" />,
    url: false,
    urlName: "/app/referral",
    src: Icons.U_Referral,
    srcActive: Icons.U_Referral,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_settings" />,
    url: true,
    urlName: "/app/settings",
    src: Icons.U_Settings,
    srcActive: Icons.U_Settings,
    hw: 20,
  },
  {
    linkName: <FormattedMessage id="message.sidepanel_logout" />,
    url: false,
    urlName: "/app/logout",
    src: Icons.U_SignOut,
    srcActive: Icons.U_SignOut,
    hw: 20,
  },
];
