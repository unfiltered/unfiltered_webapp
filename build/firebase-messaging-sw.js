importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js");

firebase.initializeApp({
  apiKey: "AIzaSyD3beR2AKzwhH8mpovDVAuLX5klBtFNAe0",
  authDomain: "unfiltered-783bb.firebaseapp.com",
  projectId: "unfiltered-783bb",
  storageBucket: "unfiltered-783bb.appspot.com",
  messagingSenderId: "545075574083",
  appId: "1:545075574083:web:943c5332b5a002f36eabf6",
});
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log("background message payload", payload);
  // Customize notification here
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.message,
    icon: "./cmeLogo.png",
  };
  return self.registration.showNotification(notificationTitle, notificationOptions);
});
